﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ImportTrackProcessDialogController : MonoBehaviour {

	public ImportTrackSceneManager manager;
	public Text percentLabel;
	public Text workLabel;
	public Text successedLabel;
	public GameObject okButtonObj;
	public ProgressBar progressBar;
	public GameObject loadingCircleObj;

	private float? oldProgress { get; set; }
	private bool? oldSuccess { get; set; }

	public void Show () {
		SetUI ();
		gameObject.SetActive (true);
	}

	public void Hide () {
		gameObject.SetActive (false);
	}

	void SetUI () {
		if (!oldProgress.HasValue || manager.progressed != oldProgress) {
			percentLabel.text = (manager.progressed * 100.0f).ToString ("F2") + "%";
			workLabel.text = manager.processingWork;
			oldProgress = manager.progressed;
			progressBar.SetProgress (manager.progressed);
		}
		if (!oldSuccess.HasValue || manager.successed != oldSuccess) {
			if (!manager.successed) {
				successedLabel.gameObject.SetActive (false);
				okButtonObj.gameObject.SetActive (false);
				percentLabel.gameObject.SetActive (true);
				workLabel.gameObject.SetActive (true);
				loadingCircleObj.SetActive (true);
			}
			else {
				successedLabel.gameObject.SetActive (true);
				okButtonObj.gameObject.SetActive (true);
				percentLabel.gameObject.SetActive (false);
				workLabel.gameObject.SetActive (false);
				loadingCircleObj.SetActive (false);
			}
			oldSuccess = manager.successed;
		}
	}

	// Use this for initialization
	void Start () {
		SetUI ();
	}
	
	// Update is called once per frame
	void Update () {
		SetUI ();
	}

	public void DecideSoundEffectPlay () {
		Utility.SoundEffectManager.Play ("se_decide");
	}
}
