using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

public class ImportTrackSceneManager : MonoBehaviour {

	[SerializeField]
	private ImportTrackListController listCtrl;
	[SerializeField]
	private UnityEngine.UI.Image guideCircle;
	[SerializeField]
	private GameObject sensorUIPrefab;
	[SerializeField]
	private RectTransform sensorUIParent;
	[SerializeField]
	private ConfigSceneDetailUIController detailUICtrl;
	[SerializeField]
	private ImportTrackScoreEditTextFieldController scoreEditFieldCtrl;
	[SerializeField]
	private GameObject pathSelectDialogPrefab;
	[SerializeField]
	private RectTransform pathSelectDialogParent;
	[SerializeField]
	private GameObject yesNoDialogPrefab;
	[SerializeField]
	private Transform yesNoDialogParent;
	[SerializeField]
	private ImportTrackProcessDialogController processDialog;
	[SerializeField]
	private GameObject okDialogPrefab;
	[SerializeField]
	private FadeController fade;
	
	private Sprite[] sensorIcons;
	
	private Dictionary<ImportTrackListInformation.ImportMenu, ImportTrackListInformation> contentsSource { get; set; }
	private Dictionary<string, ImportTrackListInformation> trackContentsSource { get; set; }
	public ImportTrackListInformation[] contents { get; set; }
	public bool forceUpdateContents { get; set; }
	private string refineWord { get; set; }

	private SensorUIImageController[] sensorImages { get; set; }

	private float sensorTurboSpeed0 { get; set; }
	private float sensorTurboSpeed3 { get; set; }
	private float sensorTurboFirstSpeed { get { return 65.0f; } }
	private float sensorTurboAddSpeed { get { return 180.0f * Time.deltaTime; } }
	private float sensorTurboMaxSpeed { get { return sensorTurboFirstSpeed * 1.5f; } }
	
	public ImportTrackListInformation.ManagerState state { get; set; }
	public ImportTrackListInformation.ImportMenu selectedMenuId { get; set; }
	public string selectedTrackId { get; set; }
	public string selectedScoreId { get; set; }
	
	private SimaiRawReader converter { get; set; }
	public float progressed { get; private set; }
	public string processingWork { get; private set; }
	public bool successed { get; private set; }

	private float trackIdListScrollValue;
	private float scoreIdListScrollValue;

	// Use this for initialization
	IEnumerator Start () {
		while (UserDataController.instance == null) {
			yield return null;
		}

		sensorIcons = new Sprite[] {
			SpriteTank.Get ("icon_allow_up"),
			SpriteTank.Get ("icon_dammy"),
			SpriteTank.Get ("icon_dammy"),
			SpriteTank.Get ("icon_allow_down"),
			SpriteTank.Get ("icon_step_edit"),
			SpriteTank.Get ("icon_step_back"),
			SpriteTank.Get ("icon_dammy"),
			SpriteTank.Get ("icon_dammy"),
		};
		CreateSensors ();

		converter = new SimaiRawReader ();
		if (MiscInformationController.instance.importTrackRequesterSceneName == "editor") {
			MiscInformationController.instance.importTrackRequesterSceneName = string.Empty;
			selectedTrackId = EditorInformationController.instance.trackId;
			selectedScoreId = EditorInformationController.instance.scoreId;
			ChangeState (ImportTrackListInformation.ManagerState.EDIT_SCORE);
			selectedMenuId = ImportTrackListInformation.ImportMenu.EDIT_SCORE;
			scoreEditFieldCtrl.Show (EditorInformationController.instance.scoreScript, EditorInformationController.instance.scriptType);
		}
		else {
			selectedTrackId = string.Empty;
			selectedScoreId = string.Empty;
			selectedMenuId = ImportTrackListInformation.ImportMenu.NONE;
			ChangeState (ImportTrackListInformation.ManagerState.IMPORT);
		}
		
		float radius = (Constants.instance.MAIMAI_OUTER_RADIUS + 5) * 2;
		guideCircle.transform.RectCast ().sizeDelta = new Vector2 (radius, radius);
		while (MaipadDynamicCreatedSpriteTank.instance.guideCircleMenu == null) {
			yield return null;
		}
		guideCircle.sprite = MaipadDynamicCreatedSpriteTank.instance.guideCircleMenu;

		if (MiscInformationController.instance.lastLoadedBgmKey != "maisq_bgm_main") {
			AudioManagerLite.Pause ();
			AudioManagerLite.Load (Utility.SoundEffectManager.GetClip ("maisq_bgm_main"));
			MiscInformationController.instance.lastLoadedBgmKey = "maisq_bgm_main";
			AudioManagerLite.Seek (0);
			AudioManagerLite.Play (true);
		}
	}

	private Dictionary<ImportTrackListInformation.ImportMenu, ImportTrackListInformation> CreateImportContents () {
		var c = new Dictionary<ImportTrackListInformation.ImportMenu, ImportTrackListInformation> ();
		CreateContent (c, ImportTrackListInformation.CreateInfo(ImportTrackListInformation.ImportMenu.IMPORT_OFFICIAL_WEB_SITE, "Import from official web site", null));
		CreateContent (c, ImportTrackListInformation.CreateInfo(ImportTrackListInformation.ImportMenu.IMPORT_FILELIST, "Import from filelist.txt", null));
		CreateContent (c, ImportTrackListInformation.CreateInfo(ImportTrackListInformation.ImportMenu.IMPORT_MAIDATA, "Import from maidata.txt", null));
		CreateContent (c, ImportTrackListInformation.CreateInfo(ImportTrackListInformation.ImportMenu.TRACK_LIST, "Track Manager", null));
		return c;
	}

	private Dictionary<string, ImportTrackListInformation> CreateTrackListContents () {
		var c = new Dictionary<string, ImportTrackListInformation> ();
		List<Dictionary<string, object>> tracksData = new List<Dictionary<string, object>> ();
		foreach (var trackId in UserDataController.instance.tracks.Keys) {
			// ID中に検索ワードが含まれていたら表示する. 検索ワードが空なら全表示.
			if (string.IsNullOrEmpty(refineWord) || trackId.Contains (refineWord)) {
				tracksData.Add (new Dictionary<string, object>() { { "id", trackId }, { "imported_at", UserDataController.instance.GetTrack (trackId).imported_at.ToSystem() } });
			}
		}
		tracksData.Sort ((x, y) => ((System.DateTime)x ["imported_at"]).CompareTo ((System.DateTime)y ["imported_at"]));
		foreach (var trackData in tracksData) {
			CreateTrackContent (c, ImportTrackListInformation.CreateTrackInfo((string)trackData["id"]));
		}
		return c;
	}
	
	private Dictionary<ImportTrackListInformation.ImportMenu, ImportTrackListInformation> CreateEditTrackContents () {
		var c = new Dictionary<ImportTrackListInformation.ImportMenu, ImportTrackListInformation> ();
		var track = UserDataController.instance.GetTrack (selectedTrackId);
		if (track == null) return c;
		CreateContent (c, ImportTrackListInformation.CreateInfo(ImportTrackListInformation.ImportMenu.EDIT_TRACK_ID, "ID", selectedTrackId));
		CreateContent (c, ImportTrackListInformation.CreateInfo(ImportTrackListInformation.ImportMenu.EDIT_TITLE, "Title", track.title ?? string.Empty));
		CreateContent (c, ImportTrackListInformation.CreateInfo(ImportTrackListInformation.ImportMenu.EDIT_TITLE_RUBY, "Title Furigana", track.title_ruby ?? string.Empty));
		CreateContent (c, ImportTrackListInformation.CreateInfo(ImportTrackListInformation.ImportMenu.EDIT_ARTIST, "Artist", track.artist ?? string.Empty));
		CreateContent (c, ImportTrackListInformation.CreateInfo(ImportTrackListInformation.ImportMenu.EDIT_ARTIST_RUBY, "Artist Furigana", track.artist_ruby ?? string.Empty));
		CreateContent (c, ImportTrackListInformation.CreateInfo(ImportTrackListInformation.ImportMenu.EDIT_BPM, "BPM", track.whole_bpm ?? string.Empty));
		CreateContent (c, ImportTrackListInformation.CreateAudioInfo(this, "Audio", selectedTrackId, ()=>forceUpdateContents = true));
		CreateContent (c, ImportTrackListInformation.CreateJacketInfo(this, "Jacket", selectedTrackId, ()=>forceUpdateContents = true));
		CreateContent (c, ImportTrackListInformation.CreateInfo(ImportTrackListInformation.ImportMenu.SCORE_LIST, "Scores", null));
		return c;
	}
	
	private Dictionary<string, ImportTrackListInformation> CreateScoreListContents () {
		var c = new Dictionary<string, ImportTrackListInformation> ();
		var track = UserDataController.instance.GetTrack (selectedTrackId);
		if (track == null) return c;
		var scores = track.scores;
		foreach (var scoreId in scores.Keys) {
			CreateTrackContent (c, ImportTrackListInformation.CreateScoreInfo(scoreId));
		}
		return c;
	}

	private Dictionary<ImportTrackListInformation.ImportMenu, ImportTrackListInformation> CreateEditScoreContents () {
		var c = new Dictionary<ImportTrackListInformation.ImportMenu, ImportTrackListInformation> ();
		var score = UserDataController.instance.GetScore (selectedTrackId, selectedScoreId);
		if (score == null) return c;
		CreateContent (c, ImportTrackListInformation.CreateInfo(ImportTrackListInformation.ImportMenu.EDIT_SCORE_ID, "ID", selectedScoreId));
		CreateContent (c, ImportTrackListInformation.CreateInfo(ImportTrackListInformation.ImportMenu.EDIT_LEVEL, "Level", score.level ?? string.Empty));
		CreateContent (c, ImportTrackListInformation.CreateInfo(ImportTrackListInformation.ImportMenu.EDIT_DESIGNER, "Notes Design", score.notes_design ?? string.Empty));
		CreateContent (c, ImportTrackListInformation.CreateInfo(ImportTrackListInformation.ImportMenu.EDIT_SCORE, "Score", null));
		return c;
	}
	
	private void CreateContent (Dictionary<ImportTrackListInformation.ImportMenu, ImportTrackListInformation> contents, ImportTrackListInformation content) {
		contents[content.menuId] = content;
	}

	private void CreateTrackContent (Dictionary<string, ImportTrackListInformation> contents, ImportTrackListInformation content) {
		contents[content.key] = content;
	}

	private void ChangeState (ImportTrackListInformation.ManagerState state) {
		this.state = state;
		bool isTrackContents = false;
		bool isRefineSupported = false;
		bool isPlusMinusSupported = true;
		switch (state) {
		case ImportTrackListInformation.ManagerState.IMPORT:
			contentsSource = CreateImportContents();
			isPlusMinusSupported = false;
			break;
		case ImportTrackListInformation.ManagerState.SELECT_TRACK:
			trackContentsSource = CreateTrackListContents();
			isTrackContents = true;
			isRefineSupported = true;
			break;
		case ImportTrackListInformation.ManagerState.EDIT_TRACK:
			contentsSource = CreateEditTrackContents();
			break;
		case ImportTrackListInformation.ManagerState.SELECT_SCORE:
			trackContentsSource = CreateScoreListContents();
			isTrackContents = true;
			break;
		case ImportTrackListInformation.ManagerState.EDIT_SCORE:
			contentsSource = CreateEditScoreContents();
			break;
		}
		if (!isTrackContents) {
			contents = new ImportTrackListInformation[contentsSource.Values.Count];
			contentsSource.Values.CopyTo (contents, 0);
		}
		else {
			contents = new ImportTrackListInformation[trackContentsSource.Values.Count];
			trackContentsSource.Values.CopyTo (contents, 0);
		}
		RefineWordSensorActivate (isRefineSupported);
		PlusMinusSensorIconActivate (isPlusMinusSupported);
		listCtrl.ResizeScrollView ();
	}
	
	private void CreateSensors() {
		sensorImages = new SensorUIImageController[8];
		for (int i = 0; i < 8; i++) {
			var sensorImageObj = Instantiate(sensorUIPrefab) as GameObject;
			sensorImageObj.name = "Sensor for UI " + (i + 1).ToString();
			sensorImageObj.SetParentEx (sensorUIParent);
			var sensorImage = sensorImageObj.GetComponent<SensorUIImageController>();
			sensorImage.SetPosition (Constants.instance.GetPieceDegree(i), Constants.instance.MAIMAI_OUTER_RADIUS);
			sensorImage.Setup(sensorIcons[i]);
			sensorImages[i] = sensorImage;
		}

		// Scroll Up.
		sensorImages [0].onSensorHoldDownRepeat = () => {
			Utility.SoundEffectManager.Play ("se_select");
			float speed;
			if (sensorTurboSpeed0 == 0) {
				speed = sensorTurboFirstSpeed;
			}
			else {
				speed = sensorTurboSpeed0;
			}
			sensorTurboSpeed0 += sensorTurboAddSpeed;
			if (speed > sensorTurboMaxSpeed) speed = sensorTurboMaxSpeed;
			var rect = listCtrl.transform.RectCast ();
			rect.anchoredPosition = new Vector2 (0, rect.anchoredPosition.y - speed);
		};
		sensorImages [0].onSensorHoldUp = () => {
			sensorTurboSpeed0 = 0;
		};
		// Level Up.
		sensorImages [1].onSensorClick = ()=>OnPlusMinusButtonClick(true);
		// Level Down.
		sensorImages [2].onSensorClick = ()=>OnPlusMinusButtonClick(false);
		// Scroll Down.
		sensorImages [3].onSensorHoldDownRepeat = () => {
			Utility.SoundEffectManager.Play ("se_select");
			float speed;
			if (sensorTurboSpeed3 == 0) {
				speed = sensorTurboFirstSpeed;
			}
			else {
				speed = sensorTurboSpeed3;
			}
			sensorTurboSpeed3 += sensorTurboAddSpeed;
			if (speed > sensorTurboMaxSpeed) speed = sensorTurboMaxSpeed;
			var rect = listCtrl.transform.RectCast ();
			rect.anchoredPosition = new Vector2 (0, rect.anchoredPosition.y + speed);
		};
		sensorImages [3].onSensorHoldUp = () => {
			sensorTurboSpeed3 = 0;
		};
		sensorImages [4].onSensorClick = () => {
			if (state == ImportTrackListInformation.ManagerState.SELECT_TRACK) {
				if (!string.IsNullOrEmpty(selectedTrackId)) {
					OnDoubleTappedCell (trackContentsSource [selectedTrackId]);
				}
			}
			else if (state == ImportTrackListInformation.ManagerState.SELECT_SCORE) {
				if (!string.IsNullOrEmpty(selectedScoreId)) {
					OnDoubleTappedCell (trackContentsSource [selectedScoreId]);
				}
			}
			else {
				if (selectedMenuId != ImportTrackListInformation.ImportMenu.NONE) {
					OnDoubleTappedCell (contentsSource [selectedMenuId]);
				}
			}
		};
		sensorImages [5].onSensorClick = () => {
			Utility.SoundEffectManager.Play ("se_decide");
			switch (state) {
			case ImportTrackListInformation.ManagerState.EDIT_SCORE:
				ChangeState (ImportTrackListInformation.ManagerState.SELECT_SCORE);
				listCtrl.transform.RectCast().anchoredPosition = new Vector2(0, scoreIdListScrollValue);
				forceUpdateContents = true;
				break;
			case ImportTrackListInformation.ManagerState.SELECT_SCORE:
				ChangeState (ImportTrackListInformation.ManagerState.EDIT_TRACK);
				listCtrl.transform.RectCast().anchoredPosition = new Vector2(0, 0);
				selectedScoreId = string.Empty;
				scoreIdListScrollValue = 0;
				forceUpdateContents = true;
				break;
			case ImportTrackListInformation.ManagerState.EDIT_TRACK:
				ChangeState (ImportTrackListInformation.ManagerState.SELECT_TRACK);
				listCtrl.transform.RectCast().anchoredPosition = new Vector2(0, trackIdListScrollValue);
				forceUpdateContents = true;
				break;
			case ImportTrackListInformation.ManagerState.SELECT_TRACK:
				ChangeState (ImportTrackListInformation.ManagerState.IMPORT);
				listCtrl.transform.RectCast().anchoredPosition = new Vector2(0, 0);
				selectedTrackId = string.Empty;
				trackIdListScrollValue = 0;
				forceUpdateContents = true;
				break;
			case ImportTrackListInformation.ManagerState.IMPORT:
				fade.Activate (()=> {
					Application.LoadLevel ("SceneMainMenu");
				});
				break;
			}
			selectedMenuId = ImportTrackListInformation.ImportMenu.NONE;
		};
	}

	/// <summary>
	/// 検索ダイアログ表示
	/// </summary>
	private void OpenTrackIdRefineWordDialog () {
		detailUICtrl.Show ("Search from Track ID", refineWord, (result)=>{
			if (refineWord != result) {
				refineWord = result;
				if (string.IsNullOrEmpty(selectedTrackId) || !selectedTrackId.Contains(refineWord)) {
					selectedTrackId = string.Empty;
				}
				ChangeState (ImportTrackListInformation.ManagerState.SELECT_TRACK);
				listCtrl.transform.RectCast().anchoredPosition = new Vector2 (0, 0);
				forceUpdateContents = true;
			}
		});
	}

	/// <summary>
	/// ID検索ボタン表示切替
	/// </summary>
	private void RefineWordSensorActivate (bool isActive) {
		if (isActive) {
			// ID検索.
			sensorImages [6].Setup (SpriteTank.Get ("icon_search"));
			sensorImages [6].onSensorClick = OpenTrackIdRefineWordDialog;
		}
		else {
			sensorImages [6].Setup (SpriteTank.Get ("icon_dammy"));
			sensorImages [6].onSensorClick = null;
		}
	}

	// +-アイコンを表示するか否か.
	private void PlusMinusSensorIconActivate (bool isActive) {
		if (isActive) {
			sensorImages [1].Setup (SpriteTank.Get ("icon_level_plus"));
			sensorImages [2].Setup (SpriteTank.Get ("icon_level_minus"));
		}
		else {
			sensorImages [1].Setup (SpriteTank.Get ("icon_dammy"));
			sensorImages [2].Setup (SpriteTank.Get ("icon_dammy"));
		}
	}

	private void OpenPathSelectDialog (System.Action<string> callback) {
		var dialog = Instantiate (pathSelectDialogPrefab) as GameObject;
		dialog.transform.SetParent (pathSelectDialogParent);
		dialog.transform.localPosition = dialog.transform.localEulerAngles = Vector3.zero;
		dialog.transform.localScale = Vector3.one;
		var ctrl = dialog.GetComponent<PathSelectDialogListControllerHolder> ().controller;
		string lastImportedPathDirectory = UserDataController.instance.user.last_track_imported_path;
		ctrl.Setup (lastImportedPathDirectory, (path)=>{
			UserDataController.instance.user.last_track_imported_path = path;
			UserDataController.instance.SaveUserInfo ();
			callback (path);
		});
	}

	private void OpenYesNoDialog (string caption, System.Action<bool> callback) {
		var dialog = Instantiate (yesNoDialogPrefab) as GameObject;
		dialog.SetParentEx (yesNoDialogParent);
		var controller = dialog.GetComponent<YesNoDialogController>();
		controller.Setup (caption, callback);
	}
	
	private void OpenOkDialog (string caption, System.Action callback) {
		var dialog = Instantiate (okDialogPrefab) as GameObject;
		dialog.SetParentEx (yesNoDialogParent);
		var controller = dialog.GetComponent<OkDialogController>();
		controller.Setup (caption, callback);
	}

	public void OnTappedCell (ImportTrackListInformation info) {
		Utility.SoundEffectManager.Play ("se_decide");
	}
	
	public void OnDoubleTappedCell (ImportTrackListInformation info) {
		Utility.SoundEffectManager.Play ("se_decide");
		switch (info.menuId) {
		// IMPORT.
		case ImportTrackListInformation.ImportMenu.IMPORT_FILELIST:
		{
			// ImportTrackListControllerからソース引っ張ってくる.
			OpenPathSelectDialog (ImportFromFilelist);
			break;
		}
		case ImportTrackListInformation.ImportMenu.IMPORT_MAIDATA:
		{
			// ImportTrackListControllerからソース引っ張ってくる.
			OpenPathSelectDialog (ImportFromMaidata);
			break;
		}
		case ImportTrackListInformation.ImportMenu.IMPORT_OFFICIAL_WEB_SITE:
		{
			// ImportTrackListControllerからソース引っ張ってくる.
			ImportFromOfficialWebSite ();
			break;
		}
		case ImportTrackListInformation.ImportMenu.TRACK_LIST:
		{
			ChangeState (ImportTrackListInformation.ManagerState.SELECT_TRACK);
			listCtrl.transform.RectCast().anchoredPosition = new Vector2 (0, 0);
			forceUpdateContents = true;
			break;
		}
		// TRACK_LIST.
		case ImportTrackListInformation.ImportMenu.TRACK_ID:
		{
			selectedTrackId = info.trackOrScoreId;
			trackIdListScrollValue = listCtrl.transform.RectCast().anchoredPosition.y;
			ChangeState (ImportTrackListInformation.ManagerState.EDIT_TRACK);
			listCtrl.transform.RectCast().anchoredPosition = new Vector2 (0, 0);
			forceUpdateContents = true;
			break;
		}
		// EDIT_TRACK.
		case ImportTrackListInformation.ImportMenu.EDIT_TRACK_ID:
		{
			// 文字列編集ダイアログを開く.
			detailUICtrl.Show (info.key, selectedTrackId, (result)=>{
				System.Action up = ()=>{
					UserDataController.instance.RemoveCache (selectedTrackId);
					UserDataController.instance.ChangeTrackId (selectedTrackId, result);
					UserDataController.instance.SaveTracks ();
					UserDataController.instance.SaveRecords ();
					selectedTrackId = result;
					info.value = result;
					ChangeState (ImportTrackListInformation.ManagerState.EDIT_TRACK);
					forceUpdateContents = true;
				};
				if (!string.IsNullOrEmpty (result)) {
					if (UserDataController.instance.ExistsTrack (result)) {
						OpenYesNoDialog ("overwrite?", (yes)=>{
							if (yes) {
								up ();
							}
						});
					}
					else {
						up ();
					}
				}
			});
			break;
		}
		case ImportTrackListInformation.ImportMenu.EDIT_TITLE:
		{
			// 文字列編集ダイアログを開く.
			var trackInfo = UserDataController.instance.GetTrack(selectedTrackId);
			detailUICtrl.Show (info.key, trackInfo.title, (result)=>{
				trackInfo.title = result;
				UserDataController.instance.SaveTracks ();
				info.value = result.ToImportMenuValueString();
				info.valueColor = result.ToImportMenuValueColor();
				forceUpdateContents = true;
			});
			break;
		}
		case ImportTrackListInformation.ImportMenu.EDIT_TITLE_RUBY:
		{
			// 文字列編集ダイアログを開く.
			var trackInfo = UserDataController.instance.GetTrack(selectedTrackId);
			detailUICtrl.Show (info.key, trackInfo.title_ruby, (result)=>{
				trackInfo.title_ruby = result;
				UserDataController.instance.SaveTracks ();
				info.value = result.ToImportMenuValueString();
				info.valueColor = result.ToImportMenuValueColor();
				forceUpdateContents = true;
			});
			break;
		}
		case ImportTrackListInformation.ImportMenu.EDIT_ARTIST:
		{
			// 文字列編集ダイアログを開く.
			var trackInfo = UserDataController.instance.GetTrack(selectedTrackId);
			detailUICtrl.Show (info.key, trackInfo.artist, (result)=>{
				trackInfo.artist = result;
				UserDataController.instance.SaveTracks ();
				info.value = result.ToImportMenuValueString();
				info.valueColor = result.ToImportMenuValueColor();
				forceUpdateContents = true;
			});
			break;
		}
		case ImportTrackListInformation.ImportMenu.EDIT_ARTIST_RUBY:
		{
			// 文字列編集ダイアログを開く.
			var trackInfo = UserDataController.instance.GetTrack(selectedTrackId);
			detailUICtrl.Show (info.key, trackInfo.artist_ruby, (result)=>{
				trackInfo.artist_ruby = result;
				UserDataController.instance.SaveTracks ();
				info.value = result.ToImportMenuValueString();
				info.valueColor = result.ToImportMenuValueColor();
				forceUpdateContents = true;
			});
			break;
		}
		case ImportTrackListInformation.ImportMenu.EDIT_BPM:
		{
			// 文字列編集ダイアログを開く.
			var trackInfo = UserDataController.instance.GetTrack(selectedTrackId);
			detailUICtrl.Show (info.key, trackInfo.whole_bpm, (result)=>{
				trackInfo.whole_bpm = result;
				UserDataController.instance.SaveTracks ();
				info.value = result.ToImportMenuValueString();
				info.valueColor = result.ToImportMenuValueColor();
				forceUpdateContents = true;
			});
			break;
		}
		case ImportTrackListInformation.ImportMenu.EDIT_AUDIO:
		{
			// パス選択ダイアログを開く.
			OpenPathSelectDialog((path)=>{
				string dstPath = DataPath.GetAudioPath (selectedTrackId, Path.GetExtension (path));
				File.Copy (path, dstPath, true);
				info.audioClip = null;
				UserDataController.instance.RemoveAudioCache (selectedTrackId);
				UserDataController.instance.GetTrack (selectedTrackId).audio = Path.GetFileName (path);
				UserDataController.instance.SaveTracks ();
				#if !UNITY_EDITOR && UNITY_IOS
				iPhone.SetNoBackupFlag (dstAudioPath);
				#endif
				string trackId = selectedTrackId;
				StartCoroutine(info.LoadAudio (this, (data)=>{
					UserDataController.instance.CacheReplace (trackId, data);
					forceUpdateContents = true;
				}));
			});
			break;
		}
		case ImportTrackListInformation.ImportMenu.EDIT_JACKET:
		{
			// パス選択ダイアログを開く.
			OpenPathSelectDialog((path)=>{
				string dstPath = DataPath.GetJacketPath (selectedTrackId, Path.GetExtension (path));
				File.Copy (path, dstPath, true);
				info.sprite = null;
				UserDataController.instance.RemoveJacketCache (selectedTrackId);
				UserDataController.instance.GetTrack (selectedTrackId).jacket = Path.GetFileName (path);
				UserDataController.instance.SaveTracks ();
				#if !UNITY_EDITOR && UNITY_IOS
				iPhone.SetNoBackupFlag (dstAudioPath);
				#endif
				string trackId = selectedTrackId;
				StartCoroutine(info.LoadJacket (this, (data)=>{
					UserDataController.instance.CacheReplace (trackId, data);
					forceUpdateContents = true;
				}));
			});
			break;
		}
		case ImportTrackListInformation.ImportMenu.SCORE_LIST:
		{
			ChangeState (ImportTrackListInformation.ManagerState.SELECT_SCORE);
			listCtrl.transform.RectCast().anchoredPosition = new Vector2 (0, 0);
			forceUpdateContents = true;
			break;
		}
		// SCORE_LIST.
		case ImportTrackListInformation.ImportMenu.SCORE_ID:
		{
			scoreIdListScrollValue = listCtrl.transform.RectCast().anchoredPosition.y;
			selectedScoreId = info.trackOrScoreId;
			ChangeState (ImportTrackListInformation.ManagerState.EDIT_SCORE);
			listCtrl.transform.RectCast().anchoredPosition = new Vector2 (0, 0);
			forceUpdateContents = true;
			break;
		}
		// EDIT_SCORE.
		case ImportTrackListInformation.ImportMenu.EDIT_SCORE_ID:
		{
			// 文字列編集ダイアログを開く.
			detailUICtrl.Show (info.key, selectedScoreId, (result)=>{
				var trackInfo = UserDataController.instance.GetTrack (selectedTrackId);
				System.Action up = ()=>{
					if (trackInfo.scores.ContainsKey (result)) {
						trackInfo.scores[result] = trackInfo.scores[selectedScoreId];
						trackInfo.scores.Remove (selectedScoreId);
					}
					else {
						trackInfo.scores[result] = ScoreInformation.NewInitialize ();
					}
					UserDataController.instance.SaveTracks ();
					UserDataController.instance.SaveRecords ();
					selectedScoreId = result;
					info.value = result;
					ChangeState (ImportTrackListInformation.ManagerState.EDIT_SCORE);
					forceUpdateContents = true;
				};
				if (!string.IsNullOrEmpty (result)) {
					if (trackInfo.scores.ContainsKey (result)) {
						OpenYesNoDialog ("overwrite?", (yes)=>{
							if (yes) {
								up ();
							}
						});
					}
					else {
						up ();
					}
				}
			});
			break;
		}
		case ImportTrackListInformation.ImportMenu.EDIT_LEVEL:
		{
			// 文字列編集ダイアログを開く.
			var scoreInfo = UserDataController.instance.GetScore(selectedTrackId, selectedScoreId);
			detailUICtrl.Show (info.key, scoreInfo.level, (result)=>{
				scoreInfo.level = result;
				UserDataController.instance.SaveTracks ();
				info.value = result.ToImportMenuValueString();
				info.valueColor = result.ToImportMenuValueColor();
				forceUpdateContents = true;
			});
			break;
		}
		case ImportTrackListInformation.ImportMenu.EDIT_DESIGNER:
		{
			// 文字列編集ダイアログを開く.
			var scoreInfo = UserDataController.instance.GetScore(selectedTrackId, selectedScoreId);
			detailUICtrl.Show (info.key, scoreInfo.notes_design, (result)=>{
				scoreInfo.notes_design = result;
				UserDataController.instance.SaveTracks ();
				info.value = result.ToImportMenuValueString();
				info.valueColor = result.ToImportMenuValueColor();
				forceUpdateContents = true;
			});
			break;
		}
		case ImportTrackListInformation.ImportMenu.EDIT_SCORE:
		{
			var scoreInfo = UserDataController.instance.GetScore (selectedTrackId, selectedScoreId);
			scoreEditFieldCtrl.Show (scoreInfo.score, scoreInfo.script_type);
			break;
		}
		


		}

	}

	public void OnPlusMinusButtonClick (bool isPlus) {
		Utility.SoundEffectManager.Play ("se_decide");
		if (state == ImportTrackListInformation.ManagerState.SELECT_TRACK) {
			if (isPlus) {
				detailUICtrl.Show ("Please Input New Track ID", string.Empty, (result)=>{
					if (!string.IsNullOrEmpty (result)) {
						System.Action up = ()=>{
							UserDataController.instance.RemoveCache (result);
							UserDataController.instance.AddTrack (result, TrackInformation.NewInitialize());
							UserDataController.instance.SaveTracks ();
							UserDataController.instance.SaveRecords ();
							selectedTrackId = result;
							refineWord = string.Empty;
							ChangeState (ImportTrackListInformation.ManagerState.EDIT_TRACK);
							listCtrl.transform.RectCast().anchoredPosition = new Vector2 (0, 0);
							forceUpdateContents = true;
						};
						if (!string.IsNullOrEmpty (result)) {
							if (UserDataController.instance.ExistsTrack (result)) {
								OpenYesNoDialog ("overwrite?", (yes)=>{
									if (yes) {
										up ();
									}
								});
							}
							else {
								up ();
							}
						}
					}
				});
			}
			else {
				if (string.IsNullOrEmpty(selectedTrackId)) return;
				OpenYesNoDialog ("delete?", (yes)=>{
					if (yes) {
						UserDataController.instance.RemoveCache (selectedTrackId);
						UserDataController.instance.RemoveTrack (selectedTrackId);
						UserDataController.instance.SaveTracks ();
						UserDataController.instance.SaveRecords ();
						selectedTrackId = string.Empty;
						ChangeState (ImportTrackListInformation.ManagerState.SELECT_TRACK);
						forceUpdateContents = true;
					}
				});
			}
		}
		else if (state == ImportTrackListInformation.ManagerState.SELECT_SCORE) {
			if (isPlus) {
				detailUICtrl.Show ("Please Input New Score ID", string.Empty, (result)=>{
					var trackInfo = UserDataController.instance.GetTrack (selectedTrackId);
					System.Action up = ()=>{
						if (trackInfo.scores.ContainsKey (result)) {
							trackInfo.scores[result] = trackInfo.scores[selectedScoreId];
							trackInfo.scores.Remove (selectedScoreId);
						}
						else {
							trackInfo.scores[result] = ScoreInformation.NewInitialize();
							trackInfo.scores[result].script_type = "simai";
						}
						UserDataController.instance.SaveTracks ();
						UserDataController.instance.SaveRecords ();
						selectedScoreId = result;
						ChangeState (ImportTrackListInformation.ManagerState.EDIT_SCORE);
						listCtrl.transform.RectCast().anchoredPosition = new Vector2 (0, 0);
						forceUpdateContents = true;
					};
					if (!string.IsNullOrEmpty (result)) {
						if (trackInfo.scores.ContainsKey (result)) {
							OpenYesNoDialog ("overwrite?", (yes)=>{
								if (yes) {
									up ();
								}
							});
						}
						else {
							up ();
						}
					}
				});
			}
			else {
				if (string.IsNullOrEmpty(selectedTrackId) || string.IsNullOrEmpty(selectedScoreId)) return;
				OpenYesNoDialog ("delete?", (yes)=>{
					if (yes) {
						var trackInfo = UserDataController.instance.GetTrack (selectedTrackId);
						trackInfo.scores.Remove (selectedScoreId);
						UserDataController.instance.SaveTracks ();
						UserDataController.instance.SaveRecords ();
						selectedScoreId = string.Empty;
						ChangeState (ImportTrackListInformation.ManagerState.SELECT_SCORE);
						forceUpdateContents = true;
					}
				});
			}
		}
		else {
			if (selectedMenuId == ImportTrackListInformation.ImportMenu.NONE ||
			    selectedMenuId == ImportTrackListInformation.ImportMenu.SCORE_LIST ||
			    state == ImportTrackListInformation.ManagerState.IMPORT ||
			    !contentsSource.ContainsKey (selectedMenuId))
				return;
			var info = contentsSource [selectedMenuId];
			if (isPlus) {
				OnDoubleTappedCell (info);
			}
			else {
				switch (selectedMenuId) {
				case ImportTrackListInformation.ImportMenu.EDIT_TITLE:
				{
					UserDataController.instance.GetTrack(selectedTrackId).title = string.Empty;
					UserDataController.instance.SaveTracks ();
					info.value = string.Empty.ToImportMenuValueString();
					info.valueColor = string.Empty.ToImportMenuValueColor();
					forceUpdateContents = true;
					break;
				}
				case ImportTrackListInformation.ImportMenu.EDIT_TITLE_RUBY:
				{
					UserDataController.instance.GetTrack(selectedTrackId).title_ruby = string.Empty;
					UserDataController.instance.SaveTracks ();
					info.value = string.Empty.ToImportMenuValueString();
					info.valueColor = string.Empty.ToImportMenuValueColor();
					forceUpdateContents = true;
					break;
				}
				case ImportTrackListInformation.ImportMenu.EDIT_ARTIST:
				{
					UserDataController.instance.GetTrack(selectedTrackId).artist = string.Empty;
					UserDataController.instance.SaveTracks ();
					info.value = string.Empty.ToImportMenuValueString();
					info.valueColor = string.Empty.ToImportMenuValueColor();
					forceUpdateContents = true;
					break;
				}
				case ImportTrackListInformation.ImportMenu.EDIT_ARTIST_RUBY:
				{
					UserDataController.instance.GetTrack(selectedTrackId).artist_ruby = string.Empty;
					UserDataController.instance.SaveTracks ();
					info.value = string.Empty.ToImportMenuValueString();
					info.valueColor = string.Empty.ToImportMenuValueColor();
					forceUpdateContents = true;
					break;
				}
				case ImportTrackListInformation.ImportMenu.EDIT_BPM:
				{
					UserDataController.instance.GetTrack(selectedTrackId).whole_bpm = string.Empty;
					UserDataController.instance.SaveTracks ();
					info.value = string.Empty.ToImportMenuValueString();
					info.valueColor = string.Empty.ToImportMenuValueColor();
					forceUpdateContents = true;
					break;
				}
				case ImportTrackListInformation.ImportMenu.EDIT_AUDIO:
				{
					OpenYesNoDialog ("Delete Audio?", (yes)=>{
						if (yes) {
							var trackInfo = UserDataController.instance.GetTrack(selectedTrackId);
							string path = DataPath.GetAudioPath (selectedTrackId, Path.GetExtension (trackInfo.audio));
							File.Delete (path);
							info.audioClip = null;
							trackInfo.audio = string.Empty;
							UserDataController.instance.SaveTracks ();
							UserDataController.instance.CacheReplace (selectedTrackId, (AudioClip)null);
							StartCoroutine(info.LoadAudio (this, (data)=>{
								forceUpdateContents = true;
							}));
						}
					});
					break;
				}
				case ImportTrackListInformation.ImportMenu.EDIT_JACKET:
				{
					OpenYesNoDialog ("Delete Jacket?", (yes)=>{
						if (yes) {
							var trackInfo = UserDataController.instance.GetTrack(selectedTrackId);
							string path = DataPath.GetJacketPath (selectedTrackId, Path.GetExtension (trackInfo.jacket));
							File.Delete (path);
							info.sprite = null;
							trackInfo.jacket = string.Empty;
							UserDataController.instance.SaveTracks ();
							UserDataController.instance.CacheReplace (selectedTrackId, (Sprite)null);
							StartCoroutine(info.LoadJacket (this, (data)=>{
								forceUpdateContents = true;
							}));
						}
					});
					break;
				}
				case ImportTrackListInformation.ImportMenu.EDIT_LEVEL:
				{
					UserDataController.instance.GetScore(selectedTrackId, selectedScoreId).level = string.Empty;
					UserDataController.instance.SaveTracks ();
					info.value = string.Empty.ToImportMenuValueString();
					info.valueColor = string.Empty.ToImportMenuValueColor();
					forceUpdateContents = true;
					break;
				}
				case ImportTrackListInformation.ImportMenu.EDIT_DESIGNER:
				{
					UserDataController.instance.GetScore(selectedTrackId, selectedScoreId).notes_design = string.Empty;
					UserDataController.instance.SaveTracks ();
					info.value = string.Empty.ToImportMenuValueString();
					info.valueColor = string.Empty.ToImportMenuValueColor();
					forceUpdateContents = true;
					break;
				}

				}
			}
		}
	}

	public void ScoreScriptEditRef (string script) {
		var scoreInfo = UserDataController.instance.GetScore (selectedTrackId, selectedScoreId);
		UserDataController.instance.ChangeScore (selectedTrackId, selectedScoreId, scoreInfo.script_type, script);
		UserDataController.instance.SaveTracks ();
		UserDataController.instance.SaveRecords ();
	}

	private void ImportFromFilelist (string path) {
		try {
			StartCoroutine (ImportFromFielistCoroutine(path));
		}
		catch (System.Exception e) {
			Debug.LogException (e);
			processDialog.Hide ();
			OpenOkDialog ("failed...", null);
		}
	}
	
	private IEnumerator ImportFromFielistCoroutine (string path) {
		int count = 0;
		progressed = 0.0f;
		processingWork = string.Empty;
		successed = false;
		processDialog.Show ();
		yield return null;
		
		const string filename = "maidata.txt";
		string directory = Path.GetDirectoryName (path);
		string[] filelist = File.ReadAllLines (path);
		int max = filelist.Length;
		foreach (string dirname in filelist) {
			if (!string.IsNullOrEmpty(dirname)) {
				processingWork = dirname;
				
				string p = Path.Combine(Path.Combine (directory, dirname), filename).Replace("\\", "/");
				if (File.Exists (p)) {
					UserDataController.instance.RemoveCache (dirname);
					ImportFromMaidataRoutine (dirname, File.ReadAllText (p), p);
				}
				else {
					Debug.LogError ("File Not Found >> \"" + p + "\"");
				}
			}
			
			count++;
			progressed = (float)count / (float)max;
			yield return null;
		}
		
		UserDataController.instance.SaveTracks ();
		UserDataController.instance.SaveRecords ();
		processingWork = string.Empty;
		progressed = 1.0f;
		successed = true;
	}
	
	private void ImportFromMaidata (string path) {
		try {
			StartCoroutine (ImportFromMaidataCoroutine (path));
		}
		catch (System.Exception e) {
			Debug.LogException (e);
			processDialog.Hide ();
			OpenOkDialog ("failed...", null);
		}
	}
	
	private IEnumerator ImportFromMaidataCoroutine (string path) {
		string directory = Path.GetDirectoryName (path).Replace("\\", "/");
		string trackId = Path.GetFileName (directory).Replace("\\", "/");
		UserDataController.instance.RemoveCache (trackId);
		progressed = 0.0f;
		processingWork = trackId;
		successed = false;
		processDialog.Show ();
		yield return null;
		
		ImportFromMaidataRoutine (trackId, File.ReadAllText(path));
		UserDataController.instance.SaveTracks ();
		UserDataController.instance.SaveRecords ();
		progressed = 1.0f;
		processingWork = string.Empty;
		successed = true;
	}
	
	private void ImportFromMaidataRoutine (string trackId, string maidata, string path = null) {
		// TrackInfomation生成.
		TrackInformation oldTrackInfo = null;
		if (UserDataController.instance.ExistsTrack (trackId)) {
			// ダイアログを出したい.

			// 引き継ぎ項目のため一時記憶.
			oldTrackInfo = UserDataController.instance.GetTrack (trackId);
			// 既にあったら消す.
			UserDataController.instance.RemoveTrack (trackId);
		}

		CommonScriptFormatter.ReadResult result;
		var rawdata = SimaiRawReader.Read(maidata, out result);
		if (result == CommonScriptFormatter.ReadResult.SUCCESS) {
			var trackInfo = SimaiRawReader.ToTrackInformation(rawdata);
			// 一部データの引き継ぎ (simaiのマクロになければ引き継ぐ).
			if (oldTrackInfo != null) {
				if (!rawdata.allMacros.Contains("title")) {
					trackInfo.title = oldTrackInfo.title;
				}
				if (!rawdata.allMacros.Contains("titleruby")) {
					trackInfo.title_ruby = oldTrackInfo.title_ruby;
				}
				if (!rawdata.allMacros.Contains("freemsg") && !rawdata.allMacros.Contains("artist")) {
					trackInfo.artist = oldTrackInfo.artist;
				}
				if (!rawdata.allMacros.Contains("artistruby")) {
					trackInfo.artist_ruby = oldTrackInfo.artist_ruby;
				}
				if (!rawdata.allMacros.Contains("wholebpm")) {
					trackInfo.whole_bpm = oldTrackInfo.whole_bpm;
				}
				if (!rawdata.allMacros.Contains("bg")) {
					trackInfo.jacket = oldTrackInfo.jacket;
				}
				if (!rawdata.allMacros.Contains("track")) {
					trackInfo.audio = oldTrackInfo.audio;
				}
				if (!rawdata.allMacros.Contains("seek")) {
					trackInfo.seek = oldTrackInfo.seek;
				}
				if (!rawdata.allMacros.Contains("wait")) {
					trackInfo.wait = oldTrackInfo.wait;
				}
				if (!rawdata.allMacros.Contains("first")) {
					trackInfo.simai_first = oldTrackInfo.simai_first;
				}
				trackInfo.imported_at = oldTrackInfo.imported_at;
			}
			if (path != null) {
				ImportResources (trackInfo, trackId, path);
			}
			// スコアデータを追加.
			UserDataController.instance.AddTrack (trackId, trackInfo);
		}
		else {
			// エラーを出したい.
		}
	}

	private void ImportResources (TrackInformation trackInfo, string trackId, string path) {
		// コピーするデータを探す.
		string directory = Path.GetDirectoryName (path).Replace("\\", "/");
		string srcFilePath = string.Empty;
		System.Func<string, bool> find = (name)=>{
			if (!string.IsNullOrEmpty(name)) {
				srcFilePath = Path.Combine (directory, name).Replace("\\", "/");
				return File.Exists (srcFilePath);
			}
			return false;
		};
		// 音楽データをコピー.
		#if UNITY_STANDALONE
		if (find(trackInfo.audio) || find("track.ogg") || find("track.wav") || find("track.mp3")) {
		#elif UNITY_ANDROID || UNITY_IOS
		if (find(trackInfo.audio) || find("track.mp3") || find("track.wav") || find("track.ogg")) {
		#else
		if (find(trackInfo.audio) || find("track.wav")) {
		#endif
			string dstAudioPath = DataPath.GetAudioPath (trackId, Path.GetExtension (srcFilePath));
			File.Copy (srcFilePath, dstAudioPath, true);
			trackInfo.audio = Path.GetFileName (srcFilePath);
			#if !UNITY_EDITOR && UNITY_IOS
			iPhone.SetNoBackupFlag (dstAudioPath);
			#endif
		}
		// 画像データをコピー.
		if (find(trackInfo.jacket) || find("bg.jpg") || find("bg.jpeg") || find("bg.png") || find("bg.gif") || find("bg.bmp")) {
			string dstJacketPath = DataPath.GetJacketPath (trackId, Path.GetExtension (srcFilePath));
			File.Copy (srcFilePath, dstJacketPath, true);
			trackInfo.jacket = Path.GetFileName (srcFilePath);
			#if !UNITY_EDITOR && UNITY_IOS
			iPhone.SetNoBackupFlag (dstJacketPath);
			#endif
		}
	}
			
	private void ImportFromMaiScript (string path) {
		
	}	
	
	private void ImportFromOfficialWebSite () {
		try {
			StartCoroutine (ImportFromOfficialSiteCoroutine());
		}
		catch (System.Exception e) {
			Debug.LogException (e);
			processDialog.Hide ();
			OpenOkDialog ("failed...", null);
		}
	}
	
	private IEnumerator ImportFromOfficialSiteCoroutine () {
		int count = 0;
		progressed = 0.0f;
		processingWork = string.Empty;
		successed = false;
		processDialog.Show ();
		yield return null;
		
		bool error = false;
		string url = DataPath.GetOfficialSiteScorePackageURL ();
		using (WWW www = new WWW(url)) {
			while (!www.isDone) {
				yield return www;
			}
			if (string.IsNullOrEmpty(www.error)) {
				string json = www.text;
				var data = JsonConvert.DeserializeObject<DownloadOfficialSiteScoresInformation>(json);
				int max = data.list.Length;
				foreach (var info in data.list) {
					processingWork = info.track_id;
					UserDataController.instance.RemoveCache (info.track_id);
					ImportFromMaidataRoutine(info.track_id, info.maidata);
					
					count++;
					progressed = (float)count / (float)max;
					yield return null;
				}
			}
			else {
				error = true;
				Debug.LogError (www.error);
				processDialog.Hide ();
				OpenOkDialog ("failed...", null);
			}
		}
		
		if (!error) {
			UserDataController.instance.SaveTracks ();
			UserDataController.instance.SaveRecords ();
			processingWork = string.Empty;
			progressed = 1.0f;
			successed = true;
		}
	}

}
