﻿using UnityEngine;
using System.Collections;

public class NetworkController : SingletonMonoBehaviour<NetworkController> {
	public readonly string ip = "127.0.0.1";
	public readonly int port = 1192;
	public bool connected { get; private set; }
	public bool isServer { get; private set; }
	public bool isClient { get; private set; }

	// Use this for initialization
	void Start () {
		connected = false;
		isServer = false;
		isClient = false;
	}


}
