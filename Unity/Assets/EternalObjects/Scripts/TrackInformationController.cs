using UnityEngine;
using System.Collections;

public class TrackInformationController : SingletonMonoBehaviour<TrackInformationController> {
	public string title;
	public string artist;
	public string whole_bpm;
	public long seek;
	public long wait;
	public float? simaiFirst;
	public Sprite jacket;
	public AudioClip audioClip;
	public string level;
	public string notes_design;
	public string trackId;
	public string difficulty;
	public RhythmGameLibrary.Score.Order[] score;
	public ResultInformation saveData;

	private bool seekChanged { get; set; }
	private bool waitChanged { get; set; }

	/// <summary>
	/// Setup関数を呼んだことがあるか. リズムゲームシーンを直接呼んだ時のデバッグ用.
	/// </summary>
	public bool calledSetup { get; set; }

	private IEnumerator Setup (string trackId, string difficulty, bool isEditor, string scriptType, string scoreScript) {
		seekChanged = false;
		waitChanged = false;

		this.trackId = trackId;
		TrackInformation track = UserDataController.instance.GetTrack(trackId);
		ResultInformation result = UserDataController.instance.GetResult(trackId, difficulty);
		
		title = track.title;
		artist = track.artist;
		whole_bpm = track.whole_bpm;
		seek = (track.seek * 1000.0f).ToLong ();
		wait = (track.wait * 1000.0f).ToLong ();
		simaiFirst = track.simai_first;
		this.difficulty = difficulty;
		string bgName = track.jacket;
		string musicName = track.audio;
		if (string.IsNullOrEmpty(bgName))
			bgName = "bg.jpg";
		if (string.IsNullOrEmpty(musicName))
			musicName = "track.mp3";
		saveData = result;

		var scoreInfo = track.scores [difficulty];
		level = scoreInfo.level;
		notes_design = scoreInfo.notes_design;
		
		// Score.
		if (!isEditor) {
			scriptType = scoreInfo.script_type;
			scoreScript = scoreInfo.score;
		}
		score = UserDataController.instance.LoadNotes (scriptType, scoreScript);
		// Jacket.
		yield return StartCoroutine (UserDataController.instance.LoadJacket (trackId, System.IO.Path.GetExtension (bgName), (data)=>jacket = data));
		// Audio.
		yield return StartCoroutine (UserDataController.instance.LoadAudio (trackId, System.IO.Path.GetExtension (musicName), (data)=>audioClip = data));

		calledSetup = true;
	}

	
	public IEnumerator Setup (string trackId, string difficulty) {
		yield return StartCoroutine (Setup (trackId, difficulty, false, null, null));
	}

	public IEnumerator SetupForEditor (string trackId, string difficulty, string scriptType, string scoreScript) {
		yield return StartCoroutine (Setup (trackId, difficulty, true, scriptType, scoreScript));
	}


	
	// シンクモードクライアント用セットアップ.
	public IEnumerator Setup (TrackInformation trackInfo, string scoreId, string jacketExtension, string audioExtension) {
		seekChanged = false;
		waitChanged = false;
		
		this.trackId = trackId;
		TrackInformation track = trackInfo;
		ResultInformation result = null;
		
		title = track.title;
		artist = track.artist;
		whole_bpm = track.whole_bpm;
		seek = (track.seek * 1000.0f).ToLong ();
		wait = (track.wait * 1000.0f).ToLong ();
		simaiFirst = track.simai_first;
		this.difficulty = scoreId;
		saveData = result;
		
		var scoreInfo = track.scores [scoreId];
		level = scoreInfo.level;
		notes_design = scoreInfo.notes_design;

		score = UserDataController.instance.LoadNotes (scoreInfo.script_type, scoreInfo.score);


		
		string path;
		// Jacket.
		path = DataPath.GetSyncTempDirectoryPath() + "bg" + jacketExtension;
		if (System.IO.File.Exists (path)) {
			using (WWW www = new WWW (DataPath.LOCAL_PATH_PREFIX + path)) {
				while (!www.isDone) {
					yield return www;
				}
				jacket = www.DownloadSprite();
			}
		}
		else {
			jacket = null;
		}
		// Audio.
		path = DataPath.GetSyncTempDirectoryPath() + "track" + audioExtension;
		if (System.IO.File.Exists (path)) {
			using (WWW www = new WWW (DataPath.LOCAL_PATH_PREFIX + path)) {
				while (!www.isDone) {
					yield return www;
				}
				audioClip = www.DownloadAudio();
			}
		}
		else {
			audioClip = null;
		}
		
		calledSetup = true;
	}
	
	// チュートリアル用セットアップ.
	public IEnumerator SetupTutorial () {
		seekChanged = false;
		waitChanged = false;
		
		trackId = "tutorial";
		difficulty = "EASY";
		TrackInformation track = Newtonsoft.Json.JsonConvert.DeserializeObject<TrackInformation>(Resources.Load<TextAsset> ("tutorial/track_info").text);
		
		title = track.title;
		artist = track.artist;
		whole_bpm = track.whole_bpm;
		seek = (track.seek * 1000.0f).ToLong ();
		wait = (track.wait * 1000.0f).ToLong ();
		simaiFirst = track.simai_first;
		saveData = null;
		
		var scoreInfo = track.scores [difficulty];
		level = scoreInfo.level;
		notes_design = scoreInfo.notes_design;
		
		// Score.
		score = UserDataController.instance.LoadNotes (scoreInfo.script_type, scoreInfo.score);
		// Jacket.
		jacket = SpriteTank.Get ("maisq_jacket_tutorial");
		// Audio.
		audioClip = Utility.SoundEffectManager.GetClip ("maisq_audio_tutorial");
		
		calledSetup = true;

		yield break;
	}









	public void SetSeekWait (string seekText, string waitText) {
		float value = 0;
		if (string.IsNullOrEmpty (seekText) || float.TryParse (seekText, out value)) {
			long lSeek = (value * 1000.0f).ToLong();
			if (seek != lSeek) {
				seek = lSeek;
				seekChanged = true;
			}
		}
		value = 0;
		if (string.IsNullOrEmpty (waitText) || float.TryParse (waitText, out value)) {
			long lWait = (value * 1000.0f).ToLong();
			if (wait != lWait) {
				wait = lWait;
				waitChanged = true;
			}
		}
	}

	public void SavingSeekWait () {
		if (calledSetup && (seekChanged || waitChanged)) {
			var track = UserDataController.instance.GetTrack (trackId);
			if (track != null) {
				if (seekChanged) {
					track.seek = (float)seek / 1000.0f;
				}
				if (waitChanged) {
					track.wait = (float)wait / 1000.0f;
				}
				track.simai_first = null;
				UserDataController.instance.SaveTracks ();
				seekChanged = false;
				waitChanged = false;
			}
		}
	}

	/// <summary>
	/// simaiのfirst値が入っていたらseekとwaitを自動設定.
	/// </summary>
	public void SetSimaiFirstData (float firstBpm, long startWait) {
		if (simaiFirst.HasValue) {
			seek = (simaiFirst.Value * 1000.0f).ToLong ();
			wait = (60.0f / firstBpm * 4 * 1000.0f).ToLong () + startWait;
		}
	}

}