using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

public class EditorInformationController : SingletonMonoBehaviour<EditorInformationController> {
	public string trackId;
	public string scoreId;
	public Sprite jacket;
	public List<ScoreEditorTimeListInformation> score;
	public int selectionStartIndex;
	public int selectionEndIndex;
	public float timelineScrolledAmount;
	public bool multiSelectableMode { get { return selectionStartIndex != selectionEndIndex; } }
	public string scriptType;
	public string scoreScript;
	public int scoreEndMarkIndex;
	
	/// <summary>
	/// Setup関数を呼んだことがあるか. スコアエディタシーンを直接呼んだ時のデバッグ用.
	/// </summary>
	public bool calledSetup { get; set; }

	public IEnumerator Setup (string trackId, string scoreId, string scriptType, string scoreScript) {
		this.trackId = trackId;
		this.scoreId = scoreId;
		this.scriptType = scriptType;
		this.scoreScript = scoreScript;
		var trackInfo = UserDataController.instance.GetTrack (trackId);
		string bgName = trackInfo.jacket;
		if (string.IsNullOrEmpty(bgName)) bgName = "bg.jpg";

		var reader = new ScoreEditorScriptReader ();
		if (scriptType == "simai") {
			string simai = scoreScript;
			SimaiNoteReader.Result result;
			string maiScript = SimaiNoteReader.ToMsqScript (simai, out result);
			reader.ReadMaiScript (maiScript);
			score = new List<ScoreEditorTimeListInformation> (reader.GetEditorScore ());
		}
		else if (scriptType == "maisq") {
			string maiScript = scoreScript;
			reader.ReadMaiScript (maiScript);
			score = new List<ScoreEditorTimeListInformation> (reader.GetEditorScore ());
		}
		else if (scriptType == "json") {
			// 未対応.
		}
		else if (string.IsNullOrEmpty (scriptType)) {
			score = new List<ScoreEditorTimeListInformation> ();
			var firstContent = new ScoreEditorTimeListInformation ();
			firstContent.bpm = 100;
			firstContent.step = RhythmGameLibrary.Score.ScoreStepData.Create(4, false);
			score.Add (firstContent);
		}
		if (score == null) {
			scoreEndMarkIndex = -1;
		}
		else {
			scoreEndMarkIndex = score.Count - 1;
		}

		// Jacket.
		yield return StartCoroutine (UserDataController.instance.LoadJacket (trackId, System.IO.Path.GetExtension (bgName), (data)=>jacket = data));
		calledSetup = true;
	}

	public void Save () {
		var data = ScoreEditorAutoSaveInformation.NewInitialize ();
		data.track_id = trackId;
		data.score_id = scoreId;
		data.script_type = scriptType;
		data.score = scoreScript;
		data.selection_start_index = selectionStartIndex;
		data.selection_end_index = selectionEndIndex;
		data.timeline_scrolled_amount = timelineScrolledAmount;
		data.score_end_mark_index = scoreEndMarkIndex;
		try {
			string json = JsonConvert.SerializeObject (data);
			var enc = new EncryptUtil ();
			string enctypt = enc.EncryptString(json);
			File.WriteAllText (DataPath.GetScoreEditorAutoSavePath (), enctypt, System.Text.Encoding.UTF8);
		}
		catch (System.Exception e) {
			Debug.LogException (e);
		}
	}

	public IEnumerator Load () {
		ScoreEditorAutoSaveInformation data = null;
		if (File.Exists (DataPath.GetScoreEditorAutoSavePath ())) {
			try {
				string encrypt = File.ReadAllText (DataPath.GetScoreEditorAutoSavePath ());
				var enc = new EncryptUtil ();
				string json;
				if (enc.DecryptString(encrypt, out json)) {
					data = JsonConvert.DeserializeObject<ScoreEditorAutoSaveInformation>(json);
					data.UpdateNewVersion ();
				}
			}
			catch (System.Exception e) {
				Debug.LogException (e);
			}
		}
		if (data != null) {
			selectionStartIndex = data.selection_start_index;
			selectionEndIndex = data.selection_end_index;
			timelineScrolledAmount = data.timeline_scrolled_amount;
			scoreEndMarkIndex = data.score_end_mark_index;
			yield return StartCoroutine (Setup (data.track_id, data.score_id, data.script_type, data.score));
		}
	}
}
