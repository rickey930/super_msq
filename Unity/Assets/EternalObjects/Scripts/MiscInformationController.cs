﻿using System.Collections.Generic;

public class MiscInformationController : SingletonMonoBehaviour<MiscInformationController> {
	#region その他なんでも君.
	public bool repeatOptionChanged { get; set; }
	public float adjustDelayBaseBpm { get; set; }
	public string rhythmGameRequesterSceneName { get; set; }
	public string importTrackRequesterSceneName { get; set; }
	public float mainMenuListScrollAmount { get; set; }
	public int syncMode { get; set; } // 0はシングルプレー、1はホスト、2はクライアント.
	public bool syncClientAfterResult { get; set; } // ホストが選曲状態としてクライアントがメインメニューに戻ってきた.
	public string lastLoadedBgmKey { get; set; } //最後に鳴らしたbgmのキー.
	public bool selectMusicVoiceCallRequest { get; set; } // select trackシーンで「please select music」のボイスを鳴らしたいならシーン遷移前にtrueにする.
	public string optionSceneCallbackSceneName { get; set; } // configやprofileシーンの決定やキャンセルボタンを押したときに戻るシーン名.
	#endregion
}
