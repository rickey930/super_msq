using UnityEngine;
using System.Collections;

public class MaipadDynamicCreatedSpriteTank : MonoBehaviour {
	public static MaipadDynamicCreatedSpriteTank instance;

	void Awake () {
		if (instance == null) {
			instance = this;
			StartCoroutine(WaitAfterCreate());
		}
		else {
			Destroy(this);
		}
	}

	IEnumerator WaitAfterCreate () {
		while (UserDataController.instance == null || UserDataController.instance.config == null) {
			yield return null;
		}
		// プログラミックお絵かき. // スプライトの動的生成ではなぜかCanvasで大きさが変になるので必要な時に#if trueにするようにした.普段は#if falseで.
#if false
		Create ();
		OutputPNG ();
#endif
	}

	public Sprite guideCircle1;
	public Sprite guideCircle2;
	public Sprite guideCircle3;
	public Sprite guideCircle4;
	public Sprite guideCircle5;
	public Sprite guideCircle6;
	public Sprite guideCircle7;
	public Sprite guideCircleDef;
	public Sprite guideCircleMenu;
	public Sprite guideCircleInner;
	public Sprite guideCircleCenter;
	public Sprite guideCircleTitle;
	public Sprite tapArc;
	public Sprite breakArc;
	public Sprite starArc;
	public Sprite eachArc0;
	public Sprite eachArc1;
	public Sprite eachArc2;
	public Sprite eachArc3;
	public Sprite eachArc4;
	public Sprite breakEachArc1;
	public Sprite breakEachArc2;
	public Sprite breakEachArc3;
	public Sprite breakEachArc4;
	public Sprite buttonTapCircleEffect;
	public Sprite holdingPerfectCircleEffect;
	public Sprite holdingGreatCircleEffect;
	public Sprite holdingGoodCircleEffect;
	public Sprite sensorSizeSample;

	public float GetLoadEndRate () {
		int count = 0;
		int end = 0;
		count++;
		if (guideCircle1 != null) end++;
		count++;
		if (guideCircle2 != null) end++;
		count++;
		if (guideCircle3 != null) end++;
		count++;
		if (guideCircle4 != null) end++;
		count++;
		if (guideCircle5 != null) end++;
		count++;
		if (guideCircle6 != null) end++;
		count++;
		if (guideCircle7 != null) end++;
		count++;
		if (guideCircleDef != null) end++;
		count++;
		if (guideCircleMenu != null) end++;
		count++;
		if (guideCircleInner != null) end++;
		count++;
		if (guideCircleCenter != null) end++;
		count++;
		if (guideCircleTitle != null) end++;
		count++;
		if (tapArc != null) end++;
		count++;
		if (breakArc != null) end++;
		count++;
		if (starArc != null) end++;
		count++;
		if (eachArc0 != null) end++;
		count++;
		if (eachArc1 != null) end++;
		count++;
		if (eachArc2 != null) end++;
		count++;
		if (eachArc3 != null) end++;
		count++;
		if (eachArc4 != null) end++;
		count++;
		if (breakEachArc1 != null) end++;
		count++;
		if (breakEachArc2 != null) end++;
		count++;
		if (breakEachArc3 != null) end++;
		count++;
		if (breakEachArc4 != null) end++;
		count++;
		if (buttonTapCircleEffect != null) end++;
		count++;
		if (holdingPerfectCircleEffect != null) end++;
		count++;
		if (holdingGreatCircleEffect != null) end++;
		count++;
		if (holdingGoodCircleEffect != null) end++;
		count++;
		if (sensorSizeSample != null) end++;

		return (float)end / (float)count;
	}

	public void Create() {
		int size;
		string spriteName;

		
		spriteName = "Maimai Guide Circle ";
		size = ((Constants.instance.MAIMAI_OUTER_RADIUS + 5) * 2).ToInt();
		{
			int level = 0;
			SpriteCreator.DrawMethod drawGuideCircle = (Texture2D canvas) => {
				Color color;
				if (level <= -2) { //特殊処理.
					color = Color.cyan;
				}
				else {
					color = MaimaiStyleDesigner.GetLevelColor (level);
				}
				Vector2 center = canvas.GetCenter ();
				canvas.DrawCircle (center, Constants.instance.MAIMAI_OUTER_RADIUS, color, 2);
				if (level != -3) {
					for (int i = 0; i < 8; i++) {
						Vector2 pos = Constants.instance.GetOuterPieceAxis (center, i);
						canvas.FillCircle (pos, 4, color);
					}
				}
			};
			level = 0;
			guideCircle1 = SpriteCreator.Create (size, size, drawGuideCircle, spriteName + (level + 1).ToString ());
			level++;
			guideCircle2 = SpriteCreator.Create (size, size, drawGuideCircle, spriteName + (level + 1).ToString ());
			level++;
			guideCircle3 = SpriteCreator.Create (size, size, drawGuideCircle, spriteName + (level + 1).ToString ());
			level++;
			guideCircle4 = SpriteCreator.Create (size, size, drawGuideCircle, spriteName + (level + 1).ToString ());
			level++;
			guideCircle5 = SpriteCreator.Create (size, size, drawGuideCircle, spriteName + (level + 1).ToString ());
			level++;
			guideCircle6 = SpriteCreator.Create (size, size, drawGuideCircle, spriteName + (level + 1).ToString ());
			level++;
			guideCircle7 = SpriteCreator.Create (size, size, drawGuideCircle, spriteName + (level + 1).ToString ());
			level = -1;
			guideCircleDef = SpriteCreator.Create (size, size, drawGuideCircle, spriteName + "Default");
			level = -2;
			guideCircleMenu = SpriteCreator.Create (size, size, drawGuideCircle, spriteName + "Menu");
			level = -3;
			guideCircleTitle = SpriteCreator.Create (size, size, drawGuideCircle, spriteName + "Title");
		}

		size = ((Constants.instance.MAIMAI_INNER_RADIUS + 5) * 2).ToInt();
		{
			SpriteCreator.DrawMethod drawGuideCircle = (Texture2D canvas) => {
				Color color = MaimaiStyleDesigner.GetLevelColor (-1);
				Vector2 center = canvas.GetCenter ();
				canvas.DrawCircle (center, Constants.instance.MAIMAI_INNER_RADIUS, color, 2);
				for (int i = 0; i < 8; i++) {
					Vector2 pos = Constants.instance.GetInnerPieceAxis (center, i);
					canvas.FillCircle (pos, 4, color);
				}
			};
			guideCircleInner = SpriteCreator.Create (size, size, drawGuideCircle, spriteName + "Inner");
		}
		
		size = 8;
		{
			SpriteCreator.DrawMethod drawGuideCircle = (Texture2D canvas) => {
				Color color = MaimaiStyleDesigner.GetLevelColor (-1);
				Vector2 center = canvas.GetCenter ();
				Vector2 pos = center;
				canvas.FillCircle (pos, 4, color);
			};
			guideCircleCenter = SpriteCreator.Create (size, size, drawGuideCircle, spriteName + "Center");
		}

		
		spriteName = "Maimai Note Arc ";
		size = ((Constants.instance.MAIMAI_OUTER_RADIUS + 5) * 2).ToInt();
		{
			var type = MaimaiStyleDesigner.NoteType.TAP_CIRCLE;
			int each = 0;
			bool yellow = false;
			SpriteCreator.DrawMethod drawNoteArc = (Texture2D canvas) => {
				Color color = MaimaiStyleDesigner.GetNoteArcColor (type, yellow);
				Vector2 center = canvas.GetCenter ();
				if (each == 0) {
					canvas.DrawArc (center, Constants.instance.MAIMAI_OUTER_RADIUS, -45.0f-22.5f, 22.5f, color, 1);
					canvas.DrawArc (center, Constants.instance.MAIMAI_OUTER_RADIUS, -45.0f+0.0f, 90.0f, color, 2);
					canvas.DrawArc (center, Constants.instance.MAIMAI_OUTER_RADIUS, -45.0f+90.0f, 22.5f, color, 1);
				} else if (each == 4) {
					canvas.DrawCircle (center, Constants.instance.MAIMAI_OUTER_RADIUS, color, 3);
				} else if (each > 0) {
					//canvas.DrawArc (center, Constants.instance.MAIMAI_OUTER_RADIUS, each*45.0f+(each-1)*-45.0f-45.0f-22.5f-45.0f, 22.5f, color, 1);
					//canvas.DrawArc (center, Constants.instance.MAIMAI_OUTER_RADIUS, each*45.0f+(each-1)*-45.0f-45.0f-45.0f, 45.0f, color, 2);
					//canvas.DrawArc (center, Constants.instance.MAIMAI_OUTER_RADIUS, each*45.0f+(each-1)*-45.0f-45.0f+0.0f, 45.0f*each, color, 3);
					//canvas.DrawArc (center, Constants.instance.MAIMAI_OUTER_RADIUS, each*45.0f+(each-1)*-45.0f-45.0f+45.0f*each, 45.0f, color, 2);
					//canvas.DrawArc (center, Constants.instance.MAIMAI_OUTER_RADIUS, each*45.0f+(each-1)*-45.0f-45.0f+45.0f*each+45.0f, 22.5f, color, 1);

					canvas.DrawArc (center, Constants.instance.MAIMAI_OUTER_RADIUS, each*45.0f+(each-1)*-45.0f-45.0f+0.0f, 45.0f*each, color, 3);
				}
			};
			yellow = false;
			each = 0;
			type = MaimaiStyleDesigner.NoteType.TAP_CIRCLE;
			tapArc = SpriteCreator.Create (size, size, drawNoteArc, spriteName + "TAP");
			type = MaimaiStyleDesigner.NoteType.TAP_STAR;
			starArc = SpriteCreator.Create (size, size, drawNoteArc, spriteName + "STAR");
			type = MaimaiStyleDesigner.NoteType.BREAK_CIRCLE;
			breakArc = SpriteCreator.Create (size, size, drawNoteArc, spriteName + "BREAK");
			type = MaimaiStyleDesigner.NoteType.TAP_CIRCLE;
			yellow = true;
			each = 0;
			eachArc0 = SpriteCreator.Create (size, size, drawNoteArc, spriteName + "EACH0");
			each = 1;
			eachArc1 = SpriteCreator.Create (size, size, drawNoteArc, spriteName + "EACH1");
			each = 2;
			eachArc2 = SpriteCreator.Create (size, size, drawNoteArc, spriteName + "EACH2");
			each = 3;
			eachArc3 = SpriteCreator.Create (size, size, drawNoteArc, spriteName + "EACH3");
			each = 4;
			eachArc4 = SpriteCreator.Create (size, size, drawNoteArc, spriteName + "EACH4");
			type = MaimaiStyleDesigner.NoteType.BREAK_CIRCLE;
			each = 1;
			breakEachArc1 = SpriteCreator.Create (size, size, drawNoteArc, spriteName + "BREAKEACH1");
			each = 2;
			breakEachArc2 = SpriteCreator.Create (size, size, drawNoteArc, spriteName + "BREAKEACH2");
			each = 3;
			breakEachArc3 = SpriteCreator.Create (size, size, drawNoteArc, spriteName + "BREAKEACH3");
			each = 4;
			breakEachArc4 = SpriteCreator.Create (size, size, drawNoteArc, spriteName + "BREAKEACH4");
		}


		spriteName = "Maimai Holding Effect ";
		size = ((Constants.instance.SENSOR_RADIUS + 5) * 2).ToInt ();
		{
			Color[] colors = new Color[] {
				Color.white,
				MaimaiStyleDesigner.GetJudgementColor (0),
				MaimaiStyleDesigner.GetJudgementColor (1),
				MaimaiStyleDesigner.GetJudgementColor (2)
			};
			int index = 0;
			SpriteCreator.DrawMethod drawHoldingEffect = (Texture2D canvas) => {
				Color color = colors[index];
				Vector2 center = canvas.GetCenter ();
				canvas.DrawCircle(center, Constants.instance.SENSOR_RADIUS, color, 1);
			};
			index = 0;
			buttonTapCircleEffect = SpriteCreator.Create (size, size, drawHoldingEffect, spriteName + "BUTTON TAP");
			index = 1;
			holdingPerfectCircleEffect = SpriteCreator.Create (size, size, drawHoldingEffect, spriteName + "HOLD PERFECT");
			index = 2;
			holdingGreatCircleEffect = SpriteCreator.Create (size, size, drawHoldingEffect, spriteName + "HOLD GREAT");
			index = 3;
			holdingGoodCircleEffect = SpriteCreator.Create (size, size, drawHoldingEffect, spriteName + "HOLD GOOD");
		}

		spriteName = "Maimai Sensor Size Sample";
		size = 70 * 2;
		{
			SpriteCreator.DrawMethod drawSensorSizeSample = (Texture2D canvas) => {
				Vector2 center = canvas.GetCenter ();
				canvas.DrawCircle(center, size / 2, MaimaiStyleDesigner.ByteToPercentRGBA(143, 143, 243, 255), 2);
			};
			sensorSizeSample = SpriteCreator.Create (size, size, drawSensorSizeSample, spriteName);
		}


	}

	void OutputPNG () {
		Sprite[] sprites = new Sprite[] {
			guideCircle1,
			guideCircle2,
			guideCircle3,
			guideCircle4,
			guideCircle5,
			guideCircle6,
			guideCircle7,
			guideCircleDef,
			guideCircleMenu,
			guideCircleInner,
			guideCircleCenter,
			guideCircleTitle,
			tapArc,
			breakArc,
			starArc,
			eachArc0,
			eachArc1,
			eachArc2,
			eachArc3,
			eachArc4,
			breakEachArc1,
			breakEachArc2,
			breakEachArc3,
			breakEachArc4,
			buttonTapCircleEffect,
			holdingPerfectCircleEffect,
			holdingGreatCircleEffect,
			holdingGoodCircleEffect,
			sensorSizeSample,
		};
		string directory = Application.dataPath + "/Images/DinamicCreatedSprites/";
		if (!System.IO.Directory.Exists (directory)) {
			System.IO.Directory.CreateDirectory (directory);
		}
		foreach (var sp in sprites) {
			var data = sp.texture.EncodeToPNG ();
			System.IO.File.WriteAllBytes (directory + sp.name + ".png", data);
		}
	}


}
