using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;

public class UserDataController : SingletonMonoBehaviour<UserDataController> {
	[System.NonSerialized]
	public Dictionary<string, TrackInformation> tracks;
	[System.NonSerialized]
	public Dictionary<string, RecordInfomation> records;
	[System.NonSerialized]
	public UserInformation user;
	[System.NonSerialized]
	public ConfigInformation config;
	[System.NonSerialized]
	public Dictionary<string, string> library;
	[System.NonSerialized]
	public Dictionary<string, Sprite> jacketCache;
	[System.NonSerialized]
	public Dictionary<string, AudioClip> audioCache;
	[System.NonSerialized]
	public Dictionary<string, string> language;

	//暗号化解いてJSONにするとか.
	//JSONにして暗号化するとか.

	protected override void Awake () {
		base.Awake ();
		Setup ();
//		SaveTracks ();
//		SaveRecords ();
//		SaveUserInfo ();
//		SaveConfig ();
	}

	public void Setup () {
		jacketCache = new Dictionary<string, Sprite> ();
		audioCache = new Dictionary<string, AudioClip> ();
		LoadTracks ();
		LoadRecords ();
		LoadUserInfo ();
		LoadConfig ();
		LoadLibrary ();
		LoadLanguage ();
	}

	public void LoadTracks () {
		if (File.Exists (DataPath.GetTrackPath ())) {
			try {
				string encrypt = File.ReadAllText (DataPath.GetTrackPath ());
				var enc = new EncryptUtil ();
				string json;
				if (enc.DecryptString(encrypt, out json)) {
					tracks = JsonConvert.DeserializeObject<Dictionary<string, TrackInformation>>(json);
					foreach (var track in tracks.Values) {
						track.UpdateNewVersion ();
					}
				}
			}
			catch (System.Exception e) {
				Debug.LogException (e);
			}
		}
		if (tracks == null) {
			tracks = new Dictionary<string, TrackInformation> ();
		}
	}
	
	public void LoadRecords () {
		if (File.Exists (DataPath.GetRecordPath ())) {
			try {
				string encrypt = File.ReadAllText (DataPath.GetRecordPath ());
				var enc = new EncryptUtil ();
				string json;
				if (enc.DecryptString(encrypt, out json)) {
					records = JsonConvert.DeserializeObject<Dictionary<string, RecordInfomation>>(json);
					foreach (var record in records.Values) {
						record.UpdateNewVersion ();
					}
				}
			}
			catch (System.Exception e) {
				Debug.LogException (e);
			}
		}
		if (records == null) {
			records = new Dictionary<string, RecordInfomation> ();
		}
	}

	public void LoadUserInfo () {
		if (File.Exists (DataPath.GetUserPath ())) {
			try {
				string encrypt = File.ReadAllText (DataPath.GetUserPath ());
				var enc = new EncryptUtil ();
				string json;
				if (enc.DecryptString(encrypt, out json)) {
					user = JsonConvert.DeserializeObject<UserInformation>(json);
					user.UpdateNewVersion ();
				}
			}
			catch (System.Exception e) {
				Debug.LogException (e);
			}
		}
		if (user == null) {
			user = UserInformation.NewInitialize ();
		}
	}
	
	public void LoadConfig () {
		if (File.Exists (DataPath.GetConfigPath ())) {
			try {
				string encrypt = File.ReadAllText (DataPath.GetConfigPath ());
				var enc = new EncryptUtil ();
				string json;
				if (enc.DecryptString(encrypt, out json)) {
					config = JsonConvert.DeserializeObject<ConfigInformation>(json);
					config.UpdateNewVersion ();
				}
			}
			catch (System.Exception e) {
				Debug.LogException (e);
			}
		}
		if (config == null) {
			config = ConfigInformation.NewInitialize ();
			//config = ConfigInfomation.NewDebugCustomInitialize ();
		}
	}
	
	public void LoadLibrary () {
		if (File.Exists (DataPath.GetLibraryPath ())) {
			try {
				string json = File.ReadAllText (DataPath.GetLibraryPath ());
				library = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
			}
			catch (System.Exception e) {
				Debug.LogException (e);
			}
		}
		if (library == null) {
			library = new Dictionary<string, string>();
		}
	}
	
	public bool LoadLanguage () {
		/*
		string json = Resources.Load<TextAsset> ("language/japanese_default").text;
		language = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

		if (File.Exists (DataPath.GetLanguagePath ())) {
			try {
				string ujson = File.ReadAllText (DataPath.GetLanguagePath ());
				var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(ujson);
				foreach (var key in data.Keys) {
					language[key] = data[key];
				}
			}
			catch (System.Exception e) {
				Debug.LogException (e);
			}
			return true;
		}*/
		return false;
	}

	public void SaveTracks () {
		try {
			string json = JsonConvert.SerializeObject (tracks);
			var enc = new EncryptUtil ();
			string enctypt = enc.EncryptString(json);
			File.WriteAllText (DataPath.GetTrackPath (), enctypt, System.Text.Encoding.UTF8);
		}
		catch (System.Exception e) {
			Debug.LogException (e);
		}
	}

	public void SaveRecords () {
		try {
			string json = JsonConvert.SerializeObject (records);
			var enc = new EncryptUtil ();
			string enctypt = enc.EncryptString(json);
			File.WriteAllText (DataPath.GetRecordPath (), enctypt, System.Text.Encoding.UTF8);
		}
		catch (System.Exception e) {
			Debug.LogException (e);
		}
	}

	public void SaveUserInfo () {
		try {
			string json = JsonConvert.SerializeObject (user);
			var enc = new EncryptUtil ();
			string enctypt = enc.EncryptString(json);
			File.WriteAllText (DataPath.GetUserPath (), enctypt, System.Text.Encoding.UTF8);
		}
		catch (System.Exception e) {
			Debug.LogException (e);
		}
	}

	public void SaveConfig () {
		try {
			string json = JsonConvert.SerializeObject (config);
			var enc = new EncryptUtil ();
			string enctypt = enc.EncryptString(json);
			File.WriteAllText (DataPath.GetConfigPath (), enctypt, System.Text.Encoding.UTF8);
		}
		catch (System.Exception e) {
			Debug.LogException (e);
		}
	}
	
	public void SaveLibrary () {
		try {
			string json = JsonConvert.SerializeObject (library);
			File.WriteAllText (DataPath.GetLibraryPath (), json, System.Text.Encoding.UTF8);
		}
		catch (System.Exception e) {
			Debug.LogException (e);
		}
	}
	
	public void SaveLanguage () {
		try {
			string json = JsonConvert.SerializeObject (language);
			File.WriteAllText (DataPath.GetLibraryPath (), json, System.Text.Encoding.UTF8);
		}
		catch (System.Exception e) {
			Debug.LogException (e);
		}
	}
	
	public RhythmGameLibrary.Score.Order[] LoadNotes (string scriptType, string score) {
		if (scriptType == "json") {
			// jsonをscoreに直読み.
			try {
				return JsonConvert.DeserializeObject<RhythmGameLibrary.Score.Order[]>(score);
			}
			catch (System.Exception e) {
				Debug.LogException (e);
			}
		}
		else if (scriptType == "simai") {
			return SimaiNoteReader.Read(score);
		}
		else if (scriptType == "maisq") {
			return MsqScriptReader.Read(score);
		}
		else if (scriptType == "resource_simai") {
			return SimaiNoteReader.Read(Resources.Load <TextAsset> (score).text);
		}
		else if (scriptType == "resource_maisq") {
			return MsqScriptReader.Read(Resources.Load <TextAsset> (score).text);
		}
		return null;
	}

	public IEnumerator CreateResourceCache (bool isJacketCache, bool isAudioCache) {
		if (!isJacketCache && !isAudioCache)
			yield break;
		while (config == null || jacketCache == null || audioCache == null)
			yield return null;
		
		if (config.user_recources_cache_type.ToResourcesCacheType() == ConfigTypes.ResourcesCacheType.BULK) {
			foreach (var trackId in tracks.Keys) {
				if (isJacketCache && !jacketCache.ContainsKey (trackId)) {
					yield return StartCoroutine (LoadJacket (trackId, Path.GetExtension(tracks[trackId].jacket), (data)=>{}));
				}
				if (isAudioCache && !audioCache.ContainsKey (trackId)) {
					yield return StartCoroutine (LoadAudio (trackId, Path.GetExtension(tracks[trackId].audio), (data)=>{}));
				}
			}
		}
	}

	public IEnumerator LoadJacket (string trackId, string extension, System.Action<Sprite> callback) {
		var type = config.user_recources_cache_type.ToResourcesCacheType();
		bool useCache = type == ConfigTypes.ResourcesCacheType.USED || type == ConfigTypes.ResourcesCacheType.BULK;
		if (useCache && jacketCache != null && jacketCache.ContainsKey (trackId)) {
			callback (jacketCache[trackId]);
			yield break;
		}

		string path = string.Empty;
		System.Func<string, bool> find = (ext)=>{
			path = DataPath.GetJacketPath (trackId, ext);
			return File.Exists (path);
		};
		System.Action<Sprite> call = (data) => {
			if (useCache || (jacketCache != null && jacketCache.ContainsKey(trackId))) {
				jacketCache[trackId] = data;
			}
			callback (data);
		};
		if (find(extension) || find(".jpg") || find(".jpeg") || find(".png") || find(".gif") || find(".bmp")) {
			using (WWW www = new WWW (DataPath.LOCAL_PATH_PREFIX + path)) {
				while (!www.isDone) {
					yield return www;
				}
				call(www.DownloadSprite());
			}
		}
		else {
			call (null);
		}
	}
	
	public IEnumerator LoadAudio (string trackId, string extension, System.Action<AudioClip> callback) {
		var type = config.user_recources_cache_type.ToResourcesCacheType();
		bool useCache = type == ConfigTypes.ResourcesCacheType.USED || type == ConfigTypes.ResourcesCacheType.BULK;
		if (useCache && audioCache != null && audioCache.ContainsKey (trackId)) {
			callback (audioCache[trackId]);
			yield break;
		}

		string path = string.Empty;
		System.Func<string, bool> find = (ext)=>{
			path = DataPath.GetAudioPath (trackId, ext);
			return File.Exists (path);
		};
		System.Action<AudioClip> call = (data) => {
			if (useCache || (audioCache != null && audioCache.ContainsKey(trackId))) {
				audioCache[trackId] = data;
			}
			callback (data);
		};
#if UNITY_STANDALONE
		if (find(extension) || find(".ogg") || find(".wav") || find(".mp3")) {
#elif UNITY_ANDROID || UNITY_IOS
		if (find(extension) || find(".mp3") || find(".wav") || find(".ogg")) {
#else
		if (find(extension) || find(".wav")) {
#endif
			using (WWW www = new WWW (DataPath.LOCAL_PATH_PREFIX + path)) {
				while (!www.isDone) {
					yield return www;
				}
				call(www.DownloadAudio());
			}
		}
		else {
			call (null);
		}
	}
	
	public IEnumerator LoadSprite (string path, System.Action<Sprite> callback) {
		if (File.Exists (path)) {
			using (WWW www = new WWW (DataPath.LOCAL_PATH_PREFIX + path)) {
				while (!www.isDone) {
					yield return www;
				}
				callback(www.DownloadSprite());
			}
		}
		else {
			callback (null);
		}
	}
	
	public IEnumerator LoadAudio (string path, System.Action<AudioClip> callback) {
		if (File.Exists (path)) {
			using (WWW www = new WWW (DataPath.LOCAL_PATH_PREFIX + path)) {
				while (!www.isDone) {
					yield return www;
				}
				callback(www.DownloadAudio());
			}
		}
		else {
			callback (null);
		}
	}
	
	public IEnumerator LoadPlayerIcon (string extension, System.Action<Sprite> callback) {
		string path = DataPath.GetPlayerIconPath (extension);
		yield return StartCoroutine (LoadSprite(path, callback));
	}
	
	public IEnumerator LoadPlayerPlate (string extension, System.Action<Sprite> callback) {
		string path = DataPath.GetPlayerPlatePath (extension);
		yield return StartCoroutine (LoadSprite(path, callback));
	}
	
	public IEnumerator LoadPlayerFrame (string extension, System.Action<Sprite> callback) {
		string path = DataPath.GetPlayerFramePath (extension);
		yield return StartCoroutine (LoadSprite(path, callback));
	}

	public void CacheReplace (string trackId, Sprite jacket) {
		if (IsContainJacketCache (trackId)) {
			jacketCache[trackId] = jacket;
		}
	}
	
	public void CacheReplace (string trackId, AudioClip audio) {
		if (IsContainAudioCache (trackId)) {
			audioCache[trackId] = audio;
		}
	}

	public void RemoveCache (string trackId) {
		RemoveJacketCache (trackId);
		RemoveAudioCache (trackId);
	}

	public void RemoveJacketCache (string trackId) {
		jacketCache.Remove (trackId);
	}
	
	public void RemoveAudioCache (string trackId) {
		audioCache.Remove (trackId);
	}

	public bool IsContainJacketCache (string trackId) {
		return jacketCache != null && jacketCache.ContainsKey (trackId);
	}

	public bool IsContainAudioCache (string trackId) {
		return audioCache != null && audioCache.ContainsKey (trackId);
	}



	public TrackInformation GetTrack (string trackId) {
		if (tracks.ContainsKey(trackId)) {
			return tracks[trackId];
		}
		return null;
	}
	
	public RecordInfomation GetRecord (string trackId) {
		if (records.ContainsKey(trackId)) {
			return records[trackId];
		}
		return null;
	}
	
	public ScoreInformation GetScore (string trackId, string scoreId) {
		TrackInformation track = GetTrack(trackId);
		if (track != null) {
			if (track.scores.ContainsKey(scoreId)) {
				return track.scores[scoreId];
			}
		}
		return null;
	}
	
	public ResultInformation GetResult (string trackId, string scoreId) {
		RecordInfomation record = GetRecord(trackId);
		if (record != null) {
			if (record.results.ContainsKey(scoreId)) {
				return record.results[scoreId];
			}
		}
		return null;
	}

	public string GetLibrary (string libName) {
		if (library != null && library.ContainsKey(libName)) {
			return library[libName];
		}
		return null;
	}

	public int CalcFullScore (string scoreScriptType, string scoreScript) {
		// 譜面を取得する.
		var score = LoadNotes (scoreScriptType, scoreScript);
		// 譜面をノーツに変換する.
		if (score != null) {
			var allNotes = new MaimaiScoreConverter(score).ReadScore ();
			// 理論値を求める.
			int full_score = MaimaiJudgeEvaluateManager.CalcBFullScore (allNotes);
			return full_score;
		}
		return 0;
	}

	public void ChangeScore (string trackId, string scoreId, string scoreScriptType, string scoreScript) {
		ScoreInformation scoreInfo = GetScore (trackId, scoreId);
		scoreInfo.script_type = scoreScriptType;
		scoreInfo.score = scoreScript;
		scoreInfo.full_score = CalcFullScore (scoreScriptType, scoreScript);
		RecordInfomation record = GetRecord(trackId);
		if (record != null) {
			ResultInformation result = GetResult (trackId, scoreId);
			if (result != null) {
				// 比較する.
				if (scoreInfo.full_score != result.full_score) {
					record.results.Remove (scoreId);
				}
			}
		}
	}
	
	public void AddTrack (string trackId, TrackInformation track) {
		foreach (string scoreId in track.scores.Keys) {
			ScoreInformation scoreInfo = track.scores[scoreId];
			scoreInfo.full_score = CalcFullScore (scoreInfo.script_type, scoreInfo.score);
		}
		
		if (ExistsTrack(trackId)) {
			// 前の譜面データと新しい譜面データの総合点が異なる場合はレコードを消す.
			TrackInformation old = tracks[trackId];
			RecordInfomation record = GetRecord(trackId);
			if (record != null) {
				foreach (string scoreId in old.scores.Keys) {
					ResultInformation result = GetResult(trackId, scoreId);
					if (result != null) {
						ScoreInformation scoreInfo = old.scores[scoreId];
						// 比較する.
						if (scoreInfo.full_score != result.full_score) {
							record.results.Remove(scoreId);
						}
					}
				}
			}
		}
		tracks[trackId] = track;
	}
	// Tracksを保存する際にはRecordsの保存も忘れずに. 外部から呼び出すはず.
	
	public bool ExistsTrack (string trackId) {
		return tracks.ContainsKey(trackId);
	}
	
	public void RemoveTrack (string trackId) {
		// リソースファイルを削除する.
		string path;
		string[] extensions;
		extensions = new string[] {
			Path.GetExtension(tracks[trackId].jacket), ".jpg", ".jpeg", ".png", ".gif", ".bmp"
		};
		foreach (string ext in extensions) {
			path = DataPath.GetJacketPath (trackId, ext);
			File.Delete (path);
		}
		extensions = new string[] {
			Path.GetExtension(tracks[trackId].audio), ".mp3", ".ogg", ".wav"
		};
		foreach (string ext in extensions) {
			path = DataPath.GetAudioPath (trackId, ext);
			File.Delete (path);
		}
		// 取り除く.
		tracks.Remove(trackId);
		records.Remove(trackId);
	}
	
	public void ChangeTrackId (string oldTrackId, string newTrackId) {
		// リソースファイルをリネームする.
		string extension, oldPath, newPath;
		extension = Path.GetExtension(tracks[oldTrackId].jacket);
		oldPath = DataPath.GetJacketPath (oldTrackId, extension);
		if (File.Exists (oldPath)) {
			newPath = DataPath.GetJacketPath (newTrackId, extension);
			File.Delete (newPath);
			File.Move (oldPath, newPath);
		}
		
		extension = Path.GetExtension(tracks[oldTrackId].audio);
		oldPath = DataPath.GetAudioPath (oldTrackId, extension);
		if (File.Exists (oldPath)) {
			newPath = DataPath.GetAudioPath (newTrackId, extension);
			File.Delete (newPath);
			File.Move (oldPath, newPath);
		}
		
		// 変更する.
		tracks[newTrackId] = tracks[oldTrackId];
		if (records.ContainsKey(oldTrackId)) {
			records[newTrackId] = records[oldTrackId];
		}
		// 取り除く.
		RemoveTrack(oldTrackId);
	}
	
	public void TracksUserSort(string[] trackIds) {
		Dictionary<string, TrackInformation> sortedTracks = new Dictionary<string, TrackInformation> ();
		foreach (string trackId in trackIds) {
			if (tracks.ContainsKey(trackId)) {
				sortedTracks[trackId] = tracks[trackId];
			}
		}
		tracks = sortedTracks;
	}
	
	public void SetResult (string trackId, string scoreId, ResultInformation result) {
		if (!records.ContainsKey(trackId)) {
			records[trackId] = RecordInfomation.NewInitialize();
		}
		records[trackId].results[scoreId] = result;
	}

	public void SetUserExperience (float achievementRate, int notesCount, int missCount, bool ap, bool fc, float syncRate) {
		if (notesCount >= 10) { // 10ノート以上の譜面で経験値取得.
			int exp;
			// プレイ.
			exp = 1;
			// ランク.
			if (achievementRate >= 1.0f)
				exp += 4;
			else if (achievementRate >= 0.97f)
				exp += 3;
			else if (achievementRate >= 0.94f) 
				exp += 2;
			else if (achievementRate >= 0.8f)
				exp += 1;
			// コンボ.
			if (ap)
				exp += 5;
			else if (fc)
				exp += 4;
			else if (missCount == 0)
				exp += 3;
			else if (missCount == 1)
				exp += 2;
			else if (missCount == 2)
				exp += 1;
			float rate = 1.0f + syncRate;
			exp = (exp * rate).ToInt();
			user.user_level_info.InjectionExperience (exp);
		}
	}

	// Limit: breakは2500点での、各要素の現在のcount全体に対する最大レート点 (tap1グレなら500点)
	public void SetAvaterAbility (int tapCount, int tapScore, int tapLimitScore, int holdCount, int holdScore, int holdLimitScore, int slideCount, int slideScore, int slideLimitScore, int breakCount, int breakScore, int breakLimitScore) {
		//各要素点の中で、何％取れたか
		//全ノートの中で、各要素は何％か (ノート数)
		//それを掛け算する
		
		int allScore = tapScore + holdScore + slideScore + breakScore;
		int allCount = tapCount + holdCount + slideCount + breakCount;
		
		if (allScore == 0) return;
		System.Func<int, int, int, float> abilityCalc = (elementCount, elementScore, elementLimitScore)=>{
			float elementAchievement = elementLimitScore > 0 ? (float)elementScore / (float)elementLimitScore : 0.0f;
			float elementAmount = allCount > 0 ? (float)elementCount / (float)allCount : 0.0f;
			return elementAchievement * elementAmount;
		};
		user.avater_ability.AddElement (abilityCalc (tapCount, tapScore, tapLimitScore), abilityCalc (holdCount, holdScore, holdLimitScore), abilityCalc (slideCount, slideScore, slideLimitScore), abilityCalc (breakCount, breakScore, breakLimitScore));
	}
	
}
