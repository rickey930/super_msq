using UnityEngine;
using System.Collections;

public class ResultInformationController : SingletonMonoBehaviour<ResultInformationController> {
	public int score;
	public int achievement;
	public bool full_ap;
	public bool ap;
	public bool fc;
	public bool no_miss;
	public int sync;
	public int full_score;
	public int tap_score;
	public int hold_score;
	public int slide_score;
	public int break_score;
	public int trap_score;
	public int full_tap_score;
	public int full_hold_score;
	public int full_slide_score;
	public int full_break_score;
	public int full_trap_score;
	public int perfect_amount;
	public int great_amount;
	public int good_amount;
	public int miss_amount;
	public int max_combo;
	public bool auto_play;
	public ConfigTypes.ChangeBreak change_break_type;
	public bool isSyncMode;

	public void Setup (int score, float achievement, bool ap, bool fc, bool noMiss, int sync, int fullScore,
	                   int tapScore, int holdScore, int slideScore, int breakScore, int trapScore,
	                   int fullTapScore, int fullHoldScore, int fullSlideScore, int fullBreakScore, int fullTrapScore,
	                   int perfectAmount, int greatAmount, int goodAmount, int missAmount, int maxCombo,
	                   bool autoPlay, ConfigTypes.ChangeBreak changeBreakType, bool isSyncMode) {
		this.score = score;
		this.achievement = Mathf.Floor((achievement * 10000.0f)).ToInt ();
		this.ap = ap;
		this.fc = fc;
		this.no_miss = noMiss;
		this.sync = sync;
		this.full_score = fullScore;
		this.full_ap = score == fullScore;
		this.tap_score = tapScore;
		this.hold_score = holdScore;
		this.slide_score = slideScore;
		this.break_score = breakScore;
		this.trap_score = trapScore;
		this.full_tap_score = fullTapScore;
		this.full_hold_score = fullHoldScore;
		this.full_slide_score = fullSlideScore;
		this.full_break_score = fullBreakScore;
		this.full_trap_score = fullTrapScore;
		this.perfect_amount = perfectAmount;
		this.great_amount = greatAmount;
		this.good_amount = goodAmount;
		this.miss_amount = missAmount;
		this.max_combo = maxCombo;
		this.auto_play = autoPlay;
		this.change_break_type = changeBreakType;
		this.isSyncMode = isSyncMode;
	}

	/// <summary>
	/// 保存するのに都合のいいデータを取得.
	/// </summary>
	/// <param name="before">前のセーブデータ.</param>
	public ResultInformation GetSavingData (ResultInformation before) {
		if (before == null) 
			before = ResultInformation.NewInitialize ();
		ResultInformation after;
		bool score_save_canceller = change_break_type != ConfigTypes.ChangeBreak.NORMAL; // change_break;
		bool all_save_canceller = auto_play; // auto_play;
		if (!all_save_canceller) {
			after = ResultInformation.NewInitialize ();
			if (before.full_score == this.full_score) {
				after.score = score_save_canceller ? before.score : Mathf.Max(before.score, this.score);
				after.achievement = score_save_canceller ? before.achievement : Mathf.Max(before.achievement, this.achievement);
				after.full_ap = before.full_ap || this.full_ap;
				after.ap = before.ap || this.ap;
				after.fc = before.fc || this.fc;
				after.no_miss = before.no_miss || this.no_miss;
				after.sync = Mathf.Max(before.sync, this.sync);
				after.play_count = before.play_count + 1;
			}
			else {
				after.score = this.score;
				after.achievement = this.achievement;
				after.full_ap = this.full_ap;
				after.ap = this.ap;
				after.fc = this.fc;
				after.no_miss = this.no_miss;
				after.sync = this.sync;
				after.play_count = 1;
			}
			after.full_score = this.full_score;
			after.last_played_at = Datetime.Now();
		}
		else {
			after = before;
		}
		return after;
	}
}
