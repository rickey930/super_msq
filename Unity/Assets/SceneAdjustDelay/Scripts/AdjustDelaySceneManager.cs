﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Utility;

public class AdjustDelaySceneManager : MonoBehaviour {
	
	[SerializeField]
	private GameObject evaluateEffectPrefab;
	[SerializeField]
	private Text delayLabel;
	[SerializeField]
	private InputField bpmInputField;
	[SerializeField]
	private GameObject menuDialog;
	[SerializeField]
	private FadeController fader;
	[SerializeField]
	private GameObject[] afterShowDescriptionLabelObjs;
	[SerializeField]
	private MaimaiSensor touchSensor;
	[SerializeField]
	private SensorEffectMother sensorEffectMother;
	[SerializeField]
	private Image backgroundImage;
	
	private MaimaiTimer timer;
	private AdjustDelayEvaluate evaluater;
	private float delay;
	private float oldDelay;
	private bool onceAdjust; // 1度でも調整したか.

	private bool initialized;
	
	IEnumerator Start () {
		initialized = false;

		MiscInformationController.instance.lastLoadedBgmKey = string.Empty;
		AudioManagerLite.Pause ();

		if (MiscInformationController.instance.adjustDelayBaseBpm <= 0)
			MiscInformationController.instance.adjustDelayBaseBpm = 100.0f;
		bpmInputField.text = MiscInformationController.instance.adjustDelayBaseBpm.ToString ();
		oldDelay = delay = 0;
		timer = new MaimaiTimer (AudioManagerLite.GetManager ());
		evaluater = new AdjustDelayEvaluate (timer, MiscInformationController.instance.adjustDelayBaseBpm, evaluateEffectPrefab, touchSensor.transform);


		while (ProfileInformationController.instance == null || !ProfileInformationController.instance.loaded_player_frame) {
			yield return null;
		}
		backgroundImage.sprite = ProfileInformationController.instance.player_frame;

		timer.initialize_ModeEndless ();
		initialized = true;
	}

	void Update () {
		if (!initialized) return;

		timer.timeUpdate ();

		if (!timer.pause) {
			touchSensor.Check ();
			if (touchSensor.push) {
				sensorEffectMother.Birth ();
				long timing = evaluater.JudgeTiming ();
				delay = (float)timing / 1000.0f;
				if (!onceAdjust) {
					foreach (GameObject obj in afterShowDescriptionLabelObjs) {
						obj.SetActive (true);
					}
					onceAdjust = true;
				}
			}
		
			evaluater.AutoBeatProc ();

			if (oldDelay != delay) {
				delayLabel.text = delay.ToString ("F3") + "sec.";
			}
		}
	}
	
	public void OnBackButtonClick() {
		SoundEffectManager.Play("se_decide");
		BackScene ();
	}
	
	public void OnDecideButtonClick() {
		SoundEffectManager.Play("se_decide");
		UserDataController.instance.config.judge_offset = delay;
		BackScene ();
	}

	public void OnBpmResettingButtonClick () {
		SoundEffectManager.Play("se_decide");
		float value;
		if (float.TryParse (bpmInputField.text, out value)) {
			if (value > 0) {
				MiscInformationController.instance.adjustDelayBaseBpm = value;
			}
			else {
				MiscInformationController.instance.adjustDelayBaseBpm = 100.0f;
			}
		}
		Application.LoadLevel ("SceneAdjustDelay");
	}
	
	private void BackScene() {
		if (timer != null) {
			timer.pause = true;
		}
		fader.Activate(delegate() {
			Application.LoadLevel("SceneMainMenu");
		});
	}

	public void OnOpenMenuButtonClick () {
		SoundEffectManager.Play("se_decide");
		menuDialog.SetActive (true);
		if (timer != null) {
			timer.pause = true;
		}
	}
}
