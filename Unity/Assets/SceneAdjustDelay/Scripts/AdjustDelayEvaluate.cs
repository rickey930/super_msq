﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RhythmGameLibrary;
using Utility;

public class AdjustDelayEvaluate : RhythmGameEvaluateOnBeat<int, XMaimaiNote.XInspector.Inspector> {

	public AdjustDelayEvaluate (RhythmGameTimer timer, float fixedBpm, GameObject evaluateEffectObj, Transform evaluateEffectBasePosition)
	: base (timer, new Dictionary<int, XMaimaiNote.XInspector.Inspector[]>(), 0, 0, fixedBpm, 0) {
		this.oneBeatTime = (long)((60.0f / fixedBpm) * 1000.0f);
		this.evaluateEffectObj = evaluateEffectObj;
		this.evaluateEffectBasePosition = evaluateEffectBasePosition;
	}

	private long baseTime;
	private long oneBeatTime; // 1拍の時間.

	private GameObject evaluateEffectObj;
	private Transform evaluateEffectBasePosition;

	protected override void OnBeat () {
		SoundEffectManager.Play("se_startcount");
		baseTime = timer.gameTime;
	}

	public long JudgeTiming() {
		long time = timer.gameTime;
		long timing = time - baseTime;
		// 遅判定ならこれでいいけど、早判定のとき数値が大きすぎるので、1拍の時間を計算して半分以上なら次の拍のベースタイムを基準とする.
		// 100, 120 -> 120 - 100 = +20.
		// 100, 50, 140 -> +40, 50 / 2 = 25, 25 < 40 -> 140 - 150 = -10.
		long half = oneBeatTime / 2;
		if (timing > half) {
			timing = time - (baseTime + oneBeatTime);
		}
		Evaluate (timing);
		return timing;
	}
	
	/// <summary>
	/// 判定したノートに対する評価.
	/// </summary>
	/// <param name="timing">ジャストタイミングとの差</param>
	protected virtual void Evaluate(long timing) {
		CreateEvaluateObject();
	}

	private void CreateEvaluateObject() {
		var evaleff = GameObject.Instantiate (evaluateEffectObj) as GameObject;
		evaleff.SetParentEx (evaluateEffectBasePosition);
		var effrend = evaleff.GetComponentInChildren<SpriteRenderer>();
		effrend.sprite = SpriteTank.Get ("effect_perfect");
	}

}
