using UnityEngine;
using System.Collections;

public class ProfileListInformation {
	public ProfileCategory.CategoryId id { get; set; }
	public string key { get; set; }
	public string value { get; set; }
	public Color valueColor { get; set; }
	public Sprite sprite { get; set; }
	public bool useText { get; set; }
	public bool usePicture { get; set; }

	public ProfileListInformation(ProfileCategory.CategoryId id, string key, string value, Color valueColor, bool useText, bool usePicture) {
		this.id = id;
		this.key = key;
		this.value = value;
		this.valueColor = valueColor;
		this.useText = useText;
		this.usePicture = usePicture;
	}
	
	public void SetValue (string value, Color valueColor) {
		this.value = value;
		this.valueColor = valueColor;
	}
	
	public IEnumerator LoadSprite (MonoBehaviour component, string extension, System.Action<Sprite> callback) {
		if (sprite == null) {
			if (id == ProfileCategory.CategoryId.PLAYER_ICON) {
				yield return component.StartCoroutine (UserDataController.instance.LoadPlayerIcon (extension, (data)=>sprite = data));
			}
			else if (id == ProfileCategory.CategoryId.PLAYER_PLATE) {
				yield return component.StartCoroutine (UserDataController.instance.LoadPlayerPlate (extension, (data)=>sprite = data));
			}
			else if (id == ProfileCategory.CategoryId.PLAYER_FRAME) {
				yield return component.StartCoroutine (UserDataController.instance.LoadPlayerFrame (extension, (data)=>sprite = data));
			}
		}
		if (sprite != null) {
			usePicture = true;
			useText = false;
		}
		else {
			usePicture = false;
			useText = true;
		}
		callback (sprite);
	}
}

public static class ProfileCategory {
	public enum CategoryId {
		NONE,
		PLAYER_NAME,
		PLAYER_TITLE,
		PLAYER_ICON,
		PLAYER_PLATE,
		PLAYER_FRAME,
		SORT_TYPE,
		REFINE_LEVEL,
		REFINE_DIFFICULTY,
		REFINE_NOTES_DESIGNER,
		REFINE_NO_MISS,
		REFINE_FC,
		REFINE_AP,
	}
	public static CategoryId ToProfileCategory(this int value) {
		return (CategoryId)value;
	}
	public static int ToInt(this CategoryId value) {
		return (int)value;
	}

	public static string ToProfileValueString (this string value) {
		if (string.IsNullOrEmpty (value)) {
			return "(EMPTY)";
		}
		return value;
	}

	public static Color ToProfileValueColor (this string value) {
		if (string.IsNullOrEmpty (value)) {
			return Color.cyan;
		}
		return Color.white;
	}

	public static string ToConditionAchievedString (this int value) {
		// TrackListRefineCondition.FindFCより.
		if (value == 0) {
			return "OFF";
		}
		else if (value == 1) {
			return "ACHIEVED";
		}
		else if (value == 2) {
			return "NOT ACHIEVED";
		}
		return string.Empty;
	}
	
}
