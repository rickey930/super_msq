﻿using System.Collections.Generic;

public class MaimaiScheduler {
	private IMaimaiSchedule[] schedule;
	private int scheduleIndex;

	public MaimaiScheduler(IMaimaiSchedule[] schedule) {
		var s = new List<IMaimaiSchedule> (schedule);
		s.Sort ((a, b) => {
			var mat = a.GetScheduleTime () - b.GetScheduleTime ();
			if (mat > 0) return 1;
			else if (mat < 0) return -1;
			return a.GetSourceIndex() - b.GetSourceIndex();
		});
		this.schedule = s.ToArray ();
		scheduleIndex = 0;
	}

	/// <summary>
	/// 頭出し
	/// </summary>
	public void Cueing(long time) {
		int len = schedule.Length;
		int backMinIndex = len;
		bool find = false;
		for (int i = 0; i < len; i++) {
			// timeより後に表示を終えるやつらの中で、一番若いindex.
			if (time <= schedule[i].GetScheduleEndTime()) {
				backMinIndex = UnityEngine.Mathf.Min(i, backMinIndex);
			}
			// timeより後の中で、一番最初のindex.それより小さく、表示していたいものがあればそれを使用.
			if (time <= schedule[i].GetScheduleTime()) {
				scheduleIndex = UnityEngine.Mathf.Min(i, backMinIndex);
				find = true;
				break;
			}
		}
		if (!find) {
			scheduleIndex = backMinIndex;
		}
	}

	/// <summary>
	/// スケジュールの実行.
	/// </summary>
	/// <param name="time">Time.</param>
	public void Run(long time) {
		int len = schedule.Length;
		for (int i = scheduleIndex; i < len; i++) {
			if (time >= schedule[i].GetScheduleTime()) {
				schedule[i].ScheduleToDo(time);
				scheduleIndex = i + 1;
			}
		}
	}

}

public interface IMaimaiSchedule {
	/// <summary>
	/// ScheduleToDoメソッドを実行する時間.
	/// </summary>
	long GetScheduleTime();
	/// <summary>
	/// 頭出し(Cueing)したときに参照される時間.
	/// </summary>
	long GetScheduleEndTime();
	/// <summary>
	/// GetScheduleTimeメソッド(時間)が来たら実行する内容.
	/// </summary>
	void ScheduleToDo(long nowTime);
	/// <summary>
	/// 元データの順位.
	/// </summary>
	/// <returns>時間が同じ中でも前後関係をつけたい</returns>
	int GetSourceIndex();
}