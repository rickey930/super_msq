﻿using UnityEngine;
using System.Collections;

public class MaisqObjectTimeAnimationAlpha : TimeAnimationAlpha {
	private WideSlideMarkerMeshRenderer wideSlideMarkerMeshRenderer;
	private SlideEvaluateTextureMeshRenderer slideEvaluateTextureMeshRenderer;
	protected override void Start () {
		base.Start ();
		wideSlideMarkerMeshRenderer = GetComponent<WideSlideMarkerMeshRenderer> ();
		slideEvaluateTextureMeshRenderer = GetComponent<SlideEvaluateTextureMeshRenderer> ();
	}
	
	protected override void ReferenceValue (float value) {
		base.ReferenceValue (value);
		if (wideSlideMarkerMeshRenderer != null) {
			Color temp = wideSlideMarkerMeshRenderer.material.GetColor ("_SpColor");;
			temp.a = value;
			wideSlideMarkerMeshRenderer.material.SetColor ("_SpColor", temp);
		}
		if (slideEvaluateTextureMeshRenderer != null) {
			Color temp = slideEvaluateTextureMeshRenderer.material.GetColor ("_SpColor");;
			temp.a = value;
			slideEvaluateTextureMeshRenderer.material.SetColor ("_SpColor", temp);
		}
	}
}
