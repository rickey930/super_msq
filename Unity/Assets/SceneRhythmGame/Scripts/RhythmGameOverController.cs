﻿using UnityEngine;
using System.Collections;

public class RhythmGameOverController : MonoBehaviour {
	
	[SerializeField]
	private BgInfoAnimationController bgInfoNameSpriteCtrl;
	[SerializeField]
	private GameObject dialog;
	[SerializeField]
	private float showDialogTime;

	private float timer;
	private bool dialogShowed;

	// Use this for initialization
	void Start () {
		bgInfoNameSpriteCtrl.Setup (Color.red, "game over", 36f, 0.3f);
		bgInfoNameSpriteCtrl.ActivateSpriteRenderer();
		timer = 0;
		dialogShowed = false;
		dialog.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		if (timer > showDialogTime) {
			if (!dialogShowed) {
				dialog.SetActive(true);
				dialogShowed = true;
			}
		}
		else {
			timer += Time.deltaTime;
		}
	}

	public void Show() {
		this.gameObject.SetActive(true);
	}

	public void Hide() {
		this.gameObject.SetActive(false);
	}
}
