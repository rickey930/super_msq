using UnityEngine;
using System.Collections;

public class TestScoreCreator {

	public static IEnumerator LoadTestScore(System.Action<RhythmGameLibrary.Score.Order[]> callback) {
		var score = CreateTestScore1();
		callback (score);
		
		yield break;
	}

	public static IEnumerator LoadTestFile(System.Action<RhythmGameLibrary.Score.Order[]> callback) {
		string msqScript;
		string path =  @"file:///D:/test_maidata.txt"; //score_info.txt
		using (WWW www = new WWW (path)) {
			yield return www;
			if (string.IsNullOrEmpty(www.error)) {
				msqScript = www.text;
			}
			else {
				Debug.LogError(www.error);
				yield break;
			}
		}
		callback (MsqScriptReader.Read (msqScript));
	}
	
	public static IEnumerator LoadTestRawSimaiFile(System.Action<RhythmGameLibrary.Score.Order[]> callback) {
		string simaiRawScript;
		string path =  @"file:///D:/test_rawmaidata.txt";
		using (WWW www = new WWW (path)) {
			yield return www;
			if (string.IsNullOrEmpty(www.error)) {
				simaiRawScript = www.text;
			}
			else {
				Debug.LogError(www.error);
				yield break;
			}
		}
		CommonScriptFormatter.ReadResult result;
		var raw = SimaiRawReader.Read(simaiRawScript, out result);
		callback (SimaiNoteReader.Read (raw.inote[5]));
		//callback (SimaiActiveMessageReader.Read (raw.activeMessageTime, raw.activeMessageContent, Vector2.zero, 1, Color.white));
	}

	public static XMaimaiNote.XDocument.Document[] CreateTestDocument1(out float firstBpm) {
		var converter = new MaimaiScoreConverter(new RhythmGameLibrary.Score.Order[0]);
		
		var sampleSlideScore = new XMaimaiScore.Note.Slide (
			null,
			new XMaimaiScore.Step.Interval (1.0f),
			new XMaimaiScore.Note.Slide.Chain[][] {
			new XMaimaiScore.Note.Slide.Chain[] {
				new XMaimaiScore.Note.Slide.Chain (
					new XMaimaiScore.Note.Slide.Command[] {
					new XMaimaiScore.Note.Slide.Command.Straight (
						CircleCalculator.PointOnCircle(Vector2.zero, 1.01f, Constants.instance.GetPieceDegree(1 - 1)+2f),
						CircleCalculator.PointOnCircle(Vector2.zero, 1f, Constants.instance.GetPieceDegree(4 - 1))
						),
				},
				null) { guide_start_marker_positioning = true },
				new XMaimaiScore.Note.Slide.Chain (
					new XMaimaiScore.Note.Slide.Command[] {
					new XMaimaiScore.Note.Slide.Command.Straight (
						CircleCalculator.PointOnCircle(Vector2.zero, 1f, Constants.instance.GetPieceDegree(1 - 1)),
						CircleCalculator.PointOnCircle(Vector2.zero, 1f, Constants.instance.GetPieceDegree(5 - 1))
						),
				},
				null) { is_surface = true },
				new XMaimaiScore.Note.Slide.Chain (
					new XMaimaiScore.Note.Slide.Command[] {
					new XMaimaiScore.Note.Slide.Command.Straight (
						CircleCalculator.PointOnCircle(Vector2.zero, 1.01f, Constants.instance.GetPieceDegree(1 - 1)-2f),
						CircleCalculator.PointOnCircle(Vector2.zero, 1f, Constants.instance.GetPieceDegree(6 - 1))
						),
					/*
							new XMaimaiScore.Note.Slide.Command.Curve (
								Vector2.zero,
								1.00f,
								Constants.instance.GetPieceDegree(1 - 1),
								-3 * 45f
							),*/
				},
				null) { guide_start_marker_positioning = true },
			},
		}
		);
		
		var sampleSlideScore2 = new XMaimaiScore.Note.Slide (
			new XMaimaiScore.Note.Tap(5) {
			time = 3.0f
		},
		new XMaimaiScore.Step.Interval (1.0f),
		new XMaimaiScore.Note.Slide.Chain[][] {
			new XMaimaiScore.Note.Slide.Chain[] {
				new XMaimaiScore.Note.Slide.Chain (
					new XMaimaiScore.Note.Slide.Command[] {
					new XMaimaiScore.Note.Slide.Command.Straight (
						CircleCalculator.PointOnCircle(Vector2.zero, 1f, Constants.instance.GetPieceDegree(5 - 1)),
						CircleCalculator.PointOnCircle(Vector2.zero, Constants.instance.MAIMAI_INNER_RADIUS / Constants.instance.MAIMAI_OUTER_RADIUS, Constants.instance.GetPieceDegree(3 - 1))
						),
					
					new XMaimaiScore.Note.Slide.Command.Curve (
						Vector2.zero,
						Constants.instance.MAIMAI_INNER_RADIUS / Constants.instance.MAIMAI_OUTER_RADIUS,
						Constants.instance.GetPieceDegree(3 - 1),
						-5 * 45f
						),
					new XMaimaiScore.Note.Slide.Command.Straight (
						CircleCalculator.PointOnCircle(Vector2.zero, Constants.instance.MAIMAI_INNER_RADIUS / Constants.instance.MAIMAI_OUTER_RADIUS, Constants.instance.GetPieceDegree(6 - 1)),
						CircleCalculator.PointOnCircle(Vector2.zero, 1f, Constants.instance.GetPieceDegree(4 - 1))
						),
				}, null),
			},
		}
		);
		
		var sampleSlideScore3 = new XMaimaiScore.Note.Slide (
			null,
			new XMaimaiScore.Step.Interval (1.0f),
			new XMaimaiScore.Note.Slide.Chain[][] {
			new XMaimaiScore.Note.Slide.Chain[] {
				new XMaimaiScore.Note.Slide.Chain (
					new XMaimaiScore.Note.Slide.Command[] {
					//	new XMaimaiScore.Note.Slide.Command.Straight (
					//		CircleCalculator.PointOnCircle(Vector2.zero, 1f, Constants.instance.GetPieceDegree(5 - 1)),
					//		CircleCalculator.PointOnCircle(Vector2.zero, 1f, Constants.instance.GetPieceDegree(8 - 1))
					//		),
					new XMaimaiScore.Note.Slide.Command.Curve (
						Vector2.zero,
						1.0f,
						Constants.instance.GetPieceDegree(4 - 1),
						-2*45f),
				}, null),
			},
		}
		);
		
		sampleSlideScore.SetWaitStep (XMaimaiScore.Step.CreateInterval (1.0f));
		var sampleSlide = converter.Scrutinize (sampleSlideScore);
		var sampleSlidePatternDoc = ((XMaimaiNote.XDocument.Document.Slide)sampleSlide[0]).pattern;
		sampleSlidePatternDoc.visualizeTime = 3000;
		sampleSlidePatternDoc.moveStartTime = 4000;
		sampleSlidePatternDoc.moveEndTime = 5000;
		sampleSlidePatternDoc.markerColor = XMaimaiNote.XDocument.GuideColor.SINGLE;
		
		sampleSlideScore2.SetWaitStep (XMaimaiScore.Step.CreateInterval (1.0f));
		var sampleSlide2 = converter.Scrutinize (sampleSlideScore2);
		var sampleSlidePatternDoc2 = ((XMaimaiNote.XDocument.Document.Slide)sampleSlide2[1]).pattern;
		sampleSlidePatternDoc2.visualizeTime = 3000;
		sampleSlidePatternDoc2.moveStartTime = 4000;
		sampleSlidePatternDoc2.moveEndTime = 5000;
		sampleSlidePatternDoc2.markerColor = XMaimaiNote.XDocument.GuideColor.SINGLE;
		
		sampleSlideScore3.SetWaitStep (XMaimaiScore.Step.CreateInterval (1.0f));
		var sampleSlide3 = converter.Scrutinize (sampleSlideScore3);
		var sampleSlidePatternDoc3 = ((XMaimaiNote.XDocument.Document.Slide)sampleSlide3[0]).pattern;
		sampleSlidePatternDoc3.visualizeTime = 3000;
		sampleSlidePatternDoc3.moveStartTime = 4000;
		sampleSlidePatternDoc3.moveEndTime = 5000;
		sampleSlidePatternDoc3.markerColor = XMaimaiNote.XDocument.GuideColor.SINGLE;
		
		var docs = new XMaimaiNote.XDocument.Document[] {
			
			new XMaimaiNote.XDocument.Document.Break () {
				justTime = 3000,
				button = new XMaimaiNote.XDocument.Document.Button(6),
				guideSpinSpeed = 0,
				guideShapeType = XMaimaiNote.XDocument.Document.Break.GuideRingShapeType.CIRCLE,
				guideFadeSpeed = 300,
				guideMoveSpeed = 500,
				guideScale = 0.75f,
				//guideColor = XMaimaiNote.XDocument.GuideColor.EACH,
				archId = 0
			},
			new XMaimaiNote.XDocument.Document.Hold () {
				headTime = 3000,
				footTime = 4000,
				button = new XMaimaiNote.XDocument.Document.Button(4),
				guideFadeSpeed = 300,
				guideMoveSpeed = 500,
				guideScale = 0.75f,
				guideColor = XMaimaiNote.XDocument.GuideColor.EACH,
				archId = 0
			},
			new XMaimaiNote.XDocument.Document.Hold () {
				headTime = 3000,
				footTime = 9000,
				button = new XMaimaiNote.XDocument.Document.Button(1),
				guideFadeSpeed = 300,
				guideMoveSpeed = 500,
				guideScale = 0.75f,
				guideColor = XMaimaiNote.XDocument.GuideColor.EACH,
				archId = 0
			},
			sampleSlide[0],
			sampleSlide2[0],
			sampleSlide2[1],
			sampleSlide3[0],
		};

		firstBpm = 60.0f / ((3.0f - 0.5f) / 4.0f);
		return docs;
	}

	private static RhythmGameLibrary.Score.Order[] CreateTestScore1() {
		string msqScript = @"bpm(128);beat(4);note(tap(1));log('age');";
		return MsqScriptReader.Read (msqScript);
	}

}
