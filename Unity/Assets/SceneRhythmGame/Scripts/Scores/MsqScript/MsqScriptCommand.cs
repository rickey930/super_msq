using System;
using System.Collections.Generic;

namespace XMsqScript {
	/// <summary>
	/// コマンドを呼び出したオブジェクトの情報
	/// </summary>
	public class CallerInfo {
		public Formatter.Room script { get; private set; }
		public TextDetailInfo place { get; private set; }
		public Global global { get; private set; }
		/// <summary>
		/// 裸で呼び出したらglobal、拡張で呼び出したら拡張元のオブジェクトが入る.
		/// </summary>
		public Object field { get; private set; }
		/// <summary>
		/// 呼び出した関数.
		/// </summary>
		public Function called { get; private set; }
		public CallerInfo(Global global) {
			this.script = null;
			this.place = null;
			this.global = global;
			this.field = global;
			this.called = null;
		}
		private CallerInfo(Formatter.Room script, TextDetailInfo place, Global global, Function called, Object field) {
			this.script = script;
			this.place = place;
			this.global = global;
			this.field = field;
			this.called = called;
		}
		public CallerInfo CalledGlobal(Formatter.Room script, TextDetailInfo place, Function called) {
			return new CallerInfo(script, place, global, called, global);
		}
		public CallerInfo CalledExpand(Formatter.Room script, TextDetailInfo place, Function called, Object field) {
			Global newgl = field.type == Type.GLOBAL ? field.Cast<Global>() : global;
			return new CallerInfo(script, place, newgl, called, field);
		}
	}
	
	/// <summary>
	/// Msqコマンドの定義.
	/// </summary>
	public class Command {
		private Dictionary<Type, Dictionary<string, Func<CallerInfo, Object[], Object>>> commandList;
		
		public Object ExecuteCommand(CallerInfo caller, string name, params Object[] arguments) {
			if (commandList.ContainsKey(caller.field.type) && 
			    commandList[caller.field.type] != null && 
			    commandList[caller.field.type].ContainsKey(name) &&
			    commandList[caller.field.type][name] != null) {
				return commandList[caller.field.type][name](caller, arguments);
			}
			return new CommandNotFoundException(caller, name, arguments);
		}
		delegate Object SlideArg1NumberEntity(CallerInfo caller, ISlideWait wait, IStep step, Number arg);
		delegate Object SlideArgs2NumbersEntity(CallerInfo caller, ISlideWait wait, IStep step, Number arg0, Number args1);
		delegate Object SlideArgs3NumbersEntity(CallerInfo caller, ISlideWait wait, IStep step, Number arg0, Number args1, Number args2);
		delegate Object SlideArgs2SensorsEntity(CallerInfo caller, ISlideWait wait, IStep step, Sensor arg0, Sensor args1);
		
		public Command() {
			
			commandList = new Dictionary<Type, Dictionary<string, Func<CallerInfo, Object[], Object>>>();
			
			Action<Dictionary<string, Func<CallerInfo, Object[], Object>>> bTurnableUpgrade = (bTurnableUpgrader)=>{
				bTurnableUpgrader.Add(Document.TURN, (caller, args) => {
					if (args.Length == 1 && args[0].type == Type.NUMBER) {
						return ((IButtonTurnable)caller.field).Turn(caller, args[0].Cast<Number>());
					}
					return new ArgumentsNotMatchException(caller, Document.TURN, args);
				});
			};
			Action<Dictionary<string, Func<CallerInfo, Object[], Object>>> turnableUpgrade = (turnableUpgrader)=>{
				turnableUpgrader.Add(Document.TURN, (caller, args) => {
					if (args.Length == 1 && args[0].type == Type.NUMBER) {
						return ((IButtonTurnable)caller.field).Turn(caller, args[0].Cast<Number>());
					}
					else if (args.Length == 1 && !args[0].IsNaN) {
						return ((IFreeTurnable)caller.field).Turn(caller, args[0] as IMsqValue);
					}
					return new ArgumentsNotMatchException(caller, Document.TURN, args);
				});
			};
			Action<Dictionary<string, Func<CallerInfo, Object[], Object>>> mirrorableUpgrade = (mirrorableUpgrader)=>{
				mirrorableUpgrader.Add(Document.MIRROR_HOLIZONTAL, (caller, args) => {
					if (args.Length == 0) {
						return ((IMirrorable)caller.field).MirrorHolizontal(caller);
					}
					return new ArgumentsNotMatchException(caller, Document.MIRROR_HOLIZONTAL, args);
				});
				mirrorableUpgrader.Add(Document.MIRROR_VERTICAL, (caller, args) => {
					if (args.Length == 0) {
						return ((IMirrorable)caller.field).MirrorVertical(caller);
					}
					return new ArgumentsNotMatchException(caller, Document.MIRROR_VERTICAL, args);
				});
			};
			Action<Dictionary<string, Func<CallerInfo, Object[], Object>>> iOptionbleUpgrade = (iOptionableUpgrader)=>{
				iOptionableUpgrader.Add(Document.OPTION, (caller, args) => {
					if (args.Length == 1 && args[0].type == Type.TABLE) {
						return ((IOptionable)caller.field).Option(caller, args[0].Cast<Table>());
					}
					else if (args.Length == 1 && args[0].type == Type.ARRAY) {
						return ((IOptionable)caller.field).Option(caller, args[0].Cast<Array>());
					}
					return new ArgumentsNotMatchException(caller, Document.OPTION, args);
				});
			};
			
			// Global
			Dictionary<string, Func<CallerInfo, Object[], Object>> upgrader;
			upgrader = commandList[Type.GLOBAL] = new Dictionary<string, Func<CallerInfo, Object[], Object>>();
			upgrader.Add(Document.BPM, (caller, args) => {
				if (args.Length == 1 && !args[0].IsNaN) {
					return caller.field.Cast<Global>().Bpm(caller, args[0] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.BPM, args);
			});
			upgrader.Add(Document.BEAT, (caller, args) => {
				if (args.Length == 1 && !args[0].IsNaN) {
					return caller.field.Cast<Global>().Beat(caller, args[0] as IMsqValue);
				}
				else if (args.Length == 2 && !args[0].IsNaN && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().Beat(caller, args[0] as IMsqValue, args[1].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.BEAT, args);
			});
			upgrader.Add(Document.INTERVAL, (caller, args) => {
				if (args.Length == 1 && !args[0].IsNaN) {
					return caller.field.Cast<Global>().Interval(caller, args[0] as IMsqValue);
				}
				else if (args.Length == 2 && !args[0].IsNaN && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().Interval(caller, args[0] as IMsqValue, args[1].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.INTERVAL, args);
			});
			upgrader.Add(Document.NOTE, (caller, args) => {
				if (args.Length > 0) {
					var list = new List<INote>();
					bool error = false;
					foreach (var arg in args) {
						if (arg.IsNote) {
							list.Add(arg as INote);
						}
						else {
							error = true;
							break;
						}
					}
					if (!error) {
						return caller.field.Cast<Global>().Note(caller, list.ToArray());
					}
				}
				return new ArgumentsNotMatchException(caller, Document.NOTE, args);
			});
			upgrader.Add(Document.REST, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().Rest(caller);
				}
				else if (args.Length == 1 && args[0].type == Type.NUMBER) {
					return caller.field.Cast<Global>().Rest(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.REST, args);
			});
			upgrader.Add(Document.LOG, (caller, args) => {
				if (args.Length == 1) {
					return caller.field.Cast<Global>().Log(caller, args[0]);
				}
				return new ArgumentsNotMatchException(caller, Document.LOG, args);
			});
			upgrader.Add(Document.GET, (caller, args) => {
				if (args.Length == 1 && args[0].IsKey) {
					return caller.field.Cast<Global>().Get(caller, args[0] as IKey);
				}
				return new ArgumentsNotMatchException(caller, Document.GET, args);
			});
			upgrader.Add(Document.SET, (caller, args) => {
				if (args.Length == 2 && args[0].IsKey) {
					return caller.field.Cast<Global>().Set(caller, args[0] as IKey, args[1]);
				}
				return new ArgumentsNotMatchException(caller, Document.SET, args);
			});
			upgrader.Add(Document.GOT, (caller, args) => {
				if (args.Length == 1 && args[0].IsKey) {
					return caller.field.Cast<Global>().Got(caller, args[0] as IKey);
				}
				return new ArgumentsNotMatchException(caller, Document.GOT, args);
			});
			upgrader.Add(Document.REMOVE, (caller, args) => {
				if (args.Length == 1 && args[0].IsKey) {
					return caller.field.Cast<Global>().Remove(caller, args[0] as IKey);
				}
				return new ArgumentsNotMatchException(caller, Document.REMOVE, args);
			});
			upgrader.Add(Document.GETOP, (caller, args) => {
				if (args.Length == 1 && args[0].IsKey) {
					return caller.field.Cast<Global>().TopGet(caller, args[0] as IKey);
				}
				return new ArgumentsNotMatchException(caller, Document.GETOP, args);
			});
			upgrader.Add(Document.SETOP, (caller, args) => {
				if (args.Length == 2 && args[0].IsKey) {
					return caller.field.Cast<Global>().TopSet(caller, args[0] as IKey, args[1]);
				}
				return new ArgumentsNotMatchException(caller, Document.SETOP, args);
			});
			upgrader.Add(Document.GOTOP, (caller, args) => {
				if (args.Length == 1 && args[0].IsKey) {
					return caller.field.Cast<Global>().TopGot(caller, args[0] as IKey);
				}
				return new ArgumentsNotMatchException(caller, Document.GOTOP, args);
			});
			upgrader.Add(Document.REMTOP, (caller, args) => {
				if (args.Length == 1 && args[0].IsKey) {
					return caller.field.Cast<Global>().TopRemove(caller, args[0] as IKey);
				}
				return new ArgumentsNotMatchException(caller, Document.REMTOP, args);
			});
			upgrader.Add(Document.EXTRA_FORMAT, (caller, args) => {
				if (args.Length == 2 && args[0].IsKey && args[1].type == Type.TEXT) {
					return caller.field.Cast<Global>().ExtraFormat(caller, args[0] as IKey, args[1].Cast<Text>());
				}
				return new ArgumentsNotMatchException(caller, Document.EXTRA_FORMAT, args);
			});
			upgrader.Add(Document.LIBRARY, (caller, args) => {
				if (args.Length == 1 && args[0].IsKey) {
					return caller.field.Cast<Global>().Library(caller, args[0] as IKey);
				}
				return new ArgumentsNotMatchException(caller, Document.LIBRARY, args);
			});
			upgrader.Add(Document.INCLUDE, (caller, args) => {
				if (args.Length == 1 && args[0].IsKey) {
					return caller.field.Cast<Global>().Include(caller, args[0] as IKey);
				}
				return new ArgumentsNotMatchException(caller, Document.INCLUDE, args);
			});
			upgrader.Add(Document.MARGE, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.GLOBAL) {
					return caller.field.Cast<Global>().Marge(caller, args[0].Cast<Global>());
				}
				else if (args.Length == 2 && args[0].type == Type.GLOBAL && args[1].type == Type.FLAG) {
					return caller.field.Cast<Global>().Marge(caller, args[0].Cast<Global>(), args[1].Cast<Flag>());
				}
				return new ArgumentsNotMatchException(caller, Document.MARGE, args);
			});
			upgrader.Add(Document.MATH_ADDITION, (caller, args) => {
				if (args.Length == 2 &&
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().Add(caller, args[0].Cast<Number>(), args[1].Cast<Number>());
				}
				else if (args.Length == 2 &&
				         !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().Add(caller, args[0] as IMsqValue, args[1] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.MATH_ADDITION, args);
			});
			upgrader.Add(Document.MATH_SUBTRACTION, (caller, args) => {
				if (args.Length == 2 &&
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().Sub(caller, args[0].Cast<Number>(), args[1].Cast<Number>());
				}
				else if (args.Length == 2 &&
				         !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().Sub(caller, args[0] as IMsqValue, args[1] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.MATH_SUBTRACTION, args);
			});
			upgrader.Add(Document.MATH_MULTIPLICATION, (caller, args) => {
				if (args.Length == 2 &&
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().Mul(caller, args[0].Cast<Number>(), args[1].Cast<Number>());
				}
				else if (args.Length == 2 &&
				         !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().Mul(caller, args[0] as IMsqValue, args[1] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.MATH_MULTIPLICATION, args);
			});
			upgrader.Add(Document.MATH_DIVISION, (caller, args) => {
				if (args.Length == 2 &&
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().Div(caller, args[0].Cast<Number>(), args[1].Cast<Number>());
				}
				else if (args.Length == 2 &&
				         !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().Div(caller, args[0] as IMsqValue, args[1] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.MATH_DIVISION, args);
			});
			upgrader.Add(Document.MATH_POWER, (caller, args) => {
				if (args.Length == 2 &&
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().Pow(caller, args[0].Cast<Number>(), args[1].Cast<Number>());
				}
				else if (args.Length == 2 &&
				         !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().Pow(caller, args[0] as IMsqValue, args[1] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.MATH_POWER, args);
			});
			upgrader.Add(Document.MATH_MODULO, (caller, args) => {
				if (args.Length == 2 &&
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().Mod(caller, args[0].Cast<Number>(), args[1].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.MATH_MODULO, args);
			});
			upgrader.Add(Document.EQUAL, (caller, args) => {
				if (args.Length == 2 &&
				    !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().Equal(caller, args[0] as IMsqValue, args[1] as IMsqValue);
				}
				else if (args.Length == 2 &&
				         args[0].type == Type.TEXT && args[1].type == Type.TEXT) {
					return caller.field.Cast<Global>().Equal(caller, args[0].Cast<Text>(), args[1].Cast<Text>());
				}
				else if (args.Length == 2 &&
				         args[0].type == Type.POSITION && args[1].type == Type.POSITION) {
					return caller.field.Cast<Global>().Equal(caller, args[0].Cast<Position>(), args[1].Cast<Position>());
				}
				return new ArgumentsNotMatchException(caller, Document.EQUAL, args);
			});
			upgrader.Add(Document.NOT_EQUAL, (caller, args) => {
				if (args.Length == 2 &&
				    !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().NotEqual(caller, args[0] as IMsqValue, args[1] as IMsqValue);
				}
				else if (args.Length == 2 &&
				         args[0].type == Type.TEXT && args[1].type == Type.TEXT) {
					return caller.field.Cast<Global>().NotEqual(caller, args[0].Cast<Text>(), args[1].Cast<Text>());
				}
				else if (args.Length == 2 &&
				         args[0].type == Type.POSITION && args[1].type == Type.POSITION) {
					return caller.field.Cast<Global>().NotEqual(caller, args[0].Cast<Position>(), args[1].Cast<Position>());
				}
				return new ArgumentsNotMatchException(caller, Document.NOT_EQUAL, args);
			});
			upgrader.Add(Document.GREATER, (caller, args) => {
				if (args.Length == 2 &&
				    !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().Greater(caller, args[0] as IMsqValue, args[1] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.GREATER, args);
			});
			upgrader.Add(Document.LESS, (caller, args) => {
				if (args.Length == 2 &&
				    !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().Less(caller, args[0] as IMsqValue, args[1] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.LESS, args);
			});
			upgrader.Add(Document.GREATER_EQUAL, (caller, args) => {
				if (args.Length == 2 &&
				    !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().GreaterEqual(caller, args[0] as IMsqValue, args[1] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.GREATER_EQUAL, args);
			});
			upgrader.Add(Document.LESS_EQUAL, (caller, args) => {
				if (args.Length == 2 &&
				    !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().LessEqual(caller, args[0] as IMsqValue, args[1] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.LESS_EQUAL, args);
			});
			upgrader.Add(Document.BIT_AND, (caller, args) => {
				if (args.Length == 2 &&
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().BitAnd(caller, args[0].Cast<Number>(), args[1].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.BIT_AND, args);
			});
			upgrader.Add(Document.BIT_OR, (caller, args) => {
				if (args.Length == 2 &&
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().BitOr(caller, args[0].Cast<Number>(), args[1].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.BIT_OR, args);
			});
			upgrader.Add(Document.BIT_XOR, (caller, args) => {
				if (args.Length == 2 &&
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().BitXor(caller, args[0].Cast<Number>(), args[1].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.BIT_XOR, args);
			});
			upgrader.Add(Document.BIT_NOT, (caller, args) => {
				if (args.Length == 1 &&
				    args[0].type == Type.NUMBER) {
					return caller.field.Cast<Global>().BitNot(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.BIT_NOT, args);
			});
			upgrader.Add(Document.BIT_SHIFT_LEFT, (caller, args) => {
				if (args.Length == 2 &&
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().BitShiftLeft(caller, args[0].Cast<Number>(), args[1].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.BIT_SHIFT_LEFT, args);
			});
			upgrader.Add(Document.BIT_SHIFT_RIGHT, (caller, args) => {
				if (args.Length == 2 &&
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().BitShiftRight(caller, args[0].Cast<Number>(), args[1].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.BIT_SHIFT_RIGHT, args);
			});
			upgrader.Add(Document.NOT, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.FLAG) {
					return caller.field.Cast<Global>().Not(caller, args[0].Cast<Flag>());
				}
				return new ArgumentsNotMatchException(caller, Document.NOT, args);
			});
			upgrader.Add(Document.CONCATENATE, (caller, args) => {
				if (args.Length == 2 &&
				    args[0].type == Type.TEXT && args[1].type == Type.TEXT) {
					return caller.field.Cast<Global>().Concat(caller, args[0].Cast<Text>(), args[1].Cast<Text>());
				}
				return new ArgumentsNotMatchException(caller, Document.CONCATENATE, args);
			});
			upgrader.Add(Document.POSITION, (caller, args) => {
				if (args.Length == 2 &&
				    !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().Pos(caller, args[0] as IMsqValue, args[1] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.POSITION, args);
			});
			upgrader.Add(Document.RGB255, (caller, args) => {
				if (args.Length == 3 &&
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER && args[2].type == Type.NUMBER) {
					return caller.field.Cast<Global>().RGB255(caller, args[0].Cast<Number>(), args[1].Cast<Number>(), args[2].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.RGB255, args);
			});
			upgrader.Add(Document.RGBA255, (caller, args) => {
				if (args.Length == 4 &&
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER && args[2].type == Type.NUMBER && args[3].type == Type.NUMBER) {
					return caller.field.Cast<Global>().RGBA255(caller, args[0].Cast<Number>(), args[1].Cast<Number>(), args[2].Cast<Number>(), args[3].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.RGBA255, args);
			});
			upgrader.Add(Document.ARGB255, (caller, args) => {
				if (args.Length == 4 &&
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER && args[2].type == Type.NUMBER && args[3].type == Type.NUMBER) {
					return caller.field.Cast<Global>().ARGB255(caller, args[0].Cast<Number>(), args[1].Cast<Number>(), args[2].Cast<Number>(), args[3].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.ARGB255, args);
			});
			upgrader.Add(Document.RGB, (caller, args) => {
				if (args.Length == 3 &&
				    !args[0].IsNaN && !args[1].IsNaN && !args[2].IsNaN) {
					return caller.field.Cast<Global>().RGB(caller, args[0] as IMsqValue, args[1] as IMsqValue, args[2] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.RGB, args);
			});
			upgrader.Add(Document.RGBA, (caller, args) => {
				if (args.Length == 4 &&
				    !args[0].IsNaN && !args[1].IsNaN && !args[2].IsNaN && !args[3].IsNaN) {
					return caller.field.Cast<Global>().RGBA(caller, args[0] as IMsqValue, args[1] as IMsqValue, args[2] as IMsqValue, args[3] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.RGBA, args);
			});
			upgrader.Add(Document.ARGB, (caller, args) => {
				if (args.Length == 4 &&
				    !args[0].IsNaN && !args[1].IsNaN && !args[2].IsNaN && !args[3].IsNaN) {
					return caller.field.Cast<Global>().ARGB(caller, args[0] as IMsqValue, args[1] as IMsqValue, args[2] as IMsqValue, args[3] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.ARGB, args);
			});
			upgrader.Add(Document.BLACK, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().Black(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.BLACK, args);
			});
			upgrader.Add(Document.WHITE, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().White(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.WHITE, args);
			});
			upgrader.Add(Document.GRAY, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().Gray(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.GRAY, args);
			});
			upgrader.Add(Document.RED, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().Red(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.RED, args);
			});
			upgrader.Add(Document.GREEN, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().Green(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.GREEN, args);
			});
			upgrader.Add(Document.BLUE, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().Blue(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.BLUE, args);
			});
			upgrader.Add(Document.YELLOW, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().Yellow(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.YELLOW, args);
			});
			upgrader.Add(Document.MAGENTA, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().Magenta(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.MAGENTA, args);
			});
			upgrader.Add(Document.CYAN, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().Cyan(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.CYAN, args);
			});
			upgrader.Add(Document.SENSOR_DEGREE, (caller, args) => {
				if (args.Length == 1 && 
				    args[0].type == Type.NUMBER) {
					return caller.field.Cast<Global>().SensorDegree(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.SENSOR_DEGREE, args);
			});
			upgrader.Add(Document.SENSOR_DEGREE_DISTANCE_CLOCKWISE, (caller, args) => {
				if (args.Length == 2 && 
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().SensorDegreeDistanceClockwise(caller, args[0].Cast<Number>(), args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.SENSOR_DEGREE_DISTANCE_CLOCKWISE, args);
			});
			upgrader.Add(Document.SENSOR_DEGREE_DISTANCE_COUNTERCLOCKWISE, (caller, args) => {
				if (args.Length == 2 && 
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().SensorDegreeDistanceCounterClockwise(caller, args[0].Cast<Number>(), args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.SENSOR_DEGREE_DISTANCE_COUNTERCLOCKWISE, args);
			});
			upgrader.Add(Document.SENSOR_DEGREE_DISTANCE_RIGHT, (caller, args) => {
				if (args.Length == 2 && 
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().SensorDegreeDistanceRight(caller, args[0].Cast<Number>(), args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.SENSOR_DEGREE_DISTANCE_RIGHT, args);
			});
			upgrader.Add(Document.SENSOR_DEGREE_DISTANCE_LEFT, (caller, args) => {
				if (args.Length == 2 && 
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().SensorDegreeDistanceLeft(caller, args[0].Cast<Number>(), args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.SENSOR_DEGREE_DISTANCE_LEFT, args);
			});
			upgrader.Add(Document.ANGLE_X, (caller, args) => {
				if (args.Length == 1 && 
				    !args[0].IsNaN) {
					return caller.field.Cast<Global>().AngleX(caller, args[0] as IMsqValue);
				}
				else if (args.Length == 2 && 
				    !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().AngleX(caller, args[0] as IMsqValue, args[0] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.ANGLE_X, args);
			});
			upgrader.Add(Document.ANGLE_Y, (caller, args) => {
				if (args.Length == 1 && 
				    !args[0].IsNaN) {
					return caller.field.Cast<Global>().AngleY(caller, args[0] as IMsqValue);
				}
				else if (args.Length == 2 && 
				    !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().AngleY(caller, args[0] as IMsqValue, args[0] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.ANGLE_Y, args);
			});
			upgrader.Add(Document.OUTER_SENSOR_POSITION_X, (caller, args) => {
				if (args.Length == 1 && 
				    args[0].type == Type.NUMBER) {
					return caller.field.Cast<Global>().OuterSensorPositionX(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.OUTER_SENSOR_POSITION_X, args);
			});
			upgrader.Add(Document.OUTER_SENSOR_POSITION_Y, (caller, args) => {
				if (args.Length == 1 && 
				    args[0].type == Type.NUMBER) {
					return caller.field.Cast<Global>().OuterSensorPositionY(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.OUTER_SENSOR_POSITION_Y, args);
			});
			upgrader.Add(Document.INNER_SENSOR_POSITION_X, (caller, args) => {
				if (args.Length == 1 && 
				    args[0].type == Type.NUMBER) {
					return caller.field.Cast<Global>().InnerSensorPositionX(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.INNER_SENSOR_POSITION_X, args);
			});
			upgrader.Add(Document.INNER_SENSOR_POSITION_Y, (caller, args) => {
				if (args.Length == 1 && 
				    args[0].type == Type.NUMBER) {
					return caller.field.Cast<Global>().InnerSensorPositionY(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.INNER_SENSOR_POSITION_Y, args);
			});
			upgrader.Add(Document.CENTER_SENSOR_POSITION_X, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().CenterSensorPositionX(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.CENTER_SENSOR_POSITION_X, args);
			});
			upgrader.Add(Document.CENTER_SENSOR_POSITION_Y, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().CenterSensorPositionY(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.CENTER_SENSOR_POSITION_Y, args);
			});
			upgrader.Add(Document.SENSOR_POSITION_X, (caller, args) => {
				if (args.Length == 0 && 
				    args[0].type == Type.SENSOR) {
					return caller.field.Cast<Global>().SensorPositionX(caller, args[0].Cast<Sensor>());
				}
				return new ArgumentsNotMatchException(caller, Document.SENSOR_POSITION_X, args);
			});
			upgrader.Add(Document.SENSOR_POSITION_Y, (caller, args) => {
				if (args.Length == 0 && 
				    args[0].type == Type.SENSOR) {
					return caller.field.Cast<Global>().SensorPositionY(caller, args[0].Cast<Sensor>());
				}
				return new ArgumentsNotMatchException(caller, Document.SENSOR_POSITION_Y, args);
			});
			upgrader.Add(Document.OUTER_SENSOR_RADIUS, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().OuterSensorRadius(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.OUTER_SENSOR_RADIUS, args);
			});
			upgrader.Add(Document.INNER_SENSOR_RADIUS, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().InnerSensorRadius(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.INNER_SENSOR_RADIUS, args);
			});
			upgrader.Add(Document.CENTER_SENSOR_RADIUS, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().CenterSensorRadius(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.CENTER_SENSOR_RADIUS, args);
			});
			upgrader.Add(Document.ANGLE, (caller, args) => {
				if (args.Length == 1 && 
				    !args[0].IsNaN) {
					return caller.field.Cast<Global>().Angle(caller, args[0] as IMsqValue);
				}
				else if (args.Length == 2 && 
				         !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().Angle(caller, args[0] as IMsqValue, args[1] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.ANGLE, args);
			});
			upgrader.Add(Document.OUTER_SENSOR_POSITION, (caller, args) => {
				if (args.Length == 1 && 
				    args[0].type == Type.NUMBER) {
					return caller.field.Cast<Global>().OuterSensorPosition(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.OUTER_SENSOR_POSITION, args);
			});
			upgrader.Add(Document.INNER_SENSOR_POSITION, (caller, args) => {
				if (args.Length == 1 && 
				    args[0].type == Type.NUMBER) {
					return caller.field.Cast<Global>().InnerSensorPosition(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.INNER_SENSOR_POSITION, args);
			});
			upgrader.Add(Document.CENTER_SENSOR_POSITION, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().CenterSensorPosition(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.CENTER_SENSOR_POSITION, args);
			});
			upgrader.Add(Document.SENSOR_POSITION, (caller, args) => {
				if (args.Length == 1 && 
				    args[0].type == Type.SENSOR) {
					return caller.field.Cast<Global>().SensorPosition(caller, args[0].Cast<Sensor>());
				}
				return new ArgumentsNotMatchException(caller, Document.SENSOR_POSITION, args);
			});
			upgrader.Add(Document.TAP, (caller, args) => {
				if (args.Length == 1 && 
				    args[0].type == Type.NUMBER) {
					return caller.field.Cast<Global>().Tap(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.TAP, args);
			});
			upgrader.Add(Document.HOLD, (caller, args) => {
				if (args.Length == 2 && 
				    args[0].type == Type.NUMBER && args[1].IsStep) {
					return caller.field.Cast<Global>().Hold(caller, args[0].Cast<Number>(), args[1] as IStep);
				}
				return new ArgumentsNotMatchException(caller, Document.HOLD, args);
			});
			upgrader.Add(Document.SLIDE, (caller, args) => {
				if (args.Length > 0) {
					ISlideHead head = null;
					INote headStead = null;
					ISlideWait wait = null;
					IStep step = null;
					var patterns = new List<ISlidePattern>();
					int index = 0;
					bool error = false;
					bool waitIsIStep = false;
					if (args[index].IsSlideHead) {
						head = args[index] as ISlideHead;
						index++;
					}
					else if (args[index].IsNote) {
						headStead = args[index] as INote;
						index++;
					}
					if (args[index].IsSlideWait) {
						wait = args[index] as ISlideWait;
						if (wait is IStep) {
							waitIsIStep = true;
						}
						index++;
					}
					if (args[index].IsStep) {
						step = args[index] as IStep;
						index++;
					}
					else if (waitIsIStep) {
						step = wait as IStep;
						wait = null;
					}
					for (int i = index; i < args.Length; i++) {
						if (args[i].IsSlidePattern) {
							patterns.Add(args[i] as ISlidePattern);
						}
						else if (args[i].IsException) {
							return args[i];
						}
						else {
							error = true;
							break;
						}
					}
					if (patterns.Count > 0 && !error) {
						if (headStead == null) {
							return caller.field.Cast<Global>().Slide(caller, head, wait, step, patterns.ToArray());
						}
						else {
							return caller.field.Cast<Global>().Slide(caller, headStead, wait, step, patterns.ToArray());
						}
					}
				}
				return new ArgumentsNotMatchException(caller, Document.SLIDE, args);
			});
			upgrader.Add(Document.BREAK, (caller, args) => {
				if (args.Length == 1 && 
				    args[0].type == Type.NUMBER) {
					return caller.field.Cast<Global>().Break(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.BREAK, args);
			});
			upgrader.Add(Document.MESSAGE, (caller, args) => {
				if (args.Length == 5 && 
				    args[0].type == Type.TEXT && args[1].type == Type.POSITION && !args[2].IsNaN && args[3].type == Type.COLOR && args[4].IsStep) {
					return caller.field.Cast<Global>().Message(caller, args[0].Cast<Text>(), args[1].Cast<Position>(), args[2] as IMsqValue, args[3].Cast<Color>(), args[4] as IStep);
				}
				return new ArgumentsNotMatchException(caller, Document.MESSAGE, args);
			});
			upgrader.Add(Document.SCROLL_MESSAGE, (caller, args) => {
				if (args.Length == 5 && 
				    args[0].type == Type.TEXT && !args[1].IsNaN && !args[2].IsNaN && args[3].type == Type.COLOR && args[4].IsStep) {
					return caller.field.Cast<Global>().ScrollMessage(caller, args[0].Cast<Text>(), args[1] as IMsqValue, args[2] as IMsqValue, args[3].Cast<Color>(), args[4] as IStep);
				}
				return new ArgumentsNotMatchException(caller, Document.SCROLL_MESSAGE, args);
			});
			upgrader.Add(Document.SOUND_MESSAGE, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.TEXT) {
					return caller.field.Cast<Global>().SoundMessage(caller, args[0].Cast<Text>());
				}
				return new ArgumentsNotMatchException(caller, Document.SOUND_MESSAGE, args);
			});
			upgrader.Add(Document.TRAP, (caller, args) => {
				if (args.Length == 2 && 
				    args[0].type == Type.SENSOR && args[1].IsStep) {
					return caller.field.Cast<Global>().Trap(caller, args[0].Cast<Sensor>(), args[1] as IStep);
				}
				return new ArgumentsNotMatchException(caller, Document.TRAP, args);
			});
			upgrader.Add(Document.STEP, (caller, args) => {
				if (args.Length == 2 &&
				    !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().Step(caller, args[0] as IMsqValue, args[1] as IMsqValue);
				}
				else if (args.Length == 1 &&
				         !args[0].IsNaN) {
					return caller.field.Cast<Global>().Step(caller, args[0] as IMsqValue);
				}
				else if (args.Length == 3 &&
				         !args[0].IsNaN && !args[1].IsNaN && !args[2].IsNaN) {
					return caller.field.Cast<Global>().Step(caller, args[0] as IMsqValue, args[1] as IMsqValue, args[2] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.STEP, args);
			});
			upgrader.Add(Document.STEPS, (caller, args) => {
				if (args.Length > 0) {
					var steps = new List<IStep>();
					bool error = false;
					for (int i = 0; i < args.Length; i++) {
						if (args[i].IsStep) {
							steps.Add(args[i] as IStep);
						}
						else if (args[i].IsException) {
							return args[i];
						}
						else {
							error = true;
							break;
						}
					}
					if (steps.Count > 0 && !error) {
						return caller.field.Cast<Global>().Steps(caller, steps.ToArray());
					}
				}
				return new ArgumentsNotMatchException(caller, Document.STEPS, args);
			});
			upgrader.Add(Document.HEAD_STAR, (caller, args) => {
				if (args.Length == 1 && 
				    args[0].type == Type.NUMBER) {
					return caller.field.Cast<Global>().Star(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.HEAD_STAR, args);
			});
			upgrader.Add(Document.HEAD_BREAK_STAR, (caller, args) => {
				if (args.Length == 1 && 
				    args[0].type == Type.NUMBER) {
					return caller.field.Cast<Global>().BreakStar(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.HEAD_BREAK_STAR, args);
			});
			upgrader.Add(Document.HEAD_NOTHING, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().NoHead(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.HEAD_NOTHING, args);
			});
			upgrader.Add(Document.WAIT_DEFAULT, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().WaitDefault(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.WAIT_DEFAULT, args);
			});
			upgrader.Add(Document.WAIT_CUSTOM_LOCAL_BPM, (caller, args) => {
				if (args.Length == 1 &&
				         !args[0].IsNaN) {
					return caller.field.Cast<Global>().WaitCustomBpm(caller, args[0] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.WAIT_CUSTOM_LOCAL_BPM, args);
			});
			upgrader.Add(Document.WAIT_CUSTOM_INTERVAL, (caller, args) => {
				if (args.Length == 1 &&
				    !args[0].IsNaN) {
					return caller.field.Cast<Global>().WaitCustomInterval(caller, args[0] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.WAIT_CUSTOM_INTERVAL, args);
			});
			upgrader.Add(Document.WAIT_CUSTOM_STEP, (caller, args) => {
				if (args.Length == 2 &&
				    !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().WaitCustomStep(caller, args[0] as IMsqValue, args[1] as IMsqValue);
				}
				else if (args.Length == 1 &&
						 !args[0].IsNaN) {
					return caller.field.Cast<Global>().WaitCustomInterval(caller, args[0] as IMsqValue);
				}
				else if (args.Length == 3 &&
				         !args[0].IsNaN && !args[1].IsNaN && !args[2].IsNaN) {
					return caller.field.Cast<Global>().WaitCustomStep(caller, args[0] as IMsqValue, args[1] as IMsqValue, args[2] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.WAIT_CUSTOM_STEP, args);
			});
			upgrader.Add(Document.WAITS, (caller, args) => {
				if (args.Length > 0) {
					var waits = new List<ISlideWait>();
					bool error = false;
					for (int i = 0; i < args.Length; i++) {
						if (args[i].IsStep) {
							waits.Add(args[i] as ISlideWait);
						}
						else if (args[i].IsException) {
							return args[i];
						}
						else {
							error = true;
							break;
						}
					}
					if (waits.Count > 0 && !error) {
						return caller.field.Cast<Global>().Waits(caller, waits.ToArray());
					}
				}
				return new ArgumentsNotMatchException(caller, Document.WAITS, args);
			});
			upgrader.Add(Document.PATTERN, (caller, args) => {
				if (args.Length > 0) {
					ISlideWait wait = null;
					IStep step = null;
					var chains = new List<ISlidePattern>();
					int index = 0;
					bool error = false;
					bool waitIsIStep = false;
					if (args[index].IsSlideWait) {
						wait = args[index] as ISlideWait;
						if (wait is IStep) {
							waitIsIStep = true;
						}
						index++;
					}
					if (args[index].IsStep) {
						step = args[index] as IStep;
						index++;
					}
					else if (waitIsIStep) {
						step = wait as IStep;
						wait = null;
					}
					for (int i = index; i < args.Length; i++) {
						if (args[i].IsSlidePattern) {
							chains.Add(args[i] as ISlidePattern);
						}
						else if (args[i].IsException) {
							return args[i];
						}
						else {
							error = true;
							break;
						}
					}
					if (chains.Count > 0 && !error) {
						return caller.field.Cast<Global>().Pattern(caller, wait, step, chains.ToArray());
					}
				}
				return new ArgumentsNotMatchException(caller, Document.PATTERN, args);
			});
			upgrader.Add(Document.CHAIN, (caller, args) => {
				if (args.Length > 0) {
					ISlideWait wait = null;
					IStep step = null;
					var commands = new List<ISlideChain>();
					int index = 0;
					bool error = false;
					bool waitIsIStep = false;
					if (args[index].IsSlideWait) {
						wait = args[index] as ISlideWait;
						if (wait is IStep) {
							waitIsIStep = true;
						}
						index++;
					}
					if (args[index].IsStep) {
						step = args[index] as IStep;
						index++;
					}
					else if (waitIsIStep) {
						step = wait as IStep;
						wait = null;
					}
					for (int i = index; i < args.Length; i++) {
						if (args[i].IsSlideChain) {
							commands.Add(args[i] as ISlideChain);
						}
						else if (args[i].IsException) {
							return args[i];
						}
						else {
							error = true;
							break;
						}
					}
					if (commands.Count > 0 && !error) {
						return caller.field.Cast<Global>().Chain(caller, wait, step, commands.ToArray());
					}
				}
				return new ArgumentsNotMatchException(caller, Document.CHAIN, args);
			});
			upgrader.Add(Document.STRAIGHT, (caller, args) => {
				if (args.Length >= 2) {
					ISlideWait wait = null;
					IStep step = null;
					int index = 0;
					bool waitIsIStep = false;
					if (args[index].IsSlideWait) {
						wait = args[index] as ISlideWait;
						if (wait is IStep) {
							waitIsIStep = true;
						}
						index++;
					}
					if (args[index].IsStep) {
						step = args[index] as IStep;
						index++;
					}
					else if (waitIsIStep) {
						step = wait as IStep;
						wait = null;
					}
					if (args[index].type == Type.POSITION && args[index + 1].type == Type.POSITION) {
						return caller.field.Cast<Global>().Straight(caller, wait, step, args[index].Cast<Position>(), args[index + 1].Cast<Position>());
					}
				}
				return new ArgumentsNotMatchException(caller, Document.STRAIGHT, args);
			});
			upgrader.Add(Document.CURVE, (caller, args) => {
				if (args.Length >= 4) {
					ISlideWait wait = null;
					IStep step = null;
					int index = 0;
					bool waitIsIStep = false;
					if (args[index].IsSlideWait) {
						wait = args[index] as ISlideWait;
						if (wait is IStep) {
							waitIsIStep = true;
						}
						index++;
					}
					if (args[index].IsStep) {
						step = args[index] as IStep;
						index++;
					}
					else if (waitIsIStep) {
						step = wait as IStep;
						wait = null;
					}
					if (args[index].type == Type.POSITION && !args[index + 1].IsNaN && !args[index + 2].IsNaN && !args[index + 3].IsNaN) {
						return caller.field.Cast<Global>().Curve(caller, wait, step, args[index].Cast<Position>(), args[index + 1] as IMsqValue, args[index + 2] as IMsqValue, args[index + 3] as IMsqValue);
					}
				}
				return new ArgumentsNotMatchException(caller, Document.CURVE, args);
			});
			upgrader.Add(Document.CONTINUED_STRAIGHT, (caller, args) => {
				if (args.Length >= 1) {
					ISlideWait wait = null;
					IStep step = null;
					int index = 0;
					bool waitIsIStep = false;
					if (args[index].IsSlideWait) {
						wait = args[index] as ISlideWait;
						if (wait is IStep) {
							waitIsIStep = true;
						}
						index++;
					}
					if (args[index].IsStep) {
						step = args[index] as IStep;
						index++;
					}
					else if (waitIsIStep) {
						step = wait as IStep;
						wait = null;
					}
					if (args[index].type == Type.POSITION) {
						return caller.field.Cast<Global>().ContinuedStraight(caller, wait, step, args[index].Cast<Position>());
					}
				}
				return new ArgumentsNotMatchException(caller, Document.CONTINUED_STRAIGHT, args);
			});
			upgrader.Add(Document.CONTINUED_CURVE, (caller, args) => {
				if (args.Length >= 2) {
					ISlideWait wait = null;
					IStep step = null;
					int index = 0;
					bool waitIsIStep = false;
					if (args[index].IsSlideWait) {
						wait = args[index] as ISlideWait;
						if (wait is IStep) {
							waitIsIStep = true;
						}
						index++;
					}
					if (args[index].IsStep) {
						step = args[index] as IStep;
						index++;
					}
					else if (waitIsIStep) {
						step = wait as IStep;
						wait = null;
					}
					if (args[index].type == Type.POSITION && !args[index + 1].IsNaN) {
						return caller.field.Cast<Global>().ContinuedCurve(caller, wait, step, args[index].Cast<Position>(), args[index + 1] as IMsqValue);
					}
				}
				return new ArgumentsNotMatchException(caller, Document.CONTINUED_CURVE, args);
			});
			
			Func<CallerInfo, Object[], string, SlideArgs2NumbersEntity, Object> slideCommandMethodArgs2Numbers = (caller, args, commandName, entity) => {
				if (args.Length >= 2) {
					ISlideWait wait = null;
					IStep step = null;
					int index = 0;
					bool waitIsIStep = false;
					if (args[index].IsSlideWait) {
						wait = args[index] as ISlideWait;
						if (wait is IStep) {
							waitIsIStep = true;
						}
						index++;
					}
					if (args[index].IsStep) {
						step = args[index] as IStep;
						index++;
					}
					else if (waitIsIStep) {
						step = wait as IStep;
						wait = null;
					}
					if (args[index].type == Type.NUMBER && args[index + 1].type == Type.NUMBER) {
						return entity(caller, wait, step, args[index].Cast<Number>(), args[index + 1].Cast<Number>());
					}
				}
				return new ArgumentsNotMatchException(caller, commandName, args);
			};
			Func<CallerInfo, Object[], string, SlideArg1NumberEntity, Object> slideCommandMethodArgs1Number = (caller, args, commandName, entity) => {
				if (args.Length >= 1) {
					ISlideWait wait = null;
					IStep step = null;
					int index = 0;
					bool waitIsIStep = false;
					if (args[index].IsSlideWait) {
						wait = args[index] as ISlideWait;
						if (wait is IStep) {
							waitIsIStep = true;
						}
						index++;
					}
					if (args[index].IsStep) {
						step = args[index] as IStep;
						index++;
					}
					else if (waitIsIStep) {
						step = wait as IStep;
						wait = null;
					}
					if (args[index].type == Type.NUMBER) {
						return entity(caller, wait, step, args[index].Cast<Number>());
					}
				}
				return new ArgumentsNotMatchException(caller, commandName, args);
			};
			upgrader.Add(Document.OUTER_STRAIGHT_OUTER, (caller, args) => {
				return slideCommandMethodArgs2Numbers(caller, args, Document.OUTER_STRAIGHT_OUTER, caller.field.Cast<Global>().OuterStraightOuter);
			});
			upgrader.Add(Document.OUTER_STRAIGHT_INNER, (caller, args) => {
				return slideCommandMethodArgs2Numbers(caller, args, Document.OUTER_STRAIGHT_INNER, caller.field.Cast<Global>().OuterStraightInner);
			});
			upgrader.Add(Document.OUTER_STRAIGHT_CENTER, (caller, args) => {
				return slideCommandMethodArgs1Number(caller, args, Document.OUTER_STRAIGHT_CENTER, caller.field.Cast<Global>().OuterStraightCenter);
			});
			upgrader.Add(Document.OUTER_CURVE_CLOCKWISE_AXIS_CENTER, (caller, args) => {
				return slideCommandMethodArgs2Numbers(caller, args, Document.OUTER_CURVE_CLOCKWISE_AXIS_CENTER, caller.field.Cast<Global>().OuterCurveClockwiseOuterAxisCenter);
			});
			upgrader.Add(Document.OUTER_CURVE_COUNTERCLOCKWISE_AXIS_CENTER, (caller, args) => {
				return slideCommandMethodArgs2Numbers(caller, args, Document.OUTER_CURVE_COUNTERCLOCKWISE_AXIS_CENTER, caller.field.Cast<Global>().OuterCurveCounterClockwiseOuterAxisCenter);
			});
			upgrader.Add(Document.OUTER_CURVE_RIGHT_AXIS_CENTER, (caller, args) => {
				return slideCommandMethodArgs2Numbers(caller, args, Document.OUTER_CURVE_RIGHT_AXIS_CENTER, caller.field.Cast<Global>().OuterCurveRightOuterAxisCenter);
			});
			upgrader.Add(Document.OUTER_CURVE_LEFT_AXIS_CENTER, (caller, args) => {
				return slideCommandMethodArgs2Numbers(caller, args, Document.OUTER_CURVE_LEFT_AXIS_CENTER, caller.field.Cast<Global>().OuterCurveLeftOuterAxisCenter);
			});
			upgrader.Add(Document.INNER_STRAIGHT_OUTER, (caller, args) => {
				return slideCommandMethodArgs2Numbers(caller, args, Document.INNER_STRAIGHT_OUTER, caller.field.Cast<Global>().InnerStraightOuter);
			});
			upgrader.Add(Document.INNER_STRAIGHT_INNER, (caller, args) => {
				return slideCommandMethodArgs2Numbers(caller, args, Document.INNER_STRAIGHT_INNER, caller.field.Cast<Global>().InnerStraightInner);
			});
			upgrader.Add(Document.INNER_STRAIGHT_CENTER, (caller, args) => {
				return slideCommandMethodArgs1Number(caller, args, Document.INNER_STRAIGHT_CENTER, caller.field.Cast<Global>().InnerStraightCenter);
			});
			upgrader.Add(Document.INNER_CURVE_CLOCKWISE_AXIS_CENTER, (caller, args) => {
				return slideCommandMethodArgs2Numbers(caller, args, Document.INNER_CURVE_CLOCKWISE_AXIS_CENTER, caller.field.Cast<Global>().InnerCurveClockwiseInnerAxisCenter);
			});
			upgrader.Add(Document.INNER_CURVE_COUNTERCLOCKWISE_AXIS_CENTER, (caller, args) => {
				return slideCommandMethodArgs2Numbers(caller, args, Document.INNER_CURVE_COUNTERCLOCKWISE_AXIS_CENTER, caller.field.Cast<Global>().InnerCurveCounterClockwiseInnerAxisCenter);
			});
			upgrader.Add(Document.INNER_CURVE_RIGHT_AXIS_CENTER, (caller, args) => {
				return slideCommandMethodArgs2Numbers(caller, args, Document.INNER_CURVE_RIGHT_AXIS_CENTER, caller.field.Cast<Global>().InnerCurveRightInnerAxisCenter);
			});
			upgrader.Add(Document.INNER_CURVE_LEFT_AXIS_CENTER, (caller, args) => {
				return slideCommandMethodArgs2Numbers(caller, args, Document.INNER_CURVE_LEFT_AXIS_CENTER, caller.field.Cast<Global>().InnerCurveLeftInnerAxisCenter);
			});
			upgrader.Add(Document.CENTER_STRAIGHT_OUTER, (caller, args) => {
				return slideCommandMethodArgs1Number(caller, args, Document.CENTER_STRAIGHT_OUTER, caller.field.Cast<Global>().CenterStraightOuter);
			});
			upgrader.Add(Document.CENTER_STRAIGHT_INNER, (caller, args) => {
				return slideCommandMethodArgs1Number(caller, args, Document.CENTER_STRAIGHT_INNER, caller.field.Cast<Global>().CenterStraightInner);
			});
			upgrader.Add(Document.CENTER_STRAIGHT_CENTER, (caller, args) => {
				if (args.Length <= 2) {
					ISlideWait wait = null;
					IStep step = null;
					int index = 0;
					bool waitIsIStep = false;
					if (args[index].IsSlideWait) {
						wait = args[index] as ISlideWait;
						if (wait is IStep) {
							waitIsIStep = true;
						}
						index++;
					}
					if (args[index].IsStep) {
						step = args[index] as IStep;
						index++;
					}
					else if (waitIsIStep) {
						step = wait as IStep;
						wait = null;
					}
					return caller.field.Cast<Global>().CenterStraightCenter(caller, wait, step);
				}
				return new ArgumentsNotMatchException(caller, Document.CENTER_STRAIGHT_CENTER, args);
			});
			upgrader.Add(Document.SHAPE_P_AXIS_CENTER, (caller, args) => {
				return slideCommandMethodArgs2Numbers(caller, args, Document.SHAPE_P_AXIS_CENTER, caller.field.Cast<Global>().ShapePAxisCenter);
			});
			upgrader.Add(Document.SHAPE_Q_AXIS_CENTER, (caller, args) => {
				return slideCommandMethodArgs2Numbers(caller, args, Document.SHAPE_Q_AXIS_CENTER, caller.field.Cast<Global>().ShapeQAxisCenter);
			});
			upgrader.Add(Document.SHAPE_V_AXIS_CENTER, (caller, args) => {
				return slideCommandMethodArgs2Numbers(caller, args, Document.SHAPE_V_AXIS_CENTER, caller.field.Cast<Global>().ShapeVAxisCenter);
			});
			upgrader.Add(Document.SHAPE_S, (caller, args) => {
				return slideCommandMethodArgs2Numbers(caller, args, Document.SHAPE_S, caller.field.Cast<Global>().ShapeSAxisCenter);
			});
			upgrader.Add(Document.SHAPE_Z, (caller, args) => {
				return slideCommandMethodArgs2Numbers(caller, args, Document.SHAPE_Z, caller.field.Cast<Global>().ShapeZAxisCenter);
			});
			upgrader.Add(Document.SHAPE_PP, (caller, args) => {
				return slideCommandMethodArgs2Numbers(caller, args, Document.SHAPE_PP, caller.field.Cast<Global>().ShapePP);
			});
			upgrader.Add(Document.SHAPE_QQ, (caller, args) => {
				return slideCommandMethodArgs2Numbers(caller, args, Document.SHAPE_QQ, caller.field.Cast<Global>().ShapeQQ);
			});
			upgrader.Add(Document.SHAPE_V_AXIS_OUTER, (caller, args) => {
				if (args.Length >= 3) {
					ISlideWait wait = null;
					IStep step = null;
					int index = 0;
					bool waitIsIStep = false;
					if (args[index].IsSlideWait) {
						wait = args[index] as ISlideWait;
						if (wait is IStep) {
							waitIsIStep = true;
						}
						index++;
					}
					if (args[index].IsStep) {
						step = args[index] as IStep;
						index++;
					}
					else if (waitIsIStep) {
						step = wait as IStep;
						wait = null;
					}
					if (args[index].type == Type.NUMBER && args[index + 1].type == Type.NUMBER && args[index + 2].type == Type.NUMBER) {
						return caller.field.Cast<Global>().ShapeVAxisOuter(caller, wait, step, args[index].Cast<Number>(), args[index + 1].Cast<Number>(), args[index + 2].Cast<Number>());
					}
				}
				return new ArgumentsNotMatchException(caller, Document.SHAPE_V_AXIS_OUTER, args);
			});
			upgrader.Add(Document.SHAPE_W, (caller, args) => {
				return slideCommandMethodArgs2Numbers(caller, args, Document.SHAPE_W, caller.field.Cast<Global>().ShapeW);
			});

			Func<CallerInfo, Object[], string, SlideArgs2SensorsEntity, Object> slideCommandMethodArgs2Sensors = (caller, args, commandName, entity) => {
				if (args.Length >= 2) {
					ISlideWait wait = null;
					IStep step = null;
					int index = 0;
					bool waitIsIStep = false;
					if (args[index].IsSlideWait) {
						wait = args[index] as ISlideWait;
						if (wait is IStep) {
							waitIsIStep = true;
						}
						index++;
					}
					if (args[index].IsStep) {
						step = args[index] as IStep;
						index++;
					}
					else if (waitIsIStep) {
						step = wait as IStep;
						wait = null;
					}
					if (args[index].type == Type.SENSOR && args[index + 1].type == Type.SENSOR) {
						return entity(caller, wait, step, args[index].Cast<Sensor>(), args[index + 1].Cast<Sensor>());
					}
				}
				return new ArgumentsNotMatchException(caller, commandName, args);
			};
			upgrader.Add(Document.MAIPAD_SHAPE_STRAIGHT, (caller, args) => {
				return slideCommandMethodArgs2Sensors(caller, args, Document.MAIPAD_SHAPE_STRAIGHT, caller.field.Cast<Global>().ShapeMaipadDirectStraight);
			});
			upgrader.Add(Document.MAIPAD_SHAPE_CURVE, (caller, args) => {
				return slideCommandMethodArgs2Sensors(caller, args, Document.MAIPAD_SHAPE_CURVE, caller.field.Cast<Global>().ShapeMaipadDirectCurve);
			});
			upgrader.Add(Document.MAIPAD_SHAPE_CURVE_CLOCKWISE, (caller, args) => {
				return slideCommandMethodArgs2Sensors(caller, args, Document.MAIPAD_SHAPE_CURVE_CLOCKWISE, caller.field.Cast<Global>().ShapeMaipadDirectCurveClockwise);
			});
			upgrader.Add(Document.MAIPAD_SHAPE_CURVE_COUNTERCLOCKWISE, (caller, args) => {
				return slideCommandMethodArgs2Sensors(caller, args, Document.MAIPAD_SHAPE_CURVE_COUNTERCLOCKWISE, caller.field.Cast<Global>().ShapeMaipadDirectCurveCounterClockwise);
			});
			upgrader.Add(Document.MAIPAD_SHAPE_RIGHT, (caller, args) => {
				return slideCommandMethodArgs2Sensors(caller, args, Document.MAIPAD_SHAPE_RIGHT, caller.field.Cast<Global>().ShapeMaipadDirectCurveRight);
			});
			upgrader.Add(Document.MAIPAD_SHAPE_LEFT, (caller, args) => {
				return slideCommandMethodArgs2Sensors(caller, args, Document.MAIPAD_SHAPE_LEFT, caller.field.Cast<Global>().ShapeMaipadDirectCurveLeft);
			});
			upgrader.Add(Document.SIMAI_SHAPE_STRAIGHT, (caller, args) => {
				return slideCommandMethodArgs2Numbers(caller, args, Document.SIMAI_SHAPE_STRAIGHT, caller.field.Cast<Global>().OuterStraightOuter);
			});
			upgrader.Add(Document.SIMAI_SHAPE_CURVE, (caller, args) => {
				return slideCommandMethodArgs2Numbers(caller, args, Document.SIMAI_SHAPE_CURVE, caller.field.Cast<Global>().ShapeSimaiCurve);
			});
			upgrader.Add(Document.SIMAI_SHAPE_RIGHT, (caller, args) => {
				return slideCommandMethodArgs2Numbers(caller, args, Document.SIMAI_SHAPE_RIGHT, caller.field.Cast<Global>().OuterCurveRightOuterAxisCenter);
			});
			upgrader.Add(Document.SIMAI_SHAPE_LEFT, (caller, args) => {
				return slideCommandMethodArgs2Numbers(caller, args, Document.SIMAI_SHAPE_LEFT, caller.field.Cast<Global>().OuterCurveLeftOuterAxisCenter);
			});
			upgrader.Add(Document.SIMAI_SHAPE_P, (caller, args) => {
				return slideCommandMethodArgs2Numbers(caller, args, Document.SIMAI_SHAPE_P, caller.field.Cast<Global>().ShapePAxisCenter);
			});
			upgrader.Add(Document.SIMAI_SHAPE_Q, (caller, args) => {
				return slideCommandMethodArgs2Numbers(caller, args, Document.SIMAI_SHAPE_Q, caller.field.Cast<Global>().ShapeQAxisCenter);
			});
			upgrader.Add(Document.SIMAI_SHAPE_V, (caller, args) => {
				return slideCommandMethodArgs2Numbers(caller, args, Document.SIMAI_SHAPE_V, caller.field.Cast<Global>().ShapeVAxisCenter);
			});
			upgrader.Add(Document.SIMAI_SHAPE_S, (caller, args) => {
				return slideCommandMethodArgs2Numbers(caller, args, Document.SIMAI_SHAPE_S, caller.field.Cast<Global>().ShapeSAxisCenter);
			});
			upgrader.Add(Document.SIMAI_SHAPE_Z, (caller, args) => {
				return slideCommandMethodArgs2Numbers(caller, args, Document.SIMAI_SHAPE_Z, caller.field.Cast<Global>().ShapeZAxisCenter);
			});
			upgrader.Add(Document.SIMAI_SHAPE_PP, (caller, args) => {
				return slideCommandMethodArgs2Numbers(caller, args, Document.SIMAI_SHAPE_PP, caller.field.Cast<Global>().ShapePP);
			});
			upgrader.Add(Document.SIMAI_SHAPE_QQ, (caller, args) => {
				return slideCommandMethodArgs2Numbers(caller, args, Document.SIMAI_SHAPE_QQ, caller.field.Cast<Global>().ShapeQQ);
			});
			upgrader.Add(Document.SIMAI_SHAPE_W, (caller, args) => {
				return slideCommandMethodArgs2Numbers(caller, args, Document.SIMAI_SHAPE_W, caller.field.Cast<Global>().ShapeW);
			});
			
			Func<CallerInfo, Object[], string, int, Object> SimaiShapeVB = (caller, args, commandName, turningPoint) => {
				if (args.Length >= 2) {
					ISlideWait wait = null;
					IStep step = null;
					int index = 0;
					bool waitIsIStep = false;
					if (args[index].IsSlideWait) {
						wait = args[index] as ISlideWait;
						if (wait is IStep) {
							waitIsIStep = true;
						}
						index++;
					}
					if (args[index].IsStep) {
						step = args[index] as IStep;
						index++;
					}
					else if (waitIsIStep) {
						step = wait as IStep;
						wait = null;
					}
					if (args[index].type == Type.NUMBER && args[index + 1].type == Type.NUMBER) {
						return caller.field.Cast<Global>().ShapeVAxisOuter(caller, wait, step, args[index].Cast<Number>(), new Number(turningPoint), args[index + 1].Cast<Number>());
					}
				}
				return new ArgumentsNotMatchException(caller, commandName, args);
			};
			upgrader.Add(Document.SIMAI_SHAPE_V1, (caller, args) => {
				return SimaiShapeVB(caller, args, Document.SIMAI_SHAPE_V1, 1);
			});
			upgrader.Add(Document.SIMAI_SHAPE_V2, (caller, args) => {
				return SimaiShapeVB(caller, args, Document.SIMAI_SHAPE_V2, 2);
			});
			upgrader.Add(Document.SIMAI_SHAPE_V3, (caller, args) => {
				return SimaiShapeVB(caller, args, Document.SIMAI_SHAPE_V3, 3);
			});
			upgrader.Add(Document.SIMAI_SHAPE_V4, (caller, args) => {
				return SimaiShapeVB(caller, args, Document.SIMAI_SHAPE_V4, 4);
			});
			upgrader.Add(Document.SIMAI_SHAPE_V5, (caller, args) => {
				return SimaiShapeVB(caller, args, Document.SIMAI_SHAPE_V5, 5);
			});
			upgrader.Add(Document.SIMAI_SHAPE_V6, (caller, args) => {
				return SimaiShapeVB(caller, args, Document.SIMAI_SHAPE_V6, 6);
			});
			upgrader.Add(Document.SIMAI_SHAPE_V7, (caller, args) => {
				return SimaiShapeVB(caller, args, Document.SIMAI_SHAPE_V7, 7);
			});
			upgrader.Add(Document.SIMAI_SHAPE_V8, (caller, args) => {
				return SimaiShapeVB(caller, args, Document.SIMAI_SHAPE_V8, 8);
			});
			upgrader.Add(Document.CALL, (caller, args) => {
				if (args.Length > 0) {
					if (args[0].type == Type.FUNCTION) {
						var param = new Object[args.Length - 1];
						for (int i = 0; i < param.Length; i++) {
							param[i] = args[i + 1];
						}
						return caller.field.Cast<Global>().Call(caller, args[0].Cast<Function>(), param);
					}
					else {
						var param = new Object[args.Length - 1];
						for (int i = 0; i < param.Length; i++) {
							param[i] = args[i + 1];
						}
						if (args[0].type == Type.TEXT) {
							return caller.field.Cast<Global>().Call(caller, args[0].Cast<Text>(), param);
						}
						else if (args[0].type == Type.SYMBOL) {
							return caller.field.Cast<Global>().Call(caller, args[0].Cast<Symbol>(), param);
						}
						else if (args[0].type == Type.SENSOR) {
							return caller.field.Cast<Global>().Call(caller, args[0].Cast<Sensor>(), param);
						}
						else if (args[0].type == Type.FLAG) {
							return caller.field.Cast<Global>().Call(caller, args[0].Cast<Flag>(), param);
						}
						else if (args[0].type == Type.NUMBER) {
							return caller.field.Cast<Global>().Call(caller, args[0].Cast<Number>(), param);
						}
					}
				}
				return new ArgumentsNotMatchException(caller, Document.CALL, args);
			});
			upgrader.Add(Document.CALTOP, (caller, args) => {
				if (args.Length > 0) {
					var param = new Object[args.Length - 1];
					for (int i = 0; i < param.Length; i++) {
						param[i] = args[i + 1];
					}
					if (args[0].type == Type.TEXT) {
						return caller.field.Cast<Global>().TopCall(caller, args[0].Cast<Text>(), param);
					}
					else if (args[0].type == Type.SYMBOL) {
						return caller.field.Cast<Global>().TopCall(caller, args[0].Cast<Symbol>(), param);
					}
					else if (args[0].type == Type.SENSOR) {
						return caller.field.Cast<Global>().TopCall(caller, args[0].Cast<Sensor>(), param);
					}
					else if (args[0].type == Type.FLAG) {
						return caller.field.Cast<Global>().TopCall(caller, args[0].Cast<Flag>(), param);
					}
					else if (args[0].type == Type.NUMBER) {
						return caller.field.Cast<Global>().TopCall(caller, args[0].Cast<Number>(), param);
					}
				}
				return new ArgumentsNotMatchException(caller, Document.CALTOP, args);
			});
			upgrader.Add(Document.RETURN, (caller, args) => {
				if (args.Length == 1) {
					return caller.field.Cast<Global>().Return(caller, args[0]);
				}
				else if (args.Length >= 2) {
					return caller.field.Cast<Global>().Return(caller, args);
				}
				return new ArgumentsNotMatchException(caller, Document.RETURN, args);
			});
			upgrader.Add(Document.ARGUMENT, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.NUMBER) {
					return caller.field.Cast<Global>().Arg(caller, args[0].Cast<Number>());
				}
				// arg()だったら、ARGUMENTS_LENGTHを返す.
				else if (args.Length == 0) {
					return caller.field.Cast<Global>().Arg(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.ARGUMENT, args);
			});
			upgrader.Add(Document.ARGUMENTS_LENGTH, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().ArgLength(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.ARGUMENTS_LENGTH, args);
			});
			upgrader.Add(Document.ARRAY, (caller, args) => {
				return caller.field.Cast<Global>().Array(caller, args);
			});
			upgrader.Add(Document.TABLE, (caller, args) => {
				return caller.field.Cast<Global>().Table(caller);
			});
			upgrader.Add(Document.TURN, (caller, args) => {
				if (args.Length == 2 && 
				    args[0] is IButtonTurnable && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().Turn(caller, args[0] as IButtonTurnable, args[1].Cast<Number>());
				}
				else if (args.Length == 2 && 
				         args[0] is IFreeTurnable && !args[1].IsNaN) {
					return caller.field.Cast<Global>().Turn(caller, args[0] as IFreeTurnable, args[1] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.TURN, args);
			});
			upgrader.Add(Document.MIRROR_HOLIZONTAL, (caller, args) => {
				if (args.Length == 1 && 
				    args[0] is IMirrorable) {
					return caller.field.Cast<Global>().HMirror(caller, args[0] as IMirrorable);
				}
				return new ArgumentsNotMatchException(caller, Document.MIRROR_HOLIZONTAL, args);
			});
			upgrader.Add(Document.MIRROR_VERTICAL, (caller, args) => {
				if (args.Length == 1 && 
				    args[0] is IMirrorable) {
					return caller.field.Cast<Global>().VMirror(caller, args[0] as IMirrorable);
				}
				return new ArgumentsNotMatchException(caller, Document.MIRROR_VERTICAL, args);
			});
			upgrader.Add(Document.IF, (caller, args) => {
				if (args.Length == 3 && 
				    args[0].type == Type.FLAG && args[1].type == Type.FUNCTION && args[2].type == Type.FUNCTION) {
					return caller.field.Cast<Global>().If(caller, args[0].Cast<Flag>(), args[1].Cast<Function>(), args[2].Cast<Function>());
				}
				return new ArgumentsNotMatchException(caller, Document.IF, args);
			});
			upgrader.Add(Document.WHILE, (caller, args) => {
				if (args.Length == 2 && 
				    args[0].type == Type.FUNCTION && args[1].type == Type.FUNCTION) {
					return caller.field.Cast<Global>().While(caller, args[0].Cast<Function>(), args[1].Cast<Function>());
				}
				return new ArgumentsNotMatchException(caller, Document.WHILE, args);
			});
			upgrader.Add(Document.UNTIL, (caller, args) => {
				if (args.Length == 2 && 
				    args[0].type == Type.FUNCTION && args[1].type == Type.FUNCTION) {
					return caller.field.Cast<Global>().Until(caller, args[0].Cast<Function>(), args[1].Cast<Function>());
				}
				return new ArgumentsNotMatchException(caller, Document.UNTIL, args);
			});
			upgrader.Add(Document.DO_WHILE, (caller, args) => {
				if (args.Length == 2 && 
				    args[0].type == Type.FUNCTION && args[1].type == Type.FUNCTION) {
					return caller.field.Cast<Global>().DoWhile(caller, args[0].Cast<Function>(), args[1].Cast<Function>());
				}
				return new ArgumentsNotMatchException(caller, Document.DO_WHILE, args);
			});
			upgrader.Add(Document.DO_UNTIL, (caller, args) => {
				if (args.Length == 2 && 
				    args[0].type == Type.FUNCTION && args[1].type == Type.FUNCTION) {
					return caller.field.Cast<Global>().DoUntil(caller, args[0].Cast<Function>(), args[1].Cast<Function>());
				}
				return new ArgumentsNotMatchException(caller, Document.DO_UNTIL, args);
			});
			upgrader.Add(Document.FOR, (caller, args) => {
				if (args.Length == 2 && 
				    args[0].type == Type.NUMBER && args[1].type == Type.FUNCTION) {
					return caller.field.Cast<Global>().For(caller, args[0].Cast<Number>(), args[1].Cast<Function>());
				}
				else if (args.Length == 3 && 
				         args[0].type == Type.TEXT && args[1].type == Type.NUMBER && args[2].type == Type.FUNCTION) {
					return caller.field.Cast<Global>().For(caller, args[0].Cast<Text>(), args[1].Cast<Number>(), args[2].Cast<Function>());
				}
				else if (args.Length == 4 && 
				         args[0].type == Type.FUNCTION && args[1].type == Type.FUNCTION && args[2].type == Type.FUNCTION && args[3].type == Type.FUNCTION) {
					return caller.field.Cast<Global>().For(caller, args[0].Cast<Function>(), args[1].Cast<Function>(), args[2].Cast<Function>(), args[3].Cast<Function>());
				}
				return new ArgumentsNotMatchException(caller, Document.FOR, args);
			});
			
			// Number
			upgrader = commandList[Type.NUMBER] = new Dictionary<string, Func<CallerInfo, Object[], Object>>();
			upgrader.Add(Document.MATH_ADDITION, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.NUMBER) {
					return caller.field.Cast<Number>().Add(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.MATH_ADDITION, args);
			});
			upgrader.Add(Document.MATH_SUBTRACTION, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.NUMBER) {
					return caller.field.Cast<Number>().Sub(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.MATH_SUBTRACTION, args);
			});
			upgrader.Add(Document.MATH_MULTIPLICATION, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.NUMBER) {
					return caller.field.Cast<Number>().Mul(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.MATH_MULTIPLICATION, args);
			});
			upgrader.Add(Document.MATH_DIVISION, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.NUMBER) {
					return caller.field.Cast<Number>().Div(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.MATH_DIVISION, args);
			});
			upgrader.Add(Document.MATH_POWER, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.NUMBER) {
					return caller.field.Cast<Number>().Pow(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.MATH_POWER, args);
			});
			upgrader.Add(Document.MATH_MODULO, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.NUMBER) {
					return caller.field.Cast<Number>().Mod(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.MATH_MODULO, args);
			});
			upgrader.Add(Document.EQUAL, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.NUMBER) {
					return caller.field.Cast<Number>().Equal(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.EQUAL, args);
			});
			upgrader.Add(Document.NOT_EQUAL, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.NUMBER) {
					return caller.field.Cast<Number>().NotEqual(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.NOT_EQUAL, args);
			});
			upgrader.Add(Document.GREATER, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.NUMBER) {
					return caller.field.Cast<Number>().Greater(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.GREATER, args);
			});
			upgrader.Add(Document.LESS, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.NUMBER) {
					return caller.field.Cast<Number>().Less(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.LESS, args);
			});
			upgrader.Add(Document.GREATER_EQUAL, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.NUMBER) {
					return caller.field.Cast<Number>().GreaterEqual(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.GREATER_EQUAL, args);
			});
			upgrader.Add(Document.LESS_EQUAL, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.NUMBER) {
					return caller.field.Cast<Number>().LessEqual(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.LESS_EQUAL, args);
			});
			upgrader.Add(Document.INCREMENT, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Number>().Inc(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.INCREMENT, args);
			});
			upgrader.Add(Document.DECREMENT, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Number>().Dec(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.DECREMENT, args);
			});
			upgrader.Add(Document.BIT_AND, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.NUMBER) {
					return caller.field.Cast<Number>().BitAnd(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.BIT_AND, args);
			});
			upgrader.Add(Document.BIT_OR, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.NUMBER) {
					return caller.field.Cast<Number>().BitOr(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.BIT_OR, args);
			});
			upgrader.Add(Document.BIT_XOR, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.NUMBER) {
					return caller.field.Cast<Number>().BitXor(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.BIT_XOR, args);
			});
			upgrader.Add(Document.BIT_NOT, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Number>().BitNot(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.BIT_NOT, args);
			});
			upgrader.Add(Document.BIT_SHIFT_LEFT, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.NUMBER) {
					return caller.field.Cast<Number>().BitShiftLeft(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.BIT_SHIFT_LEFT, args);
			});
			upgrader.Add(Document.BIT_SHIFT_RIGHT, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.NUMBER) {
					return caller.field.Cast<Number>().BitShiftRight(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.BIT_SHIFT_RIGHT, args);
			});
			upgrader.Add(Document.TO_TEXT, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Number>().ToText(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.TO_TEXT, args);
			});
			upgrader.Add(Document.TO_VALUE, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Number>().ToValue(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.TO_VALUE, args);
			});
			upgrader.Add(Document.TURN, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.NUMBER) {
					return caller.field.Cast<Number>().ToButton(caller).Cast<Button>().Turn(caller, args[0].Cast<Number>()).Cast<Button>().ToNumber(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.TURN, args);
			});
			upgrader.Add(Document.MIRROR_HOLIZONTAL, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Number>().ToButton(caller).Cast<Button>().MirrorHolizontal(caller).Cast<Button>().ToNumber(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.MIRROR_HOLIZONTAL, args);
			});
			upgrader.Add(Document.MIRROR_VERTICAL, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Number>().ToButton(caller).Cast<Button>().MirrorVertical(caller).Cast<Button>().ToNumber(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.MIRROR_VERTICAL, args);
			});
			
			// Value
			upgrader = commandList[Type.VALUE] = new Dictionary<string, Func<CallerInfo, Object[], Object>>();
			upgrader.Add(Document.MATH_ADDITION, (caller, args) => {
				if (args.Length == 1 && !args[0].IsNaN) {
					return caller.field.Cast<Value>().Add(caller, (args[0] as IMsqValue));
				}
				return new ArgumentsNotMatchException(caller, Document.MATH_ADDITION, args);
			});
			upgrader.Add(Document.MATH_SUBTRACTION, (caller, args) => {
				if (args.Length == 1 && !args[0].IsNaN) {
					return caller.field.Cast<Value>().Sub(caller, (args[0] as IMsqValue));
				}
				return new ArgumentsNotMatchException(caller, Document.MATH_SUBTRACTION, args);
			});
			upgrader.Add(Document.MATH_MULTIPLICATION, (caller, args) => {
				if (args.Length == 1 && !args[0].IsNaN) {
					return caller.field.Cast<Value>().Mul(caller, (args[0] as IMsqValue));
				}
				return new ArgumentsNotMatchException(caller, Document.MATH_MULTIPLICATION, args);
			});
			upgrader.Add(Document.MATH_DIVISION, (caller, args) => {
				if (args.Length == 1 && !args[0].IsNaN) {
					return caller.field.Cast<Value>().Div(caller, (args[0] as IMsqValue));
				}
				return new ArgumentsNotMatchException(caller, Document.MATH_DIVISION, args);
			});
			upgrader.Add(Document.MATH_POWER, (caller, args) => {
				if (args.Length == 1 && !args[0].IsNaN) {
					return caller.field.Cast<Value>().Pow(caller, (args[0] as IMsqValue));
				}
				return new ArgumentsNotMatchException(caller, Document.MATH_POWER, args);
			});
			upgrader.Add(Document.EQUAL, (caller, args) => {
				if (args.Length == 1 && !args[0].IsNaN) {
					return caller.field.Cast<Value>().Equal(caller, (args[0] as IMsqValue));
				}
				return new ArgumentsNotMatchException(caller, Document.EQUAL, args);
			});
			upgrader.Add(Document.NOT_EQUAL, (caller, args) => {
				if (args.Length == 1 && !args[0].IsNaN) {
					return caller.field.Cast<Value>().NotEqual(caller, (args[0] as IMsqValue));
				}
				return new ArgumentsNotMatchException(caller, Document.NOT_EQUAL, args);
			});
			upgrader.Add(Document.GREATER, (caller, args) => {
				if (args.Length == 1 && !args[0].IsNaN) {
					return caller.field.Cast<Value>().Greater(caller, (args[0] as IMsqValue));
				}
				return new ArgumentsNotMatchException(caller, Document.GREATER, args);
			});
			upgrader.Add(Document.LESS, (caller, args) => {
				if (args.Length == 1 && !args[0].IsNaN) {
					return caller.field.Cast<Value>().Less(caller, (args[0] as IMsqValue));
				}
				return new ArgumentsNotMatchException(caller, Document.LESS, args);
			});
			upgrader.Add(Document.GREATER_EQUAL, (caller, args) => {
				if (args.Length == 1 && !args[0].IsNaN) {
					return caller.field.Cast<Value>().GreaterEqual(caller, (args[0] as IMsqValue));
				}
				return new ArgumentsNotMatchException(caller, Document.GREATER_EQUAL, args);
			});
			upgrader.Add(Document.LESS_EQUAL, (caller, args) => {
				if (args.Length == 1 && !args[0].IsNaN) {
					return caller.field.Cast<Value>().LessEqual(caller, (args[0] as IMsqValue));
				}
				return new ArgumentsNotMatchException(caller, Document.LESS_EQUAL, args);
			});
			upgrader.Add(Document.INCREMENT, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Value>().Inc(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.INCREMENT, args);
			});
			upgrader.Add(Document.DECREMENT, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Value>().Dec(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.DECREMENT, args);
			});
			upgrader.Add(Document.TO_TEXT, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Value>().ToText(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.TO_TEXT, args);
			});
			upgrader.Add(Document.TO_NUMBER, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Value>().ToNumber(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.TO_NUMBER, args);
			});
			
			// Flag
			upgrader = commandList[Type.FLAG] = new Dictionary<string, Func<CallerInfo, Object[], Object>>();
			upgrader.Add(Document.NOT, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Flag>().Not(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.NOT, args);
			});
			upgrader.Add(Document.TO_TEXT, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Flag>().ToText(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.TO_TEXT, args);
			});
			
			// Text
			upgrader = commandList[Type.TEXT] = new Dictionary<string, Func<CallerInfo, Object[], Object>>();
			upgrader.Add(Document.CONCATENATE, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.TEXT) {
					return caller.field.Cast<Text>().Concat(caller, args[0].Cast<Text>());
				}
				return new ArgumentsNotMatchException(caller, Document.CONCATENATE, args);
			});
			upgrader.Add(Document.MATH_ADDITION, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.TEXT) {
					return caller.field.Cast<Text>().Concat(caller, args[0].Cast<Text>());
				}
				return new ArgumentsNotMatchException(caller, Document.MATH_ADDITION, args);
			});
			upgrader.Add(Document.EQUAL, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.TEXT) {
					return caller.field.Cast<Text>().Equal(caller, args[0].Cast<Text>());
				}
				return new ArgumentsNotMatchException(caller, Document.EQUAL, args);
			});
			upgrader.Add(Document.NOT_EQUAL, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.TEXT) {
					return caller.field.Cast<Text>().NotEqual(caller, args[0].Cast<Text>());
				}
				return new ArgumentsNotMatchException(caller, Document.NOT_EQUAL, args);
			});
			upgrader.Add(Document.LENGTH, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Text>().Length(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.LENGTH, args);
			});
			upgrader.Add(Document.TO_TEXT, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Text>().ToText(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.TO_TEXT, args);
			});
			upgrader.Add(Document.TO_NUMBER, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Text>().ToNumber(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.TO_NUMBER, args);
			});
			upgrader.Add(Document.TO_VALUE, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Text>().ToValue(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.TO_VALUE, args);
			});
			upgrader.Add(Document.TO_FLAG, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Text>().ToFlag(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.TO_FLAG, args);
			});
			upgrader.Add(Document.TO_SENSOR, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Text>().ToSensor(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.TO_SENSOR, args);
			});
			upgrader.Add(Document.TO_SYMBOL, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Text>().ToSymbol(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.TO_SYMBOL, args);
			});
			
			// Sensor
			upgrader = commandList[Type.SENSOR] = new Dictionary<string, Func<CallerInfo, Object[], Object>>();
			upgrader.Add(Document.EQUAL, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.SENSOR) {
					return caller.field.Cast<Sensor>().Equal(caller, args[0].Cast<Sensor>());
				}
				return new ArgumentsNotMatchException(caller, Document.EQUAL, args);
			});
			upgrader.Add(Document.NOT_EQUAL, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.SENSOR) {
					return caller.field.Cast<Sensor>().NotEqual(caller, args[0].Cast<Sensor>());
				}
				return new ArgumentsNotMatchException(caller, Document.NOT_EQUAL, args);
			});
			upgrader.Add(Document.IS_OUTER, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Sensor>().IsOuter(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.IS_OUTER, args);
			});
			upgrader.Add(Document.IS_INNER, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Sensor>().IsInner(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.IS_INNER, args);
			});
			upgrader.Add(Document.IS_CENTER, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Sensor>().IsCenter(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.IS_CENTER, args);
			});
			upgrader.Add(Document.IS_1, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Sensor>().Is1(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.IS_1, args);
			});
			upgrader.Add(Document.IS_2, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Sensor>().Is2(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.IS_2, args);
			});
			upgrader.Add(Document.IS_3, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Sensor>().Is3(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.IS_3, args);
			});
			upgrader.Add(Document.IS_4, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Sensor>().Is4(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.IS_4, args);
			});
			upgrader.Add(Document.IS_5, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Sensor>().Is5(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.IS_5, args);
			});
			upgrader.Add(Document.IS_6, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Sensor>().Is6(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.IS_6, args);
			});
			upgrader.Add(Document.IS_7, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Sensor>().Is7(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.IS_7, args);
			});
			upgrader.Add(Document.IS_8, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Sensor>().Is8(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.IS_8, args);
			});
			upgrader.Add(Document.TO_TEXT, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Sensor>().ToText(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.TO_TEXT, args);
			});

			// Symbol
			upgrader = commandList[Type.SYMBOL] = new Dictionary<string, Func<CallerInfo, Object[], Object>>();
			upgrader.Add(Document.EQUAL, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.SYMBOL) {
					return caller.field.Cast<Symbol>().Equal(caller, args[0].Cast<Symbol>());
				}
				return new ArgumentsNotMatchException(caller, Document.EQUAL, args);
			});
			upgrader.Add(Document.NOT_EQUAL, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.SYMBOL) {
					return caller.field.Cast<Symbol>().NotEqual(caller, args[0].Cast<Symbol>());
				}
				return new ArgumentsNotMatchException(caller, Document.NOT_EQUAL, args);
			});
			upgrader.Add(Document.LENGTH, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Symbol>().Length(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.LENGTH, args);
			});
			upgrader.Add(Document.TO_TEXT, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Symbol>().ToText(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.TO_TEXT, args);
			});
			
			// Position
			upgrader = commandList[Type.POSITION] = new Dictionary<string, Func<CallerInfo, Object[], Object>>();
			upgrader.Add(Document.SET, (caller, args) => {
				if (args.Length == 2 && !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Position>().Set(caller, args[0] as IMsqValue, args[1] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.SET, args);
			});
			upgrader.Add(Document.SET_X, (caller, args) => {
				if (args.Length == 1 && !args[0].IsNaN) {
					return caller.field.Cast<Position>().SetX(caller, args[0] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.SET_X, args);
			});
			upgrader.Add(Document.SET_Y, (caller, args) => {
				if (args.Length == 1 && !args[0].IsNaN) {
					return caller.field.Cast<Position>().SetY(caller, args[0] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.SET_Y, args);
			});
			upgrader.Add(Document.GET_X, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Position>().GetX(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.GET_X, args);
			});
			upgrader.Add(Document.GET_Y, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Position>().GetY(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.GET_Y, args);
			});
			upgrader.Add(Document.EQUAL, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.POSITION) {
					return caller.field.Cast<Position>().Equal(caller, args[0].Cast<Position>());
				}
				return new ArgumentsNotMatchException(caller, Document.EQUAL, args);
			});
			upgrader.Add(Document.NOT_EQUAL, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.POSITION) {
					return caller.field.Cast<Position>().NotEqual(caller, args[0].Cast<Position>());
				}
				return new ArgumentsNotMatchException(caller, Document.NOT_EQUAL, args);
			});
			turnableUpgrade(upgrader);
			mirrorableUpgrade(upgrader);
			
			// Color
			upgrader = commandList[Type.COLOR] = new Dictionary<string, Func<CallerInfo, Object[], Object>>();
			upgrader.Add(Document.GET_R, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Color>().GetR(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.GET_R, args);
			});
			upgrader.Add(Document.GET_G, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Color>().GetG(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.GET_G, args);
			});
			upgrader.Add(Document.GET_B, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Color>().GetB(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.GET_B, args);
			});
			upgrader.Add(Document.GET_A, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Color>().GetA(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.GET_A, args);
			});
			upgrader.Add(Document.SET_R, (caller, args) => {
				if (args.Length == 1 && !args[0].IsNaN) {
					return caller.field.Cast<Color>().SetR(caller, args[0] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.SET_R, args);
			});
			upgrader.Add(Document.SET_G, (caller, args) => {
				if (args.Length == 1 && !args[0].IsNaN) {
					return caller.field.Cast<Color>().SetG(caller, args[0] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.SET_G, args);
			});
			upgrader.Add(Document.SET_B, (caller, args) => {
				if (args.Length == 1 && !args[0].IsNaN) {
					return caller.field.Cast<Color>().SetB(caller, args[0] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.SET_B, args);
			});
			upgrader.Add(Document.SET_A, (caller, args) => {
				if (args.Length == 1 && !args[0].IsNaN) {
					return caller.field.Cast<Color>().SetA(caller, args[0] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.SET_A, args);
			});
			upgrader.Add(Document.GET_R255, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Color>().GetR255(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.GET_R255, args);
			});
			upgrader.Add(Document.GET_G255, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Color>().GetG255(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.GET_G255, args);
			});
			upgrader.Add(Document.GET_B255, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Color>().GetB255(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.GET_B255, args);
			});
			upgrader.Add(Document.GET_A255, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Color>().GetA255(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.GET_A255, args);
			});
			upgrader.Add(Document.SET_R255, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.NUMBER) {
					return caller.field.Cast<Color>().SetR255(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.SET_R255, args);
			});
			upgrader.Add(Document.SET_G255, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.NUMBER) {
					return caller.field.Cast<Color>().SetG255(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.SET_G255, args);
			});
			upgrader.Add(Document.SET_B255, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.NUMBER) {
					return caller.field.Cast<Color>().SetB255(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.SET_B255, args);
			});
			upgrader.Add(Document.SET_A255, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.NUMBER) {
					return caller.field.Cast<Color>().SetA255(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.SET_A255, args);
			});

			// NoteTap
			upgrader = commandList[Type.NOTE_TAP] = new Dictionary<string, Func<CallerInfo, Object[], Object>>();
			iOptionbleUpgrade(upgrader);
			bTurnableUpgrade(upgrader);
			mirrorableUpgrade(upgrader);
			
			// NoteHold
			upgrader = commandList[Type.NOTE_HOLD] = new Dictionary<string, Func<CallerInfo, Object[], Object>>();
			iOptionbleUpgrade(upgrader);
			bTurnableUpgrade(upgrader);
			mirrorableUpgrade(upgrader);
			
			// NoteSlide
			upgrader = commandList[Type.NOTE_SLIDE] = new Dictionary<string, Func<CallerInfo, Object[], Object>>();
			iOptionbleUpgrade(upgrader);
			bTurnableUpgrade(upgrader);
			mirrorableUpgrade(upgrader);
			
			// NoteBreak
			upgrader = commandList[Type.NOTE_BREAK] = new Dictionary<string, Func<CallerInfo, Object[], Object>>();
			iOptionbleUpgrade(upgrader);
			bTurnableUpgrade(upgrader);
			mirrorableUpgrade(upgrader);
			
			// NoteMessage
			upgrader = commandList[Type.NOTE_MESSAGE] = new Dictionary<string, Func<CallerInfo, Object[], Object>>();
			iOptionbleUpgrade(upgrader);
			
			// NoteScrollMessage
			upgrader = commandList[Type.NOTE_SCROLL_MESSAGE] = new Dictionary<string, Func<CallerInfo, Object[], Object>>();
			iOptionbleUpgrade(upgrader);

			// NoteSoundMessage
			upgrader = commandList[Type.NOTE_SOUND_MESSAGE] = new Dictionary<string, Func<CallerInfo, Object[], Object>>();
			iOptionbleUpgrade(upgrader);
			
			// SlideHeadStar
			upgrader = commandList[Type.SLIDE_HEAD_STAR] = new Dictionary<string, Func<CallerInfo, Object[], Object>>();
			iOptionbleUpgrade(upgrader);
			bTurnableUpgrade(upgrader);
			mirrorableUpgrade(upgrader);
			
			// SlideHeadBreak
			upgrader = commandList[Type.SLIDE_HEAD_BREAK] = new Dictionary<string, Func<CallerInfo, Object[], Object>>();
			iOptionbleUpgrade(upgrader);
			bTurnableUpgrade(upgrader);
			mirrorableUpgrade(upgrader);
			
			// SlidePattern
			upgrader = commandList[Type.SLIDE_PATTERN] = new Dictionary<string, Func<CallerInfo, Object[], Object>>();
			iOptionbleUpgrade(upgrader);
			turnableUpgrade(upgrader);
			mirrorableUpgrade(upgrader);
			
			// SlideChain
			upgrader = commandList[Type.SLIDE_CHAIN] = new Dictionary<string, Func<CallerInfo, Object[], Object>>();
			iOptionbleUpgrade(upgrader);
			turnableUpgrade(upgrader);
			mirrorableUpgrade(upgrader);
			
			// SlideCommandStraight
			upgrader = commandList[Type.SLIDE_COMMAND_STRAIGHT] = new Dictionary<string, Func<CallerInfo, Object[], Object>>();
			iOptionbleUpgrade(upgrader);
			turnableUpgrade(upgrader);
			mirrorableUpgrade(upgrader);
			
			// SlideCommandCurve
			upgrader = commandList[Type.SLIDE_COMMAND_CURVE] = new Dictionary<string, Func<CallerInfo, Object[], Object>>();
			iOptionbleUpgrade(upgrader);
			turnableUpgrade(upgrader);
			mirrorableUpgrade(upgrader);
			
			// SlideCommandContinuedStraight
			upgrader = commandList[Type.SLIDE_COMMAND_CONTINUED_STRAIGHT] = new Dictionary<string, Func<CallerInfo, Object[], Object>>();
			iOptionbleUpgrade(upgrader);
			turnableUpgrade(upgrader);
			mirrorableUpgrade(upgrader);
			
			// SlideCommandContinuedCurve
			upgrader = commandList[Type.SLIDE_COMMAND_CONTINUED_CURVE] = new Dictionary<string, Func<CallerInfo, Object[], Object>>();
			iOptionbleUpgrade(upgrader);
			turnableUpgrade(upgrader);
			mirrorableUpgrade(upgrader);
			
			// Array
			upgrader = commandList[Type.ARRAY] = new Dictionary<string, Func<CallerInfo, Object[], Object>>();
			upgrader.Add(Document.GET, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.NUMBER) {
					return caller.field.Cast<Array>().Get(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.GET, args);
			});
			upgrader.Add(Document.SET, (caller, args) => {
				if (args.Length == 2 && args[0].type == Type.NUMBER) {
					return caller.field.Cast<Array>().Set(caller, args[0].Cast<Number>(), args[1]);
				}
				return new ArgumentsNotMatchException(caller, Document.SET, args);
			});
			upgrader.Add(Document.MATH_ADDITION, (caller, args) => {
				if (args.Length > 0) {
					return caller.field.Cast<Array>().Add(caller, args);
				}
				return new ArgumentsNotMatchException(caller, Document.MATH_ADDITION, args);
			});
			upgrader.Add(Document.ADD_RANGE, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.ARRAY) {
					var objects = args[0].Cast<Array>().entity.ToArray();
					return caller.field.Cast<Array>().Add(caller, objects);
				}
				else if (args.Length == 1 && args[0].type == Type.TABLE) {
					var objects = args[0].Cast<Table>().entity;
					var objects1 = new List<Object>();
					foreach (var key in objects.Keys) {
						objects1.Add(objects[key]);
					}
					return caller.field.Cast<Array>().Add(caller, objects1.ToArray());
				}
				return new ArgumentsNotMatchException(caller, Document.ADD_RANGE, args);
			});
			upgrader.Add(Document.INSERT, (caller, args) => {
				if (args.Length > 1 && args[0].type == Type.NUMBER) {
					var objects = new List<Object>();
					for (int i = 1; i < args.Length; i++) {
						objects.Add(args[i]);
					}
					return caller.field.Cast<Array>().Insert(caller, args[0].Cast<Number>(), objects.ToArray());
				}
				return new ArgumentsNotMatchException(caller, Document.INSERT, args);
			});
			upgrader.Add(Document.INSERT_RANGE, (caller, args) => {
				if (args.Length == 2 && args[0].type == Type.NUMBER && args[1].type == Type.ARRAY) {
					var objects = args[1].Cast<Array>().entity.ToArray();
					return caller.field.Cast<Array>().Insert(caller, args[0].Cast<Number>(), objects);
				}
				else if (args.Length == 2 && args[0].type == Type.NUMBER && args[1].type == Type.TABLE) {
					var objects = args[1].Cast<Table>().entity;
					var objects1 = new List<Object>();
					foreach (var key in objects.Keys) {
						objects1.Add(objects[key]);
					}
					return caller.field.Cast<Array>().Add(caller, objects1.ToArray());
				}
				return new ArgumentsNotMatchException(caller, Document.INSERT_RANGE, args);
			});
			upgrader.Add(Document.REMOVE, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.NUMBER) {
					return caller.field.Cast<Array>().Remove(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.REMOVE, args);
			});
			upgrader.Add(Document.LENGTH, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Array>().Length(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.LENGTH, args);
			});
			upgrader.Add(Document.IS_EMPTY, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Array>().IsEmpty(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.IS_EMPTY, args);
			});
			
			// Table
			upgrader = commandList[Type.TABLE] = new Dictionary<string, Func<CallerInfo, Object[], Object>>();
			upgrader.Add(Document.GET, (caller, args) => {
				if (args.Length == 1 && args[0].IsKey) {
					return caller.field.Cast<Table>().Get(caller, args[0] as IKey);
				}
				return new ArgumentsNotMatchException(caller, Document.GET, args);
			});
			upgrader.Add(Document.SET, (caller, args) => {
				if (args.Length == 2 && args[0].IsKey) {
					return caller.field.Cast<Table>().Set(caller, args[0] as IKey, args[1]);
				}
				return new ArgumentsNotMatchException(caller, Document.SET, args);
			});
			upgrader.Add(Document.REMOVE, (caller, args) => {
				if (args.Length == 1 && args[0].IsKey) {
					return caller.field.Cast<Table>().Remove(caller, args[0] as IKey);
				}
				return new ArgumentsNotMatchException(caller, Document.REMOVE, args);
			});
			{
				Func<CallerInfo, Object[], string, Object> hasKey = (caller, args, commandName) => {
					if (args.Length == 1 && args[0].IsKey) {
						return caller.field.Cast<Table>().HasKey(caller, args[0] as IKey);
					}
					return new ArgumentsNotMatchException(caller, commandName, args);
				};
				upgrader.Add(Document.HAS_KEY, (caller, args) => { return hasKey(caller, args, Document.HAS_KEY); });
				upgrader.Add(Document.GOT, (caller, args) => { return hasKey(caller, args, Document.GOT); });
			}
			upgrader.Add(Document.GET_KEYS, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Table>().GetKeys(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.GET_KEYS, args);
			});
			upgrader.Add(Document.LENGTH, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Table>().Length(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.LENGTH, args);
			});
			upgrader.Add(Document.IS_EMPTY, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Table>().IsEmpty(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.IS_EMPTY, args);
			});
			upgrader.Add(Document.MARGE, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.TABLE) {
					return caller.field.Cast<Table>().Marge(caller, args[0].Cast<Table>());
				}
				else if (args.Length == 2 && args[0].type == Type.TABLE && args[1].type == Type.FLAG) {
					return caller.field.Cast<Table>().Marge(caller, args[0].Cast<Table>(), args[1].Cast<Flag>());
				}
				return new ArgumentsNotMatchException(caller, Document.MARGE, args);
			});
			
			// Function
			upgrader = commandList[Type.FUNCTION] = new Dictionary<string, Func<CallerInfo, Object[], Object>>();
			upgrader.Add(Document.CALL, (caller, args) => {
				return caller.field.Cast<Function>().Call(caller, args);
			});

		}
	}
}