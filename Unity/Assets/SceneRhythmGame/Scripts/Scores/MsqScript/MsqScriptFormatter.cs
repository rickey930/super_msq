﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace XMsqScript {
	public class Formatter {
		public enum ReadResult {
			/// <summary>
			/// 変換成功.
			/// </summary>
			SUCCESS,
			/// <summary>
			/// 必須のコメント終了キーが見つからなかった.
			/// </summary>
			REQUIRED_COMMENT_END_SPECIAL_KEY_NOT_FOUND,
			/// <summary>
			/// 必須の文字列化終了キーが見つからなかった.
			/// </summary>
			REQUIRED_TEXT_END_SPECIAL_KEY_NOT_FOUND,
			/// <summary>
			/// スクリプト読み込みモード中に別のモードの終了キーが見つかった.
			/// </summary>
			FIND_END_SPECIAL_KEY_IN_READING_SCRIPT,
			/// <summary>
			/// 「()」の数が合わない.
			/// </summary>
			ERROR_ROUND_COUNT_NOT_MATCH,
			/// <summary>
			/// 「{}」の数が合わない.
			/// </summary>
			ERROR_CURLY_COUNT_NOT_MATCH,
			/// <summary>
			/// 「[]」の数が合わない.
			/// </summary>
			ERROR_SQUARE_COUNT_NOT_MATCH,
			/// <summary>
			/// 不適切な「:」
			/// </summary>
			ERROR_IMPROPER_COLON,
			/// <summary>
			/// 不適切な「;」
			/// </summary>
			ERROR_IMPROPER_SEMICOLON,
			/// <summary>
			/// 不適切な「,」
			/// </summary>
			ERROR_IMPROPER_CAMMA,
			/// <summary>
			/// 不適切な「.」
			/// </summary>
			ERROR_IMPROPER_PERIOD,
			/// <summary>
			/// コマンド名が異常
			/// </summary>
			ERROR_SYNTAX_NAME,
			/// <summary>
			/// { key:value }でvalueを宣言していない.
			/// </summary>
			ERROR_CURLY_VALUE_NOT_FOUND,
			/// <summary>
			/// スクリプトの内容が不完全.
			/// </summary>
			ERROR_INCOMPLETE_CONTENT,
		}
		
		private static class Document {
			public const string ROUND_START = "(";
			public const string ROUND_END = ")";
			public const string CURLY_START = "{";
			public const string CURLY_END = "}";
			public const string SQUARE_START = "[";
			public const string SQUARE_END = "]";
			public const string DOT = ".";
			public const string CAMMA = ",";
			public const string COLON = ":";
			public const string SEMICOLON = ";";
			public const string LOG_TEXT_START = "<\"";
			public const string LOG_TEXT_END = "\">";
			public const string LOG_SPACE = " ";
		}
		
		public class Room {
			public TextDetailInfo representative { get; private set; } // 代表
			public CommonScriptFormatter[] name { get; private set; } //ルーム名.
			public bool nameIsText { get; private set; }
			public RoomType type { get; private set; } // ルームを何で作るか.
			public Room[] children { get; private set; } // このルームのサブルーム。.
			public Expand expand { get; private set; } // このルームから拡張するルーム.
			
			public Room parent { get; private set; } // parent((this));
			public Expand expandSource { get; private set; } // (expandSource).(this)

			public Room Clone() {
				var ret = new Room();
				if (this.representative != null) {
					ret.representative = this.representative.Clone ();
				}
				if (this.name != null) {
					ret.name = new CommonScriptFormatter[this.name.Length];
					for (int i = 0; i < this.name.Length; i++) {
						ret.name [i] = this.name [i].Clone ();
					}
				}
				ret.nameIsText = this.nameIsText;
				ret.type = this.type;
				if (this.children != null) {
					ret.children = new Room[this.children.Length];
					for (int i = 0; i < this.children.Length; i++) {
						ret.children [i] = this.children [i].Clone ();
						ret.children [i].parent = ret;
					}
				}
				if (this.expand != null) {
					ret.expand = this.expand.Clone ();
					ret.expand.room.expandSource = ret.expand;
				}
				return ret;
			}
			
			private string _nameString;
			
			public enum RoomType {
				/// <summary>
				/// 括弧無し.
				/// </summary>
				VALUE,
				/// <summary>
				/// ().
				/// </summary>
				ROUND,
				/// <summary>
				/// {}.
				/// </summary>
				CURLY,
				/// <summary>
				/// [].
				/// </summary>
				SQUARE,
			}
			
			public string nameString {
				get {
					if (_nameString == null && name != null) {
						var builder = new System.Text.StringBuilder();
						foreach (var ch in name) {
							builder.Append(ch.GetString());
						}
						_nameString = builder.ToString();
					}
					return _nameString;
				}
			}
			
			public Room() {
				
			}
			
			public Room(RoomHelper helper, bool expandRetroactive = true) {
				
				// Analyzeが終わったばかりだとexpandの最奥がnowになっているので、expandの親に戻る.
				var source = helper;
				while (source != null && expandRetroactive) {
					var bsource = source.expandSource;
					if (bsource == null) {
						break;
					}
					source = bsource;
				}
				helper = source;
				
				representative = helper.representative;
				switch (helper.type) {
				case RoomHelper.RoomType.ROUND:
					type = RoomType.ROUND;
					break;
				case RoomHelper.RoomType.CURLY:
					type = RoomType.CURLY;
					break;
				case RoomHelper.RoomType.SQUARE:
					type = RoomType.SQUARE;
					break;
				default:
					type = RoomType.VALUE;
					break;
				}
				nameIsText = helper.nameIsText != 0;
				if (helper.name != null) {
					name = helper.name.ToArray();
				}
				else if (nameIsText) {
					name = new CommonScriptFormatter[0];
				}
				if (helper.children != null) {
					int length = helper.children.Count;
					children = new Room[length];
					for (int i = 0; i < length; i++) {
						children[i] = new Room(helper.children[i]);
						children[i].parent = this;
					}
				}
				if (helper.expand != null) {
					expand = new Expand(helper);
					expand.room.expandSource = expand;
				}
			}
			
			private static Dictionary<RoomType, string> _openAreaKeys;
			private static Dictionary<RoomType, string> _closeAreaKeys;
			private static Dictionary<Expand.Key, string> _expendKeys;
			public static Dictionary<RoomType, string> openAreaKeys {
				get {
					if (_openAreaKeys == null) {
						_openAreaKeys = new Dictionary<RoomType, string>() {
							{ RoomType.ROUND, Document.ROUND_START },
							{ RoomType.CURLY, Document.CURLY_START },
							{ RoomType.SQUARE, Document.SQUARE_START },
						};
					}
					return _openAreaKeys;
				}
			}
			public static Dictionary<RoomType, string> closeAreaKeys {
				get {
					if (_closeAreaKeys == null) {
						_closeAreaKeys = new Dictionary<RoomType, string>() {
							{ RoomType.ROUND, Document.ROUND_END },
							{ RoomType.CURLY, Document.CURLY_END },
							{ RoomType.SQUARE, Document.SQUARE_END },
						};
					}
					return _closeAreaKeys;
				}
			}
			public static Dictionary<Expand.Key, string> expendKeys {
				get {
					if (_expendKeys == null) {
						_expendKeys = new Dictionary<Expand.Key, string>() {
							{ Expand.Key.NOTHING, string.Empty },
							{ Expand.Key.DOT, Document.DOT },
							{ Expand.Key.COLON, Document.COLON },
						};
					}
					return _expendKeys;
				}
			}
			public override string ToString() {
				var builder = new System.Text.StringBuilder();
				if (nameIsText) {
					builder.Append(Document.LOG_TEXT_START);
				}
				builder.Append(nameString);
				if (nameIsText) {
					builder.Append(Document.LOG_TEXT_END);
				}
				if (openAreaKeys.ContainsKey(type)) {
					builder.Append(openAreaKeys[type]);
					if (type == RoomType.CURLY) {
						builder.Append(Document.LOG_SPACE);
					}
				}
				if (children != null) {
					for (int i = 0; i < children.Length; i++) {
						builder.Append(children[i].ToString());
						if (i < children.Length - 1) {
							builder.Append(Document.CAMMA);
							builder.Append(Document.LOG_SPACE);
						}
					}
				}
				if (closeAreaKeys.ContainsKey(type)) {
					if (type == RoomType.CURLY) {
						builder.Append(Document.LOG_SPACE);
					}
					builder.Append(closeAreaKeys[type]);
				}
				if (expand != null) {
					builder.Append(expendKeys[expand.key]);
					builder.Append(expand.room.ToString());
				}
				return builder.ToString();
			}
		}
		
		/// <summary>
		/// <para>ルーム拡張</para>
		/// <para>.に値するところのデータ</para>
		/// </summary>
		public class Expand {
			public Room room { get; private set; }
			public Key key { get; private set; }
			public Expand() {
				
			}
			public Expand(RoomHelper helper) {
				room = new Room(helper.expand, false);
				key = helper.expandKey;
			}
			public enum Key {
				/// <summary>
				/// 繋ぎ文字無し.
				/// </summary>
				NOTHING,
				/// <summary>
				/// .
				/// </summary>
				DOT,
				/// <summary>
				/// :
				/// </summary>
				COLON,
			}
			public Expand Clone() {
				var ret = new Expand();
				ret.room = this.room.Clone();
				ret.key = this.key;
				return ret;
			}
		}
		
		public class RoomHelper {
			public TextDetailInfo representative; // 代表
			public List<CommonScriptFormatter> name; //ルーム名.
			public int nameIsText; //ルーム名はテキストである.
			public RoomType type; // ルームを何で作るか.
			public List<RoomHelper> children; // このルームのサブルーム。.
			public RoomHelper expand; // このルームから拡張するルーム.
			public Expand.Key expandKey; // 拡張になんの文字を使ったか.
			
			public RoomHelper parent; // parent((this));
			public RoomHelper expandSource; // (expandSource).(this)
			
			public RoomHelper() {
				type = RoomType.VALUE;
			}
			
			public bool IsNamed {
				get {
					return name != null || nameIsText != 0;
				}
			}
			
			/// <summary>
			/// childrenがnullでなければtrue. 最初のChildがAddされるまではfalse
			/// </summary>
			public bool IsRoomed {
				get {
					return children != null;
				}
			}
			
			public bool IsExpanded {
				get {
					return expand != null;
				}
			}
			
			public bool IsDefined {
				get {
					return IsNamed || IsRoomed || IsExpanded;
				}
			}
			
			public void ReadName(CommonScriptFormatter script) {
				if (name == null) {
					name = new List<CommonScriptFormatter>();
				}
				if (script != null) {
					name.Add(script);
				}
			}
			
			public RoomHelper CreateChild(RoomHelper parent) {
				var child = new RoomHelper();
				child.parent = parent;
				return child;
			}
			
			// (
			public RoomHelper CreateChild() {
				return CreateChild(this);
			}
			
			// ,
			public RoomHelper CreateNext() {
				bool firstChild = parent.children == null;
				if (firstChild) {
					parent.children = new List<RoomHelper>();
				}
				if (IsDefined) {
					parent.children.Add(this);
				}
				return CreateChild(parent);
			}
			
			// )
			public RoomHelper ReturnParent() {
				bool firstChild = parent.children == null;
				if (firstChild) {
					parent.children = new List<RoomHelper>();
				}
				if (IsDefined) {
					parent.children.Add(this);
				}
				return parent;
			}
			
			// ),
			public RoomHelper ReturnParentAndCreateNext() {
				var p = ReturnParent ();
				bool firstChild = p.parent.children == null;
				if (firstChild) {
					p.parent.children = new List<RoomHelper>();
				}
				if (p.IsDefined) {
					p.parent.children.Add(p);
				}
				return p.CreateChild(p.parent);
			}
			
			public RoomHelper CreateExpand(Expand.Key key) {
				expandKey = key;
				expand = CreateChild(parent);
				expand.expandSource = this;
				return expand;
			}
			
			public RoomHelper CancelExpand() {
				expandSource.expand = null;
				return expandSource;
			}
			
			/// <summary>
			/// 拡張を遡って該当するキーが存在するかを探す.
			/// </summary>
			public bool FindExpandKey(Expand.Key key) {
				var source = this;
				while (source != null) {
					var bsource = source.expandSource;
					if (bsource == null) {
						break;
					}
					else {
						if (bsource.expandKey == key) {
							return true;
						}
					}
					source = bsource;
				}
				return false;
			}
			
			public static RoomHelper CreateField() {
				var global = new RoomHelper();
				global.type = RoomType.GLOBAL;
				var field = global.CreateChild();
				field.ReturnParent();
				return field;
			}
			
			public enum RoomType {
				/// <summary>
				/// 値.
				/// </summary>
				VALUE,
				/// <summary>
				/// ().
				/// </summary>
				ROUND,
				/// <summary>
				/// {}.
				/// </summary>
				CURLY,
				/// <summary>
				/// [].
				/// </summary>
				SQUARE,
				/// <summary>
				/// 特殊.区切りは,ではなく;になる.
				/// </summary>
				GLOBAL,
			}
			
			private static Dictionary<string, RoomType> _openAreaKeys;
			private static Dictionary<string, RoomType> _closeAreaKeys;
			private static Dictionary<string, ReadResult> _errorAreaKeys;
			public static Dictionary<string, RoomType> openAreaKeys {
				get {
					if (_openAreaKeys == null) {
						_openAreaKeys = new Dictionary<string, RoomType>() {
							{ Document.ROUND_START, RoomType.ROUND },
							{ Document.CURLY_START, RoomType.CURLY },
							{ Document.SQUARE_START, RoomType.SQUARE },
						};
					}
					return _openAreaKeys;
				}
			}
			public static Dictionary<string, RoomType> closeAreaKeys {
				get {
					if (_closeAreaKeys == null) {
						_closeAreaKeys = new Dictionary<string, RoomType>() {
							{ Document.ROUND_END, RoomType.ROUND },
							{ Document.CURLY_END, RoomType.CURLY },
							{ Document.SQUARE_END, RoomType.SQUARE },
						};
					}
					return _closeAreaKeys;
				}
			}
			public static Dictionary<string, ReadResult> errorAreaKeys {
				get {
					if (_errorAreaKeys == null) {
						_errorAreaKeys = new Dictionary<string, ReadResult>() {
							{ Document.ROUND_END, ReadResult.ERROR_ROUND_COUNT_NOT_MATCH },
							{ Document.CURLY_END, ReadResult.ERROR_CURLY_COUNT_NOT_MATCH },
							{ Document.SQUARE_END, ReadResult.ERROR_SQUARE_COUNT_NOT_MATCH },
						};
					}
					return _errorAreaKeys;
				}
			}
		}
		
		public class Result {
			public TextDetailInfo place { get; private set; }
			public ReadResult type { get; private set; }
			public Result(TextDetailInfo place, ReadResult type) {
				this.place = place;
				this.type = type;
			}
			public string Log() {
				return type.ToString() + (type == ReadResult.SUCCESS ? string.Empty : "\r\n" + TextDetailInfo.GetPlaceLog(place));
			}
		}
		
		public static Room[] Analyze(string text, CommonScriptFormatter.DefineSpecialText[] specialKeys, CommonScriptFormatter.DefineEscapeText[] escapeKey, string[] invalidTexts, out Result result) {
			CommonScriptFormatter.ReadResult analyzedResult;
			var analyzedText = CommonScriptFormatter.Analyze(text, specialKeys, escapeKey, invalidTexts, out analyzedResult);
			int textSize = analyzedText.Length;
			
			if (textSize > 0) {
				if (analyzedResult == CommonScriptFormatter.ReadResult.REQUIRED_COMMENT_END_SPECIAL_KEY_NOT_FOUND) {
					result = new Result(analyzedText[analyzedText.Length - 1].text[0], ReadResult.REQUIRED_COMMENT_END_SPECIAL_KEY_NOT_FOUND);
					return null;
				}
				if (analyzedResult == CommonScriptFormatter.ReadResult.REQUIRED_TEXT_END_SPECIAL_KEY_NOT_FOUND) {
					result = new Result(analyzedText[analyzedText.Length - 1].text[0], ReadResult.REQUIRED_TEXT_END_SPECIAL_KEY_NOT_FOUND);
					return null;
				}
				if (analyzedResult == CommonScriptFormatter.ReadResult.FIND_END_SPECIAL_KEY_IN_READING_SCRIPT) {
					result = new Result(analyzedText[analyzedText.Length - 1].text[0], ReadResult.FIND_END_SPECIAL_KEY_IN_READING_SCRIPT);
					return null;
				}
			}
			
			var now = RoomHelper.CreateField();
			var global = now.parent;
			// 解析する.
			AnalyzeRoutine(analyzedText, now, out result);
			// global領域の子供だけを返す.
			var ret = new Room[global.children.Count];
			for (int i = 0; i < ret.Length; i++) {
				ret[i] = new Room(global.children[i]);
			}
			return ret;
		}
		
		public static Room Analyze(CommonScriptFormatter[] analyzedText, RoomHelper topRoom, out Result result) {
			// 解析する.
			AnalyzeRoutine(analyzedText, topRoom, out result);
			return new Room(topRoom);
		}
		
		private static void AnalyzeRoutine(CommonScriptFormatter[] analyzedText, RoomHelper topRoom, out Result result) {
			var now = topRoom;
			int textSize = analyzedText.Length;
			bool? endmark = null;
			for (int i = 0; i < textSize; i++) {
				var entity = analyzedText[i];
				bool elseThrough = true;

				// コメントおよび無視文字でなければエンドマークをリセット.
				if (entity.type != CommonScriptFormatter.TextType.COMMENT && 
				    entity.type != CommonScriptFormatter.TextType.COMMENT_RECOGNIZED_KEY &&
				    entity.type != CommonScriptFormatter.TextType.IGNORE) {
					endmark = false;
				}

				if (entity.type == CommonScriptFormatter.TextType.SCRIPT) {
					string str = entity.GetString();
					// ( { [ である場合.
					if (RoomHelper.openAreaKeys.ContainsKey(str)) {
						// ルームを作った後にまたルームを作ろうとしていたら、拡張扱いとする.
						if (now.IsRoomed) {
							now = now.CreateExpand(Expand.Key.NOTHING);
						}
						// 現在の部屋タイプを決める.
						now.type = RoomHelper.openAreaKeys[str];
						// 匿名なら代表は括弧群にする.
						if (now.representative == null) {
							now.representative = entity.text[0];
						}
						// 子を作って子に移動
						now = now.CreateChild();
						elseThrough = false;
					}
					// ) } ] である場合.
					else if (RoomHelper.closeAreaKeys.ContainsKey(str)) {
						if (now.parent.type == RoomHelper.closeAreaKeys[str]) {
							if (now.IsDefined || now.parent.type == RoomHelper.RoomType.CURLY || now.parent.type == RoomHelper.RoomType.SQUARE || !now.parent.IsRoomed) {
								bool closeAndNext = false;
								if (i + 1 < textSize) {
									var nextEntity = analyzedText[i + 1];
									closeAndNext = nextEntity.type == CommonScriptFormatter.TextType.SCRIPT && 
										((now.parent.parent.type != RoomHelper.RoomType.GLOBAL && nextEntity.GetString() == Document.CAMMA) ||
										 (now.parent.parent.type == RoomHelper.RoomType.GLOBAL && nextEntity.GetString() == Document.SEMICOLON));
									if (nextEntity.type == CommonScriptFormatter.TextType.SCRIPT && now.parent.parent.type == RoomHelper.RoomType.GLOBAL && nextEntity.GetString() == Document.SEMICOLON) {
										endmark = true;
									}
								}
								if (now.parent.type != RoomHelper.RoomType.CURLY || !now.IsDefined || !now.parent.IsRoomed || now.FindExpandKey(Expand.Key.COLON)) {
									if (!closeAndNext) {
										now = now.ReturnParent();
									}
									// 括弧閉じ直後に,がある場合.
									else {
										i++;
										now = now.ReturnParentAndCreateNext();
									}
									elseThrough = false;
								}
								// {}のとき:がなかったらエラー.
								else {
									result = new Result(entity.text[0], ReadResult.ERROR_CURLY_VALUE_NOT_FOUND);
									return;
								}
							}
							// ()内の余分な,はエラー. []や{}には余分にあってもOK.
							else {
								result = new Result(entity.text[0], ReadResult.ERROR_IMPROPER_CAMMA);
								return;
							}
						}
						// )などが(などより多い.
						else {
							result = new Result(entity.text[0], RoomHelper.errorAreaKeys[str]);
							return;
						}
					}
					// , である場合.
					else if (str == Document.CAMMA) {
						// グローバル領域でなければ、区切りになる.
						if (now.parent.type != RoomHelper.RoomType.GLOBAL) {
							if (now.IsDefined) {
								// { key,のときはエラー
								if (now.parent.type == RoomHelper.RoomType.CURLY && !now.FindExpandKey(Expand.Key.COLON)) {
									result = new Result(entity.text[0], ReadResult.ERROR_CURLY_VALUE_NOT_FOUND);
									return;
								}
								// それ以外
								else {
									now = now.CreateNext();
									elseThrough = false;
								}
							}
							// 何も定義されてないのに区切られたらエラーになる
							else {
								result = new Result(entity.text[0], ReadResult.ERROR_IMPROPER_CAMMA);
								return;
							}
						}
						else {
							result = new Result(entity.text[0], ReadResult.ERROR_IMPROPER_CAMMA);
							return;
						}
					}
					// . である場合.
					else if (str == Document.DOT) {
						// 前後のテキストが数字だったら小数点扱いにするが、そうでなければexpandを作る.
						CommonScriptFormatter before = null;
						CommonScriptFormatter after = null;
						if (i > 0)
							before = analyzedText[i - 1];
						if (i + 1 < textSize)
							after = analyzedText[i + 1];
						int hoge;
						if (before == null || after == null || !int.TryParse(before.GetString(), out hoge) || !int.TryParse(after.GetString(), out hoge)) {
							if (now.IsDefined) {
								//名前も子供もいないとき、代表は.になる
								if (now.representative == null) {
									now.representative = entity.text[0];
								}
								now = now.CreateExpand(Expand.Key.DOT);
								elseThrough = false;
							}
							else {
								// 未定義は拡張できない
								result = new Result(entity.text[0], ReadResult.ERROR_IMPROPER_PERIOD);
								return;
							}
						}
					}
					// : である場合.
					else if (str == Document.COLON) {
						// 中身は定義済みで、まだ:で拡張したことがなければ.
						if (now.parent.type == RoomHelper.RoomType.CURLY && now.IsDefined && !now.FindExpandKey(Expand.Key.COLON)) {
							// :で拡張する.
							now = now.CreateExpand(Expand.Key.COLON);
							elseThrough = false;
						}
						else {
							result = new Result(entity.text[0], ReadResult.ERROR_IMPROPER_COLON);
							return;
						}
					}
					// ; である場合.
					else if (str == Document.SEMICOLON) {
						// グローバル領域だったら、,と同じ働きをする.
						// 未定義で;があっても特に問題はない.
						if (now.parent.type == RoomHelper.RoomType.GLOBAL) {
							if (now.IsDefined) {
								now = now.CreateNext();
							}
							// ただし定義.未定義;はエラー
							else if (now.expandSource != null) {
								now.CancelExpand();
								result = new Result(entity.text[0], ReadResult.ERROR_IMPROPER_SEMICOLON);
								return;
							}
							elseThrough = false;
							endmark = true;
						}
						else {
							result = new Result(entity.text[0], ReadResult.ERROR_IMPROPER_SEMICOLON);
							return;
						}
					}
				}
				if (elseThrough) {
					if (entity.type == CommonScriptFormatter.TextType.SCRIPT ||
					    entity.type == CommonScriptFormatter.TextType.TEXT) {
						// "n"ame みたいなのはエラー.
						if (now.nameIsText >= 2) {
							// 構文エラー.
							result = new Result(entity.text[0], ReadResult.ERROR_SYNTAX_NAME);
							return;
						}
						// ルームを作った後にまたルームを作ろうとしていたら、拡張扱いとする.
						if (now.IsRoomed) {
							now = now.CreateExpand(Expand.Key.NOTHING);
						}
						// 代表はnameの1文字目.
						if (now.representative == null) {
							now.representative = entity.text[0];
						}
						// 部屋の名前として読み込む.
						now.ReadName(entity);
					}
					else if (entity.type == CommonScriptFormatter.TextType.TEXT_RECOGNIZED_KEY) {
						if (now.nameIsText % 2 == 0) {
							// 文字列化キーだった場合、2度目だったらエラー.
							if (now.nameIsText > 0) {
								// 構文エラー.
								result = new Result(entity.text[0], ReadResult.ERROR_SYNTAX_NAME);
								return;
							}
							// 文字列化キーだった場合、それがコマンド名の始めにあるならば、コマンド名はテキストですフラグを建てる.
							else if (!now.IsNamed) {
								now.nameIsText++;
								// 代表がまだ決まっていなければそこを代表とする
								if (now.representative == null) {
									now.representative = entity.text[0];
								}
							}
							// 始めではないのなら構文エラー.
							else {
								// 構文エラー.
								result = new Result(entity.text[0], ReadResult.ERROR_SYNTAX_NAME);
								return;
							}
						}
						else {
							now.nameIsText++;
						}
					}
				}
			}

			// エンドマークがnullのままなら、1文字も認識されていない.
			// 1文字以上認識されていつつ、最後の文字が;でなければ不完全なスクリプトとしてエラーにする.
			// 1文字も無ければエラーにはならない.ただ単に中身がないだけ.
			if (endmark.HasValue && !endmark.Value) {
				result = new Result(now.parent.representative ?? now.representative, ReadResult.ERROR_INCOMPLETE_CONTENT);
				return;
			}
			result = new Result(null, ReadResult.SUCCESS);
		}

		/// <summary>
		/// ルーム情報からスクリプトを再構築する.
		/// </summary>
		public static string RebuildScript(Room[] rooms) {
			if (rooms != null) {
				var builder = new System.Text.StringBuilder();
				int len = rooms.Length;
				int finalIndex = len - 1;
				for (int i = 0; i < len; i++) {
					builder.Append(rooms[i].ToString());
					builder.Append(";");
					if (i < finalIndex) {
						builder.Append("\r\n");
					}
				}
				return builder.ToString();
			}
			return string.Empty;
		}

		/// <summary>
		/// ルーム情報からログ出力用文字列を取得する.
		/// </summary>
		public static string Log(Room[] rooms) {
			if (rooms != null) {
				var ret = RebuildScript(rooms);
				if (string.IsNullOrEmpty(ret)) {
					return "*EMPTY*";
				}
				return ret;
			}
			return "*NULL*";
		}
		
		
	}
}