﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// TextDetailInfoがスクリプト、コメント、テキスト、のどれに属するかを付与したデータ
/// </summary>
public class CommonScriptFormatter {
	public enum TextType {
		/// <summary>
		/// スクリプト
		/// </summary>
		SCRIPT,
		/// <summary>
		/// コメント
		/// </summary>
		COMMENT,
		/// <summary>
		/// テキスト
		/// </summary>
		TEXT,
		/// <summary>
		/// コメントとして認識させたキー
		/// </summary>
		COMMENT_RECOGNIZED_KEY,
		/// <summary>
		/// テキストとして認識させたキー
		/// </summary>
		TEXT_RECOGNIZED_KEY,
		/// <summary>
		/// 無効化した文字列
		/// </summary>
		IGNORE,
	}
	
	public enum ReadResult {
		/// <summary>
		/// 変換成功.
		/// </summary>
		SUCCESS,
		/// <summary>
		/// 必須のコメント終了キーが見つからなかった.
		/// </summary>
		REQUIRED_COMMENT_END_SPECIAL_KEY_NOT_FOUND,
		/// <summary>
		/// 必須の文字列化終了キーが見つからなかった.
		/// </summary>
		REQUIRED_TEXT_END_SPECIAL_KEY_NOT_FOUND,
		/// <summary>
		/// スクリプト読み込みモード中に別のモードの終了キーが見つかった.
		/// </summary>
		FIND_END_SPECIAL_KEY_IN_READING_SCRIPT,
	}
	
	public enum ReadMode {
		/// <summary>
		/// スクリプト
		/// </summary>
		SCRIPT,
		/// <summary>
		/// コメント
		/// </summary>
		COMMENT,
		/// <summary>
		/// テキスト
		/// </summary>
		TEXT,
		/// <summary>
		/// 挟みコメント
		/// </summary>
		INTERPOSE_COMMENT,
		/// <summary>
		/// 挟みテキスト
		/// </summary>
		INTERPOSE_TEXT,
		/// <summary>
		/// 終端不要コメント
		/// </summary>
		ENDABLE_COMMENT,
	}
	
	/// <summary>
	/// <para>startからendまでで囲んである文字列を特殊な文字列とする.</para>
	/// <para>modeで種類を指定する.</para>
	/// </summary>
	public class DefineSpecialText {
		public string start { get; private set; }
		public string end { get; private set; }
		public ReadMode mode { get; private set; }
		public DefineSpecialText(string start, string end, ReadMode mode) {
			this.start = start;
			this.end = end;
			this.mode = mode;
		}
		
		public enum ReadMode {
			/// <summary>
			/// コメント
			/// </summary>
			COMMENT,
			/// <summary>
			/// テキスト
			/// </summary>
			TEXT,
			/// <summary>
			/// 挟みコメント
			/// </summary>
			INTERPOSE_COMMENT,
			/// <summary>
			/// 挟みテキスト
			/// </summary>
			INTERPOSE_TEXT,
			/// <summary>
			/// 終端不要コメント
			/// </summary>
			ENDABLE_COMMENT,
		}
	}
	
	/// <summary>
	/// <para>keyを指定して、そのキーに出会ったら代替文字(replacement)に差し替える.</para>
	/// <para>テキスト読み込みモードの最中で反応する.</para>
	/// </summary>
	public class DefineEscapeText {
		public string key { get; private set; }
		public string replacement { get; private set; }
		public DefineEscapeText(string key, string replacement) {
			this.key = key;
			this.replacement = replacement;
		}
	}
	
	public TextDetailInfo[] text { get; private set; }
	public TextType type { get; private set; }
	
	public CommonScriptFormatter(TextDetailInfo[] text, TextType type) {
		this.text = text;
		this.type = type;
		this.textString = null;
	}

	public CommonScriptFormatter Clone() {
		var cText = new TextDetailInfo[text.Length];
		for (int i = 0; i < text.Length; i++) {
			cText[i] = text[i].Clone();
		}
		return new CommonScriptFormatter(cText, this.type);
	}
	
	private string textString;
	public string GetString() {
		if (textString == null) {
			var builder = new System.Text.StringBuilder();
			foreach (var ch in text) {
				builder.Append(ch.entity);
			}
			textString = builder.ToString();
		}
		return textString;
	}
	
	/// <summary>
	/// スクリプト文字列からCommonScriptFormatterインスタンスを作る
	/// </summary>
	/// <param name="text">スクリプト文字列</param>
	/// <param name="commentKeys">挟んでいる文字列をコメントとして認識させるキーペア</param>
	/// <param name="textKeys">挟んでいる文字列をテキストとして認識させるキーペア</param>
	/// <param name="invalidTexts">スクリプトとして認識させない文字列群</param>
	/// <returns></returns>
	public static CommonScriptFormatter[] Analyze(string text, DefineSpecialText[] specialkeys, DefineEscapeText[] escapeKeys, string[] invalidTexts, out ReadResult result) {
		var _ret = new List<CommonScriptFormatter>();
		var analyzedText = TextDetailInfo.Analyze(text);
		int textSize = analyzedText.Length;
		
		// Special Keyを文字の長い順に並べ替え
		var spkeysList = new List<DefineSpecialText>(specialkeys);
		spkeysList.Sort((a, b) => b.start.Length - a.start.Length);
		specialkeys = spkeysList.ToArray();
		
		const int SPECIAL_KEY_NOTHING_INDEX = -1;
		// 反応したspecialKeysのindex
		int startKeyIndex = SPECIAL_KEY_NOTHING_INDEX;
		// 読み込みモード
		ReadMode mode = ReadMode.SCRIPT;
		//挟みカウンタ
		int interposeHierarchy = 0;
		
		for (int i = 0; i < textSize; i++) {
			var entity = analyzedText[i];
			if (mode == ReadMode.SCRIPT) {
				// Special Keyを確認し、いずれかと一致するならモードを切り替える
				for (int j = 0; j < specialkeys.Length; j++) {
					var spKey = specialkeys[j];
					if (!string.IsNullOrEmpty(spKey.start) && AnalyzeHelper(i, spKey.start.Length, analyzedText) == spKey.start) {
						startKeyIndex = j;
						var items = new TextDetailInfo[spKey.start.Length];
						for (int k = 0; k < items.Length; k++) {
							items[k] = analyzedText[i + k];
						}
						var textType = spKey.mode.IsTextMode() ? TextType.TEXT_RECOGNIZED_KEY : TextType.COMMENT_RECOGNIZED_KEY;
						_ret.Add(new CommonScriptFormatter(items, textType));
						if (items.Length > 0) {
							i += items.Length - 1;
						}
						mode = spKey.mode.ModeChange();
						if (mode.IsInterposeMode()) {
							interposeHierarchy++;
						}
						break;
					}
				}
				// モードが切り替わらなければ
				if (mode == ReadMode.SCRIPT) {
					// Special KeyのEnd文字ならエラーとする.
					for (int j = 0; j < specialkeys.Length; j++) {
						var spEndKey = specialkeys[j].end;
						if (specialkeys[j].mode != DefineSpecialText.ReadMode.ENDABLE_COMMENT && !string.IsNullOrEmpty(spEndKey) && AnalyzeHelper(i, spEndKey.Length, analyzedText) == spEndKey) {
							result = ReadResult.FIND_END_SPECIAL_KEY_IN_READING_SCRIPT;
							return _ret.ToArray();
						}
					}
					// エスケープキーなら代替文字にする.
					var representative = analyzedText[i];
					bool escaped = false;
					for (int j = 0; j < escapeKeys.Length; j++) {
						var escapeKey = escapeKeys[j];
						if (!string.IsNullOrEmpty(escapeKey.key) && AnalyzeHelper(i, escapeKey.key.Length, analyzedText) == escapeKey.key) {
							var items = new TextDetailInfo[escapeKey.replacement.Length];
							for (int k = 0; k < items.Length; k++) {
								items[k] = new TextDetailInfo(escapeKey.replacement[k].ToString(), representative.index, representative.row, representative.column);
							}
							_ret.Add(new CommonScriptFormatter(items, TextType.SCRIPT));
							if (escapeKey.key.Length > 0) {
								i += escapeKey.key.Length - 1;
							}
							escaped = true;
							break;
						}
					}
					// エスケープキーでなければ.
					if (!escaped) {
						// 無効文字でなければScriptとして読み込む.
						int invalidIndex = SPECIAL_KEY_NOTHING_INDEX;
						if (invalidTexts != null) {
							for (int j = 0; j < invalidTexts.Length; j++) {
								if (!string.IsNullOrEmpty(invalidTexts[j]) && AnalyzeHelper(i, invalidTexts[j].Length, analyzedText) == invalidTexts[j]) {
									invalidIndex = j;
									break;
								}
							}
						}
						if (invalidIndex < 0) {
							_ret.Add(new CommonScriptFormatter(new TextDetailInfo[] { entity }, TextType.SCRIPT));
						}
						else {
							var items = new TextDetailInfo[invalidTexts[invalidIndex].Length];
							for (int k = 0; k < items.Length; k++) {
								items[k] = analyzedText[i + k];
							}
							_ret.Add(new CommonScriptFormatter(items, TextType.IGNORE));
							if (items.Length > 0) {
								i += items.Length - 1;
							}
						}
					}
				}
			}
			else {
				if (startKeyIndex < 0) {
					throw new System.Exception("start key indexが未登録.");
				}
				var spKey = specialkeys[startKeyIndex];
				// 挟みコメントモードのとき、切り替えた直接の原因のSpecialKeyのStart文字なら、Commentとして読みつつカウンタをインクリメント.
				if ((mode == ReadMode.INTERPOSE_COMMENT || mode == ReadMode.INTERPOSE_TEXT) &&
				    !string.IsNullOrEmpty(spKey.start) && AnalyzeHelper(i, spKey.start.Length, analyzedText) == spKey.start) {
					var items = new TextDetailInfo[spKey.start.Length];
					for (int k = 0; k < items.Length; k++) {
						items[k] = analyzedText[i + k];
					}
					if (mode == ReadMode.INTERPOSE_COMMENT) {
						_ret.Add(new CommonScriptFormatter(items, TextType.COMMENT));
					}
					else if (mode == ReadMode.INTERPOSE_TEXT) {
						_ret.Add(new CommonScriptFormatter(items, TextType.TEXT));
					}
					if (items.Length > 0) {
						i += items.Length - 1;
					}
					interposeHierarchy++;
				}
				// モードを切り替えた直接の原因のSpecialKeyのEnd文字なら、モードをScript読み込みに切り替える.
				else if (!string.IsNullOrEmpty(spKey.end) && AnalyzeHelper(i, spKey.end.Length, analyzedText) == spKey.end) {
					// 挟みモードならカウンタをデクリメント.
					if (mode.IsInterposeMode()) {
						interposeHierarchy--;
					}
					var items = new TextDetailInfo[spKey.end.Length];
					for (int k = 0; k < items.Length; k++) {
						items[k] = analyzedText[i + k];
					}
					if (interposeHierarchy == 0) {
						var textType = mode.IsTextMode() ? TextType.TEXT_RECOGNIZED_KEY : TextType.COMMENT_RECOGNIZED_KEY;
						_ret.Add(new CommonScriptFormatter(items, textType));
						startKeyIndex = SPECIAL_KEY_NOTHING_INDEX;
						mode = ReadMode.SCRIPT;
					}
					else {
						if (mode.IsCommentMode()) {
							_ret.Add(new CommonScriptFormatter(items, TextType.COMMENT));
						}
						else if (mode.IsTextMode()) {
							_ret.Add(new CommonScriptFormatter(items, TextType.TEXT));
						}
					}
					if (items.Length > 0) {
						i += items.Length - 1;
					}
				}
				else {
					if (mode.IsCommentMode()) {
						_ret.Add(new CommonScriptFormatter(new TextDetailInfo[] { entity }, TextType.COMMENT));
					}
					else if (mode.IsTextMode()) {
						// エスケープキーなら代替文字にする.
						var representative = analyzedText[i];
						bool escaped = false;
						for (int j = 0; j < escapeKeys.Length; j++) {
							var escapeKey = escapeKeys[j];
							if (!string.IsNullOrEmpty(escapeKey.key) && AnalyzeHelper(i, escapeKey.key.Length, analyzedText) == escapeKey.key) {
								var items = new TextDetailInfo[escapeKey.replacement.Length];
								for (int k = 0; k < items.Length; k++) {
									items[k] = new TextDetailInfo(escapeKey.replacement[k].ToString(), representative.index, representative.row, representative.column);
								}
								_ret.Add(new CommonScriptFormatter(items, TextType.TEXT));
								if (escapeKey.key.Length > 0) {
									i += escapeKey.key.Length - 1;
								}
								escaped = true;
								break;
							}
						}
						// エスケープキーでなければテキストとして読み込む.
						if (!escaped) {
							_ret.Add(new CommonScriptFormatter(new TextDetailInfo[] { entity }, TextType.TEXT));
						}
					}
				}
			}
		}
		// コメント終了キーが必須なのに見当たらなかった.
		if (mode == ReadMode.COMMENT || mode == ReadMode.INTERPOSE_COMMENT) {
			result = ReadResult.REQUIRED_COMMENT_END_SPECIAL_KEY_NOT_FOUND;
		}
		// 文字列化終了キーが必須なのに見当たらなかった.
		else if (mode == ReadMode.TEXT || mode == ReadMode.INTERPOSE_TEXT) {
			result = ReadResult.REQUIRED_TEXT_END_SPECIAL_KEY_NOT_FOUND;
		}
		// 成功.
		else {
			result = ReadResult.SUCCESS;
		}
		return _ret.ToArray();
	}
	
	private static string AnalyzeHelper(int index, int cmpTextLength, TextDetailInfo[] src) {
		// i+文字数がanalyzedText.Lengthより小さい
		if (index + cmpTextLength - 1 < src.Length) {
			string ret = string.Empty;
			for (int i = index; i < index + cmpTextLength; i++) {
				ret += src[i].entity;
			}
			return ret;
		}
		return string.Empty;
	}
}

public static class CommonScriptFormatterEx {
	public static CommonScriptFormatter.ReadMode ModeChange(this CommonScriptFormatter.DefineSpecialText.ReadMode mode) {
		switch (mode) {
		case CommonScriptFormatter.DefineSpecialText.ReadMode.COMMENT:
			return CommonScriptFormatter.ReadMode.COMMENT;
		case CommonScriptFormatter.DefineSpecialText.ReadMode.TEXT:
			return CommonScriptFormatter.ReadMode.TEXT;
		case CommonScriptFormatter.DefineSpecialText.ReadMode.INTERPOSE_COMMENT:
			return CommonScriptFormatter.ReadMode.INTERPOSE_COMMENT;
		case CommonScriptFormatter.DefineSpecialText.ReadMode.INTERPOSE_TEXT:
			return CommonScriptFormatter.ReadMode.INTERPOSE_TEXT;
		case CommonScriptFormatter.DefineSpecialText.ReadMode.ENDABLE_COMMENT:
			return CommonScriptFormatter.ReadMode.ENDABLE_COMMENT;
		}
		throw new System.Exception("SpecialKeyに未知のReadModeが設定されている.");
	}

	public static bool IsTextMode(this CommonScriptFormatter.DefineSpecialText.ReadMode mode) {
		switch (mode) {
		case CommonScriptFormatter.DefineSpecialText.ReadMode.TEXT:
		case CommonScriptFormatter.DefineSpecialText.ReadMode.INTERPOSE_TEXT:
			return true;
		}
		return false;
	}
	
	public static bool IsCommentMode(this CommonScriptFormatter.DefineSpecialText.ReadMode mode) {
		switch (mode) {
		case CommonScriptFormatter.DefineSpecialText.ReadMode.COMMENT:
		case CommonScriptFormatter.DefineSpecialText.ReadMode.INTERPOSE_COMMENT:
		case CommonScriptFormatter.DefineSpecialText.ReadMode.ENDABLE_COMMENT:
			return true;
		}
		return false;
	}
	
	public static bool IsInterposeMode(this CommonScriptFormatter.DefineSpecialText.ReadMode mode) {
		switch (mode) {
		case CommonScriptFormatter.DefineSpecialText.ReadMode.INTERPOSE_COMMENT:
		case CommonScriptFormatter.DefineSpecialText.ReadMode.INTERPOSE_TEXT:
			return true;
		}
		return false;
	}
	
	public static bool IsTextMode(this CommonScriptFormatter.ReadMode mode) {
		switch (mode) {
		case CommonScriptFormatter.ReadMode.TEXT:
		case CommonScriptFormatter.ReadMode.INTERPOSE_TEXT:
			return true;
		}
		return false;
	}
	
	public static bool IsCommentMode(this CommonScriptFormatter.ReadMode mode) {
		switch (mode) {
		case CommonScriptFormatter.ReadMode.COMMENT:
		case CommonScriptFormatter.ReadMode.INTERPOSE_COMMENT:
		case CommonScriptFormatter.ReadMode.ENDABLE_COMMENT:
			return true;
		}
		return false;
	}
	
	public static bool IsInterposeMode(this CommonScriptFormatter.ReadMode mode) {
		switch (mode) {
		case CommonScriptFormatter.ReadMode.INTERPOSE_COMMENT:
		case CommonScriptFormatter.ReadMode.INTERPOSE_TEXT:
			return true;
		}
		return false;
	}
	
	public static bool IsRecognizedKey(this CommonScriptFormatter.TextType type) {
		switch (type) {
		case CommonScriptFormatter.TextType.TEXT_RECOGNIZED_KEY:
		case CommonScriptFormatter.TextType.COMMENT_RECOGNIZED_KEY:
			return true;
		}
		return false;
	}
}