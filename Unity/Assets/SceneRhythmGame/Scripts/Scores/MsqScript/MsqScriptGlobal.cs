﻿using System.Collections.Generic;

namespace XMsqScript {
	public class Global : Object {
		public override Type type { get { return Type.GLOBAL; } }
		public override Object Clone() { return new Global(libraryLoader) { variant = variant }; }
		public override string Log() {
			return "MsqScript.Global()";
		}
		/// <summary>
		/// <para>グローバル領域の解析結果</para>
		/// <para>外部で、msqデータのfieldの情報を読み取って、ゲーム譜面化する</para>
		/// </summary>
		public List<Order> field { get; private set; }
		/// <summary>
		/// グローバル領域に保持されたキーバリューペア.
		/// </summary>
		public Dictionary<string, Object> variant { get; private set; }
		/// <summary>
		/// globalが所属する暗黙的なFunction.
		/// </summary>
		public Function executer { get; private set; }
		/// <summary>
		/// MsqFormatterの検証結果.
		/// </summary>
		public Formatter.Result roomFormatResult { get { return _roomFormatResult; } }
		private Formatter.Result _roomFormatResult;
		/// <summary>
		/// ライブラリローダー. 名前を渡すとスクリプトを返すようなメソッドを教えておく.
		/// </summary>
		private System.Func<string, string> libraryLoader;
		/// <summary>
		/// 文字列をコマンドとして認識するためのクラスインスタンス.
		/// </summary>
		private Command command;
		/// <summary>
		/// 名前と引数からコマンドを探して実行する.
		/// </summary>
		public Object ExecuteCommand(CallerInfo caller, string name, params Object[] arguments) {
			return command.ExecuteCommand(caller, name, arguments);
		}

		public Global(System.Func<string, string> libraryLoader) {
			field = new List<Order> ();
			command = new Command();
			this.libraryLoader = libraryLoader;
		}
		public void Recycle() {
			field.Clear ();
			if (variant != null) {
				variant.Clear ();
			}
		}

		public Object Parse(CallerInfo caller) {
			string s = caller.script.nameString;
			if (caller.script.nameIsText) {
				return new Text(s);
			}
			int number;
			if (int.TryParse(s, out number)) {
				return new Number(number);
			}
			float value;
			if (float.TryParse(s, out value)) {
				return new Value(value);
			}
			switch (s.ToLower()) {
			case Document.TRUE:
			case Document.YES:
			case Document.ON:
				return new Flag(true);
			case Document.FALSE:
			case Document.NO:
			case Document.OFF:
				return new Flag(false);
			}
			var sensorId = MaipadSensor.ToMaipadSensorId(s);
			if (sensorId != MaipadSensor.Id.UNKNOWN) {
				return new Sensor(sensorId);
			}
			return new Symbol(s);
		}

		/// <summary>
		/// msqスクリプトを解析する. 文字列を具体的なデータ型にする.
		/// </summary>
		public static Object Analyze(string script, System.Func<string, string> libraryLoader, out Formatter.Result roomFormatResult, out Order[] field) {
			var reader = new XMsqScript.Global(libraryLoader);
			var result = reader.Analyze (script);
			field = reader.field.ToArray();
			roomFormatResult = reader.roomFormatResult;
			return result;
		}
		/// <summary>
		/// msqスクリプトを解析する. 文字列を具体的なデータ型にする.
		/// </summary>
		public Object Analyze(string script) {
			var detail = Formatter.Analyze(script, //null, null, null, out result);
			new CommonScriptFormatter.DefineSpecialText[] {
				new CommonScriptFormatter.DefineSpecialText("/*", "*/", CommonScriptFormatter.DefineSpecialText.ReadMode.COMMENT),
				new CommonScriptFormatter.DefineSpecialText("//", "\n", CommonScriptFormatter.DefineSpecialText.ReadMode.ENDABLE_COMMENT),
				new CommonScriptFormatter.DefineSpecialText("/**", "**/", CommonScriptFormatter.DefineSpecialText.ReadMode.INTERPOSE_COMMENT),
				new CommonScriptFormatter.DefineSpecialText("\"", "\"", CommonScriptFormatter.DefineSpecialText.ReadMode.TEXT),
				new CommonScriptFormatter.DefineSpecialText("\'", "\'", CommonScriptFormatter.DefineSpecialText.ReadMode.TEXT),
				new CommonScriptFormatter.DefineSpecialText("<\"", "\">", CommonScriptFormatter.DefineSpecialText.ReadMode.INTERPOSE_TEXT),
			},
			new CommonScriptFormatter.DefineEscapeText[] {
				new CommonScriptFormatter.DefineEscapeText("\\\"", "\""),
				new CommonScriptFormatter.DefineEscapeText("\\\'", "\'"),
				new CommonScriptFormatter.DefineEscapeText("\\\\", "\\"),
				new CommonScriptFormatter.DefineEscapeText("\\<", "<"),
				new CommonScriptFormatter.DefineEscapeText("\\>", ">"),
				new CommonScriptFormatter.DefineEscapeText("\\n", "\n"),
				new CommonScriptFormatter.DefineEscapeText("\\r", "\r"),
				new CommonScriptFormatter.DefineEscapeText("\\t", "\t"),
				new CommonScriptFormatter.DefineEscapeText("\\f", "\f"),
				new CommonScriptFormatter.DefineEscapeText("\\a", "\a"),
				new CommonScriptFormatter.DefineEscapeText("\\v", "\v"),
			},
			new string[] { "\r", "\n", " ", "\t", "　" },
			out _roomFormatResult);
			return Analyze(detail);
		}
		/// <summary>
		/// 書式変換済みデータを具体的なデータ型にする.
		/// </summary>
		public Object Analyze(Formatter.Room[] rooms) {
			var caller = new CallerInfo(this);
			
			if (roomFormatResult != null && roomFormatResult.type != Formatter.ReadResult.SUCCESS) {
				// フォーマットエラーだった
				return new Exception(caller, ErrorCode.FORMAT_ERROR);
			}
			else if (rooms != null && rooms.Length == 0) {
				// データが存在しなかった
				return new Exception(caller, ErrorCode.EMPTY);
			}
			executer = new Function(rooms);
			variant = executer.variant; // Globalから簡単に変数群にアクセスできるように
			var analyzeResult = executer.Call(caller);

			// 例外なら例外情報を返す
			if (analyzeResult.IsException) {
				return analyzeResult;
			}
			// returnだったらreturnしたオブジェクトを返す
			else if (analyzeResult.type == Type.RETURN) {
				return analyzeResult.Cast<Return>().entity;
			}
			// それ以外なら成功したとみなしてSuccessを返す
			return new Success();
		}

		/// <summary>
		/// <para>ライブラリを読み込んで、読み込みに使ったグローバルを返す.</para>
		/// <para>libraryLoaderが設定されてなければNullReferenceExceptionが発生する.</para>
		/// <para>libraryLoaderがnullを返すならこのメソッドもnullを返す.</para>
		/// <para>ライブラリをAnalyzeした結果はresultに入る.</para>
		/// </summary>
		public Object LoadLibrary(string targetName, out Object result) {
			if (libraryLoader != null) {
				var script = libraryLoader(targetName);
				if (script != null) {
					var reader = new XMsqScript.Global(libraryLoader);
					result = reader.Analyze(script);
					return reader;
				}
				result = null;
				return null;
			}
			throw new System.NullReferenceException("MsqScriptGlobal.libraryLoaderを設定してください。");
		}

#region オーダー
		public Object Bpm(CallerInfo caller, IMsqValue bpm) {
			field.Add(new OrderBpm(bpm));
			return new Void();
		}
		public Object Beat(CallerInfo caller, IMsqValue beat) {
			field.Add(new OrderBeat(beat));
			return new Void();
		}
		public Object Beat(CallerInfo caller, IMsqValue beat, Number notePerBar) {
			field.Add(new OrderBeat(beat, notePerBar));
			return new Void();
		}
		public Object Interval(CallerInfo caller, IMsqValue interval) {
			field.Add(new OrderInterval(interval));
			return new Void();
		}
		public Object Interval(CallerInfo caller, IMsqValue interval, Number notePerBar) {
			field.Add(new OrderInterval(interval, notePerBar));
			return new Void();
		}
		public Object Note(CallerInfo caller, params INote[] notes) {
			field.Add(new OrderNotes(notes));
			return new Void();
		}
		public Object Rest(CallerInfo caller) {
			return Rest(caller, new Number(1));
		}
		public Object Rest(CallerInfo caller, Number amount) {
			field.Add(new OrderRest(amount));
			return new Void();
		}
		public Object ExtraFormat(CallerInfo caller, IKey formatName, Text script) {
			field.Add(new OrderExtraFormat(caller.place, formatName, script));
			return new Void();
		}
		public Object Library(CallerInfo caller, IKey targetName) {
			return new Library(caller.place, targetName);
		}
		public Object Include(CallerInfo caller, IKey targetName) {
			return new Include(caller.place, targetName);
		}
#endregion
		public Object Log(CallerInfo caller, Object target) {
			UnityEngine.Debug.Log(target.Log());
			return new Void();
		}
#region 変数系
		public Object Get(CallerInfo caller, IKey key) {
			return new VariantGet(key);
		}
		public Object Set(CallerInfo caller, IKey key, Object value) {
			return new VariantSet(key, value);
		}
		public Object Call(CallerInfo caller, IKey functionName, params Object[] arguments) {
			return new VariantCall(functionName, arguments);
		}
		public Object Got(CallerInfo caller, IKey key) {
			return new VariantGot(key);
		}
		public Object Remove(CallerInfo caller, IKey key) {
			return new VariantRemove(key);
		}
#endregion
#region トップ変数系
		public Object TopGet(CallerInfo caller, IKey key) {
			if (variant.ContainsKey(key.key)) {
				return variant[key.key];
			}
			return new Exception(caller, ErrorCode.UNDEFINED_VARIANT);
		}
		public Object TopSet(CallerInfo caller, IKey key, Object value) {
			variant[key.key] = value;
			return new Void();
		}
		public Object TopCall(CallerInfo caller, IKey functionName, params Object[] arguments) {
			var saved = TopGet(caller, functionName);
			if (saved.IsException)
				return saved;
			if (saved.type == Type.FUNCTION)
				return saved.Cast<Function>().Call(caller, arguments);
			return new Exception(caller, ErrorCode.TYPE_MISMATCH);
		}
		public Object TopGot(CallerInfo caller, IKey key) {
			return new Flag(variant.ContainsKey(key.key));
		}
		public Object TopRemove(CallerInfo caller, IKey key) {
			variant.Remove(key.key);
			return new Void();
		}
		/// <summary>
		/// <para>overwrite:false</para>
		/// <para>元にあるkeyのvalueを保持しながら、そのほかのデータは新しいデータのkeyのvalueで埋める</para>
		/// <para>overwrite:true</para>
		/// <para>元のデータは無視して、新しいデータで上書きする。元のデータにしかないキーはそのまま残る</para>
		/// </summary>
		public Object Marge(CallerInfo caller, Global instance, Flag overwrite) {
			foreach (var key in instance.variant.Keys) {
				if (overwrite.entity || !variant.ContainsKey(key)) {
					variant[key] = instance.variant[key];
				}
			}
			field.AddRange(instance.field);
			return this;
		}
		public Object Marge(CallerInfo caller, Global instance) {
			return Marge(caller, instance, new Flag(false));
		}
#endregion
#region 計算系
		public Object Add(CallerInfo caller, Number a0, Number a1) {
			return new Number(a0.entity + a1.entity);
		}
		public Object Add(CallerInfo caller, IMsqValue a0, IMsqValue a1) {
			return new Value(a0.value + a1.value);
		}
		public Object Sub(CallerInfo caller, Number a0, Number a1) {
			return new Number(a0.entity - a1.entity);
		}
		public Object Sub(CallerInfo caller, IMsqValue a0, IMsqValue a1) {
			return new Value(a0.value - a1.value);
		}
		public Object Mul(CallerInfo caller, Number a0, Number a1) {
			return new Number(a0.entity * a1.entity);
		}
		public Object Mul(CallerInfo caller, IMsqValue a0, IMsqValue a1) {
			return new Value(a0.value * a1.value);
		}
		public Object Div(CallerInfo caller, Number a0, Number a1) {
			if (a1.entity != 0) {
				return new Number(a0.entity / a1.entity);
			}
			return new Exception(caller, ErrorCode.DEVIDE_ZERO);
		}
		public Object Div(CallerInfo caller, IMsqValue a0, IMsqValue a1) {
			if (a1.value != 0) {
				return new Value(a0.value / a1.value);
			}
			return new Exception(caller, ErrorCode.DEVIDE_ZERO);
		}
		public Object Pow(CallerInfo caller, Number a0, Number a1) {
			return new Number((int)UnityEngine.Mathf.Pow(a0.entity, a1.entity));
		}
		public Object Pow(CallerInfo caller, IMsqValue a0, IMsqValue a1) {
			return new Value(UnityEngine.Mathf.Pow(a0.value, a1.value));
		}
		public Object Mod(CallerInfo caller, Number a0, Number a1) {
			if (a1.entity != 0) {
				return new Number(a0.entity % a1.entity);
			}
			return new Exception(caller, ErrorCode.DEVIDE_ZERO);
		}
#endregion
#region 比較系
		public Object Equal(CallerInfo caller, IMsqValue a0, IMsqValue a1) {
			return new Flag(a0.value == a1.value);
		}
		public Object NotEqual(CallerInfo caller, IMsqValue a0, IMsqValue a1) {
			return new Flag(a0.value != a1.value);
		}
		public Object Greater(CallerInfo caller, IMsqValue a0, IMsqValue a1) {
			return new Flag(a0.value > a1.value);
		}
		public Object Less(CallerInfo caller, IMsqValue a0, IMsqValue a1) {
			return new Flag(a0.value < a1.value);
		}
		public Object GreaterEqual(CallerInfo caller, IMsqValue a0, IMsqValue a1) {
			return new Flag(a0.value >= a1.value);
		}
		public Object LessEqual(CallerInfo caller, IMsqValue a0, IMsqValue a1) {
			return new Flag(a0.value <= a1.value);
		}
#endregion
#region ビット演算
		public Object BitAnd(CallerInfo caller, Number a0, Number a1) {
			return new Number(a0.entity & a1.entity);
		}
		public Object BitOr(CallerInfo caller, Number a0, Number a1) {
			return new Number(a0.entity | a1.entity);
		}
		public Object BitXor(CallerInfo caller, Number a0, Number a1) {
			return new Number(a0.entity ^ a1.entity);
		}
		public Object BitNot(CallerInfo caller, Number a0) {
			return new Number(~a0.entity);
		}
		public Object BitShiftLeft(CallerInfo caller, Number a0, Number a1) {
			return new Number(a0.entity << a1.entity);
		}
		public Object BitShiftRight(CallerInfo caller, Number a0, Number a1) {
			return new Number(a0.entity >> a1.entity);
		}
#endregion
		public Object Not(CallerInfo caller, Flag a0) {
			return new Flag(!a0.entity);
		}
		public Object Concat(CallerInfo caller, Text a0, Text a1) {
			return new Text(a0.entity + a1.entity);
		}
		public Object Equal(CallerInfo caller, Text a0, Text a1) {
			return new Flag(a0.entity == a1.entity);
		}
		public Object NotEqual(CallerInfo caller, Text a0, Text a1) {
			return new Flag(a0.entity != a1.entity);
		}
		public Object Pos(CallerInfo caller, IMsqValue a0, IMsqValue a1) {
			return new Position(a0, a1);
		}
		public Object Equal(CallerInfo caller, Position a0, Position a1) {
			return new Flag(a0.entity == a1.entity);
		}
		public Object NotEqual(CallerInfo caller, Position a0, Position a1) {
			return new Flag(a0.entity != a1.entity);
		}
#region 色系
		public Object RGB255(CallerInfo caller, Number r, Number g, Number b) {
			return new Color(r.entity, g.entity, b.entity, 255);
		}
		public Object ARGB255(CallerInfo caller, Number a, Number r, Number g, Number b) {
			return new Color(r.entity, g.entity, b.entity, a.entity);
		}
		public Object RGBA255(CallerInfo caller, Number r, Number g, Number b, Number a) {
			return new Color(r.entity, g.entity, b.entity, a.entity);
		}
		public Object RGB(CallerInfo caller, IMsqValue r, IMsqValue g, IMsqValue b) {
			return new Color(r.value, g.value, b.value, 1.0f);
		}
		public Object ARGB(CallerInfo caller, IMsqValue a, IMsqValue r, IMsqValue g, IMsqValue b) {
			return new Color(r.value, g.value, b.value, a.value);
		}
		public Object RGBA(CallerInfo caller, IMsqValue r, IMsqValue g, IMsqValue b, IMsqValue a) {
			return new Color(r.value, g.value, b.value, a.value);
		}
		public Object Black(CallerInfo caller) {
			return new Color(UnityEngine.Color.black);
		}
		public Object White(CallerInfo caller) {
			return new Color(UnityEngine.Color.white);
		}
		public Object Gray(CallerInfo caller) {
			return new Color(UnityEngine.Color.gray);
		}
		public Object Red(CallerInfo caller) {
			return new Color(UnityEngine.Color.red);
		}
		public Object Green(CallerInfo caller) {
			return new Color(UnityEngine.Color.green);
		}
		public Object Blue(CallerInfo caller) {
			return new Color(UnityEngine.Color.blue);
		}
		public Object Yellow(CallerInfo caller) {
			return new Color(UnityEngine.Color.yellow);
		}
		public Object Magenta(CallerInfo caller) {
			return new Color(UnityEngine.Color.magenta);
		}
		public Object Cyan(CallerInfo caller) {
			return new Color(UnityEngine.Color.cyan);
		}
#endregion
#region センサー番号から角度系を取得
		public Object SensorDegree(CallerInfo caller, Number buttonId) {
			var button = buttonId.ToButton(caller).Cast<Button>();
			return new Value(Constants.instance.GetPieceDegree (button.GetIndex()));
		}
		public Object SensorDegreeDistanceClockwise(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			var button1 = fromButtonId.ToButton(caller).Cast<Button>();
			var button2 = toButtonId.ToButton(caller).Cast<Button>();
			int startSensor = button1.GetIndex();
			int targetSensor = button2.GetIndex();
			if (startSensor > targetSensor) {
				float startDeg = Constants.instance.GetPieceDegree (startSensor);
				float targetDeg = Constants.instance.GetPieceDegree (targetSensor) + 360f;
				return new Value(targetDeg - startDeg);
			}
			else if (startSensor < targetSensor) {
				float startDeg = Constants.instance.GetPieceDegree (startSensor);
				float targetDeg = Constants.instance.GetPieceDegree (targetSensor);
				return new Value(targetDeg - startDeg);
			}
			return new Value(360.0f);
		}
		public Object SensorDegreeDistanceCounterClockwise(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			var button1 = fromButtonId.ToButton(caller).Cast<Button>();
			var button2 = toButtonId.ToButton(caller).Cast<Button>();
			int startSensor = button1.GetIndex();
			int targetSensor = button2.GetIndex();
			if (startSensor > targetSensor) {
				float startDeg = Constants.instance.GetPieceDegree (startSensor);
				float targetDeg = Constants.instance.GetPieceDegree (targetSensor);
				return new Value(-(startDeg - targetDeg));
			}
			else if (startSensor < targetSensor) {
				float startDeg = Constants.instance.GetPieceDegree (startSensor) + 360f;
				float targetDeg = Constants.instance.GetPieceDegree (targetSensor);
				return new Value(-(startDeg - targetDeg));
			}
			return new Value(-360.0f);
		}
		public Object SensorDegreeDistanceRight(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			var button1 = fromButtonId.ToButton(caller).Cast<Button>();
			if (button1.IsTopSide()) {
				return SensorDegreeDistanceClockwise(caller, fromButtonId, toButtonId);
			}
			return SensorDegreeDistanceCounterClockwise(caller, fromButtonId, toButtonId);
		}
		public Object SensorDegreeDistanceLeft(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			var button1 = fromButtonId.ToButton(caller).Cast<Button>();
			if (button1.IsTopSide()) {
				return SensorDegreeDistanceCounterClockwise(caller, fromButtonId, toButtonId);
			}
			return SensorDegreeDistanceClockwise(caller, fromButtonId, toButtonId);
		}
		public Object AngleX(CallerInfo caller, IMsqValue degree, IMsqValue radius) {
			var pos = Angle(caller, degree, radius).Cast<Position>();
			return new Value(pos.entity.x);
		}
		public Object AngleY(CallerInfo caller, IMsqValue degree, IMsqValue radius) {
			var pos = Angle(caller, degree, radius).Cast<Position>();
			return new Value(pos.entity.y);
		}
		public Object AngleX(CallerInfo caller, IMsqValue degree) {
			var pos = Angle(caller, degree).Cast<Position>();
			return new Value(pos.entity.x);
		}
		public Object AngleY(CallerInfo caller, IMsqValue degree) {
			var pos = Angle(caller, degree).Cast<Position>();
			return new Value(pos.entity.y);
		}
		public Object OuterSensorPositionX(CallerInfo caller, Number buttonId) {
			var pos = OuterSensorPosition(caller, buttonId).Cast<Position>();
			return new Value(pos.entity.x);
		}
		public Object OuterSensorPositionY(CallerInfo caller, Number buttonId) {
			var pos = OuterSensorPosition(caller, buttonId).Cast<Position>();
			return new Value(pos.entity.y);
		}
		public Object InnerSensorPositionX(CallerInfo caller, Number buttonId) {
			var pos = InnerSensorPosition(caller, buttonId).Cast<Position>();
			return new Value(pos.entity.x);
		}
		public Object InnerSensorPositionY(CallerInfo caller, Number buttonId) {
			var pos = InnerSensorPosition(caller, buttonId).Cast<Position>();
			return new Value(pos.entity.y);
		}
		public Object CenterSensorPositionX(CallerInfo caller) {
			var pos = CenterSensorPosition(caller).Cast<Position>();
			return new Value(pos.entity.x);
		}
		public Object CenterSensorPositionY(CallerInfo caller) {
			var pos = CenterSensorPosition(caller).Cast<Position>();
			return new Value(pos.entity.y);
		}
		public Object SensorPositionX(CallerInfo caller, Sensor sensor) {
			if (sensor.entity.IsOuter())
				return OuterSensorPositionX(caller, new Number(sensor.entity.ToInt().Value));
			if (sensor.entity.IsInner())
				return InnerSensorPositionX(caller, new Number(sensor.entity.ToInt().Value));
			return CenterSensorPositionX(caller);
		}
		public Object SensorPositionY(CallerInfo caller, Sensor sensor) {
			if (sensor.entity.IsOuter())
				return OuterSensorPositionY(caller, new Number(sensor.entity.ToInt().Value));
			if (sensor.entity.IsInner())
				return InnerSensorPositionY(caller, new Number(sensor.entity.ToInt().Value));
			return CenterSensorPositionY(caller);
		}
		public Object OuterSensorRadius(CallerInfo caller) {
			return new Value(1.0f);
		}
		public Object InnerSensorRadius (CallerInfo caller) {
			var clossExLines = CircleCalculator.LinesIntersect(
				CircleCalculator.PointOnCircle(UnityEngine.Vector2.zero, 1, Constants.instance.GetPieceDegree(0)),
				CircleCalculator.PointOnCircle(UnityEngine.Vector2.zero, 1, Constants.instance.GetPieceDegree(3)),
				CircleCalculator.PointOnCircle(UnityEngine.Vector2.zero, 1, Constants.instance.GetPieceDegree(6)),
				CircleCalculator.PointOnCircle(UnityEngine.Vector2.zero, 1, Constants.instance.GetPieceDegree(2))
				);
			var radius = CircleCalculator.PointToPointDistance(UnityEngine.Vector2.zero, clossExLines);
			return new Value(radius);
		}
		public Object CenterSensorRadius(CallerInfo caller) {
			return new Value(0.0f);
		}
		public Object SimaiPPTurnDegree(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			var start = fromButtonId.ToButton(caller).Cast<Button>();
			var target = toButtonId.ToButton(caller).Cast<Button>();
			int complementary1 = ((target.GetIndex() + 16 - (start.GetIndex() + 3)) % 8 - 7) * -1;
			
			int complementary2 = (complementary1 - 4) * -1;
			int complementary3;
			if (complementary2 == 0) {
				complementary3 = 0;
			}
			else {
				complementary3 = (int)(UnityEngine.Mathf.Pow (2, UnityEngine.Mathf.Abs (complementary2) - 1));
			}
			
			float ret = -270.0f - (22.5f * (0xf & complementary3) * (complementary2 < 0 ? 1 : -1)) - (45.0f / 8.0f * complementary1);
			
			return new Value(ret);
		}
		public Object SimaiQQTurnDegree(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			var start = fromButtonId.ToButton(caller).Cast<Button>();
			var target = toButtonId.ToButton(caller).Cast<Button>();
			int complementary1 = ((target.GetIndex() + 16 - (start.GetIndex() - 2)) % 8);
			
			int complementary2 = (complementary1 - 4) * -1;
			int complementary3;
			if (complementary2 == 0) {
				complementary3 = 0;
			}
			else {
				complementary3 = (int)(UnityEngine.Mathf.Pow (2, UnityEngine.Mathf.Abs (complementary2) - 1));
			}
			
			float ret = 270.0f - (22.5f * (0xf & complementary3) * (complementary2 < 0 ? -1 : 1)) + (45.0f / 8.0f * complementary1);
			
			return new Value(ret);
		}
#endregion
		public Object Angle(CallerInfo caller, IMsqValue degree, IMsqValue radius) {
			// 引数は右手座標だが、Unityでは回転が左手座標系だとか0度は(-1.0,0)であることだとかを考慮して、90度回したりy値をマイナスにしたりして加工する.
			return new Position(
				UnityEngine.Mathf.Sin(CircleCalculator.ToRadian(degree.value + 90)) * radius.value, 
				-UnityEngine.Mathf.Cos(CircleCalculator.ToRadian(degree.value + 270)) * radius.value
				);
		}
		public Object Angle(CallerInfo caller, IMsqValue degree) {
			return Angle(caller, degree, new Value(1.0f));
		}
#region センサー番号から位置を取得
		public Object OuterSensorPosition(CallerInfo caller, Number buttonId) {
			var button = buttonId.ToButton(caller).Cast<Button>();
			var pos = CircleCalculator.PointOnCircle(UnityEngine.Vector2.zero, 1, Constants.instance.GetPieceDegree(button.GetIndex()));
			return new Position(pos.x, -pos.y);
		}
		public Object InnerSensorPosition(CallerInfo caller, Number buttonId) {
			var button = buttonId.ToButton(caller).Cast<Button>();
			var radius = InnerSensorRadius(caller).Cast<Value>().entity;
			var pos = CircleCalculator.PointOnCircle(UnityEngine.Vector2.zero, radius, Constants.instance.GetPieceDegree(button.GetIndex()));
			return new Position(new UnityEngine.Vector2(pos.x, -pos.y));
		}
		public Object CenterSensorPosition(CallerInfo caller) {
			return new Position(UnityEngine.Vector2.zero);
		}
		public Object SensorPosition(CallerInfo caller, Sensor sensor) {
			if (sensor.entity.IsOuter())
				return OuterSensorPosition(caller, new Number(sensor.entity.ToInt().Value));
			if (sensor.entity.IsInner())
				return InnerSensorPosition(caller, new Number(sensor.entity.ToInt().Value));
			return CenterSensorPosition(caller);
		}
		/// <summary>
		/// ppスライドにおいて始点から直線を引く際に目指す位置.
		/// </summary>
		/// <param name="start">始点</param>
		public Object SimaiPPFirstStraightTargetPosition(CallerInfo caller, Number fromButtonId) {
			return Angle(caller, SensorDegree(caller, fromButtonId).Cast<Value>().Sub(caller, new Value(SimaiPPQQFirstStraightTargetPositionAdjustDegree)).Cast<Value>(), new Value(SimaiPPQQFirstStraightTargetPositionRadius));
		}
		/// <summary>
		/// qqスライドにおいて始点から直線を引く際に目指す位置.
		/// </summary>
		/// <param name="start">始点</param>
		public Object SimaiQQFirstStraightTargetPosition(CallerInfo caller, Number fromButtonId) {
			return Angle(caller, SensorDegree(caller, fromButtonId).Cast<Value>().Add(caller, new Value(SimaiPPQQFirstStraightTargetPositionAdjustDegree)).Cast<Value>(), new Value(SimaiPPQQFirstStraightTargetPositionRadius));
		}
		/// <summary>
		/// simai内部におけるppスライドやqqスライドの中心点のA～H。 □に8つの頂点を均等に並べたとき、1番が右上で、時計回りに番号が増える.
		/// </summary>
		/// <param name="button">始点</param>
		public Object SimaiPPQQCenterPosition(CallerInfo caller, Number buttonId) {
			var button = buttonId.ToButton(caller).Cast<Button>();
			var radius = 0.5f; //(205.5f - 140.0f) / 140.0f;
			var pos = CircleCalculator.PointOnCircle(UnityEngine.Vector2.zero, radius, 45.0f * button.GetId ());
			return new Position(pos.x, -pos.y);
		}
		/// <summary>
		/// ppスライドにおいて円を描く際に必要とする中心点.
		/// </summary>
		/// <param name="button">始点</param>
		public Object SimaiPPCenterPosition(CallerInfo caller, Number buttonId) {
			var button = buttonId.ToButton(caller).Cast<Button>();
			return SimaiPPQQCenterPosition(caller, new Number((button.GetIndex() + 1) % 8 + 1));
		}
		/// <summary>
		/// qqスライドにおいて円を描く際に必要とする中心点.
		/// </summary>
		/// <param name="button">始点</param>
		public Object SimaiQQCenterPosition(CallerInfo caller, Number buttonId) {
			var button = buttonId.ToButton(caller).Cast<Button>();
			return SimaiPPQQCenterPosition(caller, new Number((button.GetIndex() + 6) % 8 + 1));
		}
#endregion
#region ノート生成
		public Object Tap(CallerInfo caller, Number buttonId) {
			return new NoteTap(buttonId.entity);
		}
		public Object Hold(CallerInfo caller, Number buttonId, IStep step) {
			return new NoteHold(buttonId.entity, step);
		}
		public Object Slide(CallerInfo caller, ISlideHead head, ISlideWait wait, IStep step, params ISlidePattern[] patterns) {
			var ret = new NoteSlide(head, wait, step, patterns);
			if (!ret.IsException) {
				if (ret.IsRisky()) {
					if (ret.isMissingHeadOfFirstContinuedSlideCommand) {
						return new Exception(caller, ErrorCode.HEAD_OF_FIRST_CONTINUED_SLIDE_COMMAND_IS_MISSING);
					}
					else {
						return new Exception(caller, ErrorCode.LACK_OF_SLIDE_INFORMATION);
					}
				}
				return ret.trimmed;
			}
			return ret;
		}
		public Object Slide(CallerInfo caller, params ISlidePattern[] patterns) {
			return Slide(caller, (ISlideHead)null, null, null, patterns);
		}
		public Object Slide(CallerInfo caller, ISlideHead head, params ISlidePattern[] patterns) {
			return Slide(caller, head, null, null, patterns);
		}
		public Object Slide(CallerInfo caller, ISlideHead head, ISlideWait wait, params ISlidePattern[] patterns) {
			return Slide(caller, head, wait, null, patterns);
		}
		public Object Slide(CallerInfo caller, ISlideHead head, IStep step, params ISlidePattern[] patterns) {
			return Slide(caller, head, null, step, patterns);
		}
		public Object Slide(CallerInfo caller, ISlideWait wait, params ISlidePattern[] patterns) {
			return Slide(caller, (ISlideHead)null, wait, null, patterns);
		}
		public Object Slide(CallerInfo caller, ISlideWait wait, IStep step, params ISlidePattern[] patterns) {
			return Slide(caller, (ISlideHead)null, wait, step, patterns);
		}
		public Object Slide(CallerInfo caller, IStep step, params ISlidePattern[] patterns) {
			return Slide(caller, (ISlideHead)null, null, step, patterns);
		}
		public Object Slide(CallerInfo caller, INote note, ISlideWait wait, IStep step, params ISlidePattern[] patterns) {
			ISlideHead head = null;
			if (note.type == Type.NOTE_TAP) {
				var tap = note.Cast<NoteTap>();
				var star = new SlideHeadStar(tap.button.GetId());
				star.options = tap.options;
				head = star;
			}
			else if (note.type == Type.NOTE_BREAK) {
				var tap = note.Cast<NoteBreak>();
				var star = new SlideHeadBreak(tap.button.GetId());
				star.options = tap.options;
				head = star;
			}
			if (head != null) {
				return Slide (caller, head, wait, step, patterns);
			}
			return new Exception(caller, ErrorCode.INVALID_SLIDE_HEAD);
		}
		public Object Slide(CallerInfo caller, INote note, params ISlidePattern[] patterns) {
			return Slide (caller, note, null, null, patterns);
		}
		public Object Slide(CallerInfo caller, INote note, ISlideWait wait, params ISlidePattern[] patterns) {
			return Slide (caller, note, wait, null, patterns);
		}
		public Object Slide(CallerInfo caller, INote note, IStep step, params ISlidePattern[] patterns) {
			return Slide (caller, note, null, step, patterns);
		}
		public Object Break(CallerInfo caller, Number buttonId) {
			return new NoteBreak(buttonId.entity);
		}
		public Object Message(CallerInfo caller, Text message, Position position, IMsqValue scale, Color color, IStep step) {
			return new NoteMessage(message, position, scale, color, step);
		}
		public Object ScrollMessage(CallerInfo caller, Text message, IMsqValue y, IMsqValue scale, Color color, IStep step) {
			return new NoteScrollMessage(message, y, scale, color, step);
		}
		public Object SoundMessage(CallerInfo caller, Text soundId) {
			return new NoteSoundMessage(soundId);
		}
		public Object Trap(CallerInfo caller, Sensor sensor, IStep step) {
			return new NoteTrap(sensor, step);
		}
#endregion
#region Step生成
		public Object Step(CallerInfo caller, IMsqValue beat, IMsqValue length) {
			return new StepBasic(beat, length);
		}
		public Object Step(CallerInfo caller, IMsqValue interval) {
			return new StepInterval(interval);
		}
		public Object Step(CallerInfo caller, IMsqValue bpm, IMsqValue beat, IMsqValue length) {
			return new StepLocalBpm(bpm, beat, length);
		}
		public Object Steps(CallerInfo caller, params IStep[] steps) {
			return new StepMass(steps);
		}
#endregion
#region スライドヘッド
		public Object Star(CallerInfo caller, Number buttonId) {
			return new SlideHeadStar(buttonId);
		}
		public Object BreakStar(CallerInfo caller, Number buttonId) {
			return new SlideHeadBreak(buttonId);
		}
		public Object NoHead(CallerInfo caller) {
			return new SlideHeadNothing();
		}
#endregion
#region スライドwait
		public Object WaitDefault(CallerInfo caller) {
			return new SlideWaitDefault();
		}
		public Object WaitCustomBpm(CallerInfo caller, IMsqValue bpm) {
			return new SlideWaitLocalBpm(bpm);
		}
		public Object WaitCustomInterval(CallerInfo caller, IMsqValue interval) {
			return new SlideWaitInterval(interval);
		}
		public Object WaitCustomStep(CallerInfo caller, IMsqValue beat, IMsqValue length) {
			return new SlideWaitStepBasic(beat, length);
		}
		public Object WaitCustomStep(CallerInfo caller, IMsqValue bpm, IMsqValue beat, IMsqValue length) {
			return new SlideWaitStepLocalBpm(bpm, beat, length);
		}
		public Object Waits(CallerInfo caller, params ISlideWait[] waits) {
			return new SlideWaitMass(waits);
		}
#endregion
#region スライドの形
#region Pattern()
		public Object Pattern(CallerInfo caller, ISlideWait wait, IStep step, params ISlidePattern[] chains) {
			return new SlidePattern(wait, step, chains);
		}
		public Object Pattern(CallerInfo caller, ISlideWait wait, params ISlidePattern[] chains) {
			return Pattern(caller, wait, null, chains);
		}
		public Object Pattern(CallerInfo caller, IStep step, params ISlidePattern[] chains) {
			return Pattern(caller, null, step, chains);
		}
		public Object Pattern(CallerInfo caller, params ISlidePattern[] chains) {
			return Pattern(caller, null, null, chains);
		}
#endregion
#region Chain()
		public Object Chain(CallerInfo caller, ISlideWait wait, IStep step, params ISlideChain[] commands) {
			return new SlideChain(wait, step, commands);
		}
		public Object Chain(CallerInfo caller, ISlideWait wait, params ISlideChain[] commands) {
			return Chain(caller, wait, null, commands);
		}
		public Object Chain(CallerInfo caller, IStep step, params ISlideChain[] commands) {
			return Chain(caller, null, step, commands);
		}
		public Object Chain(CallerInfo caller, params ISlideChain[] commands) {
			return Chain(caller, null, null, commands);
		}
#endregion
#region Straight()
		public Object Straight(CallerInfo caller, ISlideWait wait, IStep step, Position start, Position target) {
			return new SlideCommandStraight(wait, step, start, target);
		}
		public Object Straight(CallerInfo caller, ISlideWait wait, Position start, Position target) {
			return Straight(caller, wait, null, start, target);
		}
		public Object Straight(CallerInfo caller, IStep step, Position start, Position target) {
			return Straight(caller, null, step, start, target);
		}
		public Object Straight(CallerInfo caller, Position start, Position target) {
			return Straight(caller, null, null, start, target);
		}
#endregion
#region Curve()
		public Object Curve(CallerInfo caller, ISlideWait wait, IStep step, Position center, IMsqValue radius, IMsqValue startDegree, IMsqValue distanceDegree) {
			return new SlideCommandCurve(wait, step, center, new Position(radius, radius), startDegree, distanceDegree);
		}
		public Object Curve(CallerInfo caller, ISlideWait wait, Position center, IMsqValue radius, IMsqValue startDegree, IMsqValue distanceDegree) {
			return Curve(caller, wait, null, center, radius, startDegree, distanceDegree);
		}
		public Object Curve(CallerInfo caller, IStep step, Position center, IMsqValue radius, IMsqValue startDegree, IMsqValue distanceDegree) {
			return Curve(caller, null, step, center, radius, startDegree, distanceDegree);
		}
		public Object Curve(CallerInfo caller, Position center, IMsqValue radius, IMsqValue startDegree, IMsqValue distanceDegree) {
			return Curve(caller, null, null, center, radius, startDegree, distanceDegree);
		}
#endregion
#region ContinuedStraight()
		public Object ContinuedStraight(CallerInfo caller, ISlideWait wait, IStep step, Position target) {
			return new SlideCommandContinuedStraight(wait, step, target);
		}
		public Object ContinuedStraight(CallerInfo caller, ISlideWait wait, Position target) {
			return ContinuedStraight(caller, wait, null, target);
		}
		public Object ContinuedStraight(CallerInfo caller, IStep step, Position target) {
			return ContinuedStraight(caller, null, step, target);
		}
		public Object ContinuedStraight(CallerInfo caller, Position target) {
			return ContinuedStraight(caller, null, null, target);
		}
#endregion
#region ContinuedCurve()
		public Object ContinuedCurve(CallerInfo caller, ISlideWait wait, IStep step, Position center, IMsqValue distanceDegree) {
			return new SlideCommandContinuedCurve(wait, step, center, distanceDegree);
		}
		public Object ContinuedCurve(CallerInfo caller, ISlideWait wait, Position center, IMsqValue distanceDegree) {
			return ContinuedCurve(caller, wait, null, center, distanceDegree);
		}
		public Object ContinuedCurve(CallerInfo caller, IStep step, Position center, IMsqValue distanceDegree) {
			return ContinuedCurve(caller, null, step, center, distanceDegree);
		}
		public Object ContinuedCurve(CallerInfo caller, Position center, IMsqValue distanceDegree) {
			return ContinuedCurve(caller, null, null, center, distanceDegree);
		}
#endregion
#endregion
#region ボタン番号からスライドの形を作成
#region OuterStraightOuter()
		public Object OuterStraightOuter(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId, Number toButtonId) {
			return new SlideCommandStraight(wait, step,
				OuterSensorPosition(caller, fromButtonId).Cast<Position>(),
				OuterSensorPosition(caller, toButtonId).Cast<Position>()
			);
		}
		public Object OuterStraightOuter(CallerInfo caller, ISlideWait wait, Number fromButtonId, Number toButtonId) {
			return OuterStraightOuter(caller, wait, null, fromButtonId, toButtonId);
		}
		public Object OuterStraightOuter(CallerInfo caller, IStep step, Number fromButtonId, Number toButtonId) {
			return OuterStraightOuter(caller, null, step, fromButtonId, toButtonId);
		}
		public Object OuterStraightOuter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return OuterStraightOuter(caller, null, null, fromButtonId, toButtonId);
		}
#endregion
#region OuterStraightInner()
		public Object OuterStraightInner(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId, Number toButtonId) {
			return new SlideCommandStraight(wait, step,
				OuterSensorPosition(caller, fromButtonId).Cast<Position>(),
				InnerSensorPosition(caller, toButtonId).Cast<Position>()
			);
		}
		public Object OuterStraightInner(CallerInfo caller, ISlideWait wait, Number fromButtonId, Number toButtonId) {
			return OuterStraightInner(caller, wait, null, fromButtonId, toButtonId);
		}
		public Object OuterStraightInner(CallerInfo caller, IStep step, Number fromButtonId, Number toButtonId) {
			return OuterStraightInner(caller, null, step, fromButtonId, toButtonId);
		}
		public Object OuterStraightInner(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return OuterStraightInner(caller, null, null, fromButtonId, toButtonId);
		}
#endregion
#region OuterStraightCenter()
		public Object OuterStraightCenter(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId) {
			return new SlideCommandStraight(wait, step,
				OuterSensorPosition(caller, fromButtonId).Cast<Position>(),
				CenterSensorPosition(caller).Cast<Position>()
			);
		}
		public Object OuterStraightCenter(CallerInfo caller, ISlideWait wait, Number fromButtonId) {
			return OuterStraightCenter(caller, wait, null, fromButtonId);
		}
		public Object OuterStraightCenter(CallerInfo caller, IStep step, Number fromButtonId) {
			return OuterStraightCenter(caller, null, step, fromButtonId);
		}
		public Object OuterStraightCenter(CallerInfo caller, Number fromButtonId) {
			return OuterStraightCenter(caller, null, null, fromButtonId);
		}
#endregion
#region InnerStraightOuter()
		public Object InnerStraightOuter(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId, Number toButtonId) {
			return new SlideCommandStraight(wait, step,
				InnerSensorPosition(caller, fromButtonId).Cast<Position>(),
				OuterSensorPosition(caller, toButtonId).Cast<Position>()
			);
		}
		public Object InnerStraightOuter(CallerInfo caller, ISlideWait wait, Number fromButtonId, Number toButtonId) {
			return InnerStraightOuter(caller, wait, null, fromButtonId, toButtonId);
		}
		public Object InnerStraightOuter(CallerInfo caller, IStep step, Number fromButtonId, Number toButtonId) {
			return InnerStraightOuter(caller, null, step, fromButtonId, toButtonId);
		}
		public Object InnerStraightOuter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return InnerStraightOuter(caller, null, null, fromButtonId, toButtonId);
		}
#endregion
#region InnerStraightInner()
		public Object InnerStraightInner(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId, Number toButtonId) {
			return new SlideCommandStraight(wait, step,
				InnerSensorPosition(caller, fromButtonId).Cast<Position>(),
				InnerSensorPosition(caller, toButtonId).Cast<Position>()
			);
		}
		public Object InnerStraightInner(CallerInfo caller, ISlideWait wait, Number fromButtonId, Number toButtonId) {
			return InnerStraightInner(caller, wait, null, fromButtonId, toButtonId);
		}
		public Object InnerStraightInner(CallerInfo caller, IStep step, Number fromButtonId, Number toButtonId) {
			return InnerStraightInner(caller, null, step, fromButtonId, toButtonId);
		}
		public Object InnerStraightInner(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return InnerStraightInner(caller, null, null, fromButtonId, toButtonId);
		}
#endregion
#region InnerStraightCenter()
		public Object InnerStraightCenter(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId) {
			return new SlideCommandStraight(wait, step,
				InnerSensorPosition(caller, fromButtonId).Cast<Position>(),
				CenterSensorPosition(caller).Cast<Position>()
			);
		}
		public Object InnerStraightCenter(CallerInfo caller, ISlideWait wait, Number fromButtonId) {
			return InnerStraightCenter(caller, wait, null, fromButtonId);
		}
		public Object InnerStraightCenter(CallerInfo caller, IStep step, Number fromButtonId) {
			return InnerStraightCenter(caller, null, step, fromButtonId);
		}
		public Object InnerStraightCenter(CallerInfo caller, Number fromButtonId) {
			return InnerStraightCenter(caller, null, null, fromButtonId);
		}
#endregion
#region CenterStraightOuter()
		public Object CenterStraightOuter(CallerInfo caller, ISlideWait wait, IStep step, Number toButtonId) {
			return new SlideCommandStraight(wait, step,
				CenterSensorPosition(caller).Cast<Position>(),
				OuterSensorPosition(caller, toButtonId).Cast<Position>()
			);
		}
		public Object CenterStraightOuter(CallerInfo caller, ISlideWait wait, Number toButtonId) {
			return CenterStraightOuter(caller, wait, null, toButtonId);
		}
		public Object CenterStraightOuter(CallerInfo caller, IStep step, Number toButtonId) {
			return CenterStraightOuter(caller, null, step, toButtonId);
		}
		public Object CenterStraightOuter(CallerInfo caller, Number toButtonId) {
			return CenterStraightOuter(caller, null, null, toButtonId);
		}
#endregion
#region CenterStraightInner()
		public Object CenterStraightInner(CallerInfo caller, ISlideWait wait, IStep step, Number toButtonId) {
			return new SlideCommandStraight(wait, step,
				CenterSensorPosition(caller).Cast<Position>(),
				InnerSensorPosition(caller, toButtonId).Cast<Position>()
			);
		}
		public Object CenterStraightInner(CallerInfo caller, ISlideWait wait, Number toButtonId) {
			return CenterStraightInner(caller, wait, null, toButtonId);
		}
		public Object CenterStraightInner(CallerInfo caller, IStep step, Number toButtonId) {
			return CenterStraightInner(caller, null, step, toButtonId);
		}
		public Object CenterStraightInner(CallerInfo caller, Number toButtonId) {
			return CenterStraightInner(caller, null, null, toButtonId);
		}
#endregion
#region CenterStraightCenter()
		public Object CenterStraightCenter(CallerInfo caller, ISlideWait wait, IStep step) {
			return new SlideCommandStraight(wait, step,
				CenterSensorPosition(caller).Cast<Position>(),
				CenterSensorPosition(caller).Cast<Position>()
				);
		}
		public Object CenterStraightCenter(CallerInfo caller, ISlideWait wait) {
			return CenterStraightCenter(caller, wait, null);
		}
		public Object CenterStraightCenter(CallerInfo caller, IStep step) {
			return CenterStraightCenter(caller, null, step);
		}
		public Object CenterStraightCenter(CallerInfo caller) {
			return CenterStraightCenter(caller, null, null);
		}
#endregion
#region OuterCurveClockwiseOuterAxisCenter()
		public Object OuterCurveClockwiseOuterAxisCenter(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId, Number toButtonId) {
			return new SlideCommandCurve(wait, step,
				CenterSensorPosition(caller).Cast<Position>(),
				OuterSensorRadius(caller).Cast<Value>(),
				SensorDegree(caller, fromButtonId).Cast<Value>(),
				SensorDegreeDistanceClockwise(caller, fromButtonId, toButtonId).Cast<Value>()
			);
		}
		public Object OuterCurveClockwiseOuterAxisCenter(CallerInfo caller, ISlideWait wait, Number fromButtonId, Number toButtonId) {
			return OuterCurveClockwiseOuterAxisCenter(caller, wait, null, fromButtonId, toButtonId);
		}
		public Object OuterCurveClockwiseOuterAxisCenter(CallerInfo caller, IStep step, Number fromButtonId, Number toButtonId) {
			return OuterCurveClockwiseOuterAxisCenter(caller, null, step, fromButtonId, toButtonId);
		}
		public Object OuterCurveClockwiseOuterAxisCenter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return OuterCurveClockwiseOuterAxisCenter(caller, null, null, fromButtonId, toButtonId);
		}
#endregion
#region OuterCurveCounterClockwiseOuterAxisCenter()
		public Object OuterCurveCounterClockwiseOuterAxisCenter(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId, Number toButtonId) {
			return new SlideCommandCurve(wait, step,
				CenterSensorPosition(caller).Cast<Position>(),
				OuterSensorRadius(caller).Cast<Value>(),
				SensorDegree(caller, fromButtonId).Cast<Value>(),
				SensorDegreeDistanceCounterClockwise(caller, fromButtonId, toButtonId).Cast<Value>()
			);
		}
		public Object OuterCurveCounterClockwiseOuterAxisCenter(CallerInfo caller, ISlideWait wait, Number fromButtonId, Number toButtonId) {
			return OuterCurveCounterClockwiseOuterAxisCenter(caller, wait, null, fromButtonId, toButtonId);
		}
		public Object OuterCurveCounterClockwiseOuterAxisCenter(CallerInfo caller, IStep step, Number fromButtonId, Number toButtonId) {
			return OuterCurveCounterClockwiseOuterAxisCenter(caller, null, step, fromButtonId, toButtonId);
		}
		public Object OuterCurveCounterClockwiseOuterAxisCenter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return OuterCurveCounterClockwiseOuterAxisCenter(caller, null, null, fromButtonId, toButtonId);
		}
#endregion
#region OuterCurveRightOuterAxisCenter()
		public Object OuterCurveRightOuterAxisCenter(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId, Number toButtonId) {
			return new SlideCommandCurve(wait, step,
				CenterSensorPosition(caller).Cast<Position>(),
				OuterSensorRadius(caller).Cast<Value>(),
				SensorDegree(caller, fromButtonId).Cast<Value>(),
				SensorDegreeDistanceRight(caller, fromButtonId, toButtonId).Cast<Value>()
			);
		}
		public Object OuterCurveRightOuterAxisCenter(CallerInfo caller, ISlideWait wait, Number fromButtonId, Number toButtonId) {
			return OuterCurveRightOuterAxisCenter(caller, wait, null, fromButtonId, toButtonId);
		}
		public Object OuterCurveRightOuterAxisCenter(CallerInfo caller, IStep step, Number fromButtonId, Number toButtonId) {
			return OuterCurveRightOuterAxisCenter(caller, null, step, fromButtonId, toButtonId);
		}
		public Object OuterCurveRightOuterAxisCenter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return OuterCurveRightOuterAxisCenter(caller, null, null, fromButtonId, toButtonId);
		}
#endregion
#region OuterCurveLeftOuterAxisCenter()
		public Object OuterCurveLeftOuterAxisCenter(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId, Number toButtonId) {
			return new SlideCommandCurve(wait, step,
				CenterSensorPosition(caller).Cast<Position>(),
				OuterSensorRadius(caller).Cast<Value>(),
				SensorDegree(caller, fromButtonId).Cast<Value>(),
				SensorDegreeDistanceLeft(caller, fromButtonId, toButtonId).Cast<Value>()
			);
		}
		public Object OuterCurveLeftOuterAxisCenter(CallerInfo caller, ISlideWait wait, Number fromButtonId, Number toButtonId) {
			return OuterCurveLeftOuterAxisCenter(caller, wait, null, fromButtonId, toButtonId);
		}
		public Object OuterCurveLeftOuterAxisCenter(CallerInfo caller, IStep step, Number fromButtonId, Number toButtonId) {
			return OuterCurveLeftOuterAxisCenter(caller, null, step, fromButtonId, toButtonId);
		}
		public Object OuterCurveLeftOuterAxisCenter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return OuterCurveLeftOuterAxisCenter(caller, null, null, fromButtonId, toButtonId);
		}
#endregion
#region InnerCurveClockwiseInnerAxisCenter()
		public Object InnerCurveClockwiseInnerAxisCenter(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId, Number toButtonId) {
			return new SlideCommandCurve(wait, step,
				CenterSensorPosition(caller).Cast<Position>(),
				InnerSensorRadius(caller).Cast<Value>(),
				SensorDegree(caller, fromButtonId).Cast<Value>(),
				SensorDegreeDistanceClockwise(caller, fromButtonId, toButtonId).Cast<Value>()
			);
		}
		public Object InnerCurveClockwiseInnerAxisCenter(CallerInfo caller, ISlideWait wait, Number fromButtonId, Number toButtonId) {
			return InnerCurveClockwiseInnerAxisCenter(caller, wait, null, fromButtonId, toButtonId);
		}
		public Object InnerCurveClockwiseInnerAxisCenter(CallerInfo caller, IStep step, Number fromButtonId, Number toButtonId) {
			return InnerCurveClockwiseInnerAxisCenter(caller, null, step, fromButtonId, toButtonId);
		}
		public Object InnerCurveClockwiseInnerAxisCenter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return InnerCurveClockwiseInnerAxisCenter(caller, null, null, fromButtonId, toButtonId);
		}
#endregion
#region InnerCurveCounterClockwiseInnerAxisCenter()
		public Object InnerCurveCounterClockwiseInnerAxisCenter(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId, Number toButtonId) {
			return new SlideCommandCurve(wait, step,
				CenterSensorPosition(caller).Cast<Position>(),
				InnerSensorRadius(caller).Cast<Value>(),
				SensorDegree(caller, fromButtonId).Cast<Value>(),
				SensorDegreeDistanceCounterClockwise(caller, fromButtonId, toButtonId).Cast<Value>()
			);
		}
		public Object InnerCurveCounterClockwiseInnerAxisCenter(CallerInfo caller, ISlideWait wait, Number fromButtonId, Number toButtonId) {
			return InnerCurveCounterClockwiseInnerAxisCenter(caller, wait, null, fromButtonId, toButtonId);
		}
		public Object InnerCurveCounterClockwiseInnerAxisCenter(CallerInfo caller, IStep step, Number fromButtonId, Number toButtonId) {
			return InnerCurveCounterClockwiseInnerAxisCenter(caller, null, step, fromButtonId, toButtonId);
		}
		public Object InnerCurveCounterClockwiseInnerAxisCenter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return InnerCurveCounterClockwiseInnerAxisCenter(caller, null, null, fromButtonId, toButtonId);
		}
#endregion
#region InnerCurveRightInnerAxisCenter()
		public Object InnerCurveRightInnerAxisCenter(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId, Number toButtonId) {
			return new SlideCommandCurve(wait, step,
				CenterSensorPosition(caller).Cast<Position>(),
				InnerSensorRadius(caller).Cast<Value>(),
				SensorDegree(caller, fromButtonId).Cast<Value>(),
				SensorDegreeDistanceRight(caller, fromButtonId, toButtonId).Cast<Value>()
			);
		}
		public Object InnerCurveRightInnerAxisCenter(CallerInfo caller, ISlideWait wait, Number fromButtonId, Number toButtonId) {
			return InnerCurveRightInnerAxisCenter(caller, wait, null, fromButtonId, toButtonId);
		}
		public Object InnerCurveRightInnerAxisCenter(CallerInfo caller, IStep step, Number fromButtonId, Number toButtonId) {
			return InnerCurveRightInnerAxisCenter(caller, null, step, fromButtonId, toButtonId);
		}
		public Object InnerCurveRightInnerAxisCenter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return InnerCurveRightInnerAxisCenter(caller, null, null, fromButtonId, toButtonId);
		}
#endregion
#region InnerCurveLeftInnerAxisCenter()
		public Object InnerCurveLeftInnerAxisCenter(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId, Number toButtonId) {
			return new SlideCommandCurve(wait, step,
				CenterSensorPosition(caller).Cast<Position>(),
				InnerSensorRadius(caller).Cast<Value>(),
				SensorDegree(caller, fromButtonId).Cast<Value>(),
				SensorDegreeDistanceLeft(caller, fromButtonId, toButtonId).Cast<Value>()
			);
		}
		public Object InnerCurveLeftInnerAxisCenter(CallerInfo caller, ISlideWait wait, Number fromButtonId, Number toButtonId) {
			return InnerCurveLeftInnerAxisCenter(caller, wait, null, fromButtonId, toButtonId);
		}
		public Object InnerCurveLeftInnerAxisCenter(CallerInfo caller, IStep step, Number fromButtonId, Number toButtonId) {
			return InnerCurveLeftInnerAxisCenter(caller, null, step, fromButtonId, toButtonId);
		}
		public Object InnerCurveLeftInnerAxisCenter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return InnerCurveLeftInnerAxisCenter(caller, null, null, fromButtonId, toButtonId);
		}
#endregion
#region ShapeQAxisCenter()
		public Object ShapeQAxisCenter(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId, Number toButtonId) {
			//if (t==(s+4)%8) As-B((s+2)%8)-At
			//else As-B((s+2)%8)>B((t+6)%8)-At
			var ret = new List<ISlideCommand>();
			ISlideCommand instance;
			var button1 = fromButtonId.ToButton(caller).Cast<Button>();
			var continueButtonId1 = MsqHelperShapeQStartTo(button1);
			instance = OuterStraightInner(caller, wait, step, fromButtonId, continueButtonId1).Cast<SlideCommandStraight>();
			ret.Add(instance);
			var button2 = toButtonId.ToButton(caller).Cast<Button>();
			if (MsqHelperShapePOrShapeQIsStraightToTarget(button1, button2)) {
				instance = InnerStraightOuter(caller, wait, step, continueButtonId1, toButtonId).Cast<SlideCommandStraight>();
				ret.Add (instance);
			}
			else {
				var continueButtonId2 = MsqHelperShapeQFromTarget(button2);
				instance = InnerCurveClockwiseInnerAxisCenter(caller, wait, step, continueButtonId1, continueButtonId2).Cast<SlideCommandCurve>();
				ret.Add (instance);
				instance = InnerStraightOuter(caller, wait, step, continueButtonId2, toButtonId).Cast<SlideCommandStraight>();
				ret.Add (instance);
			}
			return Chain(caller, wait, step, ret.ToArray());
		}
		public Object ShapeQAxisCenter(CallerInfo caller, ISlideWait wait, Number fromButtonId, Number toButtonId) {
			return ShapeQAxisCenter(caller, wait, null, fromButtonId, toButtonId);
		}
		public Object ShapeQAxisCenter(CallerInfo caller, IStep step, Number fromButtonId, Number toButtonId) {
			return ShapeQAxisCenter(caller, null, step, fromButtonId, toButtonId);
		}
		public Object ShapeQAxisCenter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return ShapeQAxisCenter(caller, null, null, fromButtonId, toButtonId);
		}
#endregion
#region ShapePAxisCenter()
		public Object ShapePAxisCenter(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId, Number toButtonId) {
			//if (t==(s+4)%8) As-B((s+6)%8)-At
			//else As-B((s+6)%8)<B((t+2)%8)-At
			var ret = new List<ISlideCommand>();
			ISlideCommand instance;
			var button1 = fromButtonId.ToButton(caller).Cast<Button>();
			var continueButtonId1 = MsqHelperShapePStartTo(button1);
			instance = OuterStraightInner(caller, wait, step, fromButtonId, continueButtonId1).Cast<SlideCommandStraight>();
			ret.Add(instance);
			var button2 = toButtonId.ToButton(caller).Cast<Button>();
			if (MsqHelperShapePOrShapeQIsStraightToTarget(button1, button2)) {
				instance = InnerStraightOuter(caller, wait, step, continueButtonId1, toButtonId).Cast<SlideCommandStraight>();
				ret.Add (instance);
			}
			else {
				var continueButtonId2 = MsqHelperShapePFromTarget(button2);
				instance = InnerCurveCounterClockwiseInnerAxisCenter(caller, wait, step, continueButtonId1, continueButtonId2).Cast<SlideCommandCurve>();;
				ret.Add (instance);
				instance = InnerStraightOuter(caller, wait, step, continueButtonId2, toButtonId).Cast<SlideCommandStraight>();
				ret.Add (instance);
			}
			return Chain(caller, wait, step, ret.ToArray());
		}
		public Object ShapePAxisCenter(CallerInfo caller, ISlideWait wait, Number fromButtonId, Number toButtonId) {
			return ShapePAxisCenter(caller, wait, null, fromButtonId, toButtonId);
		}
		public Object ShapePAxisCenter(CallerInfo caller, IStep step, Number fromButtonId, Number toButtonId) {
			return ShapePAxisCenter(caller, null, step, fromButtonId, toButtonId);
		}
		public Object ShapePAxisCenter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return ShapePAxisCenter(caller, null, null, fromButtonId, toButtonId);
		}
#endregion
#region ShapeSAxisCenter()
		public Object ShapeSAxisCenter(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId, Number toButtonId) {
			//As-B((s+6)%8)-B((t+6)%8)-At
			var button1 = fromButtonId.ToButton(caller).Cast<Button>();
			var button2 = toButtonId.ToButton(caller).Cast<Button>();
			var continueButtonId1 = new Number((button1.GetIndex() + 8 - 2) % 8 + 1);
			var continueButtonId2 = new Number((button2.GetIndex() + 8 - 2) % 8 + 1);
			return Chain(caller, wait, step, new ISlideCommand[] {
				OuterStraightInner(caller, wait, step, fromButtonId, continueButtonId1).Cast<SlideCommandStraight>(),
				InnerStraightInner(caller, wait, step, continueButtonId1, continueButtonId2).Cast<SlideCommandStraight>(),
				InnerStraightOuter(caller, wait, step, continueButtonId2, toButtonId).Cast<SlideCommandStraight>()
			});
		}
		public Object ShapeSAxisCenter(CallerInfo caller, ISlideWait wait, Number fromButtonId, Number toButtonId) {
			return ShapeSAxisCenter(caller, wait, null, fromButtonId, toButtonId);
		}
		public Object ShapeSAxisCenter(CallerInfo caller, IStep step, Number fromButtonId, Number toButtonId) {
			return ShapeSAxisCenter(caller, null, step, fromButtonId, toButtonId);
		}
		public Object ShapeSAxisCenter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return ShapeSAxisCenter(caller, null, null, fromButtonId, toButtonId);
		}
#endregion
#region ShapeZAxisCenter()
		public Object ShapeZAxisCenter(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId, Number toButtonId) {
			//As-B((s+2)%8)-B((t+2)%8)-At
			var button1 = fromButtonId.ToButton(caller).Cast<Button>();
			var button2 = toButtonId.ToButton(caller).Cast<Button>();
			var continueButtonId1 = new Number((button1.GetIndex() + 2) % 8 + 1);
			var continueButtonId2 = new Number((button2.GetIndex() + 2) % 8 + 1);
			return Chain(caller, wait, step, new ISlideCommand[] {
				OuterStraightInner(caller, wait, step, fromButtonId, continueButtonId1).Cast<SlideCommandStraight>(),
				InnerStraightInner(caller, wait, step, continueButtonId1, continueButtonId2).Cast<SlideCommandStraight>(),
				InnerStraightOuter(caller, wait, step, continueButtonId2, toButtonId).Cast<SlideCommandStraight>()
			});
		}
		public Object ShapeZAxisCenter(CallerInfo caller, ISlideWait wait, Number fromButtonId, Number toButtonId) {
			return ShapeZAxisCenter(caller, wait, null, fromButtonId, toButtonId);
		}
		public Object ShapeZAxisCenter(CallerInfo caller, IStep step, Number fromButtonId, Number toButtonId) {
			return ShapeZAxisCenter(caller, null, step, fromButtonId, toButtonId);
		}
		public Object ShapeZAxisCenter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return ShapeZAxisCenter(caller, null, null, fromButtonId, toButtonId);
		}
#endregion
#region ShapeVAxisCenter()
		public Object ShapeVAxisCenter(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId, Number toButtonId) {
			//As-C-At
			return Chain(caller, wait, step, new ISlideCommand[] {
				OuterStraightCenter(caller, wait, step, fromButtonId).Cast<SlideCommandStraight>(),
				CenterStraightOuter(caller, wait, step, toButtonId).Cast<SlideCommandStraight>()
			});
		}
		public Object ShapeVAxisCenter(CallerInfo caller, ISlideWait wait, Number fromButtonId, Number toButtonId) {
			return ShapeVAxisCenter(caller, wait, null, fromButtonId, toButtonId);
		}
		public Object ShapeVAxisCenter(CallerInfo caller, IStep step, Number fromButtonId, Number toButtonId) {
			return ShapeVAxisCenter(caller, null, step, fromButtonId, toButtonId);
		}
		public Object ShapeVAxisCenter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return ShapeVAxisCenter(caller, null, null, fromButtonId, toButtonId);
		}
#endregion
#region ShapeQQ()
		public Object ShapeQQ(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId, Number toButtonId) {
			var ret = new List<ISlideCommand>();
			ISlideCommand instance;
			//As-C
			instance = new SlideCommandStraight(wait, step,
				OuterSensorPosition(caller, fromButtonId).Cast<Position>(), // Start A
				SimaiQQFirstStraightTargetPosition(caller, fromButtonId).Cast<Position>() // ほぼ中心.
				);
			ret.Add(instance);
			// Turn
			instance = new SlideCommandContinuedCurve(wait, step,
				SimaiQQCenterPosition(caller, fromButtonId).Cast<Position>(),
				SimaiQQTurnDegree(caller, fromButtonId, toButtonId).Cast<Value>()
				);
			ret.Add (instance);
			// - At
			instance = new SlideCommandContinuedStraight(wait, step,
				OuterSensorPosition(caller, toButtonId).Cast<Position>() // Target A
				);
			ret.Add (instance);
			return Chain(caller, wait, step, ret.ToArray());
		}
		public Object ShapeQQ(CallerInfo caller, ISlideWait wait, Number fromButtonId, Number toButtonId) {
			return ShapeQQ(caller, wait, null, fromButtonId, toButtonId);
		}
		public Object ShapeQQ(CallerInfo caller, IStep step, Number fromButtonId, Number toButtonId) {
			return ShapeQQ(caller, null, step, fromButtonId, toButtonId);
		}
		public Object ShapeQQ(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return ShapeQQ(caller, null, null, fromButtonId, toButtonId);
		}
#endregion
#region ShapePP()
		public Object ShapePP(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId, Number toButtonId) {
			var ret = new List<ISlideCommand>();
			ISlideCommand instance;
			//As-C
			instance = new SlideCommandStraight(wait, step,
				OuterSensorPosition(caller, fromButtonId).Cast<Position>(), // Start A
				SimaiPPFirstStraightTargetPosition(caller, fromButtonId).Cast<Position>() // ほぼ中心.
			);
			ret.Add(instance);
			// Turn
			instance = new SlideCommandContinuedCurve(wait, step,
				SimaiPPCenterPosition(caller, fromButtonId).Cast<Position>(),
				SimaiPPTurnDegree(caller, fromButtonId, toButtonId).Cast<Value>()
			);
			ret.Add (instance);
			// - At
			instance = new SlideCommandContinuedStraight(wait, step,
				OuterSensorPosition(caller, toButtonId).Cast<Position>() // Target A
			);
			ret.Add (instance);
			return Chain(caller, wait, step, ret.ToArray());
		}
		public Object ShapePP(CallerInfo caller, ISlideWait wait, Number fromButtonId, Number toButtonId) {
			return ShapePP(caller, wait, null, fromButtonId, toButtonId);
		}
		public Object ShapePP(CallerInfo caller, IStep step, Number fromButtonId, Number toButtonId) {
			return ShapePP(caller, null, step, fromButtonId, toButtonId);
		}
		public Object ShapePP(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return ShapePP(caller, null, null, fromButtonId, toButtonId);
		}
#endregion
#region ShapeVAxisOuter()
		public Object ShapeVAxisOuter(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId, Number turnButtonId, Number toButtonId) {
			//As-Ap-At
			return Chain(caller, wait, step, new ISlideCommand[] {
				OuterStraightOuter(caller, wait, step, fromButtonId, turnButtonId).Cast<SlideCommandStraight>(),
				OuterStraightOuter(caller, wait, step, turnButtonId, toButtonId).Cast<SlideCommandStraight>()
			});
		}
		public Object ShapeVAxisOuter(CallerInfo caller, ISlideWait wait, Number fromButtonId, Number turnButtonId, Number toButtonId) {
			return ShapeVAxisOuter(caller, wait, null, fromButtonId, turnButtonId, toButtonId);
		}
		public Object ShapeVAxisOuter(CallerInfo caller, IStep step, Number fromButtonId, Number turnButtonId, Number toButtonId) {
			return ShapeVAxisOuter(caller, null, step, fromButtonId, turnButtonId, toButtonId);
		}
		public Object ShapeVAxisOuter(CallerInfo caller, Number fromButtonId, Number turnButtonId, Number toButtonId) {
			return ShapeVAxisOuter(caller, null, null, fromButtonId, turnButtonId, toButtonId);
		}
#endregion
#region ShapeW()
		public Object ShapeW(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId, Number toButtonId) {
			//As-A(t-1),As-At,As-A(t+1)
			var fromButtonPos = Angle(caller, SensorDegree(caller, fromButtonId).Cast<Value>(), new Value(1f)).Cast<Position>();
			var sideRadius = new Value(1.01f);
			var toLeftButtonId = toButtonId.ToButton(caller).Cast<Button>().Turn(caller, new Number(-1)).Cast<Button>().ToNumber(caller).Cast<Number>();
			var toLeftPos = Angle(caller, SensorDegree(caller, toLeftButtonId).Cast<Value>().Add(caller, new Value(2f)).Cast<Value>(), sideRadius).Cast<Position>();
			var toLeftSlide = Straight(caller, wait, step, fromButtonPos, toLeftPos).Cast<SlideCommandStraight>();
			var toRightButtonId = toButtonId.ToButton(caller).Cast<Button>().Turn(caller, new Number(1)).Cast<Button>().ToNumber(caller).Cast<Number>();
			var toRightPos = Angle(caller, SensorDegree(caller, toRightButtonId).Cast<Value>().Sub(caller, new Value(2f)).Cast<Value>(), sideRadius).Cast<Position>();
			var toRightSlide = Straight(caller, wait, step, fromButtonPos, toRightPos).Cast<SlideCommandStraight>();
			var toCenterSlide = OuterStraightOuter(caller, wait, step, fromButtonId, toButtonId).Cast<SlideCommandStraight>();
			toLeftSlide.optionHelper.options.entity[Document.NoteOption.SIDE] = new Flag(true);
			toCenterSlide.optionHelper.options.entity[Document.NoteOption.CENTER] = new Flag(true);
			toRightSlide.optionHelper.options.entity[Document.NoteOption.SIDE] = new Flag(true);
			return Pattern(caller, wait, step, new ISlideChain[] {
				toLeftSlide, toCenterSlide, toRightSlide
			});
		}
		public Object ShapeW(CallerInfo caller, ISlideWait wait, Number fromButtonId, Number toButtonId) {
			return ShapeW(caller, wait, null, fromButtonId, toButtonId);
		}
		public Object ShapeW(CallerInfo caller, IStep step, Number fromButtonId, Number toButtonId) {
			return ShapeW(caller, null, step, fromButtonId, toButtonId);
		}
		public Object ShapeW(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return ShapeW(caller, null, null, fromButtonId, toButtonId);
		}
#endregion
#region OuterCurveClockwiseInner()
		public Object OuterCurveClockwiseInner(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId, Number toButtonId) {
			//if (t==(s+2)%8) As-Bt
			//else As-B((s+2)%8)>>Bt
			var ret = new List<ISlideCommand>();
			ISlideCommand instance;
			var button1 = fromButtonId.ToButton(caller).Cast<Button>();
			var button2 =toButtonId.ToButton(caller).Cast<Button>();
			var As = fromButtonId;
			var Bt = toButtonId;
			if (MsqHelperOuterClockwiseInnerIsStraightToTarget(button1, button2)) {
				instance = OuterStraightInner(caller, wait, step, As, Bt).Cast<SlideCommandStraight>();
				ret.Add(instance);
			}
			else {
				var Bs = MsqHelperShapeQStartTo(button1);
				instance = OuterStraightInner(caller, wait, step, As, Bs).Cast<SlideCommandStraight>();
				ret.Add(instance);
				instance = InnerCurveClockwiseInnerAxisCenter(caller, wait, step, Bs, Bt).Cast<SlideCommandCurve>();
				ret.Add(instance);
			}
			return Chain(caller, wait, step, ret.ToArray());
		}
		public Object OuterCurveClockwiseInner(CallerInfo caller, ISlideWait wait, Number fromButtonId, Number toButtonId) {
			return OuterCurveClockwiseInner(caller, wait, null, fromButtonId, toButtonId);
		}
		public Object OuterCurveClockwiseInner(CallerInfo caller, IStep step, Number fromButtonId, Number toButtonId) {
			return OuterCurveClockwiseInner(caller, null, step, fromButtonId, toButtonId);
		}
		public Object OuterCurveClockwiseInner(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return OuterCurveClockwiseInner(caller, null, null, fromButtonId, toButtonId);
		}
#endregion
#region OuterCurveCounterClockwiseInner()
		public Object OuterCurveCounterClockwiseInner(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId, Number toButtonId) {
			//if (t==(s+6)%8) As-Bt
			//else As-B((s+6)%8)<<Bt
			var ret = new List<ISlideCommand>();
			ISlideCommand instance;
			var button1 = fromButtonId.ToButton(caller).Cast<Button>();
			var button2 = toButtonId.ToButton(caller).Cast<Button>();
			var As = fromButtonId;
			var Bt = toButtonId;
			if (MsqHelperOuterCounterClockwiseInnerIsStraightToTarget(button1, button2)) {
				instance = OuterStraightInner(caller, wait, step, As, Bt).Cast<SlideCommandStraight>();
				ret.Add(instance);
			}
			else {
				var Bs = MsqHelperShapePStartTo(button1);
				instance = OuterStraightInner(caller, wait, step, As, Bs).Cast<SlideCommandStraight>();
				ret.Add(instance);
				instance = InnerCurveCounterClockwiseInnerAxisCenter(caller, wait, step, Bs, Bt).Cast<SlideCommandCurve>();;
				ret.Add(instance);
			}
			return Chain(caller, wait, step, ret.ToArray());
		}
		public Object OuterCurveCounterClockwiseInner(CallerInfo caller, ISlideWait wait, Number fromButtonId, Number toButtonId) {
			return OuterCurveCounterClockwiseInner(caller, wait, null, fromButtonId, toButtonId);
		}
		public Object OuterCurveCounterClockwiseInner(CallerInfo caller, IStep step, Number fromButtonId, Number toButtonId) {
			return OuterCurveCounterClockwiseInner(caller, null, step, fromButtonId, toButtonId);
		}
		public Object OuterCurveCounterClockwiseInner(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return OuterCurveCounterClockwiseInner(caller, null, null, fromButtonId, toButtonId);
		}
#endregion
#region OuterCurveRightInner()
		public Object OuterCurveRightInner(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId, Number toButtonId) {
			if (fromButtonId.ToButton(caller).Cast<Button>().IsTopSide ()) {
				return OuterCurveClockwiseInner(caller, wait, step, fromButtonId, toButtonId);
			}
			return OuterCurveCounterClockwiseInner(caller, wait, step, fromButtonId, toButtonId);
		}
		public Object OuterCurveRightInner(CallerInfo caller, ISlideWait wait, Number fromButtonId, Number toButtonId) {
			return OuterCurveRightInner(caller, wait, null, fromButtonId, toButtonId);
		}
		public Object OuterCurveRightInner(CallerInfo caller, IStep step, Number fromButtonId, Number toButtonId) {
			return OuterCurveRightInner(caller, null, step, fromButtonId, toButtonId);
		}
		public Object OuterCurveRightInner(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return OuterCurveRightInner(caller, null, null, fromButtonId, toButtonId);
		}
#endregion
#region OuterCurveLeftInner()
		public Object OuterCurveLeftInner(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId, Number toButtonId) {
			if (fromButtonId.ToButton(caller).Cast<Button>().IsTopSide ()) {
				return OuterCurveCounterClockwiseInner(caller, wait, step, fromButtonId, toButtonId);
			}
			return OuterCurveClockwiseInner(caller, wait, step, fromButtonId, toButtonId);
		}
		public Object OuterCurveLeftInner(CallerInfo caller, ISlideWait wait, Number fromButtonId, Number toButtonId) {
			return OuterCurveLeftInner(caller, wait, null, fromButtonId, toButtonId);
		}
		public Object OuterCurveLeftInner(CallerInfo caller, IStep step, Number fromButtonId, Number toButtonId) {
			return OuterCurveLeftInner(caller, null, step, fromButtonId, toButtonId);
		}
		public Object OuterCurveLeftInner(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return OuterCurveLeftInner(caller, null, null, fromButtonId, toButtonId);
		}
#endregion
#region InnerCurveClockwiseOuter()
		public Object InnerCurveClockwiseOuter(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId, Number toButtonId) {
			//if (t==(s+2)%8) Bs-At
			//else Bs>B((t+6)%8)-At
			var ret = new List<ISlideCommand>();
			ISlideCommand instance;
			var button1 = fromButtonId.ToButton(caller).Cast<Button>();
			var button2 = toButtonId.ToButton(caller).Cast<Button>();
			var Bs = fromButtonId;
			var At = toButtonId;
			if (MsqHelperOuterClockwiseInnerIsStraightToTarget(button1, button2)) {
				instance = InnerStraightOuter(caller, wait, step, Bs, At).Cast<SlideCommandStraight>();
				ret.Add (instance);
			}
			else {
				var Bt = MsqHelperShapeQFromTarget(button2);
				instance = InnerCurveClockwiseInnerAxisCenter(caller, wait, step, Bs, Bt).Cast<SlideCommandCurve>();
				ret.Add (instance);
				instance = InnerStraightOuter(caller, wait, step, Bt, At).Cast<SlideCommandStraight>();
				ret.Add (instance);
			}
			return Chain(caller, wait, step, ret.ToArray());
		}
		public Object InnerCurveClockwiseOuter(CallerInfo caller, ISlideWait wait, Number fromButtonId, Number toButtonId) {
			return InnerCurveClockwiseOuter(caller, wait, null, fromButtonId, toButtonId);
		}
		public Object InnerCurveClockwiseOuter(CallerInfo caller, IStep step, Number fromButtonId, Number toButtonId) {
			return InnerCurveClockwiseOuter(caller, null, step, fromButtonId, toButtonId);
		}
		public Object InnerCurveClockwiseOuter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return InnerCurveClockwiseOuter(caller, null, null, fromButtonId, toButtonId);
		}
#endregion
#region InnerCurveCounterClockwiseOuter()
		public Object InnerCurveCounterClockwiseOuter(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId, Number toButtonId) {
			//if (t==(s+6)%8) Bs-At
			//else Bs<B((t+2)%8)-At
			var ret = new List<ISlideCommand>();
			ISlideCommand instance;
			var button1 = fromButtonId.ToButton(caller).Cast<Button>();
			var button2 = toButtonId.ToButton(caller).Cast<Button>();
			var Bs = fromButtonId;
			var At = toButtonId;
			if (MsqHelperOuterCounterClockwiseInnerIsStraightToTarget(button1, button2)) {
				instance = InnerStraightOuter(caller, wait, step, Bs, At).Cast<SlideCommandStraight>();
				ret.Add (instance);
			}
			else {
				var Bt = MsqHelperShapePFromTarget(button2);
				instance = InnerCurveCounterClockwiseInnerAxisCenter(caller, wait, step, Bs, Bt).Cast<SlideCommandCurve>();;
				ret.Add (instance);
				instance = InnerStraightOuter(caller, wait, step, Bt, At).Cast<SlideCommandStraight>();
				ret.Add (instance);
			}
			return Chain(caller, wait, step, ret.ToArray());
		}
		public Object InnerCurveCounterClockwiseOuter(CallerInfo caller, ISlideWait wait, Number fromButtonId, Number toButtonId) {
			return InnerCurveCounterClockwiseOuter(caller, wait, null, fromButtonId, toButtonId);
		}
		public Object InnerCurveCounterClockwiseOuter(CallerInfo caller, IStep step, Number fromButtonId, Number toButtonId) {
			return InnerCurveCounterClockwiseOuter(caller, null, step, fromButtonId, toButtonId);
		}
		public Object InnerCurveCounterClockwiseOuter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return InnerCurveCounterClockwiseOuter(caller, null, null, fromButtonId, toButtonId);
		}
#endregion
#region InnerCurveRightOuter()
		public Object InnerCurveRightOuter(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId, Number toButtonId) {
			if (fromButtonId.ToButton(caller).Cast<Button>().IsTopSide ()) {
				return InnerCurveClockwiseOuter(caller, wait, step, fromButtonId, toButtonId);
			}
			return InnerCurveCounterClockwiseOuter(caller, wait, step, fromButtonId, toButtonId);
		}
		public Object InnerCurveRightOuter(CallerInfo caller, ISlideWait wait, Number fromButtonId, Number toButtonId) {
			return InnerCurveRightOuter(caller, wait, null, fromButtonId, toButtonId);
		}
		public Object InnerCurveRightOuter(CallerInfo caller, IStep step, Number fromButtonId, Number toButtonId) {
			return InnerCurveRightOuter(caller, null, step, fromButtonId, toButtonId);
		}
		public Object InnerCurveRightOuter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return InnerCurveRightOuter(caller, null, null, fromButtonId, toButtonId);
		}
#endregion
#region InnerCurveLeftOuter()
		public Object InnerCurveLeftOuter(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId, Number toButtonId) {
			if (fromButtonId.ToButton(caller).Cast<Button>().IsTopSide ()) {
				return InnerCurveCounterClockwiseOuter(caller, wait, step, fromButtonId, toButtonId);
			}
			return InnerCurveClockwiseOuter(caller, wait, step, fromButtonId, toButtonId);
		}
		public Object InnerCurveLeftOuter(CallerInfo caller, ISlideWait wait, Number fromButtonId, Number toButtonId) {
			return InnerCurveLeftOuter(caller, wait, null, fromButtonId, toButtonId);
		}
		public Object InnerCurveLeftOuter(CallerInfo caller, IStep step, Number fromButtonId, Number toButtonId) {
			return InnerCurveLeftOuter(caller, null, step, fromButtonId, toButtonId);
		}
		public Object InnerCurveLeftOuter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return InnerCurveLeftOuter(caller, null, null, fromButtonId, toButtonId);
		}
#endregion
#region OuterCurveClockwiseCenter()
		public Object OuterCurveClockwiseCenter(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId) {
			var ret = new List<ISlideCommand>();
			ISlideCommand instance;
			//As-C
			instance = new SlideCommandStraight(wait, step, 
				OuterSensorPosition(caller, fromButtonId).Cast<Position>(), // Start A
				CenterSensorPosition(caller).Cast<Position>()
				);
			ret.Add(instance);
			// Turn
			instance = new SlideCommandContinuedCurve(wait, step, 
				SimaiQQCenterPosition(caller, fromButtonId).Cast<Position>(),
				new Value(360)
				);
			ret.Add (instance);
			return Chain(caller, wait, step, ret.ToArray());
		}
		public Object OuterCurveClockwiseCenter(CallerInfo caller, ISlideWait wait, Number fromButtonId) {
			return OuterCurveClockwiseCenter(caller, wait, null, fromButtonId);
		}
		public Object OuterCurveClockwiseCenter(CallerInfo caller, IStep step, Number fromButtonId) {
			return OuterCurveClockwiseCenter(caller, null, step, fromButtonId);
		}
		public Object OuterCurveClockwiseCenter(CallerInfo caller, Number fromButtonId) {
			return OuterCurveClockwiseCenter(caller, null, null, fromButtonId);
		}
#endregion
#region OuterCurveCounterClockwiseCenter()
		public Object OuterCurveCounterClockwiseCenter(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId) {
			var ret = new List<ISlideCommand>();
			ISlideCommand instance;
			//As-C
			instance = new SlideCommandStraight(wait, step, 
				OuterSensorPosition(caller, fromButtonId).Cast<Position>(), // Start A
				CenterSensorPosition(caller).Cast<Position>()
				);
			ret.Add(instance);
			// Turn
			instance = new SlideCommandContinuedCurve(wait, step, 
				SimaiPPCenterPosition(caller, fromButtonId).Cast<Position>(),
				new Value(-360)
				);
			ret.Add (instance);
			return Chain(caller, wait, step, ret.ToArray());
		}
		public Object OuterCurveCounterClockwiseCenter(CallerInfo caller, ISlideWait wait, Number fromButtonId) {
			return OuterCurveCounterClockwiseCenter(caller, wait, null, fromButtonId);
		}
		public Object OuterCurveCounterClockwiseCenter(CallerInfo caller, IStep step, Number fromButtonId) {
			return OuterCurveCounterClockwiseCenter(caller, null, step, fromButtonId);
		}
		public Object OuterCurveCounterClockwiseCenter(CallerInfo caller, Number fromButtonId) {
			return OuterCurveCounterClockwiseCenter(caller, null, null, fromButtonId);
		}
#endregion
#region OuterCurveRightCenter()
		public Object OuterCurveRightCenter(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId) {
			if (fromButtonId.ToButton(caller).Cast<Button>().IsTopSide ()) {
				return OuterCurveClockwiseCenter(caller, wait, step, fromButtonId);
			}
			return OuterCurveCounterClockwiseCenter(caller, wait, step, fromButtonId);
		}
		public Object OuterCurveRightCenter(CallerInfo caller, ISlideWait wait, Number fromButtonId) {
			return OuterCurveRightCenter(caller, wait, null, fromButtonId);
		}
		public Object OuterCurveRightCenter(CallerInfo caller, IStep step, Number fromButtonId) {
			return OuterCurveRightCenter(caller, null, step, fromButtonId);
		}
		public Object OuterCurveRightCenter(CallerInfo caller, Number fromButtonId) {
			return OuterCurveRightCenter(caller, null, null, fromButtonId);
		}
#endregion
#region OuterCurveLeftCenter()
		public Object OuterCurveLeftCenter(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId) {
			if (fromButtonId.ToButton(caller).Cast<Button>().IsTopSide ()) {
				return OuterCurveCounterClockwiseCenter(caller, wait, step, fromButtonId);
			}
			return OuterCurveClockwiseCenter(caller, wait, step, fromButtonId);
		}
		public Object OuterCurveLeftCenter(CallerInfo caller, ISlideWait wait, Number fromButtonId) {
			return OuterCurveLeftCenter(caller, wait, null, fromButtonId);
		}
		public Object OuterCurveLeftCenter(CallerInfo caller, IStep step, Number fromButtonId) {
			return OuterCurveLeftCenter(caller, null, step, fromButtonId);
		}
		public Object OuterCurveLeftCenter(CallerInfo caller, Number fromButtonId) {
			return OuterCurveLeftCenter(caller, null, null, fromButtonId);
		}
#endregion
#region CenterCurveClockwiseOuter()
		public Object CenterCurveClockwiseOuter(CallerInfo caller, ISlideWait wait, IStep step, Number toButtonId) {
			var ret = new List<ISlideCommand>();
			ISlideCommand instance;
			// Turn
			var toButtonDeg = SensorDegree(caller, toButtonId).Cast<Value>();
			var radius = new Value(0.5f);
			var center = Angle(caller, toButtonDeg, radius).Cast<Position>();
			instance = new SlideCommandCurve(wait, step, 
				center,
				radius,
				new Value(toButtonDeg.entity + 180.0f),
				new Value(180.0f)
			);
			ret.Add (instance);
			return Chain(caller, wait, step, ret.ToArray());
		}
		public Object CenterCurveClockwiseOuter(CallerInfo caller, ISlideWait wait, Number toButtonId) {
			return CenterCurveClockwiseOuter(caller, wait, null, toButtonId);
		}
		public Object CenterCurveClockwiseOuter(CallerInfo caller, IStep step, Number toButtonId) {
			return CenterCurveClockwiseOuter(caller, null, step, toButtonId);
		}
		public Object CenterCurveClockwiseOuter(CallerInfo caller, Number toButtonId) {
			return CenterCurveClockwiseOuter(caller, null, null, toButtonId);
		}
#endregion
#region CenterCurveCounterClockwiseOuter()
		public Object CenterCurveCounterClockwiseOuter(CallerInfo caller, ISlideWait wait, IStep step, Number toButtonId) {
			var ret = new List<ISlideCommand>();
			ISlideCommand instance;
			// Turn
			var toButtonDeg = SensorDegree(caller, toButtonId).Cast<Value>();
			var radius = new Value(0.5f);
			var center = Angle(caller, toButtonDeg, radius).Cast<Position>();
			instance = new SlideCommandCurve(wait, step, 
				center,
				radius,
				new Value(toButtonDeg.entity + 180.0f),
				new Value(-180.0f)
			);
			ret.Add (instance);
			return Chain(caller, wait, step, ret.ToArray());
		}
		public Object CenterCurveCounterClockwiseOuter(CallerInfo caller, ISlideWait wait, Number toButtonId) {
			return CenterCurveCounterClockwiseOuter(caller, wait, null, toButtonId);
		}
		public Object CenterCurveCounterClockwiseOuter(CallerInfo caller, IStep step, Number toButtonId) {
			return CenterCurveCounterClockwiseOuter(caller, null, step, toButtonId);
		}
		public Object CenterCurveCounterClockwiseOuter(CallerInfo caller, Number toButtonId) {
			return CenterCurveCounterClockwiseOuter(caller, null, null, toButtonId);
		}
#endregion
#region CenterCurveRightOuter()
		public Object CenterCurveRightOuter(CallerInfo caller, ISlideWait wait, IStep step, Number toButtonId) {
			if (toButtonId.ToButton(caller).Cast<Button>().IsTopSide ()) {
				return CenterCurveCounterClockwiseOuter(caller, wait, step, toButtonId);
			}
			return CenterCurveClockwiseOuter(caller, wait, step, toButtonId);
		}
		public Object CenterCurveRightOuter(CallerInfo caller, ISlideWait wait, Number toButtonId) {
			return CenterCurveRightOuter(caller, wait, null, toButtonId);
		}
		public Object CenterCurveRightOuter(CallerInfo caller, IStep step, Number toButtonId) {
			return CenterCurveRightOuter(caller, null, step, toButtonId);
		}
		public Object CenterCurveRightOuter(CallerInfo caller, Number toButtonId) {
			return CenterCurveRightOuter(caller, null, null, toButtonId);
		}
#endregion
#region CenterCurveLeftOuter()
		public Object CenterCurveLeftOuter(CallerInfo caller, ISlideWait wait, IStep step, Number toButtonId) {
			if (toButtonId.ToButton(caller).Cast<Button>().IsTopSide ()) {
				return CenterCurveClockwiseOuter(caller, wait, step, toButtonId);
			}
			return CenterCurveCounterClockwiseOuter(caller, wait, step, toButtonId);
		}
		public Object CenterCurveLeftOuter(CallerInfo caller, ISlideWait wait, Number toButtonId) {
			return CenterCurveLeftOuter(caller, wait, null, toButtonId);
		}
		public Object CenterCurveLeftOuter(CallerInfo caller, IStep step, Number toButtonId) {
			return CenterCurveLeftOuter(caller, null, step, toButtonId);
		}
		public Object CenterCurveLeftOuter(CallerInfo caller, Number toButtonId) {
			return CenterCurveLeftOuter(caller, null, null, toButtonId);
		}
#endregion
#region InnerCurveClockwiseCenter()
		public Object InnerCurveClockwiseCenter(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId) {
			var ret = new List<ISlideCommand>();
			ISlideCommand instance;
			//Bs-C
			instance = new SlideCommandStraight(wait, step, 
				InnerSensorPosition(caller, fromButtonId).Cast<Position>(), // Start A
				CenterSensorPosition(caller).Cast<Position>()
				);
			ret.Add(instance);
			// Turn
			instance = new SlideCommandContinuedCurve(wait, step, 
				SimaiQQCenterPosition(caller, fromButtonId).Cast<Position>(),
				new Value(360)
				);
			ret.Add (instance);
			return Chain(caller, wait, step, ret.ToArray());
		}
		public Object InnerCurveClockwiseCenter(CallerInfo caller, ISlideWait wait, Number fromButtonId) {
			return InnerCurveClockwiseCenter(caller, wait, null, fromButtonId);
		}
		public Object InnerCurveClockwiseCenter(CallerInfo caller, IStep step, Number fromButtonId) {
			return InnerCurveClockwiseCenter(caller, null, step, fromButtonId);
		}
		public Object InnerCurveClockwiseCenter(CallerInfo caller, Number fromButtonId) {
			return InnerCurveClockwiseCenter(caller, null, null, fromButtonId);
		}
#endregion
#region InnerCurveCounterClockwiseCenter()
		public Object InnerCurveCounterClockwiseCenter(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId) {
			var ret = new List<ISlideCommand>();
			ISlideCommand instance;
			//Bs-C
			instance = new SlideCommandStraight(wait, step, 
				InnerSensorPosition(caller, fromButtonId).Cast<Position>(), // Start A
				CenterSensorPosition(caller).Cast<Position>()
				);
			ret.Add(instance);
			// Turn
			instance = new SlideCommandContinuedCurve(wait, step, 
				SimaiPPCenterPosition(caller, fromButtonId).Cast<Position>(),
				new Value(-360)
				);
			ret.Add (instance);
			return Chain(caller, wait, step, ret.ToArray());
		}
		public Object InnerCurveCounterClockwiseCenter(CallerInfo caller, ISlideWait wait, Number fromButtonId) {
			return InnerCurveCounterClockwiseCenter(caller, wait, null, fromButtonId);
		}
		public Object InnerCurveCounterClockwiseCenter(CallerInfo caller, IStep step, Number fromButtonId) {
			return InnerCurveCounterClockwiseCenter(caller, null, step, fromButtonId);
		}
		public Object InnerCurveCounterClockwiseCenter(CallerInfo caller, Number fromButtonId) {
			return InnerCurveCounterClockwiseCenter(caller, null, null, fromButtonId);
		}
#endregion
#region InnerCurveRightCenter()
		public Object InnerCurveRightCenter(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId) {
			if (fromButtonId.ToButton(caller).Cast<Button>().IsTopSide ()) {
				return InnerCurveClockwiseCenter(caller, wait, step, fromButtonId);
			}
			return InnerCurveCounterClockwiseCenter(caller, wait, step, fromButtonId);
		}
		public Object InnerCurveRightCenter(CallerInfo caller, ISlideWait wait, Number fromButtonId) {
			return InnerCurveRightCenter(caller, wait, null, fromButtonId);
		}
		public Object InnerCurveRightCenter(CallerInfo caller, IStep step, Number fromButtonId) {
			return InnerCurveRightCenter(caller, null, step, fromButtonId);
		}
		public Object InnerCurveRightCenter(CallerInfo caller, Number fromButtonId) {
			return InnerCurveRightCenter(caller, null, null, fromButtonId);
		}
#endregion
#region InnerCurveLeftCenter()
		public Object InnerCurveLeftCenter(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId) {
			if (fromButtonId.ToButton(caller).Cast<Button>().IsTopSide ()) {
				return InnerCurveCounterClockwiseCenter(caller, wait, step, fromButtonId);
			}
			return InnerCurveClockwiseCenter(caller, wait, step, fromButtonId);
		}
		public Object InnerCurveLeftCenter(CallerInfo caller, ISlideWait wait, Number fromButtonId) {
			return InnerCurveLeftCenter(caller, wait, null, fromButtonId);
		}
		public Object InnerCurveLeftCenter(CallerInfo caller, IStep step, Number fromButtonId) {
			return InnerCurveLeftCenter(caller, null, step, fromButtonId);
		}
		public Object InnerCurveLeftCenter(CallerInfo caller, Number fromButtonId) {
			return InnerCurveLeftCenter(caller, null, null, fromButtonId);
		}
#endregion
#region CenterCurveClockwiseInner()
		public Object CenterCurveClockwiseInner(CallerInfo caller, ISlideWait wait, IStep step, Number toButtonId) {
			var ret = new List<ISlideCommand>();
			ISlideCommand instance;
			// Turn
			var toButtonDeg = SensorDegree(caller, toButtonId).Cast<Value>();
			var radius = InnerSensorRadius(caller).Cast<Value>().Mul(caller, new Value(0.5f)).Cast<Value>();
			var center = Angle(caller, toButtonDeg, radius).Cast<Position>();
			instance = new SlideCommandCurve(wait, step, 
				center,
				radius,
				new Value(toButtonDeg.entity + 180.0f),
				new Value(180.0f)
				);
			ret.Add (instance);
			return Chain(caller, wait, step, ret.ToArray());
		}
		public Object CenterCurveClockwiseInner(CallerInfo caller, ISlideWait wait, Number toButtonId) {
			return CenterCurveClockwiseInner(caller, wait, null, toButtonId);
		}
		public Object CenterCurveClockwiseInner(CallerInfo caller, IStep step, Number toButtonId) {
			return CenterCurveClockwiseInner(caller, null, step, toButtonId);
		}
		public Object CenterCurveClockwiseInner(CallerInfo caller, Number toButtonId) {
			return CenterCurveClockwiseInner(caller, null, null, toButtonId);
		}
#endregion
#region CenterCurveCounterClockwiseInner()
		public Object CenterCurveCounterClockwiseInner(CallerInfo caller, ISlideWait wait, IStep step, Number toButtonId) {
			var ret = new List<ISlideCommand>();
			ISlideCommand instance;
			// Turn
			var toButtonDeg = SensorDegree(caller, toButtonId).Cast<Value>();
			var radius = InnerSensorRadius(caller).Cast<Value>().Mul(caller, new Value(0.5f)).Cast<Value>();
			var center = Angle(caller, toButtonDeg, radius).Cast<Position>();
			instance = new SlideCommandCurve(wait, step, 
				center,
				radius,
				new Value(toButtonDeg.entity + 180.0f),
				new Value(-180.0f)
				);
			ret.Add (instance);
			return Chain(caller, wait, step, ret.ToArray());
		}
		public Object CenterCurveCounterClockwiseInner(CallerInfo caller, ISlideWait wait, Number toButtonId) {
			return CenterCurveCounterClockwiseInner(caller, wait, null, toButtonId);
		}
		public Object CenterCurveCounterClockwiseInner(CallerInfo caller, IStep step, Number toButtonId) {
			return CenterCurveCounterClockwiseInner(caller, null, step, toButtonId);
		}
		public Object CenterCurveCounterClockwiseInner(CallerInfo caller, Number toButtonId) {
			return CenterCurveCounterClockwiseInner(caller, null, null, toButtonId);
		}
#endregion
#region CenterCurveRightInner()
		public Object CenterCurveRightInner(CallerInfo caller, ISlideWait wait, IStep step, Number toButtonId) {
			if (toButtonId.ToButton(caller).Cast<Button>().IsTopSide ()) {
				return CenterCurveCounterClockwiseInner(caller, wait, step, toButtonId);
			}
			return CenterCurveClockwiseInner(caller, wait, step, toButtonId);
		}
		public Object CenterCurveRightInner(CallerInfo caller, ISlideWait wait, Number toButtonId) {
			return CenterCurveRightInner(caller, wait, null, toButtonId);
		}
		public Object CenterCurveRightInner(CallerInfo caller, IStep step, Number toButtonId) {
			return CenterCurveRightInner(caller, null, step, toButtonId);
		}
		public Object CenterCurveRightInner(CallerInfo caller, Number toButtonId) {
			return CenterCurveRightInner(caller, null, null, toButtonId);
		}
#endregion
#region CenterCurveLeftInner()
		public Object CenterCurveLeftInner(CallerInfo caller, ISlideWait wait, IStep step, Number toButtonId) {
			if (toButtonId.ToButton(caller).Cast<Button>().IsTopSide ()) {
				return CenterCurveClockwiseInner(caller, wait, step, toButtonId);
			}
			return CenterCurveCounterClockwiseInner(caller, wait, step, toButtonId);
		}
		public Object CenterCurveLeftInner(CallerInfo caller, ISlideWait wait, Number toButtonId) {
			return CenterCurveLeftInner(caller, wait, null, toButtonId);
		}
		public Object CenterCurveLeftInner(CallerInfo caller, IStep step, Number toButtonId) {
			return CenterCurveLeftInner(caller, null, step, toButtonId);
		}
		public Object CenterCurveLeftInner(CallerInfo caller, Number toButtonId) {
			return CenterCurveLeftInner(caller, null, null, toButtonId);
		}
#endregion
#region ShapeMaipadDirectStraight()
		public Object ShapeMaipadDirectStraight(CallerInfo caller, ISlideWait wait, IStep step, Sensor start, Sensor target) {
			if (start.entity.IsOuter()) {
				if (target.entity.IsOuter())
					return OuterStraightOuter(caller, wait, step, new Number(start.entity.ToInt().Value), new Number(target.entity.ToInt().Value));
				if (target.entity.IsInner())
					return OuterStraightInner(caller, wait, step, new Number(start.entity.ToInt().Value), new Number(target.entity.ToInt().Value));
				if (target.entity.IsCenter())
					return OuterStraightCenter(caller, wait, step, new Number(start.entity.ToInt().Value));
			}
			if (start.entity.IsInner ()) {
				if (target.entity.IsOuter())
					return InnerStraightOuter(caller, wait, step, new Number(start.entity.ToInt().Value), new Number(target.entity.ToInt().Value));
				if (target.entity.IsInner())
					return InnerStraightInner(caller, wait, step, new Number(start.entity.ToInt().Value), new Number(target.entity.ToInt().Value));
				if (target.entity.IsCenter())
					return InnerStraightCenter(caller, wait, step, new Number(start.entity.ToInt().Value));
			}
			if (start.entity.IsCenter ()) {
				if (target.entity.IsOuter())
					return CenterStraightOuter(caller, wait, step, new Number(target.entity.ToInt().Value));
				if (target.entity.IsInner())
					return CenterStraightInner(caller, wait, step, new Number(target.entity.ToInt().Value));
			}
			return CenterStraightCenter(caller, wait, step);
		}
		public Object ShapeMaipadDirectStraight(CallerInfo caller, ISlideWait wait, Sensor start, Sensor target) {
			return ShapeMaipadDirectStraight(caller, wait, null, start, target);
		}
		public Object ShapeMaipadDirectStraight(CallerInfo caller, IStep step, Sensor start, Sensor target) {
			return ShapeMaipadDirectStraight(caller, null, step, start, target);
		}
		public Object ShapeMaipadDirectStraight(CallerInfo caller, Sensor start, Sensor target) {
			return ShapeMaipadDirectStraight(caller, null, null, start, target);
		}
#endregion
#region ShapeMaipadDirectCurveClockwise()
		public Object ShapeMaipadDirectCurveClockwise(CallerInfo caller, ISlideWait wait, IStep step, Sensor start, Sensor target) {
			if (start.entity.IsOuter()) {
				if (target.entity.IsOuter())
					return OuterCurveClockwiseOuterAxisCenter(caller, wait, step, new Number(start.entity.ToInt().Value), new Number(target.entity.ToInt().Value));
				if (target.entity.IsInner())
					return OuterCurveClockwiseInner(caller, wait, step, new Number(start.entity.ToInt().Value), new Number(target.entity.ToInt().Value));
				if (target.entity.IsCenter())
					return OuterCurveClockwiseCenter(caller, wait, step, new Number(start.entity.ToInt().Value));
			}
			if (start.entity.IsInner ()) {
				if (target.entity.IsOuter())
					return InnerCurveClockwiseOuter(caller, wait, step, new Number(start.entity.ToInt().Value), new Number(target.entity.ToInt().Value));
				if (target.entity.IsInner())
					return InnerCurveClockwiseInnerAxisCenter(caller, wait, step, new Number(start.entity.ToInt().Value), new Number(target.entity.ToInt().Value));
				if (target.entity.IsCenter())
					return InnerCurveClockwiseCenter(caller, wait, step, new Number(start.entity.ToInt().Value));
			}
			if (start.entity.IsCenter ()) {
				if (target.entity.IsOuter())
					return CenterCurveClockwiseOuter(caller, wait, step, new Number(target.entity.ToInt().Value));
				if (target.entity.IsInner())
					return CenterCurveClockwiseInner(caller, wait, step, new Number(target.entity.ToInt().Value));
			}
			return CenterStraightCenter(caller, wait, step);
		}
		public Object ShapeMaipadDirectCurveClockwise(CallerInfo caller, ISlideWait wait, Sensor start, Sensor target) {
			return ShapeMaipadDirectCurveClockwise(caller, wait, null, start, target);
		}
		public Object ShapeMaipadDirectCurveClockwise(CallerInfo caller, IStep step, Sensor start, Sensor target) {
			return ShapeMaipadDirectCurveClockwise(caller, null, step, start, target);
		}
		public Object ShapeMaipadDirectCurveClockwise(CallerInfo caller, Sensor start, Sensor target) {
			return ShapeMaipadDirectCurveClockwise(caller, null, null, start, target);
		}
#endregion
#region ShapeMaipadDirectCurveCounterClockwise()
		public Object ShapeMaipadDirectCurveCounterClockwise(CallerInfo caller, ISlideWait wait, IStep step, Sensor start, Sensor target) {
			if (start.entity.IsOuter()) {
				if (target.entity.IsOuter())
					return OuterCurveCounterClockwiseOuterAxisCenter(caller, wait, step, new Number(start.entity.ToInt().Value), new Number(target.entity.ToInt().Value));
				if (target.entity.IsInner())
					return OuterCurveCounterClockwiseInner(caller, wait, step, new Number(start.entity.ToInt().Value), new Number(target.entity.ToInt().Value));
				if (target.entity.IsCenter())
					return OuterCurveCounterClockwiseCenter(caller, wait, step, new Number(start.entity.ToInt().Value));
			}
			if (start.entity.IsInner ()) {
				if (target.entity.IsOuter())
					return InnerCurveCounterClockwiseOuter(caller, wait, step, new Number(start.entity.ToInt().Value), new Number(target.entity.ToInt().Value));
				if (target.entity.IsInner())
					return InnerCurveCounterClockwiseInnerAxisCenter(caller, wait, step, new Number(start.entity.ToInt().Value), new Number(target.entity.ToInt().Value));
				if (target.entity.IsCenter())
					return InnerCurveCounterClockwiseCenter(caller, wait, step, new Number(start.entity.ToInt().Value));
			}
			if (start.entity.IsCenter ()) {
				if (target.entity.IsOuter())
					return CenterCurveCounterClockwiseOuter(caller, wait, step, new Number(target.entity.ToInt().Value));
				if (target.entity.IsInner())
					return CenterCurveCounterClockwiseInner(caller, wait, step, new Number(target.entity.ToInt().Value));
			}
			return CenterStraightCenter(caller, wait, step);
		}
		public Object ShapeMaipadDirectCurveCounterClockwise(CallerInfo caller, ISlideWait wait, Sensor start, Sensor target) {
			return ShapeMaipadDirectCurveCounterClockwise(caller, wait, null, start, target);
		}
		public Object ShapeMaipadDirectCurveCounterClockwise(CallerInfo caller, IStep step, Sensor start, Sensor target) {
			return ShapeMaipadDirectCurveCounterClockwise(caller, null, step, start, target);
		}
		public Object ShapeMaipadDirectCurveCounterClockwise(CallerInfo caller, Sensor start, Sensor target) {
			return ShapeMaipadDirectCurveCounterClockwise(caller, null, null, start, target);
		}
#endregion
#region ShapeMaipadDirectCurveRight()
		public Object ShapeMaipadDirectCurveRight(CallerInfo caller, ISlideWait wait, IStep step, Sensor start, Sensor target) {
			if (start.entity.IsOuter()) {
				if (target.entity.IsOuter())
					return OuterCurveRightOuterAxisCenter(caller, wait, step, new Number(start.entity.ToInt().Value), new Number(target.entity.ToInt().Value));
				if (target.entity.IsInner())
					return OuterCurveRightInner(caller, wait, step, new Number(start.entity.ToInt().Value), new Number(target.entity.ToInt().Value));
				if (target.entity.IsCenter())
					return OuterCurveRightCenter(caller, wait, step, new Number(start.entity.ToInt().Value));
			}
			if (start.entity.IsInner ()) {
				if (target.entity.IsOuter())
					return InnerCurveRightOuter(caller, wait, step, new Number(start.entity.ToInt().Value), new Number(target.entity.ToInt().Value));
				if (target.entity.IsInner())
					return InnerCurveRightInnerAxisCenter(caller, wait, step, new Number(start.entity.ToInt().Value), new Number(target.entity.ToInt().Value));
				if (target.entity.IsCenter())
					return InnerCurveRightCenter(caller, wait, step, new Number(start.entity.ToInt().Value));
			}
			if (start.entity.IsCenter ()) {
				if (target.entity.IsOuter())
					return CenterCurveRightOuter(caller, wait, step, new Number(target.entity.ToInt().Value));
				if (target.entity.IsInner())
					return CenterCurveRightInner(caller, wait, step, new Number(target.entity.ToInt().Value));
			}
			return CenterStraightCenter(caller, wait, step);
		}
		public Object ShapeMaipadDirectCurveRight(CallerInfo caller, ISlideWait wait, Sensor start, Sensor target) {
			return ShapeMaipadDirectCurveRight(caller, wait, null, start, target);
		}
		public Object ShapeMaipadDirectCurveRight(CallerInfo caller, IStep step, Sensor start, Sensor target) {
			return ShapeMaipadDirectCurveRight(caller, null, step, start, target);
		}
		public Object ShapeMaipadDirectCurveRight(CallerInfo caller, Sensor start, Sensor target) {
			return ShapeMaipadDirectCurveRight(caller, null, null, start, target);
		}
#endregion
#region ShapeMaipadDirectCurveLeft()
		public Object ShapeMaipadDirectCurveLeft(CallerInfo caller, ISlideWait wait, IStep step, Sensor start, Sensor target) {
			if (start.entity.IsOuter()) {
				if (target.entity.IsOuter())
					return OuterCurveLeftOuterAxisCenter(caller, wait, step, new Number(start.entity.ToInt().Value), new Number(target.entity.ToInt().Value));
				if (target.entity.IsInner())
					return OuterCurveLeftInner(caller, wait, step, new Number(start.entity.ToInt().Value), new Number(target.entity.ToInt().Value));
				if (target.entity.IsCenter())
					return OuterCurveLeftCenter(caller, wait, step, new Number(start.entity.ToInt().Value));
			}
			if (start.entity.IsInner ()) {
				if (target.entity.IsOuter())
					return InnerCurveLeftOuter(caller, wait, step, new Number(start.entity.ToInt().Value), new Number(target.entity.ToInt().Value));
				if (target.entity.IsInner())
					return InnerCurveLeftInnerAxisCenter(caller, wait, step, new Number(start.entity.ToInt().Value), new Number(target.entity.ToInt().Value));
				if (target.entity.IsCenter())
					return InnerCurveLeftCenter(caller, wait, step, new Number(start.entity.ToInt().Value));
			}
			if (start.entity.IsCenter ()) {
				if (target.entity.IsOuter())
					return CenterCurveLeftOuter(caller, wait, step, new Number(target.entity.ToInt().Value));
				if (target.entity.IsInner())
					return CenterCurveLeftInner(caller, wait, step, new Number(target.entity.ToInt().Value));
			}
			return CenterStraightCenter(caller, wait, step);
		}
		public Object ShapeMaipadDirectCurveLeft(CallerInfo caller, ISlideWait wait, Sensor start, Sensor target) {
			return ShapeMaipadDirectCurveLeft(caller, wait, null, start, target);
		}
		public Object ShapeMaipadDirectCurveLeft(CallerInfo caller, IStep step, Sensor start, Sensor target) {
			return ShapeMaipadDirectCurveLeft(caller, null, step, start, target);
		}
		public Object ShapeMaipadDirectCurveLeft(CallerInfo caller, Sensor start, Sensor target) {
			return ShapeMaipadDirectCurveLeft(caller, null, null, start, target);
		}
#endregion
#region ShapeMaipadDirectSlide()
		public Object ShapeMaipadDirectSlide(CallerInfo caller, ISlideWait wait, IStep step, Sensor start, MaipadSensor.ShapeType type, Sensor target) {
			if (type == MaipadSensor.ShapeType.RIGHT)
				return ShapeMaipadDirectCurveRight(caller, wait, step, start, target);
			if (type == MaipadSensor.ShapeType.LEFT)
				return ShapeMaipadDirectCurveLeft(caller, wait, step, start, target);
			return ShapeMaipadDirectStraight(caller, wait, step, start, target);
		}
		public Object ShapeMaipadDirectSlide(CallerInfo caller, ISlideWait wait, Sensor start, MaipadSensor.ShapeType type, Sensor target) {
			return ShapeMaipadDirectSlide(caller, wait, null, start, type, target);
		}
		public Object ShapeMaipadDirectSlide(CallerInfo caller, IStep step, Sensor start, MaipadSensor.ShapeType type, Sensor target) {
			return ShapeMaipadDirectSlide(caller, null, step, start, type, target);
		}
		public Object ShapeMaipadDirectSlide(CallerInfo caller, Sensor start, MaipadSensor.ShapeType type, Sensor target) {
			return ShapeMaipadDirectSlide(caller, null, null, start, type, target);
		}
#endregion
#region ShapeMaipadDirectCurve()
		public Object ShapeMaipadDirectCurve(CallerInfo caller, ISlideWait wait, IStep step, Sensor start, Sensor target) {
			if (!start.entity.IsCenter() && !target.entity.IsCenter()) {
				int startb = new Button(start.entity.ToInt().Value).GetIndex();
				int targetb = new Button(target.entity.ToInt().Value).GetIndex();
				if (targetb == (startb + 1) % 8 || targetb == (startb + 2) % 8 || targetb == (startb + 3) % 8)
					return ShapeMaipadDirectCurveClockwise(caller, wait, step, start, target); // 時計回り
				if (targetb == (startb + 5) % 8 || targetb == (startb + 6) % 8 || targetb == (startb + 7) % 8)
					return ShapeMaipadDirectCurveCounterClockwise(caller, wait, step, start, target); // 反時計回り
			}
			return new Exception(caller, ErrorCode.INCOMPREHENSIBLE_CURVE); // エラー
		}
		public Object ShapeMaipadDirectCurve(CallerInfo caller, ISlideWait wait, Sensor start, Sensor target) {
			return ShapeMaipadDirectCurve(caller, wait, null, start, target);
		}
		public Object ShapeMaipadDirectCurve(CallerInfo caller, IStep step, Sensor start, Sensor target) {
			return ShapeMaipadDirectCurve(caller, null, step, start, target);
		}
		public Object ShapeMaipadDirectCurve(CallerInfo caller, Sensor start, Sensor target) {
			return ShapeMaipadDirectCurve(caller, null, null, start, target);
		}
#endregion
#region ShapeSimaiCurve()
		public Object ShapeSimaiCurve(CallerInfo caller, ISlideWait wait, IStep step, Number fromButtonId, Number toButtonId) {
			int start = fromButtonId.ToButton(caller).Cast<Button>().GetIndex();
			int target = toButtonId.ToButton(caller).Cast<Button>().GetIndex();
			if (target == (start + 1) % 8 || target == (start + 2) % 8 || target == (start + 3) % 8)
				return OuterCurveClockwiseOuterAxisCenter(caller, wait, step, fromButtonId, toButtonId); // 時計回り
			if (target == (start + 5) % 8 || target == (start + 6) % 8 || target == (start + 7) % 8)
				return OuterCurveCounterClockwiseOuterAxisCenter(caller, wait, step, fromButtonId, toButtonId); // 反時計回り
			return new Exception(caller, ErrorCode.INCOMPREHENSIBLE_CURVE); // エラー
		}
		public Object ShapeSimaiCurve(CallerInfo caller, ISlideWait wait, Number fromButtonId, Number toButtonId) {
			return ShapeSimaiCurve(caller, wait, null, fromButtonId, toButtonId);
		}
		public Object ShapeSimaiCurve(CallerInfo caller, IStep step, Number fromButtonId, Number toButtonId) {
			return ShapeSimaiCurve(caller, null, step, fromButtonId, toButtonId);
		}
		public Object ShapeSimaiCurve(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return ShapeSimaiCurve(caller, null, null, fromButtonId, toButtonId);
		}
#endregion
#endregion
		public Object Call(CallerInfo caller, Function function, params Object[] arguments) {
			return function.Call(caller, arguments);
		}
		public Object Return(CallerInfo caller, Object returnObject) {
			return new Return(returnObject);
		}
		public Object Return(CallerInfo caller, Object[] returnObjects) {
			return new Return(new Array(returnObjects));
		}
		public Object Arg(CallerInfo caller, Number index) {
			return new Argument(index.entity);
		}
		public Object Arg(CallerInfo caller) {
			return new ArgumentLength();
		}
		public Object ArgLength(CallerInfo caller) {
			return new ArgumentLength();
		}
		public Object Array(CallerInfo caller, Object[] objects) {
			return new Array(objects);
		}
		public Object Table(CallerInfo caller) {
			return new Table();
		}
		public Object Turn(CallerInfo caller, IButtonTurnable turnable, Number amount) {
			return turnable.Turn(caller, amount);
		}
		public Object Turn(CallerInfo caller, IFreeTurnable turnable, IMsqValue degree) {
			return turnable.Turn (caller, degree);
		}
		public Object HMirror(CallerInfo caller, IMirrorable mirrable) {
			return mirrable.MirrorHolizontal(caller);
		}
		public Object VMirror(CallerInfo caller, IMirrorable mirrable) {
			return mirrable.MirrorVertical(caller);
		}
		public Object If(CallerInfo caller, Flag condition, Function yes, Function no) {
			if (condition.entity) {
				yes.Call(caller);
			}
			else {
				no.Call(caller);
			}
			return new Void();
		}
		public Object While(CallerInfo caller, Function condition, Function content) {
			int counter = 0;
			bool checkerError;
			while (MsqHelperWhileConditionChecker(caller, condition, out checkerError)) {
				counter++;
				content.Call(caller);
				if (counter == int.MaxValue) {
					return new Exception(caller, ErrorCode.INFINITE_LOOP);
				}
			}
			if (checkerError)
				return new Exception(caller, ErrorCode.LOOP_CONDITION_TYPE_MISMATCH);
			return new Void();
		}
		public Object Until(CallerInfo caller, Function condition, Function content) {
			int counter = 0;
			bool checkerError;
			while (MsqHelperUntilConditionChecker(caller, condition, out checkerError)) {
				counter++;
				content.Call(caller);
				if (counter == int.MaxValue) {
					return new Exception(caller, ErrorCode.INFINITE_LOOP);
				}
			}
			if (checkerError)
				return new Exception(caller, ErrorCode.LOOP_CONDITION_TYPE_MISMATCH);
			return new Void();
		}
		public Object DoWhile(CallerInfo caller, Function condition, Function content) {
			int counter = 0;
			bool checkerError;
			do {
				counter++;
				content.Call(caller);
				if (counter == int.MaxValue) {
					return new Exception(caller, ErrorCode.INFINITE_LOOP);
				}
			} while (MsqHelperWhileConditionChecker(caller, condition, out checkerError));
			if (checkerError)
				return new Exception(caller, ErrorCode.LOOP_CONDITION_TYPE_MISMATCH);
			return new Void();
		}
		public Object DoUntil(CallerInfo caller, Function condition, Function content) {
			int counter = 0;
			bool checkerError;
			do {
				counter++;
				content.Call(caller);
				if (counter == int.MaxValue) {
					return new Exception(caller, ErrorCode.INFINITE_LOOP);
				}
			} while (MsqHelperUntilConditionChecker(caller, condition, out checkerError));
			if (checkerError)
				return new Exception(caller, ErrorCode.LOOP_CONDITION_TYPE_MISMATCH);
			return new Void();
		}
		// 変数にアクセスできない.
		public Object For(CallerInfo caller, Number times, Function content) {
			for (int i = 0; i < times.entity; i++) {
				content.Call(caller);
			}
			return new Void();
		}
		// 変数にアクセスできる.
		public Object For(CallerInfo caller, Text variantName, Number times, Function content) {
			int counter = 0;
			var index = new Number(0);
			for (variant[variantName.entity] = index; index.entity < times.entity; index.entity++) {
				counter++;
				content.Call(caller);
				if (counter == int.MaxValue) {
					return new Exception(caller, ErrorCode.INFINITE_LOOP);
				}
			}
			return new Void();
		}
		// for条件を改造できる.
		public Object For(CallerInfo caller, Function initialize, Function condition, Function update, Function content) {
			int counter = 0;
			bool checkerError;
			for (initialize.Call(caller); MsqHelperWhileConditionChecker(caller, condition, out checkerError); update.Call(caller)) {
				counter++;
				content.Call(caller);
				if (counter == int.MaxValue) {
					return new Exception(caller, ErrorCode.INFINITE_LOOP);
				}
			}
			if (checkerError)
				return new Exception(caller, ErrorCode.LOOP_CONDITION_TYPE_MISMATCH);
			return new Void();
		}
		
		private const float SimaiPPQQFirstStraightTargetPositionAdjustDegree = 22.5f * 0.65f;
		private const float SimaiPPQQFirstStraightTargetPositionRadius = 0.15f;
		/// <summary>
		/// start - 2 のボタンを得る.
		/// </summary>
		private static Number MsqHelperShapePStartTo(Button start) {
			return new Number((start.GetIndex() + 8 - 2) % 8 + 1);
		}
		/// <summary>
		/// start + 2 のボタンを得る.
		/// </summary>
		private static Number MsqHelperShapePFromTarget(Button target) {
			return new Number((target.GetIndex() + 8 + 2) % 8 + 1);
		}
		/// <summary>
		/// start + 2 のボタンを得る.
		/// </summary>
		private static Number MsqHelperShapeQStartTo(Button start) {
			return MsqHelperShapePFromTarget(start);
		}
		/// <summary>
		/// start - 2 のボタンを得る.
		/// </summary>
		private static Number MsqHelperShapeQFromTarget(Button target) {
			return MsqHelperShapePStartTo(target);
		}
		/// <summary>
		/// 回転せず直線のみで構成する条件を達成するか(shape_p or shape_q).
		/// </summary>
		private static bool MsqHelperShapePOrShapeQIsStraightToTarget(Button start, Button target) {
			return target.GetIndex () == (start.GetIndex () + 4) % 8;
		}
		/// <summary>
		/// 回転せず直線のみで構成する条件を達成するか(outer clockwise inner).
		/// </summary>
		private static bool MsqHelperOuterClockwiseInnerIsStraightToTarget(Button start, Button target) {
			return target.GetIndex () == (start.GetIndex () + 2) % 8;
		}
		/// <summary>
		/// 回転せず直線のみで構成する条件を達成するか(outer counterclockwise inner).
		/// </summary>
		private static bool MsqHelperOuterCounterClockwiseInnerIsStraightToTarget(Button start, Button target) {
			return target.GetIndex () == (start.GetIndex () + 6) % 8;
		}
		/// <summary>
		/// Functionの返り値がFlagでありそれがtrueであるか.
		/// </summary>
		private static bool MsqHelperWhileConditionChecker(CallerInfo caller, Function func, out bool notFlagType) {
			var data = func.Call(caller);
			notFlagType = data.type != Type.FLAG;
			if (!notFlagType)
				return data.Cast<Flag>().entity;
			return false;
		}
		/// <summary>
		/// Functionの返り値がFlagでありそれがfalseであるか.
		/// </summary>
		private static bool MsqHelperUntilConditionChecker(CallerInfo caller, Function func, out bool notFlagType) {
			var data = func.Call(caller);
			notFlagType = data.type != Type.FLAG;
			if (!notFlagType)
				return !data.Cast<Flag>().entity;
			return false;
		}
	}
}
