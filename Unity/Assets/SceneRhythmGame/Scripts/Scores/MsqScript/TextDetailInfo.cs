﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 1文字にインデックス、行、列、の情報を付与したデータ
/// </summary>
public class TextDetailInfo {
	public string entity { get; private set; }
	public int index { get; private set; }
	public int row { get; private set; }
	public int column { get; private set; }
	
	public TextDetailInfo(string entity, int index, int row, int column) {
		this.entity = entity;
		this.index = index;
		this.row = row;
		this.column = column;
	}

	public TextDetailInfo Clone() {
		return new TextDetailInfo(this.entity, this.index, this.row, this.column);
	}
	
	/// <summary>
	/// 文字列からTextDetailInfoインスタンスを作成する
	/// </summary>
	/// <param name="text">文字列</param>
	/// <returns></returns>
	public static TextDetailInfo[] Analyze(string text) {
		int row = 0;
		int column = 0;
		var _ret = new List<TextDetailInfo>();
		int textSize = text.Length;
		for (int i = 0; i < textSize; i++) {
			string entity = text.Substring(i, 1);
			_ret.Add(new TextDetailInfo(entity, i, row, column));
			column++;
			if (entity == "\n") {
				row++;
				column = 0;
			}
		}
		return _ret.ToArray();
	}

	/// <summary>
	/// <para>rowとcolumnを表示する</para>
	/// <para>nullだったら"unknown_place"</para>
	/// </summary>
	public static string GetPlaceLog(TextDetailInfo place) {
		if (place != null) {
			return "{ \"row\":" + (place.row + 1).ToString() +
				", \"column\":" + (place.column + 1).ToString() +
					" }";
		}
		else {
			return "unknown_place";
		}
	}
}