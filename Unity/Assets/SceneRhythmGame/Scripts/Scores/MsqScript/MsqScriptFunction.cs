﻿using System.Collections.Generic;

namespace XMsqScript {
	public class Function : Object {
		public override Type type { get { return Type.FUNCTION; } }
		public override Object Clone() { return new Function(entity); }
		public override string Log() {
			var builder = new System.Text.StringBuilder ();
			for (int i = 0; i < entity.Length; i++) {
				builder.Append (entity[i].ToString());
				if (i < entity.Length - 1) builder.Append(", ");
			}
			return "MsqScript.Function(" + builder.ToString() + ")";
		}
		public Formatter.Room[] entity;
		/// <summary>
		/// 変数群.
		/// </summary>
		public Dictionary<string, Object> variant;
		/// <summary>
		/// 呼び出し元.
		/// </summary>
		public CallerInfo caller;

		public Function(params Formatter.Room[] data) {
			this.entity = data;
			this.variant = new Dictionary<string, Object>();
			//.Clone()を使いたいかなと思って作ったけど、複製する意味はないかもしれない。
			//entity = new Formatter.Room[data.Length];
			//for (int i = 0; i < data.Length; i++) {
			//	entity[i] = data[i].Clone();
			//}
		}
		public Object Call(CallerInfo caller, params Object[] arguments) {
			Object[] contents;
			return Call(caller, out contents, arguments);
		}
		public Object Call(CallerInfo caller, out Object[] contents, params Object[] arguments) {
			var retContents = new List<Object>();
			this.caller = caller;
			foreach (var s in entity) {
				try {
					var obj = Analyze(caller, s, arguments);
					if (obj.IsException) {
						contents = retContents.ToArray();
						return obj;
					}
					else if (obj.type == Type.RETURN) {
						contents = retContents.ToArray();
						return obj.Cast<Return>().entity;
					}
					retContents.Add(obj);
				}
				catch (System.StackOverflowException) {
					contents = retContents.ToArray();
					return new Exception(caller, ErrorCode.STACK_OVERFLOW);
				}
			}
			contents = retContents.ToArray();
			return new Void();
		}
		public Object Analyze(CallerInfo caller, Formatter.Room script, params Object[] arguments) {
			string command = script.nameString;
			Object ret;
			var self = caller.CalledGlobal(script, script.representative, this);
			var global = caller.global;
			if (script.type == Formatter.Room.RoomType.VALUE) {
				ret = global.Parse(self);
			}
			else if (script.type == Formatter.Room.RoomType.SQUARE) {
				if (command == null) {
					ret = Nest(self, script.children, arguments, Document.ARRAY);
				}
				else {
					// 名前付き[]はエラー.
					ret = new Exception(self, ErrorCode.UNNECESSARY_COMMAND_NAME);
				}
			}
			else if (script.type == Formatter.Room.RoomType.CURLY) {
				if (command == null) {
					ret = CreateTable(self, script.children, arguments);
				}
				else {
					// 名前付き{}はエラー.
					ret = new Exception(self, ErrorCode.UNNECESSARY_COMMAND_NAME);
				}
			}
			else if (command == Document.FUNCTION) {
				// functionは子を実行するのではなく単に処理内容を記憶する
				ret = new Function(script.children);
			}
			else {
				ret = Nest(self, script.children, arguments, command);
			}
			// 拡張する.
			var expand = script.expand;
			while (!ret.IsException && expand != null) {
				if (expand.key == Formatter.Expand.Key.NOTHING) {
					var roomType = expand.room.type;
					var commandType = ret.type;
					var place = expand.room.representative;
					string commandName = string.Empty;
					var upgrader = self.CalledExpand(script, place, this, ret);
					if (expand.room.nameString != null) {
						// 宣言無し拡張では名前付き拡張を許さない.
						ret = new Exception(upgrader, ErrorCode.UNNECESSARY_COMMAND_NAME);
						break;
					}
					else if (roomType == Formatter.Room.RoomType.ROUND) {
						if (commandType == Type.FUNCTION) {
							// .call()の省略形となる
							commandName = Document.CALL;
						}
						else {
							// function以外の()アクセスはエラー
							ret = new Exception(upgrader, ErrorCode.INVALID_ROUND_BRACKET_ACCESS);
							break;
						}
					}
					else if (roomType == Formatter.Room.RoomType.SQUARE) {
						if (commandType == Type.ARRAY || commandType == Type.TABLE) {
							// .get()の省略形となる
							commandName = Document.GET;
						}
						else if (ret.IsNote || ret.IsSlideHead || ret.IsSlidePattern) {
							// .option(array())の省略形となる
							var array = Nest(self, expand.room.children, arguments, Document.ARRAY);
							ret = global.ExecuteCommand(upgrader, Document.OPTION, array);
						}
						else {
							// noteかarrayおよびdictionary以外の[]アクセスはエラー
							ret = new Exception(upgrader, ErrorCode.INVALID_SQUARE_BRACKET_ACCESS);
							break;
						}
					}
					else if (roomType == Formatter.Room.RoomType.CURLY) {
						if (ret.IsNote || ret.IsSlideHead || ret.IsSlidePattern) {
							// .option({})の省略形となる
							var table = CreateTable(self, expand.room.children, arguments);
							ret = global.ExecuteCommand(upgrader, Document.OPTION, table);
						}
						// note以外の{}アクセスはエラー
						else {
							ret = new Exception(upgrader, ErrorCode.INVALID_CURLY_BRACKET_ACCESS);
							break;
						}
					}
					if (!ret.IsException && commandName != string.Empty) {
						ret = Nest(upgrader, expand.room.children, arguments, commandName);
					}
				}
				else if (expand.key == Formatter.Expand.Key.DOT) {
					var upgrader = self.CalledExpand(script, expand.room.representative, this, ret);
					if (expand.room.type == Formatter.Room.RoomType.ROUND && expand.room.nameString != null) {
						if (ret.type == Type.GLOBAL && expand.room.nameString == Document.FUNCTION) {
							// globalオブジェクトに対するfunction. 単に処理内容を記憶する.
							ret = new Function(expand.room.children);
						}
						else {
							Function executer = ret.type == Type.GLOBAL ? ret.Cast<Global>().executer : this;
							ret = executer.Nest(upgrader, expand.room.children, arguments, expand.room.nameString);
						}
					}
					else {
						// .アクセスの後は名前付き()しか許さない.
						ret = new Exception(upgrader, ErrorCode.REQUIRED_COMMAND_NAME);
						break;
					}
				}
				else if (expand.key == Formatter.Expand.Key.COLON) {
					// :後の続きはCreateTable自身に任せる.
					return ret;
				}
				expand = expand.room.expand;
			}
			return ret;
		}
		private Object Nest(CallerInfo nester, Formatter.Room[] children, Object[] callArguments, string commandName) {
			Object ret = null;
			var global = nester.global;
			var args = new List<Object>();
			foreach (var child in children) {
				var arg = Analyze(nester, child, callArguments);
				if (!arg.IsException) {
					args.Add(arg);
				}
				else {
					// エラーだった.
					ret = arg;
					break;
				}
			}
			if (ret == null) {
				ret = global.ExecuteCommand(nester, commandName, args.ToArray());
				// 引数型であるならば、call時に呼ばれた引数を返す.
				if (ret.type == Type.ARGUMENT) {
					int argIndex = ret.Cast<Argument>().index;
					if (argIndex >= 0 && argIndex < callArguments.Length) {
						ret = callArguments[argIndex];
					}
					else {
						ret = new Exception(nester, ErrorCode.INDEX_OUT_OF_ARGUMENTS);
					}
				}
				// 引数長型なら、call時に呼ばれた引数の量を返す.
				else if (ret.type == Type.ARGUMENTS_LENGTH) {
					ret = new Number(callArguments.Length);
				}
				// 変数出力型なら、値を取得してそれを返す.未設定ならエラーにする.
				else if (ret.type == Type.VARIANT_GET) {
					ret = GetCommand(nester, ret.Cast<VariantGet>());
				}
				// 変数入力型なら、値を設定してVoidを返す.
				else if (ret.type == Type.VARIANT_SET) {
					ret = SetCommand(nester, ret.Cast<VariantSet>());
				}
				// 関数呼び出し型なら、値を取得して関数を呼び出す.未設定ならエラーにする.
				else if (ret.type == Type.VARIANT_CALL) {
					ret = CallCommand(nester, ret.Cast<VariantCall>());
				}
				// 変数定義確認型なら、値を取得できるかをFlagで返す.
				else if (ret.type == Type.VARIANT_GOT) {
					ret = GotCommand(nester, ret.Cast<VariantGot>());
				}
				// 変数削除型なら、値を削除してVoidを返す.
				else if (ret.type == Type.VARIANT_REMOVE) {
					ret = RemoveCommand(nester, ret.Cast<VariantRemove>());
				}
				// ライブラリ読み込み型なら、ライブラリを読み込んでGlobalを返す.
				else if (ret.type == Type.LIBRARY) {
					ret = LibraryCommand(nester, ret.Cast<Library>());
				}
				// ライブラリマージ型なら、ライブラリを読み込んでマージし、Voidを返す.
				else if (ret.type == Type.INCLUDE) {
					ret = IncludeCommand(nester, ret.Cast<Include>());
				}
			}
			return ret;
		}
		private Object CreateTable(CallerInfo customer, Formatter.Room[] children, Object[] callArguments) {
			Object ret = null;
			var entity = new Dictionary<string, Object>();
			foreach (var child in children) {
				var tableKey = Analyze(customer, child, callArguments);
				if (tableKey.IsKey) {
					var expand = child.expand;
					while (expand != null && expand.key != Formatter.Expand.Key.COLON) {
						expand = expand.room.expand;
					}
					if (expand == null) {
						// MsqScriptFormatterで制御してるから、通常はこの例外は起きない.
						throw new System.Exception("{}内に:がない");
					}
					Object tableValue = Analyze(customer, expand.room, callArguments);
					if (!tableValue.IsException) {
						string key = (tableKey as IKey).key;
						entity.Add (key, tableValue);
					}
					else {
						// エラーだった.
						return tableValue;
					}
				}
				else if (tableKey.IsException) {
					// エラーだった.
					return tableKey;
				}
				else {
					// テーブル作成時のキーがテキストまたはシンボルではないときはエラー.
					return new Exception(customer, ErrorCode.INVALID_TABLE_KEY);
				}
			}
			ret = new Table(entity);
			return ret;
		}

		private Object GetCommand(CallerInfo caller, string key, out bool isDefined) {
			if (this.variant.ContainsKey(key)) {
				isDefined = true;
				return this.variant[key];
			}
			else {
				if (this.caller.called != null && this.caller.called.caller != null) {
					return this.caller.called.GetCommand(caller, key, out isDefined);
				}
				else {
					isDefined = false;
					return new Exception(caller, ErrorCode.UNDEFINED_VARIANT);
				}
			}
		}

		private Object GetCommand(CallerInfo caller, VariantGet cmd) {
			bool isDefined;
			return GetCommand(caller, cmd.key.key, out isDefined);
		}

		private Object CallCommand(CallerInfo caller, VariantCall cmd) {
			var saved = GetCommand(caller, cmd);
			if (saved.IsException)
				return saved;
			if (saved.type == Type.FUNCTION)
				return saved.Cast<Function>().Call(caller, cmd.arguments);
			return new Exception(caller, ErrorCode.TYPE_MISMATCH);
		}

		private Object SetCommand(CallerInfo caller, VariantSet cmd) {
			variant[cmd.key.key] = cmd.value;
			return new Void();
		}
		
		private Object GotCommand(CallerInfo caller, VariantGot cmd) {
			bool isDefined;
			GetCommand(caller, cmd.key.key, out isDefined);
			return new Flag(isDefined);
		}
		
		private Object RemoveCommand(CallerInfo caller, VariantRemove cmd) {
			variant.Remove(cmd.key.key);
			return new Void();
		}

		private Object LibraryCommand(CallerInfo caller, Library cmd) {
			Object result;
			var ret = caller.global.LoadLibrary(cmd.targetName.key, out result);
			if (ret != null) {
				if (result.IsException) {
					return new Exception(caller, ErrorCode.LIBRARY_ERROR);
				}
				return ret;
			}
			return new Exception(caller, ErrorCode.LIBRARY_NOT_FOUND);
		}

		private Object IncludeCommand(CallerInfo caller, Include cmd) {
			var info = LibraryCommand(caller, cmd);
			if (info.IsException) {
				return info;
			}
			caller.global.Marge(caller, info.Cast<Global>(), new Flag(true));
			return new Void();
		}
	}
}