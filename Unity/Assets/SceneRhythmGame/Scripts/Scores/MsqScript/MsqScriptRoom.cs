﻿//#define FormatterTest
using UnityEngine;
using System.Collections;
using XMsqScript;

public class MsqScriptRoom : MonoBehaviour {
	
	// Use this for initialization
	IEnumerator Start () {
#if FormatterTest
		yield return StartCoroutine(LoadTestFile((script)=>{
			Formatter.Result formatResult;
			var detail = Formatter.Analyze(script, //null, null, null, out result);
			                               new CommonScriptFormatter.DefineSpecialText[] {
				new CommonScriptFormatter.DefineSpecialText("/*", "*/", CommonScriptFormatter.DefineSpecialText.ReadMode.COMMENT),
				new CommonScriptFormatter.DefineSpecialText("//", "\n", CommonScriptFormatter.DefineSpecialText.ReadMode.ENDABLE_COMMENT),
				new CommonScriptFormatter.DefineSpecialText("/**", "**/", CommonScriptFormatter.DefineSpecialText.ReadMode.INTERPOSE_COMMENT),
				new CommonScriptFormatter.DefineSpecialText("\"", "\"", CommonScriptFormatter.DefineSpecialText.ReadMode.TEXT),
				new CommonScriptFormatter.DefineSpecialText("\'", "\'", CommonScriptFormatter.DefineSpecialText.ReadMode.TEXT),
				new CommonScriptFormatter.DefineSpecialText("<\"", "\">", CommonScriptFormatter.DefineSpecialText.ReadMode.INTERPOSE_TEXT),
			},
			new CommonScriptFormatter.DefineEscapeText[] {
				new CommonScriptFormatter.DefineEscapeText("\\\"", "\""),
				new CommonScriptFormatter.DefineEscapeText("\\\'", "\'"),
				new CommonScriptFormatter.DefineEscapeText("\\\\", "\\"),
				new CommonScriptFormatter.DefineEscapeText("\\<", "<"),
				new CommonScriptFormatter.DefineEscapeText("\\>", ">"),
				new CommonScriptFormatter.DefineEscapeText("\\n", "\n"),
				new CommonScriptFormatter.DefineEscapeText("\\r", "\r"),
				new CommonScriptFormatter.DefineEscapeText("\\t", "\t"),
				new CommonScriptFormatter.DefineEscapeText("\\f", "\f"),
				new CommonScriptFormatter.DefineEscapeText("\\a", "\a"),
				new CommonScriptFormatter.DefineEscapeText("\\v", "\v"),
			},
			new string[] { "\r", "\n", " ", "\t", "　" },
			out formatResult);
			
			Debug.Log (formatResult.Log());
			Debug.Log (Formatter.Log(detail));
		}));
#else
		yield return StartCoroutine (LoadTestFile ((script) => {
			//UserDataController.instance.library["a"] = "set(k,3);";
			//MsqScriptReader.Read(script);
			//SimaiNoteReader.Read(script);
			CommonScriptFormatter.ReadResult result;
			//SimaiNoteReader.Result sResult;
			var res = SimaiRawReader.Read(script, out result);
			var list = SimaiActiveMessageReader.Read(res.activeMessageTime, res.activeMessageContent, Vector2.zero, 1, UnityEngine.Color.black);
			//var list = SimaiNoteReader.Read(res.inote[6]);
			Debug.Log(list);
		}));
#endif
	}
	
	public static IEnumerator LoadTestFile(System.Action<string> callback) {
		string msqScript;
		//string path =  @"file:///D:/test_simaidata.txt";
		string path =  @"file:///D:/test_rawmaidata.txt";
		using (WWW www = new WWW (path)) {
			yield return www;
			if (string.IsNullOrEmpty(www.error)) {
				msqScript = www.text;
			}
			else {
				Debug.LogError(www.error);
				yield break;
			}
		}
		callback (msqScript);
	}
	
	
}
