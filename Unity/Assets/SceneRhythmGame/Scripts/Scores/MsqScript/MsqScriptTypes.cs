﻿using System;
using System.Collections.Generic;

namespace XMsqScript {
	public enum Type {
		GLOBAL,
		FUNCTION,
		ARGUMENT,
		ARGUMENTS_LENGTH,
		VARIANT_GET,
		VARIANT_SET,
		VARIANT_CALL,
		VARIANT_GOT,
		VARIANT_REMOVE,
		LIBRARY,
		INCLUDE,
		RETURN,
		ORDER,
		VOID,
		ARRAY,
		TABLE,
		NUMBER,
		VALUE,
		FLAG,
		TEXT,
		SENSOR,
		SYMBOL,
		POSITION,
		COLOR,
		BUTTON,
		NOTE_TAP,
		NOTE_HOLD,
		NOTE_SLIDE,
		NOTE_BREAK,
		NOTE_MESSAGE,
		NOTE_SCROLL_MESSAGE,
		NOTE_SOUND_MESSAGE,
		NOTE_TRAP,
		STEP_BASIC,
		STEP_INTERVAL,
		STEP_LOCALBPM,
		STEP_MASS,
		SLIDE_HEAD_STAR,
		SLIDE_HEAD_BREAK,
		SLIDE_HEAD_NOTHING,
		SLIDE_WAIT_DEFAULT,
		SLIDE_WAIT_INTERVAL,
		SLIDE_WAIT_LOCALBPM,
		SLIDE_WAIT_STEP_BASIC,
		SLIDE_WAIT_STEP_LOCALBPM,
		SLIDE_WAIT_MASS,
		SLIDE_PATTERN,
		SLIDE_CHAIN,
		SLIDE_COMMAND_STRAIGHT,
		SLIDE_COMMAND_CURVE,
		SLIDE_COMMAND_CONTINUED_STRAIGHT,
		SLIDE_COMMAND_CONTINUED_CURVE,
		SUCCESS,
		EXCEPTION,
		ARGUMENTS_NOT_MATCH_EXCEPTION,
		COMMAND_NOT_FOUND_EXCEPTION,
	}
	public enum ErrorCode {
		/// <summary>
		/// データが無い
		/// </summary>
		EMPTY,
		/// <summary>
		/// フォーマットエラーなのでXMsqScript.Formatter.Resultを参照せよ
		/// </summary>
		FORMAT_ERROR,
		/// <summary>
		/// 値に変換できなかった
		/// </summary>
		PARSE_ERROR,
		/// <summary>
		/// コマンド名が見つからなかった
		/// </summary>
		COMMAND_NOT_FOUND,
		/// <summary>
		/// 引数が適切なコマンドが見つからなかった
		/// </summary>
		COMMAND_AUGUMENTS_NOT_MATCH,
		/// <summary>
		/// 無効な()アクセス
		/// </summary>
		INVALID_ROUND_BRACKET_ACCESS,
		/// <summary>
		/// 無効な{}アクセス
		/// </summary>
		INVALID_CURLY_BRACKET_ACCESS,
		/// <summary>
		/// 無効な[]アクセス
		/// </summary>
		INVALID_SQUARE_BRACKET_ACCESS,
		/// <summary>
		/// 0で割り算しようとした
		/// </summary>
		DEVIDE_ZERO,
		/// <summary>
		/// Arrayアクセスのインデックスが範囲外である
		/// </summary>
		INDEX_OUT_OF_RANGE,
		/// <summary>
		/// Tableアクセスのキーが見つからなかった
		/// </summary>
		KEY_NOT_FOUND,
		/// <summary>
		/// Argumentアクセスのインデックスが範囲外である
		/// </summary>
		INDEX_OUT_OF_ARGUMENTS,
		/// <summary>
		/// 変数が定義されていなかった
		/// </summary>
		UNDEFINED_VARIANT,
		/// <summary>
		/// 型が合わない
		/// </summary>
		TYPE_MISMATCH,
		/// <summary>
		/// スライドを作る上での情報が不足している
		/// </summary>
		/// slideコマンドとしては正しいが、stepが親に存在しなかったなどでエラーにもしたい
		/// スライドコマンドを作ったあと、チェックしてみて、だめだったらexception型を返す。
		LACK_OF_SLIDE_INFORMATION,
		/// <summary>
		/// スライドヘッドが不適切(ヘッドの代わりに書いていいノートはTapかBreakのみ).
		/// </summary>
		INVALID_SLIDE_HEAD,
		/// <summary>
		/// 最初のスライドコマンドがContinued系なのにHeadが存在しない
		/// </summary>
		HEAD_OF_FIRST_CONTINUED_SLIDE_COMMAND_IS_MISSING,
		/// <summary>
		/// 無限ループに陥った.
		/// </summary>
		INFINITE_LOOP,
		/// <summary>
		/// ループ条件チェックにはFlag型を返すFunctionが必要である
		/// </summary>
		LOOP_CONDITION_TYPE_MISMATCH,
		/// <summary>
		/// コマンド名は不要.
		/// </summary>
		UNNECESSARY_COMMAND_NAME,
		/// <summary>
		/// コマンド名が必要.
		/// </summary>
		REQUIRED_COMMAND_NAME,
		/// <summary>
		/// Table作成時のキーがTextかSymbol以外だった.
		/// </summary>
		INVALID_TABLE_KEY,
		/// <summary>
		/// 再帰呼び出ししすぎ(いわゆるstack overflow).
		/// </summary>
		STACK_OVERFLOW,
		/// <summary>
		/// <para>ShapeSimaiCurve/ShapeMaipadCurveのときtargetがstartから見て対角線上のセンサー番号であるため左右どちらのカーブなのか確定できない.</para>
		/// <para>maipad式スライド直接入力においては、^はCには使えない.</para>
		/// </summary>
		INCOMPREHENSIBLE_CURVE,
		/// <summary>
		/// ライブラリが見つからなかった.
		/// </summary>
		LIBRARY_NOT_FOUND,
		/// <summary>
		/// ライブラリ内でエラーが見つかった.
		/// </summary>
		LIBRARY_ERROR,
	}
	
	public enum OrderTitle {
		SET_BPM,
		SET_BEAT,
		SET_INTERVAL,
		SET_NOTES,
		SET_REST,
		EXTRA_FORMAT,
	}
	
	public interface IObject {
		Type type { get; }
		Object Clone();
		T Cast<T>() where T : Object;
		string Log();
		bool IsNaN { get; }
		bool IsKey { get; }
		bool IsException { get; }
		bool IsOrder { get; }
		bool IsVoid { get; }
		bool IsSuccess { get; }
		bool IsNote { get; }
		bool IsStep { get; }
		bool IsSlideHead { get; }
		bool IsSlideWait { get; }
		bool IsSlidePattern { get; }
		bool IsSlideChain { get; }
		bool IsSlideCommand { get; }
	}
	
	public interface IMsqValue : IObject {
		float value { get; set; }
	}
	public interface IKey : IObject {
		string key { get; }
	}
	public interface IMirrorable : IObject {
		Object MirrorHolizontal(CallerInfo caller);
		Object MirrorVertical(CallerInfo caller);
	}
	public interface IButtonTurnable : IObject {
		Object Turn(CallerInfo caller, Number amount);
	}
	public interface IFreeTurnable : IObject {
		Object Turn(CallerInfo caller, IMsqValue amount);
	}
	public interface IOptionable : IObject {
		Table GetOption();
		Object Option(CallerInfo caller, Table content);
		Object Option(CallerInfo caller, Array content);
	}
	public interface INote : IOptionable {
	}
	public interface IStep : ISlideWait {
		
	}
	public interface ISlideHead : IOptionable, IButtonTurnable, IMirrorable {
		int? star { get; }
	}
	public interface ISlideWait : IObject {
		bool IsMass { get; }
	}
	public interface ISlidePattern : IOptionable, IButtonTurnable, IFreeTurnable, IMirrorable {
		ISlideWait wait { get; set; }
		IStep step { get; set; }
	}
	public interface ISlideChain : ISlidePattern {
	}
	public interface ISlideCommand : ISlideChain {
		bool IsStraight { get; }
		bool IsCurve { get; }
		bool IsSlideContinuedCommand { get; }
	}
	public interface ISlideContinuedCommand : ISlideCommand {
		ISlideTrimmedCommand GetEntity(ISlideHead head, ISlideTrimmedCommand beforeCommand);
	}
	public interface ISlideTrimmedCommand : ISlideCommand {
	}
	
	public abstract class Object : IObject {
		public abstract Type type { get; }
		public abstract Object Clone();
		public T Cast<T>() where T : Object {
			return (T)this;
		}
		public virtual string Log() {
			return "MsqScript.Object()";
		}
		public virtual bool IsNaN { get { return true; } }
		public virtual bool IsKey { get { return false; } }
		public virtual bool IsException { get { return false; } }
		public virtual bool IsOrder { get { return false; } }
		public virtual bool IsVoid { get { return false; } }
		public virtual bool IsSuccess { get { return false; } }
		public virtual bool IsNote { get { return false; } }
		public virtual bool IsStep { get { return false; } }
		public virtual bool IsSlideHead { get { return false; } }
		public virtual bool IsSlideWait { get { return false; } }
		public virtual bool IsSlidePattern { get { return false; } }
		public virtual bool IsSlideChain { get { return false; } }
		public virtual bool IsSlideCommand { get { return false; } }
	}
	
	public class Void : Object {
		public override Type type { get { return Type.VOID; } }
		public override Object Clone() { return new Void(); }
		public override string Log() {
			return "MsqScript.Void()";
		}
		public override bool IsVoid { get { return true; } }
	}
	
	public class Return : Object {
		public override Type type { get { return Type.RETURN; } }
		public override Object Clone() { return new Return(entity); }
		public override string Log() {
			return "MsqScript.Return(" + entity.Log() + ")";
		}
		public Object entity;
		public Return(Object data) {
			entity = data;
		}
	}
	
	public class VariantGet : Object {
		public override Type type { get { return Type.VARIANT_GET; } }
		public override Object Clone() { return new VariantGet(key.Clone() as IKey); }
		public override string Log() {
			return "MsqScript.VariantGet(" + key + ")";
		}
		public IKey key;
		public VariantGet(IKey key) {
			this.key = key;
		}
	}
	
	public class VariantSet : Object {
		public override Type type { get { return Type.VARIANT_SET; } }
		public override Object Clone() { return new VariantSet(key.Clone() as IKey, value.Clone()); }
		public override string Log() {
			return "MsqScript.VariantSet(key = " + key + ", value = " + value.Log() + ")";
		}
		public IKey key;
		public Object value;
		public VariantSet(IKey key, Object value) {
			this.key = key;
			this.value = value;
		}
	}
	
	public class VariantCall : VariantGet {
		public override Type type { get { return Type.VARIANT_CALL; } }
		public override Object Clone() { return new VariantCall(key.Clone() as IKey, arguments); }
		public override string Log() {
			return "MsqScript.VariantCall(" + key + ")";
		}
		public Object[] arguments;
		public VariantCall(IKey key, params Object[] arguments) : base(key) { this.arguments = arguments; }
	}
	
	public class VariantGot : VariantGet {
		public override Type type { get { return Type.VARIANT_GOT; } }
		public override Object Clone() { return new VariantGot(key.Clone() as IKey); }
		public override string Log() {
			return "MsqScript.VariantGot(" + key + ")";
		}
		public VariantGot(IKey key) : base(key) { }
	}
	
	public class VariantRemove : VariantGet {
		public override Type type { get { return Type.VARIANT_REMOVE; } }
		public override Object Clone() { return new VariantRemove(key.Clone() as IKey); }
		public override string Log() {
			return "MsqScript.VariantRemove(" + key + ")";
		}
		public VariantRemove(IKey key) : base(key) { }
	}
	
	/// <summary>
	/// 外部ライブラリ読み込み.  TODO::譜面とは別に、ライブラリとしてmsqscriptを保存する場所を用意したい.
	/// </summary>
	public class Library : Object {
		public override Type type { get { return Type.LIBRARY; } }
		public override string Log() {
			return "MsqScript.Library(" + targetName.key + ")";
		}
		public override Object Clone() {
			return new Library(place.Clone(), targetName.Clone() as IKey);
		}
		public TextDetailInfo place;
		public IKey targetName { get; set; }
		public Library(TextDetailInfo place, IKey targetName) { this.place = place; this.targetName = targetName; }
	}
	
	/// <summary>
	/// 外部ライブラリを読み込んで、グローバルに上書きマージ."marge( lib() )"の短縮形.
	/// </summary>
	public class Include : Library {
		public override Type type { get { return Type.INCLUDE; } }
		public override string Log() {
			return "MsqScript.Include(" + targetName.key + ")";
		}
		public override Object Clone() {
			return new Include(place.Clone(), targetName.Clone() as IKey);
		}
		public Include(TextDetailInfo place, IKey targetName) : base(place, targetName) { }
	}
	
	public class Argument : Object {
		public override Type type { get { return Type.ARGUMENT; } }
		public override Object Clone() { return new Argument(index); }
		public override string Log() {
			return "MsqScript.Argument(" + index.ToString() + ")";
		}
		public int index;
		public Argument(int data) {
			index = data;
		}
	}
	
	public class ArgumentLength : Object {
		public override Type type { get { return Type.ARGUMENTS_LENGTH; } }
		public override Object Clone() { return new ArgumentLength(); }
		public override string Log() {
			return "MsqScript.ArgumentLength()";
		}
		public ArgumentLength() {
		}
	}
	
	public class Number : Object, IMsqValue, IKey {
		public override Type type { get { return Type.NUMBER; } }
		public override Object Clone() { return new Number(entity); }
		public override string Log() {
			return "MsqScript.Number(" + entity.ToString() + ")";
		}
		public override bool IsNaN { get { return false; } }
		public override bool IsKey { get { return true; } }
		public int entity;
		public float value { get { return entity; } set { entity = (int)value; } }
		public string key { get { return entity.ToString(); } }
		public Number(int data) {
			entity = data;
		}
		public Object Add(CallerInfo caller, Number a0) {
			entity += a0.entity;
			return this;
		}
		public Object Sub(CallerInfo caller, Number a0) {
			entity -= a0.entity;
			return this;
		}
		public Object Mul(CallerInfo caller, Number a0) {
			entity *= a0.entity;
			return this;
		}
		public Object Div(CallerInfo caller, Number a0) {
			if (a0.entity != 0) {
				entity /= a0.entity;
				return this;
			}
			return new Exception(caller, ErrorCode.DEVIDE_ZERO);
		}
		public Object Pow(CallerInfo caller, Number a0) {
			entity = (int)UnityEngine.Mathf.Pow(entity, a0.entity);
			return this;
		}
		public Object Mod(CallerInfo caller, Number a0) {
			if (a0.entity != 0) {
				entity %= a0.entity;
				return this;
			}
			return new Exception(caller, ErrorCode.DEVIDE_ZERO);
		}
		public Object Equal(CallerInfo caller, Number cmp) {
			return new Flag(entity == cmp.entity);
		}
		public Object NotEqual(CallerInfo caller, Number cmp) {
			return new Flag(entity != cmp.entity);
		}
		public Object Greater(CallerInfo caller, Number cmp) {
			return new Flag(entity > cmp.entity);
		}
		public Object Less(CallerInfo caller, Number cmp) {
			return new Flag(entity < cmp.entity);
		}
		public Object GreaterEqual(CallerInfo caller, Number cmp) {
			return new Flag(entity >= cmp.entity);
		}
		public Object LessEqual(CallerInfo caller, Number cmp) {
			return new Flag(entity <= cmp.entity);
		}
		public Object Inc(CallerInfo caller) {
			entity++;
			return this;
		}
		public Object Dec(CallerInfo caller) {
			entity--;
			return this;
		}
		public Object BitAnd(CallerInfo caller, Number a0) {
			entity &= a0.entity;
			return this;
		}
		public Object BitOr(CallerInfo caller, Number a0) {
			entity |= a0.entity;
			return this;
		}
		public Object BitXor(CallerInfo caller, Number a0) {
			entity ^= a0.entity;
			return this;
		}
		public Object BitNot(CallerInfo caller) {
			entity = ~entity;
			return this;
		}
		public Object BitShiftLeft(CallerInfo caller, Number a0) {
			entity <<= a0.entity;
			return this;
		}
		public Object BitShiftRight(CallerInfo caller, Number a0) {
			entity >>= a0.entity;
			return this;
		}
		public Object ToText(CallerInfo caller) {
			return new Text(key);
		}
		public Object ToValue(CallerInfo caller) {
			return new Value(entity);
		}
		public Object ToButton(CallerInfo caller) {
			return new Button(this);
		}
	}
	
	public class Value : Object, IMsqValue {
		public override Type type { get { return Type.VALUE; } }
		public override Object Clone() { return new Value(entity); }
		public override string Log() {
			return "MsqScript.Value(" + entity.ToString() + ")";
		}
		public override bool IsNaN { get { return false; } }
		public float entity;
		public float value { get { return entity; } set { entity = value; } }
		public Value(float data) {
			entity = data;
		}
		public Object Add(CallerInfo caller, IMsqValue a0) {
			entity += a0.value;
			return this;
		}
		public Object Sub(CallerInfo caller, IMsqValue a0) {
			entity -= a0.value;
			return this;
		}
		public Object Mul(CallerInfo caller, IMsqValue a0) {
			entity *= a0.value;
			return this;
		}
		public Object Div(CallerInfo caller, IMsqValue a0) {
			if (a0.value != 0) {
				entity /= a0.value;
				return this;
			}
			return new Exception(caller, ErrorCode.DEVIDE_ZERO);
		}
		public Object Pow(CallerInfo caller, IMsqValue a0) {
			entity = (int)UnityEngine.Mathf.Pow(entity, a0.value);
			return this;
		}
		public Object ToNumber(CallerInfo caller) {
			return new Number(UnityEngine.Mathf.FloorToInt(entity));
		}
		public Object Equal(CallerInfo caller, IMsqValue cmp) {
			return new Flag(entity == cmp.value);
		}
		public Object NotEqual(CallerInfo caller, IMsqValue cmp) {
			return new Flag(entity != cmp.value);
		}
		public Object Greater(CallerInfo caller, IMsqValue cmp) {
			return new Flag(entity > cmp.value);
		}
		public Object Less(CallerInfo caller, IMsqValue cmp) {
			return new Flag(entity < cmp.value);
		}
		public Object GreaterEqual(CallerInfo caller, IMsqValue cmp) {
			return new Flag(entity >= cmp.value);
		}
		public Object LessEqual(CallerInfo caller, IMsqValue cmp) {
			return new Flag(entity <= cmp.value);
		}
		public Object Inc(CallerInfo caller) {
			entity++;
			return this;
		}
		public Object Dec(CallerInfo caller) {
			entity--;
			return this;
		}
		public Object ToText(CallerInfo caller) {
			return new Text(entity.ToString());
		}
	}
	
	public class Flag : Object, IKey {
		public override Type type { get { return Type.FLAG; } }
		public override Object Clone() { return new Flag(entity); }
		public override string Log() {
			return "MsqScript.Flag(" + entity.ToString() + ")";
		}
		public override bool IsKey { get { return true; } }
		public bool entity;
		public string key { get { return entity.ToString(); } }
		public Flag(bool data) {
			entity = data;
		}
		public Flag Not(CallerInfo caller) {
			entity = !entity;
			return this;
		}
		public Object ToText(CallerInfo caller) {
			return new Text(key);
		}
	}
	
	public class Text : Object, IKey {
		public override Type type { get { return Type.TEXT; } }
		public override Object Clone() { return new Text(entity); }
		public override string Log() {
			return "MsqScript.Text(" + entity + ")";
		}
		public override bool IsKey { get { return true; } }
		public string entity;
		public string key { get { return entity; } }
		public Text(string data) {
			entity = data;
		}
		public Object Concat(CallerInfo caller, Text a0) {
			entity += a0.entity;
			return this;
		}
		public Object Equal(CallerInfo caller, Text cmp) {
			return new Flag(entity == cmp.entity);
		}
		public Object NotEqual(CallerInfo caller, Text cmp) {
			return new Flag(entity != cmp.entity);
		}
		public Object Length(CallerInfo caller) {
			return new Number(entity.Length);
		}
		public Object ToText(CallerInfo caller) {
			return this;
		}
		public Object ToNumber(CallerInfo caller) {
			int number;
			if (int.TryParse(entity, out number)) {
				return new Number(number);
			}
			return new Exception(caller, ErrorCode.PARSE_ERROR);
		}
		public Object ToValue(CallerInfo caller) {
			float value;
			if (float.TryParse(entity, out value)) {
				return new Value(value);
			}
			return new Exception(caller, ErrorCode.PARSE_ERROR);
		}
		public Object ToFlag(CallerInfo caller) {
			switch (entity.ToLower()) {
			case Document.TRUE:
			case Document.YES:
			case Document.ON:
				return new Flag(true);
			case Document.FALSE:
			case Document.NO:
			case Document.OFF:
				return new Flag(false);
			}
			return new Exception(caller, ErrorCode.PARSE_ERROR);
		}
		public Object ToSensor(CallerInfo caller) {
			var sensorId = MaipadSensor.ToMaipadSensorId(entity);
			if (sensorId != MaipadSensor.Id.UNKNOWN) {
				return new Sensor(sensorId);
			}
			return new Exception(caller, ErrorCode.PARSE_ERROR);
		}
		public Object ToSymbol(CallerInfo caller) {
			return new Symbol(entity);
		}
		public Object Parse(CallerInfo caller) {
			var ret = ToNumber(caller);
			if (ret.IsException)
				ret = ToValue(caller);
			if (ret.IsException)
				ret = ToFlag(caller);
			if (ret.IsException)
				ret = ToSensor(caller);
			return ret;
		}
	}

	public class Sensor : Object, IKey, IButtonTurnable, IMirrorable {
		public override Type type { get { return Type.SENSOR; } }
		public override Object Clone() { return new Sensor(entity); }
		public override string Log() {
			return "MsqScript.Sensor(" + MaipadSensor.ToString(entity) + ")";
		}
		public override bool IsKey { get { return true; } }
		public MaipadSensor.Id entity;
		public string key { get { return MaipadSensor.ToString(entity); } }
		public Sensor(MaipadSensor.Id data) {
			entity = data;
		}
		public Object IsOuter(CallerInfo caller) {
			return new Flag(entity.IsOuter());
		}
		public Object IsInner(CallerInfo caller) {
			return new Flag(entity.IsInner());
		}
		public Object IsCenter(CallerInfo caller) {
			return new Flag(entity.IsCenter());
		}
		public Object Is1(CallerInfo caller) {
			return new Flag(entity.Is1());
		}
		public Object Is2(CallerInfo caller) {
			return new Flag(entity.Is2());
		}
		public Object Is3(CallerInfo caller) {
			return new Flag(entity.Is3());
		}
		public Object Is4(CallerInfo caller) {
			return new Flag(entity.Is4());
		}
		public Object Is5(CallerInfo caller) {
			return new Flag(entity.Is5());
		}
		public Object Is6(CallerInfo caller) {
			return new Flag(entity.Is6());
		}
		public Object Is7(CallerInfo caller) {
			return new Flag(entity.Is7());
		}
		public Object Is8(CallerInfo caller) {
			return new Flag(entity.Is8());
		}
		public Object Equal(CallerInfo caller, Sensor cmp) {
			return new Flag(entity == cmp.entity);
		}
		public Object NotEqual(CallerInfo caller, Sensor cmp) {
			return new Flag(entity != cmp.entity);
		}
		public Object ToText(CallerInfo caller) {
			return new Text(key);
		}
		public int CalcTurn(int amount, int? buttonId = null) {
			if (!buttonId.HasValue) {
				buttonId = entity.ToInt();
				if (!buttonId.HasValue) {
					return 0;
				}
			}
			if (amount < 0) {
				int div = -amount / 8;
				int mod = -amount % 8;
				amount = 8 * (div + 1) - mod;
			}
			return (buttonId.Value - 1 + amount) % 8 + 1;
		}
		public int CalcMirrorHolizontal(int? buttonId = null) {
			if (!buttonId.HasValue) {
				buttonId = entity.ToInt();
				if (!buttonId.HasValue) {
					return 0;
				}
			}
			return 9 - buttonId.Value;
		}
		public int CalcMirrorVertical(int? buttonId = null) {
			if (!buttonId.HasValue) {
				buttonId = entity.ToInt();
				if (!buttonId.HasValue) {
					return 0;
				}
			}
			return CalcMirrorHolizontal(CalcTurn(buttonId.Value, 4));
		}
		public Object Turn(CallerInfo caller, Number amount) {
			if (entity.IsCenter()) return this;
			var i = CalcTurn(amount.entity);
			entity = entity.OtherDeg(i);
			return this;
		}
		public Object MirrorHolizontal(CallerInfo caller) {
			if (entity.IsCenter()) return this;
			var i = CalcMirrorHolizontal();
			entity = entity.OtherDeg(i);
			return this;
		}
		public Object MirrorVertical(CallerInfo caller) {
			if (entity.IsCenter()) return this;
			var i = CalcMirrorVertical();
			entity = entity.OtherDeg(i);
			return this;
		}
	}
	
	public class Symbol : Object, IKey {
		public override Type type { get { return Type.SYMBOL; } }
		public override Object Clone() { return new Symbol(entity); }
		public override string Log() {
			return "MsqScript.Symbol(" + entity + ")";
		}
		public override bool IsKey { get { return true; } }
		public string entity;
		public string key { get { return entity; } }
		public Symbol(string data) {
			entity = data;
		}
		public Object Equal(CallerInfo caller, Symbol cmp) {
			return new Flag(entity == cmp.entity);
		}
		public Object NotEqual(CallerInfo caller, Symbol cmp) {
			return new Flag(entity != cmp.entity);
		}
		public Object Length(CallerInfo caller) {
			return new Number(entity.Length);
		}
		public Object ToText(CallerInfo caller) {
			return new Text(entity);
		}
	}
	
	public class Position : Object, IButtonTurnable, IFreeTurnable, IMirrorable {
		public override Type type { get { return Type.POSITION; } }
		public override Object Clone() { return new Position(entity); }
		public override string Log() {
			return "MsqScript.Position(x = " + entity.x.ToString() + ", y = " + entity.y.ToString() + ")";
		}
		public UnityEngine.Vector2 entity;
		public Position(float x, float y) {
			entity = new UnityEngine.Vector2(x, y);
		}
		public Position(UnityEngine.Vector2 data) {
			entity = data;
		}
		public Position(IMsqValue x, IMsqValue y) : this(x.value, y.value) {
		}
		public Object Set(CallerInfo caller, IMsqValue x, IMsqValue y) {
			entity.Set(x.value, y.value);
			return this;
		}
		public Object GetX(CallerInfo caller) {
			return new Value(entity.x);
		}
		public Object GetY(CallerInfo caller) {
			return new Value(entity.y);
		}
		public Object SetX(CallerInfo caller, IMsqValue x) {
			entity.x = x.value;
			return this;
		}
		public Object SetY(CallerInfo caller, IMsqValue y) {
			entity.y = y.value;
			return this;
		}
		public Object Equal(CallerInfo caller, Position cmp) {
			return new Flag(entity == cmp.entity);
		}
		public Object NotEqual(CallerInfo caller, Position cmp) {
			return new Flag(entity != cmp.entity);
		}
		public Object Turn(CallerInfo caller, Number amount) {
			return Turn(caller, new Value(Constants.instance.GetPieceDegree (amount.entity) - 22.5f));
		}
		public Object Turn(CallerInfo caller, IMsqValue degree) {
			// 左手座標.
			var pos = entity;
			// 右手座標.
			pos = Constants.instance.ToLeftHandedCoordinateSystemPosition (pos);
			float addDeg = degree.value;
			float deg = CircleCalculator.PointToDegree (pos) + 180.0f;
			float radius = CircleCalculator.PointToPointDistance (UnityEngine.Vector2.zero, pos);
			var turned = CircleCalculator.PointOnCircle (UnityEngine.Vector2.zero, radius, deg + addDeg);
			// 左手座標.
			var ret = Constants.instance.ToLeftHandedCoordinateSystemPosition (turned);
			entity = ret;
			return this;
		}
		public Object MirrorHolizontal(CallerInfo caller) {
			entity.x *= -1;
			return this;
		}
		public Object MirrorVertical(CallerInfo caller) {
			entity.y *= -1;
			return this;
		}
	}
	
	public class Color : Object {
		public override Type type { get { return Type.COLOR; } }
		public override Object Clone() { return new Color(entity); }
		public override string Log() {
			var builder = new System.Text.StringBuilder();
			float[] data = new float[] { entity.r, entity.g, entity.b, entity.a };
			string[] names = new string[] { "r", "g", "b", "a" };
			builder.Append("MsqScript.Color(");
			for (int i = 0; i < data.Length; i++) {
				if (i > 0) {
					builder.Append(", ");
				}
				int output = (int)(data[i] * 255.0f);
				builder.Append(names[i]);
				builder.Append(" = ");
				builder.Append(output.ToString());
			}
			builder.Append(")");
			return builder.ToString();
		}
		public UnityEngine.Color entity;
		public Color(UnityEngine.Color data) {
			entity = data;
		}
		public Color(float r, float g, float b, float a) {
			float[] elms = new float[] { r, g, b, a };
			for (int i = 0; i < elms.Length; i++) {
				if (elms[i] < 0.0f)
					elms[i] = 0.0f;
				if (elms[i] > 1.0f)
					elms[i] = 1.0f;
			}
			entity = new UnityEngine.Color(r, g, b, a);
		}
		public Color(int r, int g, int b, int a) {
			float[] elms = new float[] { r, g, b, a };
			for (int i = 0; i < elms.Length; i++) {
				if (elms[i] < 0.0f)
					elms[i] = 0.0f;
				if (elms[i] > 255.0f)
					elms[i] = 255.0f;
			}
			entity = new UnityEngine.Color(r / 255.0f, g / 255.0f, b / 255.0f, a / 255.0f);
		}
		public Object SetR(CallerInfo caller, IMsqValue r) {
			entity.r = r.value;
			return this;
		}
		public Object SetG(CallerInfo caller, IMsqValue g) {
			entity.g = g.value;
			return this;
		}
		public Object SetB(CallerInfo caller, IMsqValue b) {
			entity.b = b.value;
			return this;
		}
		public Object SetA(CallerInfo caller, IMsqValue a) {
			entity.a = a.value;
			return this;
		}
		public Object GetR(CallerInfo caller) {
			return new Value(entity.r);
		}
		public Object GetG(CallerInfo caller) {
			return new Value(entity.g);
		}
		public Object GetB(CallerInfo caller) {
			return new Value(entity.b);
		}
		public Object GetA(CallerInfo caller) {
			return new Value(entity.a);
		}
		public Object SetR255(CallerInfo caller, Number r) {
			float v = r.entity;
			if (v < 0) v = 0;
			if (v > 255) v = 255;
			entity.r = v / 255.0f;
			return this;
		}
		public Object SetG255(CallerInfo caller, Number g) {
			float v = g.entity;
			if (v < 0)
				v = 0;
			if (v > 255)
				v = 255;
			entity.g = v / 255.0f;
			return this;
		}
		public Object SetB255(CallerInfo caller, Number b) {
			float v = b.entity;
			if (v < 0)
				v = 0;
			if (v > 255)
				v = 255;
			entity.b = v / 255.0f;
			return this;
		}
		public Object SetA255(CallerInfo caller, Number a) {
			float v = a.entity;
			if (v < 0)
				v = 0;
			if (v > 255)
				v = 255;
			entity.a = v / 255.0f;
			return this;
		}
		public Object GetR255(CallerInfo caller) {
			return new Number((int)(entity.r * 255.0f));
		}
		public Object GetG255(CallerInfo caller) {
			return new Number((int)(entity.g * 255.0f));
		}
		public Object GetB255(CallerInfo caller) {
			return new Number((int)(entity.b * 255.0f));
		}
		public Object GetA255(CallerInfo caller) {
			return new Number((int)(entity.a * 255.0f));
		}
	}
	
	public class Button : Object, IButtonTurnable, IMirrorable {
		public override Type type { get { return Type.BUTTON; } }
		public override Object Clone() { return new Button(entity); }
		public override string Log() {
			return "MsqScript.Button(" + entity.ToString() + ")";
		}
		public int entity;
		public Button(int data) {
			int b = data - 1;
			if (b < 0)
				b = 0;
			if (b > 7)
				b = 7;
			entity = b + 1;
		}
		public Button(Number data) : this(data.entity) { }
		public int GetId() {
			return entity;
		}
		public int GetIndex() {
			return entity - 1;
		}
		public bool IsTopSide() {
			return entity < 3 || entity > 6;
		}
		public bool IsBottomSide() {
			return entity > 2 && entity < 7;
		}
		public bool IsLeftSide() {
			return entity < 5;
		}
		public bool IsRightSide() {
			return entity > 4;
		}
		public int CalcTurn(int amount, int? buttonId = null) {
			if (!buttonId.HasValue) {
				buttonId = GetId();
			}
			if (amount < 0) {
				int div = -amount / 8;
				int mod = -amount % 8;
				amount = 8 * (div + 1) - mod;
			}
			return (buttonId.Value - 1 + amount) % 8 + 1;
		}
		public int CalcMirrorHolizontal(int? buttonId = null) {
			if (!buttonId.HasValue) {
				buttonId = GetId();
			}
			return 9 - buttonId.Value;
		}
		public int CalcMirrorVertical(int? buttonId = null) {
			if (!buttonId.HasValue) {
				buttonId = GetId();
			}
			return CalcMirrorHolizontal(CalcTurn(buttonId.Value, 4));
		}
		
		public Object Turn(CallerInfo caller, Number amount) {
			entity = CalcTurn(amount.entity);
			return this;
		}
		public Object MirrorHolizontal(CallerInfo caller) {
			entity = CalcMirrorHolizontal();
			return this;
		}
		public Object MirrorVertical(CallerInfo caller) {
			entity = CalcMirrorVertical();
			return this;
		}
		public Object ToNumber(CallerInfo caller) {
			return new Number(entity);
		}
	}
	
	public class NoteTap : Object, INote, IButtonTurnable, IMirrorable {
		public override Type type { get { return Type.NOTE_TAP; } }
		public override Object Clone() {
			var ret = new NoteTap(button.entity);
			ret.options = options.Clone().Cast<Table>();
			return ret;
		}
		public override string Log() {
			var builder = new System.Text.StringBuilder();
			builder.Append ("MsqScript.NoteTap(");
			builder.Append (button.Log());
			builder.Append (")");
			if (options.entity.Count > 0) {
				builder.Append (".Option(");
				builder.Append (options.Log());
				builder.Append (")");
			}
			return builder.ToString();
		}
		public override bool IsNote { get { return true; } }
		public NoteTap(int buttonId) { button = new Button(buttonId); options = new Table(); }
		public Button button;
		public Table options;
		public Table GetOption() { return options; }
		public Object Option(CallerInfo caller, Table content) {
			foreach (var key in content.entity.Keys) {
				var value = content.entity[key];
				if (key == Document.NoteOption.SECRET) {
					if (value.IsKey) {
						value = new Text((value as IKey).key).Parse(null);
					}
					if (value.type == Type.FLAG) {
						options.entity[key] = value;
					}
				}
				else if ((key == Document.NoteOption.TIME ||
				          key == Document.NoteOption.FADE ||
				          key == Document.NoteOption.FADE_RATE ||
				          key == Document.NoteOption.MOVE ||
				          key == Document.NoteOption.MOVE_RATE ||
				          key == Document.NoteOption.SIZE ||
				          key == Document.NoteOption.SIZE_RATE)) {
					if (value.IsKey) {
						value = new Text((value as IKey).key).Parse(null);
					}
					if (!value.IsNaN) {
						options.entity[key] = value;
					}
				}
				else if (key == Document.NoteOption.SHAPE) {
					string text = null;
					if (value.IsKey) {
						text = (value as IKey).key;
					}
					if (text == Document.NoteOption.RING ||
					    text == Document.NoteOption.SINGLE_RING ||
					    text == Document.NoteOption.EACH_RING) {
						options.entity[key] = new Text(Document.NoteOption.RING);
					}
					else if (text == Document.NoteOption.STAR ||
					         text == Document.NoteOption.SINGLE_STAR ||
					         text == Document.NoteOption.EACH_STAR) {
						options.entity[key] = new Text(Document.NoteOption.STAR);
					}
					else if (text == Document.NoteOption.WSTAR ||
					         text == Document.NoteOption.SINGLE_WSTAR ||
					         text == Document.NoteOption.EACH_WSTAR) {
						options.entity[key] = new Text(Document.NoteOption.WSTAR);
					}
					if (text == Document.NoteOption.PINK_RING ||
					    text == Document.NoteOption.SINGLE_RING ||
					    text == Document.NoteOption.BLUE_STAR ||
					    text == Document.NoteOption.SINGLE_STAR ||
					    text == Document.NoteOption.BLUE_WSTAR ||
					    text == Document.NoteOption.SINGLE_WSTAR) {
						options.entity[Document.NoteOption.COLOR] = new Text(Document.NoteOption.SINGLE);
					}
					else if (text == Document.NoteOption.YELLOW_RING ||
					         text == Document.NoteOption.EACH_RING ||
					         text == Document.NoteOption.YELLOW_STAR ||
					         text == Document.NoteOption.EACH_STAR ||
					         text == Document.NoteOption.YELLOW_WSTAR ||
					         text == Document.NoteOption.EACH_WSTAR) {
						options.entity[Document.NoteOption.COLOR] = new Text(Document.NoteOption.EACH);
					}
				}
				else if (key == Document.NoteOption.COLOR) {
					string text = null;
					if (value.IsKey) {
						text = (value as IKey).key;
					}
					if (text != null) {
						switch (text) {
						case Document.NoteOption.SINGLE:
						case "1":
							options.entity[key] = new Text(Document.NoteOption.SINGLE);
							break;
						case Document.NoteOption.EACH:
						case "2":
							options.entity[key] = new Text(Document.NoteOption.EACH);
							break;
						}
					}
				}

			}
			return this;
		}
		public Object Option(CallerInfo caller, Array content) {
			foreach (var elm in content.entity) {
				string key = null;
				if (elm.IsKey)
					key = (elm as IKey).key;
				else continue;
				switch (key) {
				case Document.NoteOption.SECRET:
					options.entity[Document.NoteOption.SECRET] = new Flag(true);
					break;
				case Document.NoteOption.OVERT:
					options.entity[Document.NoteOption.SECRET] = new Flag(false);
					break;
				}
			}
			return this;
		}
		public Object Turn(CallerInfo caller, Number amount) {
			button.Turn(caller, amount);
			return this;
		}
		public Object MirrorHolizontal(CallerInfo caller) {
			button.MirrorHolizontal(caller);
			return this;
		}
		public Object MirrorVertical(CallerInfo caller) {
			button.MirrorVertical(caller);
			return this;
		}
	}
	
	public class NoteHold : Object, INote, IButtonTurnable, IMirrorable {
		public override Type type { get { return Type.NOTE_HOLD; } }
		public override Object Clone() {
			var ret = new NoteHold(button.entity, (IStep)step.Clone());
			ret.options = options.Clone().Cast<Table>();
			return ret;
		}
		public override string Log() {
			var builder = new System.Text.StringBuilder();
			builder.Append ("MsqScript.NoteHold(");
			builder.Append (button.Log());
			builder.Append (", ");
			builder.Append (step.Log());
			builder.Append (")");
			if (options.entity.Count > 0) {
				builder.Append (".Option(");
				builder.Append (options.Log());
				builder.Append (")");
			}
			return builder.ToString();
		}
		public override bool IsNote { get { return true; } }
		public NoteHold(int buttonId, IStep step) { button = new Button(buttonId); this.step = step; options = new Table(); }
		public Button button;
		public IStep step;
		public Table options;
		public Table GetOption() { return options; }
		public Object Option(CallerInfo caller, Table content) {
			foreach (var key in content.entity.Keys) {
				var value = content.entity[key];
				if (key == Document.NoteOption.SECRET) {
					if (value.IsKey) {
						value = new Text((value as IKey).key).Parse(null);
					}
					if (value.type == Type.FLAG) {
						options.entity[key] = value;
					}
				}
				else if ((key == Document.NoteOption.TIME ||
				          key == Document.NoteOption.FADE ||
				          key == Document.NoteOption.FADE_RATE ||
				          key == Document.NoteOption.MOVE ||
				          key == Document.NoteOption.MOVE_RATE ||
				          key == Document.NoteOption.SIZE ||
				          key == Document.NoteOption.SIZE_RATE)) {
					if (value.IsKey) {
						value = new Text((value as IKey).key).Parse(null);
					}
					if (!value.IsNaN) {
						options.entity[key] = value;
					}
				}
				else if (key == Document.NoteOption.COLOR) {
					string text = null;
					if (value.IsKey) {
						text = (value as IKey).key;
					}
					if (text != null) {
						switch (text) {
						case Document.NoteOption.SINGLE:
						case "1":
							options.entity[key] = new Text(Document.NoteOption.SINGLE);
							break;
						case Document.NoteOption.EACH:
						case "2":
							options.entity[key] = new Text(Document.NoteOption.EACH);
							break;
						}
					}
				}
				
			}
			return this;
		}
		public Object Option(CallerInfo caller, Array content) {
			foreach (var elm in content.entity) {
				string key = null;
				if (elm.IsKey)
					key = (elm as IKey).key;
				else continue;
				switch (key) {
				case Document.NoteOption.SECRET:
					options.entity[Document.NoteOption.SECRET] = new Flag(true);
					break;
				case Document.NoteOption.OVERT:
					options.entity[Document.NoteOption.SECRET] = new Flag(false);
					break;
				}
			}
			return this;
		}
		public Object Turn(CallerInfo caller, Number amount) {
			button.Turn(caller, amount);
			return this;
		}
		public Object MirrorHolizontal(CallerInfo caller) {
			button.MirrorHolizontal(caller);
			return this;
		}
		public Object MirrorVertical(CallerInfo caller) {
			button.MirrorVertical(caller);
			return this;
		}
	}
	
	public class NoteSlide : Object, INote, IButtonTurnable, IMirrorable {
		public override Type type { get { return Type.NOTE_SLIDE; } }
		public override Object Clone() {
			ISlideHead h = null;
			ISlideWait w = null;
			IStep s = null;
			var os = new ISlidePattern[orders.Length];
			
			if (head != null)
				h = (ISlideHead)head.Clone();
			if (wait != null)
				w = (ISlideWait)wait.Clone();
			if (step != null)
				s = (IStep)step.Clone();
			if (orders != null) {
				for (int i = 0; i < os.Length; i++) {
					os[i] = (ISlidePattern)orders[i].Clone();
				}
			}
			var ret = new NoteSlide(h, w, s, os);
			ret.options = options.Clone().Cast<Table>();
			return ret;
		}
		public override string Log() {
			var builder = new System.Text.StringBuilder();
			builder.Append("MsqScript.NoteSlide(head = ");
			builder.Append(head != null ? head.Log() : "null");
			builder.Append(", wait = ");
			builder.Append(wait != null ? wait.Log() : "null");
			builder.Append(", step = ");
			builder.Append(step != null ? step.Log() : "null");
			builder.Append(", orders = ");
			if (orders == null) {
				builder.Append("null");
			}
			else {
				builder.Append("[");
				for (int i = 0; i < orders.Length; i++) {
					if (i > 0) {
						builder.Append(", ");
					}
					else {
						builder.Append(" ");
					}
					builder.Append(orders[i].Log());
				}
				builder.Append(" ]");
			}
			builder.Append(")");
			if (options.entity.Count > 0) {
				builder.Append (".Option(");
				builder.Append (options.Log());
				builder.Append (")");
			}
			return builder.ToString();
		}
		public override bool IsNote { get { return true; } }
		public NoteSlide(ISlideHead head, ISlideWait wait, IStep step, params ISlidePattern[] orders) {
			this.head = head; this.wait = wait; this.step = step; this.orders = orders; 
			options = new Table();
		}
		public ISlideHead head;
		public ISlideWait wait;
		public IStep step;
		public ISlidePattern[] orders;
		public Table options;
		public Table GetOption() { return options; }
		public Object Option(CallerInfo caller, Table content) {
			foreach (var key in content.entity.Keys) {
				var value = content.entity[key];
				if (key == Document.NoteOption.SECRET) {
					if (value.IsKey) {
						value = new Text((value as IKey).key).Parse(null);
					}
					if (value.type == Type.FLAG) {
						options.entity[key] = value;
					}
				}
			}
			return this;
		}
		public Object Option(CallerInfo caller, Array content) {
			foreach (var elm in content.entity) {
				string key = null;
				if (elm.IsKey)
					key = (elm as IKey).key;
				else continue;
				switch (key) {
				case Document.NoteOption.SECRET:
					options.entity[Document.NoteOption.SECRET] = new Flag(true);
					break;
				case Document.NoteOption.OVERT:
					options.entity[Document.NoteOption.SECRET] = new Flag(false);
					break;
				}
			}
			return this;
		}
		public Object Turn(CallerInfo caller, Number amount) {
			if (head != null) {
				head.Turn(caller, amount);
			}
			if (orders != null) {
				foreach (var order in orders) {
					order.Turn(caller, amount);
				}
			}
			return this;
		}
		public Object MirrorHolizontal(CallerInfo caller) {
			if (head != null) {
				head.MirrorHolizontal(caller);
			}
			if (orders != null) {
				foreach (var order in orders) {
					order.MirrorHolizontal(caller);
				}
			}
			return this;
		}
		public Object MirrorVertical(CallerInfo caller) {
			if (head != null) {
				head.MirrorVertical(caller);
			}
			if (orders != null) {
				foreach (var order in orders) {
					order.MirrorHolizontal(caller);
				}
			}
			return this;
		}
		
		private NoteSlide _trimmed;
		public NoteSlide trimmed { get { return _trimmed; } }
		/// <summary>
		/// IsRisky()を実行し、それがtrueのとき、最初のスライドコマンドがContinued系なのにheadが無い、という状況であるためのリスクならtrueになる
		/// </summary>
		public bool isMissingHeadOfFirstContinuedSlideCommand { get; private set; }
		/// <summary>
		/// スライドコマンドが実行時にエラーになる可能性があるならtrueを返す
		/// </summary>
		public bool IsRisky() {
			if (_trimmed == null && !isMissingHeadOfFirstContinuedSlideCommand) {
				var ret = Clone().Cast<NoteSlide>();
				// データが無ければデフォルトで上書き
				if (ret.head == null)
					ret.head = new SlideHeadNothing();
				if (ret.wait == null)
					ret.wait = new SlideWaitDefault();
				if (ret.orders == null || ret.orders.Length == 0)
					return true; // patternがひとつもない
				var patterns = new List<ISlidePattern>(ret.orders);
				for (int i = 0; i < patterns.Count; i++) {
					var order = patterns[i];
					// ret.ordersの中身はpattern/chain/commandのどれか
					SlidePattern pattern;
					if (order.type == Type.SLIDE_PATTERN) {
						// パターンだったらそのまま使う
						pattern = order.Cast<SlidePattern>();
					}
					else {
						// パターンを作って、チェインかコマンドを子供にして、パターンをスライドの子供とする。
						pattern = new SlidePattern(null, null, null);
						pattern.orders = new ISlidePattern[] { (ISlideChain)order };
						patterns[i] = (ISlidePattern)pattern;
					}
					// データが無ければ親で上書き
					if (pattern.wait == null)
						pattern.wait = ret.wait;
					if (pattern.step == null)
						pattern.step = ret.step;
					if (pattern.orders == null || pattern.orders.Length == 0)
						return true; // chainがひとつもない
					var chains = new List<ISlidePattern>(pattern.orders);
					// pattern.ordersの中身はchain/commandのどれか
					for (int j = 0; j < chains.Count; j++) {
						var pOrder = chains[j];
						// Array型なら展開する
						while (pOrder.type == Type.SLIDE_PATTERN) {
							var chainArray = pOrder.Cast<SlidePattern>();
							pattern.optionHelper.FillFromChild(chainArray.GetOption());
							chains.RemoveAt(j);
							chains.InsertRange(j, pOrder.Cast<SlidePattern>().orders);
							if (j < chains.Count) {
								pOrder = chains[j];
							}
							else {
								return true; //展開したが、やっぱりchainがひとつもない.
							}
						}
						SlideChain chain;
						if (pOrder.type == Type.SLIDE_CHAIN) {
							// チェインだったらそのまま使う
							chain = pOrder.Cast<SlideChain>();
						}
						else {
							// チェインを作って、コマンドを子供にして、チェインをパターンの子供とする。
							chain = new SlideChain(null, null, null);
							chain.orders = new ISlideCommand[] { (ISlideCommand)pOrder };
							chains[j] = (ISlidePattern)chain;
						}
						// 親にデータが無ければ子で上書き
						if (pattern.wait == null)
							pattern.wait = chain.wait;
						if (pattern.step == null)
							pattern.step = chain.step;
						if (chain.orders == null || chain.orders.Length == 0)
							return true; // commandがひとつもない
						var commands = new List<ISlideChain>(chain.orders);
						ISlideTrimmedCommand beforeCommand = null;
						for (int k = 0; k < commands.Count; k++) {
							var cOrder = commands[k];
							// Array型なら展開する
							while (cOrder.type == Type.SLIDE_CHAIN) {
								var commandArray = cOrder.Cast<SlideChain>();
								chain.optionHelper.FillFromChild(commandArray.GetOption());
								commands.RemoveAt(k);
								commands.InsertRange(k, commandArray.orders);
								if (k < commands.Count) {
									cOrder = commands[k];
								}
								else {
									return true; //展開したが、やっぱりcommandがひとつもない.
								}
							}
							// 親にデータが無ければ子で上書き
							if (pattern.wait == null)
								pattern.wait = cOrder.wait;
							if (pattern.step == null)
								pattern.step = cOrder.step;
							chain.optionHelper.FillFromChild(cOrder.GetOption());
							ISlideCommand command = (ISlideCommand)cOrder;
							// continue系であるならばheadをつぎ込んでいく
							if (command.IsSlideContinuedCommand) {
								var continuedCommand = (ISlideContinuedCommand)command;
								// continue系で続きが作れない(一番最初のスライドコマンドがContinue系なのにstarが無い)場合はリスキーとする.
								if (ret.head.type == Type.SLIDE_HEAD_NOTHING && beforeCommand == null) {
									isMissingHeadOfFirstContinuedSlideCommand = true;
									return true;
								}
								else {
									commands[k] = continuedCommand.GetEntity(ret.head, beforeCommand);
								}
							}
							// SlideCommandArray型でもContinue系でもない(もう展開した)のでISlideTrimmedCommandであることが確定した.
							beforeCommand = (ISlideTrimmedCommand)commands[k];
						}
						chain.orders = commands.ToArray();
						pattern.optionHelper.FillFromChild(chain.GetOption());
					}
					if (pattern.step == null) {
						return true;
					}
					pattern.orders = chains.ToArray();
				}
				ret.orders = patterns.ToArray();
				_trimmed = ret;
			}
			return _trimmed == null;
		}
	}
	
	public class NoteBreak : NoteTap {
		public override Type type { get { return Type.NOTE_BREAK; } }
		public override Object Clone() {
			var ret = new NoteBreak(button.entity);
			ret.options = options.Clone().Cast<Table>();
			return ret;
		}
		public override string Log() {
			var builder = new System.Text.StringBuilder();
			builder.Append ("MsqScript.NoteBreak(");
			builder.Append (button.Log());
			builder.Append (")");
			if (options.entity.Count > 0) {
				builder.Append (".Option(");
				builder.Append (options.Log());
				builder.Append (")");
			}
			return builder.ToString();
		}
		public NoteBreak(int buttonId) : base (buttonId) { }
	}
	
	public class NoteMessage : Object, INote {
		public override Type type { get { return Type.NOTE_MESSAGE; } }
		public override Object Clone() {
			var ret = new NoteMessage(message.Clone().Cast<Text>(), position.Clone().Cast<Position>(), (IMsqValue)scale.Clone(), color.Clone().Cast<Color>(), (IStep)step.Clone());
			ret.options = options.Clone().Cast<Table>();
			return ret;
		}
		public override string Log() {
			var builder = new System.Text.StringBuilder();
			builder.Append ("MsqScript.NoteMessage(");
			builder.Append (message.Log());
			builder.Append (")");
			if (options.entity.Count > 0) {
				builder.Append (".Option(");
				builder.Append (options.Log());
				builder.Append (")");
			}
			return builder.ToString();
		}
		public override bool IsNote { get { return true; } }
		public Text message;
		public Position position;
		public IMsqValue scale;
		public Color color;
		public IStep step;
		public Table options;
		public Table GetOption() { return options; }
		public Object Option(CallerInfo caller, Table content) {
			foreach (var key in content.entity.Keys) {
				var value = content.entity[key];
				if (key == Document.NoteOption.SECRET) {
					if (value.IsKey) {
						value = new Text((value as IKey).key).Parse(null);
					}
					if (value.type == Type.FLAG) {
						options.entity[key] = value;
					}
				}
				else if (key == Document.NoteOption.TIME) {
					if (value.IsKey) {
						value = new Text((value as IKey).key).Parse(null);
					}
					if (!value.IsNaN) {
						options.entity[key] = value;
					}
				}
				else if (key == Document.NoteOption.ALIGNMENT) {
					string text = null;
					if (value.IsKey) {
						text = (value as IKey).key;
					}
					if (text == Document.NoteOption.LEFT ||
					    text == Document.NoteOption.CENTER ||
					    text == Document.NoteOption.RIGHT) {
						options.entity[key] = new Text(text);
					}
				}
			}
			return this;
		}
		public Object Option(CallerInfo caller, Array content) {
			foreach (var elm in content.entity) {
				string key = null;
				if (elm.IsKey)
					key = (elm as IKey).key;
				else continue;
				switch (key) {
				case Document.NoteOption.SECRET:
					options.entity[Document.NoteOption.SECRET] = new Flag(true);
					break;
				case Document.NoteOption.OVERT:
					options.entity[Document.NoteOption.SECRET] = new Flag(false);
					break;
				case Document.NoteOption.LEFT:
				case Document.NoteOption.CENTER:
				case Document.NoteOption.RIGHT:
					options.entity[Document.NoteOption.ALIGNMENT] = new Text(key);
					break;
				}
			}
			return this;
		}
		public NoteMessage(Text message, Position position, IMsqValue scale, Color color, IStep step) {
			this.message = message;
			this.position = position;
			this.scale = scale;
			this.color = color;
			this.step = step;
			options = new Table();
		}
	}
	
	public class NoteScrollMessage : Object, INote {
		public override Type type { get { return Type.NOTE_SCROLL_MESSAGE; } }
		public override Object Clone() {
			var ret = new NoteScrollMessage(message.Clone().Cast<Text>(), (IMsqValue)y.Clone(), (IMsqValue)scale.Clone(), color.Clone().Cast<Color>(), (IStep)step.Clone());
			ret.options = options.Clone().Cast<Table>();
			return ret;
		}
		public override string Log() {
			var builder = new System.Text.StringBuilder();
			builder.Append ("MsqScript.NoteScrollMessage(");
			builder.Append (message.Log());
			builder.Append (")");
			if (options.entity.Count > 0) {
				builder.Append (".Option(");
				builder.Append (options.Log());
				builder.Append (")");
			}
			return builder.ToString();
		}
		public override bool IsNote { get { return true; } }
		public Text message;
		public IMsqValue y;
		public IMsqValue scale;
		public Color color;
		public IStep step;
		public Table options;
		public Table GetOption() { return options; }
		public Object Option(CallerInfo caller, Table content) {
			foreach (var key in content.entity.Keys) {
				var value = content.entity[key];
				if (key == Document.NoteOption.SECRET) {
					if (value.IsKey) {
						value = new Text((value as IKey).key).Parse(null);
					}
					if (value.type == Type.FLAG) {
						options.entity[key] = value;
					}
				}
				else if (key == Document.NoteOption.TIME) {
					if (value.IsKey) {
						value = new Text((value as IKey).key).Parse(null);
					}
					if (!value.IsNaN) {
						options.entity[key] = value;
					}
				}
				else if (key == Document.NoteOption.ALIGNMENT) {
					string text = null;
					if (value.IsKey) {
						text = (value as IKey).key;
					}
					if (text == Document.NoteOption.LEFT ||
					    text == Document.NoteOption.CENTER ||
					    text == Document.NoteOption.RIGHT) {
						options.entity[key] = new Text(text);
					}
				}
			}
			return this;
		}
		public Object Option(CallerInfo caller, Array content) {
			foreach (var elm in content.entity) {
				string key = null;
				if (elm.IsKey)
					key = (elm as IKey).key;
				else continue;
				switch (key) {
				case Document.NoteOption.SECRET:
					options.entity[Document.NoteOption.SECRET] = new Flag(true);
					break;
				case Document.NoteOption.OVERT:
					options.entity[Document.NoteOption.SECRET] = new Flag(false);
					break;
				case Document.NoteOption.LEFT:
				case Document.NoteOption.CENTER:
				case Document.NoteOption.RIGHT:
					options.entity[Document.NoteOption.ALIGNMENT] = new Text(key);
					break;
				}
			}
			return this;
		}
		public NoteScrollMessage(Text message, IMsqValue y, IMsqValue scale, Color color, IStep step) {
			this.message = message;
			this.y = y;
			this.scale = scale;
			this.color = color;
			this.step = step;
			options = new Table();
		}
	}
	
	public class NoteSoundMessage : Object, INote {
		public override Type type { get { return Type.NOTE_SOUND_MESSAGE; } }
		public override Object Clone() {
			var ret = new NoteSoundMessage(soundId);
			ret.options = options.Clone().Cast<Table>();
			return ret;
		}
		public override string Log() {
			var builder = new System.Text.StringBuilder();
			builder.Append ("MsqScript.NoteSoundMessage(");
			builder.Append (soundId.Log());
			builder.Append (")");
			if (options.entity.Count > 0) {
				builder.Append (".Option(");
				builder.Append (options.Log());
				builder.Append (")");
			}
			return builder.ToString();
		}
		public override bool IsNote { get { return true; } }
		public Text soundId;
		public Table options;
		public Table GetOption() { return options; }
		public Object Option(CallerInfo caller, Table content) {
			foreach (var key in content.entity.Keys) {
				var value = content.entity[key];
				if (key == Document.NoteOption.SECRET) {
					if (value.IsKey) {
						value = new Text((value as IKey).key).Parse(null);
					}
					if (value.type == Type.FLAG) {
						options.entity[key] = value;
					}
				}
				else if (key == Document.NoteOption.TIME) {
					if (value.IsKey) {
						value = new Text((value as IKey).key).Parse(null);
					}
					if (!value.IsNaN) {
						options.entity[key] = value;
					}
				}
			}
			return this;
		}
		public Object Option(CallerInfo caller, Array content) {
			foreach (var elm in content.entity) {
				string key = null;
				if (elm.IsKey)
					key = (elm as IKey).key;
				else continue;
				switch (key) {
				case Document.NoteOption.SECRET:
					options.entity[Document.NoteOption.SECRET] = new Flag(true);
					break;
				case Document.NoteOption.OVERT:
					options.entity[Document.NoteOption.SECRET] = new Flag(false);
					break;
				}
			}
			return this;
		}
		public NoteSoundMessage(Text soundId) {
			this.soundId = soundId;
			options = new Table();
		}
	}
	
	public class NoteTrap : Object, INote, IButtonTurnable, IMirrorable {
		public override Type type { get { return Type.NOTE_TRAP; } }
		public override Object Clone() {
			var ret = new NoteTrap(sensor.Clone().Cast<Sensor>(), (IStep)step.Clone());
			ret.options = options.Clone().Cast<Table>();
			return ret;
		}
		public override string Log() {
			var builder = new System.Text.StringBuilder();
			builder.Append ("MsqScript.NoteTrap(");
			builder.Append (sensor.Log());
			builder.Append (", ");
			builder.Append (step.Log());
			builder.Append (")");
			if (options.entity.Count > 0) {
				builder.Append (".Option(");
				builder.Append (options.Log());
				builder.Append (")");
			}
			return builder.ToString();
		}
		public override bool IsNote { get { return true; } }
		public NoteTrap(Sensor sensor, IStep step) { this.sensor = sensor; this.step = step; options = new Table(); }
		public Sensor sensor;
		public IStep step;
		public Table options;
		public Table GetOption() { return options; }
		public Object Option(CallerInfo caller, Table content) {
			foreach (var key in content.entity.Keys) {
				var value = content.entity[key];
				if (key == Document.NoteOption.SECRET) {
					if (value.IsKey) {
						value = new Text((value as IKey).key).Parse(null);
					}
					if (value.type == Type.FLAG) {
						options.entity[key] = value;
					}
				}
				else if ((key == Document.NoteOption.TIME ||
				          key == Document.NoteOption.FADE ||
				          key == Document.NoteOption.FADE_RATE ||
				          key == Document.NoteOption.SIZE ||
				          key == Document.NoteOption.SIZE_RATE)) {
					if (value.IsKey) {
						value = new Text((value as IKey).key).Parse(null);
					}
					if (!value.IsNaN) {
						options.entity[key] = value;
					}
				}
				
			}
			return this;
		}
		public Object Option(CallerInfo caller, Array content) {
			foreach (var elm in content.entity) {
				string key = null;
				if (elm.IsKey)
					key = (elm as IKey).key;
				else continue;
				switch (key) {
				case Document.NoteOption.SECRET:
					options.entity[Document.NoteOption.SECRET] = new Flag(true);
					break;
				case Document.NoteOption.OVERT:
					options.entity[Document.NoteOption.SECRET] = new Flag(false);
					break;
				}
			}
			return this;
		}
		public Object Turn(CallerInfo caller, Number amount) {
			sensor.Turn(caller, amount);
			return this;
		}
		public Object MirrorHolizontal(CallerInfo caller) {
			sensor.MirrorHolizontal(caller);
			return this;
		}
		public Object MirrorVertical(CallerInfo caller) {
			sensor.MirrorVertical(caller);
			return this;
		}
	}
	
	public class StepBasic : Object, IStep {
		public override Type type { get { return Type.STEP_BASIC; } }
		public override Object Clone() { return new StepBasic((IMsqValue)beat.Clone(), (IMsqValue)length.Clone()); }
		public override string Log() {
			return "MsqScript.StepBasic(beat = " + beat.Log() + ", length = " + length.Log() + ")";
		}
		public override bool IsStep { get { return true; } }
		public override bool IsSlideWait { get { return true; } }
		public virtual bool IsMass { get { return false; } }
		public StepBasic(IMsqValue beat, IMsqValue length) { this.beat = beat; this.length = length; }
		public IMsqValue beat;
		public IMsqValue length;
	}
	
	public class StepInterval : Object, IStep {
		public override Type type { get { return Type.STEP_INTERVAL; } }
		public override Object Clone() { return new StepInterval((IMsqValue)interval.Clone()); }
		public override string Log() {
			return "MsqScript.StepInterval(" + interval.Log() + ")";
		}
		public override bool IsStep { get { return true; } }
		public override bool IsSlideWait { get { return true; } }
		public virtual bool IsMass { get { return false; } }
		public StepInterval(IMsqValue data) { interval = data; }
		public IMsqValue interval;
	}
	
	public class StepLocalBpm : StepBasic, IStep {
		public override Type type { get { return Type.STEP_LOCALBPM; } }
		public override Object Clone() { return new StepLocalBpm((IMsqValue)bpm.Clone(), (IMsqValue)beat.Clone(), (IMsqValue)length.Clone()); }
		public override string Log() {
			return "MsqScript.StepLocalBpm(bpm = " + bpm.Log() + ", beat = " + beat.Log() + ", length = " + length.Log() + ")";
		}
		public StepLocalBpm(IMsqValue bpm, IMsqValue beat, IMsqValue length) : base (beat, length) { this.bpm = bpm; }
		public IMsqValue bpm;
	}
	
	public class StepMass : Object, IStep {
		public override Type type { get { return Type.STEP_MASS; } }
		public override Object Clone() { 
			var steps = new IStep[entities.Length];
			for (int i = 0; i < steps.Length; i++) {
				steps[i] = entities[i].Clone() as IStep;
			}
			return new StepMass(steps);
		}
		public override string Log() {
			var builder = new System.Text.StringBuilder();
			builder.Append("MsqScript.StepMass(");
			for (int i = 0; i < entities.Length; i++) {
				builder.Append(entities[i].Log());
				if (i < entities.Length - 1) {
					builder.Append(", ");
				}
			}
			builder.Append(")");
			return builder.ToString();
		}
		public override bool IsStep { get { return true; } }
		public override bool IsSlideWait { get { return true; } }
		public virtual bool IsMass { get { return true; } }
		public StepMass(IStep[] steps) { entities = steps; }
		public IStep[] entities;
	}
	
	public class SlideHeadStar : NoteTap, ISlideHead {
		public override Type type { get { return Type.SLIDE_HEAD_STAR; } }
		public override Object Clone() { 
			var ret = new SlideHeadStar(button.entity);
			ret.options = options.Clone().Cast<Table>();
			return ret;
		}
		public override string Log() {
			var builder = new System.Text.StringBuilder();
			builder.Append ("MsqScript.SlideHeadStar(");
			builder.Append (button.Log());
			builder.Append (")");
			if (options.entity.Count > 0) {
				builder.Append (".Option(");
				builder.Append (options.Log());
				builder.Append (")");
			}
			return builder.ToString();
		}
		public override bool IsSlideHead { get { return true; } }
		public SlideHeadStar(int data) : base(data) { }
		public SlideHeadStar(Number data) : base(data.entity) { }
		public int? star { get { return button.entity; } }
	}
	
	public class SlideHeadBreak : NoteBreak, ISlideHead {
		public override Type type { get { return Type.SLIDE_HEAD_BREAK; } }
		public override Object Clone() {
			var ret = new SlideHeadBreak(button.entity);
			ret.options = options.Clone().Cast<Table>();
			return ret;
		}
		public override string Log() {
			var builder = new System.Text.StringBuilder();
			builder.Append ("MsqScript.SlideHeadBreak(");
			builder.Append (button.Log());
			builder.Append (")");
			if (options.entity.Count > 0) {
				builder.Append (".Option(");
				builder.Append (options.Log());
				builder.Append (")");
			}
			return builder.ToString();
		}
		public override bool IsSlideHead { get { return true; } }
		public SlideHeadBreak(int data) : base(data) { }
		public SlideHeadBreak(Number data) : base(data.entity) { }
		public int? star { get { return button.entity; } }
	}
	
	public class SlideHeadNothing : Object, ISlideHead {
		public override Type type { get { return Type.SLIDE_HEAD_NOTHING; } }
		public override Object Clone() { return new SlideHeadNothing(); }
		public override string Log() {
			return "MsqScript.SlideHeadNothing()";
		}
		public override bool IsSlideHead { get { return true; } }
		public SlideHeadNothing() { }
		public int? star { get { return null; } }
		public Table GetOption() {
			return null;
		}
		public Object Option(CallerInfo caller, Table content) {
			return this;
		}
		public Object Option(CallerInfo caller, Array content) {
			return this;
		}
		public Object Turn(CallerInfo caller, Number amount) {
			return this;
		}
		public Object MirrorHolizontal(CallerInfo caller) {
			return this;
		}
		public Object MirrorVertical(CallerInfo caller) {
			return this;
		}
	}
	
	public class SlideWaitDefault : Object, ISlideWait {
		public override Type type { get { return Type.SLIDE_WAIT_DEFAULT; } }
		public override Object Clone() { return new SlideWaitDefault(); }
		public override string Log() {
			return "MsqScript.SlideWaitDefault()";
		}
		public override bool IsSlideWait { get { return true; } }
		public virtual bool IsMass { get { return false; } }
		public SlideWaitDefault() { }
	}

	public class SlideWaitLocalBpm : SlideWaitDefault {
		public override Type type { get { return Type.SLIDE_WAIT_LOCALBPM; } }
		public override Object Clone() { return new SlideWaitLocalBpm((IMsqValue)bpm.Clone()); }
		public override string Log() {
			return "MsqScript.SlideWaitLocalBpm(" + bpm.Log() + ")";
		}
		public SlideWaitLocalBpm(IMsqValue bpm) { this.bpm = bpm; }
		public IMsqValue bpm;
	}
	
	public class SlideWaitInterval : Object, ISlideWait {
		public override Type type { get { return Type.SLIDE_WAIT_INTERVAL; } }
		public override Object Clone() { return new SlideWaitInterval((IMsqValue)time.Clone()); }
		public override string Log() {
			return "MsqScript.SlideWaitInterval(" + time.Log() + ")";
		}
		public override bool IsSlideWait { get { return true; } }
		public virtual bool IsMass { get { return false; } }
		public SlideWaitInterval(IMsqValue time) { this.time = time; }
		public IMsqValue time;
	}

	public class SlideWaitStepBasic : Object, ISlideWait {
		public override Type type { get { return Type.SLIDE_WAIT_STEP_BASIC; } }
		public override Object Clone() { return new SlideWaitStepBasic((IMsqValue)beat.Clone(), (IMsqValue)length.Clone()); }
		public override string Log() {
			return "MsqScript.SlideWaitStepBasic(beat = " + beat.Log() + ", length = " + length.Log() + ")";
		}
		public override bool IsSlideWait { get { return true; } }
		public virtual bool IsMass { get { return false; } }
		public SlideWaitStepBasic(IMsqValue beat, IMsqValue length) { this.beat = beat; this.length = length; }
		public IMsqValue beat;
		public IMsqValue length;
	}
	
	public class SlideWaitStepLocalBpm : SlideWaitStepBasic {
		public override Type type { get { return Type.SLIDE_WAIT_STEP_LOCALBPM; } }
		public override Object Clone() { return new SlideWaitStepLocalBpm((IMsqValue)bpm.Clone(), (IMsqValue)beat.Clone(), (IMsqValue)length.Clone()); }
		public override string Log() {
			return "MsqScript.SlideWaitStepLocalBpm(bpm = " + bpm.Log() + ", beat = " + beat.Log() + ", length = " + length.Log() + ")";
		}
		public override bool IsSlideWait { get { return true; } }
		public SlideWaitStepLocalBpm(IMsqValue bpm, IMsqValue beat, IMsqValue length) : base (beat, length) { this.bpm = bpm; }
		public IMsqValue bpm;
	}
	
	public class SlideWaitMass : Object, ISlideWait {
		public override Type type { get { return Type.SLIDE_WAIT_MASS; } }
		public override Object Clone() { 
			var waits = new ISlideWait[entities.Length];
			for (int i = 0; i < waits.Length; i++) {
				waits[i] = entities[i].Clone() as IStep;
			}
			return new SlideWaitMass(waits);
		}
		public override string Log() {
			var builder = new System.Text.StringBuilder();
			builder.Append("MsqScript.SlideWaitMass(");
			for (int i = 0; i < entities.Length; i++) {
				builder.Append(entities[i].Log());
				if (i < entities.Length - 1) {
					builder.Append(", ");
				}
			}
			builder.Append(")");
			return builder.ToString();
		}
		public override bool IsSlideWait { get { return true; } }
		public virtual bool IsMass { get { return true; } }
		public SlideWaitMass(ISlideWait[] waits) { entities = waits; }
		public ISlideWait[] entities;
	}
	
	public class SlidePattern : Object, ISlidePattern {
		public override Type type { get { return Type.SLIDE_PATTERN; } }
		public override Object Clone() {
			ISlideWait w = null;
			IStep s = null;
			ISlidePattern[] os = new ISlidePattern[orders.Length];
			if (wait != null)
				w = (ISlideWait)wait.Clone();
			if (step != null)
				s = (IStep)step.Clone();
			if (orders != null) {
				for (int i = 0; i < os.Length; i++) {
					os[i] = (ISlidePattern)orders[i].Clone();
				}
			}
			var ret = new SlidePattern(w, s, os);
			ret.optionHelper = optionHelper.Clone();
			return ret;
		}
		public override string Log() {
			var builder = new System.Text.StringBuilder();
			builder.Append("MsqScript.SlidePattern(wait = ");
			builder.Append(wait != null ? wait.Log() : "null");
			builder.Append(", step = ");
			builder.Append(step != null ? step.Log() : "null");
			builder.Append(", orders = ");
			if (orders == null) {
				builder.Append("null");
			}
			else {
				builder.Append("[");
				for (int i = 0; i < orders.Length; i++) {
					if (i > 0) {
						builder.Append(", ");
					}
					else {
						builder.Append(" ");
					}
					builder.Append(orders[i].Log());
				}
				builder.Append(" ]");
			}
			builder.Append(")");
			builder.Append(optionHelper.Log());
			return builder.ToString();
		}
		public override bool IsSlidePattern { get { return true; } }
		public SlidePattern(ISlideWait wait, IStep step, ISlidePattern[] orders) {
			this.wait = wait;
			this.step = step;
			this.orders = orders;
			optionHelper = new SlidePatternOptionHelper();
		}
		public ISlideWait wait { get; set; }
		public IStep step { get; set; }
		public ISlidePattern[] orders { get; set; }
		public SlidePatternOptionHelper optionHelper;
		public Table GetOption() { return optionHelper.options; }
		public Object Option(CallerInfo caller, Table content) {
			optionHelper.Option(caller, content);
			return this;
		}
		public Object Option(CallerInfo caller, Array content) {
			optionHelper.Option(caller, content);
			return this;
		}
		public Object Turn(CallerInfo caller, Number amount) {
			if (orders != null) {
				foreach (var order in orders) {
					order.Turn(caller, amount);
				}
			}
			return this;
		}
		public Object Turn(CallerInfo caller, IMsqValue degree) {
			if (orders != null) {
				foreach (var order in orders) {
					order.Turn(caller, degree);
				}
			}
			return this;
		}
		public Object MirrorHolizontal(CallerInfo caller) {
			if (orders != null) {
				foreach (var order in orders) {
					order.MirrorHolizontal(caller);
				}
			}
			return this;
		}
		public Object MirrorVertical(CallerInfo caller) {
			if (orders != null) {
				foreach (var order in orders) {
					order.MirrorVertical(caller);
				}
			}
			return this;
		}
	}
	
	public class SlideChain : Object, ISlideChain {
		public override Type type { get { return Type.SLIDE_CHAIN; } }
		public override Object Clone() {
			ISlideWait w = null;
			IStep s = null;
			ISlideChain[] os = new ISlideChain[orders.Length];
			if (wait != null)
				w = (ISlideWait)wait.Clone();
			if (step != null)
				s = (IStep)step.Clone();
			if (orders != null) {
				for (int i = 0; i < os.Length; i++) {
					os[i] = (ISlideChain)orders[i].Clone();
				}
			}
			var ret = new SlideChain(w, s, os);
			ret.optionHelper = optionHelper.Clone();
			return ret;
		}
		public override string Log() {
			var builder = new System.Text.StringBuilder();
			builder.Append("MsqScript.SlideChain(wait = ");
			builder.Append(wait != null ? wait.Log() : "null");
			builder.Append(", step = ");
			builder.Append(step != null ? step.Log() : "null");
			builder.Append(", orders = ");
			if (orders == null) {
				builder.Append("null");
			}
			else {
				builder.Append("[");
				for (int i = 0; i < orders.Length; i++) {
					if (i > 0) {
						builder.Append(", ");
					}
					else {
						builder.Append(" ");
					}
					builder.Append(orders[i].Log());
				}
				builder.Append(" ]");
			}
			builder.Append(")");
			builder.Append(optionHelper.Log());
			return builder.ToString();
		}
		public override bool IsSlidePattern { get { return true; } }
		public override bool IsSlideChain { get { return true; } }
		public SlideChain(ISlideWait wait, IStep step, ISlideChain[] orders) {
			this.wait = wait;
			this.step = step;
			this.orders = orders;
			optionHelper = new SlidePatternOptionHelper();
		}
		public ISlideWait wait { get; set; }
		public IStep step { get; set; }
		public ISlideChain[] orders { get; set; }
		public SlidePatternOptionHelper optionHelper;
		public Table GetOption() { return optionHelper.options; }
		public Object Option(CallerInfo caller, Table content) {
			optionHelper.Option(caller, content);
			foreach (var key in content.entity.Keys) {
				var value = content.entity[key];
				if (key == Document.NoteOption.CENTER ||
				    key == Document.NoteOption.SIDE) {
					if (value.IsKey) {
						value = new Text((value as IKey).key).Parse(null);
					}
					if (value.type == Type.FLAG) {
						optionHelper.options.entity[key] = value;
					}
				}
				else if (key == Document.NoteOption.SECTIONS) {
					if (value.type == Type.ARRAY) {
						bool allSensor = true;
						var array = value.Cast<Array>();
						foreach (var elm in array.entity) {
							if (elm.type != Type.SENSOR) {
								allSensor = false;
								break;
							}
						}
						if (allSensor) {
							optionHelper.options.entity[key] = value;
						}
					}
				}
			}
			return this;
		}
		public Object Option(CallerInfo caller, Array content) {
			optionHelper.Option(caller, content);
			foreach (var elm in content.entity) {
				string key = null;
				if (elm.IsKey)
					key = (elm as IKey).key;
				else continue;
				switch (key) {
				case Document.NoteOption.CENTER:
					optionHelper.options.entity[Document.NoteOption.CENTER] = new Flag(true);
					optionHelper.options.entity[Document.NoteOption.SIDE] = new Flag(false);
					break;
				case Document.NoteOption.SIDE:
					optionHelper.options.entity[Document.NoteOption.SIDE] = new Flag(true);
					optionHelper.options.entity[Document.NoteOption.CENTER] = new Flag(false);
					break;
				}
			}
			return this;
		}
		public Object Turn(CallerInfo caller, Number amount) {
			if (orders != null) {
				foreach (var order in orders) {
					order.Turn(caller, amount);
				}
			}
			return this;
		}
		public Object Turn(CallerInfo caller, IMsqValue degree) {
			if (orders != null) {
				foreach (var order in orders) {
					order.Turn(caller, degree);
				}
			}
			return this;
		}
		public Object MirrorHolizontal(CallerInfo caller) {
			if (orders != null) {
				foreach (var order in orders) {
					order.MirrorHolizontal(caller);
				}
			}
			return this;
		}
		public Object MirrorVertical(CallerInfo caller) {
			if (orders != null) {
				foreach (var order in orders) {
					order.MirrorVertical(caller);
				}
			}
			return this;
		}
	}
	
	public class SlideCommandStraight : Object, ISlideTrimmedCommand {
		public override Type type { get { return Type.SLIDE_COMMAND_STRAIGHT; } }
		public override Object Clone() {
			ISlideWait w = null;
			IStep s = null;
			if (wait != null)
				w = (ISlideWait)wait.Clone();
			if (step != null)
				s = (IStep)step.Clone();
			var ret = new SlideCommandStraight(w, s, start.Clone().Cast<Position>(), target.Clone().Cast<Position>());
			ret.optionHelper = optionHelper.Clone();
			return ret;
		}
		public override string Log() {
			var builder = new System.Text.StringBuilder();
			builder.Append("MsqScript.SlideCommandStraight(wait = ");
			builder.Append(wait != null ? wait.Log() : "null");
			builder.Append(", step = ");
			builder.Append(step != null ? step.Log() : "null");
			builder.Append(", start = ");
			builder.Append(start.Log());
			builder.Append(", target = ");
			builder.Append(target.Log());
			builder.Append(")");
			builder.Append(optionHelper.Log());
			return builder.ToString();
		}
		public override bool IsSlidePattern { get { return true; } }
		public override bool IsSlideChain { get { return true; } }
		public override bool IsSlideCommand { get { return true; } }
		public bool IsSlideContinuedCommand { get { return false; } }
		public bool IsStraight { get { return true; } }
		public bool IsCurve { get { return false; } }
		public SlideCommandStraight(ISlideWait wait, IStep step, Position start, Position target) {
			this.wait = wait;
			this.step = step;
			this.start = start;
			this.target = target;
			optionHelper = new SlidePatternOptionHelper();
		}
		public ISlideWait wait { get; set; }
		public IStep step { get; set; }
		public Position start;
		public Position target;
		public SlidePatternOptionHelper optionHelper;
		public Table GetOption() { return optionHelper.options; }
		public Object Option(CallerInfo caller, Table content) {
			optionHelper.Option(caller, content);
			return this;
		}
		public Object Option(CallerInfo caller, Array content) {
			optionHelper.Option(caller, content);
			return this;
		}
		public Object Turn(CallerInfo caller, Number amount) {
			start.Turn(caller, amount);
			target.Turn(caller, amount);
			return this;
		}
		public Object Turn(CallerInfo caller, IMsqValue degree) {
			start.Turn(caller, degree);
			target.Turn(caller, degree);
			return this;
		}
		public Object MirrorHolizontal(CallerInfo caller) {
			start.MirrorHolizontal(caller);
			target.MirrorHolizontal(caller);
			return this;
		}
		public Object MirrorVertical(CallerInfo caller) {
			start.MirrorVertical(caller);
			target.MirrorVertical(caller);
			return this;
		}
	}
	
	public class SlideCommandCurve : Object, ISlideTrimmedCommand {
		public override Type type { get { return Type.SLIDE_COMMAND_CURVE; } }
		public override Object Clone() { 
			ISlideWait w = null;
			IStep s = null;
			if (wait != null)
				w = (ISlideWait)wait.Clone();
			if (step != null)
				s = (IStep)step.Clone();
			var ret = new SlideCommandCurve(w, s, center.Clone().Cast<Position>(), radius.Clone().Cast<Position>(), (IMsqValue)start_degree.Clone(), (IMsqValue)distance_degree.Clone());
			ret.optionHelper = optionHelper.Clone();
			return ret;
		}
		public override string Log() {
			var builder = new System.Text.StringBuilder();
			builder.Append("MsqScript.SlideCommandCurve(wait = ");
			builder.Append(wait != null ? wait.Log() : "null");
			builder.Append(", step = ");
			builder.Append(step != null ? step.Log() : "null");
			builder.Append(", center = ");
			builder.Append(center.Log());
			builder.Append(", radius = ");
			builder.Append(radius.Log());
			builder.Append(", start_deg = ");
			builder.Append(start_degree.Log());
			builder.Append(", distance_deg = ");
			builder.Append(distance_degree.Log());
			builder.Append(")");
			builder.Append(optionHelper.Log());
			return builder.ToString();
		}
		public override bool IsSlidePattern { get { return true; } }
		public override bool IsSlideChain { get { return true; } }
		public override bool IsSlideCommand { get { return true; } }
		public bool IsSlideContinuedCommand { get { return false; } }
		public bool IsStraight { get { return false; } }
		public bool IsCurve { get { return true; } }
		public SlideCommandCurve(ISlideWait wait, IStep step, Position center, Position radius, IMsqValue start_degree, IMsqValue distance_degree) {
			this.wait = wait;
			this.step = step;
			this.center = center;
			this.radius = radius;
			this.start_degree = start_degree;
			this.distance_degree = distance_degree;
			optionHelper = new SlidePatternOptionHelper();
		}
		public SlideCommandCurve(ISlideWait wait, IStep step, Position center, IMsqValue radius, IMsqValue start_degree, IMsqValue distance_degree)
			: this (wait, step, center, new Position(radius, radius), start_degree, distance_degree) {
		}
		public ISlideWait wait { get; set; }
		public IStep step { get; set; }
		public Position center;
		public Position radius;
		public IMsqValue start_degree;
		public IMsqValue distance_degree;
		public SlidePatternOptionHelper optionHelper;
		public Table GetOption() { return optionHelper.options; }
		public Object Option(CallerInfo caller, Table content) {
			optionHelper.Option(caller, content);
			return this;
		}
		public Object Option(CallerInfo caller, Array content) {
			optionHelper.Option(caller, content);
			return this;
		}
		public Object Turn(CallerInfo caller, Number amount) {
			center.Turn(caller, amount);
			float sd = start_degree.value;
			sd += amount.entity * (360.0f / 8.0f);
			while (sd > 360.0f)
				sd -= 360.0f;
			while (sd < 0)
				sd += 360.0f;
			start_degree.value = sd;
			return this;
		}
		public Object Turn(CallerInfo caller, IMsqValue degree) {
			center.Turn(caller, degree);
			float sd = start_degree.value;
			sd += degree.value;
			while (sd > 360.0f)
				sd -= 360.0f;
			while (sd < 0)
				sd += 360.0f;
			start_degree.value = sd;
			return this;
		}
		public Object MirrorHolizontal(CallerInfo caller) {
			center.MirrorHolizontal(caller);
			float sd = start_degree.value;
			sd = 360.0f - sd;
			while (sd > 360.0f)
				sd -= 360.0f;
			while (sd < 0)
				sd += 360.0f;
			start_degree.value = sd;
			distance_degree.value *= -1;
			return this;
		}
		public Object MirrorVertical(CallerInfo caller) {
			center.MirrorVertical(caller);
			float sd = start_degree.value;
			sd = 180.0f - sd;
			while (sd > 360.0f)
				sd -= 360.0f;
			while (sd < 0)
				sd += 360.0f;
			start_degree.value = sd;
			distance_degree.value *= -1;
			return this;
		}
	}
	
	public class SlideCommandContinuedStraight : Object, ISlideContinuedCommand {
		public override Type type { get { return Type.SLIDE_COMMAND_CONTINUED_STRAIGHT; } }
		public override Object Clone() {
			ISlideWait w = null;
			IStep s = null;
			if (wait != null)
				w = (ISlideWait)wait.Clone();
			if (step != null)
				s = (IStep)step.Clone();
			var ret = new SlideCommandContinuedStraight(w, s, target.Clone().Cast<Position>());
			ret.optionHelper = optionHelper.Clone();
			return ret;
		}
		public override string Log() {
			var builder = new System.Text.StringBuilder();
			builder.Append("MsqScript.SlideCommandContinuedStraight(wait = ");
			builder.Append(wait != null ? wait.Log() : "null");
			builder.Append(", step = ");
			builder.Append(step != null ? step.Log() : "null");
			builder.Append(", target = ");
			builder.Append(target.Log());
			builder.Append(")");
			builder.Append(optionHelper.Log());
			return builder.ToString();
		}
		public override bool IsSlidePattern { get { return true; } }
		public override bool IsSlideChain { get { return true; } }
		public override bool IsSlideCommand { get { return true; } }
		public bool IsSlideContinuedCommand { get { return true; } }
		public bool IsStraight { get { return true; } }
		public bool IsCurve { get { return false; } }
		public SlideCommandContinuedStraight(ISlideWait wait, IStep step, Position target) {
			this.wait = wait;
			this.step = step;
			this.target = target;
			optionHelper = new SlidePatternOptionHelper();
		}
		public ISlideWait wait { get; set; }
		public IStep step { get; set; }
		public Position target;
		public ISlideTrimmedCommand GetEntity (ISlideHead head, ISlideTrimmedCommand beforeCommand) {
			// SlideCommandStraightを取得する
			float sx, sy;
			if (beforeCommand == null) {
				CalcContinuedStraightPosition (head, out sx, out sy);
			}
			else {
				CalcContinuedStraightPosition (beforeCommand, out sx, out sy);
			}
			return new SlideCommandStraight(wait, step, new Position (sx, sy), target);
		}
		public SlidePatternOptionHelper optionHelper;
		public Table GetOption() { return optionHelper.options; }
		public Object Option(CallerInfo caller, Table content) {
			optionHelper.Option(caller, content);
			return this;
		}
		public Object Option(CallerInfo caller, Array content) {
			optionHelper.Option(caller, content);
			return this;
		}
		public Object Turn(CallerInfo caller, Number amount) {
			target.Turn(caller, amount);
			return this;
		}
		public Object Turn(CallerInfo caller, IMsqValue degree) {
			target.Turn(caller, degree);
			return this;
		}
		public Object MirrorHolizontal(CallerInfo caller) {
			target.MirrorHolizontal(caller);
			return this;
		}
		public Object MirrorVertical(CallerInfo caller) {
			target.MirrorVertical(caller);
			return this;
		}
		public static void CalcContinuedStraightPosition(ISlideHead head, out float start_x, out float start_y) {
			if (head == null) {
				head = new SlideHeadNothing();
			}
			if (head is SlideHeadNothing) {
				start_x = 0;
				start_y = 0;
			}
			int? star = null;
			if (head.type == Type.SLIDE_HEAD_STAR) {
				star = (head as SlideHeadStar).button.GetIndex();
			}
			else if (head.type == Type.SLIDE_HEAD_BREAK) {
				star = (head as SlideHeadBreak).button.GetIndex();
			}
			if (star.HasValue) {
				int button = star.Value;
				// 右手.
				var pos = CircleCalculator.PointOnCircle(UnityEngine.Vector2.zero, 1, Constants.instance.GetPieceDegree(button));
				// 左手.
				pos = Constants.instance.ToLeftHandedCoordinateSystemPosition(pos);
				start_x = pos.x;
				start_y = pos.y;
			}
			else {
				start_x = 0;
				start_y = 0;
			}
		}
		public static bool CalcContinuedStraightPosition(ISlideTrimmedCommand beforeCommand, out float start_x, out float start_y) {
			// 前回が直線で今回も直線なら前回の終点をそのまま今回の始点に持ってくる.
			if (beforeCommand.type == Type.SLIDE_COMMAND_STRAIGHT) {
				var command = beforeCommand as SlideCommandStraight;
				// 左手.
				start_x = command.target.entity.x;
				start_y = command.target.entity.y;
				return true;
			}
			// 前回が曲線で今回は直線なら前回の終点を算出して、今回の始点に持ってくる.
			else if (beforeCommand.type == Type.SLIDE_COMMAND_CURVE) {
				var command = beforeCommand as SlideCommandCurve;
				// 右手.
				var pos = CircleCalculator.PointOnEllipse(Constants.instance.ToLeftHandedCoordinateSystemPosition(command.center.entity), command.radius.entity, command.start_degree.value + command.distance_degree.value);
				// 左手.
				pos = Constants.instance.ToLeftHandedCoordinateSystemPosition(pos);
				start_x = pos.x;
				start_y = pos.y;
				return true;
			}
			start_x = start_y = 0;
			return false;
		}
	}
	
	public class SlideCommandContinuedCurve : Object, ISlideContinuedCommand {
		public override Type type { get { return Type.SLIDE_COMMAND_CONTINUED_CURVE; } }
		public override Object Clone() {
			ISlideWait w = null;
			IStep s = null;
			if (wait != null)
				w = (ISlideWait)wait.Clone();
			if (step != null)
				s = (IStep)step.Clone();
			var ret = new SlideCommandContinuedCurve(w, s, center.Clone().Cast<Position>(), (IMsqValue)distance_degree.Clone());
			ret.optionHelper = optionHelper.Clone();
			return ret;
		}
		public override string Log() {
			var builder = new System.Text.StringBuilder();
			builder.Append("MsqScript.SlideCommandContinuedCurve(wait = ");
			builder.Append(wait != null ? wait.Log() : "null");
			builder.Append(", step = ");
			builder.Append(step != null ? step.Log() : "null");
			builder.Append(", center = ");
			builder.Append(center.Log());
			builder.Append(", distance_deg = ");
			builder.Append(distance_degree.Log());
			builder.Append(")");
			builder.Append(optionHelper.Log());
			return builder.ToString();
		}
		public override bool IsSlidePattern { get { return true; } }
		public override bool IsSlideChain { get { return true; } }
		public override bool IsSlideCommand { get { return true; } }
		public bool IsSlideContinuedCommand { get { return true; } }
		public bool IsStraight { get { return false; } }
		public bool IsCurve { get { return true; } }
		public SlideCommandContinuedCurve(ISlideWait wait, IStep step, Position center, IMsqValue distance_degree) {
			this.wait = wait;
			this.step = step;
			this.center = center;
			this.distance_degree = distance_degree;
			optionHelper = new SlidePatternOptionHelper();
		}
		public ISlideWait wait { get; set; }
		public IStep step { get; set; }
		public Position center;
		public IMsqValue distance_degree;
		public ISlideTrimmedCommand GetEntity(ISlideHead head, ISlideTrimmedCommand beforeCommand) {
			// SlideCommandCurveを取得する
			float rx, ry, sd;
			if (beforeCommand == null) {
				CalcContinuedCurvePosition (head, center.entity, out rx, out ry, out sd);
			}
			else {
				CalcContinuedCurvePosition (beforeCommand, center.entity, out rx, out ry, out sd);
			}
			return new SlideCommandCurve(wait, step, center, new Position(rx, ry), new Value(sd), distance_degree);
		}
		public SlidePatternOptionHelper optionHelper;
		public Table GetOption() { return optionHelper.options; }
		public Object Option(CallerInfo caller, Table content) {
			optionHelper.Option(caller, content);
			return this;
		}
		public Object Option(CallerInfo caller, Array content) {
			optionHelper.Option(caller, content);
			return this;
		}
		public Object Turn(CallerInfo caller, Number amount) {
			center.Turn(caller, amount);
			return this;
		}
		public Object Turn(CallerInfo caller, IMsqValue degree) {
			center.Turn(caller, degree);
			return this;
		}
		public Object MirrorHolizontal(CallerInfo caller) {
			center.MirrorHolizontal(caller);
			distance_degree.value *= -1;
			return this;
		}
		public Object MirrorVertical(CallerInfo caller) {
			center.MirrorVertical(caller);
			distance_degree.value *= -1;
			return this;
		}
		public static void CalcContinuedCurvePosition(ISlideHead head, UnityEngine.Vector2 center, out float radius_x, out float radius_y, out float start_degree) {
			UnityEngine.Vector2 pos;
			center = Constants.instance.ToLeftHandedCoordinateSystemPosition (center);
			// 右手.
			int? star = null;
			if (head == null) {
				head = new SlideHeadNothing();
			}
			if (head.type == Type.SLIDE_HEAD_STAR) {
				star = (head as SlideHeadStar).button.GetIndex();
			}
			else if (head.type == Type.SLIDE_HEAD_BREAK) {
				star = (head as SlideHeadBreak).button.GetIndex();
			}
			if (star.HasValue) {
				int button = star.Value;
				// 右手.
				pos = CircleCalculator.PointOnCircle (UnityEngine.Vector2.zero, 1, Constants.instance.GetPieceDegree (button));
				float radius = CircleCalculator.PointToPointDistance (center, pos);
				radius_x = radius;
				radius_y = radius;
				start_degree = CircleCalculator.PointToDegree(center, pos);
			}
			else {
				pos = UnityEngine.Vector2.zero;
				float radius = CircleCalculator.PointToPointDistance (center, pos);
				radius_x = radius;
				radius_y = radius;
				start_degree = CircleCalculator.PointToDegree (center, pos);
			}
		}
		public static bool CalcContinuedCurvePosition(ISlideTrimmedCommand beforeCommand, UnityEngine.Vector2 center, out float radius_x, out float radius_y, out float start_degree) {
			// 前回が直線で今回は曲線なら、前回の終点と、今回の回転軸から、今回の半径と始点角度を決める.
			if (beforeCommand.type == Type.SLIDE_COMMAND_STRAIGHT) {
				var command = beforeCommand as SlideCommandStraight;
				// 右手.
				center = Constants.instance.ToLeftHandedCoordinateSystemPosition(center);
				UnityEngine.Vector2 pos = Constants.instance.ToLeftHandedCoordinateSystemPosition(command.target.entity);
				float radius = CircleCalculator.PointToPointDistance(center, pos);
				radius_x = radius;
				radius_y = radius;
				start_degree = CircleCalculator.PointToDegree(center, pos);
				return true;
			}
			// 前回が曲線で今回も曲線なら、前回の回転軸・半径から求めた終点と、今回の回転軸から、今回の半径と始点角度を決める.
			else if (beforeCommand.type == Type.SLIDE_COMMAND_CURVE) {
				var command = beforeCommand as SlideCommandCurve;
				// 右手.
				center = Constants.instance.ToLeftHandedCoordinateSystemPosition(center);
				UnityEngine.Vector2 pos = CircleCalculator.PointOnEllipse(Constants.instance.ToLeftHandedCoordinateSystemPosition(command.center.entity), command.radius.entity, command.start_degree.value + command.distance_degree.value);
				float radius = CircleCalculator.PointToPointDistance(center, pos);
				radius_x = radius;
				radius_y = radius;
				start_degree = CircleCalculator.PointToDegree(center, pos);
				return true;
			}
			radius_x = radius_y = start_degree = 0;
			return false;
		}
	}

	// スライドパターンのオプションの共通項目.
	public class SlidePatternOptionHelper {
		public SlidePatternOptionHelper() {
			options = new Table();
		}
		public Table options;
		public void Option(CallerInfo caller, Table content) {
			foreach (var key in content.entity.Keys) {
				var value = content.entity[key];
				if (key == Document.NoteOption.SECRET) {
					if (value.IsKey) {
						value = new Text((value as IKey).key).Parse(null);
					}
					if (value.type == Type.FLAG) {
						options.entity[key] = value;
					}
				}
				else if ((key == Document.NoteOption.TIME ||
				          key == Document.NoteOption.FADE ||
				          key == Document.NoteOption.FADE_RATE ||
				          key == Document.NoteOption.SIZE ||
				          key == Document.NoteOption.SIZE_RATE)) {
					if (value.IsKey) {
						value = new Text((value as IKey).key).Parse(null);
					}
					if (!value.IsNaN) {
						options.entity[key] = value;
					}
				}
				else if (key == Document.NoteOption.COLOR) {
					string text = null;
					if (value.IsKey) {
						text = (value as IKey).key;
					}
					if (text != null) {
						switch (text) {
						case Document.NoteOption.SINGLE:
						case "1":
							options.entity[key] = new Text(Document.NoteOption.SINGLE);
							break;
						case Document.NoteOption.EACH:
						case "2":
							options.entity[key] = new Text(Document.NoteOption.EACH);
							break;
						}
					}
				}
			}
		}
		public void Option(CallerInfo caller, Array content) {
			foreach (var elm in content.entity) {
				string key = null;
				if (elm.IsKey)
					key =(elm as IKey).key;
				else continue;
				switch (key) {
				case Document.NoteOption.SECRET:
					options.entity[Document.NoteOption.SECRET] = new Flag(true);
					break;
				case Document.NoteOption.OVERT:
					options.entity[Document.NoteOption.SECRET] = new Flag(false);
					break;
				}
			}
		}
		public string Log() {
			if (options.entity.Count > 0) {
				return ".Option(" + options.Log() + ")";
			}
			return string.Empty;
		}
		public SlidePatternOptionHelper Clone() {
			var ret = new SlidePatternOptionHelper();
			ret.options = this.options.Clone().Cast<Table>();
			return ret;
		}
		/// <summary>
		/// 子にしかないkeyがあれば、子のvalueを使って埋める.
		/// </summary>
		public void FillFromChild(Table child) {
            options.Marge(null, child);
		}
	}
	
	public class Array : Object {
		public override Type type { get { return Type.ARRAY; } }
		public override Object Clone() { return new Array(entity.ToArray()); }
		public override string Log() {
			var builder = new System.Text.StringBuilder();
			builder.Append("MsqScript.Array[");
			int size = entity.Count;
			for (int i = 0; i < size; i++) {
				if (i > 0) {
					builder.Append(", ");
				}
				else {
					builder.Append(" ");
				}
				builder.Append(entity[i].Log());
			}
			builder.Append(" ]");
			return builder.ToString();
		}
		public List<Object> entity;
		public Array(params Object[] data) {
			entity = new List<Object>(data);
		}
		public Object Get(CallerInfo caller, Number index) {
			if (index.entity >= 0 && index.entity < entity.Count) {
				return entity[index.entity];
			}
			return new Exception(caller, ErrorCode.INDEX_OUT_OF_RANGE);
		}
		public Object Add(CallerInfo caller, params Object[] data) {
			entity.AddRange(data);
			return this;
		}
		public Object Set(CallerInfo caller, Number index, Object data) {
			if (index.entity >= 0 && index.entity < entity.Count) {
				entity[index.entity] = data;
				return this;
			}
			else if (index.entity == entity.Count) {
				entity.Add(data);
				return this;
			}
			return new Exception(caller, ErrorCode.INDEX_OUT_OF_RANGE);
		}
		public Object Insert(CallerInfo caller, Number index, params Object[] data) {
			if (index.entity >= 0 && index.entity < entity.Count) {
				entity.InsertRange(index.entity, data);
				return this;
			}
			else if (index.entity == entity.Count) {
				entity.AddRange(data);
				return this;
			}
			return new Exception(caller, ErrorCode.INDEX_OUT_OF_RANGE);
		}
		public Object Remove(CallerInfo caller, Number index) {
			if (index.entity >= 0 && index.entity < entity.Count) {
				entity.RemoveAt(index.entity);
				return this;
			}
			return new Exception(caller, ErrorCode.INDEX_OUT_OF_RANGE);
		}
		public Object Length(CallerInfo caller) {
			return new Number(entity.Count);
		}
		public Object IsEmpty(CallerInfo caller) {
			return new Flag(entity.Count == 0);
		}
	}
	
	public class Table : Object {
		public override Type type { get { return Type.TABLE; } }
		public override Object Clone() { return new Table(entity); }
		public override string Log() {
			var builder = new System.Text.StringBuilder();
			builder.Append("MsqScript.Table{");
			int index = 0;
			foreach (var key in entity.Keys) {
				if (index > 0) {
					builder.Append(", ");
				}
				else {
					builder.Append(" ");
				}
				builder.Append("MsqScript.Pair(MsqScript.Text(");
				builder.Append(key);
				builder.Append("), ");
				builder.Append(entity[key].Log());
				builder.Append(")");
				index++;
			}
			builder.Append(" }");
			return builder.ToString();
		}
		public Dictionary<string, Object> entity;
		public Table() : this(null) { }
		public Table(Dictionary<string, Object> data) {
			if (data == null)
				entity = new Dictionary<string, Object>();
			else
				entity = new Dictionary<string, Object>(data);
		}
		public Object Get(CallerInfo caller, IKey key) {
			if (entity.ContainsKey(key.key)) {
				return entity[key.key];
			}
			return new Exception(caller, ErrorCode.KEY_NOT_FOUND);
		}
		public Object Set(CallerInfo caller, IKey key, Object value) {
			entity[key.key] = value;
			return this;
		}
		public Object Remove(CallerInfo caller, IKey key) {
			entity.Remove(key.key);
			return this;
		}
		public Object HasKey(CallerInfo caller, IKey key) {
			return new Flag(entity.ContainsKey(key.key));
		}
		public Object GetKeys(CallerInfo caller) {
			var keys = new Text[entity.Count];
			int index = 0;
			foreach (var key in entity.Keys) {
				keys[index] = new Text(key);
				index++;
			}
			return new Array(keys);
		}
		public Object Length(CallerInfo caller) {
			return new Number(entity.Count);
		}
		public Object IsEmpty(CallerInfo caller) {
			return new Flag(entity.Count == 0);
		}
		/// <summary>
		/// <para>overwrite:false</para>
		/// <para>元にあるkeyのvalueを保持しながら、そのほかのデータは新しいデータのkeyのvalueで埋める</para>
		/// <para>overwrite:true</para>
		/// <para>元のデータは無視して、新しいデータで上書きする。元のデータにしかないキーはそのまま残る</para>
		/// </summary>
		public Object Marge(CallerInfo caller, Table data, Flag overwrite) {
			foreach (var key in data.entity.Keys) {
				if (overwrite.entity || !entity.ContainsKey(key)) {
					entity[key] = data.entity[key];
				}
			}
			return this;
		}
		public Object Marge(CallerInfo caller, Table data) {
			return Marge(caller, data, new Flag(false));
		}
	}

	public class Success : Object {
		public override Type type { get { return Type.SUCCESS; } }
		public override Object Clone() { return new Success(); }
		public override string Log() {
			return "MsqScript.Success()";
		}
		public override bool IsSuccess { get { return true; } }
		public Success() { }
	}

	public abstract class Order : Object {
		public override Type type { get { return Type.ORDER; } }
		public abstract OrderTitle title { get; }
	}

	public class OrderBpm : Order {
		public override OrderTitle title { get { return OrderTitle.SET_BPM; } }
		public IMsqValue aug { get; set; }
		public OrderBpm(IMsqValue aug) { this.aug = aug; }
		public override string Log() {
			return "MsqScript.OrderBpm(" + aug.value.ToString() + ")";
		}
		public override Object Clone() {
			return new OrderBpm(aug.Clone() as IMsqValue);
		}
	}

	public class OrderBeat : OrderBpm {
		public override OrderTitle title { get { return OrderTitle.SET_BEAT; } }
		public Number notePerBar { get; set; }
		public OrderBeat(IMsqValue aug) : this (aug, new Number(0)) { }
		public OrderBeat(IMsqValue aug, Number notePerBar) : base (aug) { this.notePerBar = notePerBar; }
		public override string Log() {
			return "MsqScript.OrderBeat(beat = " + aug.value.ToString() + ", notePerBar = " + notePerBar.entity.ToString() + ")";
		}
		public override Object Clone() {
			return new OrderBeat(aug.Clone() as IMsqValue, notePerBar.Clone().Cast<Number>());
		}
	}

	public class OrderInterval : OrderBpm {
		public override OrderTitle title { get { return OrderTitle.SET_INTERVAL; } }
		public Number notePerBar { get; set; }
		public OrderInterval(IMsqValue aug) : this (aug, new Number(0)) { }
		public OrderInterval(IMsqValue aug, Number notePerBar) : base (aug) { this.notePerBar = notePerBar; }
		public override string Log() {
			return "MsqScript.OrderInterval(interval = " + aug.value.ToString() + ", notePerBar = " + notePerBar.entity.ToString() + ")";
		}
		public override Object Clone() {
			return new OrderBeat(aug.Clone() as IMsqValue, notePerBar.Clone().Cast<Number>());
		}
	}

	public class OrderNotes : Order {
		public override OrderTitle title { get { return OrderTitle.SET_NOTES; } }
		public INote[] augs { get; set; }
		public OrderNotes(INote[] augs) { this.augs = augs; }
		public override string Log() {
			var builder = new System.Text.StringBuilder();
			builder.Append("MsqScript.OrderNotes(");
			for (int i = 0; i < augs.Length; i++) {
				builder.Append(augs[i].Log());
				if (i < augs.Length - 1) {
					builder.Append(",\n");
				}
			}
			builder.Append(")");
			return builder.ToString();
		}
		public override Object Clone() {
			var list = new List<INote>();
			foreach (var aug in augs) {
				var note = aug.Clone() as INote;
				if (note != null) {
					list.Add(note);
				}
			}
			return new OrderNotes(list.ToArray());
		}
	}

	public class OrderRest : Order {
		public override OrderTitle title { get { return OrderTitle.SET_REST; } }
		public Number aug { get; set; }
		public OrderRest(Number aug) { this.aug = aug; }
		public override string Log() {
			return "MsqScript.OrderRest(" + aug.entity.ToString() + ")";
		}
		public override Object Clone() {
			return new OrderRest(aug.Clone().Cast<Number>());
		}
	}

	public class OrderExtraFormat : Order {
		public override OrderTitle title { get { return OrderTitle.EXTRA_FORMAT; } }
		public TextDetailInfo place;
		public IKey formatName { get; set; }
		public Text script { get; set; }
		public OrderExtraFormat(TextDetailInfo place, IKey formatName, Text script) { this.place = place; this.formatName = formatName; this.script = script; }
		public override string Log() {
			return "MsqScript.OrderExtraFormat(" + formatName.key + ", " + script.entity + ")";
		}
		public override Object Clone() {
			return new OrderExtraFormat(place.Clone(), formatName.Clone() as IKey, script.Clone().Cast<Text>());
		}
	}
	
	public class Exception : Object {
		public override Type type { get { return Type.EXCEPTION; } }
		public override Object Clone() { return new Exception(caller, code); }
		public override string Log() {
			return "MsqScript.Exception(" + code.ToString() + ", " + TextDetailInfo.GetPlaceLog(place) + ")";
		}
		public override bool IsException { get { return true; } }
		protected CallerInfo caller;
		public TextDetailInfo place;
		public ErrorCode code;
		public Exception(CallerInfo caller, ErrorCode code) {
			this.caller = caller;
			this.place = caller.place;
			this.code = code;
		}
	}
	
	public class ArgumentsNotMatchException : Exception {
		public override Type type { get { return Type.ARGUMENTS_NOT_MATCH_EXCEPTION; } }
		public override Object Clone() { return new ArgumentsNotMatchException(caller, command, args); } //place.Clone()を使いたいかも。
		public override string Log() {
			var builder = new System.Text.StringBuilder();
			for (int i = 0; i < args.Length; i++) {
				if (i > 0) {
					builder.Append(", ");
				}
				builder.Append(args[i].type.ToString());
			}
			return "MsqScript.ArgumentsNotMatchException(" + field.ToString() + "." + command + "(" + builder.ToString() + ")" + ", " + TextDetailInfo.GetPlaceLog(place) + ")";
		}
		public Type field;
		public string command;
		public Object[] args;
		public ArgumentsNotMatchException(CallerInfo caller, string command, Object[] args) 
		: base (caller, ErrorCode.COMMAND_AUGUMENTS_NOT_MATCH) {
			this.field = caller.field.type;
			this.command = command;
			this.args = args;
		}
	}
	
	public class CommandNotFoundException : ArgumentsNotMatchException {
		public override Type type { get { return Type.COMMAND_NOT_FOUND_EXCEPTION; } }
		public override Object Clone() { return new CommandNotFoundException(caller, command, args); }
		public override string Log() {
			return "MsqScript.CommandNotFoundException(" + command + " in " + field.ToString() + ", " + TextDetailInfo.GetPlaceLog(place) + ")";
		}
		public CommandNotFoundException(CallerInfo caller, string command, Object[] args) 
		: base (caller, command, args) {
			code = ErrorCode.COMMAND_NOT_FOUND;
		}
	}
	
}