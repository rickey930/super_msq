﻿using UnityEngine;
using System;
using System.Text;
using System.Collections.Generic;

public class SimaiRawReader : SimaiReader {
	private static class Document {
		public const string AMPERSAND = "&";
		public const string EQUAL = "=";
		public const string RETURN = "\n";
		public const string SIMAI_SMALLER = "┃";
		public const string SIMAI_RETURN = "┓";
		public const string TITLE = "title";
		public const string TITLE_RUBY = "titleruby";
		public const string FREE_MESSAGE = "freemsg";
		public const string FREE_MESSAGE_LEVEL = "freemsg_";
		public const string ARTIST = "artist";
		public const string ARTIST_RUBY = "artistruby";
		public const string DES_LEVEL = "des_";
		public const string DESIGNER_LEVEL = "designer_";
		public const string FIRST = "first";
		public const string FIRST_LEVEL = "first_";
		public const string WHOLE_BPM = "wholebpm";
		public const string SEEK = "seek";
		public const string WAIT = "wait";
		public const string BG = "bg";
		public const string TRACK = "track";
		public const string ACTIVE_MESSAGE_FIRST = "amsg_first";
		public const string TAP_SOUND_OFFSET = "tap_ofs";
		public const string HOLD_SOUND_OFFSET = "hold_ofs";
		public const string SLIDE_SOUND_OFFSET = "slide_ofs";
		public const string BREAK_SOUND_OFFSET = "break_ofs";
		public const string LEVEL = "lv_";
		public const string INOTE = "inote_";
		public const string ACTIVE_MESSAGE_TIME = "amsg_time";
		public const string ACTIVE_MESSAGE_CONTENT = "amsg_content";
		public const int MAX_LEVEL = 7;
	}

	public string title { get; private set; }
	public string titleRuby { get; private set; }
	public string freeMessage { get; private set; }
	public Dictionary<int, string> freeMessages { get; private set; }
	public Dictionary<int, string> designers { get; private set; }
	public string artist { get; private set; }
	public string artistRuby { get; private set; }
	public decimal first { get; private set; }
	public Dictionary<int, decimal> firsts { get; private set; }
	public string wholeBpm { get; private set; }
	public decimal seek { get; private set; }
	public decimal wait { get; private set; }
	public string bg { get; private set; }
	public string track { get; private set; }
	public decimal activeMessageFirst { get; private set; }
	public decimal tapSoundOffset { get; private set; }
	public decimal holdSoundOffset { get; private set; }
	public decimal slideSoundOffset { get; private set; }
	public decimal breakSoundOffset { get; private set; }
	public Dictionary<int, string> level { get; private set; }
	public Dictionary<int, string> inote { get; private set; }
	public string activeMessageTime { get; private set; }
	public string activeMessageContent { get; private set; }
	public HashSet<string> allMacros { get; private set; }

	public SimaiRawReader() {
		freeMessages = new Dictionary<int, string>();
		designers = new Dictionary<int, string>();
		firsts = new Dictionary<int, decimal>();
		level = new Dictionary<int, string>();
		inote = new Dictionary<int, string>();
		allMacros = new HashSet<string>();
	}

	public static SimaiRawReader Read(string simaiRawScript, out CommonScriptFormatter.ReadResult result) {
		var reader = new SimaiRawReader();
		result = reader.Analyze(simaiRawScript);
		return reader;
	}
	
	public static TrackInformation ToTrackInformation(SimaiRawReader reader) {
		var ret = TrackInformation.NewInitialize ();
		ret.title = reader.title;
		ret.title_ruby = reader.titleRuby;
		ret.artist = reader.artist;
		ret.artist_ruby = reader.artistRuby;
		ret.whole_bpm = reader.wholeBpm;
		ret.seek = (float)reader.seek;
		ret.wait = (float)reader.wait;
		ret.simai_first = (float)reader.first;
		ret.jacket = reader.bg;
		ret.audio = reader.track;
		var levelNames = new Dictionary<int, string>() {
			{ 1, "EASY" }, { 2, "BASIC" }, { 3, "ADVANCED" }, { 4, "EXPERT" }, { 5, "MASTER" }, { 6, "Re:MASTER" }, { 7, "mai-EDIT" }
		};
		var scoreTank = new Dictionary<string, ScoreInformation> ();
		foreach (var key in reader.inote.Keys) {
			string id = levelNames.ContainsKey(key) ? levelNames[key] : key.ToString();
			var scoreInfo = scoreTank.ContainsKey(id) ? scoreTank[id] : ScoreInformation.NewInitialize();
			scoreInfo.level = reader.level.ContainsKey(key) ? reader.level[key] : string.Empty;
			scoreInfo.script_type = "simai";
			scoreInfo.notes_design = reader.designers.ContainsKey(key) ? reader.designers[key] : string.Empty;
			scoreInfo.score = reader.inote[key];
		}
		// スコア順並び替え.
		var difficulties = new string[] {
			"EASY", "BASIC", "ADVANCED", "EXPERT", "MASTER", "Re:MASTER"
		};
		System.Action<string> scoresSort = (difficulty) => {
			if (scoreTank.ContainsKey(difficulty)) {
				ret.scores[difficulty] = scoreTank[difficulty];
			}
		};
		// 基本スコアを優先で入れる.
		foreach (string difficulty in difficulties) {
			scoresSort(difficulty);
		}
		// その他のスコアは定義順(作った順).
		foreach (string key in scoreTank.Keys) {
			if (!ret.scores.ContainsKey(key)) {
				scoresSort(key);
			}
		}
		return ret;
	}

	public CommonScriptFormatter.ReadResult Analyze(string simaiRawScript) {
		CommonScriptFormatter.ReadResult analyzedResult;
		analyzedText = CommonScriptFormatter.Analyze(
			simaiRawScript,
			new CommonScriptFormatter.DefineSpecialText[] {
				new CommonScriptFormatter.DefineSpecialText("/*", "*/", CommonScriptFormatter.DefineSpecialText.ReadMode.COMMENT),
				new CommonScriptFormatter.DefineSpecialText(">>", "\n", CommonScriptFormatter.DefineSpecialText.ReadMode.ENDABLE_COMMENT),
				new CommonScriptFormatter.DefineSpecialText("//", "\n", CommonScriptFormatter.DefineSpecialText.ReadMode.ENDABLE_COMMENT),
				new CommonScriptFormatter.DefineSpecialText("/**", "**/", CommonScriptFormatter.DefineSpecialText.ReadMode.INTERPOSE_COMMENT),
			},
			new CommonScriptFormatter.DefineEscapeText[0],
			new string[] { "\r", "\n", " ", "\t", "　" },
			out analyzedResult
		);
		if (analyzedText.Length > 0) {
			if (analyzedResult != CommonScriptFormatter.ReadResult.SUCCESS) {
				return analyzedResult;
			}
		}

		textSize = analyzedText.Length;
		for (int i = textSize - 1; i >= 0; i--) {
			if (analyzedText[i].type == CommonScriptFormatter.TextType.SCRIPT) {
				lastScriptIndex = i;
				break;
			}
		}
		Main();
		return analyzedResult;
	}

	/// <summary>
	/// メインの処理.
	/// </summary>
	private void Main() {
		for (; index < textSize; index++) {
			ReadGlobal();
		}
	}

	private void ReadGlobal() {
		var keys = new string[] {
			Document.AMPERSAND, 
		};

		string str = SearchKey(keys);

		if (str == Document.AMPERSAND) {
			index++;
			ReadKey();
		}
	}

	private void ReadKey() {
		var keys = new string[] {
			Document.TITLE, Document.TITLE_RUBY, Document.ARTIST, Document.ARTIST_RUBY, Document.WHOLE_BPM, 
			Document.FREE_MESSAGE, Document.FREE_MESSAGE_LEVEL, Document.DES_LEVEL, Document.DESIGNER_LEVEL,
			Document.FIRST, Document.FIRST_LEVEL, Document.ACTIVE_MESSAGE_FIRST, Document.SEEK, Document.WAIT,
			Document.BG, Document.TRACK, 
			Document.TAP_SOUND_OFFSET, Document.HOLD_SOUND_OFFSET, Document.SLIDE_SOUND_OFFSET, Document.BREAK_SOUND_OFFSET,
			Document.LEVEL,
			Document.INOTE, Document.ACTIVE_MESSAGE_TIME, Document.ACTIVE_MESSAGE_CONTENT,
		};

		string str = SearchKey(keys);
		
		if (str == Document.TITLE) {
			index++;
			title = ReplaceSimaiSpecialChar(ReadText(), true);
			allMacros.Add(str);
		}
		else if (str == Document.TITLE_RUBY) {
			index++;
			titleRuby = ReplaceSimaiSpecialChar(ReadText(), true);
			allMacros.Add(str);
		}
		else if (str == Document.ARTIST) {
			index++;
			artist = ReplaceSimaiSpecialChar(ReadText(), false);
			allMacros.Add(str);
		}
		else if (str == Document.ARTIST_RUBY) {
			index++;
			artistRuby = ReplaceSimaiSpecialChar(ReadText(), false);
			allMacros.Add(str);
		}
		else if (str == Document.WHOLE_BPM) {
			index++;
			wholeBpm = ReplaceSimaiSpecialChar(ReadText(), true);
			allMacros.Add(str);
		}
		else if (str == Document.FREE_MESSAGE) {
			index++;
			freeMessage = ReplaceSimaiSpecialChar(ReadText(), false);
			allMacros.Add(str);
		}
		else if (str == Document.FREE_MESSAGE_LEVEL) {
			ReadLevel((lv, value)=>{
				freeMessages[lv] = ReplaceSimaiSpecialChar(value, false); 
				allMacros.Add(str + lv.ToString());
			});
		}
		else if (str == Document.DES_LEVEL || str == Document.DESIGNER_LEVEL) {
			ReadLevel((lv, value)=>{
				designers[lv] = ReplaceSimaiSpecialChar(value, false);
				allMacros.Add(str + lv.ToString());
			});
		}
		else if (str == Document.FIRST) {
			ReadValue((value)=>first=value);
			allMacros.Add(str);
		}
		else if (str == Document.FIRST_LEVEL) {
			ReadLevel((lv, text)=>{
				decimal value;
				if (decimal.TryParse(text, out value)) {
					firsts[lv] = value;
					allMacros.Add(str + lv.ToString());
				}
			});
		}
		else if (str == Document.ACTIVE_MESSAGE_FIRST) {
			ReadValue((value)=>activeMessageFirst=value);
			allMacros.Add(str);
		}
		else if (str == Document.SEEK) {
			ReadValue((value)=>seek=value);
			allMacros.Add(str);
		}
		else if (str == Document.WAIT) {
			ReadValue((value)=>wait=value);
			allMacros.Add(str);
		}
		else if (str == Document.BG) {
			index++;
			bg = ReplaceSimaiSpecialChar(ReadText(), true);
			allMacros.Add(str);
		}
		else if (str == Document.TRACK) {
			index++;
			track = ReplaceSimaiSpecialChar(ReadText(), true);
			allMacros.Add(str);
		}
		else if (str == Document.TAP_SOUND_OFFSET) {
			ReadValue((value)=>tapSoundOffset=value);
			allMacros.Add(str);
		}
		else if (str == Document.HOLD_SOUND_OFFSET) {
			ReadValue((value)=>holdSoundOffset=value);
			allMacros.Add(str);
		}
		else if (str == Document.SLIDE_SOUND_OFFSET) {
			ReadValue((value)=>slideSoundOffset=value);
			allMacros.Add(str);
		}
		else if (str == Document.BREAK_SOUND_OFFSET) {
			ReadValue((value)=>breakSoundOffset=value);
			allMacros.Add(str);
		}
		else if (str == Document.LEVEL) {
			ReadLevel((lv, value)=>{
				level[lv] = value;
				allMacros.Add(str + lv.ToString());
			});
		}
		else if (str == Document.INOTE) {
			ReadNote((lv, value)=>{
				inote[lv] = value;
				allMacros.Add(str + lv.ToString());
			});
		}
		else if (str == Document.ACTIVE_MESSAGE_TIME) {
			index++;
			activeMessageTime = ReadNote();
			allMacros.Add(str);
		}
		else if (str == Document.ACTIVE_MESSAGE_CONTENT) {
			index++;
			activeMessageContent = ReadNote();
			allMacros.Add(str);
		}
	}

	private string ReadText() {
		const string x = "x";
		const string open = Document.EQUAL;
		const string close = Document.RETURN;
		string format;
		var split = StoreReplace(open, close, new string[] { open, close }, x, new string[] { "\n" }, false, out format);
		
		const string fEXA = "=x\n";
		
		int valueCount = split.Length;
		switch (format) {
		case fEXA: { // =x\n
			if (valueCount == 1) {
				return split[0];
			}
			break;
		}
		}
		if (valueCount == 1 && isLastScriptIndex) {
			return split[0];
		}
		return string.Empty;
	}
	
	public string ReadNote() {
		const string x = "x";
		const string open = Document.EQUAL;
		const string close = Document.AMPERSAND;
		string format;
		var split = StoreReplace(open, close, new string[] { open, close }, x, new string[] { "\n", " ", "\t", "　" }, false, out format);
		
		const string fEXA = "=x&";
		const string fEA = "=&";
		
		int valueCount = split.Length;
		switch (format) {
		case fEXA: { // =x&
			if (valueCount == 1) {
				index--;
				return split[0];
			}
			break;
		}
		case fEA: { //=&
			index--;
			break;
		}
		}
		if (valueCount == 1 && isLastScriptIndex) {
			return split[0];
		}
		return string.Empty;
	}
	
	private string ReadLevel(Func<string> readMethod, out int level) {
		var keys = new string[Document.MAX_LEVEL];
		for (int i = 0; i < keys.Length; i++) keys[i] = (i + 1).ToString();
		string str = SearchKey(keys);
		
		if (str != null) {
			level = int.Parse(str);
			index++;
			return readMethod();
		}
		level = -1; //エラーとしてlevelを-1にする.
		return string.Empty;
	}
	
	private void ReadValue(Action<decimal> set) {
		index++;
		decimal value;
		string text = ReadText();
		if (decimal.TryParse(text, out value)) {
			set(value);
		}
	}
	
	private void ReadLevel(Action<int, string> set) {
		index++;
		int level;
		string value = ReadLevel(ReadText, out level);
		if (level >= 0) {
			set(level, value);
		}
	}

	private void ReadNote(Action<int, string> set) {
		index++;
		int level;
		string value = ReadLevel(ReadNote, out level);
		if (level >= 0) {
			set(level, value);
		}
	}

	private string ReplaceSimaiSpecialChar(string text, bool smaller) {
		var builder = new StringBuilder();
		for (int i = 0; i < text.Length; i++) {
			string s = text.Substring(i, 1);
			if (s == Document.SIMAI_RETURN) {
				builder.Append(Document.RETURN);
			}
			else if (s == Document.SIMAI_SMALLER && smaller) {
				// 何もしない. simaiの文字縮小機能は舞平方では対応しないが、表示はさせたくない.
			}
			else {
				builder.Append(s);
			}
		}
		return builder.ToString();
	}

}
