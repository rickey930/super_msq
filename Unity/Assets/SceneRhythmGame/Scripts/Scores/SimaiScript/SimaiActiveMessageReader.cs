﻿using UnityEngine;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel;

public class SimaiActiveMessageReader : SimaiNoteReader {
	public static class AmsgDocument {
		public const string ACTIVE_MESSAGE_HEAD = "┃";
		public const string ACTIVE_MESSAGE_RETURN = "┓";
		public const string RETURN = "\n";
		public const string ROUND_OPEN = "(";
		public const string ROUND_CLOSE = ")";
		public const string CURLY_OPEN = "{";
		public const string CURLY_CLOSE = "}";
		public const string CAMMA = ",";
		public const string SHOW_KEY = "1";
		public const string HIDE_KEY = "0";
		public const string END_KEY = "E";
	}

	private string[] messages;
	private int messageIndex;
	private bool isScrolling;
	private Vector2 position;
	private float scrollPosY;
	private float scale;
	private Color color;

	private bool nowBeatIsInterval;
	private float nowBpm;
	private float nowBeat;
	private bool useBeatIsInterval;
	private float useBpm;
	private float useBeat;
	private int stepBeatCount;
	private bool visible;
	private StringBuilder msqStep;

	public SimaiActiveMessageReader(Vector2 position, float scale, Color color) : this(scale, color) {
		this.position = position;
		this.isScrolling = false;
	}
	
	public SimaiActiveMessageReader(float scrollPosY, float scale, Color color) : this(scale, color) {
		this.scrollPosY = scrollPosY;
		this.isScrolling = true;
	}

	private SimaiActiveMessageReader(float scale, Color color) : base() {
		this.scale = scale;
		this.color = color;
		this.messageIndex = 0;
		
		this.nowBeatIsInterval = false;
		this.nowBpm = 0;
		this.nowBeat = 0;
		this.useBeatIsInterval = false;
		this.useBpm = 0;
		this.useBeat = 0;
		this.stepBeatCount = 0;
		this.visible = false;
		this.msqStep = new StringBuilder();
	}

	public static string[] ReadMessages(string simaiActiveMessageContentScript) {
		CommonScriptFormatter.ReadResult analyzedResult; // SimaiRawReaderのAnalyze後の文字列が渡されるはずなので、直にこのメソッドに文字列が渡されなければSuccess以外になりようがない.が念のため.
		var analyzedText = CommonScriptFormatter.Analyze(
			simaiActiveMessageContentScript,
			new CommonScriptFormatter.DefineSpecialText[] {
				new CommonScriptFormatter.DefineSpecialText("/*", "*/", CommonScriptFormatter.DefineSpecialText.ReadMode.COMMENT),
				new CommonScriptFormatter.DefineSpecialText(">>", "\n", CommonScriptFormatter.DefineSpecialText.ReadMode.ENDABLE_COMMENT),
				new CommonScriptFormatter.DefineSpecialText("//", "\n", CommonScriptFormatter.DefineSpecialText.ReadMode.ENDABLE_COMMENT),
				new CommonScriptFormatter.DefineSpecialText("/**", "**/", CommonScriptFormatter.DefineSpecialText.ReadMode.INTERPOSE_COMMENT),
			},
			new CommonScriptFormatter.DefineEscapeText[0],
			new string[] { "\r", "\n" },
			out analyzedResult
		);
		if (analyzedText.Length > 0) {
			if (analyzedResult != CommonScriptFormatter.ReadResult.SUCCESS) {
				return new string[0];
			}
		}

		int textSize = analyzedText.Length;
		var list = new List<string>();
		StringBuilder builder = null;
		for (int i = 0; i < textSize; i++) {
			var entity = analyzedText[i];
			if (entity.type == CommonScriptFormatter.TextType.SCRIPT) {
				var str = entity.GetString();
				if (str == AmsgDocument.ACTIVE_MESSAGE_HEAD) {
					if (builder != null) {
						list.Add(builder.ToString());
					}
					builder = new StringBuilder();
				}
				else if (builder != null) {
					if (str == AmsgDocument.ACTIVE_MESSAGE_RETURN) {
						builder.Append("\\n");
					}
					else {
						builder.Append(str);
					}
				}
			}
		}
		if (builder != null) {
			list.Add(builder.ToString());
		}
		return list.ToArray();
	}

	/// <summary>
	/// <para>【使用禁止】</para>
	/// <para>代わりに、 string Analyze(string simaiActiveMessageTimeScript, string simaiActiveMessageContentScript) を使ってください.</para>
	/// </summary>
	[BrowsableAttribute(false), EditorBrowsable(EditorBrowsableState.Never)]
	public override string Analyze(string simaiActiveMessageTimeScript) {
		throw new System.MethodAccessException("使用禁止メソッドです。");
	}

	public string Analyze(string simaiActiveMessageTimeScript, string simaiActiveMessageContentScript) {
		messages = ReadMessages(simaiActiveMessageContentScript);
		return base.Analyze(simaiActiveMessageTimeScript);
	}

	/// <summary>
	/// simaiScriptのamsg_time部とamsg_content部を解読してオーダーデータを作り、譜面のドキュメントに変換する.
	/// </summary>
	public static XMaimaiNote.XDocument.Document[] Convert(string simaiActiveMessageTimeScript, string simaiActiveMessageContentScript, Vector2 position, float scale, Color color) {
		var score = Read(simaiActiveMessageTimeScript, simaiActiveMessageContentScript, position, scale, color);
		if (score != null) {
			return new MaimaiScoreConverter (score).ReadScore ();
		}
		return null;
	}

	/// <summary>
	/// simaiScriptのamsg_time部とamsg_content部を解読してオーダーデータを作り、譜面のドキュメントに変換する.
	/// </summary>
	public static XMaimaiNote.XDocument.Document[] Convert(string simaiActiveMessageTimeScript, string simaiActiveMessageContentScript, float scrollPosY, float scale, Color color) {
		var score = Read(simaiActiveMessageTimeScript, simaiActiveMessageContentScript, scrollPosY, scale, color);
		if (score != null) {
			return new MaimaiScoreConverter (score).ReadScore ();
		}
		return null;
	}

	/// <summary>
	/// simaiScriptのamsg_time部とamsg_content部を解読してオーダーデータを作る.
	/// </summary>
	/// //XMaimaiScore.MaimaiScoreOrderData[]
	public static RhythmGameLibrary.Score.Order[] Read(string simaiActiveMessageTimeScript, string simaiActiveMessageContentScript, Vector2 position, float scale, Color color) {
		Result result;
		var msqScript = ToMsqScript(simaiActiveMessageTimeScript, simaiActiveMessageContentScript, position, scale, color, out result);
		if (result.type != ReadResult.SUCCESS) {
			Debug.Log (result.Log());
			return null;
		}
		return MsqScriptReader.Read (msqScript);
	}
	
	/// <summary>
	/// simaiScriptのamsg_time部とamsg_content部を解読してオーダーデータを作る.
	/// </summary>
	/// //XMaimaiScore.MaimaiScoreOrderData[]
	public static RhythmGameLibrary.Score.Order[] Read(string simaiActiveMessageTimeScript, string simaiActiveMessageContentScript, float scrollPosY, float scale, Color color) {
		Result result;
		var msqScript = ToMsqScript(simaiActiveMessageTimeScript, simaiActiveMessageContentScript, scrollPosY, scale, color, out result);
		if (result.type != ReadResult.SUCCESS) {
			Debug.Log (result.Log());
			return null;
		}
		return MsqScriptReader.Read (msqScript);
	}
	
	public static string ToMsqScript(string simaiActiveMessageTimeScript, string simaiActiveMessageContentScript, Vector2 position, float scale, Color color, out Result result) {
		var reader = new SimaiActiveMessageReader(position, scale, color);
		var field = reader.Analyze(simaiActiveMessageTimeScript, simaiActiveMessageContentScript);
		result = reader.result;
		return field;
	}
	
	public static string ToMsqScript(string simaiActiveMessageTimeScript, string simaiActiveMessageContentScript, float scrollPosY, float scale, Color color, out Result result) {
		var reader = new SimaiActiveMessageReader(scrollPosY, scale, color);
		var field = reader.Analyze(simaiActiveMessageTimeScript, simaiActiveMessageContentScript);
		result = reader.result;
		return field;
	}

	public override string ReadGlobal() {
		var keys = new string[] {
			AmsgDocument.ROUND_OPEN, AmsgDocument.CURLY_OPEN, 
			AmsgDocument.SHOW_KEY, AmsgDocument.HIDE_KEY,
			AmsgDocument.CAMMA,
			AmsgDocument.END_KEY,
		};
		
		var msq = new StringBuilder();
		string newLines;
		string str = SearchKey(keys, out newLines);
		msq.Append(newLines);
		
		System.Action rest = () => {
			if (restCount > 0 && ((str != Document.CAMMA && str != AmsgDocument.HIDE_KEY) || isLastScriptIndex)) {
				msq.Append(XMsqScript.Document.REST);
				msq.Append("(");
				if (restCount > 1) {
					msq.Append(restCount);
				}
				msq.Append(");");
				restCount = 0;
			}
		};

		if (str == AmsgDocument.ROUND_OPEN) {
			msq.Append(ReadBpm(out nowBpm));
		}
		else if (str == AmsgDocument.CURLY_OPEN) {
			msq.Append(ReadBeat(out nowBeatIsInterval, out nowBeat));
		}
		else if (str == AmsgDocument.SHOW_KEY) {
			index++;
			msq.Append(VisualizeKeyNext(str));
		}
		else if (str == AmsgDocument.HIDE_KEY) {
			index++;
			restCount++;
			msq.Append(VisualizeKeyNext(str));
		}
		else if (str == AmsgDocument.CAMMA) {
			if (useBpm != nowBpm || useBeat != nowBeat || useBeatIsInterval != nowBeatIsInterval) {
				if (this.visible) {
					stepBeatCount++;
				}
				WriteStep();
				useBpm = nowBpm;
				useBeat = nowBeat;
				useBeatIsInterval = nowBeatIsInterval;
			}
			else if (this.visible) {
				stepBeatCount++;
			}
			restCount++;
		}
		else if (str == AmsgDocument.END_KEY) {
			index = analyzedText.Length;
		}
		else if (!isLastScriptIndex) {
			CreateResult(ReadResult.GLOBAL_SYNTAX_ERROR);
			return string.Empty;
		}
		if (isLastScriptIndex) {
			if (this.visible) {
				WriteStep();
				WriteNote(msq);
				this.visible = false;
			}
		}
		rest();
		return msq.ToString();
	}
	
	private string VisualizeKeyNext(string visible) {
		var keys = new string[] {
			AmsgDocument.CAMMA,
		};
		var msq = new StringBuilder();
		string newLines;
		string str = SearchKey(keys, out newLines);
		msq.Append(newLines);
		if (str == AmsgDocument.CAMMA || isLastScriptIndex) {
			if (useBpm != nowBpm || useBeat != nowBeat || useBeatIsInterval != nowBeatIsInterval) {
				if (this.visible) {
					stepBeatCount++;
				}
				WriteStep();
				useBpm = nowBpm;
				useBeat = nowBeat;
				useBeatIsInterval = nowBeatIsInterval;
			}
			else if (this.visible) {
				stepBeatCount++;
			}
			if (this.visible && (visible == AmsgDocument.SHOW_KEY || visible == AmsgDocument.HIDE_KEY)) {
				WriteStep();
				WriteNote(msq);
				this.visible = false;
			}
			if (visible == AmsgDocument.SHOW_KEY) {
				this.visible = true;
				if (isLastScriptIndex) {
					// 最後のメッセージは、simaiはゲーム終了まで表示されるが、舞平方ではそれはできないので1拍だけ表示するようにする.
					stepBeatCount++;
					WriteStep();
					WriteNote(msq);
					this.visible = false;
				}
			}
		}
		else {
			CreateResult(ReadResult.BUTTON_NEXT_SYNTAX_ERROR);
			return string.Empty;
		}
		return msq.ToString();
	}
	
	private void WriteNote(StringBuilder msq) {
		if (messageIndex < messages.Length) {
			msq.Append(XMsqScript.Document.NOTE);
			msq.Append("(");
			msq.Append(isScrolling ? XMsqScript.Document.SCROLL_MESSAGE : XMsqScript.Document.MESSAGE);
			msq.Append("(<\"");
			msq.Append(messages[messageIndex]);
			msq.Append("\">,");
			if (isScrolling) {
				msq.Append(scrollPosY);
				msq.Append(",");
			}
			else {
				msq.Append(XMsqScript.Document.POSITION);
				msq.Append("(");
				msq.Append(position.x);
				msq.Append(",");
				msq.Append(position.y);
				msq.Append("),");
			}
			msq.Append(scale);
			msq.Append(",");
			msq.Append(XMsqScript.Document.RGBA);
			msq.Append("(");
			msq.Append(color.r);
			msq.Append(",");
			msq.Append(color.g);
			msq.Append(",");
			msq.Append(color.b);
			msq.Append(",");
			msq.Append(color.a);
			msq.Append("),");
			msq.Append(XMsqScript.Document.STEPS);
			msq.Append("(");
			msq.Append(msqStep.ToString());
			msq.Append(")));");
			messageIndex++;
			msqStep.Remove(0, msqStep.Length);
		}
	}
	
	private void WriteStep() {
		if (useBpm == 0 || useBeat == 0 || stepBeatCount == 0)
			return;
		if (msqStep.Length > 0) {
			msqStep.Append(",");
		}
		msqStep.Append(XMsqScript.Document.STEP);
		msqStep.Append("(");
		if (!useBeatIsInterval) {
			msqStep.Append(useBpm);
			msqStep.Append(",");
			msqStep.Append(useBeat);
			msqStep.Append(",");
			msqStep.Append(stepBeatCount);
			msqStep.Append(")");
		}
		else {
			msqStep.Append(useBeat * stepBeatCount);
			msqStep.Append(")");
		}
		stepBeatCount = 0;
	}

}
