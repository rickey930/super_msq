﻿using UnityEngine;
using System.Text;
using System.Collections.Generic;

public class SimaiNoteReader : SimaiReader {
	public enum ReadResult {
		/// <summary>
		/// 変換成功.
		/// </summary>
		SUCCESS,
		/// <summary>
		/// 必須のコメント終了キーが見つからなかった.
		/// </summary>
		REQUIRED_COMMENT_END_SPECIAL_KEY_NOT_FOUND,
		/// <summary>
		/// 必須の文字列化終了キーが見つからなかった.
		/// </summary>
		REQUIRED_TEXT_END_SPECIAL_KEY_NOT_FOUND,
		/// <summary>
		/// スクリプト読み込みモード中に別のモードの終了キーが見つかった.
		/// </summary>
		FIND_END_SPECIAL_KEY_IN_READING_SCRIPT,
		/// <summary>
		/// グローバルの文法エラー.
		/// </summary>
		GLOBAL_SYNTAX_ERROR,
		/// <summary>
		/// BPMパースエラー.
		/// </summary>
		BPM_PARSE_ERROR,
		/// <summary>
		/// BPM文法エラー.
		/// </summary>
		BPM_SYNTAX_ERROR,
		/// <summary>
		/// Beatパースエラー.
		/// </summary>
		BEAT_PARSE_ERROR,
		/// <summary>
		/// Barパースエラー.
		/// </summary>
		BAR_PARSE_ERROR,
		/// <summary>
		/// Beat文法エラー.
		/// </summary>
		BEAT_SYNTAX_ERROR,
		/// <summary>
		/// Step:BPMパースエラー.
		/// </summary>
		STEP_BPM_PARSE_ERROR,
		/// <summary>
		/// Step:Beatパースエラー.
		/// </summary>
		STEP_BEAT_PARSE_ERROR,
		/// <summary>
		/// Step:Lengthパースエラー.
		/// </summary>
		STEP_LENGTH_PARSE_ERROR,
		/// <summary>
		/// Step:Intervalパースエラー.
		/// </summary>
		STEP_INTERVAL_PARSE_ERROR,
		/// <summary>
		/// Wait:BPMパースエラー.
		/// </summary>
		WAIT_BPM_PARSE_ERROR,
		/// <summary>
		/// Wait:Beatパースエラー.
		/// </summary>
		WAIT_BEAT_PARSE_ERROR,
		/// <summary>
		/// Step:Lengthパースエラー.
		/// </summary>
		WAIT_LENGTH_PARSE_ERROR,
		/// <summary>
		/// Step:Intervalパースエラー.
		/// </summary>
		WAIT_INTERVAL_PARSE_ERROR,
		/// <summary>
		/// Step文法エラー.
		/// </summary>
		STEP_SYNTAX_ERROR,
		/// <summary>
		/// ボタンへ付加する属性の文法エラー.
		/// </summary>
		BUTTON_NEXT_SYNTAX_ERROR,
		/// <summary>
		/// 分割キーがないタップ入力時の文法エラー.
		/// </summary>
		BUTTON_BUTTON_NEXT_SYNTAX_ERROR,
		/// <summary>
		/// 分割キーの次の文字の文法エラー.
		/// </summary>
		SLASH_NEXT_SYNTAX_ERROR,
		/// <summary>
		/// ホールドのステップにwaitが指定してある.
		/// </summary>
		HOLD_INVALIDATE_STEP_ERROR,
		/// <summary>
		/// ホールドキーの次の文字の文法エラー.
		/// </summary>
		HOLD_KEY_NEXT_ERROR,
		/// <summary>
		/// ホールドのステップの次の文字の文法エラー.
		/// </summary>
		HOLD_STEP_NEXT_ERROR,
		/// <summary>
		/// ブレイクキーの次の文字の文法エラー.
		/// </summary>
		BREAK_KEY_NEXT_ERROR,
		/// <summary>
		/// スターキーの次の文字の文法エラー.
		/// </summary>
		STAR_KEY_NEXT_ERROR,
		/// <summary>
		/// スライド形キーの次の文字の文法エラー.
		/// </summary>
		SLIDE_SHAPE_KEY_NEXT_ERROR,
		/// <summary>
		/// スライドターゲットの次の文字の文法エラー.
		/// </summary>
		SLIDE_TARGET_NEXT_ERROR,
		/// <summary>
		/// スライドのステップの次の文字の文法エラー.
		/// </summary>
		SLIDE_STEP_NEXT_ERROR,
		/// <summary>
		/// 同一始点スライドキーの次の文字の文法エラー.
		/// </summary>
		SAME_HEAD_KEY_NEXT_ERROR,
		/// <summary>
		/// msq直接入力の文法エラー.
		/// </summary>
		DIRECT_MSQ_INPUT_ERROR,
		/// <summary>
		/// 直接入力キーの次の文字の文法エラー.
		/// </summary>
		NUMBERSIGN_NEXT_ERROR,
		/// <summary>
		/// maipad式スライド入力のセンサーの次の文字の文法エラー.
		/// </summary>
		DIRECT_SLIDE_SENSOR_NEXT_ERROR,
		/// <summary>
		/// maipad式スライド入力のスライド形の次の文字の文法エラー.
		/// </summary>
		DIRECT_SLIDE_SHAPE_NEXT_ERROR,
		/// <summary>
		/// maipad式スライド入力の終点の次の文字の文法エラー.
		/// </summary>
		DIRECT_SLIDE_TARGET_NEXT_ERROR,
		/// <summary>
		/// maipad式スライド入力のステップの次の文字の文法エラー.
		/// </summary>
		DIRECT_SLIDE_STEP_NEXT_ERROR,
		/// <summary>
		/// maipad式スライド入力の同一始点スライドキーの次の文字の文法エラー.
		/// </summary>
		DIRECT_SLIDE_SAME_HEAD_KEY_NEXT_ERROR,

	}
	/// <summary>
	/// simaiScriptのinote部を解読してオーダーデータを作り、譜面のドキュメントに変換する.
	/// </summary>
	public static XMaimaiNote.XDocument.Document[] Convert(string simaiNoteScript) {
		var score = Read (simaiNoteScript);
		if (score != null) {
			return new MaimaiScoreConverter (score).ReadScore ();
		}
		return null;
	}
	/// <summary>
	/// simaiScriptのinote部を解読してオーダーデータを作る.
	/// </summary>
	/// //XMaimaiScore.MaimaiScoreOrderData[]
	public static RhythmGameLibrary.Score.Order[] Read(string simaiNoteScript) {
		Result result;
		var msqScript = ToMsqScript (simaiNoteScript, out result);
		if (result.type != ReadResult.SUCCESS) {
			Debug.Log (result.Log());
			return null;
		}
		return MsqScriptReader.Read (msqScript);
	}

	public static string ToMsqScript(string simaiNoteScript, out Result result) {
		var reader = new SimaiNoteReader();
		var field = reader.Analyze(simaiNoteScript);
		result = reader.result;
		return field;
	}

	public class Result {
		public TextDetailInfo place { get; private set; }
		public ReadResult type { get; private set; }
		public Result(TextDetailInfo place, ReadResult type) {
			this.place = place;
			this.type = type;
		}
		public string Log() {
			return type.ToString() + (type == ReadResult.SUCCESS ? string.Empty : "\r\n" + TextDetailInfo.GetPlaceLog(place));
		}
	}

	protected void CreateResult(ReadResult type) {
		if (result == null) {
			TextDetailInfo text = null;
			if (type != ReadResult.SUCCESS && index < textSize) {
				text = analyzedText[index].text[0];
			}
			result = new Result(text, type);
		}
	}
	
	protected static class Document {
		public const string ROUND_OPEN = "(";
		public const string ROUND_CLOSE = ")";
		public const string CURLY_OPEN = "{";
		public const string CURLY_CLOSE = "}";
		public const string SQUARE_OPEN = "[";
		public const string SQUARE_CLOSE = "]";
		public const string CAMMA = ",";
		public const string COLON = ":";
		public const string NUMBER = "#";
		public const string SLASH = "/";
		public const string HOLD_KEY = "h";
		public const string BREAK_KEY = "b";
		public const string STAR_KEY = "$";
		public const string SAMEHEAD_KEY = "*";
		public const string BUTTON_1 = "1";
		public const string BUTTON_2 = "2";
		public const string BUTTON_3 = "3";
		public const string BUTTON_4 = "4";
		public const string BUTTON_5 = "5";
		public const string BUTTON_6 = "6";
		public const string BUTTON_7 = "7";
		public const string BUTTON_8 = "8";
		public const string SENSOR_A = "A";
		public const string SENSOR_A1 = "A1";
		public const string SENSOR_A2 = "A2";
		public const string SENSOR_A3 = "A3";
		public const string SENSOR_A4 = "A4";
		public const string SENSOR_A5 = "A5";
		public const string SENSOR_A6 = "A6";
		public const string SENSOR_A7 = "A7";
		public const string SENSOR_A8 = "A8";
		public const string SENSOR_B = "B";
		public const string SENSOR_B1 = "B1";
		public const string SENSOR_B2 = "B2";
		public const string SENSOR_B3 = "B3";
		public const string SENSOR_B4 = "B4";
		public const string SENSOR_B5 = "B5";
		public const string SENSOR_B6 = "B6";
		public const string SENSOR_B7 = "B7";
		public const string SENSOR_B8 = "B8";
		public const string SENSOR_C = "C";
		public const string SHAPE_STRAIGHT = "-";
		public const string SHAPE_CURVE = "^";
		public const string SHAPE_RIGHT = ">";
		public const string SHAPE_LEFT = "<";
		public const string SHAPE_P = "p";
		public const string SHAPE_Q = "q";
		public const string SHAPE_S = "s";
		public const string SHAPE_Z = "z";
		public const string SHAPE_V = "v";
		public const string SHAPE_PP = "pp";
		public const string SHAPE_QQ = "qp";
		public const string SHAPE_V1 = "V1";
		public const string SHAPE_V2 = "V2";
		public const string SHAPE_V3 = "V3";
		public const string SHAPE_V4 = "V4";
		public const string SHAPE_V5 = "V5";
		public const string SHAPE_V6 = "V6";
		public const string SHAPE_V7 = "V7";
		public const string SHAPE_V8 = "V8";
		public const string SHAPE_W = "w";
		public const string END_KEY = "E";

		public static bool IsButton(string key) {
			switch(key) {
			case BUTTON_1:
			case BUTTON_2:
			case BUTTON_3:
			case BUTTON_4:
			case BUTTON_5:
			case BUTTON_6:
			case BUTTON_7:
			case BUTTON_8:
				return true;
			default:
				return false;
			}
		}
		
		public static bool IsSensor(string key) {
			switch(key) {
			case SENSOR_A1:
			case SENSOR_A2:
			case SENSOR_A3:
			case SENSOR_A4:
			case SENSOR_A5:
			case SENSOR_A6:
			case SENSOR_A7:
			case SENSOR_A8:
			case SENSOR_B1:
			case SENSOR_B2:
			case SENSOR_B3:
			case SENSOR_B4:
			case SENSOR_B5:
			case SENSOR_B6:
			case SENSOR_B7:
			case SENSOR_B8:
			case SENSOR_C:
				return true;
			default:
				return false;
			}
		}
		
		public static string GetSensorABC(string key) {
			switch(key) {
			case SENSOR_A1:
			case SENSOR_A2:
			case SENSOR_A3:
			case SENSOR_A4:
			case SENSOR_A5:
			case SENSOR_A6:
			case SENSOR_A7:
			case SENSOR_A8:
				return SENSOR_A;
			case SENSOR_B1:
			case SENSOR_B2:
			case SENSOR_B3:
			case SENSOR_B4:
			case SENSOR_B5:
			case SENSOR_B6:
			case SENSOR_B7:
			case SENSOR_B8:
				return SENSOR_B;
			case SENSOR_C:
				return SENSOR_C;
			default:
				return string.Empty;
			}
		}
		
		public static bool IsSlideShape(string key) {
			switch(key) {
			case SHAPE_STRAIGHT:
			case SHAPE_CURVE:
			case SHAPE_RIGHT:
			case SHAPE_LEFT:
			case SHAPE_P:
			case SHAPE_Q:
			case SHAPE_S:
			case SHAPE_Z:
			case SHAPE_V:
			case SHAPE_PP:
			case SHAPE_QQ:
			case SHAPE_W:
			case SHAPE_V1:
			case SHAPE_V2:
			case SHAPE_V3:
			case SHAPE_V4:
			case SHAPE_V5:
			case SHAPE_V6:
			case SHAPE_V7:
			case SHAPE_V8:
				return true;
			default:
				return false;
			}
		}
	}

	protected CommonScriptFormatter.ReadResult analyzedResult;
	public Result result { get; private set; }
	public string field { get; private set; }
	protected int restCount;
	private Dictionary<string, string> slideShapeReplacer;
	private Dictionary<string, string> directSlideShapeReplacer;

	public SimaiNoteReader() {
		index = 0;
		restCount = 0;
		slideShapeReplacer = new Dictionary<string, string>();
		slideShapeReplacer[Document.SHAPE_STRAIGHT] = XMsqScript.Document.SIMAI_SHAPE_STRAIGHT;
		slideShapeReplacer[Document.SHAPE_CURVE] = XMsqScript.Document.SIMAI_SHAPE_CURVE;
		slideShapeReplacer[Document.SHAPE_RIGHT] = XMsqScript.Document.SIMAI_SHAPE_RIGHT;
		slideShapeReplacer[Document.SHAPE_LEFT] = XMsqScript.Document.SIMAI_SHAPE_LEFT;
		slideShapeReplacer[Document.SHAPE_P] = XMsqScript.Document.SIMAI_SHAPE_P;
		slideShapeReplacer[Document.SHAPE_Q] = XMsqScript.Document.SIMAI_SHAPE_Q;
		slideShapeReplacer[Document.SHAPE_S] = XMsqScript.Document.SIMAI_SHAPE_S;
		slideShapeReplacer[Document.SHAPE_Z] = XMsqScript.Document.SIMAI_SHAPE_Z;
		slideShapeReplacer[Document.SHAPE_V] = XMsqScript.Document.SIMAI_SHAPE_V;
		slideShapeReplacer[Document.SHAPE_PP] = XMsqScript.Document.SIMAI_SHAPE_PP;
		slideShapeReplacer[Document.SHAPE_QQ] = XMsqScript.Document.SIMAI_SHAPE_QQ;
		slideShapeReplacer[Document.SHAPE_W] = XMsqScript.Document.SIMAI_SHAPE_W;
		slideShapeReplacer[Document.SHAPE_V1] = XMsqScript.Document.SIMAI_SHAPE_V1;
		slideShapeReplacer[Document.SHAPE_V2] = XMsqScript.Document.SIMAI_SHAPE_V2;
		slideShapeReplacer[Document.SHAPE_V3] = XMsqScript.Document.SIMAI_SHAPE_V3;
		slideShapeReplacer[Document.SHAPE_V4] = XMsqScript.Document.SIMAI_SHAPE_V4;
		slideShapeReplacer[Document.SHAPE_V5] = XMsqScript.Document.SIMAI_SHAPE_V5;
		slideShapeReplacer[Document.SHAPE_V6] = XMsqScript.Document.SIMAI_SHAPE_V6;
		slideShapeReplacer[Document.SHAPE_V7] = XMsqScript.Document.SIMAI_SHAPE_V7;
		slideShapeReplacer[Document.SHAPE_V8] = XMsqScript.Document.SIMAI_SHAPE_V8;

		directSlideShapeReplacer = new Dictionary<string, string>();
		directSlideShapeReplacer[Document.SHAPE_STRAIGHT] = XMsqScript.Document.MAIPAD_SHAPE_STRAIGHT;
		directSlideShapeReplacer[Document.SHAPE_CURVE] = XMsqScript.Document.MAIPAD_SHAPE_CURVE;
		directSlideShapeReplacer[Document.SHAPE_RIGHT] = XMsqScript.Document.MAIPAD_SHAPE_RIGHT;
		directSlideShapeReplacer[Document.SHAPE_LEFT] = XMsqScript.Document.MAIPAD_SHAPE_LEFT;
	}

	public virtual string Analyze(string simaiNoteScript) {
		analyzedText = CommonScriptFormatter.Analyze(
			simaiNoteScript,
				new CommonScriptFormatter.DefineSpecialText[] {
				new CommonScriptFormatter.DefineSpecialText("/*", "*/", CommonScriptFormatter.DefineSpecialText.ReadMode.COMMENT),
				new CommonScriptFormatter.DefineSpecialText(">>", "\n", CommonScriptFormatter.DefineSpecialText.ReadMode.ENDABLE_COMMENT),
				new CommonScriptFormatter.DefineSpecialText("//", "\n", CommonScriptFormatter.DefineSpecialText.ReadMode.ENDABLE_COMMENT),
				new CommonScriptFormatter.DefineSpecialText("/**", "**/", CommonScriptFormatter.DefineSpecialText.ReadMode.INTERPOSE_COMMENT),
				new CommonScriptFormatter.DefineSpecialText("\"", "\"", CommonScriptFormatter.DefineSpecialText.ReadMode.TEXT),
				new CommonScriptFormatter.DefineSpecialText("\'", "\'", CommonScriptFormatter.DefineSpecialText.ReadMode.TEXT),
				new CommonScriptFormatter.DefineSpecialText("<\"", "\">", CommonScriptFormatter.DefineSpecialText.ReadMode.INTERPOSE_TEXT),
			},
			new CommonScriptFormatter.DefineEscapeText[] {
				new CommonScriptFormatter.DefineEscapeText("\\\"", "\""),
				new CommonScriptFormatter.DefineEscapeText("\\\'", "\'"),
				new CommonScriptFormatter.DefineEscapeText("\\\\", "\\"),
				new CommonScriptFormatter.DefineEscapeText("\\<", "<"),
				new CommonScriptFormatter.DefineEscapeText("\\>", ">"),
				new CommonScriptFormatter.DefineEscapeText("\\n", "\n"),
				new CommonScriptFormatter.DefineEscapeText("\\r", "\r"),
				new CommonScriptFormatter.DefineEscapeText("\\t", "\t"),
				new CommonScriptFormatter.DefineEscapeText("\\f", "\f"),
				new CommonScriptFormatter.DefineEscapeText("\\a", "\a"),
				new CommonScriptFormatter.DefineEscapeText("\\v", "\v"),
			},
			new string[] { "\r", "\n", " ", "\t", "　" },
			out analyzedResult
		);
		if (analyzedText.Length > 0) {
			if (analyzedResult == CommonScriptFormatter.ReadResult.REQUIRED_COMMENT_END_SPECIAL_KEY_NOT_FOUND) {
				result = new Result(analyzedText[analyzedText.Length - 1].text[0], ReadResult.REQUIRED_COMMENT_END_SPECIAL_KEY_NOT_FOUND);
				field = null;
			}
			if (analyzedResult == CommonScriptFormatter.ReadResult.REQUIRED_TEXT_END_SPECIAL_KEY_NOT_FOUND) {
				result = new Result(analyzedText[analyzedText.Length - 1].text[0], ReadResult.REQUIRED_TEXT_END_SPECIAL_KEY_NOT_FOUND);
				field = null;
			}
			if (analyzedResult == CommonScriptFormatter.ReadResult.FIND_END_SPECIAL_KEY_IN_READING_SCRIPT) {
				result = new Result(analyzedText[analyzedText.Length - 1].text[0], ReadResult.FIND_END_SPECIAL_KEY_IN_READING_SCRIPT);
				field = null;
			}
		}
		textSize = analyzedText.Length;
		for (int i = textSize - 1; i >= 0; i--) {
			if (analyzedText[i].type == CommonScriptFormatter.TextType.SCRIPT) {
				lastScriptIndex = i;
				break;
			}
		}
		field = Main();
		return field;
	}

	/// <summary>
	/// メインの処理.
	/// </summary>
	public string Main() {
		var msq = new StringBuilder();
		for (; index < textSize && result == null; index++) {
			msq.Append(ReadGlobal());
		}
		CreateResult(ReadResult.SUCCESS);
		return msq.ToString();
	}
	
	private void WriteTap(StringBuilder msq, string button) {
		msq.Append(XMsqScript.Document.TAP);
		msq.Append("(");
		msq.Append(button);
		msq.Append(")");
	}
	
	private void WriteBreak(StringBuilder msq, string button) {
		msq.Append(XMsqScript.Document.BREAK);
		msq.Append("(");
		msq.Append(button);
		msq.Append(")");
	}
	
	private void WriteStarOption(StringBuilder msq) {
		msq.Append("{");
		msq.Append(XMsqScript.Document.NoteOption.SHAPE);
		msq.Append(":");
		msq.Append(XMsqScript.Document.NoteOption.STAR);
		msq.Append("}");
	}
	
	private void WriteSlideHead(StringBuilder msq, string button, bool isBreak) {
		msq.Append(XMsqScript.Document.SLIDE);
		msq.Append("(");
		if (isBreak) {
			WriteBreak(msq, button);
		}
		else {
			WriteTap(msq, button);
		}
		msq.Append(",");
	}
	
	public virtual string ReadGlobal() {
		var keys = new string[] {
			Document.ROUND_OPEN, Document.CURLY_OPEN, 
			Document.BUTTON_1, Document.BUTTON_2, Document.BUTTON_3, Document.BUTTON_4, Document.BUTTON_5, Document.BUTTON_6, Document.BUTTON_7, Document.BUTTON_8,
			Document.NUMBER,
			Document.CAMMA,
			Document.END_KEY,
		};
		
		var msq = new StringBuilder();
		string newLines;
		string str = SearchKey(keys, out newLines);
		msq.Append(newLines);

		System.Action rest = () => {
			if (restCount > 0 && (str != Document.CAMMA || isLastScriptIndex)) {
				msq.Append(XMsqScript.Document.REST);
				msq.Append("(");
				if (restCount > 1) {
					msq.Append(restCount);
				}
				msq.Append(");");
				restCount = 0;
			}
		};
		rest();
		
		if (str == Document.ROUND_OPEN) {
			msq.Append(ReadBpm());
		}
		else if (str == Document.CURLY_OPEN) {
			msq.Append(ReadBeat());
		}
		else if (Document.IsButton(str)) {
			msq.Append(XMsqScript.Document.NOTE);
			msq.Append("(");
			index++;
			msq.Append(ButtonNext(str));
			msq.Append(");");
		}
		else if (str == Document.NUMBER) {
			index++;
			msq.Append(NumberSignNext());
		}
		else if (str == Document.CAMMA) {
			restCount++;
		}
		else if (str == Document.END_KEY) {
			index = analyzedText.Length;
		}
		else if (!isLastScriptIndex) {
			CreateResult(ReadResult.GLOBAL_SYNTAX_ERROR);
			return string.Empty;
		}
		if (isLastScriptIndex) {
			rest();
		}
		return msq.ToString();
	}
	
	public string ReadBpm() {
		float a;
		return ReadBpm(out a);
	}
	public string ReadBpm(out float _value) {
		_value = 0;
		const string x = "x";
		const string open = Document.ROUND_OPEN;
		const string close = Document.ROUND_CLOSE;
		string format;
		string newLines;
		var split = StoreReplace(open, close, new string[] { open, close }, x, null, true, out format, out newLines);

		const string fRXR = "(x)";

		int valueCount = split.Length;
		switch (format) {
		case fRXR: { // (x)
			if (valueCount == 1) {
				float value;
				if (!float.TryParse(split[0], out value)) {
					CreateResult(ReadResult.BPM_PARSE_ERROR);
					return string.Empty;
				}
				_value = value;
				return newLines + XMsqScript.Document.BPM + "(" + value + ");";
			}
			break;
		}
		}
		CreateResult(ReadResult.BPM_SYNTAX_ERROR);
		return string.Empty;
	}

	public string ReadBeat() {
		bool a;
		float b;
		return ReadBeat(out a, out b);
	}
	protected string ReadBeat(out bool _interval, out float _value) {
		_interval = false;
		_value = 0;
		const string x = "x";
		const string open = Document.CURLY_OPEN;
		const string close = Document.CURLY_CLOSE;
		string format;
		string newLines;
		var split = StoreReplace(open, close, new string[] { open, close, Document.NUMBER, Document.COLON }, x, null, true, out format, out newLines);
		
		const string fCXC = "{x}";
		const string fCNXC = "{#x}";
		const string fCXLXC = "{x:x}";
		const string fCNXLXC = "{#x:x}";
		
		int valueCount = split.Length;
		switch (format) {
		case fCXC:
		case fCNXC:{ // {x} {#x}
			if (valueCount == 1) {
				float value;
				if (!float.TryParse(split[0], out value)) {
					CreateResult(ReadResult.BEAT_PARSE_ERROR);
					return string.Empty;
				}
				if (format == fCXC) {
					_interval = false;
					_value = value;
					return newLines + XMsqScript.Document.BEAT + "(" + value + ");";
				}
				else {
					_interval = true;
					_value = value;
					return newLines + XMsqScript.Document.INTERVAL + "(" + value + ");";
				}
			}
			break;
		}
		case fCXLXC: 
		case fCNXLXC: { // {x:x} {#x:x}
			if (valueCount == 2) {
				float value;
				int bar;
				if (!float.TryParse(split[0], out value)) {
					CreateResult(ReadResult.BEAT_PARSE_ERROR);
					return string.Empty;
				}
				if (!int.TryParse(split[1], out bar)) {
					CreateResult(ReadResult.BAR_PARSE_ERROR);
					return string.Empty;
				}
				if (format == fCXLXC) {
					_interval = false;
					_value = value;
					return newLines + XMsqScript.Document.BEAT + "(" + value.ToString() + "," + bar.ToString() + ");";
				}
				else {
					_interval = true;
					_value = value;
					return newLines + XMsqScript.Document.INTERVAL + "(" + value.ToString() + "," + bar.ToString() + ");";
				}
			}
			break;
		}
		}
		CreateResult(ReadResult.BEAT_SYNTAX_ERROR);
		return string.Empty;
	}

	/// <summary>
	/// <para>ステップの読み込み</para>
	/// <para>return.length==1なら[0]==step</para>
	/// <para>return.length==2なら[0]==wait, [1]==step</para>
	/// </summary>
	public string[] ReadStep() {
		const string x = "x";
		const string open = Document.SQUARE_OPEN;
		const string close = Document.SQUARE_CLOSE;
		string format;
		string newLines;
		var split = StoreReplace(open, close, 
		                         new string[] { open, close, Document.ROUND_OPEN, Document.ROUND_CLOSE, Document.NUMBER, Document.COLON, Document.SLASH },
		x, null, true, out format, out newLines);
		
		
		/*
			 * formatが以下にひっかかれば、stepを作れる.
			 * wait_default
			 *  step_basic		[x:x]
			 *  step_interval	[x]				[#x]
			 *  step_localbpm	[(x)x:x]
			 * 
			 * wait_bpm
			 *  step_basic		[(x)/x:x]		[x#x:x]
			 *  step_interval	[(x)/x]			[(x)/#x]	[x#x]
			 *  step_localbpm	[(x)/(x)x:x] 	[x#(x)x:x]
			 * 
			 * wait_interval
			 *  step_basic		[x/x:x]			[#x/x:x]	[x##x:x]
			 *  step_interval	[x/x]			[#x/x]		[x/#x]		[#x/#x]	[x##x]
			 *  step_localbpm	[x/(x)x:x]		[#x/(x)x:x]	[x##(x)x:x]
			 * 
			 * wait_basic
			 *  step_basic		[x:x/x:x]
			 *  step_interval	[x:x/x]			[x:x/#x]
			 *  step_localbpm	[x:x/(x)x:x]
			 * 
			 * wait_localbpm
			 *  step_basic		[(x)x:x/x:x]
			 *  step_interval	[(x)x:x/x]		[(x)x:x/#x]
			 *  step_localbpm	[(x)x:x/(x)x:x]
			 */
		
		const string stepBasic0 = "[x:x]";
		const string stepInterval0 = "[x]";
		const string stepInterval1 = "[#x]";
		const string stepLocalBpm0 = "[(x)x:x]";
		
		const string waitBpmStepBasic0 = "[(x)/x:x]";
		const string waitBpmStepBasic1 = "[x#x:x]";
		const string waitBpmStepInterval0 = "[(x)/x]";
		const string waitBpmStepInterval1 = "[(x)/#x]";
		const string waitBpmStepInterval2 = "[x#x]";
		const string waitBpmStepLocalBpm0 = "[(x)/(x)x:x]";
		const string waitBpmStepLocalBpm1 = "[x#(x)x:x]";
		
		const string waitIntervalStepBasic0 = "[x/x:x]";
		const string waitIntervalStepBasic1 = "[#x/x:x]";
		const string waitIntervalStepBasic2 = "[x##x:x]";
		const string waitIntervalStepInterval0 = "[x/x]";
		const string waitIntervalStepInterval1 = "[#x/x]";
		const string waitIntervalStepInterval2 = "[x/#x]";
		const string waitIntervalStepInterval3 = "[#x/#x]";
		const string waitIntervalStepInterval4 = "[x##x]";
		const string waitIntervalStepLocalBpm0 = "[x/(x)x:x]";
		const string waitIntervalStepLocalBpm1 = "[#x/(x)x:x]";
		const string waitIntervalStepLocalBpm2 = "[x##(x)x:x]";
		
		const string waitBasicStepBasic0 = "[x:x/x:x]";
		const string waitBasicStepInterval0 = "[x:x/x]";
		const string waitBasicStepInterval1 = "[x:x/#x]";
		const string waitBasicStepLocalBpm0 = "[x:x/(x)x:x]";
		
		const string waitLocalBpmStepBasic0 = "[(x)x:x/x:x]";
		const string waitLocalBpmStepInterval0 = "[(x)x:x/x]";
		const string waitLocalBpmStepInterval1 = "[(x)x:x/#x]";
		const string waitLocalBpmStepLocalBpm0 = "[(x)x:x/(x)x:x]";
		
		System.Func<string, float[], string> createMsq = (title, values) => {
			var builder = new StringBuilder();
			builder.Append(newLines);
			newLines = string.Empty;
			builder.Append(title);
			builder.Append("(");
			for (int i = 0; i < values.Length; i++) {
				builder.Append(values[i].ToString());
				if (i < values.Length - 1) { 
					builder.Append(",");
				}
			}
			builder.Append(")");
			return builder.ToString();
		};

		System.Func<float[], string> createStepMsq = (values) => {
			return createMsq(XMsqScript.Document.STEP, values);
		};

		int valueCount = split.Length;
		switch (format) {
		case stepBasic0: { // [x:x]
			if (valueCount == 2) {
				float beat, length;
				if (!float.TryParse(split[0], out beat)) {
					CreateResult(ReadResult.STEP_BEAT_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[1], out length)) {
					CreateResult(ReadResult.STEP_LENGTH_PARSE_ERROR);
					return new string[0];
				}
				return new string[] { createStepMsq(new float[] { beat, length }) };
			}
			break;
		}
		case stepInterval0:
		case stepInterval1: { // [x] [#x]
			if (valueCount == 1) {
				float interval;
				if (!float.TryParse(split[0], out interval)) {
					CreateResult(ReadResult.STEP_INTERVAL_PARSE_ERROR);
					return new string[0];
				}
				return new string[] { createStepMsq(new float[] { interval }) };
			}
			break;
		}
		case stepLocalBpm0: { // [(x)x:x]
			if (valueCount == 3) {
				float bpm, beat, length;
				if (!float.TryParse(split[0], out bpm)) {
					CreateResult(ReadResult.STEP_BPM_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[1], out beat)) {
					CreateResult(ReadResult.STEP_BEAT_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[2], out length)) {
					CreateResult(ReadResult.STEP_LENGTH_PARSE_ERROR);
					return new string[0];
				}
				return new string[] { createStepMsq(new float[] { bpm, beat, length }) };
			}
			break;
		}
		case waitBpmStepBasic0:
		case waitBpmStepBasic1: { // [(x)/x:x] [x#x:x]
			if (valueCount == 3) {
				float wbpm, beat, length;
				if (!float.TryParse(split[0], out wbpm)) {
					CreateResult(ReadResult.WAIT_BPM_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[1], out beat)) {
					CreateResult(ReadResult.STEP_BEAT_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[2], out length)) {
					CreateResult(ReadResult.STEP_LENGTH_PARSE_ERROR);
					return new string[0];
				}
				return new string[] { 
					createMsq(XMsqScript.Document.WAIT_CUSTOM_LOCAL_BPM, new float[] { wbpm }),
					createStepMsq(new float[] { beat, length })
				};
			}
			break;
		}
		case waitBpmStepInterval0:
		case waitBpmStepInterval1: 
		case waitBpmStepInterval2: { // [(x)/x] [(x)/#x] [x#x]
			if (valueCount == 2) {
				float wbpm, interval;
				if (!float.TryParse(split[0], out wbpm)) {
					CreateResult(ReadResult.WAIT_BPM_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[1], out interval)) {
					CreateResult(ReadResult.STEP_INTERVAL_PARSE_ERROR);
					return new string[0];
				}
				return new string[] {
					createMsq(XMsqScript.Document.WAIT_CUSTOM_LOCAL_BPM, new float[] { wbpm }),
					createStepMsq(new float[] { interval })
				};
			}
			break;
		}
		case waitBpmStepLocalBpm0:
		case waitBpmStepLocalBpm1: { // [(x)/(x)x:x] [x#(x)x:x]
			if (valueCount == 4) {
				float wbpm, bpm, beat, length;
				if (!float.TryParse(split[0], out wbpm)) {
					CreateResult(ReadResult.WAIT_BPM_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[1], out bpm)) {
					CreateResult(ReadResult.STEP_BPM_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[2], out beat)) {
					CreateResult(ReadResult.STEP_BEAT_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[3], out length)) {
					CreateResult(ReadResult.STEP_LENGTH_PARSE_ERROR);
					return new string[0];
				}
				return new string[] {
					createMsq(XMsqScript.Document.WAIT_CUSTOM_LOCAL_BPM, new float[] { wbpm }),
					createStepMsq(new float[] { bpm, beat, length })
				};
			}
			break;
		}
		case waitIntervalStepBasic0:
		case waitIntervalStepBasic1:
		case waitIntervalStepBasic2: { // [x/x:x] [#x/x:x] [x##x:x]
			if (valueCount == 3) {
				float winterval, beat, length;
				if (!float.TryParse(split[0], out winterval)) {
					CreateResult(ReadResult.WAIT_INTERVAL_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[1], out beat)) {
					CreateResult(ReadResult.STEP_BEAT_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[2], out length)) {
					CreateResult(ReadResult.STEP_LENGTH_PARSE_ERROR);
					return new string[0];
				}
				return new string[] {
					createStepMsq(new float[] { winterval }),
					createStepMsq(new float[] { beat, length })
				};
			}
			break;
		}
		case waitIntervalStepInterval0:
		case waitIntervalStepInterval1: 
		case waitIntervalStepInterval2: 
		case waitIntervalStepInterval3: 
		case waitIntervalStepInterval4: { // [x/x] [#x/x] [x/#x] [#x/#x] [x##x]
			if (valueCount == 2) {
				float winterval, interval;
				if (!float.TryParse(split[0], out winterval)) {
					CreateResult(ReadResult.WAIT_INTERVAL_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[1], out interval)) {
					CreateResult(ReadResult.STEP_INTERVAL_PARSE_ERROR);
					return new string[0];
				}
				return new string[] { 
					createStepMsq(new float[] { winterval }),
					createStepMsq(new float[] { interval })
				};
			}
			break;
		}
		case waitIntervalStepLocalBpm0:
		case waitIntervalStepLocalBpm1: 
		case waitIntervalStepLocalBpm2: { // [x/(x)x:x] [#x/(x)x:x] [x##(x)x:x]
			if (valueCount == 4) {
				float winterval, bpm, beat, length;
				if (!float.TryParse(split[0], out winterval)) {
					CreateResult(ReadResult.WAIT_INTERVAL_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[1], out bpm)) {
					CreateResult(ReadResult.STEP_BPM_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[2], out beat)) {
					CreateResult(ReadResult.STEP_BEAT_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[3], out length)) {
					CreateResult(ReadResult.STEP_LENGTH_PARSE_ERROR);
					return new string[0];
				}
				return new string[] { 
					createStepMsq(new float[] { winterval }),
					createStepMsq(new float[] { bpm, beat, length })
				};
			}
			break;
		}
		case waitBasicStepBasic0: { // [x:x/x:x]
			if (valueCount == 4) {
				float wbeat, wlength, beat, length;
				if (!float.TryParse(split[0], out wbeat)) {
					CreateResult(ReadResult.WAIT_BEAT_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[1], out wlength)) {
					CreateResult(ReadResult.WAIT_LENGTH_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[2], out beat)) {
					CreateResult(ReadResult.STEP_BEAT_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[3], out length)) {
					CreateResult(ReadResult.STEP_LENGTH_PARSE_ERROR);
					return new string[0];
				}
				return new string[] { 
					createStepMsq(new float[] { wbeat, wlength }),
					createStepMsq(new float[] { beat, length })
				};
			}
			break;
		}
		case waitBasicStepInterval0:
		case waitBasicStepInterval1: { // [x:x/x] [x:x/#x]
			if (valueCount == 3) {
				float wbeat, wlength, interval;
				if (!float.TryParse(split[0], out wbeat)) {
					CreateResult(ReadResult.WAIT_BEAT_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[1], out wlength)) {
					CreateResult(ReadResult.WAIT_LENGTH_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[2], out interval)) {
					CreateResult(ReadResult.STEP_LENGTH_PARSE_ERROR);
					return new string[0];
				}
				return new string[] { 
					createStepMsq(new float[] { wbeat, wlength }),
					createStepMsq(new float[] { interval })
				};
			}
			break;
		}
		case waitBasicStepLocalBpm0: { // [x:x/(x)x:x]
			if (valueCount == 5) {
				float wbeat, wlength, bpm, beat, length;
				if (!float.TryParse(split[0], out wbeat)) {
					CreateResult(ReadResult.WAIT_BEAT_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[1], out wlength)) {
					CreateResult(ReadResult.WAIT_LENGTH_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[2], out bpm)) {
					CreateResult(ReadResult.STEP_BPM_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[3], out beat)) {
					CreateResult(ReadResult.STEP_BEAT_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[4], out length)) {
					CreateResult(ReadResult.STEP_LENGTH_PARSE_ERROR);
					return new string[0];
				}
				return new string[] {
					createStepMsq(new float[] { wbeat, wlength }),
					createStepMsq(new float[] { bpm, beat, length })
				};
			}
			break;
		}
		case waitLocalBpmStepBasic0: { // [(x)x:x/x:x]
			if (valueCount == 5) {
				float wbpm, wbeat, wlength, beat, length;
				if (!float.TryParse(split[0], out wbpm)) {
					CreateResult(ReadResult.WAIT_BPM_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[1], out wbeat)) {
					CreateResult(ReadResult.WAIT_BEAT_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[2], out wlength)) {
					CreateResult(ReadResult.WAIT_LENGTH_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[3], out beat)) {
					CreateResult(ReadResult.STEP_BEAT_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[4], out length)) {
					CreateResult(ReadResult.STEP_LENGTH_PARSE_ERROR);
					return new string[0];
				}
				return new string[] { 
					createStepMsq(new float[] { wbpm, wbeat, wlength }),
					createStepMsq(new float[] { beat, length })
				};
			}
			break;
		}
		case waitLocalBpmStepInterval0:
		case waitLocalBpmStepInterval1: { // [(x)x:x/x] // [(x)x:x/#x]
			if (valueCount == 4) {
				float wbpm, wbeat, wlength, interval;
				if (!float.TryParse(split[0], out wbpm)) {
					CreateResult(ReadResult.WAIT_BPM_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[1], out wbeat)) {
					CreateResult(ReadResult.WAIT_BEAT_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[2], out wlength)) {
					CreateResult(ReadResult.WAIT_LENGTH_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[3], out interval)) {
					CreateResult(ReadResult.STEP_INTERVAL_PARSE_ERROR);
					return new string[0];
				}
				return new string[] { 
					createStepMsq(new float[] { wbpm, wbeat, wlength }),
					createStepMsq(new float[] { interval })
				};
			}
			break;
		}
		case waitLocalBpmStepLocalBpm0: { // [(x)x:x/(x)x:x]
			if (valueCount == 6) {
				float wbpm, wbeat, wlength, bpm, beat, length;
				if (!float.TryParse(split[0], out wbpm)) {
					CreateResult(ReadResult.WAIT_BPM_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[1], out wbeat)) {
					CreateResult(ReadResult.WAIT_BEAT_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[2], out wlength)) {
					CreateResult(ReadResult.WAIT_LENGTH_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[3], out bpm)) {
					CreateResult(ReadResult.STEP_BPM_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[4], out beat)) {
					CreateResult(ReadResult.STEP_BEAT_PARSE_ERROR);
					return new string[0];
				}
				if (!float.TryParse(split[5], out length)) {
					CreateResult(ReadResult.STEP_LENGTH_PARSE_ERROR);
					return new string[0];
				}
				return new string[] { 
					createStepMsq(new float[] { wbpm, wbeat, wlength }),
					createStepMsq(new float[] { bpm, beat, length })
				};
			}
			break;
		}
		}
		CreateResult(ReadResult.STEP_SYNTAX_ERROR);
		return new string[0];
	}

	private string ButtonNext(string button) {
		var keys = new string[] {
			Document.BUTTON_1, Document.BUTTON_2, Document.BUTTON_3, Document.BUTTON_4, Document.BUTTON_5, Document.BUTTON_6, Document.BUTTON_7, Document.BUTTON_8,
			Document.HOLD_KEY, Document.BREAK_KEY, Document.STAR_KEY,
			Document.SHAPE_STRAIGHT, Document.SHAPE_CURVE, Document.SHAPE_RIGHT, Document.SHAPE_LEFT, 
			Document.SHAPE_P, Document.SHAPE_Q, Document.SHAPE_S, Document.SHAPE_Z, Document.SHAPE_V,
			Document.SHAPE_PP, Document.SHAPE_QQ, Document.SHAPE_W, 
			Document.SHAPE_V1, Document.SHAPE_V2, Document.SHAPE_V3, Document.SHAPE_V4, Document.SHAPE_V5, Document.SHAPE_V6, Document.SHAPE_V7, Document.SHAPE_V8, 

			Document.CAMMA, Document.SLASH,
		};
		var msq = new StringBuilder();
		string newLines;
		string str = SearchKey(keys, out newLines);
		msq.Append(newLines);
		if (Document.IsButton(str)) {
			WriteTap(msq, button);
			msq.Append(",");
			index++;
			msq.Append(ButtonButtonNext(str));
		}
		else if (str == Document.HOLD_KEY) {
			index++;
			msq.Append(HoldKeyNext(button));
		}
		else if (str == Document.BREAK_KEY) {
			index++;
			msq.Append(BreakKeyNext(button, false));
		}
		else if (str == Document.STAR_KEY) {
			index++;
			msq.Append(StarKeyNext(button, false));
		}
		else if (Document.IsSlideShape(str)) {
			WriteSlideHead(msq, button, false);
			index++;
			msq.Append(SlideShapeKeyNext(button, str));
		}
		else if (str == Document.CAMMA || isLastScriptIndex) {
			WriteTap(msq, button);
		}
		else if (str == Document.SLASH) {
			WriteTap(msq, button);
			msq.Append(",");
			index++;
			msq.Append(SlashNext());
		}
		else {
			CreateResult(ReadResult.BUTTON_NEXT_SYNTAX_ERROR);
			return string.Empty;
		}
		return msq.ToString();
	}
	
	private string ButtonButtonNext(string button) {
		var keys = new string[] {
			Document.BUTTON_1, Document.BUTTON_2, Document.BUTTON_3, Document.BUTTON_4, Document.BUTTON_5, Document.BUTTON_6, Document.BUTTON_7, Document.BUTTON_8,
			Document.CAMMA, Document.SLASH,
		};
		var msq = new StringBuilder();
		string newLines;
		string str = SearchKey(keys, out newLines);
		msq.Append(newLines);
		if (Document.IsButton(str)) {
			WriteTap(msq, button);
			msq.Append(",");
			index++;
			msq.Append(ButtonButtonNext(str));
		}
		else if (str == Document.CAMMA || isLastScriptIndex) {
			WriteTap(msq, button);
		}
		else if (str == Document.SLASH) {
			WriteTap(msq, button);
			msq.Append(",");
			index++;
			msq.Append(SlashNext());
		}
		else {
			CreateResult(ReadResult.BUTTON_BUTTON_NEXT_SYNTAX_ERROR);
			return string.Empty;
		}
		return msq.ToString();
	}
	
	private string SlashNext() {
		var keys = new string[] {
			Document.BUTTON_1, Document.BUTTON_2, Document.BUTTON_3, Document.BUTTON_4, Document.BUTTON_5, Document.BUTTON_6, Document.BUTTON_7, Document.BUTTON_8,
		};
		var msq = new StringBuilder();
		string newLines;
		string str = SearchKey(keys, out newLines);
		msq.Append(newLines);
		if (Document.IsButton(str)) {
			index++;
			msq.Append(ButtonNext(str));
		}
		else {
			CreateResult(ReadResult.SLASH_NEXT_SYNTAX_ERROR);
			return string.Empty;
		}
		return msq.ToString();
	}

	private string HoldKeyNext(string button) {
		var keys = new string[] {
			Document.SQUARE_OPEN, 
		};
		var msq = new StringBuilder();
		string newLines;
		string str = SearchKey(keys, out newLines);
		msq.Append(newLines);
		if (str == Document.SQUARE_OPEN) {
			var step = ReadStep();
			if (step != null && step.Length == 1) {
				index++;
				msq.Append(HoldStepNext(button, step[0]));
			}
			else {
				CreateResult(ReadResult.HOLD_INVALIDATE_STEP_ERROR);
				return string.Empty;
			}
		}
		else {
			CreateResult(ReadResult.HOLD_KEY_NEXT_ERROR);
			return string.Empty;
		}
		return msq.ToString();
	}
	
	private string HoldStepNext(string button, string step) {
		var keys = new string[] {
			Document.CAMMA, Document.SLASH,
		};
		var msq = new StringBuilder();
		string newLines;
		string str = SearchKey(keys, out newLines);
		msq.Append(newLines);
		if (str != null || isLastScriptIndex) {
			msq.Append(XMsqScript.Document.HOLD);
			msq.Append("(");
			msq.Append(button);
			msq.Append(",");
			msq.Append(step);
			msq.Append(")");
			if (str == Document.SLASH) {
				msq.Append(",");
				index++;
				msq.Append(SlashNext());
			}
		}
		else {
			CreateResult(ReadResult.HOLD_STEP_NEXT_ERROR);
			return string.Empty;
		}
		return msq.ToString();
	}
	
	private string BreakKeyNext(string button, bool star) {
		var keys = new string[] {
			Document.SHAPE_STRAIGHT, Document.SHAPE_CURVE, Document.SHAPE_RIGHT, Document.SHAPE_LEFT, 
			Document.SHAPE_P, Document.SHAPE_Q, Document.SHAPE_S, Document.SHAPE_Z, Document.SHAPE_V,
			Document.SHAPE_PP, Document.SHAPE_QQ, Document.SHAPE_W, 
			Document.SHAPE_V1, Document.SHAPE_V2, Document.SHAPE_V3, Document.SHAPE_V4, Document.SHAPE_V5, Document.SHAPE_V6, Document.SHAPE_V7, Document.SHAPE_V8, 

			Document.CAMMA, Document.SLASH, Document.STAR_KEY, 
		};
		var msq = new StringBuilder();
		string newLines;
		string str = SearchKey(keys, out newLines);
		msq.Append(newLines);
		if (!star && Document.IsSlideShape(str)) {
			WriteSlideHead(msq, button, true);
			index++;
			msq.Append(SlideShapeKeyNext(button, str));
		}
		else if (str == Document.CAMMA || (str == null && isLastScriptIndex)) {
			WriteBreak(msq, button);
			if (star) {
				WriteStarOption(msq);
			}
		}
		else if (str == Document.SLASH) {
			WriteBreak(msq, button);
			if (star) {
				WriteStarOption(msq);
			}
			msq.Append(",");
			index++;
			msq.Append(SlashNext());
		}
		else if ((str == Document.STAR_KEY || (str == null && isLastScriptIndex)) && !star) {
			index++;
			msq.Append(StarKeyNext(button, true));
		}
		else {
			CreateResult(ReadResult.BREAK_KEY_NEXT_ERROR);
			return string.Empty;
		}
		return msq.ToString();
	}
	
	private string StarKeyNext(string button, bool isBreak) {
		var keys = new string[] {
			Document.CAMMA, Document.SLASH, Document.BREAK_KEY, 
		};
		var msq = new StringBuilder();
		string newLines;
		string str = SearchKey(keys, out newLines);
		msq.Append(newLines);
		if (str == Document.CAMMA || (str == null && isLastScriptIndex)) {
			if (isBreak) {
				WriteBreak(msq, button);
			}
			else {
				WriteTap(msq, button);
			}
			WriteStarOption(msq);
		}
		else if (str == Document.SLASH) {
			if (isBreak) {
				WriteBreak(msq, button);
			}
			else {
				WriteTap(msq, button);
			}
			WriteStarOption(msq);
			msq.Append(",");
			index++;
			msq.Append(SlashNext());
		}
		else if ((str == Document.BREAK_KEY || (str == null && isLastScriptIndex)) && !isBreak) {
			index++;
			msq.Append(BreakKeyNext(button, true));
		}
		else {
			CreateResult(ReadResult.STAR_KEY_NEXT_ERROR);
			return string.Empty;
		}
		return msq.ToString();
	}
	
	private string SlideShapeKeyNext(string start, string shape) {
		var keys = new string[] {
			Document.BUTTON_1, Document.BUTTON_2, Document.BUTTON_3, Document.BUTTON_4, Document.BUTTON_5, Document.BUTTON_6, Document.BUTTON_7, Document.BUTTON_8,
		};
		var msq = new StringBuilder();
		string newLines;
		string str = SearchKey(keys, out newLines);
		msq.Append(newLines);
		if (Document.IsButton(str)) {
			index++;
			msq.Append(SlideTargetNext(start, shape, str));
		}
		else {
			CreateResult(ReadResult.SLIDE_SHAPE_KEY_NEXT_ERROR);
			return string.Empty;
		}
		return msq.ToString();
	}
	
	private string SlideTargetNext(string start, string shape, string target) {
		var keys = new string[] {
			Document.SQUARE_OPEN, 
		};
		var msq = new StringBuilder();
		string newLines;
		string str = SearchKey(keys, out newLines);
		msq.Append(newLines);
		if (str != null) {
			var step = ReadStep();
			if (step != null) {
				if (step.Length == 1) {
					index++;
					msq.Append(SlideStepNext(start, shape, target, step[0]));
				}
				else {
					index++;
					msq.Append(SlideStepNext(start, shape, target, step[0] + "," + step[1]));
				}
			}
		}
		else {
			CreateResult(ReadResult.SLIDE_TARGET_NEXT_ERROR);
			return string.Empty;
		}
		return msq.ToString();
	}
	
	private string SlideStepNext(string start, string shape, string target, string step) {
		var keys = new string[] {
			Document.CAMMA, Document.SLASH, Document.SAMEHEAD_KEY,
		};
		var msq = new StringBuilder();
		string newLines;
		string str = SearchKey(keys, out newLines);
		msq.Append(newLines);
		if (str != null || isLastScriptIndex) {
			msq.Append(slideShapeReplacer[shape]);
			msq.Append("(");
			msq.Append(step);
			msq.Append(",");
			msq.Append(start);
			msq.Append(",");
			msq.Append(target);
			msq.Append(")");
			if (str == Document.CAMMA || isLastScriptIndex) {
				msq.Append(")");
			}
			else if (str == Document.SLASH) {
				msq.Append("), ");
				index++;
				msq.Append(SlashNext());
			}
			else if (str == Document.SAMEHEAD_KEY) {
				msq.Append(",");
				index++;
				msq.Append(SameHeadKeyNext(start));
			}
		}
		else {
			CreateResult(ReadResult.SLIDE_STEP_NEXT_ERROR);
			return string.Empty;
		}
		return msq.ToString();
	}
	
	private string SameHeadKeyNext(string start) {
		var keys = new string[] {
			Document.SHAPE_STRAIGHT, Document.SHAPE_CURVE, Document.SHAPE_RIGHT, Document.SHAPE_LEFT, 
			Document.SHAPE_P, Document.SHAPE_Q, Document.SHAPE_S, Document.SHAPE_Z, Document.SHAPE_V,
			Document.SHAPE_PP, Document.SHAPE_QQ, Document.SHAPE_W, 
			Document.SHAPE_V1, Document.SHAPE_V2, Document.SHAPE_V3, Document.SHAPE_V4, Document.SHAPE_V5, Document.SHAPE_V6, Document.SHAPE_V7, Document.SHAPE_V8, 
		};
		var msq = new StringBuilder();
		string newLines;
		string str = SearchKey(keys, out newLines);
		msq.Append(newLines);
		if (str != null) {
			index++;
			msq.Append(SlideShapeKeyNext(start, str));
		}
		else {
			CreateResult(ReadResult.SAME_HEAD_KEY_NEXT_ERROR);
			return string.Empty;
		}
		return msq.ToString();
	}
	
	private string NumberSignNext() {
		var keys = new string[] {
			Document.BUTTON_1, Document.BUTTON_2, Document.BUTTON_3, Document.BUTTON_4, Document.BUTTON_5, Document.BUTTON_6, Document.BUTTON_7, Document.BUTTON_8,
			Document.SENSOR_A1, Document.SENSOR_A2, Document.SENSOR_A3, Document.SENSOR_A4, Document.SENSOR_A5, Document.SENSOR_A6, Document.SENSOR_A7, Document.SENSOR_A8,
			Document.SENSOR_B1, Document.SENSOR_B2, Document.SENSOR_B3, Document.SENSOR_B4, Document.SENSOR_B5, Document.SENSOR_B6, Document.SENSOR_B7, Document.SENSOR_B8,
			Document.SENSOR_C, 
		};
		var msq = new StringBuilder();
		var chain = new StringBuilder();
		string newLines;
		string str = SearchKey(keys, out newLines);
		msq.Append(newLines);
		if (Document.IsButton(str)) {
			msq.Append(XMsqScript.Document.NOTE);
			msq.Append("(");
			WriteSlideHead(msq, str, false);
			string sensor = Document.SENSOR_A + str;
			index++;
			msq.Append(DirectSlideSensorNext(chain, sensor, Document.SENSOR_A, sensor));
			msq.Append(");");
		}
		else if (Document.IsSensor(str)) {
			msq.Append(XMsqScript.Document.NOTE);
			msq.Append("(");
			msq.Append(XMsqScript.Document.SLIDE);
			msq.Append("(");
			index++;
			msq.Append(DirectSlideSensorNext(chain, str, Document.GetSensorABC(str), str));
			msq.Append(");");
		}
		else if (str == null && index < textSize && analyzedText[index].type == CommonScriptFormatter.TextType.TEXT_RECOGNIZED_KEY) {
			index++;
			// msq直書き.
			for (; index < textSize; index++) {
				var textEntity = analyzedText[index];
				str = textEntity.GetString();
				if (textEntity.type == CommonScriptFormatter.TextType.TEXT) {
					msq.Append(str);
				}
				else if (textEntity.type == CommonScriptFormatter.TextType.SCRIPT) {
					if (str == Document.CAMMA || isLastScriptIndex) {
						// #<" hoge ">,
						// のように、文字列が終わったら速やかにカンマで区切らせる.
						break;
					}
					else {
						CreateResult(ReadResult.DIRECT_MSQ_INPUT_ERROR);
						return string.Empty;
					}
				}
			}
		}
		else {
			CreateResult(ReadResult.NUMBERSIGN_NEXT_ERROR);
			return string.Empty;
		}
		return msq.ToString();
	}

	private string DirectSlideSensorNext(StringBuilder chain, string head, string sensorABC, string sensor) {
		var keys = new string[] {
			Document.SHAPE_STRAIGHT, Document.SHAPE_CURVE, Document.SHAPE_RIGHT, Document.SHAPE_LEFT, 
		};
		var msq = new StringBuilder();
		string newLines;
		string str = SearchKey(keys, out newLines);
		msq.Append(newLines);
		if (str != null) {
			index++;
			msq.Append(DirectSlideShapeNext(chain, head, str, sensorABC, sensor));
		}
		else {
			CreateResult(ReadResult.DIRECT_SLIDE_SENSOR_NEXT_ERROR);
			return string.Empty;
		}
		return msq.ToString();
	}
	
	private string DirectSlideShapeNext(StringBuilder chain, string head, string shape, string sensorABC, string sensor) {
		var keys = new string[] {
			Document.BUTTON_1, Document.BUTTON_2, Document.BUTTON_3, Document.BUTTON_4, Document.BUTTON_5, Document.BUTTON_6, Document.BUTTON_7, Document.BUTTON_8,
			Document.SENSOR_A1, Document.SENSOR_A2, Document.SENSOR_A3, Document.SENSOR_A4, Document.SENSOR_A5, Document.SENSOR_A6, Document.SENSOR_A7, Document.SENSOR_A8,
			Document.SENSOR_B1, Document.SENSOR_B2, Document.SENSOR_B3, Document.SENSOR_B4, Document.SENSOR_B5, Document.SENSOR_B6, Document.SENSOR_B7, Document.SENSOR_B8,
			Document.SENSOR_C, 
		};
		var msq = new StringBuilder();
		string newLines;
		string str = SearchKey(keys, out newLines);
		msq.Append(newLines);
		if (Document.IsButton(str)) {
			string beforeABC = Document.GetSensorABC(sensor);
			// Cだったら、それより前のセンサーABを使用する.
			if (beforeABC == Document.SENSOR_C) {
				beforeABC = sensorABC;
			}
			// Cじゃなかったら、このセンサーABをテンプレートとする
			else {
				sensorABC = beforeABC;
			}
			// テンプレも今回もCだったら、暗黙的にAとする.
			if (beforeABC == Document.SENSOR_C) {
				beforeABC = sensorABC = Document.SENSOR_A;
			}
			string target = beforeABC + str;
			chain.Append(directSlideShapeReplacer[shape]);
			chain.Append("(");
			chain.Append(sensor);
			chain.Append(",");
			chain.Append(target);
			chain.Append(")");
			index++;
			msq.Append(DirectSlideTargetSensorNext(chain, head, sensorABC, target));
		}
		else if (Document.IsSensor(str)) {
			string beforeABC = Document.GetSensorABC(str);
			// Cじゃなかったら、このセンサーABをテンプレートとする
			if (beforeABC != Document.SENSOR_C) {
				sensorABC = beforeABC;
			}
			chain.Append(directSlideShapeReplacer[shape]);
			chain.Append("(");
			chain.Append(sensor);
			chain.Append(",");
			chain.Append(str);
			chain.Append(")");
			index++;
			msq.Append(DirectSlideTargetSensorNext(chain, head, sensorABC, str));
		}
		else {
			CreateResult(ReadResult.DIRECT_SLIDE_SHAPE_NEXT_ERROR);
			return string.Empty;
		}
		return msq.ToString();
	}
	
	private string DirectSlideTargetSensorNext(StringBuilder chain, string head, string sensorABC, string sensor) {
		var keys = new string[] {
			Document.SHAPE_STRAIGHT, Document.SHAPE_CURVE, Document.SHAPE_RIGHT, Document.SHAPE_LEFT, 
			Document.SQUARE_OPEN,
		};
		var msq = new StringBuilder();
		string newLines;
		string str = SearchKey(keys, out newLines);
		msq.Append(newLines);
		if (Document.IsSlideShape(str)) {
			chain.Append(",");
			index++;
			msq.Append(DirectSlideShapeNext(chain, head, str, sensorABC, sensor));
		}
		else if (str == Document.SQUARE_OPEN) {
			var step = ReadStep();
			if (step != null) {
				index++;
				if (step.Length == 1) {
					msq.Append(DirectSlideStepNext(chain, head, step[0]));
				}
				else {
					msq.Append(DirectSlideStepNext(chain, head, step[0] + "," + step[1]));
				}
			}
		}
		else {
			CreateResult(ReadResult.DIRECT_SLIDE_TARGET_NEXT_ERROR);
			return string.Empty;
		}
		return msq.ToString();
	}
	
	private string DirectSlideStepNext(StringBuilder chain, string head, string step) {
		var keys = new string[] {
			Document.CAMMA, Document.SLASH, Document.SAMEHEAD_KEY,
		};
		var msq = new StringBuilder();
		string newLines;
		string str = SearchKey(keys, out newLines);
		msq.Append(newLines);
		if (str != null || isLastScriptIndex) {
			msq.Append(XMsqScript.Document.CHAIN);
			msq.Append("(");
			msq.Append(step);
			msq.Append(",");
			msq.Append(chain.ToString());
			msq.Append(")"); //chainの閉じ.
			if (str == Document.CAMMA || isLastScriptIndex) {
				msq.Append(")");
			}
			else if (str == Document.SLASH) {
				msq.Append("), ");
				index++;
				msq.Append(SlashNext());
			}
			else if (str == Document.SAMEHEAD_KEY) {
				msq.Append(",");
				index++;
				msq.Append(DirectSlideSameHeadKeyNext(head));
			}
		}
		else {
			CreateResult(ReadResult.DIRECT_SLIDE_STEP_NEXT_ERROR);
			return string.Empty;
		}
		return msq.ToString();
	}
	
	private string DirectSlideSameHeadKeyNext(string head) {
		var keys = new string[] {
			Document.SHAPE_STRAIGHT, Document.SHAPE_CURVE, Document.SHAPE_RIGHT, Document.SHAPE_LEFT, 
		};
		var msq = new StringBuilder();
		var chain = new StringBuilder();
		string newLines;
		string str = SearchKey(keys, out newLines);
		msq.Append(newLines);
		if (str != null) {
			index++;
			msq.Append(DirectSlideShapeNext(chain, head, str, Document.GetSensorABC(head), head));
		}
		else {
			CreateResult(ReadResult.DIRECT_SLIDE_SAME_HEAD_KEY_NEXT_ERROR);
			return string.Empty;
		}
		return msq.ToString();
	}

}
