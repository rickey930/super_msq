﻿using System.Text;
using System.Collections.Generic;
using System.Linq;

public class SimaiReader {
	protected CommonScriptFormatter[] analyzedText;
	protected int index;
	protected int textSize;
	protected int lastScriptIndex;
	protected bool isLastScriptIndex { get { return index >= lastScriptIndex; } }
	
	/// <summary>
	/// <para>open(含む)からclose(含む)まで、keysを区切りに、keys以外の文字をreplacedText1文字で置き換える.</para>
	/// <para>置換した結果はformatに入り、このメソッドは分割した実態を返す</para>
	/// </summary>
	protected string[] StoreReplace(string open, string close, string[] keys, string replacedText, string[] enabledIgnoreTexts, bool requiredCloseKey, out string format) {
		string newLines;
		return StoreReplace(open, close, keys, replacedText, enabledIgnoreTexts, requiredCloseKey, out format, out newLines);
	}
	/// <summary>
	/// <para>open(含む)からclose(含む)まで、keysを区切りに、keys以外の文字をreplacedText1文字で置き換える.</para>
	/// <para>置換した結果はformatに入り、このメソッドは分割した実態を返す</para>
	/// </summary>
	protected string[] StoreReplace(string open, string close, string[] keys, string replacedText, string[] enabledIgnoreTexts, bool requiredCloseKey, out string format, out string newLines) {
		newLines = string.Empty;
		if (enabledIgnoreTexts == null) enabledIgnoreTexts = new string[0];
		var formatBuilder = new StringBuilder();
		bool readOpened = false;
		bool readClosed = false;
		bool ignoreOnly = true;
		bool textX = false;
		var entity = new StringBuilder();
		var entities = new List<StringBuilder>();
		entities.Add(entity);
		int newLine = 0;
		for (; index < textSize; index++) {
			var textEntity = analyzedText[index];
			if (textEntity.type == CommonScriptFormatter.TextType.SCRIPT ||
			    textEntity.type == CommonScriptFormatter.TextType.TEXT_RECOGNIZED_KEY ||
			    textEntity.type == CommonScriptFormatter.TextType.TEXT ||
			    textEntity.type == CommonScriptFormatter.TextType.IGNORE && enabledIgnoreTexts.Contains(textEntity.GetString())) {
				string str = textEntity.GetString();
				if (str == open) {
					readOpened = true;
				}
				if (readOpened) {
					bool censored = false;
					foreach (var check in keys) {
						if (str == check) {
							formatBuilder.Append(str);
							censored = true;
							textX = false;
							if (entity.Length > 0) {
								if (ignoreOnly) {
									entity.Remove(0, entity.Length);
								}
								entity = new StringBuilder();
								ignoreOnly = true;
								entities.Add(entity);
							}
							break;
						}
					}
					if (!censored) {
						if (ignoreOnly && textEntity.type != CommonScriptFormatter.TextType.IGNORE) {
							ignoreOnly = false;
						}
						entity.Append(str);
						if (!textX) {
							formatBuilder.Append(replacedText);
							textX = true;
						}
					}
					if (str == close) {
						readClosed = true;
						break;
					}
				}
			}
			if (textEntity.text[0].entity == "\n") {
				newLine++;
			}
		}
		format = formatBuilder.ToString();
		var ret = new string[(readClosed || requiredCloseKey) ? entities.Count - 1 : entities.Count];
		for (int i = 0; i < ret.Length; i++) {
			ret[i] = entities[i].ToString();
		}
		for (int i = 0; i < newLine; i++) {
			newLines += "\n";
		}
		return ret;
	}
	
	/// <summary>
	/// <para>今から読み込むところに、指定した文字列が存在するなら、その文字を返す</para>
	/// <para>無ければnull</para>
	/// </summary>
	protected string SearchKey(string[] keys) {
		string newLines;
		return SearchKey(keys, out newLines);
	}
	
	/// <summary>
	/// <para>今から読み込むところに、指定した文字列が存在するなら、その文字を返す</para>
	/// <para>無ければnull</para>
	/// </summary>
	protected string SearchKey(string[] keys, out string newLines) {
		newLines = string.Empty;
		string ret = null;
		int retlen = 0;
		int plusIndex = 0;
		int plusNewLine = 0;
		foreach (var key in keys) {
			int keylen = key.Length;
			int newLine = 0;
			if (retlen < keylen) {
				var builder = new StringBuilder();
				for (int i = 0; i < keylen; i++) {
					if (i + index < textSize) {
						var entity = analyzedText[index + i];
						if (entity.type == CommonScriptFormatter.TextType.SCRIPT ||
						    entity.type == CommonScriptFormatter.TextType.TEXT_RECOGNIZED_KEY ||
						    entity.type == CommonScriptFormatter.TextType.TEXT) {
							builder.Append(entity.GetString());
						}
						else {
							keylen++;
						}
						if (entity.text[0].entity == "\n") {
							newLine++;
						}
					}
				}
				if (builder.ToString() == key) {
					ret = key;
					plusNewLine = newLine;
					plusIndex = keylen - 1;
					retlen = ret.Length;
				}
			}
		}
		index += plusIndex;
		for (int i = 0; i < plusNewLine; i++) {
			newLines += "\n";
		}
		return ret;
	}
}
