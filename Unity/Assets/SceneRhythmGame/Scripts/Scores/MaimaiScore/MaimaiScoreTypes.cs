﻿using UnityEngine;
using System.Collections.Generic;

namespace XMaimaiScore {
	// XMaimaiNote.XDocumentを作るための譜面用の定義.
	// 譜面なのでBPMや拍の要素はあっていいが、例えばタップが丸か星かはもうこのクラスで定められるようにする.

	#region Button
	[System.Serializable]
	public class Button {
		public int entity;
		public Button() { }
		public Button(int data) {
			int b = data - 1;
			if (b < 0)
				b = 0;
			if (b > 7)
				b = 7;
			entity = b + 1;
		}
		public static Button Create(int id) { return new Button (id); }
		public int GetId() { return entity; }
		public int GetIndex() { return entity - 1; }
		public bool IsTopSide() { return entity < 3 || entity> 6; }
		public bool IsBottomSide() { return entity > 2 && entity < 7; }
		public bool IsLeftSide() { return entity < 5; }
		public bool IsRightSide() { return entity > 4; }
		public int CalcTurn(int amount, int? buttonId = null) {
			if (!buttonId.HasValue) {
				buttonId = GetId();
			}
			if (amount < 0) {
				int div = -amount / 8;
				int mod = -amount % 8;
				amount = 8 * (div + 1) - mod;
			}
			return (buttonId.Value - 1 + amount) % 8 + 1;
		}
		public int CalcMirrorHolizontal(int? buttonId = null) {
			if (!buttonId.HasValue) {
				buttonId = GetId();
			}
			return 9 - buttonId.Value;
		}
		public int CalcMirrorVertical(int? buttonId = null) {
			if (!buttonId.HasValue) {
				buttonId = GetId();
			}
			return CalcMirrorHolizontal(CalcTurn(buttonId.Value, 4));
		}
		
		public Button Turn(int amount) {
			entity = CalcTurn(amount);
			return this;
		}
		public Button MirrorHolizontal() {
			entity = CalcMirrorHolizontal();
			return this;
		}
		public Button MirrorVertical() {
			entity = CalcMirrorVertical();
			return this;
		}
		public Button Clone() {
			return new Button (entity);
		}
	}
	#endregion
	#region Step
	[System.Serializable]
	public abstract class Step {
		public enum Type {
			BASIC, INTERVAL, LOCAL_BPM, MASS, 
		}
		public abstract new Type GetType ();
		public abstract Step Clone();

		[System.Serializable]
		public class Basic : Step {
			public override Type GetType () { return Type.BASIC; }
			public Basic() { }
			public Basic(float beat, float length) { this.beat = beat; this.length = length; }
			public float beat;
			public float length;
			public override Step Clone() {
				return new Basic (beat, length);
			}
		}
		
		[System.Serializable]
		public class Interval : Step {
			public override Type GetType () { return Type.INTERVAL; }
			public Interval() { }
			public Interval(float data) { interval = data; }
			public float interval;
			public override Step Clone() {
				return new Interval (interval);
			}
		}
		
		[System.Serializable]
		public class LocalBpm : Basic {
			public override Type GetType () { return Type.LOCAL_BPM; }
			public LocalBpm() { }
			public LocalBpm(float bpm, float beat, float length) : base (beat, length) { this.bpm = bpm; }
			public float bpm;
			public override Step Clone() {
				return new LocalBpm (bpm, beat, length);
			}
		}
		
		[System.Serializable]
		public class Mass : Step {
			public override Type GetType () { return Type.MASS; }
			public Mass() { }
			public Mass(Step[] steps) { this.steps = steps; }
			public Step[] steps;
			public override Step Clone() {
				return new Mass (steps);
			}
		}

		public static Step CreateBasic(float beat, float length) { return new Basic (beat, length); }
		public static Step CreateInterval(float interval) { return new Interval (interval); }
		public static Step CreateLocalBpm(float bpm, float beat, float length) { return new LocalBpm (bpm, beat, length); }
		public static Step CreateSlideWaitDefault() { return new Basic (4, 1); }
		public static Step CreateSlideWaitLocalBpm(float bpm) { return new LocalBpm (bpm, 4, 1); }
		public static Step CreateMass(Step[] steps) { return new Mass (steps); }
	}
	#endregion
	#region Note
	[System.Serializable]
	public abstract class Note {
		public interface IEachArch {
			
		}
		public interface ISlideHead : IEachArch, IGuideSpeedCustomizable, IGuideScaleCustomizable, IRingShapeCustomizable, IButtonTurnable, IMirrorable {
			Note.Type GetType ();
		}
		public interface ISecretable {
		}
		public interface IGuideColorCustomizable {
		}
		public interface IGuideSpeedCustomizable {
		}
		public interface IGuideScaleCustomizable {
		}
		public interface IRingShapeCustomizable {
		}
		public interface IButtonTurnable {
			void Turn(int amount);
		}
		public interface IFreeTurnable {
			void Turn(float degree);
		}
		public interface IMirrorable {
			void MirrorHolizontal();
			void MirrorVertical();
		}

		public Note() {
		}

		public enum Type {
			TAP, HOLD, SLIDE, BREAK, MESSAGE, SCROLL_MESSAGE, SOUND_MESSAGE, TRAP,
		}
		public abstract new Type GetType ();

		public abstract Note Clone ();

		[System.Serializable]
		public class Break : Note, ISecretable, ISlideHead, IButtonTurnable, IMirrorable {
			public override Type GetType () { return Type.BREAK; }
			public Break() : base() { }
			public Break(int buttonId) : base() { 
				this.button = new Button(buttonId);
				this.time = null;
				this.shape_type = null;
				this.fade_speed = null;
				this.move_speed = null;
				this.guide_scale = null;
				this.fade_speed_rate = null;
				this.move_speed_rate = null;
				this.guide_scale_rate = null;
				this.secret = null;
			}
			public float? time;
			public Button button;
			public int? shape_type;
			public float? fade_speed;
			public float? move_speed;
			public float? guide_scale;
			public float? fade_speed_rate;
			public float? move_speed_rate;
			public float? guide_scale_rate;
			public bool? secret;

			public void Turn(int amount) {
				button.Turn(amount);
			}
			public void MirrorHolizontal() {
				button.MirrorHolizontal();
			}
			public void MirrorVertical() {
				button.MirrorVertical();
			}
			public override Note Clone () {
				var ret = new Break (button.entity);
				ret.secret = secret;
				ret.time = time;
				ret.shape_type = shape_type;
				ret.fade_speed = fade_speed;
				ret.move_speed = move_speed;
				ret.guide_scale = guide_scale;
				ret.fade_speed_rate = fade_speed_rate;
				ret.move_speed_rate = move_speed_rate;
				ret.guide_scale_rate = guide_scale_rate;
				return ret;
			}
		}

		[System.Serializable]
		public class Tap : Break, IGuideColorCustomizable {
			public override Type GetType () { return Type.TAP; }
			public Tap() { }
			public Tap(int buttonId) : base (buttonId) {
				this.guide_color = null;
			}
			public int? guide_color;
			public override Note Clone () {
				var ret = new Tap (button.entity);
				ret.secret = secret;
				ret.time = time;
				ret.shape_type = shape_type;
				ret.fade_speed = fade_speed;
				ret.move_speed = move_speed;
				ret.guide_scale = guide_scale;
				ret.fade_speed_rate = fade_speed_rate;
				ret.move_speed_rate = move_speed_rate;
				ret.guide_scale_rate = guide_scale_rate;
				ret.guide_color = guide_color;
				return ret;
			}
		}
		
		[System.Serializable]
		public class Hold : Note, IButtonTurnable, IMirrorable, ISecretable, IEachArch, IGuideSpeedCustomizable, IGuideScaleCustomizable, IGuideColorCustomizable {
			public override Type GetType () { return Type.HOLD; }
			public Hold() : base() { }
			public Hold(int buttonId, Step step) : base() {
				this.button = new Button(buttonId);
				this.step = step;
				this.guide_color = null;
				this.fade_speed = null;
				this.move_speed = null;
				this.guide_scale = null;
				this.fade_speed_rate = null;
				this.move_speed_rate = null;
				this.guide_scale_rate = null;
				this.secret = null;
				this.time = null;
			}
			public float? time;
			public Button button;
			public Step step;
			public int? guide_color;
			public float? fade_speed;
			public float? move_speed;
			public float? guide_scale;
			public float? fade_speed_rate;
			public float? move_speed_rate;
			public float? guide_scale_rate;
			public bool? secret;

			public void Turn(int amount) {
				button.Turn(amount);
			}
			public void MirrorHolizontal() {
				button.MirrorHolizontal();
			}
			public void MirrorVertical() {
				button.MirrorVertical();
			}
			public override Note Clone () {
				var ret = new Hold (button.entity, step.Clone());
				ret.secret = secret;
				ret.guide_color = guide_color;
				ret.fade_speed = fade_speed;
				ret.move_speed = move_speed;
				ret.guide_scale = guide_scale;
				ret.fade_speed_rate = fade_speed_rate;
				ret.move_speed_rate = move_speed_rate;
				ret.guide_scale_rate = guide_scale_rate;
				ret.time = time;
				return ret;
			}
		}

		// スコアのスライドは、ドキュメントの"タップかブレークと、スライド"のまとめ役.
		[System.Serializable]
		public class Slide : Note, ISecretable, IGuideScaleCustomizable, IGuideColorCustomizable {
			public override Type GetType () { return Type.SLIDE; }
			public Slide() : base() { }
			public Slide(ISlideHead head, Step moveStep, Chain[][] order) : base() {
				this.head = head;
				this.patterns = new Pattern[order.Length];
				for (int i = 0; i < patterns.Length; i++) {
					patterns[i] = new Pattern(moveStep, order[i]);
				}
				this.secret = null;
			}

			public Slide(ISlideHead head, Pattern[] order) : base() {
				this.head = head;
				this.patterns = order;
				this.secret = null;
			}

			public ISlideHead head;
			public Pattern[] patterns;
			public bool? secret; //ガイドの非表示化.
			public void SetWaitStep(Step waitStep) {
				foreach (var pattern in patterns) {
					pattern.SetWaitStep(waitStep);
				}
			}

			public override Note Clone() {
				var ret = new Slide ();
				if (head == null)
					ret.head = null;
				else if (head.GetType() == Type.TAP)
					ret.head = (Note.Tap)((Note.Tap)head).Clone ();
				else if (head.GetType() == Type.BREAK)
					ret.head = (Note.Break)((Note.Break)head).Clone ();
				if (patterns != null) {
					ret.patterns = new Pattern[patterns.Length];
					for (int i = 0; i < patterns.Length; i++) {
						ret.patterns [i] = patterns [i].Clone ();
					}
				}
				ret.secret = secret;
				return ret;
			}

			// スコアのスライドパターンは、ドキュメントのスライドの実体.
			[System.Serializable]
			public class Pattern : ISecretable, IButtonTurnable, IFreeTurnable, IMirrorable {
				public Pattern() { }
				public Pattern(Step moveStep, Chain[] chains) {
					this.wait_step = null;
					this.move_step = moveStep;
					this.chains = chains;
					this.guide_color = null;
					this.fade_speed = null;
					this.marker_scale = null;
					this.fade_speed_rate = null;
					this.marker_scale_rate = null;
					this.secret = null;
					this.time = null;
				}
				public Chain[] chains;
				public Step wait_step;
				public Step move_step;
				public int? guide_color;
				public float? fade_speed;
				public float? marker_scale;
				public float? fade_speed_rate;
				public float? marker_scale_rate;
				public bool? secret;
				public float? time; // visualizeTime

				public void SetWaitStep(Step waitStep) {
					wait_step = waitStep;
				}

				public void Turn(int amount) {
					if (chains != null) {
						foreach (var chain in chains) {
							if (chain != null) {
								chain.Turn (amount);
							}
						}
					}
				}
				public void Turn(float degree) {
					if (chains != null) {
						foreach (var chain in chains) {
							if (chain != null) {
								chain.Turn (degree);
							}
						}
					}
				}
				public void MirrorHolizontal() {
					if (chains != null) {
						foreach (var chain in chains) {
							if (chain != null) {
								chain.MirrorHolizontal();
							}
						}
					}
				}
				public void MirrorVertical() {
					if (chains != null) {
						foreach (var chain in chains) {
							if (chain != null) {
								chain.MirrorVertical();
							}
						}
					}
				}
				public Pattern Clone() {
					var ret = new Pattern ();
					if (chains != null) {
						ret.chains = new Chain[chains.Length];
						for (int i = 0; i < chains.Length; i++) {
							ret.chains [i] = chains [i].Clone ();
						}
					}
					ret.wait_step = wait_step;
					ret.move_step = move_step;
					ret.guide_color = guide_color;
					ret.fade_speed = fade_speed;
					ret.marker_scale = marker_scale;
					ret.fade_speed_rate = fade_speed_rate;
					ret.marker_scale_rate = marker_scale_rate;
					ret.secret = secret;
					ret.time = time;
					return ret;
				}
			}
			
			[System.Serializable]
			public class Chain : IButtonTurnable, IFreeTurnable, IMirrorable {
				public Chain() { }
				public Chain(Command[] commands, int[] sections) {
					this.commands = commands;
					this.sections = sections;
					this.is_surface = null;
					this.guide_start_marker_positioning = null;
				}

				public Command[] commands;
				/// <summary>
				/// <para>判定するべきセンサーID</para>
				/// <para>0-7 Outer</para>
				/// <para>8-15 Inner</para>
				/// <para>16 Center</para>
				/// </summary>
				public int[] sections;

				/// <summary>
				/// <para>ガイドの始点をマーカーに合わせる.</para>
				/// <para>ワイドスライドの真ん中以外をtrueにすると見た目がいい</para>
				/// </summary>
				public bool? guide_start_marker_positioning;

				/// <summary>
				/// <para>表面である</para>
				/// <para>ワイドスライドの真ん中をtrueにするとガイドの見た目がいい</para>
				/// </summary>
				public bool? is_surface;

				public static int CalcSectionTurn(int sector, int amount) {
					if (sector < 0 || sector > 15)
						return sector;
					if (sector < 8) {
						var b = new Button(sector + 1);
						b.Turn(amount);
						return b.GetIndex();
					}
					else {
						var b = new Button(sector - 7);
						b.Turn(amount);
						return b.GetIndex() + 8;
					}
				}
				public static int CalcSectionMirrorHolizontal(int sector) {
					if (sector < 0 || sector > 15)
						return sector;
					if (sector < 8) {
						var b = new Button(sector + 1);
						b.MirrorHolizontal();
						return b.GetIndex();
					}
					else {
						var b = new Button(sector - 7);
						b.MirrorHolizontal();
						return b.GetIndex() + 8;
					}
				}
				public static int CalcSectionMirrorVertical(int sector) {
					if (sector < 0 || sector > 15)
						return sector;
					if (sector < 8) {
						var b = new Button(sector + 1);
						b.MirrorVertical();
						return b.GetIndex();
					}
					else {
						var b = new Button(sector - 7);
						b.MirrorVertical();
						return b.GetIndex() + 8;
					}
				}

				public void Turn(int amount) {
					if (commands != null) {
						foreach (var command in commands) {
							if (command != null) {
								command.Turn (amount);
							}
						}
					}
					if (sections != null) {
						for (int i = 0; i < sections.Length; i++) {
							sections[i] = CalcSectionTurn(sections[i], amount);
						}
					}
				}
				public void Turn(float degree) {
					if (commands != null) {
						foreach (var command in commands) {
							if (command != null) {
								command.Turn (degree);
							}
						}
					}
					if (sections != null) {
						int amount = (degree / 45.0f).ToInt();
						for (int i = 0; i < sections.Length; i++) {
							sections[i] = CalcSectionTurn(sections[i], amount);
						}
					}
				}
				public void MirrorHolizontal() {
					if (commands != null) {
						foreach (var command in commands) {
							if (command != null) {
								command.MirrorHolizontal();
							}
						}
					}
					if (sections != null) {
						for (int i = 0; i < sections.Length; i++) {
							sections[i] = CalcSectionMirrorHolizontal(sections[i]);
						}
					}
				}
				public void MirrorVertical() {
					if (commands != null) {
						foreach (var command in commands) {
							if (command != null) {
								command.MirrorVertical();
							}
						}
					}
					if (sections != null) {
						for (int i = 0; i < sections.Length; i++) {
							sections[i] = CalcSectionMirrorVertical(sections[i]);
						}
					}
				}
				public Chain Clone() {
					var ret = new Chain ();
					if (commands != null) {
						ret.commands = new Command[commands.Length];
						for (int i = 0; i < commands.Length; i++) {
							ret.commands [i] = commands [i].Clone ();
						}
					}
					if (sections != null) {
						ret.sections = new int[sections.Length];
						for (int i = 0; i < sections.Length; i++) {
							ret.sections [i] = sections [i];
						}
					}
					ret.is_surface = this.is_surface;
					ret.guide_start_marker_positioning = this.guide_start_marker_positioning;
					return ret;
				}
			}
			
			[System.Serializable]
			public abstract class Command : IButtonTurnable, IFreeTurnable, IMirrorable {
				public enum Type {
					STRAIGHT, CURVE
				}
				public abstract new Type GetType();
				public abstract void Turn(int amount);
				public abstract void Turn(float degree);
				public abstract void MirrorHolizontal();
				public abstract void MirrorVertical();
				private Vector2 CalcTurn(Vector2 pos, int amount) {
					return CalcTurn (pos, Constants.instance.GetPieceDegree (amount) - 22.5f);
				}
				private Vector2 CalcTurn(Vector2 pos, float degree) {
					// 右手座標.
					pos = Constants.instance.ToLeftHandedCoordinateSystemPosition (pos);
					float addDeg = degree;
					float deg = CircleCalculator.PointToDegree (pos) + 180.0f;
					float radius = CircleCalculator.PointToPointDistance (UnityEngine.Vector2.zero, pos);
					var turned = CircleCalculator.PointOnCircle (UnityEngine.Vector2.zero, radius, deg + addDeg);
					// 左手座標.
					var ret = Constants.instance.ToLeftHandedCoordinateSystemPosition (turned);
					return ret;
				}
				private Vector2 CalcMirrorHolizontal(Vector2 pos) {
					return new Vector2(pos.x * -1, pos.y);
				}
				private Vector2 CalcMirrorVertical(Vector2 pos) {
					return new Vector2(pos.x, pos.y * -1);
				}
				public abstract Command Clone();
				
				[System.Serializable]
				public class Straight : Command {
					public Straight() { }
					public Straight(Vector2 start, Vector2 target) {
						this.start = start;
						this.target = target;
					}
					public override Type GetType() { return Type.STRAIGHT; }
					public Vector2 start;
					public Vector2 target;

					public override void Turn(int amount) {
						start = CalcTurn (start, amount);
						target = CalcTurn (target, amount);
					}
					public override void Turn(float degree) {
						start = CalcTurn (start, degree);
						target = CalcTurn (target, degree);
					}
					public override void MirrorHolizontal() {
						start = CalcMirrorHolizontal (start);
						target = CalcMirrorHolizontal (target);
					}
					public override void MirrorVertical() {
						start = CalcMirrorVertical (start);
						target = CalcMirrorVertical (target);
					}
					public override Command Clone() {
						var ret = new Straight ();
						ret.start = start;
						ret.target = target;
						return ret;
					}
				}
				
				[System.Serializable]
				public class Curve : Command {
					public Curve() { }
					public Curve(Vector2 axis, float radius, float degOfStart, float degOfDistance) {
						this.axis = axis;
						this.radius = radius;
						this.degOfStart = degOfStart;
						this.degOfDistance = degOfDistance;
					}
					public override Type GetType() { return Type.CURVE; }
					public Vector2 axis;
					public float radius;
					public float degOfStart;
					public float degOfDistance;

					public override void Turn(int amount) {
						axis = CalcTurn (axis, amount);
						float sd = degOfStart;
						sd += amount * (360.0f / 8.0f);
						while (sd > 360.0f)
							sd -= 360.0f;
						while (sd < 0)
							sd += 360.0f;
						degOfStart = sd;
					}
					public override void Turn(float degree) {
						axis = CalcTurn (axis, degree);
						float sd = degOfStart;
						sd += degree;
						while (sd > 360.0f)
							sd -= 360.0f;
						while (sd < 0)
							sd += 360.0f;
						degOfStart = sd;
					}
					public override void MirrorHolizontal() {
						axis = CalcMirrorHolizontal (axis);
						float sd = degOfStart;
						sd = 360.0f - sd;
						while (sd > 360.0f)
							sd -= 360.0f;
						while (sd < 0)
							sd += 360.0f;
						degOfStart = sd;
						degOfDistance *= -1;
					}
					public override void MirrorVertical() {
						axis = CalcMirrorVertical (axis);
						float sd = degOfStart;
						sd = 180.0f - sd;
						while (sd > 360.0f)
							sd -= 360.0f;
						while (sd < 0)
							sd += 360.0f;
						degOfStart = sd;
						degOfDistance *= -1;
					}
					public override Command Clone() {
						var ret = new Curve ();
						ret.axis = axis;
						ret.radius = radius;
						ret.degOfStart = degOfStart;
						ret.degOfDistance = degOfDistance;
						return ret;
					}
				}
			}
		}
		
		[System.Serializable]
		public class Message : Note, ISecretable {
			public override Type GetType () { return Type.MESSAGE; }
			public string message;
			public Vector2 position;
			public float scale;
			public Color color;
			public Step step;
			public float? time;
			public bool? secret;
			public int? alignment;
			public Message() { }
			public Message(string message, Vector2 position, float scale, Color color, Step step) {
				this.message = message;
				this.position = position;
				this.scale = scale;
				this.color = color;
				this.step = step;
				this.time = null;
				this.secret = null;
				this.alignment = null;
			}
			public override Note Clone () {
				var ret = new Message ();
				ret.message = message;
				ret.position = position;
				ret.scale = scale;
				ret.color = color;
				ret.step = step;
				ret.time = time;
				ret.secret = secret;
				ret.alignment = alignment;
				return ret;
			}

		}

		[System.Serializable]
		public class ScrollMessage : Note, ISecretable {
			public override Type GetType () { return Type.SCROLL_MESSAGE; }
			public string message;
			public float y;
			public float scale;
			public Color color;
			public Step step;
			public float? time;
			public bool? secret;
			public int? alignment;
			public ScrollMessage() { }
			public ScrollMessage(string message, float y, float scale, Color color, Step step) {
				this.message = message;
				this.y = y;
				this.scale = scale;
				this.color = color;
				this.step = step;
				this.time = null;
				this.secret = null;
				this.alignment = null;
			}
			public override Note Clone () {
				var ret = new ScrollMessage ();
				ret.message = message;
				ret.y = y;
				ret.scale = scale;
				ret.color = color;
				ret.step = step;
				ret.time = time;
				ret.secret = secret;
				ret.alignment = alignment;
				return ret;
			}
		}
		
		[System.Serializable]
		public class SoundMessage : Note, ISecretable {
			public override Type GetType () { return Type.SOUND_MESSAGE; }
			public string soundId;
			public float? time;
			public bool? secret;
			public SoundMessage() { }
			public SoundMessage(string soundId) {
				this.soundId = soundId;
			}
			public override Note Clone () {
				var ret = new SoundMessage ();
				ret.soundId = soundId;
				ret.time = time;
				ret.secret = secret;
				return ret;
			}
		}
		
		[System.Serializable]
		public class Trap : Note, IButtonTurnable, IMirrorable, ISecretable, IGuideSpeedCustomizable, IGuideScaleCustomizable {
			public override Type GetType () { return Type.TRAP; }
			public Trap() : base() { }
			public Trap(int sensor, Step step) : base() {
				this.sensor = sensor;
				this.step = step;
				this.fade_speed = null;
				this.guide_scale = null;
				this.fade_speed_rate = null;
				this.guide_scale_rate = null;
				this.secret = null;
				this.time = null;
			}
			public float? time;
			public int sensor; // slideのchainのsectionsと同じ仕様.
			public Step step;
			public float? fade_speed;
			public float? guide_scale;
			public float? fade_speed_rate;
			public float? guide_scale_rate;
			public bool? secret;
			
			public void Turn(int amount) {
				sensor = Slide.Chain.CalcSectionTurn(sensor, amount);
			}
			public void MirrorHolizontal() {
				sensor = Slide.Chain.CalcSectionMirrorHolizontal(sensor);
			}
			public void MirrorVertical() {
				sensor = Slide.Chain.CalcSectionMirrorVertical(sensor);
			}
			public override Note Clone () {
				var ret = new Trap (sensor, step.Clone());
				ret.secret = secret;
				ret.fade_speed = fade_speed;
				ret.guide_scale = guide_scale;
				ret.fade_speed_rate = fade_speed_rate;
				ret.guide_scale_rate = guide_scale_rate;
				ret.time = time;
				return ret;
			}
		}
	}
	#endregion
}