using UnityEngine;
using System.Collections.Generic;
using XMaimaiNote.XDocument;
using XMaimaiScore;

public class MsqScriptReader {
	/// <summary>
	/// msqScriptを解読してオーダーデータを作り、譜面のドキュメントに変換する.
	/// </summary>
	public static Document[] Convert(string msqScript) {
		return new MaimaiScoreConverter (Read (msqScript)).ReadScore ();
	}

	/// <summary>
	/// msqScriptを解読してオーダーデータを作る.
	/// </summary>
	public static RhythmGameLibrary.Score.Order[] Read(string msqScript) {
		XMsqScript.Formatter.Result roomResult;
		XMsqScript.Order[] msqCmds;
		var msqResult = XMsqScript.Global.Analyze (msqScript, UserDataController.instance.GetLibrary, out roomResult, out msqCmds);
		if (msqResult.IsException) {
			// エラーだったらログを出してnullを返す.
			var ex = msqResult.Cast<XMsqScript.Exception>();
			if (ex.code == XMsqScript.ErrorCode.FORMAT_ERROR) {
				Debug.Log ("[ERROR][roomer]" + roomResult.Log());
			}
			else {
				Debug.Log ("[ERROR][msq]" + ex.Log());
			}
			return null;
		}
		var result = new List<RhythmGameLibrary.Score.Order> ();
		foreach (var msqCmd in msqCmds) {
			var cmd = ReadCommand(msqCmd);
			if (cmd != null) {
				result.AddRange(cmd);
			}
			else {
				return null;
			}
		}
		return result.ToArray();
	}

	private static RhythmGameLibrary.Score.Order[] ReadCommand(XMsqScript.Order msqCmd) {
		var result = new List<RhythmGameLibrary.Score.Order> ();
		switch (msqCmd.title) {
		case XMsqScript.OrderTitle.SET_BPM:
			result.Add(new RhythmGameLibrary.Score.OrderBpm(msqCmd.Cast<XMsqScript.OrderBpm>().aug.value));
			break;
		case XMsqScript.OrderTitle.SET_BEAT:
			result.Add(new RhythmGameLibrary.Score.OrderBeat(msqCmd.Cast<XMsqScript.OrderBeat>().aug.value));
			break;
		case XMsqScript.OrderTitle.SET_INTERVAL:
			result.Add(new RhythmGameLibrary.Score.OrderInterval(msqCmd.Cast<XMsqScript.OrderInterval>().aug.value));
			break;
		case XMsqScript.OrderTitle.SET_NOTES:
			var notes = new List<Note>();
			var msqNotes = msqCmd.Cast<XMsqScript.OrderNotes>().augs;
			foreach (var msqNote in msqNotes) {
				notes.Add (ReadNote (msqNote));
			}
			result.Add(new RhythmGameLibrary.Score.OrderNotes<Note>(notes.ToArray()));
			break;
		case XMsqScript.OrderTitle.SET_REST:
			result.Add(new RhythmGameLibrary.Score.OrderRest(msqCmd.Cast<XMsqScript.OrderRest>().aug.entity));
			break;
		case XMsqScript.OrderTitle.EXTRA_FORMAT:
			var extraFormatOrder = msqCmd.Cast<XMsqScript.OrderExtraFormat>();
			if (extraFormatOrder.formatName.key == "simai") {
				SimaiNoteReader.Result simaiReaderResult;
				string msqs = SimaiNoteReader.ToMsqScript(extraFormatOrder.script.entity, out simaiReaderResult);
				if (simaiReaderResult.type != SimaiNoteReader.ReadResult.SUCCESS) {
					Debug.Log ("[ERROR][simai]" + simaiReaderResult.Log());
					return null;
				}
				XMsqScript.Formatter.Result roomResult;
				XMsqScript.Order[] msqCmds;
				var msqResult = XMsqScript.Global.Analyze(msqs, UserDataController.instance.GetLibrary, out roomResult, out msqCmds);
				if (msqResult.IsException) {
					Debug.Log ("[ERROR][extra->msq]" + msqResult.Log());
					return null;
				}
				foreach (var msqCmd2 in msqCmds) {
					var cmd = ReadCommand(msqCmd2);
					if (cmd != null) {
						result.AddRange(cmd);
					}
					else {
						return null; //ReadCommand(msqCmd2)内でエラーが起きたのをそのまま返す.
					}
				}
			}
			else {
				Debug.Log ("[ERROR]Extra format name \"" + extraFormatOrder.formatName.key + "\" is undefined. " + TextDetailInfo.GetPlaceLog(extraFormatOrder.place));
				return null;
			}
			break;
		}
		return result.ToArray();
	}

	private static Note ReadNote(XMsqScript.INote note) {
		switch (note.type) {
		case XMsqScript.Type.NOTE_TAP: {
			return ReadTap(note as XMsqScript.NoteTap);
		}
		case XMsqScript.Type.NOTE_HOLD: {
			return ReadHold(note as XMsqScript.NoteHold);
		}
		case XMsqScript.Type.NOTE_SLIDE: {
			return ReadSlide(note as XMsqScript.NoteSlide);
		}
		case XMsqScript.Type.NOTE_BREAK: {
			return ReadBreak(note as XMsqScript.NoteBreak);
		}
		case XMsqScript.Type.NOTE_MESSAGE: {
			return ReadMessageNote(note as XMsqScript.NoteMessage);
		}
		case XMsqScript.Type.NOTE_SCROLL_MESSAGE: {
			return ReadScrollMessageNote(note as XMsqScript.NoteScrollMessage);
		}
		case XMsqScript.Type.NOTE_SOUND_MESSAGE: {
			return ReadSoundMessageNote(note as XMsqScript.NoteSoundMessage);
		}
		case XMsqScript.Type.NOTE_TRAP: {
			return ReadTrap(note as XMsqScript.NoteTrap);
		}
		}
		throw new UnityException ("invalied type.");
	}
	
	private static Note ReadTap(XMsqScript.NoteTap msq) {
		return ReadTapOption(new Note.Tap(msq.button.GetId()), msq.options);
	}
	
	private static Note.Tap ReadTapOption(Note.Tap score, XMsqScript.Table option) {
		foreach (var key in option.entity.Keys) {
			var value = option.entity[key];
			if (key == XMsqScript.Document.NoteOption.SECRET) {
				score.secret = value.Cast<XMsqScript.Flag>().entity;
			}
			else if (key == XMsqScript.Document.NoteOption.TIME) {
				score.time = (value as XMsqScript.IMsqValue).value;
			}
			else if (key == XMsqScript.Document.NoteOption.SHAPE) {
				var s = value.Cast<XMsqScript.Text>().entity;
				if (s == XMsqScript.Document.NoteOption.RING) {
					score.shape_type = (int)XMaimaiNote.XDocument.Document.Tap.GuideRingShapeType.CIRCLE;
				}
				else if (s == XMsqScript.Document.NoteOption.STAR) {
					score.shape_type = (int)XMaimaiNote.XDocument.Document.Tap.GuideRingShapeType.STAR;
				}
				else if (s == XMsqScript.Document.NoteOption.WSTAR) {
					score.shape_type = (int)XMaimaiNote.XDocument.Document.Tap.GuideRingShapeType.WSTAR;
				}
			}
			else if (key == XMsqScript.Document.NoteOption.FADE) {
				score.fade_speed = (value as XMsqScript.IMsqValue).value;
			}
			else if (key == XMsqScript.Document.NoteOption.FADE_RATE) {
				score.fade_speed_rate = (value as XMsqScript.IMsqValue).value;
			}
			else if (key == XMsqScript.Document.NoteOption.MOVE) {
				score.move_speed = (value as XMsqScript.IMsqValue).value;
			}
			else if (key == XMsqScript.Document.NoteOption.MOVE_RATE) {
				score.move_speed_rate = (value as XMsqScript.IMsqValue).value;
			}
			else if (key == XMsqScript.Document.NoteOption.SIZE) {
				score.guide_scale = (value as XMsqScript.IMsqValue).value;
			}
			else if (key == XMsqScript.Document.NoteOption.SIZE_RATE) {
				score.guide_scale_rate = (value as XMsqScript.IMsqValue).value;
			}
			else if (key == XMsqScript.Document.NoteOption.COLOR) {
				var s = value.Cast<XMsqScript.Text>().entity;
				if (s == XMsqScript.Document.NoteOption.SINGLE) {
					score.guide_color = (int)XMaimaiNote.XDocument.GuideColor.SINGLE;
				}
				else if (s == XMsqScript.Document.NoteOption.EACH) {
					score.guide_color = (int)XMaimaiNote.XDocument.GuideColor.EACH;
				}
			}
		}
		return score;
	}
	
	private static Note ReadHold(XMsqScript.NoteHold msq) {
		var step = ReadStep (msq.step);
		var score = new Note.Hold(msq.button.GetId(), step);
		foreach (var key in msq.options.entity.Keys) {
			var value = msq.options.entity[key];
			if (key == XMsqScript.Document.NoteOption.SECRET) {
				score.secret = value.Cast<XMsqScript.Flag>().entity;
			}
			else if (key == XMsqScript.Document.NoteOption.TIME) {
				score.time = (value as XMsqScript.IMsqValue).value;
			}
			else if (key == XMsqScript.Document.NoteOption.FADE) {
				score.fade_speed = (value as XMsqScript.IMsqValue).value;
			}
			else if (key == XMsqScript.Document.NoteOption.FADE_RATE) {
				score.fade_speed_rate = (value as XMsqScript.IMsqValue).value;
			}
			else if (key == XMsqScript.Document.NoteOption.MOVE) {
				score.move_speed = (value as XMsqScript.IMsqValue).value;
			}
			else if (key == XMsqScript.Document.NoteOption.MOVE_RATE) {
				score.move_speed_rate = (value as XMsqScript.IMsqValue).value;
			}
			else if (key == XMsqScript.Document.NoteOption.SIZE) {
				score.guide_scale = (value as XMsqScript.IMsqValue).value;
			}
			else if (key == XMsqScript.Document.NoteOption.SIZE_RATE) {
				score.guide_scale_rate = (value as XMsqScript.IMsqValue).value;
			}
			else if (key == XMsqScript.Document.NoteOption.COLOR) {
				var s = value.Cast<XMsqScript.Text>().entity;
				if (s == XMsqScript.Document.NoteOption.SINGLE) {
					score.guide_color = (int)XMaimaiNote.XDocument.GuideColor.SINGLE;
				}
				else if (s == XMsqScript.Document.NoteOption.EACH) {
					score.guide_color = (int)XMaimaiNote.XDocument.GuideColor.EACH;
				}
			}
		}
		return score;
	}
	
	private static Note ReadSlide(XMsqScript.NoteSlide msq) {
		var head = ReadSlideHead (msq.head);
		var patterns = new List<Note.Slide.Pattern>();
		foreach (var order in msq.orders) {
			patterns.Add (ReadSlidePattern(msq.head, order));
		}
		var score = new Note.Slide(head, patterns.ToArray());

		foreach (var key in msq.options.entity.Keys) {
			var value = msq.options.entity[key];
			if (key == XMsqScript.Document.NoteOption.SECRET) {
				score.secret = value.Cast<XMsqScript.Flag>().entity;
			}
		}
		return score;
	}

	private static Note ReadBreak(XMsqScript.NoteBreak msq) {
		return ReadBreakOption(new Note.Break(msq.button.GetId()), msq.options);
	}
	
	private static Note.Break ReadBreakOption(Note.Break score, XMsqScript.Table option) {
		foreach (var key in option.entity.Keys) {
			var value = option.entity[key];
			if (key == XMsqScript.Document.NoteOption.SECRET) {
				score.secret = value.Cast<XMsqScript.Flag>().entity;
			}
			else if (key == XMsqScript.Document.NoteOption.TIME) {
				score.time = (value as XMsqScript.IMsqValue).value;
			}
			else if (key == XMsqScript.Document.NoteOption.SHAPE) {
				var s = value.Cast<XMsqScript.Text>().entity;
				if (s == XMsqScript.Document.NoteOption.RING) {
					score.shape_type = (int)XMaimaiNote.XDocument.Document.Tap.GuideRingShapeType.CIRCLE;
				}
				else if (s == XMsqScript.Document.NoteOption.STAR) {
					score.shape_type = (int)XMaimaiNote.XDocument.Document.Tap.GuideRingShapeType.STAR;
				}
				else if (s == XMsqScript.Document.NoteOption.WSTAR) {
					score.shape_type = (int)XMaimaiNote.XDocument.Document.Tap.GuideRingShapeType.WSTAR;
				}
			}
			else if (key == XMsqScript.Document.NoteOption.FADE) {
				score.fade_speed = (value as XMsqScript.IMsqValue).value;
			}
			else if (key == XMsqScript.Document.NoteOption.FADE_RATE) {
				score.fade_speed_rate = (value as XMsqScript.IMsqValue).value;
			}
			else if (key == XMsqScript.Document.NoteOption.MOVE) {
				score.move_speed = (value as XMsqScript.IMsqValue).value;
			}
			else if (key == XMsqScript.Document.NoteOption.MOVE_RATE) {
				score.move_speed_rate = (value as XMsqScript.IMsqValue).value;
			}
			else if (key == XMsqScript.Document.NoteOption.SIZE) {
				score.guide_scale = (value as XMsqScript.IMsqValue).value;
			}
			else if (key == XMsqScript.Document.NoteOption.SIZE_RATE) {
				score.guide_scale_rate = (value as XMsqScript.IMsqValue).value;
			}
		}
		return score;
	}
	
	private static Note.Message ReadMessageNote(XMsqScript.NoteMessage msq) {
		var score = new Note.Message (msq.message.entity,
		                              msq.position.entity,
		                              msq.scale.value,
		                              msq.color.entity,
		                              ReadStep(msq.step));
		foreach (var key in msq.options.entity.Keys) {
			var value = msq.options.entity[key];
			if (key == XMsqScript.Document.NoteOption.SECRET) {
				score.secret = value.Cast<XMsqScript.Flag>().entity;
			}
			else if (key == XMsqScript.Document.NoteOption.TIME) {
				score.time = (value as XMsqScript.IMsqValue).value;
			}
			else if (key == XMsqScript.Document.NoteOption.ALIGNMENT) {
				var s = value.Cast<XMsqScript.Text>().entity;
				if (s == XMsqScript.Document.NoteOption.LEFT) {
					score.alignment = (int)XMaimaiNote.XDocument.Alignment.LEFT;
				}
				else if (s == XMsqScript.Document.NoteOption.CENTER) {
					score.alignment = (int)XMaimaiNote.XDocument.Alignment.CENTER;
				}
				else if (s == XMsqScript.Document.NoteOption.RIGHT) {
					score.alignment = (int)XMaimaiNote.XDocument.Alignment.RIGHT;
				}
			}
		}
		return score;
	}
	
	private static Note.ScrollMessage ReadScrollMessageNote(XMsqScript.NoteScrollMessage msq) {
		var score = new Note.ScrollMessage (msq.message.entity,
		                              msq.y.value,
		                              msq.scale.value,
		                              msq.color.entity,
		                              ReadStep(msq.step));
		foreach (var key in msq.options.entity.Keys) {
			var value = msq.options.entity[key];
			if (key == XMsqScript.Document.NoteOption.SECRET) {
				score.secret = value.Cast<XMsqScript.Flag>().entity;
			}
			else if (key == XMsqScript.Document.NoteOption.TIME) {
				score.time = (value as XMsqScript.IMsqValue).value;
			}
			else if (key == XMsqScript.Document.NoteOption.ALIGNMENT) {
				var s = value.Cast<XMsqScript.Text>().entity;
				if (s == XMsqScript.Document.NoteOption.LEFT) {
					score.alignment = (int)XMaimaiNote.XDocument.Alignment.LEFT;
				}
				else if (s == XMsqScript.Document.NoteOption.CENTER) {
					score.alignment = (int)XMaimaiNote.XDocument.Alignment.CENTER;
				}
				else if (s == XMsqScript.Document.NoteOption.RIGHT) {
					score.alignment = (int)XMaimaiNote.XDocument.Alignment.RIGHT;
				}
			}
		}
		return score;
	}
	
	private static Note.SoundMessage ReadSoundMessageNote(XMsqScript.NoteSoundMessage msq) {
		var score = new Note.SoundMessage (msq.soundId.entity);
		foreach (var key in msq.options.entity.Keys) {
			var value = msq.options.entity[key];
			if (key == XMsqScript.Document.NoteOption.SECRET) {
				score.secret = value.Cast<XMsqScript.Flag>().entity;
			}
			else if (key == XMsqScript.Document.NoteOption.TIME) {
				score.time = (value as XMsqScript.IMsqValue).value;
			}
		}
		return score;
	}
	
	private static Note ReadTrap(XMsqScript.NoteTrap msq) {
		var step = ReadStep (msq.step);
		var score = new Note.Trap((int)msq.sensor.entity, step);
		foreach (var key in msq.options.entity.Keys) {
			var value = msq.options.entity[key];
			if (key == XMsqScript.Document.NoteOption.SECRET) {
				score.secret = value.Cast<XMsqScript.Flag>().entity;
			}
			else if (key == XMsqScript.Document.NoteOption.TIME) {
				score.time = (value as XMsqScript.IMsqValue).value;
			}
			else if (key == XMsqScript.Document.NoteOption.FADE) {
				score.fade_speed = (value as XMsqScript.IMsqValue).value;
			}
			else if (key == XMsqScript.Document.NoteOption.FADE_RATE) {
				score.fade_speed_rate = (value as XMsqScript.IMsqValue).value;
			}
			else if (key == XMsqScript.Document.NoteOption.SIZE) {
				score.guide_scale = (value as XMsqScript.IMsqValue).value;
			}
			else if (key == XMsqScript.Document.NoteOption.SIZE_RATE) {
				score.guide_scale_rate = (value as XMsqScript.IMsqValue).value;
			}
		}
		return score;
	}

	private static Step ReadStep(XMsqScript.IStep step) {
		switch (step.type) {
		case XMsqScript.Type.STEP_BASIC: {
			var s = step as XMsqScript.StepBasic;
			return Step.CreateBasic(s.beat.value, s.length.value);
		}
		case XMsqScript.Type.STEP_INTERVAL: {
			var s = step as XMsqScript.StepInterval;
			return Step.CreateInterval(s.interval.value);
		}
		case XMsqScript.Type.STEP_LOCALBPM: {
			var s = step as XMsqScript.StepLocalBpm;
			return Step.CreateLocalBpm(s.bpm.value, s.beat.value, s.length.value);
		}
		case XMsqScript.Type.STEP_MASS: {
			var s = step as XMsqScript.StepMass;
			var list = new List<Step>();
			foreach (var entity in s.entities) {
				list.Add(ReadStep(entity));
			}
			return Step.CreateMass(list.ToArray());
		}
		}
		throw new UnityException ("invalied type.");
	}
	
	private static Step ReadStep(XMsqScript.ISlideWait wait) {
		switch (wait.type) {
		case XMsqScript.Type.SLIDE_WAIT_DEFAULT: {
			return Step.CreateSlideWaitDefault();
		}
		case XMsqScript.Type.SLIDE_WAIT_INTERVAL: {
			var s = wait as XMsqScript.SlideWaitInterval;
			return Step.CreateInterval(s.time.value);
		}
		case XMsqScript.Type.SLIDE_WAIT_LOCALBPM: {
			var s = wait as XMsqScript.SlideWaitLocalBpm;
			return Step.CreateSlideWaitLocalBpm(s.bpm.value);
		}
		case XMsqScript.Type.SLIDE_WAIT_STEP_BASIC: {
			var s = wait as XMsqScript.SlideWaitStepBasic;
			return Step.CreateBasic(s.beat.value, s.length.value);
		}
		case XMsqScript.Type.SLIDE_WAIT_STEP_LOCALBPM: {
			var s = wait as XMsqScript.SlideWaitStepLocalBpm;
			return Step.CreateLocalBpm(s.bpm.value, s.beat.value, s.length.value);
		}
		case XMsqScript.Type.SLIDE_WAIT_MASS: {
			var s = wait as XMsqScript.SlideWaitMass;
			var list = new List<Step>();
			foreach (var entity in s.entities) {
				list.Add(ReadStep(entity));
			}
			return Step.CreateMass(list.ToArray());
		}
		default: {
			if (wait.IsStep) {
				return ReadStep(wait as XMsqScript.IStep);
			}
			break;
		}
		}
		throw new UnityException ("invalied type.");
	}

	private static Note.ISlideHead ReadSlideHead(XMsqScript.ISlideHead head) {
		switch (head.type) {
		case XMsqScript.Type.SLIDE_HEAD_NOTHING: {
			return null;
		}
		case XMsqScript.Type.SLIDE_HEAD_STAR: {
			return ReadTapOption(new Note.Tap(head.star.Value), head.GetOption());
		}
		case XMsqScript.Type.SLIDE_HEAD_BREAK: {
			return ReadBreakOption(new Note.Break(head.star.Value), head.GetOption());
		}
		}
		throw new UnityException ("invalied type.");
	}

	private static Note.Slide.Pattern ReadSlidePattern(XMsqScript.ISlideHead head, XMsqScript.ISlidePattern pattern) {
		Note.Slide.Pattern score = null;
		switch (pattern.type) {
		case XMsqScript.Type.SLIDE_PATTERN: {
			var s = pattern as XMsqScript.SlidePattern;
			var list = new List<Note.Slide.Chain>();
			foreach (var order in s.orders) {
				list.Add (ReadSlideChain(head, order));
			}
			var wait = ReadStep (s.wait);
			var step = ReadStep (s.step);
			score = new Note.Slide.Pattern(step, list.ToArray());
			score.SetWaitStep (wait);
			break;
		}
		}
		if (score != null) {
			foreach (var key in pattern.GetOption().entity.Keys) {
				var value = pattern.GetOption().entity[key];
				if (key == XMsqScript.Document.NoteOption.SECRET) {
					score.secret = value.Cast<XMsqScript.Flag>().entity;
				}
				else if (key == XMsqScript.Document.NoteOption.TIME) {
					score.time = (value as XMsqScript.IMsqValue).value;
				}
				else if (key == XMsqScript.Document.NoteOption.FADE) {
					score.fade_speed = (value as XMsqScript.IMsqValue).value;
				}
				else if (key == XMsqScript.Document.NoteOption.FADE_RATE) {
					score.fade_speed_rate = (value as XMsqScript.IMsqValue).value;
				}
				else if (key == XMsqScript.Document.NoteOption.SIZE) {
					score.marker_scale = (value as XMsqScript.IMsqValue).value;
				}
				else if (key == XMsqScript.Document.NoteOption.SIZE_RATE) {
					score.marker_scale_rate = (value as XMsqScript.IMsqValue).value;
				}
				else if (key == XMsqScript.Document.NoteOption.COLOR) {
					var s = value.Cast<XMsqScript.Text>().entity;
					if (s == XMsqScript.Document.NoteOption.SINGLE) {
						score.guide_color = (int)XMaimaiNote.XDocument.GuideColor.SINGLE;
					}
					else if (s == XMsqScript.Document.NoteOption.EACH) {
						score.guide_color = (int)XMaimaiNote.XDocument.GuideColor.EACH;
					}
				}
			}
			return score;
		}
		throw new UnityException ("invalied type.");
	}

	private static Note.Slide.Chain ReadSlideChain(XMsqScript.ISlideHead head, XMsqScript.ISlidePattern chain) {
		Note.Slide.Chain score = null;
		switch (chain.type) {
		case XMsqScript.Type.SLIDE_CHAIN: {
			var s = chain as XMsqScript.SlideChain;
			var list = new List<Note.Slide.Command>();
			XMsqScript.ISlideTrimmedCommand before = null;
			foreach (var order in s.orders) {
				list.Add (ReadSlideCommand(head, order, ref before));
			}
			score = new Note.Slide.Chain(list.ToArray(), null);
			break;
		}
		}
		if (score != null) {
			foreach (var key in chain.GetOption().entity.Keys) {
				var value = chain.GetOption().entity[key];
				if (key == XMsqScript.Document.NoteOption.CENTER) {
					score.is_surface = value.Cast<XMsqScript.Flag>().entity;
				}
				else if (key == XMsqScript.Document.NoteOption.SIDE) {
					score.guide_start_marker_positioning = value.Cast<XMsqScript.Flag>().entity;
				}
				else if (key == XMsqScript.Document.NoteOption.SECTIONS) {
					var array = value.Cast<XMsqScript.Array>();
					score.sections = new int[array.entity.Count];
					int index = 0;
					foreach (var elm in array.entity) {
						score.sections[index] = (int)elm.Cast<XMsqScript.Sensor>().entity;
						index++;
					}
				}
			}
			return score;
		}
		throw new UnityException ("invalied type.");
	}

	private static Note.Slide.Command ReadSlideCommand(XMsqScript.ISlideHead head, XMsqScript.ISlideChain command, ref XMsqScript.ISlideTrimmedCommand beforeCommand) {
		switch (command.type) {
		case XMsqScript.Type.SLIDE_COMMAND_STRAIGHT: {
			var s = command as XMsqScript.SlideCommandStraight;
			beforeCommand = s;
			// Msqは左手座標、NoteDocumentは右手座標なので、変換する
			return new Note.Slide.Command.Straight(s.start.entity.ChangePosHandSystem(), s.target.entity.ChangePosHandSystem());
		}
		case XMsqScript.Type.SLIDE_COMMAND_CURVE: {
			var s = command as XMsqScript.SlideCommandCurve;
			beforeCommand = s;
			// Msqは左手座標、NoteDocumentは右手座標なので、変換する。角度はどちらも右手座標なのでなにもしない.
			return new Note.Slide.Command.Curve(s.center.entity.ChangePosHandSystem(), s.radius.entity.x, s.start_degree.value, s.distance_degree.value);
		}
		case XMsqScript.Type.SLIDE_COMMAND_CONTINUED_STRAIGHT: {
			var s = command as XMsqScript.SlideCommandContinuedStraight;
			var entity = s.GetEntity(head, beforeCommand) as XMsqScript.SlideCommandStraight;
			beforeCommand = entity;
			return new Note.Slide.Command.Straight(entity.start.entity.ChangePosHandSystem(), s.target.entity.ChangePosHandSystem());
		}
		case XMsqScript.Type.SLIDE_COMMAND_CONTINUED_CURVE: {
			var s = command as XMsqScript.SlideCommandContinuedCurve;
			var entity = s.GetEntity(head, beforeCommand) as XMsqScript.SlideCommandCurve;
			beforeCommand = entity;
			return new Note.Slide.Command.Curve(s.center.entity.ChangePosHandSystem(), entity.radius.entity.x, entity.start_degree.value, s.distance_degree.value);
		}
		}
		throw new UnityException ("invalied type.");
	}
}
