﻿namespace XMaimaiNote {
	namespace XDocument {
		public abstract class ScrutinyDocument : Document {
			public interface IGuideRingColor : IEachArch {
				GuideColor? orderGuideColor { get; }
				GuideColor guideColor { get; set; }
			}
			public interface IGuideMarkerColor {
				GuideColor? orderMarkerColor { get; }
				GuideColor markerColor { get; set; }
				long eachTime { get; }
				bool secret { get; }
				bool IsEach (IGuideMarkerColor other);
			}
			public interface IEachArch {
				int? archId { get; set; }
				long eachTime { get; }
				long guideFadeSpeed { get; }
				long guideMoveSpeed { get; }
				bool secret { get; }
				bool IsEach (IEachArch other);
			}
			public interface ISlideHead {
				Document.Tap.GuideRingShapeType guideShapeType { get; set; }
				float guideSpinSpeed { get; set; }
				float guideScale { get; set; }
			}

			public new class Tap : Document.Tap, ISlideHead, IGuideRingColor {
				public GuideColor? orderGuideColor { get; set; }
				public long eachTime { get { return justTime; } }
				public bool IsEach (IEachArch other) {
					return !secret && !other.secret &&
						eachTime == other.eachTime &&
						guideFadeSpeed == other.guideFadeSpeed &&
						guideMoveSpeed == other.guideMoveSpeed;
				}
			}
			
			public new class Hold : Document.Hold, IGuideRingColor {
				public GuideColor? orderGuideColor { get; set; }
				public long eachTime { get { return headTime; } }
				public bool IsEach (IEachArch other) {
					return !secret && !other.secret &&
						eachTime == other.eachTime &&
						guideFadeSpeed == other.guideFadeSpeed &&
						guideMoveSpeed == other.guideMoveSpeed;
				}
			}
			
			public new class Slide : Document.Slide {
				public new class Pattern : Document.Slide.Pattern, IGuideMarkerColor {
					public GuideColor? orderMarkerColor { get; set; }
					public long eachTime { get { return visualizeTime; } }
					public bool IsEach (IGuideMarkerColor other) {
						return !secret && !other.secret && eachTime == other.eachTime;
					}
				}
			}

			public new class Break : Document.Break, ISlideHead, IEachArch {
				public long eachTime { get { return justTime; } }
				public bool IsEach (IEachArch other) {
					return !secret && !other.secret &&
						eachTime == other.eachTime &&
						guideFadeSpeed == other.guideFadeSpeed &&
						guideMoveSpeed == other.guideMoveSpeed;
				}
			}
			public new class Message : Document.Message {
			}
			public new class ScrollMessage : Document.ScrollMessage {
			}
			public new class SoundMessage : Document.SoundMessage {
			}
		}
	}
}