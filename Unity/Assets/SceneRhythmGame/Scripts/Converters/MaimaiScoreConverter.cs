using UnityEngine;
using System.Collections.Generic;
using RhythmGameLibrary.Score;
using XMaimaiScore;
using XMaimaiNote.XDocument;

public class MaimaiScoreConverter : ScoreConverter<Order, Note, Document> {
	public MaimaiScoreConverter(Order[] score_data, bool mirror, int turn, bool tapToBreak, bool breakToTap, bool ringToStar, bool starToRing, float guideFadeSpeed, float guideMoveSpeed, float guideScale, float markerScale, bool starRotation, float trapScale, int[] getTimeRequestScoreIndexes) : base(score_data, 4, 4, Constants.instance.START_COUNT_START_TIME, getTimeRequestScoreIndexes) {
		this.subTimeMaxValue = 0;
		
		this.mirror = mirror;
		this.turn = turn;
		this.tapToBreak = tapToBreak;
		this.breakToTap = breakToTap;
		this.ringToStar = ringToStar;
		this.starToRing = starToRing;
		this.guideFadeSpeed = guideFadeSpeed;
		this.guideMoveSpeed = guideMoveSpeed;
		this.guideScale = guideScale;
		this.markerScale = markerScale;
		this.starRotation = starRotation;
		this.trapScale = trapScale;
		this.trapScaleInverse = 1.0f / Constants.instance.SENSOR_RADIUS_DEFAULT;
		
		this.rings = new List<ScrutinyDocument.IGuideRingColor> ();
		this.markers = new List<ScrutinyDocument.IGuideMarkerColor> ();
		this.archTargets = new List<ScrutinyDocument.IEachArch> ();
	}
	public MaimaiScoreConverter(Order[] score_data) : this (score_data, false, 0, false, false, false, false, 0.3f, 0.7f, 0.75f, 1.0f, true, 70.0f, null) { }
	
	private long subTimeMaxValue; //スライドやホールドの終点時間の、全ノート中の最大値.
	private float trapScaleInverse; //トラップスケールの計算用逆数.
	
	// オプションのコピー.
	private bool mirror;
	private int turn;
	private bool tapToBreak;
	private bool breakToTap;
	private bool ringToStar;
	private bool starToRing;
	private float guideFadeSpeed;
	private float guideMoveSpeed;
	private float guideScale;
	private float markerScale;
	private float trapScale;
	private bool starRotation;
	
	// イーチ情報加工用.
	private List<ScrutinyDocument.IGuideRingColor> rings;
	private List<ScrutinyDocument.IGuideMarkerColor> markers;
	private List<ScrutinyDocument.IEachArch> archTargets;
	
	protected override Document[] ReadScoreOrderNotes(Note[] notes) {
		var createNotes = new List<Document> ();
		foreach (var noteInfo in notes) {
			var info = noteInfo.Clone ();
			if (info.GetType() == Note.Type.TAP) {
				var note = Scrutinize (info as Note.Tap);
				if (note != null) {
					createNotes.Add (note);
					if (note.type == Type.TAP) {
						rings.Add ((ScrutinyDocument.IGuideRingColor)note);
					}
					archTargets.Add ((ScrutinyDocument.IEachArch)note);
				}
			}
			else if (info.GetType() == Note.Type.HOLD) {
				var note = Scrutinize (info as Note.Hold);
				if (note != null) {
					createNotes.Add (note);
					rings.Add ((ScrutinyDocument.IGuideRingColor)note);
					archTargets.Add ((ScrutinyDocument.IEachArch)note);
				}
			}
			else if (info.GetType() == Note.Type.SLIDE) {
				var slnotes = Scrutinize (info as Note.Slide);
				if (slnotes != null) {
					createNotes.AddRange (slnotes);
					foreach (var note in slnotes) {
						if (note.type == Type.SLIDE)
							markers.Add ((ScrutinyDocument.IGuideMarkerColor)((ScrutinyDocument.Slide)note).pattern);
						else if (note.type == Type.TAP)
							rings.Add((ScrutinyDocument.IGuideRingColor)note);
					}
				}
			}
			else if (info.GetType() == Note.Type.BREAK) {
				var note = Scrutinize (info as Note.Break);
				if (note != null) {
					createNotes.Add (note);
					if (note.type == Type.TAP) {
						rings.Add ((ScrutinyDocument.IGuideRingColor)note);
					}
					archTargets.Add ((ScrutinyDocument.IEachArch)note);
				}
			}
			else if (info.GetType() == Note.Type.MESSAGE) {
				var note = Scrutinize (info as Note.Message);
				if (note != null) {
					createNotes.Add (note);
				}
			}
			else if (info.GetType() == Note.Type.SCROLL_MESSAGE) {
				var note = Scrutinize (info as Note.ScrollMessage);
				if (note != null) {
					createNotes.Add (note);
				}
			}
			else if (info.GetType() == Note.Type.SOUND_MESSAGE) {
				var note = Scrutinize (info as Note.SoundMessage);
				if (note != null) {
					createNotes.Add (note);
				}
			}
			else if (info.GetType() == Note.Type.TRAP) {
				var note = Scrutinize (info as Note.Trap);
				if (note != null) {
					createNotes.Add (note);
				}
			}
		}
		return createNotes.ToArray();
	}
	
	protected override void ReadEndScoreEvent (Document[] notes) {
		// eachの設定.

		int ringLen = rings.Count;
		for (int i = 0; i < ringLen; i++) {
			var r1 = rings[i];
			if (r1.orderGuideColor.HasValue) {
				r1.guideColor = r1.orderGuideColor.Value;
			}
			else {
				bool setted = false;
				for (int j = 0; j < ringLen; j++) {
					if (i != j) {
						var r2 = rings[j];
						//orderGuideColor.HasValueがfalseで、両者fadeSpeed,moveSpeed,justTimeがすべて同じならeach color.
						if (r1.IsEach(r2)) {
							r1.guideColor = GuideColor.EACH;
							setted = true;
							break;
						}
					}
				}
				if (!setted) {
					r1.guideColor = GuideColor.SINGLE;
				}
			}
		}
		
		int markLen = markers.Count;
		for (int i = 0; i < markLen; i++) {
			var m1 = markers[i];
			if (m1.orderMarkerColor.HasValue) {
				m1.markerColor = m1.orderMarkerColor.Value;
			}
			else {
				bool setted = false;
				for (int j = 0; j < markLen; j++) {
					if (i != j) {
						var m2 = markers[j];
						//orderMarkerColor.HasValueがfalseで、両者visualizeTimeが同じならeach color.
						if (m1.IsEach(m2)) {
							m1.markerColor = GuideColor.EACH;
							setted = true;
							break;
						}
					}
				}
				if (!setted) {
					m1.markerColor = GuideColor.SINGLE;
				}
			}
		}

		int archLen = archTargets.Count;
		int archId = 0;
		for (int i = 0; i < archLen; i++) {
			var a1 = archTargets[i];
			for (int j = 0; j < archLen; j++) {
				if (i != j) {
					var a2 = archTargets[j];
					if (a1.IsEach(a2)) {
						if (a1.archId.HasValue) {
							if (!a2.archId.HasValue) {
								a2.archId = a1.archId.Value;
							}
						}
						else {
							a1.archId = archId;
							a2.archId = archId;
							archId++;
						}
					}
				}
			}
		}
		
	}
	
	
	public Document Scrutinize (Note.Tap data) {
		if (!this.tapToBreak) {
			var result = new ScrutinyDocument.Tap();
			// button
			if (data.button != null && data.button.GetIndex() >= 0 && data.button.GetIndex() < 8) {
				if (this.turn > 0) {
					data.Turn (this.turn);
				}
				if (this.mirror) {
					data.MirrorHolizontal ();
				}
				result.button = new Document.Button(data.button.entity);
			}
			else return null;
			// justTime
			if (data.time.HasValue){
				result.justTime = (data.time.Value * 1000.0f).ToLong();;
			}
			else {
				result.justTime = GetTimeMS ();
			}
			// secret
			if (data.secret.HasValue) {
				result.secret = data.secret.Value;
			}
			else {
				result.secret = false;
			}
			// guideSpinSpeed
			result.guideSpinSpeed = 0;
			// guideShapeType
			if (ringToStar) {
				result.guideShapeType = Document.Tap.GuideRingShapeType.STAR;
			}
			else if (data.shape_type.HasValue && !starToRing) {
				result.guideShapeType = (Document.Tap.GuideRingShapeType)data.shape_type.Value;
			}
			else {
				result.guideShapeType = Document.Tap.GuideRingShapeType.CIRCLE;
			}
			// guideColor
			if (data.guide_color.HasValue) {
				result.orderGuideColor = (GuideColor)data.guide_color.Value;
			}
			else {
				result.orderGuideColor = null;
			}
			// guideFadeSpeed
			float rate = data.fade_speed_rate ?? 1f;
			if (data.fade_speed.HasValue) {
				result.guideFadeSpeed = (data.fade_speed.Value * rate * 1000.0f).ToLong();
			}
			else {
				result.guideFadeSpeed = (this.guideFadeSpeed * rate * 1000.0f).ToLong();
			}
			// guideMoveSpeed
			rate = data.move_speed_rate ?? 1f;
			if (data.move_speed.HasValue) {
				result.guideMoveSpeed = (data.move_speed.Value * rate * 1000.0f).ToLong();
			}
			else {
				result.guideMoveSpeed = (this.guideMoveSpeed * rate * 1000.0f).ToLong();
			}
			// guideScale
			rate = data.guide_scale_rate ?? 1f;
			if (data.guide_scale.HasValue) {
				result.guideScale = data.guide_scale.Value * rate;
			}
			else {
				result.guideScale = this.guideScale * rate;
			}
			return result;
		}
		else {
			var result = new ScrutinyDocument.Break();
			// button
			if (data.button != null && data.button.GetIndex() >= 0 && data.button.GetIndex() < 8) {
				if (this.turn > 0) {
					data.Turn (this.turn);
				}
				if (this.mirror) {
					data.MirrorHolizontal ();
				}
				result.button = new Document.Button(data.button.entity);
			}
			else return null;
			// justTime
			if (data.time.HasValue){
				result.justTime = (data.time.Value * 1000.0f).ToLong();;
			}
			else {
				result.justTime = GetTimeMS ();
			}
			// secret
			if (data.secret.HasValue) {
				result.secret = data.secret.Value;
			}
			else {
				result.secret = false;
			}
			// guideSpinSpeed
			result.guideSpinSpeed = 0;
			// guideShapeType
			if (ringToStar) {
				result.guideShapeType = Document.Tap.GuideRingShapeType.STAR;
			}
			else if (data.shape_type.HasValue && !starToRing) {
				result.guideShapeType = (Document.Tap.GuideRingShapeType)data.shape_type.Value;
			}
			else {
				result.guideShapeType = Document.Tap.GuideRingShapeType.CIRCLE;
			}
			// guideFadeSpeed
			float rate = data.fade_speed_rate ?? 1f;
			if (data.fade_speed.HasValue) {
				result.guideFadeSpeed = (data.fade_speed.Value * rate * 1000.0f).ToLong();
			}
			else {
				result.guideFadeSpeed = (this.guideFadeSpeed * rate * 1000.0f).ToLong();
			}
			// guideMoveSpeed
			rate = data.move_speed_rate ?? 1f;
			if (data.move_speed.HasValue) {
				result.guideMoveSpeed = (data.move_speed.Value * rate * 1000.0f).ToLong();
			}
			else {
				result.guideMoveSpeed = (this.guideMoveSpeed * rate * 1000.0f).ToLong();
			}
			// guideScale
			rate = data.guide_scale_rate ?? 1f;
			if (data.guide_scale.HasValue) {
				result.guideScale = data.guide_scale.Value * rate;
			}
			else {
				result.guideScale = this.guideScale * rate;
			}
			return result;
		}
	}
	
	public Document Scrutinize (Note.Break data) {
		if (!this.breakToTap) {
			var result = new ScrutinyDocument.Break();
			// button
			if (data.button != null && data.button.GetIndex() >= 0 && data.button.GetIndex() < 8) {
				if (this.turn > 0) {
					data.Turn (this.turn);
				}
				if (this.mirror) {
					data.MirrorHolizontal ();
				}
				result.button = new Document.Button(data.button.entity);
			}
			else return null;
			// justTime
			if (data.time.HasValue){
				result.justTime = (data.time.Value * 1000.0f).ToLong();
			}
			else {
				result.justTime = GetTimeMS ();
			}
			// secret
			if (data.secret.HasValue) {
				result.secret = data.secret.Value;
			}
			else {
				result.secret = false;
			}
			// guideSpinSpeed
			result.guideSpinSpeed = 0;
			// guideShapeType
			if (ringToStar) {
				result.guideShapeType = Document.Tap.GuideRingShapeType.STAR;
			}
			else if (data.shape_type.HasValue && !starToRing) {
				result.guideShapeType = (Document.Tap.GuideRingShapeType)data.shape_type.Value;
			}
			else {
				result.guideShapeType = Document.Tap.GuideRingShapeType.CIRCLE;
			}
			// guideFadeSpeed
			float rate = data.fade_speed_rate ?? 1f;
			if (data.fade_speed.HasValue) {
				result.guideFadeSpeed = (data.fade_speed.Value * rate * 1000.0f).ToLong();
			}
			else {
				result.guideFadeSpeed = (this.guideFadeSpeed * rate * 1000.0f).ToLong();
			}
			// guideMoveSpeed
			rate = data.move_speed_rate ?? 1f;
			if (data.move_speed.HasValue) {
				result.guideMoveSpeed = (data.move_speed.Value * rate * 1000.0f).ToLong();
			}
			else {
				result.guideMoveSpeed = (this.guideMoveSpeed * rate * 1000.0f).ToLong();
			}
			// guideScale
			rate = data.guide_scale_rate ?? 1f;
			if (data.guide_scale.HasValue) {
				result.guideScale = data.guide_scale.Value * rate;
			}
			else {
				result.guideScale = this.guideScale * rate;
			}
			return result;
		}
		else {
			var result = new ScrutinyDocument.Tap();
			// button
			if (data.button != null && data.button.GetIndex() >= 0 && data.button.GetIndex() < 8) {
				if (this.turn > 0) {
					data.Turn (this.turn);
				}
				if (this.mirror) {
					data.MirrorHolizontal ();
				}
				result.button = new Document.Button(data.button.entity);
			}
			else return null;
			// justTime
			if (data.time.HasValue){
				result.justTime = (data.time.Value * 1000.0f).ToLong();
			}
			else {
				result.justTime = GetTimeMS ();
			}
			// secret
			if (data.secret.HasValue) {
				result.secret = data.secret.Value;
			}
			else {
				result.secret = false;
			}
			// guideSpinSpeed
			result.guideSpinSpeed = 0;
			// guideShapeType
			if (ringToStar) {
				result.guideShapeType = Document.Tap.GuideRingShapeType.STAR;
			}
			else if (data.shape_type.HasValue && !starToRing) {
				result.guideShapeType = (Document.Tap.GuideRingShapeType)data.shape_type.Value;
			}
			else {
				result.guideShapeType = Document.Tap.GuideRingShapeType.CIRCLE;
			}
			// guideColor
			result.orderGuideColor = null;
			// guideFadeSpeed
			float rate = data.fade_speed_rate ?? 1f;
			if (data.fade_speed.HasValue) {
				result.guideFadeSpeed = (data.fade_speed.Value * rate * 1000.0f).ToLong();
			}
			else {
				result.guideFadeSpeed = (this.guideFadeSpeed * rate * 1000.0f).ToLong();
			}
			// guideMoveSpeed
			rate = data.move_speed_rate ?? 1f;
			if (data.move_speed.HasValue) {
				result.guideMoveSpeed = (data.move_speed.Value * rate * 1000.0f).ToLong();
			}
			else {
				result.guideMoveSpeed = (this.guideMoveSpeed * rate * 1000.0f).ToLong();
			}
			// guideScale
			rate = data.guide_scale_rate ?? 1f;
			if (data.guide_scale.HasValue) {
				result.guideScale = data.guide_scale.Value * rate;
			}
			else {
				result.guideScale = this.guideScale * rate;
			}
			return result;
		}
	}
	
	public Document Scrutinize (Note.Hold data) {
		var result = new ScrutinyDocument.Hold();
		// button
		if (data.button != null && data.button.GetIndex() >= 0 && data.button.GetIndex() < 8) {
			if (this.turn > 0) {
				data.Turn (this.turn);
			}
			if (this.mirror) {
				data.MirrorHolizontal ();
			}
			result.button = new Document.Button(data.button.entity);
		}
		else return null;
		// headTime
		if (data.time.HasValue) {
			result.headTime = (data.time.Value * 1000.0f).ToLong();
		}
		else {
			result.headTime = GetTimeMS ();
		}
		// footTime
		{
			decimal subTime;
			decimal? tempSubTime = StepConvert (data.step);
			if (tempSubTime.HasValue) subTime = tempSubTime.Value;
			else return null;
			decimal leaveTime;
			if (data.time.HasValue) {
				leaveTime = (decimal)(data.time.Value) + subTime;
			}
			else {
				leaveTime = GetTime () + subTime;
			}
			long leaveTimeMS = (long)(leaveTime * 1000.0M);
			subTimeMaxValue = MathlMax (subTimeMaxValue, leaveTimeMS);
			result.footTime = leaveTimeMS;
		}
		// secret
		if (data.secret.HasValue) {
			result.secret = data.secret.Value;
		}
		else {
			result.secret = false;
		}
		// fadeSpeed
		float rate = data.fade_speed_rate ?? 1f;
		if (data.fade_speed.HasValue) { 
			result.guideFadeSpeed = (data.fade_speed.Value * rate * 1000.0f).ToLong();
		}
		else {
			result.guideFadeSpeed = (this.guideFadeSpeed * rate * 1000.0f).ToLong();
		}
		// moveSpeed
		rate = data.move_speed_rate ?? 1f;
		if (data.move_speed.HasValue) { 
			result.guideMoveSpeed = (data.move_speed.Value * rate * 1000.0f).ToLong();
		}
		else {
			result.guideMoveSpeed = (this.guideMoveSpeed * rate * 1000.0f).ToLong();
		}
		// guideScale
		rate = data.guide_scale_rate ?? 1f;
		if (data.guide_scale.HasValue) {
			result.guideScale = data.guide_scale.Value * rate;
		}
		else {
			result.guideScale = this.guideScale * rate;
		}
		// guideColor
		if (data.guide_color.HasValue) {
			result.orderGuideColor = (GuideColor)data.guide_color.Value;
		}
		else {
			result.orderGuideColor = null;
		}
		return result;
	}
	
	public Document[] Scrutinize (Note.Slide data) {
		List<Document> resultSlides = new List<Document> ();
		ScrutinyDocument.ISlideHead resultHead = null;
		if (data.head != null) {
			if (data.head.GetType() == Note.Type.TAP) {
				Note.Tap head = (Note.Tap)data.head;
				resultHead = (ScrutinyDocument.ISlideHead)Scrutinize(head);
				if (starToRing) {
					resultHead.guideShapeType = Document.Tap.GuideRingShapeType.CIRCLE;
				}
				else if (head.shape_type.HasValue && !ringToStar)  {
					resultHead.guideShapeType = (Document.Tap.GuideRingShapeType)head.shape_type.Value;
				}
				else {
					Document.Tap.GuideRingShapeType shape;
					if (data.patterns.Length > 1) {
						shape = Document.Tap.GuideRingShapeType.WSTAR;
					}
					else {
						shape = Document.Tap.GuideRingShapeType.STAR;
					}
					resultHead.guideShapeType = shape;
				}
			}
			else if (data.head.GetType() == Note.Type.BREAK) {
				Note.Break head = (Note.Break)data.head;
				resultHead = (ScrutinyDocument.ISlideHead)Scrutinize(head);
				if (starToRing) {
					resultHead.guideShapeType = Document.Tap.GuideRingShapeType.CIRCLE;
				}
				else if (head.shape_type.HasValue && !ringToStar)  {
					resultHead.guideShapeType = (Document.Break.GuideRingShapeType)head.shape_type.Value;
				}
				else {
					Document.Break.GuideRingShapeType shape;
					if (data.patterns.Length > 1) {
						shape = Document.Break.GuideRingShapeType.WSTAR;
					}
					else {
						shape = Document.Break.GuideRingShapeType.STAR;
					}
					resultHead.guideShapeType = shape;
				}
			}
		}

		ScrutinyDocument.Slide resultSlide;
		ScrutinyDocument.Slide.Pattern resultPattern;
		List<ScrutinyDocument.Slide.Chain> resultChains;
		List<ScrutinyDocument.Slide.Section> resultSections;
		List<ScrutinyDocument.Slide.Marker> resultMarkers;
		List<ScrutinyDocument.Slide.Command> resultCommands;
		List<ScrutinyDocument.Slide.EvaluateTexturePosition> resultEvaluateTexturePositions = null;
		float headSpinSpeed = 0;
		foreach (var pattern in data.patterns) {
			resultSlide = new ScrutinyDocument.Slide();
			if (data.secret.HasValue) {
				resultSlide.secret = data.secret.Value;
			}
			else {
				resultSlide.secret = false;
			}
			resultPattern = new ScrutinyDocument.Slide.Pattern();
			if (this.turn > 0)
				pattern.Turn (this.turn);
			if (this.mirror)
				pattern.MirrorHolizontal ();
			if (pattern.time.HasValue) {
				resultPattern.visualizeTime = (pattern.time.Value * 1000.0f).ToLong();
			}
			else {
				resultPattern.visualizeTime = GetTimeMS ();
			}
			decimal waitTimeRange = 0;
			{
				decimal subTime;
				Step step = pattern.wait_step;
				if (step == null) step = Step.CreateSlideWaitDefault();
				decimal? tempSubTime = StepConvert (step);
				if (tempSubTime.HasValue) subTime = tempSubTime.Value;
				else return null;
				waitTimeRange = subTime;
				decimal leaveTime;
				if (pattern.time.HasValue) {
					leaveTime = (decimal)(pattern.time.Value) + subTime;
				}
				else {
					leaveTime = GetTime () + subTime;
				}
				long leaveTimeMS = (long)(leaveTime * 1000.0M);
				resultPattern.moveStartTime = leaveTimeMS;
			}
			decimal moveTimeRange = 0;
			{
				decimal subTime;
				decimal? tempSubTime = StepConvert (pattern.move_step);
				if (tempSubTime.HasValue) subTime = tempSubTime.Value;
				else return null;
				moveTimeRange = subTime;
				
				decimal leaveTime;
				if (pattern.time.HasValue) {
					leaveTime = (decimal)(pattern.time.Value) + waitTimeRange + subTime;
				}
				else {
					leaveTime = GetTime () + waitTimeRange + subTime;
				}
				long leaveTimeMS = (long)(leaveTime * 1000.0M);
				subTimeMaxValue = MathlMax (subTimeMaxValue, leaveTimeMS);
				resultPattern.moveEndTime = leaveTimeMS;
			}
			if (pattern.secret.HasValue) {
				resultPattern.secret = pattern.secret.Value;
			}
			else {
				resultPattern.secret = false;
			}
			float rate = pattern.fade_speed_rate ?? 1f;
			if (pattern.fade_speed.HasValue) {
				resultPattern.fadeSpeed = (pattern.fade_speed.Value * rate * 1000.0f).ToLong();
			}
			else {
				resultPattern.fadeSpeed = ((this.guideFadeSpeed + this.guideMoveSpeed) * rate * 1000.0f).ToLong();
			}
			rate = pattern.marker_scale_rate ?? 1f;
			if (pattern.marker_scale.HasValue) {
				resultPattern.markerScale = pattern.marker_scale.Value * rate;
			}
			else {
				resultPattern.markerScale = this.markerScale * rate;
			}
			if (resultHead != null) {
				resultPattern.guideScale = resultHead.guideScale;
			}
			else {
				resultPattern.guideScale = this.guideScale;
			}
			// guideColor
			if (pattern.guide_color.HasValue) {
				resultPattern.orderMarkerColor = (GuideColor)pattern.guide_color.Value;
			}
			else {
				resultPattern.orderMarkerColor = null;
			}

			if (pattern.chains == null || pattern.chains.Length == 0) continue;
			resultChains = new List<Document.Slide.Chain>();
			Document.Slide.Chain resultChain;
			float markerSize = Constants.instance.SLIDE_MARKER_TO_MARKER_INTERVAL * resultPattern.markerScale;
			float margin = Constants.instance.SLIDE_MARKER_TO_SENSOR_MARGIN;
			float wideSlideMargin = margin * 1.5f;
			int evTexWidth = Constants.instance.SLIDE_EVALUATE_TEXTURE_WIDTH;
			var targetPositions = new List<Vector2>();
			bool wideSlide = pattern.chains.Length > 1;
			// wスライドかそうでないかでマーカーの書き方は異なる.
			foreach (var chain in pattern.chains) {
				if (chain.commands == null || chain.commands.Length == 0) continue;
				else {
					resultSections = new List<Document.Slide.Section>();
					resultMarkers = new List<Document.Slide.Marker>();
					resultCommands = new List<Document.Slide.Command>();
					resultEvaluateTexturePositions = new List<Document.Slide.EvaluateTexturePosition>();
					resultChain = new Document.Slide.Chain();

					bool guideStartMarkerPositioning;
					if (chain.guide_start_marker_positioning.HasValue) {
						guideStartMarkerPositioning = chain.guide_start_marker_positioning.Value;
					}
					else {
						guideStartMarkerPositioning = false;
					}
					if (chain.is_surface.HasValue) {
						resultChain.isSurface = chain.is_surface.Value;
					}
					else {
						resultChain.isSurface = false;
					}

					// マーカーの作成.
					Document.Slide.Marker resultMarker;
					Document.Slide.EvaluateTexturePosition resultEvTexPos;
					float nowDistance = margin; // 現在の距離.
					float totalDistance = 0; // チェインの始点から現在のコマンドの終点までの距離.
					float chainDistance = 0; // コマンド全体の合計距離.
					float guideTotalDistance = 0; // 星の合計移動距離.
					Vector2 chainStartPos = Vector2.zero; //チェインの始点の位置.
					Vector2 chainTargetPos = Vector2.zero; //チェインの終点の位置.
					int count = 0;
					int lastCount = chain.commands.Length;
					foreach (var command in chain.commands) {
						count++;
						if (command.GetType() == Note.Slide.Command.Type.STRAIGHT) {
							var straight = (Note.Slide.Command.Straight)command;
							var start = straight.start * Constants.instance.MAIMAI_OUTER_RADIUS;
							var target = straight.target * Constants.instance.MAIMAI_OUTER_RADIUS;
							var deg = CircleCalculator.PointToDegree(start, target);
							var distance = CircleCalculator.PointToPointDistance(start, target);
							chainDistance += distance;
							Vector2 guideStart;
							if (count == 1 && guideStartMarkerPositioning) {
								guideStart = CircleCalculator.PointOnCircle(start, wideSlideMargin - Constants.instance.WIDE_SLIDE_MARKER_TEXTURE_SIZE * 0.5f, deg);
							}
							else {
								guideStart = start;
							}
							var guideDistance = CircleCalculator.PointToPointDistance(guideStart, target);
							float guidePrevTotalDistance = guideTotalDistance;
							guideTotalDistance += guideDistance;
							if (count == 1) {
								chainStartPos = start;
							}
							if (count == lastCount) {
								chainTargetPos = target;
								distance -= margin; // 終点マージン.
								if (distance < 0) distance = 0;

								if (!wideSlide) {
									// スライド評価テクスチャの位置情報.
									var evTarget = CircleCalculator.PointOnCircle (target, margin, deg + 180);
									var evStart = CircleCalculator.PointOnCircle (evTarget, evTexWidth, deg + 180);
									bool positive = true;
									float evTexRotY = deg + 90.0f;
									long temp = (long)((decimal)straight.target.x * 1000M) - (long)((decimal)straight.start.x * 1000M); //始点から終点の横移動量によって画像の上角度が決まる.
									if (temp < 0) {
										evTexRotY = deg - 90.0f;
										positive = false;
									}
									else if (temp == 0) { //始点と終点で横位置が同じなら.
										if (target.y > 0) { //下向きのとき
											positive = target.x > 0;
											evTexRotY = deg + 90.0f * (positive ? 1 : -1); //終点が中心より右にあるなら回転度は向き+90度、左にあるなら向き-90度.
										}
										else if (target.y < 0) { //上向きのとき
											positive = -target.x > 0;
											evTexRotY = deg + 90.0f * (positive ? 1 : -1); //終点が中心より左にあるなら回転度は向き-90度、右にあるなら向き+90度.
										}
									}
									for (int ep = 0; ep < evTexWidth; ep++) {
										resultEvTexPos = new Document.Slide.EvaluateTexturePosition();
										resultEvTexPos.position = CircleCalculator.PointToPointMovePoint(evStart, evTarget, ep);
										resultEvTexPos.rotation = new Vector2(deg, evTexRotY);
										resultEvTexPos.isPositive = positive;
										resultEvaluateTexturePositions.Add (resultEvTexPos);
									}
								}
							}
							float commandProgress = nowDistance - totalDistance; // コマンド間の現在の距離. 前回オーバーした分を考慮する.
							totalDistance += distance;
							while (totalDistance >= nowDistance) {
								resultMarker = new Document.Slide.Marker();
								resultMarker.position = CircleCalculator.PointToPointMovePoint(start, target, commandProgress);
								resultMarker.degree = deg;
								resultMarker.distanceFromStart = nowDistance;
								resultMarkers.Add(resultMarker);
								commandProgress += markerSize;
								nowDistance += markerSize;
							}
							if (resultMarkers.Count == 0) {
								resultMarker = new Document.Slide.Marker();
								resultMarker.position = CircleCalculator.PointToPointMovePoint(start, target, 0);
								resultMarker.degree = deg;
								resultMarker.distanceFromStart = nowDistance;
								resultMarkers.Add(resultMarker);
							}
							// 星の動きコマンド生成.
							var resultCommand = new Document.Slide.Command.Straight();
							resultCommand.start = guideStart;
							resultCommand.target = target;
							resultCommand.totalDistanceToPreviousCommand = guidePrevTotalDistance;
							resultCommand.distance = guideDistance;
							resultCommands.Add(resultCommand);
						}
						else if (command.GetType() == Note.Slide.Command.Type.CURVE) {
							var curve = (Note.Slide.Command.Curve)command;
							var axis = curve.axis * Constants.instance.MAIMAI_OUTER_RADIUS;
							var radius = curve.radius * Constants.instance.MAIMAI_OUTER_RADIUS;
							var distance = CircleCalculator.ArcDistance(radius, Mathf.Abs(curve.degOfDistance));
							int vec = curve.degOfDistance < 0 ? -1 : 1;
							chainDistance += distance;
							float guideDegOfStart;
							float guideDegOfDistance;
							if (count == 1 && guideStartMarkerPositioning) {
								float sub = (wideSlideMargin - Constants.instance.WIDE_SLIDE_MARKER_TEXTURE_SIZE * 0.5f) * vec;
								float subdistance = CircleCalculator.ArcDegreeFromArcDistance(radius, sub);
								guideDegOfStart = curve.degOfStart + subdistance;
								guideDegOfDistance = curve.degOfDistance - subdistance;
							}
							else {
								guideDegOfStart = curve.degOfStart;
								guideDegOfDistance = curve.degOfDistance;
							}
							var guideDistance = CircleCalculator.ArcDistance(radius, Mathf.Abs(guideDegOfDistance));
							float guidePrevTotalDistance = guideTotalDistance;
							guideTotalDistance += guideDistance;
							if (count == 1) {
								chainStartPos = CircleCalculator.PointOnCircle(axis, radius, curve.degOfStart);
							}
							if (count == lastCount) {
								chainTargetPos = CircleCalculator.PointOnCircle(axis, radius, curve.degOfStart + curve.degOfDistance);
								distance -= margin; // 終点マージン.
								if (distance < 0) distance = 0;

								if (!wideSlide) {
									// スライド評価テクスチャの位置情報.
									var evDistance = CircleCalculator.ArcDistance(radius, Mathf.Abs(curve.degOfDistance)) - margin;
									var evMarginDegOfDistance = CircleCalculator.ArcDegreeFromArcDistance (radius, evDistance);
									var evDegOfDistance = CircleCalculator.ArcDegreeFromArcDistance (radius, evTexWidth);
									var evDegOfStart = curve.degOfStart + (evMarginDegOfDistance - evDegOfDistance) * vec;
									var evDegOfTarget = evDegOfStart + evDegOfDistance * vec;

									float elm = CircleCalculator.PointToDegree(axis, CircleCalculator.PointOnCircle(axis, radius, evDegOfTarget));
									bool negative;
									if (vec > 0) negative = 90.0f + 22.5f <= elm && elm < 270.0f;
									else negative = !(90.0f <= elm && elm < 270.0f - 22.5f);
									float evTexRotYRate = negative ? -1 : 1; //下半分ならマイナス方向, 上半分ならプラス方向.
									for (int ep = 0; ep < evTexWidth; ep++) {
										resultEvTexPos = new Document.Slide.EvaluateTexturePosition();
										var epDeg = evDegOfStart + CircleCalculator.ArcDegreeFromArcDistance(radius, ep) * vec;
										resultEvTexPos.position = CircleCalculator.PointOnCircle(axis, radius, epDeg);
										resultEvTexPos.rotation = new Vector2(epDeg + 90.0f * vec, epDeg + 90.0f * vec + 90.0f * evTexRotYRate);
										resultEvTexPos.isPositive = !negative;
										resultEvaluateTexturePositions.Add (resultEvTexPos);
									}
								}
							}
							float commandProgress = nowDistance - totalDistance;
							totalDistance += distance;
							while (totalDistance >= nowDistance) {
								resultMarker = new Document.Slide.Marker();
								var pDeg = CircleCalculator.ArcDegreeFromArcDistance(radius, commandProgress);
								resultMarker.position = CircleCalculator.PointOnCircle(axis, radius, curve.degOfStart + pDeg * vec);
								resultMarker.degree = curve.degOfStart + 90.0f + pDeg * vec + (vec > 0 ? 0 : 180);
								resultMarker.distanceFromStart = nowDistance;
								resultMarkers.Add(resultMarker);
								commandProgress += markerSize;
								nowDistance += markerSize;
							}
							if (resultMarkers.Count == 0) {
								resultMarker = new Document.Slide.Marker();
								var pDeg = 0f;
								resultMarker.position = CircleCalculator.PointOnCircle(axis, radius, curve.degOfStart + pDeg * vec);
								resultMarker.degree = curve.degOfStart + 90.0f + pDeg * vec + (vec > 0 ? 0 : 180);
								resultMarker.distanceFromStart = nowDistance;
								resultMarkers.Add(resultMarker);
							}
							// 星の動きコマンド生成.
							var resultCommand = new Document.Slide.Command.Curve();
							resultCommand.axis = axis;
							resultCommand.radius = radius;
							resultCommand.degOfStart = guideDegOfStart;
							resultCommand.degOfDistance = guideDegOfDistance;
							resultCommand.totalDistanceToPreviousCommand = guidePrevTotalDistance;
							resultCommand.distance = guideDistance;
							resultCommands.Add(resultCommand);
						}
						else continue;
					}

					// チェインが複数あるとき、スライドの描き方を変える.
					if (wideSlide) {
						resultMarkers.Clear();
						nowDistance = wideSlideMargin;
						totalDistance = 0;
						count = 0;
						int divCount = 0;
						int divAmount = Constants.instance.WIDE_SLIDE_DEVIDE_AMOUNT;
						foreach (var command in chain.commands) {
							count++;
							if (command.GetType() == Note.Slide.Command.Type.STRAIGHT) {
								var straight = (Note.Slide.Command.Straight)command;
								var start = straight.start * Constants.instance.MAIMAI_OUTER_RADIUS;
								var target = straight.target * Constants.instance.MAIMAI_OUTER_RADIUS;
								var deg = CircleCalculator.PointToDegree(start, target);
								var distance = CircleCalculator.PointToPointDistance(start, target);
								if (count == lastCount) {
									distance -= margin; // 終点マージン.
									if (distance < 0) distance = 0;
								}
								float sub = nowDistance - totalDistance;
								float commandProgress = sub; // コマンド間の現在の距離. 前回オーバーした分を考慮する.
								totalDistance += distance;
								float inverseDivAmount = 1f / (float)divAmount;
								float progress = (chainDistance - margin) * inverseDivAmount;
								for (divCount = 0; divCount < divAmount; divCount++) {
									resultMarker = new Document.Slide.Marker();
									resultMarker.position = CircleCalculator.PointToPointMovePoint(start, target, commandProgress);
									resultMarker.degree = deg;
									resultMarker.distanceFromStart = nowDistance;
									resultMarkers.Add(resultMarker);
									commandProgress = (chainDistance - margin) * ((float)(divCount + 1) * inverseDivAmount) + sub;
									nowDistance += progress;
								}
								if (resultMarkers.Count == 0) {
									resultMarker = new Document.Slide.Marker();
									resultMarker.position = CircleCalculator.PointToPointMovePoint(start, target, commandProgress);
									resultMarker.degree = deg;
									resultMarker.distanceFromStart = nowDistance;
									resultMarkers.Add(resultMarker);
								}
							}
							else if (command.GetType() == Note.Slide.Command.Type.CURVE) {
								var curve = (Note.Slide.Command.Curve)command;
								var axis = curve.axis * Constants.instance.MAIMAI_OUTER_RADIUS;
								var radius = curve.radius * Constants.instance.MAIMAI_OUTER_RADIUS;
								var distance = CircleCalculator.ArcDistance(radius, Mathf.Abs(curve.degOfDistance));
								int vec = curve.degOfDistance < 0 ? -1 : 1;
								if (count == lastCount) {
									distance -= margin; // 終点マージン.
									if (distance < 0) distance = 0;
								}
								float sub = nowDistance - totalDistance;
								float commandProgress = sub;
								totalDistance += distance;
								float inverseDivAmount = 1f / (float)divAmount;
								float progress = (chainDistance - margin) * inverseDivAmount;
								for (divCount = 0; divCount < divAmount; divCount++) {
									resultMarker = new Document.Slide.Marker();
									var pDeg = CircleCalculator.ArcDegreeFromArcDistance(radius, commandProgress);
									resultMarker.position = CircleCalculator.PointOnCircle(axis, radius, curve.degOfStart + pDeg * vec);
									resultMarker.degree = curve.degOfStart + 90.0f + pDeg * vec + (vec > 0 ? 0 : 180);
									resultMarker.distanceFromStart = nowDistance;
									resultMarkers.Add(resultMarker);
									commandProgress = (chainDistance - margin) * ((float)(divCount + 1) * inverseDivAmount) + sub;
									nowDistance += progress;
								}
								if (resultMarkers.Count == 0) {
									resultMarker = new Document.Slide.Marker();
									var pDeg = 0f;
									resultMarker.position = CircleCalculator.PointOnCircle(axis, radius, curve.degOfStart + pDeg * vec);
									resultMarker.degree = curve.degOfStart + 90.0f + pDeg * vec + (vec > 0 ? 0 : 180);
									resultMarker.distanceFromStart = nowDistance;
									resultMarkers.Add(resultMarker);
								}
							}
							else continue;
						}
					}

					// 星の回転スピードを求める.
					float onStarMoveSpeed = moveTimeRange > 0 ? (float)((decimal)chainDistance / moveTimeRange) : 0;
					// patternの中で一番速度が速いのにする.
					headSpinSpeed = Mathf.Max(headSpinSpeed, onStarMoveSpeed);

					// セクションの作成.
					Document.Slide.Section resultSection = null;
					if (chain.sections != null && chain.sections.Length > 0) {
						int? beforeSection = null;
						foreach (var section in chain.sections) {
							resultSection = new Document.Slide.Section();
							if (beforeSection.HasValue && beforeSection.Value == section) 
								continue;
							if (section >= 0 && section <= 16) {
								resultSection.sensor = (Constants.SensorId)section;
								beforeSection = section;
								resultSections.Add (resultSection);
							}
							else return null;
						}
					}
					else {
						List<Vector2> sectionSearchPos = null;
						if (resultMarkers.Count > 0) {
							sectionSearchPos = new List<Vector2>();
							sectionSearchPos.Add (chainStartPos);
							foreach (var marker in resultMarkers) {
								sectionSearchPos.Add (marker.position);
							}
							sectionSearchPos.Add (chainTargetPos);
						}
						if (sectionSearchPos != null) {
							foreach (var searchPos in sectionSearchPos) {
								// 一番近いセンサーを探す.
								float nearestDistance = float.PositiveInfinity;
								Constants.SensorId sensor = Constants.SensorId.CENTER;
								// 中央.
								{
									var sensorPos = Vector2.zero;
									float toSensorDistance = Vector2.Distance (searchPos, sensorPos);
									if (toSensorDistance < nearestDistance) {
										nearestDistance = toSensorDistance;
									}
								}
								// 内周.
								for (int i = 0; i < 8; i++) {
									var sensorPos = Constants.instance.GetInnerPieceAxis(i);
									float toSensorDistance = Vector2.Distance (searchPos, sensorPos);
									if (toSensorDistance < nearestDistance) {
										sensor = Constants.InnerSensorIds[i];
										nearestDistance = toSensorDistance;
									}
								}
								// 外周.
								for (int i = 0; i < 8; i++) {
									var sensorPos = Constants.instance.GetOuterPieceAxis(i);
									float toSensorDistance = Vector2.Distance (searchPos, sensorPos);
									if (toSensorDistance < nearestDistance) {
										sensor = Constants.OuterSensorIds[i];
										nearestDistance = toSensorDistance;
									}
								}
								
								if (resultSections.Count == 0 || resultSections[resultSections.Count - 1].sensor != sensor) {
									resultSection = new Document.Slide.Section();
									resultSection.sensor = sensor;
									resultSections.Add (resultSection);
								}
							}
						}
					}

					// justTimeは星が終点にたどり着く時間ではなく、星がセンサーに入る時間にする.
					// justTimeを求めるにはマーカーとセクションの情報が必要.
					// あんたセンサーに侵入してどのくらいの距離を進んだんだい？.
					// case1:センサーを若干突き抜けている可能性.
					// case2:センサーの中で止まる可能性.
					// case3:センサーまで若干届いてない可能性.
					// case4:そもそもずっとセンサーの中にいる可能性.
					// case5:最初はセンサーの中にいたけど飛び出た可能性.
					// case4と5なら、前のコマンドも参照してみる.それでも変わらなければそのcaseで確定.justTimeはvisibleTimeと同等.
					// case1は交点が2つある.コマンドの終点に近い点を使い、チェインの始点からその点までの距離をonStarMoveSpeedの速さで進んで侵入した時間がjustTime.
					// case2は交点が1つある.チェインの始点からその点までの距離をonStarMoveSpeedの速さで進んで侵入した時間がjustTime.
					// case3は交点が0であるので、☆は絶対にセンサー内にたどり着かない.そのためjustTimeはmoveEndTimeと同等にするしかない.
					// 
					if (resultSections.Count > 0) {
						Vector2 lastSensorAxis = Constants.instance.GetSensorAxisFromId(resultSections[resultSections.Count - 1].sensor);
						float sensorRadius = Constants.instance.SENSOR_RADIUS;
						int resultCommandsCount = resultCommands.Count;
						int mode = 0;
						float distance = 0;
						for (int i = 0; i < resultCommandsCount; i++) {
							var command = resultCommands[resultCommandsCount - 1 - i];
							if (command.type == Document.Slide.Command.Type.STRAIGHT) {
								var straight = (Document.Slide.Command.Straight)command;
								if (CircleCalculator.CircleIntersect(lastSensorAxis, sensorRadius, straight.start, 0.0f)) {
									// コマンドの始点がセンサー内である(case4か5)なので、そのコマンドの距離を記録して、前のコマンドを見る.
									distance += CircleCalculator.PointToPointDistance(straight.start, straight.target);
									continue;
								}
								else {
									// コマンドの始点から終点に引いた直線と、センサーの交点を求める.
									var intersectPoints = CircleCalculator.LineAndCircleIntersectPoints(lastSensorAxis, sensorRadius, straight.start, straight.target);
									if (intersectPoints.Length == 2) {
										// 交点が2点ならcase1. 始点に近いほうを使う.
										int index = Vector2.Distance(straight.start, intersectPoints[0]) < Vector2.Distance(straight.start, intersectPoints[1]) ? 0 : 1;
										Vector2 intersectPoint = intersectPoints[index];
										distance += Vector2.Distance(intersectPoint, straight.target);
										mode = 1;
										break;
									}
									else if (intersectPoints.Length == 1) {
										// 交点が1点ならcase2.
										Vector2 intersectPoint = intersectPoints[0];
										distance += Vector2.Distance(intersectPoint, straight.target);
										mode = 1;
										break;
									}
									else {
										// 交点が無いならcase3.
										mode = 2;
										break;
									}
								}
							}
							else if (command.type == Document.Slide.Command.Type.CURVE) {
								var curve = (Document.Slide.Command.Curve)command;
								var start = CircleCalculator.PointOnCircle(curve.axis, curve.radius, curve.degOfStart);
								if (CircleCalculator.CircleIntersect(lastSensorAxis, sensorRadius, start, 0.0f)) {
									// コマンドの始点がセンサー内である(case4か5)なので、そのコマンドの距離を記録して、前のコマンドを見る.
									distance += CircleCalculator.ArcDistance(curve.radius, Mathf.Abs(curve.degOfDistance));
									continue;
								}
								else {
									// コマンドの円弧と、センサーの交点を求める.
									var intersectPoints = CircleCalculator.CircleAndArcIntersectPoints(lastSensorAxis, sensorRadius, curve.axis, curve.radius, curve.degOfStart, curve.degOfDistance);
									if (intersectPoints.Length == 2) {
										// 交点が2点ならcase1. 始点に近いほうを使う.
										int index = Vector2.Distance(start, intersectPoints[0]) < Vector2.Distance(start, intersectPoints[1]) ? 0 : 1;
										Vector2 intersectPoint = intersectPoints[index];
										float ipDeg = CircleCalculator.PointToDegree(curve.axis, intersectPoint);
										distance += CircleCalculator.ArcDistance(curve.radius, Mathf.Abs(curve.degOfDistance) - Mathf.Abs(ipDeg - curve.degOfStart));
										mode = 1;
										break;
									}
									else if (intersectPoints.Length == 1) {
										// 交点が1点ならcase2.
										Vector2 intersectPoint = intersectPoints[0];
										float ipDeg = CircleCalculator.PointToDegree(curve.axis, intersectPoint);
										distance += CircleCalculator.ArcDistance(curve.radius, Mathf.Abs(curve.degOfDistance) - Mathf.Abs(ipDeg - curve.degOfStart));
										mode = 1;
										break;
									}
									else {
										// 交点が無いならcase3.
										mode = 2;
										break;
									}
								}
							}
						}
						if (mode == 0) {
							// 星がセンサー内から絶対出ない場合.
							resultPattern.justTime = resultPattern.visualizeTime;
						}
						else if (mode == 1) {
							// 星がセンサーに突入する時間を入れる.理想形.
							float invadeSensorDistance = chainDistance - distance;
							if (onStarMoveSpeed > 0)
								resultPattern.justTime = resultPattern.moveStartTime + (invadeSensorDistance / onStarMoveSpeed * 1000.0f).ToLong();
							// 瞬間移動する(EndTime-StartTime==0)なら再計算できないのでEndTimeと同時刻にする.
							else
								resultPattern.justTime = resultPattern.moveEndTime;
						}
						else {
							// 星がセンサー内に突入することはない場合.
							resultPattern.justTime = resultPattern.moveEndTime;
						}
					}

					targetPositions.Add (chainTargetPos);
					resultChain.markers = resultMarkers.ToArray();
					resultChain.sections = resultSections.ToArray();
					resultChain.commands = resultCommands.ToArray();
					resultChain.totalDistance = chainDistance;
					resultChain.guideTotalDistance = guideTotalDistance;
					resultChains.Add(resultChain);
				}

			}

			// ワイドスライドの評価テクスチャの位置情報
			if (wideSlide) {
				if (resultEvaluateTexturePositions == null)
					resultEvaluateTexturePositions = new List<Document.Slide.EvaluateTexturePosition>();
				resultEvaluateTexturePositions.Clear();

				Document.Slide.EvaluateTexturePosition resultEvTexPos;
				int tplen = targetPositions.Count;
				float texCircleRadius = Constants.instance.MAIMAI_OUTER_RADIUS;
				float texProgressGoalTotal = 0;
				int resultEvTexPosAddCount = 0;
				for (int i = 0; i < tplen - 1; i++) {
					float progressGoal = (float)(i + 1) / (float)(tplen - 1);
					float texProgressGoal = (float)evTexWidth * progressGoal;
					var nowPos = targetPositions[i];
					var nextPos = targetPositions[i + 1];
					var nowPosToNextPosDeg = CircleCalculator.PointToDegree(nowPos, nextPos);
					var nowPosAndNextPosCenterPos = Vector2.Lerp(nowPos, nextPos, 0.5f);
					float nowPosToNextPosVec = CircleCalculator.TwoVectorsVerticalLineDegree (nowPos, nextPos);
					int vec = nowPosToNextPosVec == 0 ? 0 : nowPosToNextPosVec > 1 ? 1 : -1;
					var texCircleCenterPos = CircleCalculator.PointOnCircle(nowPosAndNextPosCenterPos, texCircleRadius, nowPosToNextPosDeg + 90.0f * vec); 
					var nowPosToNextPosVector = nextPos - nowPos;
					var texCircleCenterPosToNowPosVector = nowPos - texCircleCenterPos;
					var sinDeg = CircleCalculator.TwoVectorsVerticalLineDegree (texCircleCenterPosToNowPosVector, nowPosToNextPosVector);
					var texCircleCenterPosToNowPosDeg = CircleCalculator.PointToDegree(texCircleCenterPos, nowPos);
					var texCircleCenterPosToNextPosDeg = CircleCalculator.PointToDegree(texCircleCenterPos, nextPos);
					float degOfDistance = 0;
					if (sinDeg > 0) { //時計回り
						degOfDistance = CircleCalculator.DegToDegClockwiseDistanceDeg (texCircleCenterPosToNowPosDeg, texCircleCenterPosToNextPosDeg);
					}
					else if (sinDeg < 0) { //反時計回り
						degOfDistance = CircleCalculator.DegToDegCounterClockwiseDistanceDeg (texCircleCenterPosToNowPosDeg, texCircleCenterPosToNextPosDeg);
					}
					degOfDistance = Mathf.Abs(degOfDistance);
					int evTexRangeIndex = 0;
					for (int evTexIndex = resultEvTexPosAddCount; evTexIndex < texProgressGoal; evTexIndex++) {
						if (evTexIndex >= evTexWidth)
							break;
						float evProgress = (float)evTexRangeIndex / (texProgressGoal - texProgressGoalTotal);
						if (evTexIndex == evTexWidth - 1) evProgress = 1;
						float progress = evProgress * degOfDistance;
						resultEvTexPos = new Document.Slide.EvaluateTexturePosition();
						var epDeg = texCircleCenterPosToNowPosDeg + progress * vec;
						resultEvTexPos.position = CircleCalculator.PointOnCircle(texCircleCenterPos, texCircleRadius, epDeg);
						resultEvTexPos.rotation = new Vector2(epDeg + 90.0f * vec, epDeg);
						resultEvTexPos.isPositive = vec <= 0;
						resultEvTexPos.isWideSlide = true;
						resultEvaluateTexturePositions.Add (resultEvTexPos);
						resultEvTexPosAddCount++;
						evTexRangeIndex++;
					}
					texProgressGoalTotal = texProgressGoal;
				}

			}

			resultPattern.evaluateTexturePositions = resultEvaluateTexturePositions.ToArray();
			resultPattern.chains = resultChains.ToArray();
			resultSlide.pattern = resultPattern;
			resultSlides.Add (resultSlide);
		}
		
		var result = new List<Document> ();
		if (resultHead != null) {
			if (starRotation) {
				resultHead.guideSpinSpeed = headSpinSpeed;
			}
			else {
				resultHead.guideSpinSpeed = 0;
			}
			result.Add ((Document)resultHead);
		}
		result.AddRange (resultSlides);
		return result.ToArray();
	}

	public Document Scrutinize (Note.Message data) {
		var result = new Document.Message ();
		if (data.message == null) {
			return null;
		}
		else {
			result.message = data.message;
		}
		result.position = data.position;
		result.scale = data.scale;
		result.color = data.color;
		if (data.time.HasValue) {
			result.viewStartTime = (data.time.Value * 1000.0f).ToLong();
		}
		else {
			result.viewStartTime = GetTimeMS ();
		}
		{
			decimal subTime;
			decimal? tempSubTime = StepConvert (data.step);
			if (tempSubTime.HasValue) subTime = tempSubTime.Value;
			else return null;
			decimal leaveTime = GetTime () + subTime;
			long leaveTimeMS = (long)(leaveTime * 1000.0M);
			subTimeMaxValue = MathlMax (subTimeMaxValue, leaveTimeMS);
			result.viewEndTime = leaveTimeMS;
		}
		if (data.secret.HasValue) {
			result.secret = data.secret.Value;
		}
		else {
			result.secret = false;
		}
		if (data.alignment.HasValue) {
			result.alignment = (Alignment)data.alignment.Value;
		}
		else {
			result.alignment = Alignment.LEFT;
		}
		return result;
	}
	
	public Document Scrutinize (Note.ScrollMessage data) {
		var result = new Document.ScrollMessage ();
		if (data.message == null) {
			return null;
		}
		else {
			result.message = data.message;
		}
		result.y = data.y;
		result.scale = data.scale;
		result.color = data.color;
		if (data.time.HasValue) {
			result.viewStartTime = (data.time.Value * 1000.0f).ToLong();
		}
		else {
			result.viewStartTime = GetTimeMS ();
		}
		{
			decimal subTime;
			decimal? tempSubTime = StepConvert (data.step);
			if (tempSubTime.HasValue) subTime = tempSubTime.Value;
			else return null;
			decimal leaveTime = GetTime () + subTime;
			long leaveTimeMS = (long)(leaveTime * 1000.0M);
			subTimeMaxValue = MathlMax (subTimeMaxValue, leaveTimeMS);
			result.viewEndTime = leaveTimeMS;
		}
		if (data.secret.HasValue) {
			result.secret = data.secret.Value;
		}
		else {
			result.secret = false;
		}
		if (data.alignment.HasValue) {
			result.alignment = (Alignment)data.alignment.Value;
		}
		else {
			result.alignment = Alignment.LEFT;
		}
		return result;
	}
	
	public Document Scrutinize (Note.SoundMessage data) {
		var result = new Document.SoundMessage ();
		result.soundId = data.soundId;
		if (data.time.HasValue) {
			result.playStartTime = (data.time.Value * 1000.0f).ToLong();
		}
		else {
			result.playStartTime = GetTimeMS ();
		}
		if (data.secret.HasValue) {
			result.secret = data.secret.Value;
		}
		else {
			result.secret = false;
		}
		return result;
	}
	
	public Document Scrutinize (Note.Trap data) {
		var result = new Document.Trap();
		// sensor
		if (data.sensor >= 0 && data.sensor <= 16) {
			if (this.turn > 0) {
				data.Turn (this.turn);
			}
			if (this.mirror) {
				data.MirrorHolizontal ();
			}
			result.sensor = (Constants.SensorId)data.sensor;
		}
		else return null;
		// headTime
		if (data.time.HasValue) {
			result.headTime = (data.time.Value * 1000.0f).ToLong();
		}
		else {
			result.headTime = GetTimeMS ();
		}
		// footTime
		{
			decimal subTime;
			decimal? tempSubTime = StepConvert (data.step);
			if (tempSubTime.HasValue) subTime = tempSubTime.Value;
			else return null;
			decimal leaveTime;
			if (data.time.HasValue) {
				leaveTime = (decimal)(data.time.Value) + subTime;
			}
			else {
				leaveTime = GetTime () + subTime;
			}
			long leaveTimeMS = (long)(leaveTime * 1000.0M);
			subTimeMaxValue = MathlMax (subTimeMaxValue, leaveTimeMS);
			result.footTime = leaveTimeMS;
		}
		// secret
		if (data.secret.HasValue) {
			result.secret = data.secret.Value;
		}
		else {
			result.secret = false;
		}
		// fadeSpeed
		float rate = data.fade_speed_rate ?? 1f;
		if (data.fade_speed.HasValue) { 
			result.guideFadeSpeed = (data.fade_speed.Value * rate * 1000.0f).ToLong();
		}
		else {
			result.guideFadeSpeed = ((this.guideFadeSpeed + this.guideMoveSpeed) * rate * 1000.0f).ToLong();
		}
		// guideScale
		rate = data.guide_scale_rate ?? 1f;
		if (data.guide_scale.HasValue) {
			result.guideScale = data.guide_scale.Value * trapScaleInverse * rate;
		}
		else {
			result.guideScale = this.trapScale * trapScaleInverse * rate;
		}
		return result;
	}
	
	
	public static long MathlMax (long value1, long value2) {
		if (value2 > value1)
			return value2;
		return value1;
	}

	public long GetFinishTimeMS () {
		return MathlMax (subTimeMaxValue, GetTimeMS ());
	}
	
	private decimal? StepConvert (Step step) {
		if (step == null)
			return null;
		if (step.GetType() == Step.Type.BASIC) {
			if (!m_bpm.HasValue) {
				return null;
			}
			var s = step as Step.Basic;
			return (CalcTime (m_bpm.Value, (decimal)s.beat, (decimal)s.length));
		}
		else if (step.GetType() == Step.Type.INTERVAL) {
			var s = step as Step.Interval;
			return (decimal)s.interval;
		}
		else if (step.GetType() == Step.Type.LOCAL_BPM) {
			var s = step as Step.LocalBpm;
			return (CalcTime ((decimal)s.bpm, (decimal)s.beat, (decimal)s.length));
		}
		else if (step.GetType() == Step.Type.MASS) {
			var s = step as Step.Mass;
			decimal total = 0;
			foreach (var entity in s.steps) {
				var result = StepConvert(entity);
				if (result.HasValue) {
					total += result.Value;
				}
				else {
					return null;
				}
			}
			return total;
		}
		return null;
	}
	
}
