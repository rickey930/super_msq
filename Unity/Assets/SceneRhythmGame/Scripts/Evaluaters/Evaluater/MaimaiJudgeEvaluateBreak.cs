using UnityEngine;
using System.Collections;
using XMaimaiNote.XInspector;
using XMaimaiNote.XEvaluateDesigner;

public class MaimaiJudgeEvaluateBreak : MaimaiJudgeEvaluateCommonMiss {
	public MaimaiJudgeEvaluateBreak(MaimaiJudgeEvaluateManager manager, 
	                                int p2600Rate, int p2550Rate, int p2500Rate, int p2000Rate, int p1500Rate, int p1250Rate, int p1000Rate,
	                                long inP1000FastTime, long inP1250FastTime, long inP1500FastTime, long inP2000FastTime, long inP2500FastTime, long inP2550FastTime, long inP2600Time,
	                                long inP2550LateTime, long inP2500LateTime, long inP2000LateTime, long inP1500LateTime, long inP1250LateTime, long inP1000LateTime) : base(manager) {
		this.p2600 = new MaimaiJudgeEvaluateInfo(p2600Rate);
		this.p2550 = new MaimaiJudgeEvaluateInfo(p2550Rate);
		this.p2500 = new MaimaiJudgeEvaluateInfo(p2500Rate);
		this.p2000 = new MaimaiJudgeEvaluateInfo(p2000Rate);
		this.p1500 = new MaimaiJudgeEvaluateInfo(p1500Rate);
		this.p1250 = new MaimaiJudgeEvaluateInfo(p1250Rate);
		this.p1000 = new MaimaiJudgeEvaluateInfo(p1000Rate);
		this.inP2600Time = Mathf.Abs((float)inP2600Time).ToLong();
		this.inP2550FastTime = Mathf.Abs((float)inP2550FastTime).ToLong();
		this.inP2550LateTime = Mathf.Abs((float)inP2550LateTime).ToLong();
		this.inP2500FastTime = Mathf.Abs((float)inP2500FastTime).ToLong();
		this.inP2500LateTime = Mathf.Abs((float)inP2500LateTime).ToLong();
		this.inP2000FastTime = Mathf.Abs((float)inP2000FastTime).ToLong();
		this.inP2000LateTime = Mathf.Abs((float)inP2000LateTime).ToLong();
		this.inP1500FastTime = Mathf.Abs((float)inP1500FastTime).ToLong();
		this.inP1500LateTime = Mathf.Abs((float)inP1500LateTime).ToLong();
		this.inP1250FastTime = Mathf.Abs((float)inP1250FastTime).ToLong();
		this.inP1250LateTime = Mathf.Abs((float)inP1250LateTime).ToLong();
		this.inP1000FastTime = Mathf.Abs((float)inP1000FastTime).ToLong();
		this.inP1000LateTime = Mathf.Abs((float)inP1000LateTime).ToLong();
	}
	public MaimaiJudgeEvaluateInfo p2600 { get; protected set; }
	public MaimaiJudgeEvaluateInfo p2550 { get; protected set; }
	public MaimaiJudgeEvaluateInfo p2500 { get; protected set; }
	public MaimaiJudgeEvaluateInfo p2000 { get; protected set; }
	public MaimaiJudgeEvaluateInfo p1500 { get; protected set; }
	public MaimaiJudgeEvaluateInfo p1250 { get; protected set; }
	public MaimaiJudgeEvaluateInfo p1000 { get; protected set; }
	public long inP2600Time { get; protected set; }
	public long inP2550FastTime { get; protected set; }
	public long inP2550LateTime { get; protected set; }
	public long inP2500FastTime { get; protected set; }
	public long inP2500LateTime { get; protected set; }
	public long inP2000FastTime { get; protected set; }
	public long inP2000LateTime { get; protected set; }
	public long inP1500FastTime { get; protected set; }
	public long inP1500LateTime { get; protected set; }
	public long inP1250FastTime { get; protected set; }
	public long inP1250LateTime { get; protected set; }
	public long inP1000FastTime { get; protected set; }
	public long inP1000LateTime { get; protected set; }

	public override void Evaluate (long timing, Inspector note) {
		if (timing >= -this.inP1000FastTime && timing < -this.inP1250FastTime) {
			this.P1000FastCallBack(note);
		}
		else if (timing >= -this.inP1250FastTime && timing < -this.inP1500FastTime) {
			this.P1250FastCallBack(note);
		}
		else if (timing >= -this.inP1500FastTime && timing < -this.inP2000FastTime) {
			this.P1500FastCallBack(note);
		}
		else if (timing >= -this.inP2000FastTime && timing < -this.inP2500FastTime) {
			this.P2000FastCallBack(note);
		}
		else if (timing >= -this.inP2500FastTime && timing < -this.inP2550FastTime) {
			this.P2500FastCallBack(note);
		}
		else if (timing >= -this.inP2550FastTime && timing < -this.inP2600Time) {
			this.P2550FastCallBack(note);
		}
		else if (timing >= -this.inP2600Time && timing <= this.inP2600Time) {
			this.P2600CallBack(note);
		}
		else if (timing > this.inP2600Time && timing <= this.inP2550LateTime) {
			this.P2550LateCallBack(note);
		}
		else if (timing > this.inP2550LateTime && timing <= this.inP2500LateTime) {
			this.P2500LateCallBack(note);
		}
		else if (timing > this.inP2500LateTime && timing <= this.inP2000LateTime) {
			this.P2000LateCallBack(note);
		}
		else if (timing > this.inP2000LateTime && timing <= this.inP1500LateTime) {
			this.P1500LateCallBack(note);
		}
		else if (timing > this.inP1500LateTime && timing <= this.inP1250LateTime) {
			this.P1250LateCallBack(note);
		}
		else if (timing > this.inP1250LateTime && timing <= this.inP1000LateTime) {
			this.P1000LateCallBack(note);
		}
	}

	public virtual void P1000FastCallBack(Inspector iNote) {
		var note = (Inspector.Break)iNote;
		manager.PlayNoteSE("se_break_non_perfect");
		note.kernel.designer.Hide ();
		var evaluateType = DrawableEvaluateBreakType.P1000F;
		note.kernel.evaluateTextureDesigner.Show (evaluateType);
		note.kernel.evaluateEffectDesigner.Show (evaluateType);
		this.p1000.count++;
		manager.AddCombo();
		manager.ResetPCombo();
		manager.ResetBPCombo();
		manager.Damage();
		note.judged = true;
		note.kernel.evaluated = true;
		if (note.kernel.eachArchDesigner != null)
			note.kernel.eachArchDesigner.HideIfIndependentState ();
		manager.SyncEvaluation(note.kernel, SyncEvaluate.FASTGOOD);
		manager.SetDrawableCommentNote(note);
	}
	
	public virtual void P1250FastCallBack(Inspector iNote) {
		var note = (Inspector.Break)iNote;
		manager.PlayNoteSE("se_break_non_perfect");
		note.kernel.designer.Hide ();
		var evaluateType = DrawableEvaluateBreakType.P1250F;
		note.kernel.evaluateTextureDesigner.Show (evaluateType);
		note.kernel.evaluateEffectDesigner.Show (evaluateType);
		this.p1250.count++;
		manager.AddCombo();
		manager.ResetPCombo();
		manager.ResetBPCombo();
		manager.Damage();
		note.judged = true;
		note.kernel.evaluated = true;
		if (note.kernel.eachArchDesigner != null)
			note.kernel.eachArchDesigner.HideIfIndependentState ();
		manager.SyncEvaluation(note.kernel, SyncEvaluate.FASTGREAT);
		manager.SetDrawableCommentNote(note);
	}
	
	public virtual void P1500FastCallBack(Inspector iNote) {
		var note = (Inspector.Break)iNote;
		manager.PlayNoteSE("se_break_non_perfect");
		note.kernel.designer.Hide ();
		var evaluateType = DrawableEvaluateBreakType.P1500F;
		note.kernel.evaluateTextureDesigner.Show (evaluateType);
		note.kernel.evaluateEffectDesigner.Show (evaluateType);
		this.p1500.count++;
		manager.AddCombo();
		manager.ResetPCombo();
		manager.ResetBPCombo();
		manager.Damage();
		note.judged = true;
		note.kernel.evaluated = true;
		if (note.kernel.eachArchDesigner != null)
			note.kernel.eachArchDesigner.HideIfIndependentState ();
		manager.SyncEvaluation(note.kernel, SyncEvaluate.FASTGREAT);
		manager.SetDrawableCommentNote(note);
	}
	
	public virtual void P2000FastCallBack(Inspector iNote) {
		var note = (Inspector.Break)iNote;
		manager.PlayNoteSE("se_break_non_perfect");
		note.kernel.designer.Hide ();
		var evaluateType = DrawableEvaluateBreakType.P2000F;
		note.kernel.evaluateTextureDesigner.Show (evaluateType);
		note.kernel.evaluateEffectDesigner.Show (evaluateType);
		this.p2000.count++;
		manager.AddCombo();
		manager.ResetPCombo();
		manager.ResetBPCombo();
		manager.Damage();
		note.judged = true;
		note.kernel.evaluated = true;
		if (note.kernel.eachArchDesigner != null)
			note.kernel.eachArchDesigner.HideIfIndependentState ();
		manager.SyncEvaluation(note.kernel, SyncEvaluate.FASTGREAT);
		manager.SetDrawableCommentNote(note);
	}
	
	public virtual void P2500FastCallBack(Inspector iNote) {
		var note = (Inspector.Break)iNote;
		manager.PlayNoteSE("se_break_2500");
		note.kernel.designer.Hide ();
		var evaluateType = DrawableEvaluateBreakType.P2500F;
		note.kernel.evaluateTextureDesigner.Show (evaluateType);
		note.kernel.evaluateEffectDesigner.Show (evaluateType);
		this.p2500.count++;
		manager.AddCombo();
		manager.AddPCombo();
		manager.ResetBPCombo();
		note.judged = true;
		note.kernel.evaluated = true;
		if (note.kernel.eachArchDesigner != null)
			note.kernel.eachArchDesigner.HideIfIndependentState ();
		manager.SyncEvaluation(note.kernel, SyncEvaluate.PERFECT);
		manager.SetDrawableCommentNote(note);
	}
	
	public virtual void P2550FastCallBack(Inspector iNote) {
		var note = (Inspector.Break)iNote;
		manager.PlayNoteSE("se_break_2500");
		note.kernel.designer.Hide ();
		var evaluateType = DrawableEvaluateBreakType.P2550F;
		note.kernel.evaluateTextureDesigner.Show (evaluateType);
		note.kernel.evaluateEffectDesigner.Show (evaluateType);
		this.p2550.count++;
		manager.AddCombo();
		manager.AddPCombo();
		manager.ResetBPCombo();
		note.judged = true;
		note.kernel.evaluated = true;
		if (note.kernel.eachArchDesigner != null)
			note.kernel.eachArchDesigner.HideIfIndependentState ();
		manager.SyncEvaluation(note.kernel, SyncEvaluate.PERFECT);
		manager.SetDrawableCommentNote(note);
	}
	
	public virtual void P2600CallBack(Inspector iNote) {
		var note = (Inspector.Break)iNote;
		manager.PlayNoteSE("se_break_2600", true);
		note.kernel.designer.Hide ();
		var evaluateType = DrawableEvaluateBreakType.P2600;
		if (!manager.resetting) {
			note.kernel.evaluateTextureDesigner.Show (evaluateType);
			note.kernel.evaluateEffectDesigner.Show (evaluateType);
		}
		this.p2600.count++;
		manager.AddCombo();
		manager.AddPCombo();
		manager.AddBPCombo();
		note.judged = true;
		note.kernel.evaluated = true;
		if (note.kernel.eachArchDesigner != null)
			note.kernel.eachArchDesigner.HideIfIndependentState ();
		manager.SyncEvaluation(note.kernel, SyncEvaluate.PERFECT);
		manager.SetDrawableCommentNote(note);
	}
	
	public virtual void P2550LateCallBack(Inspector iNote) {
		var note = (Inspector.Break)iNote;
		manager.PlayNoteSE("se_break_2500");
		note.kernel.designer.Hide ();
		var evaluateType = DrawableEvaluateBreakType.P2550L;
		note.kernel.evaluateTextureDesigner.Show (evaluateType);
		note.kernel.evaluateEffectDesigner.Show (evaluateType);
		this.p2550.count++;
		manager.AddCombo();
		manager.AddPCombo();
		manager.ResetBPCombo();
		note.judged = true;
		note.kernel.evaluated = true;
		if (note.kernel.eachArchDesigner != null)
			note.kernel.eachArchDesigner.HideIfIndependentState ();
		manager.SyncEvaluation(note.kernel, SyncEvaluate.PERFECT);
		manager.SetDrawableCommentNote(note);
	}
	
	public virtual void P2500LateCallBack(Inspector iNote) {
		var note = (Inspector.Break)iNote;
		manager.PlayNoteSE("se_break_2500");
		note.kernel.designer.Hide ();
		var evaluateType = DrawableEvaluateBreakType.P2500L;
		note.kernel.evaluateTextureDesigner.Show (evaluateType);
		note.kernel.evaluateEffectDesigner.Show (evaluateType);
		this.p2500.count++;
		manager.AddCombo();
		manager.AddPCombo();
		manager.ResetBPCombo();
		note.judged = true;
		note.kernel.evaluated = true;
		if (note.kernel.eachArchDesigner != null)
			note.kernel.eachArchDesigner.HideIfIndependentState ();
		manager.SyncEvaluation(note.kernel, SyncEvaluate.PERFECT);
		manager.SetDrawableCommentNote(note);
	}
	
	public virtual void P2000LateCallBack(Inspector iNote) {
		var note = (Inspector.Break)iNote;
		manager.PlayNoteSE("se_break_non_perfect");
		note.kernel.designer.Hide ();
		var evaluateType = DrawableEvaluateBreakType.P2000L;
		note.kernel.evaluateTextureDesigner.Show (evaluateType);
		note.kernel.evaluateEffectDesigner.Show (evaluateType);
		this.p2000.count++;
		manager.AddCombo();
		manager.ResetPCombo();
		manager.ResetBPCombo();
		manager.Damage();
		note.judged = true;
		note.kernel.evaluated = true;
		if (note.kernel.eachArchDesigner != null)
			note.kernel.eachArchDesigner.HideIfIndependentState ();
		manager.SyncEvaluation(note.kernel, SyncEvaluate.LATEGREAT);
		manager.SetDrawableCommentNote(note);
	}
	
	public virtual void P1500LateCallBack(Inspector iNote) {
		var note = (Inspector.Break)iNote;
		manager.PlayNoteSE("se_break_non_perfect");
		note.kernel.designer.Hide ();
		var evaluateType = DrawableEvaluateBreakType.P1500L;
		note.kernel.evaluateTextureDesigner.Show (evaluateType);
		note.kernel.evaluateEffectDesigner.Show (evaluateType);
		this.p1500.count++;
		manager.AddCombo();
		manager.ResetPCombo();
		manager.ResetBPCombo();
		manager.Damage();
		note.judged = true;
		note.kernel.evaluated = true;
		if (note.kernel.eachArchDesigner != null)
			note.kernel.eachArchDesigner.HideIfIndependentState ();
		manager.SyncEvaluation(note.kernel, SyncEvaluate.LATEGREAT);
		manager.SetDrawableCommentNote(note);
	}
	
	public virtual void P1250LateCallBack(Inspector iNote) {
		var note = (Inspector.Break)iNote;
		manager.PlayNoteSE("se_break_non_perfect");
		note.kernel.designer.Hide ();
		var evaluateType = DrawableEvaluateBreakType.P1250L;
		note.kernel.evaluateTextureDesigner.Show (evaluateType);
		note.kernel.evaluateEffectDesigner.Show (evaluateType);
		this.p1250.count++;
		manager.AddCombo();
		manager.ResetPCombo();
		manager.ResetBPCombo();
		manager.Damage();
		note.judged = true;
		note.kernel.evaluated = true;
		if (note.kernel.eachArchDesigner != null)
			note.kernel.eachArchDesigner.HideIfIndependentState ();
		manager.SyncEvaluation(note.kernel, SyncEvaluate.LATEGREAT);
		manager.SetDrawableCommentNote(note);
	}
	
	public virtual void P1000LateCallBack(Inspector iNote) {
		var note = (Inspector.Break)iNote;
		manager.PlayNoteSE("se_break_non_perfect");
		note.kernel.designer.Hide ();
		var evaluateType = DrawableEvaluateBreakType.P1000L;
		note.kernel.evaluateTextureDesigner.Show (evaluateType);
		note.kernel.evaluateEffectDesigner.Show (evaluateType);
		this.p1000.count++;
		manager.AddCombo();
		manager.ResetPCombo();
		manager.ResetBPCombo();
		manager.Damage();
		note.judged = true;
		note.kernel.evaluated = true;
		if (note.kernel.eachArchDesigner != null)
			note.kernel.eachArchDesigner.HideIfIndependentState ();
		manager.SyncEvaluation(note.kernel, SyncEvaluate.LATEGOOD);
		manager.SetDrawableCommentNote(note);
	}

	public override void MissCallBack(Inspector iNote) {
		var note = (Inspector.Break)iNote;
		note.kernel.designer.Hide ();
		note.kernel.evaluateTextureDesigner.Show (DrawableEvaluateBreakType.MISS);
		base.MissCallBack (note);
		if (note.kernel.eachArchDesigner != null)
			note.kernel.eachArchDesigner.HideIfIndependentState ();
		manager.SyncEvaluation(note.kernel, SyncEvaluate.MISS);
	}
	
	public override int GetPerfectCount() { return this.p2600.count + this.p2550.count + this.p2500.count; }
	public override int GetPerfectScore() { return this.p2600.score + this.p2550.score + this.p2500.score; }
	public override int GetGreatCount() { return this.p2000.count + this.p1500.count + this.p1250.count; }
	public override int GetGreatScore() { return this.p2000.score + this.p1500.score + this.p1250.score; }
	public override int GetGoodCount() { return this.p1000.count; }
	public override int GetGoodScore() { return this.p1000.score; }
	public override int GetNoteCount() {
		return GetPerfectCount() + GetGreatCount() + GetGoodCount() + GetMissCount();
	}
	
	public int GetFullScore() {
		return (this.GetPerfectCount() + this.GetGreatCount() + this.GetGoodCount() + this.GetMissCount()) * this.p2500.rate;
	}
	
	public int GetBFullScore() {
		return (this.GetPerfectCount() + this.GetGreatCount() + this.GetGoodCount() + this.GetMissCount()) * this.p2600.rate;
	}
	
	public int GetLostScore() {
		return
			(this.p2500.rate - this.p2600.rate) * this.p2600.count + 
			(this.p2500.rate - this.p2550.rate) * this.p2550.count + 
			(this.p2500.rate - this.p2500.rate) * this.p2550.count + 
			(this.p2500.rate - this.p2000.rate) * this.p2000.count + 
			(this.p2500.rate - this.p1500.rate) * this.p1500.count + 
			(this.p2500.rate - this.p1250.rate) * this.p1250.count + 
			(this.p2500.rate - this.p1000.rate) * this.p1000.count + 
			(this.p2500.rate - this.miss.rate) * this.GetMissCount();
	}
	
	public int GetBLostScore() {
		return
			(this.p2600.rate - this.p2600.rate) * this.p2600.count + 
			(this.p2600.rate - this.p2550.rate) * this.p2550.count + 
			(this.p2600.rate - this.p2500.rate) * this.p2550.count + 
			(this.p2600.rate - this.p2000.rate) * this.p2000.count + 
			(this.p2600.rate - this.p1500.rate) * this.p1500.count + 
			(this.p2600.rate - this.p1250.rate) * this.p1250.count + 
			(this.p2600.rate - this.p1000.rate) * this.p1000.count + 
			(this.p2600.rate - this.miss.rate) * this.GetMissCount();
	}
	
	public override void Reset () {
		this.p2600.count = 0;
		this.p2550.count = 0;
		this.p2500.count = 0;
		this.p2000.count = 0;
		this.p1500.count = 0;
		this.p1250.count = 0;
		this.p1000.count = 0;
		base.Reset ();
	}
	
	public class Maji : MaimaiJudgeEvaluateBreak {
		public Maji(MaimaiJudgeEvaluateManager manager, 
            int p2600Rate, int p2550Rate, int p2500Rate, int p2000Rate, int p1500Rate, int p1250Rate, int p1000Rate,
            long inMissFastTime, long inP1000FastTime, long inP1250FastTime, long inP1500FastTime, long inP2000FastTime, long inP2500FastTime, long inP2550FastTime, long inP2600Time,
            long inP2550LateTime, long inP2500LateTime, long inP2000LateTime, long inP1500LateTime, long inP1250LateTime, long inP1000LateTime, long inMissLateTime)
			: base(manager, p2600Rate, p2550Rate, p2500Rate, p2000Rate, p1500Rate, p1250Rate, p1000Rate,
			       inP1000FastTime, inP1250FastTime, inP1500FastTime, inP2000FastTime, inP2500FastTime, inP2550FastTime, inP2600Time,
			      inP2550LateTime, inP2500LateTime, inP2000LateTime, inP1500LateTime, inP1250LateTime, inP1000LateTime) {
			inFastMissTime = inMissFastTime;
			inLateMissTime = inMissLateTime;
		}
		
		public long inFastMissTime { get; protected set; }
		public long inLateMissTime { get; protected set; }
		
		public override void Evaluate (long timing, Inspector note) {
			if (timing >= -this.inFastMissTime && timing < -this.inP1000FastTime) {
				this.MissCallBack(note);
			}
			else if (timing >= -this.inP1000FastTime && timing < -this.inP1250FastTime) {
				this.P1000FastCallBack(note);
			}
			else if (timing >= -this.inP1250FastTime && timing < -this.inP1500FastTime) {
				this.P1250FastCallBack(note);
			}
			else if (timing >= -this.inP1500FastTime && timing < -this.inP2000FastTime) {
				this.P1500FastCallBack(note);
			}
			else if (timing >= -this.inP2000FastTime && timing < -this.inP2500FastTime) {
				this.P2000FastCallBack(note);
			}
			else if (timing >= -this.inP2500FastTime && timing < -this.inP2550FastTime) {
				this.P2500FastCallBack(note);
			}
			else if (timing >= -this.inP2550FastTime && timing < -this.inP2600Time) {
				this.P2550FastCallBack(note);
			}
			else if (timing >= -this.inP2600Time && timing <= this.inP2600Time) {
				this.P2600CallBack(note);
			}
			else if (timing > this.inP2600Time && timing <= this.inP2550LateTime) {
				this.P2550LateCallBack(note);
			}
			else if (timing > this.inP2550LateTime && timing <= this.inP2500LateTime) {
				this.P2500LateCallBack(note);
			}
			else if (timing > this.inP2500LateTime && timing <= this.inP2000LateTime) {
				this.P2000LateCallBack(note);
			}
			else if (timing > this.inP2000LateTime && timing <= this.inP1500LateTime) {
				this.P1500LateCallBack(note);
			}
			else if (timing > this.inP1500LateTime && timing <= this.inP1250LateTime) {
				this.P1250LateCallBack(note);
			}
			else if (timing > this.inP1250LateTime && timing <= this.inP1000LateTime) {
				this.P1000LateCallBack(note);
			}
			else if (timing > this.inP1000LateTime && timing <= this.inLateMissTime) {
				this.MissCallBack(note);
			}
		}
	}
	
	public class Gori : Maji {
		public Gori(MaimaiJudgeEvaluateManager manager, 
	            int p2600Rate,
	            long inMissFastTime, long inP2600Time, long inMissLateTime)
			: base(manager, p2600Rate, 0, 0, 0, 0, 0, 0,
			       inMissFastTime, 0, 0, 0, 0, 0, 0, inP2600Time,
			      0, 0, 0, 0, 0, 0, inMissLateTime) {
			
		}
		
		public override void Evaluate (long timing, Inspector note) {
			if (timing >= -this.inFastMissTime && timing < -this.inP2500FastTime) {
				this.MissCallBack(note);
			}
			else if (timing >= -this.inP2600Time && timing <= this.inP2600Time) {
				this.P2600CallBack(note);
			}
			else if (timing > this.inP2500LateTime && timing <= this.inLateMissTime) {
				this.MissCallBack(note);
			}
		}
	}


}
