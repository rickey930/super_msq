using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using XMaimaiNote.XInspector;

public class MaimaiSyncEvaluateManager : MonoBehaviour {
	/// <summary>
	/// シンクポイント.
	/// </summary>
	public int point { get; protected set; }
	/// <summary>
	/// シンク判定したノート数.
	/// </summary>
	public int count { get; protected set; }
	/// <summary>
	/// すべてのノートのシンク判定を終えたか.
	/// </summary>
	public bool isAllSyncEvaluated { get; protected set; }

	public float syncPercent {
		get {
			int noteCount20percent = (notesCount * 0.2f).ToInt();
			if (count > 0) {
				if (noteCount20percent < count) {
					return (float)point / (float)count;
				}
				else if (noteCount20percent > 0) {
					return (float)point / (float)noteCount20percent;
				}
			}
			return 0.0f;
		}
	}

	/// <summary>
	/// プレー人数.
	/// </summary>
	public int playerAmount { protected get; set; }
	/// <summary>
	/// ノート数総計.
	/// </summary>
	public int notesCount { protected get; set; }
	/// <summary>
	/// シンクポイントが変動した時に呼び出されるデリゲート.
	/// </summary>
	public System.Action<int> onPointChanged { protected get; set; }
	/// <summary>
	/// シンク判定済みノート数が変動した時に呼び出されるデリゲート.
	/// </summary>
	public System.Action<int> onCountChanged { protected get; set; }
	
	private Dictionary<string, List<int>> _evaluateTank;
	protected Dictionary<string, List<int>> evaluateTank {
		get {
			if (_evaluateTank == null) {
				_evaluateTank = new Dictionary<string, List<int>>();
			}
			return _evaluateTank;
		}
	}
	protected bool isSetuped { get; set; }

	/// <summary>
	/// 情報初期化. 全員で呼び出す.
	/// </summary>
	[RPC]
	public void Setup (int playerAmount, int notesCount) {
		this.playerAmount = playerAmount;
		this.notesCount = notesCount;
		this.point = 0;
		this.count = 0;
		this.isAllSyncEvaluated = false;
		this.isSetuped = true;

		if (onPointChanged != null) {
			onPointChanged (point);
		}
		if (onCountChanged != null) {
			onCountChanged (count);
		}
	}

	/// <summary>
	/// 評価記憶. サーバーだけ呼び出す.
	/// </summary>
	[RPC]
	public void SyncEvaluateTypeServerSide (string noteId, int noteType, int evaluateType) {
		if (!isSetuped) {
			Debug.LogError ("MaimaiSyncEvaluateManager is not setuped. You need use \"Setup\" method.");
			return;
		}

		if (!evaluateTank.ContainsKey (noteId)) {
			evaluateTank[noteId] = new List<int>();
		}
		var noteEvaluates = evaluateTank [noteId];
		noteEvaluates.Add (evaluateType);

		if (networkView != null && networkView.isMine) {
			if (noteEvaluates.Count == playerAmount) {
				int p = point;
				int c = count;
				bool allsyncronized = isAllSyncEvaluated;
				
				bool formed = false;
				InspectorType type = (InspectorType)noteType;
				switch (type) {
				case InspectorType.TAP:
				case InspectorType.HOLD_FOOT:
				case InspectorType.SLIDE:
				case InspectorType.TRAP:
					formed = FormedSync (noteEvaluates, FormedSyncGeneral);
					break;
				case InspectorType.BREAK:
					formed = FormedSync (noteEvaluates, FormedSyncBreak);
					break;
				}
				if (formed) {
					p++;
				}
				c++;
				
				if (c == notesCount) {
					allsyncronized = true;
				}
				
				if (networkView != null) {
					networkView.RPC ("SetSyncDataClientSide", RPCMode.AllBuffered, p, c, allsyncronized);
				}
				else {
					SetSyncDataClientSide (p, c, allsyncronized);
				}
			}
		}
	}

	/// <summary>
	/// シンクデータ反映. サーバーからクライアントに送られる.
	/// </summary>
	/// <param name="point">Point.</param>
	/// <param name="count">Count.</param>
	/// <param name="isAllSyncEvaluated">If set to <c>true</c> is all sync evaluated.</param>
	[RPC]
	public void SetSyncDataClientSide (int point, int count, bool isAllSyncEvaluated) {
		this.point = point;
		this.count = count;
		this.isAllSyncEvaluated = isAllSyncEvaluated;

		if (onPointChanged != null) {
			onPointChanged (point);
		}
		if (onCountChanged != null) {
			onCountChanged (count);
		}
	}

	private delegate bool FormedSyncAction (int ev1, int ev2);
	private bool FormedSync (List<int> evaluates, FormedSyncAction action) {
		for (int i = 0; i < playerAmount && i + 1 < playerAmount; i++) {
			for (int j = i + 1; j < playerAmount; j++) {
				if (!action (evaluates[i], evaluates[j])) {
					return false;
				}
			}
		}
		return true; //1人プレイでシンクできてしまうかも.
	}

	private bool FormedSyncGeneral (int ev1, int ev2) {
		SyncEvaluate e1 = (SyncEvaluate)ev1;
		SyncEvaluate e2 = (SyncEvaluate)ev2;
		if (e1 == SyncEvaluate.FASTGOOD) {
			if (e2 == SyncEvaluate.FASTGOOD ||
			    e2 == SyncEvaluate.FASTGREAT) {
				return true;
			}
			return false;
		}
		else if (e1 == SyncEvaluate.FASTGREAT) {
			if (e2 == SyncEvaluate.FASTGOOD ||
			    e2 == SyncEvaluate.FASTGREAT ||
			    e2 == SyncEvaluate.PERFECT) {
				return true;
			}
			return false;
		}
		else if (e1 == SyncEvaluate.PERFECT) {
			if (e2 == SyncEvaluate.FASTGREAT ||
			    e2 == SyncEvaluate.PERFECT ||
			    e2 == SyncEvaluate.LATEGREAT) {
				return true;
			}
			return false;
		}
		else if (e1 == SyncEvaluate.LATEGREAT) {
			if (e2 == SyncEvaluate.PERFECT ||
			    e2 == SyncEvaluate.LATEGREAT ||
			    e2 == SyncEvaluate.LATEGOOD) {
				return true;
			}
			return false;
		}
		else if (e1 == SyncEvaluate.LATEGOOD) {
			if (e2 == SyncEvaluate.LATEGREAT ||
			    e2 == SyncEvaluate.LATEGOOD) {
				return true;
			}
			return false;
		}
		return false;
	}

	private bool FormedSyncBreak (int ev1, int ev2) {
		SyncEvaluate e1 = (SyncEvaluate)ev1;
		SyncEvaluate e2 = (SyncEvaluate)ev2;
		if (e1 == SyncEvaluate.MISS || e2 == SyncEvaluate.MISS) {
			return false;
		}
		return e1 == e2;
	}
}
