using UnityEngine;
using System.Collections;
using XMaimaiNote.XInspector;
using XMaimaiNote.XEvaluateDesigner;

public class MaimaiJudgeEvaluateHoldHead : MaimaiJudgeEvaluateTap {
	public MaimaiJudgeEvaluateHoldHead(MaimaiJudgeEvaluateManager manager,
	                              int perfectRate, int greatRate, int goodRate, 
	                              long inFastGoodTime, long inFastGreatTime, long inPerfectTime, long inLateGreatTime, long inLateGoodTime)
	: base (manager, perfectRate, greatRate, goodRate, inFastGoodTime, inFastGreatTime, inPerfectTime, inLateGreatTime, inLateGoodTime) {

	}

	public override void FastGoodCallBack (Inspector iNote) {
		var note = (Inspector.HoldHead)iNote;
		manager.PlayNoteSE("se_tap_good");
		note.kernel.KeepStart (DrawableEvaluateTapType.FASTGOOD);
		note.judged = true;
		if (note.kernel.eachArchDesigner != null)
			note.kernel.eachArchDesigner.HideIfIndependentState ();
		manager.SetDrawableCommentNote(note);
	}

	public override void FastGreatCallBack (Inspector iNote) {
		var note = (Inspector.HoldHead)iNote;
		manager.PlayNoteSE("se_tap_great");
		note.kernel.KeepStart (DrawableEvaluateTapType.FASTGREAT);
		note.judged = true;
		if (note.kernel.eachArchDesigner != null)
			note.kernel.eachArchDesigner.HideIfIndependentState ();
		manager.SetDrawableCommentNote(note);
	}

	public override void PerfectCallBack (Inspector iNote) {
		var note = (Inspector.HoldHead)iNote;
		manager.PlayNoteSE("se_tap_perfect");
		note.kernel.KeepStart (DrawableEvaluateTapType.PERFECT);
		note.judged = true;
		if (note.kernel.eachArchDesigner != null)
			note.kernel.eachArchDesigner.HideIfIndependentState ();
		manager.SetDrawableCommentNote(note);
	}

	public override void LateGreatCallBack (Inspector iNote) {
		var note = (Inspector.HoldHead)iNote;
		manager.PlayNoteSE("se_tap_great");
		note.kernel.KeepStart (DrawableEvaluateTapType.LATEGREAT);
		note.judged = true;
		if (note.kernel.eachArchDesigner != null)
			note.kernel.eachArchDesigner.HideIfIndependentState ();
		manager.SetDrawableCommentNote(note);
	}

	public override void LateGoodCallBack (Inspector iNote) {
		var note = (Inspector.HoldHead)iNote;
		manager.PlayNoteSE("se_tap_good");
		note.kernel.KeepStart (DrawableEvaluateTapType.LATEGOOD);
		note.judged = true;
		if (note.kernel.eachArchDesigner != null)
			note.kernel.eachArchDesigner.HideIfIndependentState ();
		manager.SetDrawableCommentNote(note);
	}

	public override void MissCallBack (Inspector iNote) {
		var note = (Inspector.HoldHead)iNote;
		note.kernel.designer.Hide ();
		note.kernel.evaluateTextureDesigner.Show (DrawableEvaluateTapType.MISS);
		miss.count++;
		manager.ResetCombo ();
		manager.ResetPCombo ();
		manager.ResetBPCombo ();
		manager.Damage();
		note.judged = true;
		note.kernel.footNoteInfo.judged = true;
		note.kernel.evaluated = true;
		if (note.kernel.eachArchDesigner != null)
			note.kernel.eachArchDesigner.HideIfIndependentState ();
		manager.SyncEvaluation(note.kernel, SyncEvaluate.MISS);
	}
	
	public new class Maji : MaimaiJudgeEvaluateHoldHead {
		public Maji(MaimaiJudgeEvaluateManager manager,
	       int perfectRate, int greatRate, int goodRate, 
	       long inFastMissTime, long inFastGoodTime, long inFastGreatTime, long inPerfectTime, long inLateGreatTime, long inLateGoodTime, long inLateMissTime)
		: base (manager, perfectRate, greatRate, goodRate, inFastGoodTime, inFastGreatTime, inPerfectTime, inLateGreatTime, inLateGoodTime) {
			this.inFastMissTime = inFastMissTime;
			this.inLateMissTime = inLateMissTime;
		}
		
		public long inFastMissTime { get; protected set; }
		public long inLateMissTime { get; protected set; }
		
		public override void Evaluate (long timing, Inspector note) {
			if (timing >= -this.inFastMissTime && timing < -this.inFastGoodTime) {
				this.MissCallBack(note);
			}
			else if (timing >= -this.inFastGoodTime && timing < -this.inFastGreatTime) {
				this.FastGoodCallBack(note);
			}
			else if (timing >= -this.inFastGreatTime && timing < -this.inPerfectTime) {
				this.FastGreatCallBack(note);
			}
			else if (timing >= -this.inPerfectTime && timing <= this.inPerfectTime) {
				this.PerfectCallBack(note);
			}
			else if (timing > this.inPerfectTime && timing <= this.inLateGreatTime) {
				this.LateGreatCallBack(note);
			}
			else if (timing > this.inLateGreatTime && timing <= this.inLateGoodTime) {
				this.LateGoodCallBack(note);
			}
			else if (timing > this.inLateGoodTime && timing <= this.inLateMissTime) {
				this.MissCallBack(note);
			}
		}
	}

	public new class Gori : Maji {
		public Gori(MaimaiJudgeEvaluateManager manager,
	       int perfectRate, 
           long inFastMissTime, long inPerfectTime, long inLateMissTime)
		: base (manager, perfectRate, 0, 0, inFastMissTime, 0, 0, inPerfectTime, 0, 0, inLateMissTime) {
			
		}
		
		public override void Evaluate (long timing, Inspector note) {
			if (timing >= -this.inFastMissTime && timing < -this.inPerfectTime) {
				this.MissCallBack(note);
			}
			else if (timing >= -this.inPerfectTime && timing <= this.inPerfectTime) {
				this.PerfectCallBack(note);
			}
			else if (timing > this.inPerfectTime && timing <= this.inLateMissTime) {
				this.MissCallBack(note);
			}
		}
	}


}
