﻿using UnityEngine;
using RhythmGameLibrary;
using System.Collections;
using System.Collections.Generic;
using XMaimaiNote.XKernel;
using XMaimaiNote.XInspector;

public class MaimaiJudgeEvaluateManager : RhythmGameLibrary.RhythmGameEvaluate<int, Inspector> {
	public MaimaiJudgeEvaluateManager(
		MaimaiTimer timer, 
		Inspector[] notes, 
		long timeLag,
		ConfigTypes.SoundEffect soundEffectType,
		ConfigTypes.AnswerSound answerSoundType,
		ConfigTypes.Judgement judgementType,
		ConfigTypes.LifeDifficulty challengeLife,
		bool visualizeFastLate,
		bool allowSlideSectionOneSkip)
	: base (timer, NotesPartition(notes), timeLag, 0) {
		this.startBeatCountSoundEnabled = startBeatCountSoundEnabled;
		this.soundEffectType = soundEffectType;
		this.answerSoundType = answerSoundType;
		this.judgementType = judgementType;
		this.visualizeFastLate = visualizeFastLate;
		this.allowSlideSectionOneSkip = allowSlideSectionOneSkip;
		
		SetupEvaluaterNormal ();
		SetAchievementInfo(notes);
		
		if (judgementType == ConfigTypes.Judgement.MAJI) {
			SetupEvaluaterMaji ();
		}
		else if (judgementType == ConfigTypes.Judgement.GACHI) {
			SetupEvaluaterGachi ();
		}
		else if (judgementType == ConfigTypes.Judgement.GORI) {
			SetupEvaluaterGori ();
		}
		else {
			SetupEvaluaterNormal ();
		}

		SetLife(challengeLife);
	}

	private void SetupEvaluaterNormal () {
		long miss = MaimaiJudgeEvaluateManager.MissTimeBank (InspectorType.TAP);
		tapEvaluater = new MaimaiJudgeEvaluateTap(this, 500, 400, 250,
		                                          TapFastGoodTime, 125L, 90L, 200L, miss);
		miss = MaimaiJudgeEvaluateManager.MissTimeBank (InspectorType.HOLD_HEAD);
		holdHeadEvaluater = new MaimaiJudgeEvaluateHoldHead(this, 1000, 800, 500,
		                                                    150L, 125L, 90L, 200L, miss);
		holdFootEvaluater = new MaimaiJudgeEvaluateHoldFoot(this, holdHeadEvaluater,
		                                                    200L);
		miss = MaimaiJudgeEvaluateManager.MissTimeBank (InspectorType.SLIDE);
		slideEvaluater = new MaimaiJudgeEvaluateSlide(this, this.tapEvaluater, 50L, 1500, 1200, 750,
		                                              425L, 250L, 425L, miss);
		miss = MaimaiJudgeEvaluateManager.MissTimeBank (InspectorType.BREAK);
		breakEvaluater = new MaimaiJudgeEvaluateBreak(this, 2600, 2550, 2500, 2000, 1500, 1250, 1000,
		                                              //InP1000FastTime, InP1250FastTime, InP1500FastTime, InP2000FastTime, InP2500FastTime, InP2550FastTime, InP2600Time, InP2550LateTime, InP2500LateTime, InP2000LateTime, InP1500LateTime, InP1250LateTime, InP1000LateTime
		                                              150L, 137L, 125L, 112L, 90L, 63L, 36L, 63L, 90L, 150L, 200L, 250L, miss);
		trapEvaluater = new MaimaiJudgeEvaluateTrap(this, 500, 400, 250, TapFastGoodTime, 125L);
	}
	
	private void SetupEvaluaterMaji () {
		long miss = MaimaiJudgeEvaluateManager.MissTimeBank (InspectorType.TAP);
		tapEvaluater = new MaimaiJudgeEvaluateTap.Maji(this, 500, 400, 250,
		                                              150L, 125L, 90L, 63L, 90L, 150L, miss);
		miss = MaimaiJudgeEvaluateManager.MissTimeBank (InspectorType.HOLD_HEAD);
		holdHeadEvaluater = new MaimaiJudgeEvaluateHoldHead.Maji(this, 1000, 800, 500,
		                                                        150L, 125L, 90L, 63L, 90L, 150L, miss);
		holdFootEvaluater = new MaimaiJudgeEvaluateHoldFoot.Maji(this, holdHeadEvaluater,
		                                                        125L, 125L);
		miss = MaimaiJudgeEvaluateManager.MissTimeBank (InspectorType.SLIDE);
		slideEvaluater = new MaimaiJudgeEvaluateSlide.Maji(this, this.tapEvaluater, 50L, 1500, 1200, 750,
		                                                  425L, 250L, 125L, 250L, 425L, miss);
		miss = MaimaiJudgeEvaluateManager.MissTimeBank (InspectorType.BREAK);
		breakEvaluater = new MaimaiJudgeEvaluateBreak.Maji(this, 2600, 2550, 2500, 2000, 1500, 1250, 1000,
		                                                  //InFastMissTime, InP1000FastTime, InP1250FastTime, InP1500FastTime, InP2000FastTime, InP2500FastTime, InP2550FastTime, InP2600Time, InP2550LateTime, InP2500LateTime, InP2000LateTime, InP1500LateTime, InP1250LateTime, InP1000LateTime, InLateMissTime
		                                                   150L, 137L, 125L, 112L, 90L, 63L, 36L, 18L, 36L, 63L, 90L, 112L, 125L, 150L, miss);
		trapEvaluater = new MaimaiJudgeEvaluateTrap(this, 500, 400, 250, 125L, 90L);
	}
	
	private void SetupEvaluaterGachi () {
		long miss = MaimaiJudgeEvaluateManager.MissTimeBank (InspectorType.TAP);
		tapEvaluater = new MaimaiJudgeEvaluateTap.Maji(this, 500, 400, 250,
		                                              150L, 90L, 63L, 36L, 63L, 90L, miss);
		miss = MaimaiJudgeEvaluateManager.MissTimeBank (InspectorType.HOLD_HEAD);
		holdHeadEvaluater = new MaimaiJudgeEvaluateHoldHead.Maji(this, 1000, 800, 500,
		                                                        150L, 90L, 63L, 52L, 63L, 90L, miss);
		holdFootEvaluater = new MaimaiJudgeEvaluateHoldFoot.Maji(this, holdHeadEvaluater,
		                                                        90L, 90L);
		miss = MaimaiJudgeEvaluateManager.MissTimeBank (InspectorType.SLIDE);
		slideEvaluater = new MaimaiJudgeEvaluateSlide.Maji(this, this.tapEvaluater, 50L, 1500, 1200, 750,
		                                                  180L, 120L, 90L, 120L, 180L, miss);
		miss = MaimaiJudgeEvaluateManager.MissTimeBank (InspectorType.BREAK);
		breakEvaluater = new MaimaiJudgeEvaluateBreak.Maji(this, 2600, 2550, 2500, 2000, 1500, 1250, 1000,
		                                                  //InFastMissTime, InP1000FastTime, InP1250FastTime, InP1500FastTime, InP2000FastTime, InP2500FastTime, InP2550FastTime, InP2600Time, InP2550LateTime, InP2500LateTime, InP2000LateTime, InP1500LateTime, InP1250LateTime, InP1000LateTime, InLateMissTime
		                                                   150L, 126L, 108L, 90L, 72L, 54L, 36L, 18L, 36L, 54L, 72L, 90L, 108L, 126L, miss);
		trapEvaluater = new MaimaiJudgeEvaluateTrap(this, 500, 400, 250, 90L, 63L);
	}
	
	private void SetupEvaluaterGori () {
		long miss = MaimaiJudgeEvaluateManager.MissTimeBank (InspectorType.TAP);
		tapEvaluater = new MaimaiJudgeEvaluateTap.Gori(this, 500,
		                                              150L, 18L, miss);
		miss = MaimaiJudgeEvaluateManager.MissTimeBank (InspectorType.HOLD_HEAD);
		holdHeadEvaluater = new MaimaiJudgeEvaluateHoldHead.Gori(this, 1000,
		                                                        150L, 36L, miss);
		holdFootEvaluater = new MaimaiJudgeEvaluateHoldFoot.Gori(this, holdHeadEvaluater,
		                                                        63L, 36L);
		miss = MaimaiJudgeEvaluateManager.MissTimeBank (InspectorType.SLIDE);
		slideEvaluater = new MaimaiJudgeEvaluateSlide.Gori(this, this.tapEvaluater, 50L, 1500, 
		                                                  90L, miss);
		miss = MaimaiJudgeEvaluateManager.MissTimeBank (InspectorType.BREAK);
		breakEvaluater = new MaimaiJudgeEvaluateBreak.Gori(this, 2600,
		                                                  //InFastMissTime, InP2600Time, InLateMissTime
		                                                   150L, 18L, miss);
		trapEvaluater = new MaimaiJudgeEvaluateTrap.Gori(this, 500);
	}

	/// <summary>
	/// 評価に必要なノートを、アクションIDで振り分ける.
	/// </summary>
	private static Dictionary<int, Inspector[]> NotesPartition(Inspector[] notes) {
		var _ret = new Dictionary<int, List<Inspector>>();
		foreach (var note in notes) {
			if (!_ret.ContainsKey(note.actionId)) {
				_ret[note.actionId] = new List<Inspector>();
			}
			_ret[note.actionId].Add(note);
		}
		var ret = new Dictionary<int, Inspector[]>();
		foreach (var key in _ret.Keys) {
			ret[key] = _ret[key].ToArray();
		}
		return ret;
	}

	public static long MissTimeBank(InspectorType type) {
		if (type == InspectorType.TAP)
			return 300L;
		else if (type == InspectorType.HOLD_HEAD)
			return 300L;
		else if (type == InspectorType.HOLD_FOOT) //押しっぱなしgood時間
			return 300L;
		else if (type == InspectorType.SLIDE)
			return 600L;
		else if (type == InspectorType.BREAK)
			return 300L;
		return 0L;
	}
	public static long TapFastGoodTime = 150L;
	
	public MaimaiTimer GetTimer() { return (MaimaiTimer)timer; }

	protected bool startBeatCountSoundEnabled { get; set; }
	protected ConfigTypes.SoundEffect soundEffectType { get; set; }
	protected ConfigTypes.AnswerSound answerSoundType { get; set; }
	protected ConfigTypes.Judgement judgementType { get; set; }
	protected bool visualizeFastLate { get; set; }
	/// <summary>
	/// 一段飛ばしを許可する.
	/// </summary>
	public bool allowSlideSectionOneSkip { get; set; }

	public void PlayNoteSE(string name) {
		PlayNoteSE (name, false);
	}
	public void PlayNoteSE(string name, bool isBreak2600) {
		// オプションによってON/OFFする.
		bool option = 
			soundEffectType == ConfigTypes.SoundEffect.ON || 
				(soundEffectType == ConfigTypes.SoundEffect.ONLY_BREAK2600 && isBreak2600);
		if (option && !resetting) {
			Utility.SoundEffectManager.Play (name);
		}
	}
	
	//ノートコメント描画用ノート. //旧maiPad仕様.
	//private Inspector drawableCommentNote = null;
	public void SetDrawableCommentNote(Inspector note) {
		//this.drawableCommentNote = note;
	}

	#region 評価や時間で動作する関数群.
	
	/*
	 * スライドに関して、.
	 * OnJustやOnMissはSlideNoteで判別する.
	 * EvaluateはSlidePatternNoteで判別する.
	 * OnGuideはSlideNoteMoveStartで音を出す.
	 */
	
	protected override void Evaluate (long timing, long oldTiming, Inspector note) {
		var noteType = note.GetNoteType ();
		switch (noteType) {
		case InspectorType.TAP:
			tapEvaluater.Evaluate(timing, note);
			break;
		case InspectorType.HOLD_HEAD:
			holdHeadEvaluater.Evaluate(timing, note);
			break;
		case InspectorType.HOLD_FOOT:
			holdFootEvaluater.Evaluate(timing, note);
			break;
		case InspectorType.SLIDE:
			slideEvaluater.Evaluate(timing, note);
			break;
		case InspectorType.BREAK:
			breakEvaluater.Evaluate(timing, note);
			break;
		case InspectorType.TRAP:
			trapEvaluater.Evaluate(timing, note);
			break;
		}
	}

	protected override void OnGuide (Inspector note) {
		var noteType = note.GetNoteType ();
		switch (noteType) {
			// メッセージ系ノートはJustでHideする。Showはスケジューラ任せ.
		case InspectorType.MESSAGE:
			(note as XMaimaiNote.XInspector.Inspector.Message).kernel.designer.Hide();
			base.OnGuide(note);
			break;
		case InspectorType.SCROLL_MESSAGE:
			(note as XMaimaiNote.XInspector.Inspector.ScrollMessage).kernel.designer.Hide();
			base.OnGuide(note);
			break;
		}
	}
	
	protected override void OnJust (Inspector note) {
		var noteType = note.GetNoteType ();
		switch (noteType) {
		case InspectorType.TAP:
			tapEvaluater.PerfectCallBack(note);
			break;
		case InspectorType.HOLD_HEAD:
			holdHeadEvaluater.PerfectCallBack(note);
			break;
		case InspectorType.HOLD_FOOT:
			holdFootEvaluater.OkPullCallBack(note);
			break;
		case InspectorType.SLIDE:
			slideEvaluater.PerfectCallBack(note);
			break;
		case InspectorType.BREAK:
			breakEvaluater.P2600CallBack(note);
			break;
		}
	}
	
	protected override void OnMiss (Inspector note) {
		base.OnMiss (note);
		var noteType = note.GetNoteType ();
		switch (noteType) {
		case InspectorType.TAP:
			this.tapEvaluater.MissCallBack(note);
			break;
		case InspectorType.HOLD_HEAD:
			//ホールドヘッドのミスはすぐに非表示にする。（他のノートは消さずに過ぎ去ればそのうち消える(消す処理を入れてる)。）.
			this.holdHeadEvaluater.MissCallBack(note);
			break;
		case InspectorType.HOLD_FOOT:
			var fNote = (Inspector.HoldFoot)note;
			if (judgementType != ConfigTypes.Judgement.GORI) {
				//ホールドフットはミスにならない。.
				if (fNote.kernel.evaluate != XMaimaiNote.XEvaluateDesigner.DrawableEvaluateTapType.NONE) { //ヘッドが押せていればグドる。.
					this.holdFootEvaluater.LateCallBack(note);
				}
			}
			else {
				this.holdFootEvaluater.MissCallBack (note);
			}
			break;
		case InspectorType.SLIDE:
			//スライドは5つのうち4つ判定できてたらMISSではなくLateGood。.
			var sNote = (Inspector.Slide)note;
			if (!sNote.kernel.evaluated) {
				if (sNote.kernel.kernel.IsLateGoodPassible() && judgementType != ConfigTypes.Judgement.GORI) {
					slideEvaluater.LateGoodCallBack(note, true);
				}
				else {
					slideEvaluater.MissCallBack(note);
				}
			}
			break;
		case InspectorType.BREAK:
			this.breakEvaluater.MissCallBack(note);
			break;
		case InspectorType.TRAP:
			this.trapEvaluater.PerfectCallBack(note);
			break;
		}
	}
	
	#endregion

	#region 評価を初期化する関数群.
	
	// OnJust中で音やエフェクトを出さないフラグ.
	public bool resetting { get; set; }
	
	// スライドノートのパターンのハッシュ.
	protected HashSet<Kernel.Slide> slideHash;
	
	protected override void ResetEvaluatePrepare () {
		tapEvaluater.Reset ();
		holdHeadEvaluater.Reset ();
		slideEvaluater.Reset ();
		breakEvaluater.Reset ();
		ResetCombo();
		ResetPCombo();
		ResetBPCombo();
		if (slideHash == null) {
			slideHash = new HashSet<Kernel.Slide> ();
		}
		slideHash.Clear ();
	}
	
	protected override void OnResetJust(Inspector note, bool justTimeBefore) {
		if (note.IsEvaluatableNote()) {
			// 「途中から開始」の時間までのノートの評価を引き継ぎたかったら、評価を覚えておく必要がある.
			// 今作はオートプレーのときのみリピート機能が有効なので、『「途中から開始」の時間までのノートの評価』は全部JustでOK.
			if (justTimeBefore) {
				// 「途中から開始」の時間以前のノートは全部Just.
				resetting = true;
				var sNote = note as Inspector.Slide;
				if (sNote == null) {
					if (note.GetNoteType() != InspectorType.TRAP) {
						if (note.GetNoteType() != InspectorType.HOLD_FOOT) { //footはheadでリセットするので二重リセットを防ぐ.
							note.kernel.Reset ();
						}
						OnJust (note);
					}
				}
				else if (slideHash.Add (sNote.kernel.kernel)){
					note.kernel.Reset ();
					OnJust (note);
				}
				resetting = false;
			}
			else {
				// 「途中から開始」の時間以後のノートは初期化.
				var sNote = note as Inspector.Slide;
				if (sNote == null) {
					if (note.GetNoteType() != InspectorType.HOLD_FOOT) { //footはheadでリセットするので二重リセットを防ぐ.
						note.kernel.Reset ();
					}
				}
				else if (slideHash.Add (sNote.kernel.kernel)){
					note.kernel.Reset ();
				}
				base.OnResetJust (note, justTimeBefore);
			}
		}
	}

	protected override void OnResetGuide(Inspector note, bool guideTimeBefore) {
		if (note.IsGuideNote()) {
			note.kernel.Reset ();
			OnGuide(note);
			if (!guideTimeBefore) {
				base.OnResetGuide (note, guideTimeBefore);
			}
		}
	}

	protected override void OnResetMiss(Inspector note, bool missTimeBefore) {
		if (note.GetNoteType() == InspectorType.TRAP) {
			note.kernel.Reset ();
			base.OnResetMiss(note, missTimeBefore);
			// ミスタイムを過ぎていたらOnMiss実行.
			if (missTimeBefore) {
				OnMiss (note);
			}
		}
	}
	
	#endregion

	#region 評価やコンボ用の変数/関数群.
	
	protected MaimaiJudgeEvaluateTap tapEvaluater { get; set; }
	protected MaimaiJudgeEvaluateHoldHead holdHeadEvaluater { get; set; }
	protected MaimaiJudgeEvaluateHoldFoot holdFootEvaluater { get; set; }
	protected MaimaiJudgeEvaluateSlide slideEvaluater { get; set; }
	protected MaimaiJudgeEvaluateBreak breakEvaluater { get; set; }
	protected MaimaiJudgeEvaluateTrap trapEvaluater { get; set; }
	
	public int combo { get; set; }
	public int maxCombo { get; set; }
	public int pcombo { get; set; } //パーフェクトのつなぎ.
	public int bpcombo { get; set; } //理論値のつなぎ.
	private int notesCount { get; set; }
	public int achievementScore { get; protected set; } //100.00%スコア.
	public int theoreticalScore { get; protected set; } //理論値.
	public int? life { get; set; } // 残りライフ.
	
	public void AddCombo() {
		this.combo++;
		if (this.combo > this.maxCombo)
			this.maxCombo = this.combo;
	}
	public void AddPCombo() { this.pcombo++; }
	public void AddBPCombo() { this.bpcombo++; }
	public void ResetCombo() { this.combo = 0; }
	public void ResetPCombo() { this.pcombo = 0; }
	public void ResetBPCombo() { this.bpcombo = 0; }
	public int GetScore() {
		return GetTapScore() + GetHoldScore() + GetSlideScore() + GetBreakScore() + GetTrapScore();
	}
	public int GetTapScore() {
		return tapEvaluater.GetPerfectScore() + tapEvaluater.GetGreatScore() + tapEvaluater.GetGoodScore();
	}
	public int GetHoldScore() {
		return holdHeadEvaluater.GetPerfectScore() + holdHeadEvaluater.GetGreatScore() + holdHeadEvaluater.GetGoodScore();
	}
	public int GetSlideScore() {
		return slideEvaluater.GetPerfectScore() + slideEvaluater.GetGreatScore() + slideEvaluater.GetGoodScore();
	}
	public int GetBreakScore() {
		return breakEvaluater.GetPerfectScore() + breakEvaluater.GetGreatScore() + breakEvaluater.GetGoodScore();
	}
	public int GetTrapScore() {
		return trapEvaluater.GetPerfectScore() + trapEvaluater.GetGreatScore() + trapEvaluater.GetGoodScore();
	}
	public int GetFullTapScore () {
		return tapEvaluater.GetFullScore ();
	}
	public int GetFullHoldScore () {
		return holdHeadEvaluater.GetFullScore ();
	}
	public int GetFullSlideScore () {
		return slideEvaluater.GetFullScore ();
	}
	public int GetFullBreakScore () {
		return breakEvaluater.GetFullScore ();
	}
	public int GetBFullBreakScore () {
		return breakEvaluater.GetBFullScore ();
	}
	public int GetFullTrapScore () {
		return trapEvaluater.GetFullScore ();
	}
	public int GetTapCount() {
		return tapEvaluater.GetNoteCount();
	}
	public int GetHoldCount() {
		return holdHeadEvaluater.GetNoteCount();
	}
	public int GetSlideCount() {
		return slideEvaluater.GetNoteCount();
	}
	public int GetBreakCount() {
		return breakEvaluater.GetNoteCount();
	}
	public int GetTrapCount() {
		return trapEvaluater.GetNoteCount();
	}
	public int GetFullScore() {
		return tapEvaluater.GetFullScore() + holdHeadEvaluater.GetFullScore() + slideEvaluater.GetFullScore() + breakEvaluater.GetFullScore() + trapEvaluater.GetFullScore();
	}
	public int GetBFullScore() {
		return tapEvaluater.GetFullScore() + holdHeadEvaluater.GetFullScore() + slideEvaluater.GetFullScore() + breakEvaluater.GetBFullScore() + trapEvaluater.GetFullScore();
	}
	public int GetLostScore() {
		return tapEvaluater.GetLostScore() + holdHeadEvaluater.GetLostScore() + slideEvaluater.GetLostScore() + breakEvaluater.GetLostScore() + trapEvaluater.GetLostScore();
	}
	public int GetBLostScore() {
		return tapEvaluater.GetLostScore() + holdHeadEvaluater.GetLostScore() + slideEvaluater.GetLostScore() + breakEvaluater.GetBLostScore() + trapEvaluater.GetLostScore();
	}
	public int GetPerfectCount() {
		return tapEvaluater.GetPerfectCount() + holdHeadEvaluater.GetPerfectCount() + slideEvaluater.GetPerfectCount() + breakEvaluater.GetPerfectCount() + trapEvaluater.GetPerfectCount();
	}
	public int GetGreatCount() {
		return tapEvaluater.GetGreatCount() + holdHeadEvaluater.GetGreatCount() + slideEvaluater.GetGreatCount() + breakEvaluater.GetGreatCount() + trapEvaluater.GetGreatCount();
	}
	public int GetGoodCount() {
		return tapEvaluater.GetGoodCount() + holdHeadEvaluater.GetGoodCount() + slideEvaluater.GetGoodCount() + breakEvaluater.GetGoodCount() + trapEvaluater.GetGoodCount();
	}
	public int GetMissCount() {
		return tapEvaluater.GetMissCount() + holdHeadEvaluater.GetMissCount() + slideEvaluater.GetMissCount() + breakEvaluater.GetMissCount() + trapEvaluater.GetMissCount();
	}
	public int GetNotesCountTotal () {
		return GetPerfectCount() + GetGreatCount() + GetGoodCount() + GetMissCount();
	}
	public float GetAchievement() {
		return achievementScore == 0 ? 0 : (float)GetScore() / (float)achievementScore;
	}
	public float GetPaceAchievement() {
		int fs = GetFullScore();
		return fs == 0 ? 0 : (float)GetScore() / (float)fs;
	}
	public float GetHazardBreakPaceAchievement() { // Breakボーナスで増える.
		return achievementScore == 0 ? 1 : (float)
			(achievementScore - (// MAX点から.
			                     GetLostScore ())) / //ロスト点を引いて.
				(float)achievementScore; // MAX点で割る.
	}
	public float GetHazardAchievement() { // 最初からBreak点込み.
		return achievementScore == 0 ? 1 : (float)
			(theoreticalScore - (// MAX点から.
			                     GetBLostScore ())) / //ロスト点を引いて.
				(float)achievementScore; // MAX点で割る.
	}
	public float GetTheoreticalAchievement() { // 理論値を100%としてみる.
		return theoreticalScore == 0 ? 0 : (float)GetScore() / (float)theoreticalScore;
	}
	public float GetTheoreticalPaceAchievement() {
		int fs = GetBFullScore();
		return fs == 0 ? 0 : (float)GetScore() / (float)fs;
	}
	public float GetTheoreticalHazardAchievement() {
		return theoreticalScore == 0 ? 1 : (float)
			(theoreticalScore - (// MAX点から.
			                     GetBLostScore ())) / //ロスト点を引いて.
				(float)theoreticalScore; // MAX点で割る.
	}
	public void SetLife(ConfigTypes.LifeDifficulty lifeDif) {
		switch (lifeDif) {
		case ConfigTypes.LifeDifficulty.LIFE_1:
			life = 1;
			break;
		case ConfigTypes.LifeDifficulty.LIFE_5:
			life = 5;
			break;
		case ConfigTypes.LifeDifficulty.LIFE_10:
			life = 10;
			break;
		case ConfigTypes.LifeDifficulty.LIFE_20:
			life = 20;
			break;
		case ConfigTypes.LifeDifficulty.LIFE_50:
			life = 50;
			break;
		case ConfigTypes.LifeDifficulty.LIFE_100:
			life = 100;
			break;
		case ConfigTypes.LifeDifficulty.LIFE_300:
			life = 300;
			break;
		default:
			life = null;
			break;
		}
	}
	public void Damage() {
		if (life.HasValue && life.Value > 0) {
			PlayNoteSE("se_damage");
			life--;
		}
	}
	public bool IsAllPerfect() {
		return notesCount == GetPerfectCount();
	}
	public bool IsFullCombo() {
		return notesCount == GetPerfectCount() + GetGreatCount();
	}
	public bool IsNoMiss() {
		return notesCount == GetPerfectCount() + GetGreatCount() + GetGoodCount();
	}
	public bool IsDie() {
		return life.HasValue && life.Value <= 0;
	}
	private void SetAchievementInfo(Inspector[] notes) {
		achievementScore = 0;
		if (notes == null) return;
		var tapcount = tapEvaluater.perfect.count;
		var slidecount = slideEvaluater.perfect.count;
		var breakcount = breakEvaluater.p2500.count;
		var breakcount2 = breakEvaluater.p2600.count;
		var breakcount3 = breakEvaluater.p2550.count;
		var holdcount = holdHeadEvaluater.perfect.count;
		var trapcount = trapEvaluater.perfect.count;
		
		tapEvaluater.perfect.count = 0;
		slideEvaluater.perfect.count = 0;
		breakEvaluater.p2500.count = 0;
		breakEvaluater.p2600.count = 0;
		breakEvaluater.p2550.count = 0;
		holdHeadEvaluater.perfect.count = 0;
		trapEvaluater.perfect.count = 0;
		for (var i = 0; i < notes.Length; i++) {
			if (notes[i].GetNoteType() == InspectorType.TAP)
				tapEvaluater.perfect.count++;
			else if (notes[i].GetNoteType() == InspectorType.HOLD_HEAD)
				holdHeadEvaluater.perfect.count++;
			else if (notes[i].GetNoteType() == InspectorType.SLIDE) {
				var slNote = (Inspector.Slide)notes[i];
				if (slNote == slNote.kernel.kernel.noteInfo) {
					slideEvaluater.perfect.count++;
				}
			}
			else if (notes[i].GetNoteType() == InspectorType.BREAK)
				breakEvaluater.p2500.count++;
			else if (notes[i].GetNoteType() == InspectorType.TRAP)
				trapEvaluater.perfect.count++;
		}
		
		achievementScore = 
			tapEvaluater.GetPerfectScore () + 
				holdHeadEvaluater.GetPerfectScore () +
				slideEvaluater.GetPerfectScore () + 
				breakEvaluater.GetPerfectScore () +
				trapEvaluater.GetPerfectScore();
		notesCount = 
			tapEvaluater.GetPerfectCount () + 
				holdHeadEvaluater.GetPerfectCount () +
				slideEvaluater.GetPerfectCount () + 
				breakEvaluater.GetPerfectCount () +
				trapEvaluater.GetPerfectCount();
		breakEvaluater.p2600.count = breakEvaluater.p2500.count;
		breakEvaluater.p2500.count = 0;
		theoreticalScore = 
			tapEvaluater.GetPerfectScore () + 
				holdHeadEvaluater.GetPerfectScore () +
				slideEvaluater.GetPerfectScore () + 
				breakEvaluater.GetPerfectScore () + 
				trapEvaluater.GetPerfectScore();
		
		tapEvaluater.perfect.count = tapcount;
		slideEvaluater.perfect.count = slidecount;
		breakEvaluater.p2500.count = breakcount;
		breakEvaluater.p2600.count = breakcount2;
		breakEvaluater.p2550.count = breakcount3;
		holdHeadEvaluater.perfect.count = holdcount;
		trapEvaluater.perfect.count = trapcount;
	}
	
	#endregion
	
	#region 譜面の理論値だけ知りたい.
	private MaimaiJudgeEvaluateManager(Inspector[] notes) : base (null, NotesPartition(notes), 0, 0) {
		SetupEvaluaterNormal ();
		SetAchievementInfo(notes);
	}
	public static int CalcBFullScore (XMaimaiNote.XDocument.Document[] docs) {
		var notes = new List<Inspector>();
		foreach (var doc in docs) {
			Inspector ins = null;
			if (doc.type == XMaimaiNote.XDocument.Type.TAP)
				ins = new Inspector.Tap(null, 0, 0, 0);
			else if (doc.type == XMaimaiNote.XDocument.Type.HOLD)
				ins = new Inspector.HoldHead(null, 0, 0, 0);
			else if (doc.type == XMaimaiNote.XDocument.Type.SLIDE)
				ins = new Inspector.Slide(new Kernel.Slide.Chain(), 0, 0, 0);
			else if (doc.type == XMaimaiNote.XDocument.Type.BREAK)
				ins = new Inspector.Break(null, 0, 0, 0);
			else if (doc.type == XMaimaiNote.XDocument.Type.TRAP)
				ins = new Inspector.Trap(null, 0, 0, 0);
			if (ins != null)
				notes.Add (ins);
		}
		var work = new MaimaiJudgeEvaluateManager (notes.ToArray());
		return work.theoreticalScore;
	}
	#endregion
	
	#region シンク関連.
	public NetworkView syncEvaluater { get; set; } // 外部から設定させる.
	
	public void SyncEvaluation (Kernel note, SyncEvaluate evaluateType) {
		if (syncEvaluater != null) {
			syncEvaluater.RPC ("SyncEvaluateTypeServerSide", RPCMode.AllBuffered, note.uniqueId, (int)note.GetNoteType(), (int)evaluateType);
		}
	}
	
	#endregion
}
