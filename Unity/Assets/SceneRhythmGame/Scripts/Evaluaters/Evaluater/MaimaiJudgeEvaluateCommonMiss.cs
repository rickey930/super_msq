﻿using UnityEngine;
using System.Collections;
using XMaimaiNote.XInspector;
using XMaimaiNote.XEvaluateDesigner;

public abstract class MaimaiJudgeEvaluateCommonMiss : MaimaiJudgeEvaluate {
	public MaimaiJudgeEvaluateCommonMiss(MaimaiJudgeEvaluateManager manager) : base (manager) {
		miss = new MaimaiJudgeEvaluateInfo(0);
	}

	public MaimaiJudgeEvaluateInfo miss { get; protected set; }
	public override int GetMissCount ()	{
		return miss.count;
	}

	public virtual void MissCallBack(Inspector note) {
		miss.count++;
		manager.ResetCombo ();
		manager.ResetPCombo ();
		manager.ResetBPCombo ();
		manager.Damage();
		note.judged = true;
		note.kernel.evaluated = true;
	}
	
	public virtual void Reset () {
		miss.count = 0;
	}
}
