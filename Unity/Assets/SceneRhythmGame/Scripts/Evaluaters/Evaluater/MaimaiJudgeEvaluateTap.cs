using UnityEngine;
using System.Collections;
using XMaimaiNote.XInspector;
using XMaimaiNote.XEvaluateDesigner;

public class MaimaiJudgeEvaluateTap : MaimaiJudgeEvaluateCommonMiss {
	public MaimaiJudgeEvaluateTap(MaimaiJudgeEvaluateManager manager,
	int perfectRate, int greatRate, int goodRate, 
	long inFastGoodTime, long inFastGreatTime, long inPerfectTime, long inLateGreatTime, long inLateGoodTime)
	: base (manager) {
		this.perfect = new MaimaiJudgeEvaluateInfo (perfectRate);
		this.great = new MaimaiJudgeEvaluateInfo (greatRate);
		this.good = new MaimaiJudgeEvaluateInfo (goodRate);
		this.inPerfectTime = Mathf.Abs ((float)inPerfectTime).ToLong();
		this.inFastGreatTime = Mathf.Abs ((float)inFastGreatTime).ToLong();
		this.inLateGreatTime = Mathf.Abs ((float)inLateGreatTime).ToLong();
		this.inFastGoodTime = Mathf.Abs ((float)inFastGoodTime).ToLong();
		this.inLateGoodTime = Mathf.Abs ((float)inLateGoodTime).ToLong();
	}

	public MaimaiJudgeEvaluateInfo perfect { get; protected set; }
	public MaimaiJudgeEvaluateInfo great { get; protected set; }
	public MaimaiJudgeEvaluateInfo good { get; protected set; }
	public long inPerfectTime { get; protected set; }
	public long inFastGreatTime { get; protected set; }
	public long inLateGreatTime { get; protected set; }
	public long inFastGoodTime { get; protected set; }
	public long inLateGoodTime { get; protected set; }

	public override void Evaluate (long timing, Inspector note) {
		if (timing >= -this.inFastGoodTime && timing < -this.inFastGreatTime) {
			this.FastGoodCallBack(note);
		}
		else if (timing >= -this.inFastGreatTime && timing < -this.inPerfectTime) {
			this.FastGreatCallBack(note);
		}
		else if (timing >= -this.inPerfectTime && timing <= this.inPerfectTime) {
			this.PerfectCallBack(note);
		}
		else if (timing > this.inPerfectTime && timing <= this.inLateGreatTime) {
			this.LateGreatCallBack(note);
		}
		else if (timing > this.inLateGreatTime && timing <= this.inLateGoodTime) {
			this.LateGoodCallBack(note);
		}
	}

	public virtual void FastGoodCallBack(Inspector iNote) {
		var note = (Inspector.Tap)iNote;
		manager.PlayNoteSE("se_tap_good");
		note.kernel.designer.Hide ();
		var evaluateType = DrawableEvaluateTapType.FASTGOOD;
		note.kernel.evaluateTextureDesigner.Show (evaluateType);
		note.kernel.evaluateEffectDesigner.Show (evaluateType);
		this.good.count++;
		manager.AddCombo();
		manager.ResetPCombo();
		manager.ResetBPCombo();
		manager.Damage();
		note.judged = true;
		note.kernel.evaluated = true;
		if (note.kernel.eachArchDesigner != null)
			note.kernel.eachArchDesigner.HideIfIndependentState ();
		manager.SyncEvaluation(note.kernel, SyncEvaluate.FASTGOOD);
		manager.SetDrawableCommentNote(note);
	}
	public virtual void FastGreatCallBack(Inspector iNote) {
		var note = (Inspector.Tap)iNote;
		manager.PlayNoteSE("se_tap_great");
		note.kernel.designer.Hide ();
		var evaluateType = DrawableEvaluateTapType.FASTGREAT;
		note.kernel.evaluateTextureDesigner.Show (evaluateType);
		note.kernel.evaluateEffectDesigner.Show (evaluateType);
		this.great.count++;
		manager.AddCombo();
		manager.ResetPCombo();
		manager.ResetBPCombo();
		manager.Damage();
		note.judged = true;
		note.kernel.evaluated = true;
		if (note.kernel.eachArchDesigner != null)
			note.kernel.eachArchDesigner.HideIfIndependentState ();
		manager.SyncEvaluation(note.kernel, SyncEvaluate.FASTGREAT);
		manager.SetDrawableCommentNote(note);
	}
	public virtual void PerfectCallBack(Inspector iNote) {
		var note = (Inspector.Tap)iNote;
		manager.PlayNoteSE("se_tap_perfect");
		note.kernel.designer.Hide ();
		var evaluateType = DrawableEvaluateTapType.PERFECT;
		if (!manager.resetting) {
			note.kernel.evaluateTextureDesigner.Show (evaluateType);
			note.kernel.evaluateEffectDesigner.Show (evaluateType);
		}
		this.perfect.count++;
		manager.AddCombo();
		manager.AddPCombo();
		manager.AddBPCombo();
		note.judged = true;
		note.kernel.evaluated = true;
		if (note.kernel.eachArchDesigner != null)
			note.kernel.eachArchDesigner.HideIfIndependentState ();
		manager.SyncEvaluation(note.kernel, SyncEvaluate.PERFECT);
		manager.SetDrawableCommentNote(note);
	}
	public virtual void LateGreatCallBack(Inspector iNote) {
		var note = (Inspector.Tap)iNote;
		manager.PlayNoteSE("se_tap_great");
		note.kernel.designer.Hide ();
		var evaluateType = DrawableEvaluateTapType.LATEGREAT;
		note.kernel.evaluateTextureDesigner.Show (evaluateType);
		note.kernel.evaluateEffectDesigner.Show (evaluateType);
		this.great.count++;
		manager.AddCombo();
		manager.ResetPCombo();
		manager.ResetBPCombo();
		manager.Damage();
		note.judged = true;
		note.kernel.evaluated = true;
		if (note.kernel.eachArchDesigner != null)
			note.kernel.eachArchDesigner.HideIfIndependentState ();
		manager.SyncEvaluation(note.kernel, SyncEvaluate.LATEGREAT);
		manager.SetDrawableCommentNote(note);
	}
	public virtual void LateGoodCallBack(Inspector iNote) {
		var note = (Inspector.Tap)iNote;
		manager.PlayNoteSE("se_tap_good");
		note.kernel.designer.Hide ();
		var evaluateType = DrawableEvaluateTapType.LATEGOOD;
		note.kernel.evaluateTextureDesigner.Show (evaluateType);
		note.kernel.evaluateEffectDesigner.Show (evaluateType);
		this.good.count++;
		manager.AddCombo();
		manager.ResetPCombo();
		manager.ResetBPCombo();
		manager.Damage();
		note.judged = true;
		note.kernel.evaluated = true;
		if (note.kernel.eachArchDesigner != null)
			note.kernel.eachArchDesigner.HideIfIndependentState ();
		manager.SyncEvaluation(note.kernel, SyncEvaluate.LATEGOOD);
		manager.SetDrawableCommentNote(note);
	}
	public override void MissCallBack(Inspector iNote) {
		var note = (Inspector.Tap)iNote;
		note.kernel.designer.Hide ();
		note.kernel.evaluateTextureDesigner.Show (DrawableEvaluateTapType.MISS);
		base.MissCallBack (note);
		if (note.kernel.eachArchDesigner != null)
			note.kernel.eachArchDesigner.HideIfIndependentState ();
		manager.SyncEvaluation(note.kernel, SyncEvaluate.MISS);
	}

	public override int GetPerfectCount() { return this.perfect.count; }
	public override int GetPerfectScore() { return this.perfect.score; }
	public override int GetGreatCount() { return this.great.count; }
	public override int GetGreatScore() { return this.great.score; }
	public override int GetGoodCount() { return this.good.count; }
	public override int GetGoodScore() { return this.good.score; }
	public override int GetNoteCount() {
		return GetPerfectCount() + GetGreatCount() + GetGoodCount() + GetMissCount();
	}
	
	public int GetFullScore() {
		return (this.GetPerfectCount() + this.GetGreatCount() + this.GetGoodCount() + this.GetMissCount()) * this.perfect.rate;
	}
	
	public int GetLostScore() {
		return
			(this.perfect.rate - this.perfect.rate) * this.GetPerfectCount() + 
			(this.perfect.rate - this.great.rate) * this.GetGreatCount() + 
			(this.perfect.rate - this.good.rate) * this.GetGoodCount() + 
			(this.perfect.rate - this.miss.rate) * this.GetMissCount();
	}
	
	public override void Reset () {
		this.perfect.count = 0;
		this.great.count = 0;
		this.good.count = 0;
		base.Reset ();
	}

	
	public class Maji : MaimaiJudgeEvaluateTap {
		public Maji(MaimaiJudgeEvaluateManager manager,
          int perfectRate, int greatRate, int goodRate, 
          long inFastMissTime, long inFastGoodTime, long inFastGreatTime, long inPerfectTime, long inLateGreatTime, long inLateGoodTime, long inLateMissTime)
		: base (manager, perfectRate, greatRate, goodRate, inFastGoodTime, inFastGreatTime, inPerfectTime, inLateGreatTime, inLateGoodTime) {
			this.inFastMissTime = inFastMissTime;
			this.inLateMissTime = inLateMissTime;
		}
		
		public long inFastMissTime { get; protected set; }
		public long inLateMissTime { get; protected set; }
		
		public override void Evaluate (long timing, Inspector note) {
			if (timing >= -this.inFastMissTime && timing < -this.inFastGoodTime) {
				this.MissCallBack(note);
			}
			else if (timing >= -this.inFastGoodTime && timing < -this.inFastGreatTime) {
				this.FastGoodCallBack(note);
			}
			else if (timing >= -this.inFastGreatTime && timing < -this.inPerfectTime) {
				this.FastGreatCallBack(note);
			}
			else if (timing >= -this.inPerfectTime && timing <= this.inPerfectTime) {
				this.PerfectCallBack(note);
			}
			else if (timing > this.inPerfectTime && timing <= this.inLateGreatTime) {
				this.LateGreatCallBack(note);
			}
			else if (timing > this.inLateGreatTime && timing <= this.inLateGoodTime) {
				this.LateGoodCallBack(note);
			}
			else if (timing > this.inLateGoodTime && timing <= this.inLateMissTime) {
				this.MissCallBack(note);
			}
		}
	}
	
	public class Gori : Maji {
		public Gori(MaimaiJudgeEvaluateManager manager,
          int perfectRate,
          long inFastMissTime, long inPerfectTime, long inLateMissTime)
		: base (manager, perfectRate, 0, 0, inFastMissTime, 0, 0, 0, 0, 0, inLateMissTime) {
			
		}
		
		public override void Evaluate (long timing, Inspector note) {
			if (timing >= -this.inFastMissTime && timing < -this.inPerfectTime) {
				this.MissCallBack(note);
			}
			else if (timing >= -this.inPerfectTime && timing <= this.inPerfectTime) {
				this.PerfectCallBack(note);
			}
			else if (timing > this.inPerfectTime && timing <= this.inLateMissTime) {
				this.MissCallBack(note);
			}
		}
	}



}
