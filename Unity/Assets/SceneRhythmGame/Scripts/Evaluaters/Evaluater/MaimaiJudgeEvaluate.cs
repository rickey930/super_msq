﻿using UnityEngine;
using System.Collections;
using XMaimaiNote.XInspector;

public abstract class MaimaiJudgeEvaluate {
	public MaimaiJudgeEvaluate(MaimaiJudgeEvaluateManager manager) {
		this.manager = manager;
	}

	public MaimaiJudgeEvaluateManager manager { get; private set; }
	public void Release() { 
		manager = null;
	}

	public abstract void Evaluate(long timing, Inspector note);
	public abstract int GetPerfectCount();
	public abstract int GetPerfectScore();
	public abstract int GetGreatCount();
	public abstract int GetGreatScore();
	public abstract int GetGoodCount();
	public abstract int GetGoodScore();
	public abstract int GetMissCount();
	public abstract int GetNoteCount();
}
