using UnityEngine;
using System.Collections;
using XMaimaiNote.XInspector;
using XMaimaiNote.XEvaluateDesigner;

public class MaimaiJudgeEvaluateHoldFoot : MaimaiJudgeEvaluate {
	public MaimaiJudgeEvaluateHoldFoot(MaimaiJudgeEvaluateManager manager, MaimaiJudgeEvaluateHoldHead headEvaluater, long inIntrustHeadEvaluateTime) : base (manager) {
		this.headEvaluater = headEvaluater;
		this.inIntrustHeadEvaluateTime = inIntrustHeadEvaluateTime;
	}

	public MaimaiJudgeEvaluateHoldHead headEvaluater { get; protected set; }
	public long inIntrustHeadEvaluateTime { get; protected set; }

	public override void Evaluate (long timing, Inspector note) {
		var fNote = (Inspector.HoldFoot)note;
		if (fNote.kernel.evaluate != DrawableEvaluateTapType.NONE) {
			//離すのが早い
			if (timing < -this.inIntrustHeadEvaluateTime) {
				this.EarlyPullCallBack(note);
			}
			//タイミングばっちり
			else {
				this.OkPullCallBack(note);
			}
			//離すのが遅いのはミス判定に任せる
		}
	}

	public virtual void EarlyPullCallBack(Inspector iNote) {
		var note = (Inspector.HoldFoot)iNote;
		manager.PlayNoteSE("se_tap_good");
		note.kernel.designer.Hide ();
		note.kernel.holdKeepEffectDesigner.Hide ();
		var evaluateType = DrawableEvaluateTapType.FASTGOOD;
		note.kernel.evaluateTextureDesigner.Show (evaluateType);
		note.kernel.evaluateEffectDesigner.Show (evaluateType);
		headEvaluater.good.count++;
		manager.AddCombo();
		manager.ResetPCombo();
		manager.ResetBPCombo();
		manager.Damage();
		note.judged = true;
		note.kernel.evaluated = true;
		if (note.kernel.eachArchDesigner != null)
			note.kernel.eachArchDesigner.HideIfIndependentState ();
		//シンクはミスにする
		manager.SyncEvaluation(note.kernel, SyncEvaluate.MISS);
	}

	
	public virtual void OkPullCallBack(Inspector iNote) {
		var note = (Inspector.HoldFoot)iNote;
		manager.PlayNoteSE("se_tap_perfect");
		note.kernel.designer.Hide ();
		note.kernel.holdKeepEffectDesigner.Hide ();
		var evaluateType = note.kernel.evaluate;
		if (!manager.resetting) {
			note.kernel.evaluateTextureDesigner.Show (evaluateType);
			note.kernel.evaluateEffectDesigner.Show (evaluateType);
		}
		if (evaluateType == DrawableEvaluateTapType.PERFECT) {
			headEvaluater.perfect.count++;
			manager.AddPCombo();
			manager.AddBPCombo();
			manager.SyncEvaluation(note.kernel, SyncEvaluate.PERFECT);
		}
		else if (evaluateType == DrawableEvaluateTapType.FASTGREAT || 
		         evaluateType == DrawableEvaluateTapType.LATEGREAT) {
			headEvaluater.great.count++;
			manager.ResetPCombo();
			manager.ResetBPCombo();
			if (evaluateType == DrawableEvaluateTapType.FASTGREAT)
				manager.SyncEvaluation(note.kernel, SyncEvaluate.FASTGREAT);
			else
				manager.SyncEvaluation(note.kernel, SyncEvaluate.LATEGREAT);
		}
		else if (evaluateType == DrawableEvaluateTapType.FASTGOOD || 
		         evaluateType == DrawableEvaluateTapType.LATEGOOD) {
			headEvaluater.good.count++;
			manager.ResetPCombo();
			manager.ResetBPCombo();
			if (evaluateType == DrawableEvaluateTapType.FASTGOOD)
				manager.SyncEvaluation(note.kernel, SyncEvaluate.FASTGOOD);
			else
				manager.SyncEvaluation(note.kernel, SyncEvaluate.LATEGOOD);
		}
		manager.AddCombo();
		note.judged = true;
		note.kernel.evaluated = true;
		if (note.kernel.eachArchDesigner != null)
			note.kernel.eachArchDesigner.HideIfIndependentState ();
	}
	
	public virtual void LateCallBack(Inspector iNote) {
		var note = (Inspector.HoldFoot)iNote;
		manager.PlayNoteSE("se_tap_good");
		note.kernel.designer.Hide ();
		note.kernel.holdKeepEffectDesigner.Hide ();
		var evaluateType = DrawableEvaluateTapType.LATEGOOD;
		note.kernel.evaluateTextureDesigner.Show (evaluateType);
		note.kernel.evaluateEffectDesigner.Show (evaluateType);
		headEvaluater.good.count++;
		manager.AddCombo();
		manager.ResetPCombo();
		manager.ResetBPCombo();
		manager.Damage();
		note.judged = true;
		note.kernel.evaluated = true;
		if (note.kernel.eachArchDesigner != null)
			note.kernel.eachArchDesigner.HideIfIndependentState ();
		//シンクはミスにする
		manager.SyncEvaluation(note.kernel, SyncEvaluate.MISS);
	}

	public virtual void MissCallBack(Inspector iNote) {
		var note = (Inspector.HoldFoot)iNote;
		manager.PlayNoteSE("se_tap_good");
		note.kernel.designer.Hide ();
		note.kernel.holdKeepEffectDesigner.Hide ();
		var evaluateType = DrawableEvaluateTapType.MISS;
		note.kernel.evaluateTextureDesigner.Show (evaluateType);
		note.kernel.evaluateEffectDesigner.Show (evaluateType);
		headEvaluater.miss.count++;
		manager.ResetCombo();
		manager.ResetPCombo();
		manager.ResetBPCombo();
		manager.Damage();
		note.judged = true;
		note.kernel.evaluated = true;
		if (note.kernel.eachArchDesigner != null)
			note.kernel.eachArchDesigner.HideIfIndependentState ();
		//シンクはミスにする
		manager.SyncEvaluation(note.kernel, SyncEvaluate.MISS);
	}

	public override int GetPerfectCount() { return this.headEvaluater.GetPerfectCount(); }
	public override int GetPerfectScore() { return this.headEvaluater.GetPerfectScore(); }
	public override int GetGreatCount() { return this.headEvaluater.GetGreatCount(); }
	public override int GetGreatScore() { return this.headEvaluater.GetGreatScore(); }
	public override int GetGoodCount() { return this.headEvaluater.GetGoodCount(); }
	public override int GetGoodScore() { return this.headEvaluater.GetGoodScore(); }
	public override int GetMissCount() { return this.headEvaluater.GetMissCount(); }
	public override int GetNoteCount() { return this.headEvaluater.GetNoteCount (); }
	
	public class Maji : MaimaiJudgeEvaluateHoldFoot {
		public Maji(MaimaiJudgeEvaluateManager manager, MaimaiJudgeEvaluateHoldHead headEvaluater, long inIntrustHeadEvaluateTime, long inMajiHoldOverMissTime)
		: base (manager, headEvaluater, inIntrustHeadEvaluateTime) {
			this.inMajiHoldOverMissTime = inMajiHoldOverMissTime;
		}
		
		public long inMajiHoldOverMissTime { get; protected set; }
		
		public override void Evaluate (long timing, Inspector note) {
			var fNote = (Inspector.HoldFoot)note;
			if (fNote.kernel.evaluate != DrawableEvaluateTapType.NONE) {
				//離すのが早い.
				if (timing < -this.inIntrustHeadEvaluateTime) {
					this.EarlyPullCallBack(note);
				}
				//離したけど遅い.
				else if (timing > this.inMajiHoldOverMissTime) {
					this.LateCallBack(note);
				}
				//タイミングばっちり.
				else {
					this.OkPullCallBack(note);
				}
				//離さなかったのはミス判定に任せる.
			}
		}
	}
	
	public class Gori : Maji {
		public Gori(MaimaiJudgeEvaluateManager manager, MaimaiJudgeEvaluateHoldHead headEvaluater, long inIntrustHeadEvaluateTime, long inMajiHoldOverMissTime)
		: base (manager, headEvaluater, inIntrustHeadEvaluateTime, inMajiHoldOverMissTime) {
			
		}
		
		public override void Evaluate (long timing, Inspector note) {
			var fNote = (Inspector.HoldFoot)note;
			if (fNote.kernel.evaluate != DrawableEvaluateTapType.NONE) {
				//離すのが早い.
				if (timing < -this.inIntrustHeadEvaluateTime) {
					this.MissCallBack(note);
				}
				//離したけど遅い.
				else if (timing > this.inMajiHoldOverMissTime) {
					this.MissCallBack(note);
				}
				//タイミングばっちり.
				else {
					this.OkPullCallBack(note);
				}
				//離さなかったのはミス判定に任せる.
			}
		}
	}


}
