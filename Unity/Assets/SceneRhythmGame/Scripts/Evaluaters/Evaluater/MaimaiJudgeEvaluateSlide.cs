using UnityEngine;
using System.Collections;
using XMaimaiNote.XInspector;
using XMaimaiNote.XEvaluateDesigner;

public class MaimaiJudgeEvaluateSlide : MaimaiJudgeEvaluateCommonMiss {
	//情報はスライド(これ)が持ち、判定するのはスライドパターンの方

	public MaimaiJudgeEvaluateSlide(MaimaiJudgeEvaluateManager manager, MaimaiJudgeEvaluateTap judgeEvaluateStarTap, long backSlideTime,
	                                int perfectRate, int greatRate, int goodRate, 
	                                long inFastGreatTime, long inPerfectTime, long inLateGreatTime, long inLateGoodTime) : base (manager) {
		this.perfect = new MaimaiJudgeEvaluateInfo (perfectRate);
		this.great = new MaimaiJudgeEvaluateInfo (greatRate);
		this.good = new MaimaiJudgeEvaluateInfo (goodRate);
		this.inPerfectTime = Mathf.Abs ((float)inPerfectTime).ToLong ();
		this.inFastGreatTime = Mathf.Abs ((float)inFastGreatTime).ToLong ();
		this.inLateGreatTime = Mathf.Abs ((float)inLateGreatTime).ToLong ();
		this.inLateGoodTime = Mathf.Abs ((float)inLateGoodTime).ToLong ();
		this.inFastGoodTime = judgeEvaluateStarTap.inFastGoodTime;
		this.backSlideTime = backSlideTime;
	}

	public MaimaiJudgeEvaluateInfo perfect { get; protected set; }
	public MaimaiJudgeEvaluateInfo great { get; protected set; }
	public MaimaiJudgeEvaluateInfo good { get; protected set; }
	public long inPerfectTime { get; protected set; }
	public long inFastGreatTime { get; protected set; }
	public long inLateGreatTime { get; protected set; }
	public long inFastGoodTime { get; protected set; }
	public long inLateGoodTime { get; protected set; }
	public virtual long inSlideEvaluateStartTime { get { return inFastGoodTime; } }
	protected long backSlideTime;

	//評価していい時間になっているか。
	public bool IsEvaluateStartTime(long timing, Inspector.Slide note) {
		if (!manager.allowSlideSectionOneSkip || note.kernel.Progress () >= 0 || note == note.kernel.firstNoteInfo)
			//スライドのFastGOOD判定は☆の早GOOD判定から有効 というかスライドのなぞる部分も早GOODから。
			return timing >= (note.kernel.kernel.visualizeTime - note.justTime - inSlideEvaluateStartTime);
		else //1個飛ばしは許可してあり、チェインの0番目か1番目を判定しようとしているときに、いきなり1番目が来たとき
			// ☆ガイドが動く時間から有効. こうすると巻き込みを防止しやすくなる.
			return timing >= note.kernel.kernel.moveStartTime - note.justTime;
	}

	public override void Evaluate(long timing, Inspector note) {
		var sNote = (Inspector.Slide)note;
		//スライドのFastGOOD判定は☆の早GOOD判定から有効 というかスライドのなぞる部分も早GOODから。.
		if (IsEvaluateStartTime(timing, sNote)) {
			// 触れた時間を記憶.
			sNote.lastCensoredTime = manager.GetTimer().gameTime;
			//もし来たノートが最後のノートなら、.
			if (SlidePatternProgression(sNote)) {
				//判定する.
				if (timing < -inFastGreatTime) {
					FastGoodCallBack(note);
				}
				else if (timing >= -inFastGreatTime && timing < -inPerfectTime) {
					FastGreatCallBack(note);
				}
				else if (timing >= -inPerfectTime && timing <= inPerfectTime) {
					PerfectCallBack(note);
				}
				else if (timing > inPerfectTime && timing <= inLateGreatTime) {
					LateGreatCallBack(note);
				}
				else if (timing > inLateGreatTime && timing <= inLateGoodTime) {
					LateGoodCallBack(note, false);
				}
			}
			else {
				//最初のスライドパターンノートであればサウンドエフェクトを鳴らす.
				if (!sNote.kernel.kernel.calledSlideSound && note == sNote.kernel.firstNoteInfo) { 
					manager.PlayNoteSE("se_slide");
					sNote.kernel.kernel.calledSlideSound = true;
				}
			}
			sNote.kernel.designer.SetSlideProgress(sNote.kernel.Progress());
		}
	}

	// すべてのチェインのスライド進捗が完了したら評価していい.
	protected bool SlidePatternProgression(Inspector.Slide sNote) {
		if (SlideChainProgression (sNote)) {
			foreach (var chain in sNote.kernel.kernel.chains) {
				if (!chain.isCompleted) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	/// <summary>
	/// スライドの進捗度を進める. 評価してよければtrueを返す.
	/// </summary>
	protected bool SlideChainProgression(Inspector.Slide note) {
		MaimaiTimer timer = manager.GetTimer ();
		var kernel = note.kernel;

		// スライドノートが所属するチェインは、評価を終了した.
		if (kernel.isCompleted) return true;
		
		// まだ0番目のsectionであるとき.
		if (kernel.Progress () < 0) {
			// 判定したいノートがチェインの[0]番目のノートであれば、
			if (note == kernel.firstNoteInfo) {
				//判定済みにする.
				kernel.firstNoteInfo.judged = true;
				// indexを[1]にする.
				kernel.index = 1;
				// [1]は配列外なら、評価する.
				if (kernel.isCompleted) return true;
				// まだ[2]があるなら、続きがあるのでまだ評価しない.
				if (kernel.index + 1 >= kernel.infoAmount) return false;
			}
			// [1]があって、判定したいノートが[1]のノートであれば、
			else if (manager.allowSlideSectionOneSkip && kernel.infoAmount > 2 && kernel.noteInfos[1] == note) {
				// [0]と[1]を判定済みにする.
				kernel.noteInfos[0].judged = true;
				kernel.noteInfos[1].judged = true;
				// indexを[2]にする.
				kernel.index = 2;
				// [2]は配列外なら、評価する.
				if (kernel.isCompleted) return true;
			}
			// 判定したいノートはまだindexが満ちてないので出直せ.
			else return false;
		}

		// 判定したいノートが今判定してもいいノートの場合、
		if (note == kernel.nowNoteInfo) {
			// 判定済みにしてindexを次に送る.
			kernel.nowNoteInfo.judged = true;
			kernel.index++;
		}
		// インデックスオーバーはしないとき、判定したいノートが次に判定してもいいノートの場合、
		else if (manager.allowSlideSectionOneSkip && kernel.infoAmount - 1 >= kernel.index + 1 && kernel.nextNoteInfo == note) {
			// 1個飛ばしで判定済みにする.indexを次に送る.
			kernel.nowNoteInfo.judged = true;
			kernel.nextNoteInfo.judged = true;
			kernel.index += 2;
		}

		// すべてのsectionが反応したなら、評価する.
		if (kernel.isCompleted) return true;

		// 同時押し対策. ちょっと前に反応したのだったら、ここで判定済みにする.
		while (!kernel.isCompleted) {
			if (!kernel.nowNoteInfo.lastCensoredTime.HasValue) {
				return false;
			}
			if (timer.gameTime - kernel.nowNoteInfo.lastCensoredTime.Value < backSlideTime) {
				kernel.nowNoteInfo.judged = true;
				kernel.index++;
			}
			else {
				return false;
			}
		}
		
		// すべてのsectionが反応したなら、評価する.
		if (kernel.isCompleted) return true;
		// 続きがあるなら、まだ評価しない.
		return false;
	}

	public virtual void FastGoodCallBack(Inspector iNote) {
		var note = (Inspector.Slide)iNote;
		note.kernel.designer.Hide ();
		note.kernel.kernel.evaluateTextureDesigner.Show (DrawableEvaluateSlideType.FASTGOOD);
		this.good.count++;
		manager.AddCombo();
		manager.ResetPCombo();
		manager.ResetBPCombo();
		manager.Damage();
		note.kernel.kernel.SetJudged(true);
		note.kernel.kernel.evaluated = true;
		manager.SyncEvaluation(note.kernel, SyncEvaluate.FASTGOOD);
	}
	public virtual void FastGreatCallBack(Inspector iNote) {
		var note = (Inspector.Slide)iNote;
		note.kernel.designer.Hide ();
		note.kernel.kernel.evaluateTextureDesigner.Show (DrawableEvaluateSlideType.FASTGREAT);
		this.great.count++;
		manager.AddCombo();
		manager.ResetPCombo();
		manager.ResetBPCombo();
		manager.Damage();
		note.kernel.kernel.SetJudged(true);
		note.kernel.kernel.evaluated = true;
		manager.SyncEvaluation(note.kernel, SyncEvaluate.FASTGREAT);
	}
	public virtual void PerfectCallBack(Inspector iNote) {
		var note = (Inspector.Slide)iNote;
		note.kernel.kernel.evaluated = true;
		note.kernel.kernel.designer.Hide ();
		if (!manager.resetting) {
			note.kernel.kernel.evaluateTextureDesigner.Show (DrawableEvaluateSlideType.JUST);
		}
		this.perfect.count++;
		manager.AddCombo();
		manager.AddPCombo();
		manager.AddBPCombo();
		note.kernel.kernel.SetJudged(true);
		manager.SyncEvaluation(note.kernel, SyncEvaluate.PERFECT);
	}
	public virtual void LateGreatCallBack(Inspector iNote) {
		var note = (Inspector.Slide)iNote;
		note.kernel.designer.Hide ();
		note.kernel.kernel.evaluateTextureDesigner.Show (DrawableEvaluateSlideType.LATEGREAT);
		this.great.count++;
		manager.AddCombo();
		manager.ResetPCombo();
		manager.ResetBPCombo();
		manager.Damage();
		note.kernel.kernel.SetJudged(true);
		note.kernel.kernel.evaluated = true;
		manager.SyncEvaluation(note.kernel, SyncEvaluate.LATEGREAT);
	}
	public virtual void LateGoodCallBack(Inspector iNote, bool missCall) {
		var note = (Inspector.Slide)iNote;
		note.kernel.designer.Hide ();
		note.kernel.kernel.evaluateTextureDesigner.Show (DrawableEvaluateSlideType.LATEGOOD);
		this.good.count++; 
		manager.AddCombo(); 
		manager.ResetPCombo();
		manager.ResetBPCombo();
		manager.Damage();
		note.kernel.kernel.SetJudged(true);
		note.kernel.kernel.evaluated = true;
		if (missCall) manager.SyncEvaluation(note.kernel, SyncEvaluate.MISS);
		else manager.SyncEvaluation(note.kernel, SyncEvaluate.LATEGOOD);
	}

	public override void MissCallBack(Inspector iNote) {
		var note = (Inspector.Slide)iNote;
		note.kernel.designer.Hide ();
		note.kernel.kernel.evaluateTextureDesigner.Show (DrawableEvaluateSlideType.TOOLATE);
		this.miss.count++; 
		manager.ResetCombo(); 
		manager.ResetPCombo(); 
		manager.ResetBPCombo();
		manager.Damage();
		note.kernel.kernel.SetJudged(true);
		note.kernel.kernel.evaluated = true;
		manager.SyncEvaluation(note.kernel, SyncEvaluate.MISS);
	}

	public override int GetPerfectCount() { return this.perfect.count; }
	public override int GetPerfectScore() { return this.perfect.score; }
	public override int GetGreatCount() { return this.great.count; }
	public override int GetGreatScore() { return this.great.score; }
	public override int GetGoodCount() { return this.good.count; }
	public override int GetGoodScore() { return this.good.score; }
	public override int GetNoteCount() {
		return GetPerfectCount() + GetGreatCount() + GetGoodCount() + GetMissCount();
	}
	
	public int GetFullScore() {
		return (this.GetPerfectCount() + this.GetGreatCount() + this.GetGoodCount() + this.GetMissCount()) * this.perfect.rate;
	}
	
	public int GetLostScore() {
		return
			(this.perfect.rate - this.perfect.rate) * this.GetPerfectCount() + 
			(this.perfect.rate - this.great.rate) * this.GetGreatCount() + 
			(this.perfect.rate - this.good.rate) * this.GetGoodCount() + 
			(this.perfect.rate - this.miss.rate) * this.GetMissCount();
		
	}
	
	public override void Reset () {
		this.perfect.count = 0;
		this.great.count = 0;
		this.good.count = 0;
		base.Reset ();
	}
	
	public class Maji : MaimaiJudgeEvaluateSlide {
		public Maji(MaimaiJudgeEvaluateManager manager, MaimaiJudgeEvaluateTap judgeEvaluateStarTap, long backSlideTime,
            int perfectRate, int greatRate, int goodRate, 
            long inFastGoodTime, long inFastGreatTime, long inPerfectTime, long inLateGreatTime, long inLateGoodTime, long inLateMissTime)
		: base (manager, judgeEvaluateStarTap, backSlideTime, perfectRate, greatRate, goodRate, inFastGreatTime, inPerfectTime, inLateGreatTime, inLateGoodTime) {
			this.inFastMissTime = judgeEvaluateStarTap.inFastGoodTime;
			this.inFastGoodTime = inFastGoodTime;
			this.inLateMissTime = inLateMissTime;
		}
		
		public long inFastMissTime { get; protected set; }
		public long inLateMissTime { get; protected set; }
		public override long inSlideEvaluateStartTime { get { return inFastMissTime; } }
		
		public virtual void TooFastCallBack(Inspector iNote) {
			var note = (Inspector.Slide)iNote;
			note.kernel.kernel.evaluateTextureDesigner.Show (DrawableEvaluateSlideType.TOOFAST);
			this.miss.count++; 
			manager.ResetCombo(); 
			manager.ResetPCombo(); 
			manager.ResetBPCombo();
			note.kernel.kernel.SetJudged(true);
			note.kernel.kernel.evaluated = true;
			manager.SyncEvaluation(note.kernel, SyncEvaluate.MISS);
		}
		
	}
	
	public class Gori : Maji {
		public Gori(MaimaiJudgeEvaluateManager manager, MaimaiJudgeEvaluateTap judgeEvaluateStarTap, long backSlideTime,
		                                    int perfectRate, 
		                                    long inPerfectTime, long inLateMissTime)
		: base (manager, judgeEvaluateStarTap, backSlideTime, perfectRate, 0, 0, 0, 0, inPerfectTime, 0, 0, inLateMissTime) {
			
		}
	}


}
