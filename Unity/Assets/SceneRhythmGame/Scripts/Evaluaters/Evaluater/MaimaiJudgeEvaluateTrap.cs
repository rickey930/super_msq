﻿using UnityEngine;
using System.Collections;
using XMaimaiNote.XInspector;
using XMaimaiNote.XEvaluateDesigner;

public class MaimaiJudgeEvaluateTrap : MaimaiJudgeEvaluateCommonMiss {
	public MaimaiJudgeEvaluateTrap(MaimaiJudgeEvaluateManager manager, 
	                               int perfectRate, int greatRate, int goodRate,
	                               long inFastGoodTime, long inFastGreatTime)
	: base (manager) {
		this.perfect = new MaimaiJudgeEvaluateInfo (perfectRate);
		this.great = new MaimaiJudgeEvaluateInfo (greatRate);
		this.good = new MaimaiJudgeEvaluateInfo (goodRate);
		this.inFastGreatTime = Mathf.Abs ((float)inFastGreatTime).ToLong();
		this.inFastGoodTime = Mathf.Abs ((float)inFastGoodTime).ToLong();
	}
	
	public MaimaiJudgeEvaluateInfo perfect { get; protected set; }
	public MaimaiJudgeEvaluateInfo great { get; protected set; }
	public MaimaiJudgeEvaluateInfo good { get; protected set; }
	public long inFastGreatTime { get; protected set; }
	public long inFastGoodTime { get; protected set; }
	
	public override void Evaluate (long timing, Inspector note) {
		if (timing >= 0) {
			var trap = (Inspector.Trap)note;
			var touchedTime = trap.justTime + timing;
			var subFoot = trap.kernel.designer.footTime - touchedTime;
			if (subFoot > 0) {
				if (subFoot <= inFastGreatTime) {
					this.FastGreatCallBack(note);
				}
				else if (subFoot <= inFastGoodTime) {
					this.FastGoodCallBack(note);
				}
				else {
					this.MissCallBack(note);
				}
			}
		}
	}

	public override void MissCallBack(Inspector iNote) {
		var note = (Inspector.Trap)iNote;
		manager.PlayNoteSE("se_trap_miss");
		note.kernel.designer.Hide ();
		note.kernel.evaluateTextureDesigner.Show (DrawableEvaluateTapType.MISS);
		base.MissCallBack (note);
		manager.SyncEvaluation(note.kernel, SyncEvaluate.MISS);
	}
	public virtual void FastGoodCallBack(Inspector iNote) {
		var note = (Inspector.Trap)iNote;
		manager.PlayNoteSE("se_tap_good");
		note.kernel.designer.Hide ();
		var evaluateType = DrawableEvaluateTapType.FASTGOOD;
		note.kernel.evaluateTextureDesigner.Show (evaluateType);
		note.kernel.evaluateEffectDesigner.Show (evaluateType);
		this.good.count++;
		manager.AddCombo();
		manager.ResetPCombo();
		manager.ResetBPCombo();
		manager.Damage();
		note.judged = true;
		note.kernel.evaluated = true;
		manager.SyncEvaluation(note.kernel, SyncEvaluate.FASTGOOD);
		manager.SetDrawableCommentNote(note);
	}
	public virtual void FastGreatCallBack(Inspector iNote) {
		var note = (Inspector.Trap)iNote;
		manager.PlayNoteSE("se_tap_great");
		note.kernel.designer.Hide ();
		var evaluateType = DrawableEvaluateTapType.FASTGREAT;
		note.kernel.evaluateTextureDesigner.Show (evaluateType);
		note.kernel.evaluateEffectDesigner.Show (evaluateType);
		this.great.count++;
		manager.AddCombo();
		manager.ResetPCombo();
		manager.ResetBPCombo();
		manager.Damage();
		note.judged = true;
		note.kernel.evaluated = true;
		manager.SyncEvaluation(note.kernel, SyncEvaluate.FASTGREAT);
		manager.SetDrawableCommentNote(note);
	}
	public virtual void PerfectCallBack(Inspector iNote) {
		var note = (Inspector.Trap)iNote;
		manager.PlayNoteSE("se_tap_perfect");
		note.kernel.designer.Hide ();
		var evaluateType = DrawableEvaluateTapType.PERFECT;
		if (!manager.resetting) {
			note.kernel.evaluateTextureDesigner.Show (evaluateType);
			note.kernel.evaluateEffectDesigner.Show (evaluateType);
		}
		this.perfect.count++;
		manager.AddCombo();
		manager.AddPCombo();
		manager.AddBPCombo();
		note.judged = true;
		note.kernel.evaluated = true;
		manager.SyncEvaluation(note.kernel, SyncEvaluate.PERFECT);
		manager.SetDrawableCommentNote(note);
	}
	
	public override int GetPerfectCount() { return this.perfect.count; }
	public override int GetPerfectScore() { return this.perfect.score; }
	public override int GetGreatCount() { return this.great.count; }
	public override int GetGreatScore() { return this.great.score; }
	public override int GetGoodCount() { return this.good.count; }
	public override int GetGoodScore() { return this.good.score; }
	public override int GetNoteCount() {
		return GetPerfectCount() + GetGreatCount() + GetGoodCount() + GetMissCount();
	}
	
	public int GetFullScore() {
		return (this.GetPerfectCount() + this.GetGreatCount() + this.GetGoodCount() + this.GetMissCount()) * this.perfect.rate;
	}
	
	public int GetLostScore() {
		return
			(this.perfect.rate - this.perfect.rate) * this.GetPerfectCount() + 
				(this.perfect.rate - this.great.rate) * this.GetGreatCount() + 
				(this.perfect.rate - this.good.rate) * this.GetGoodCount() + 
				(this.perfect.rate - this.miss.rate) * this.GetMissCount();
	}
	
	public override void Reset () {
		this.perfect.count = 0;
		this.great.count = 0;
		this.good.count = 0;
		base.Reset ();
	}
	
	public class Gori : MaimaiJudgeEvaluateTrap {
		public Gori(MaimaiJudgeEvaluateManager manager,
		            int perfectRate)
		: base (manager, perfectRate, 0, 0, 0, 0) {
			
		}
		
		public override void Evaluate (long timing, Inspector note) {
			if (timing >= 0) {
				this.MissCallBack(note);
			}
		}
	}
}
