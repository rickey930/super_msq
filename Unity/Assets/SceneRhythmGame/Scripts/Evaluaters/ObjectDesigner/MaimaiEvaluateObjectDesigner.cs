﻿using UnityEngine;
using System.Collections.Generic;
using XMaimaiNote.XKernel;
using XMaimaiNote.XDocument;
using XMaimaiNote.XInspector;

namespace XMaimaiNote {
	namespace XEvaluateDesigner {
		public class EvaluateTextureDesigner : IMaimaiObjectDesigner {

			protected Constants.SensorId sensorId { get; set; }
			protected RectTransform canvas { get; private set; }
			protected bool isBasePos { get; set; }
			protected bool ignoreImgAngle { get; set; }
			public XMaimaiNote.XObjectDesigner.CommonInformation commonInfo { get; private set; }

			private MaimaiEvaluateTextureController _createdObjectCtrl;
			private MaimaiEvaluateTextureController createdObjectCtrl { 
				get {
					return _createdObjectCtrl;
				}
				set {
					_createdObjectCtrl = value;
					_createdObjectCtrl.gameObject.SetActive (false);
				}
			}

			protected float pos {
				get {
					if (isBasePos) {
						return Constants.instance.EVALUATE_SHOW_RADIUS;
					}
					var circum = Constants.instance.SensorCircumference(sensorId);
					if (circum == Constants.Circumference.OUTER)
						return Constants.instance.MAIMAI_OUTER_RADIUS;
					else if (circum == Constants.Circumference.INNER)
						return Constants.instance.MAIMAI_INNER_RADIUS;
					else
						return 0;
				}
			}

			protected float angle {
				get {
					return Constants.instance.GetSensorDegFromId(sensorId);
				}
			}

			public EvaluateTextureDesigner(RectTransform canvas, XMaimaiNote.XObjectDesigner.CommonInformation commonInfo, int buttonId) {
				this.canvas = canvas;
				this.commonInfo = commonInfo;
				this.sensorId = Constants.OuterSensorIds[buttonId - 1];
				this.isBasePos = true;
				this.ignoreImgAngle = false;
			}
			
			public EvaluateTextureDesigner(RectTransform canvas, XMaimaiNote.XObjectDesigner.CommonInformation commonInfo, Constants.SensorId sensorId) {
				this.canvas = canvas;
				this.commonInfo = commonInfo;
				this.sensorId = sensorId;
				this.isBasePos = false;
				this.ignoreImgAngle = true;
			}

			public virtual void Create () {
				var obj = GameObject.Instantiate (commonInfo.prefabs.evaluateTexturePrefab) as GameObject;
				obj.transform.SetParentEx (canvas);
				createdObjectCtrl = obj.GetComponent<MaimaiEvaluateTextureController> ();
				createdObjectCtrl.Setup1 (pos, angle, ignoreImgAngle, commonInfo.realtimeCreateObject);
			}

			public virtual void Show (DrawableEvaluateTapType type) {
				if (commonInfo.realtimeCreateObject) {
					Release ();
					Create ();
				}
				if (createdObjectCtrl != null) {
					string name = GetTapSpriteName (type);
					Sprite sprite = SpriteTank.Get (name);
					createdObjectCtrl.Setup2 (sprite);
					createdObjectCtrl.Show ();
				}
			}
			
			public virtual void Show (DrawableEvaluateBreakType type) {
				if (commonInfo.realtimeCreateObject) {
					Release ();
					Create ();
				}
				if (createdObjectCtrl != null) {
					string name = GetBreakSpriteName (type);
					Sprite sprite = SpriteTank.Get (name);
					createdObjectCtrl.Setup2 (sprite);
					createdObjectCtrl.Show ();
				}
			}

			public virtual void Hide () {
				if (createdObjectCtrl != null) {
					if (commonInfo.realtimeCreateObject) {
						Release();
					}
					else {
						createdObjectCtrl.Hide ();
					}
				}
			}

			public virtual void Reset () {
				if (createdObjectCtrl != null) {
					createdObjectCtrl.Reset();
				}
			}

			public virtual void Release () {
				if (createdObjectCtrl != null) {
					createdObjectCtrl.Release ();
					_createdObjectCtrl = null;
				}
			}
			
			protected virtual string GetTapSpriteName (DrawableEvaluateTapType type) {
				switch (type) {
				case DrawableEvaluateTapType.PERFECT:
					return "judge_perfect";
				case DrawableEvaluateTapType.FASTGREAT:
				case DrawableEvaluateTapType.LATEGREAT:
					return "judge_great";
				case DrawableEvaluateTapType.FASTGOOD:
				case DrawableEvaluateTapType.LATEGOOD:
					return "judge_good";
				case DrawableEvaluateTapType.MISS:
					return "judge_miss";
				}
				return null;
			}
			
			protected virtual string GetBreakSpriteName (DrawableEvaluateBreakType type) {
				switch (type) {
				case DrawableEvaluateBreakType.P2600:
					return "judge_break_2600";
				case DrawableEvaluateBreakType.P2550F:
				case DrawableEvaluateBreakType.P2550L:
					return "judge_break_2550";
				case DrawableEvaluateBreakType.P2500F:
				case DrawableEvaluateBreakType.P2500L:
					return "judge_break_2500";
				case DrawableEvaluateBreakType.P2000F:
				case DrawableEvaluateBreakType.P2000L:
					return "judge_break_2000";
				case DrawableEvaluateBreakType.P1500F:
				case DrawableEvaluateBreakType.P1500L:
					return "judge_break_1500";
				case DrawableEvaluateBreakType.P1250F:
				case DrawableEvaluateBreakType.P1250L:
					return "judge_break_1250";
				case DrawableEvaluateBreakType.P1000F:
				case DrawableEvaluateBreakType.P1000L:
					return "judge_break_1000";
				case DrawableEvaluateBreakType.MISS:
					return "judge_miss";
				}
				return null;
			}
		}

		public class EvaluateEffectDesigner : EvaluateTextureDesigner {
			public EvaluateEffectDesigner(RectTransform canvas, XMaimaiNote.XObjectDesigner.CommonInformation commonInfo, int buttonId)
			: base (canvas, commonInfo, buttonId) {
				isBasePos = false;
			}
			public EvaluateEffectDesigner(RectTransform canvas, XMaimaiNote.XObjectDesigner.CommonInformation commonInfo, Constants.SensorId sensorId)
			: base (canvas, commonInfo, sensorId) {
				isBasePos = false;
			}

			private MaimaiEvaluateEffectController _createdObjectCtrl;
			protected MaimaiEvaluateEffectController createdObjectCtrl { 
				get {
					return _createdObjectCtrl;
				}
				set {
					_createdObjectCtrl = value;
					_createdObjectCtrl.gameObject.SetActive (false);
				}
			}
			public override void Create () {
				var obj = GameObject.Instantiate (commonInfo.prefabs.evaluateEffectPrefab) as GameObject;
				obj.transform.SetParentEx (canvas);
				createdObjectCtrl = obj.GetComponent<MaimaiEvaluateEffectController> ();
				createdObjectCtrl.Setup1 (pos, angle, ignoreImgAngle, commonInfo.realtimeCreateObject);
			}
			
			public override void Show (DrawableEvaluateTapType type) {
				if (commonInfo.realtimeCreateObject) {
					Release ();
					Create ();
				}
				if (createdObjectCtrl != null) {
					string name = commonInfo.visualizeFastLate ? GetFSTapSpriteName (type) : GetTapSpriteName (type);
					Sprite sprite = SpriteTank.Get (name);
					createdObjectCtrl.Setup2 (sprite);
					createdObjectCtrl.Show ();
				}
			}
			
			public override void Show (DrawableEvaluateBreakType type) {
				if (commonInfo.realtimeCreateObject) {
					Release ();
					Create ();
				}
				if (createdObjectCtrl != null) {
					string name = commonInfo.visualizeFastLate ? GetFSBreakSpriteName (type) : GetBreakSpriteName (type);
					Sprite sprite = SpriteTank.Get (name);
					createdObjectCtrl.Setup2 (sprite);
					createdObjectCtrl.Show ();
				}
			}
			
			public override void Hide () {
				if (createdObjectCtrl != null) {
					if (commonInfo.realtimeCreateObject) {
						Release();
					}
					else {
						createdObjectCtrl.Hide ();
					}
				}
			}
			
			public override void Reset () {
				if (createdObjectCtrl != null) {
					createdObjectCtrl.Reset();
				}
			}
			
			public override void Release () {
				if (createdObjectCtrl != null) {
					createdObjectCtrl.Release ();
					_createdObjectCtrl = null;
				}
			}
			
			protected override string GetTapSpriteName(DrawableEvaluateTapType type) {
				switch (type) {
				case DrawableEvaluateTapType.PERFECT:
					return "effect_perfect";
				case DrawableEvaluateTapType.FASTGREAT:
				case DrawableEvaluateTapType.LATEGREAT:
					return "effect_great";
				case DrawableEvaluateTapType.FASTGOOD:
				case DrawableEvaluateTapType.LATEGOOD:
					return "effect_good";
				}
				return null;
			}
			
			protected virtual string GetFSTapSpriteName(DrawableEvaluateTapType type) {
				switch (type) {
				case DrawableEvaluateTapType.PERFECT:
					return "effect_perfect";
				case DrawableEvaluateTapType.FASTGREAT:
				case DrawableEvaluateTapType.FASTGOOD:
					return "effect_fast";
				case DrawableEvaluateTapType.LATEGREAT:
				case DrawableEvaluateTapType.LATEGOOD:
					return "effect_slow";
				}
				return null;
			}
			
			protected override string GetBreakSpriteName(DrawableEvaluateBreakType type) {
				switch (type) {
				case DrawableEvaluateBreakType.P2600:
					return "effect_break_2600";
				case DrawableEvaluateBreakType.P2550F:
				case DrawableEvaluateBreakType.P2550L:
				case DrawableEvaluateBreakType.P2500F:
				case DrawableEvaluateBreakType.P2500L:
					return "effect_break_perfect";
				case DrawableEvaluateBreakType.P2000F:
				case DrawableEvaluateBreakType.P2000L:
				case DrawableEvaluateBreakType.P1500F:
				case DrawableEvaluateBreakType.P1500L:
				case DrawableEvaluateBreakType.P1250F:
				case DrawableEvaluateBreakType.P1250L:
					return "effect_break_great";
				case DrawableEvaluateBreakType.P1000F:
				case DrawableEvaluateBreakType.P1000L:
					return "effect_break_good";
				}
				return null;
			}
			
			protected virtual string GetFSBreakSpriteName(DrawableEvaluateBreakType type) {
				switch (type) {
				case DrawableEvaluateBreakType.P2600:
					return "effect_perfect";
				case DrawableEvaluateBreakType.P2550F:
				case DrawableEvaluateBreakType.P2500F:
				case DrawableEvaluateBreakType.P2000F:
				case DrawableEvaluateBreakType.P1500F:
				case DrawableEvaluateBreakType.P1250F:
				case DrawableEvaluateBreakType.P1000F:
					return "effect_fast";
				case DrawableEvaluateBreakType.P2550L:
				case DrawableEvaluateBreakType.P2500L:
				case DrawableEvaluateBreakType.P2000L:
				case DrawableEvaluateBreakType.P1500L:
				case DrawableEvaluateBreakType.P1250L:
				case DrawableEvaluateBreakType.P1000L:
					return "effect_slow";
				}
				return null;
			}
		}

		public class HoldKeepEffectDesigner : EvaluateEffectDesigner {
			public HoldKeepEffectDesigner(RectTransform canvas, XMaimaiNote.XObjectDesigner.CommonInformation commonInfo, int buttonId)
			: base (canvas, commonInfo, buttonId) {
				
			}
			public override void Create () {
				var obj = GameObject.Instantiate (commonInfo.prefabs.holdKeepEffectPrefab) as GameObject;
				obj.transform.SetParentEx (canvas);
				createdObjectCtrl = obj.GetComponent<MaimaiEvaluateEffectController> ();
				createdObjectCtrl.Setup1 (pos, angle, ignoreImgAngle);
			}
			
			public override void Show (DrawableEvaluateTapType type) {
				if (commonInfo.realtimeCreateObject) {
					Release ();
					Create ();
				}
				if (createdObjectCtrl != null) {
					Sprite sprite = GetHoldKeepSprite (type);
					createdObjectCtrl.Setup2 (sprite);
					createdObjectCtrl.Show ();
				}
			}
			
			protected virtual Sprite GetHoldKeepSprite(DrawableEvaluateTapType type) {
				switch (type) {
				case DrawableEvaluateTapType.PERFECT:
					return MaipadDynamicCreatedSpriteTank.instance.holdingPerfectCircleEffect;
				case DrawableEvaluateTapType.FASTGREAT:
				case DrawableEvaluateTapType.LATEGREAT:
					return MaipadDynamicCreatedSpriteTank.instance.holdingGreatCircleEffect;
				case DrawableEvaluateTapType.FASTGOOD:
				case DrawableEvaluateTapType.LATEGOOD:
					return MaipadDynamicCreatedSpriteTank.instance.holdingGoodCircleEffect;
				}
				return null;
			}
		}


		public class SlideEvaluateTextureDesigner : IMaimaiObjectDesigner {
			public Document.Slide.EvaluateTexturePosition[] evTexPositons { get; set; }
			public XMaimaiNote.XObjectDesigner.CommonInformation commonInfo { get; private set; }
			protected RectTransform canvas { get; private set; }
			private bool isWideSlide { get; set; }
			
			private MaimaiSlideEvaluateTextureController _createdObjectCtrl;
			private MaimaiSlideEvaluateTextureController createdObjectCtrl { 
				get {
					return _createdObjectCtrl;
				}
				set {
					_createdObjectCtrl = value;
					_createdObjectCtrl.gameObject.SetActive (false);
				}
			}

			public SlideEvaluateTextureDesigner (RectTransform canvas, XMaimaiNote.XObjectDesigner.CommonInformation commonInfo, Document.Slide.EvaluateTexturePosition[] evTexPositons) {
				this.canvas = canvas;
				this.commonInfo = commonInfo;
				this.evTexPositons = evTexPositons;
			}

			public void Create () {
				var obj = GameObject.Instantiate (commonInfo.prefabs.evaluateSlidePrefab) as GameObject;
				obj.transform.SetParentEx (canvas);

				var filter = obj.GetComponentInChildren<MeshFilter> ();
				var vertices = filter.mesh.vertices;
				var uvs = filter.mesh.uv;
				var textureHeight = Constants.instance.SLIDE_EVALUATE_TEXTURE_HEIGHT;
				// マーカーの位置から頂点を求める.
				for (int i = 0; i < evTexPositons.Length; i++) {
					var evTexPos = evTexPositons [i];
					this.isWideSlide = evTexPos.isWideSlide;
					float repair = evTexPos.isWideSlide ? 0.35f : 0.0f; //ワイドスライドは仕様上位置が微妙なので、vertexの方で調整する.
					// 上.
					var up = CircleCalculator.PointOnCircle (evTexPos.position, textureHeight * (0.5f + repair), evTexPos.rotation.y).ChangePosHandSystem ();
					vertices [i].x = up.x;
					vertices [i].y = up.y;
					uvs[i].y = 1 - uvs[i].y; //力(vertex)の上下向きが変わったので、uvも上下反転する.
					if (!evTexPos.isPositive) uvs[i].x = 1 - uvs[i].x; //バックテクスチャを使う場合、uvの左右反転もする.
					// 下.
					var down = CircleCalculator.PointOnCircle (evTexPos.position, textureHeight * (0.5f - repair), evTexPos.rotation.y + 180).ChangePosHandSystem ();
					vertices [vertices.Length - 1 - i].x = down.x;
					vertices [vertices.Length - 1 - i].y = down.y;
					uvs[uvs.Length - 1 - i].y = 1 - uvs[uvs.Length - 1 - i].y;
					if (!evTexPos.isPositive) uvs[uvs.Length - 1 - i].x = 1 - uvs[uvs.Length - 1 - i].x;
				}
				filter.mesh.vertices = vertices;
				filter.mesh.uv = uvs;
				// レンダラーに情報を付与する.
				var renderer = obj.GetComponentInChildren<SlideEvaluateTextureMeshRenderer> ();
				renderer.slideEvaluaterMesh = filter.mesh;

				createdObjectCtrl = obj.GetComponent<MaimaiSlideEvaluateTextureController> ();
			}

			public void Show (DrawableEvaluateSlideType type) {
				if (commonInfo.realtimeCreateObject) {
					Release ();
					Create ();
				}
				if (createdObjectCtrl != null) {
					if (!isWideSlide) {
						string[] names = GetSlideSpriteNames (type);
						if (names == null)
							return;
						Sprite[] sprites = new Sprite[] { SpriteTank.Get (names [0]), SpriteTank.Get (names [1]) };
						createdObjectCtrl.Setup (sprites, commonInfo.realtimeCreateObject);
						createdObjectCtrl.gameObject.SetActive (true);
					}
					else {
						string name = GetWideSlideSpriteNames(type);
						Sprite sprite = SpriteTank.Get (name);
						Sprite[] sprites = new Sprite[] { sprite, sprite };
						createdObjectCtrl.Setup (sprites, commonInfo.realtimeCreateObject);
						createdObjectCtrl.gameObject.SetActive (true);
					}
				}
			}
			
			public virtual void Hide () {
				if (createdObjectCtrl != null) {
					if (commonInfo.realtimeCreateObject) {
						Release();
					}
					else {
						createdObjectCtrl.Hide ();
					}
				}
			}
			
			public virtual void Reset () {
				if (createdObjectCtrl != null) {
					createdObjectCtrl.Reset();
				}
			}
			
			public virtual void Release () {
				if (createdObjectCtrl != null) {
					createdObjectCtrl.Release ();
					_createdObjectCtrl = null;
				}
			}
			
			protected virtual string[] GetSlideSpriteNames(DrawableEvaluateSlideType type) {
				switch (type) {
				case DrawableEvaluateSlideType.JUST:
					return new string[] { "judge_slide_just", "judge_slide_just_flip" };
				case DrawableEvaluateSlideType.FASTGREAT:
					return new string[] { "judge_slide_great_fast", "judge_slide_great_fast_flip" };
				case DrawableEvaluateSlideType.LATEGREAT:
					return new string[] { "judge_slide_great_late", "judge_slide_great_late_flip" };
				case DrawableEvaluateSlideType.FASTGOOD:
					return new string[] { "judge_slide_good_fast", "judge_slide_good_fast_flip" };
				case DrawableEvaluateSlideType.LATEGOOD:
					return new string[] { "judge_slide_good_late", "judge_slide_good_late_flip" };
				case DrawableEvaluateSlideType.TOOFAST:
					return new string[] { "judge_slide_miss_fast", "judge_slide_miss_fast_flip" };
				case DrawableEvaluateSlideType.TOOLATE:
					return new string[] { "judge_slide_miss_late", "judge_slide_miss_late_flip" };
				}
				return null;
			}

			protected virtual string GetWideSlideSpriteNames(DrawableEvaluateSlideType type) {
				switch (type) {
				case DrawableEvaluateSlideType.JUST:
					return "judge_slide_wide_just";
				case DrawableEvaluateSlideType.FASTGREAT:
					return "judge_slide_wide_great_fast";
				case DrawableEvaluateSlideType.LATEGREAT:
					return "judge_slide_wide_great_late";
				case DrawableEvaluateSlideType.FASTGOOD:
					return "judge_slide_wide_good_fast";
				case DrawableEvaluateSlideType.LATEGOOD:
					return "judge_slide_wide_good_late";
				case DrawableEvaluateSlideType.TOOFAST:
					return "judge_slide_wide_miss_fast";
				case DrawableEvaluateSlideType.TOOLATE:
					return "judge_slide_wide_miss_late";
				}
				return null;
			}

		}

		public enum DrawableEvaluateTapType {
			PERFECT, FASTGREAT, LATEGREAT, FASTGOOD, LATEGOOD, MISS, NONE
		}
		public enum DrawableEvaluateSlideType {
			JUST, FASTGREAT, LATEGREAT, FASTGOOD, LATEGOOD, TOOFAST, TOOLATE, NONE
		}
		public enum DrawableEvaluateBreakType {
			P2600, P2550F, P2550L, P2500F, P2500L, P2000F, P2000L, P1500F, P1500L,  P1250F, P1250L, P1000F,P1000L, MISS, NONE
		}

	}
}