﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

[RequireComponent( typeof( CanvasRenderer ) )]
[RequireComponent( typeof( RectTransform ) )]
public class SlideEvaluateTextureMeshRenderer : Graphic {

	public Mesh slideEvaluaterMesh { protected get; set; }

	public void SetTextures (Sprite main, Sprite flip) {
		this.material.SetTexture ("_FrontTex", main.texture);
		this.material.SetTexture ("_BackTex", flip.texture);
	}
	
	/// <summary>
	/// ここでメッシュを生成する
	/// </summary>
	/// <param name="vbo">Vbo.</param>
	protected override void OnFillVBO(List<UIVertex> vbo) {
		UIVertex v = UIVertex.simpleVert;
		var mesh = this.slideEvaluaterMesh;
		if (mesh == null || mesh.vertexCount < 4) { 
			base.OnFillVBO(vbo);
			return;
		}
		
		for (int i = 0; i < mesh.vertices.Length / 2; i++) {
			// 左下
			v.position = mesh.vertices[mesh.vertices.Length - 1 - i];
			v.uv0 = mesh.uv[mesh.uv.Length - 1 - i];
			vbo.Add (v);
			// 右下
			v.position = mesh.vertices[mesh.vertices.Length - 2 - i];
			v.uv0 = mesh.uv[mesh.uv.Length - 2 - i];
			vbo.Add (v);
			// 右上
			v.position = mesh.vertices[1 + i];
			v.uv0 = mesh.uv[1 + i];
			vbo.Add (v);
			// 左上
			v.position = mesh.vertices[0 + i];
			v.uv0 = mesh.uv[0 + i];
			vbo.Add (v);
		}
	}
	
	/// <summary>
	/// メッシュに設定するテクスチャの指定
	/// </summary>
	public override Texture mainTexture {
		get {
			return base.material.mainTexture;
		}
	}
}
