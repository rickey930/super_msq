﻿using UnityEngine;
using System.Collections;

public class MaimaiEvaluateEffectController : MonoBehaviour {
	[SerializeField]
	private RectTransform angleElement;
	[SerializeField]
	private RectTransform positionElement;
	[SerializeField]
	private UnityEngine.UI.Image imageElement;
	[SerializeField]
	private TimeAnimationScale[] timeAnimationScale;
	[SerializeField]
	private TimeAnimationAlpha[] timeAnimationAlpha;

	public void Setup1 (float pos, float angle, bool ignoreImgAngle) {
		angleElement.localRotation = Quaternion.Euler (0, 0, Constants.instance.ToLeftHandedCoordinateSystemDegree(angle));
		positionElement.anchoredPosition = new Vector2 (0, pos);
		if (ignoreImgAngle) {
			imageElement.rectTransform.localRotation = Quaternion.Euler (0, 0, Constants.instance.ToLeftHandedCoordinateSystemDegree(-angle));
		}
	}
	
	public void Setup1 (float pos, float angle, bool ignoreImgAngle, bool realtimeCreateObjectMode) {
		Setup1(pos, angle, ignoreImgAngle);
		if (realtimeCreateObjectMode) {
			foreach (var ta in timeAnimationScale) {
				ta.clearanceType = ClearanceType.DESTROY;
			}
		}
		else {
			foreach (var ta in timeAnimationScale) {
				ta.clearanceType = ClearanceType.DEACTIVE;
			}
		}
	}
	
	public void Setup2 (Sprite image) {
		imageElement.sprite = image;
	}

	public void Show () {
		this.gameObject.SetActive (true);
	}
	
	public void Hide () {
		this.gameObject.SetActive (false);
	}
	
	public void Reset () {
		foreach (var t in timeAnimationAlpha) {
			t.Restart();
		}
		foreach (var t in timeAnimationScale) {
			t.Restart();
		}
	}
	
	public void Release () {
		Destroy (this.gameObject);
	}
}