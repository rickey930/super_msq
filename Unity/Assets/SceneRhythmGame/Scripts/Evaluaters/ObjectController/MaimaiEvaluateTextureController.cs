﻿using UnityEngine;
using System.Collections;

public class MaimaiEvaluateTextureController : MonoBehaviour {
	[SerializeField]
	private RectTransform angleElement;
	[SerializeField]
	private RectTransform positionElement;
	[SerializeField]
	private UnityEngine.UI.Image imageElement;
	[SerializeField]
	private TimeAnimationScale timeAnimationScale;
	[SerializeField]
	private TimeAnimationAlpha timeAnimationAlpha;

	public void Setup1 (float pos, float angle, bool ignoreImgAngle, bool realtimeCreateObjectMode) {
		angleElement.localRotation = Quaternion.Euler (0, 0, Constants.instance.ToLeftHandedCoordinateSystemDegree(angle));
		positionElement.anchoredPosition = new Vector2 (0, pos);
		if (ignoreImgAngle) {
			imageElement.rectTransform.localRotation = Quaternion.Euler (0, 0, Constants.instance.ToLeftHandedCoordinateSystemDegree(-angle));
		}
		if (realtimeCreateObjectMode) {
			timeAnimationAlpha.clearanceType = ClearanceType.DESTROY;
		}
		else {
			timeAnimationAlpha.clearanceType = ClearanceType.DEACTIVE;
		}
	}

	public void Setup2 (Sprite image) {
		imageElement.sprite = image;
	}
	
	public void Show () {
		this.gameObject.SetActive (true);
	}
	
	public void Hide () {
		this.gameObject.SetActive (false);
	}

	public void Reset () {
		timeAnimationAlpha.Restart();
		timeAnimationScale.Restart();
	}
	
	public void Release () {
		Destroy (this.gameObject);
	}
}