﻿using UnityEngine;
using System.Collections;

public class MaimaiSlideEvaluateTextureController : MonoBehaviour {
	[SerializeField]
	private new SlideEvaluateTextureMeshRenderer renderer;
	[SerializeField]
	private TimeAnimationAlpha timeAnimationAlpha;

	private Material copyMaterial;

	public void Setup (Sprite[] images, bool realtimeCreateObjectMode) {
		this.copyMaterial = new Material (renderer.material);
		renderer.material = copyMaterial;
		renderer.SetTextures (images [0], images [1]);
		if (realtimeCreateObjectMode) {
			timeAnimationAlpha.clearanceType = ClearanceType.DESTROY;
		}
		else {
			timeAnimationAlpha.clearanceType = ClearanceType.DEACTIVE;
		}
	}

	void OnDestroy () {
		if (copyMaterial != null) {
			GameObject.Destroy (copyMaterial);
		}
	}
	
	public void Show () {
		this.gameObject.SetActive (true);
	}
	
	public void Hide () {
		this.gameObject.SetActive (false);
	}
	
	public void Reset () {
		timeAnimationAlpha.Restart();
	}
	
	public void Release () {
		Destroy (this.gameObject);
	}
}
