﻿namespace RhythmGameLibrary {
	public class RhythmGameNote {
		public long justTime { get; set; }
		public long missTime { get; set; }
		public bool judged { get; set; }
		public bool justTimeElapsed { get; set; } //JustTimeは過ぎ去ったフラグ.
		public long judgementableTime { get; set; } //この時間が過ぎたらJudgeしていいよ.

		/// <summary>
		/// RhythmGameNote コンストラクタ.
		/// </summary>
		/// <param name="justTime">判定タイム</param>
		/// <param name="missTimeFromJustTimeRelative">判定タイムから何ミリ秒経ったらミスか</param>
		public RhythmGameNote(long justTime, long missTimeFromJustTimeRelative) {
			this.justTime = justTime;
			this.missTime = missTimeFromJustTimeRelative;
			this.judged = false;
			this.justTimeElapsed = false;
			this.judgementableTime = 0;
		}

		public virtual long overrideJustProcTime () {
			return justTime;
		}

	}
}
