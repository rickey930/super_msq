using System;
using System.Collections.Generic;

namespace RhythmGameLibrary {
	public class RhythmGameEvaluate<NoteActionKey, NoteClass> where NoteClass : RhythmGameNote {
		/// <summary>
		/// 判定遅延補正.
		/// </summary>
		public long timeLag { get; set; }
		/// <summary>
		/// DJ GUIDEさん用判定遅延補正 (音声とか).
		/// </summary>
		public long justTimeLag { get; set; }

		protected RhythmGameTimer timer;
		protected long oldTime;
		protected Dictionary<NoteActionKey, NoteClass[]> notes; //[action][index]
		protected Dictionary<NoteActionKey, int> notesMissIndex;
		protected Dictionary<NoteActionKey, int> notesJustIndex;
		protected Dictionary<NoteActionKey, int> notesGuideIndex;
		protected Dictionary<NoteActionKey, NoteClass[]> judgementableTimeSortedNotes;
		protected Dictionary<NoteActionKey, int> notesJudgementableIndex;

		public RhythmGameEvaluate(RhythmGameTimer timer, Dictionary<NoteActionKey, NoteClass[]> notes, long timeLag, long justTimeLag) {
			this.timer = timer;
			this.timeLag = timeLag;
			this.justTimeLag = justTimeLag;
			this.notes = new Dictionary<NoteActionKey, NoteClass[]>();
			notesMissIndex = new Dictionary<NoteActionKey, int>();
			notesJustIndex = new Dictionary<NoteActionKey, int> ();
			notesGuideIndex = new Dictionary<NoteActionKey, int>();
			judgementableTimeSortedNotes = new Dictionary<NoteActionKey, NoteClass[]>();
			notesJudgementableIndex = new Dictionary<NoteActionKey, int>();
			foreach (NoteActionKey key in notes.Keys) {
				// インデックス初期化.
				notesMissIndex.Add(key, 0);
				notesJustIndex.Add(key, 0);
				notesGuideIndex.Add(key, 0);
				notesJudgementableIndex.Add(key, 0);
				// ノートをジャストタイム順に並べ替える
				var n = notes[key];
				System.Array.Sort<NoteClass> (n, (note1, note2)=>{
					return note1.justTime.CompareTo(note2.justTime);
				});
				this.notes[key] = n;
				// ノートを判定許可時間順に並べる
				var jn = n.Clone() as NoteClass[];
				Array.Sort<NoteClass> (jn, (note1, note2)=>{
					return note1.judgementableTime.CompareTo (note2.judgementableTime);
				});
				judgementableTimeSortedNotes[key] = jn;
			}
		}
 
		/// <summary>
		/// 判定されてない、一番近いノートを返す.
		/// </summary>
		/// <param name="actionId">アクションID</param>
		/// <returns></returns>
		public NoteClass JudgeNote(NoteActionKey actionId) {
			if (!notes.ContainsKey(actionId)) return null; // そのアクションIDのノートが存在しない場合.

			long time = timer.gameTime;
			int index = notesMissIndex[actionId];
			NoteClass[] searchNotes = notes[actionId];
			long min = long.MaxValue;
			long diff = 0L;
			long abs = 0L;
			NoteClass find = null;
			long findDiff = 0L;
			long findOldDiff = 0L;
			for (int i = index; i < searchNotes.Length; i++) {
				if (!searchNotes[i].judged) {
					diff = time - (searchNotes[i].justTime + timeLag);
					abs = Math.Abs(diff);
					if (min > abs) {
						if (time >= searchNotes[i].judgementableTime + timeLag) { // Judgeしていい時間ならば.
							min = abs;
							find = searchNotes[i];
							findDiff = diff;
							// 反応した時間では+JUSTを過ぎちゃったけど、前回更新時には、まだJUST圏内だったとか処理落ちでJUST範囲を飛んだとかなら、JUST判定にする。そのための数値.
							findOldDiff = oldTime - (searchNotes[i].justTime + timeLag);
						}
					}
					else break;
				}
			}
			if (find != null) Evaluate(findDiff, findOldDiff, find);
			return find;
		}

		/// <summary>
		/// <para>判定されておらず、判定していい時間になったノートをすべて返す</para>.
		/// <para>このメソッドを使うアクションIDを持つノートには、RhythmGameNote.judgementableTimeを設定しておくこと.</para>
		/// </summary>
		/// <param name="actionId">アクションID</param>
		/// <returns></returns>
		public NoteClass[] JudgementableNotes(NoteActionKey actionId) {
			if (!notes.ContainsKey(actionId)) return null; // そのアクションIDのノートが存在しない場合.
			
			long time = timer.gameTime;
			int index = notesJudgementableIndex[actionId];
			NoteClass[] searchNotes = judgementableTimeSortedNotes[actionId];
			List<NoteClass> find = new List<NoteClass>();
			List<long> findDiff = new List<long>();
			List<long> findOldDiff = new List<long>();
			bool nextIndexDecided = false;
			for (int i = index; i < searchNotes.Length; i++) {
				if (!searchNotes[i].judged) {
					if (!nextIndexDecided) {
						notesJudgementableIndex[actionId] = i; //searchNotesの中でjudgedできてない最小indexを次のサーチfor開始indexにする.
						nextIndexDecided = true;
					}
					if (time >= searchNotes[i].judgementableTime + timeLag) { // Judgeしていい時間ならば.
						find.Add (searchNotes[i]);
						findDiff.Add (time - (searchNotes[i].justTime + timeLag));
						// 反応した時間では+JUSTを過ぎちゃったけど、前回更新時には、まだJUST圏内だったとか処理落ちでJUST範囲を飛んだとかなら、JUST判定にする。そのための数値.
						findOldDiff.Add (oldTime - (searchNotes[i].justTime + timeLag));
					}
					else break;
				}
			}
			for (int i = 0; i < find.Count; i++) Evaluate(findDiff[i], findOldDiff[i], find[i]);
			return find.ToArray();
		}

		/// <summary>
		/// 判定したノートに対する評価.
		/// </summary>
		/// <param name="timing">ジャストタイミングとの差</param>
		/// <param name="oldTiming">1フレーム前の時間とジャスト時間の差</param>
		/// <param name="note">判定したノート</param>
		protected virtual void Evaluate(long timing, long oldTiming, NoteClass note) {

		}
		
		/// <summary>
		/// 次の更新のための準備.
		/// </summary>
		public void NextUpdatePrepare() {
			oldTime = timer.gameTime;
		}

		/// <summary>
		/// ミスノートを自動判定.
		/// </summary>
		public void AutoMissProc() {
			long time = timer.gameTime;
			int index;
			NoteClass[] ns;
			foreach (NoteActionKey actionId in notes.Keys) {
				index = notesMissIndex[actionId];
				ns = notes[actionId];
				for (int i = index; i < ns.Length; i++) {
					NoteClass n = ns[i];
					if (time >= (n.justTime + timeLag) + n.missTime) {
						if (!n.judged) {
							OnMiss(n);
							if (n.judged) index = i;
							else break; // ミスと判断されなければ中断.
						}
					}
					else {
						break; //判定時間に来ていないノートならミス判定終了.
					}
				}
				notesMissIndex[actionId] = index;
			}
		}

		/// <summary>
		/// ミスが見つかったら呼ばれるメソッド.
		/// </summary>
		/// <param name="note">ミスしたノート.</param>
		protected virtual void OnMiss(NoteClass note) {
			note.judged = true;
		}
		
		/// <summary>
		/// DJ AUTOさんのターン.
		/// </summary>
		public void AutoJustProc() {
			long time = timer.gameTime;
			int index;
			NoteClass[] ns;
			foreach (NoteActionKey actionId in notes.Keys) {
				index = notesJustIndex[actionId];
				ns = notes[actionId];
				for (int i = index; i < ns.Length; i++) {
					NoteClass n = ns[i];
					if (time >= n.overrideJustProcTime() + timeLag) {
						if (!n.judged) {
							OnJust(n);
							if (n.judged) index = i;
							else break; // JUSTと判断されなければ中断.
						}
					}
					else {
						break; //判定時間に来ていないノートならJUST判定終了.
					}
				}
				notesJustIndex[actionId] = index;
			}
		}
		
		/// <summary>
		/// DJ AUTOさんがノートを処理するときに呼ばれるメソッド.
		/// </summary>
		/// <param name="note">処理するノート.</param>
		protected virtual void OnJust(NoteClass note) {
			note.judged = true;
		}

		/// <summary>
		/// DJ GUIDEさんのターン.
		/// </summary>
		public void AutoGuideProc() {
			long time = timer.gameTime;
			int index;
			NoteClass[] ns;
			foreach (NoteActionKey actionId in notes.Keys) {
				index = notesGuideIndex[actionId];
				ns = notes[actionId];
				for (int i = index; i < ns.Length; i++) {
					NoteClass n = ns[i];
					if (time >= n.justTime - justTimeLag) {
						if (!n.justTimeElapsed) {
							OnGuide(n);
						}
					}
					else {
						break;
					}
				}
				notesGuideIndex[actionId] = index;
			}
		}
		
		/// <summary>
		/// DJ GUIDEさんがノートを処理するときに呼ばれるメソッド.
		/// </summary>
		/// <param name="note">処理するノート.</param>
		protected virtual void OnGuide(NoteClass note) {
			note.justTimeElapsed = true;
		}
		
		/// <summary>
		/// 指定した時間からの評価をリセットする.
		/// </summary>
		public void ResetEvaluate(long gameTime) {
			ResetEvaluatePrepare ();
			long time = gameTime;
			int justIndex, missIndex, guideIndex;
			bool justIndexStopped, missIndexStopped;
			NoteClass[] ns;
			foreach (NoteActionKey actionId in notes.Keys) {
				justIndex = missIndex = guideIndex = 0;
				justIndexStopped = missIndexStopped = false;
				ns = notes[actionId];
				for (int i = 0; i < ns.Length; i++) {
					NoteClass n = ns[i];
					
					// JustTime.
					bool justTimeBefore = time > n.justTime + timeLag;
					OnResetJust(n, justTimeBefore);
					if (justTimeBefore && !justIndexStopped && n.judged) justIndex = i;
					else justIndexStopped = true;
					
					// MissTime.
					bool missTimeBefore = time >= (n.justTime + timeLag) + n.missTime;
					OnResetMiss(n, missTimeBefore);
					if (missTimeBefore && !missIndexStopped && n.judged) missIndex = i;
					else missIndexStopped = true;
					
					// GuideTime.
					bool guideTimeBefore = time >= n.justTime - justTimeLag;
					OnResetGuide(n, guideTimeBefore);
					if (guideTimeBefore) guideIndex = i;
				}
				notesJustIndex[actionId] = justIndex;
				notesMissIndex[actionId] = missIndex;
				notesGuideIndex[actionId] = guideIndex;
			}
		}
		
		/// <summary>
		/// 評価リセットの下準備を行うメソッド (点数を初期化したりだとか).
		/// </summary>
		protected virtual void ResetEvaluatePrepare () {
			
		}
		
		/// <summary>
		/// DJ AUTOさんが評価をリセットするときに呼ばれるメソッド. OnResetMissと処理がかぶってほしくないときはOnResetMissも上書きして何もしないようにする.
		/// </summary>
		/// <param name="note">リセットするノート.</param>
		/// <param name="guideTimeBefore">指定した時間より前のノートか.</param>
		protected virtual void OnResetJust(NoteClass note, bool justTimeBefore) {
			note.judged = justTimeBefore;
		}
		
		/// <summary>
		/// Mr.ZERO-POINTSさんが評価をリセットするときに呼ばれるメソッド. OnResetJustと処理がかぶってほしくないときはOnResetJustも上書きして何もしないようにする.
		/// </summary>
		/// <param name="note">リセットするノート.</param>
		/// <param name="guideTimeBefore">指定した時間より前のノートか.</param>
		protected virtual void OnResetMiss(NoteClass note, bool missTimeBefore) {
			note.judged = missTimeBefore;
		}
		
		/// <summary>
		/// DJ GUIDEさんが評価をリセットするときに呼ばれるメソッド.
		/// </summary>
		/// <param name="note">リセットするノート.</param>
		/// <param name="guideTimeBefore">指定した時間より前のノートか.</param>
		protected virtual void OnResetGuide(NoteClass note, bool guideTimeBefore) {
			note.justTimeElapsed = guideTimeBefore;
		}
		
		
	}
}