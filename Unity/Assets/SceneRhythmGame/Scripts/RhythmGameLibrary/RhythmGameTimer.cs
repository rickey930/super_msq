﻿using IUtility;

namespace RhythmGameLibrary {
	public class RhythmGameTimer {
		/// <summary>
		/// ゲーム時間
		/// </summary>
		public long gameTime { get; private set; }
		/// <summary>
		/// ポーズ
		/// </summary>
		public bool pause { get; set; }
		/// <summary>
		/// ゲーム終了時間が過ぎたか
		/// </summary>
		public bool finish { get; private set; }
		/// <summary>
		/// リスタートしてから1度もtimeUpdateしてない状態である.
		/// </summary>
		public bool restarted { get; private set; }
		private long seek;
		private long wait;
		private IStopwatch beginTimer;
		private IStopwatch playingTimer;
		private IStopwatch afterTimer;
		private IAudioManager audio;
		private TimerState state;
		private long oldCurrentPosition;
		private long finishTime;
		private bool audioRollbacked;
		private bool endless;
		private bool firstStart;
		private long beginTimerSeek;
		private long afterTimerSeek;
		private bool forceUseAfterTimer;

		private enum TimerState {
			BEGIN, PLAYING, AFTER
		}

		/// <summary>
		/// リズムゲーム用タイマー
		/// </summary>
		/// <param name="audioManager">オーディオの再生や停止などの機能を持つマネージャ</param>
		/// <param name="timer1">新しいIStopwatchの実装クラスを適当に作って渡すこと</param>
		/// <param name="timer2">新しいIStopwatchの実装クラスを適当に作って渡すこと</param>
		/// <param name="timer3">新しいIStopwatchの実装クラスを適当に作って渡すこと</param>
		public RhythmGameTimer(IAudioManager audioManager, IStopwatch timer1, IStopwatch timer2, IStopwatch timer3) {
			seek = 0;
			wait = 0;
			beginTimerSeek = 0;
			afterTimerSeek = 0;
			beginTimer = timer1;
			playingTimer = timer2;
			afterTimer = timer3;
			audio = audioManager;
			endless = false;
			firstStart = true;
			forceUseAfterTimer = false;
		}

		private void initCommon(long seek, long wait, long finishTime) {
			if (!audio.useAudio () || seek >= audio.getDuration ()) {
				forceUseAfterTimer = true;
			}
			else {
				this.seek = seek;
				this.wait = wait;
				forceUseAfterTimer = false;
			}
			this.finishTime = finishTime;
			beginTimerSeek = 0;
			afterTimerSeek = 0;
			beginTimer.stop();
			playingTimer.stop();
			afterTimer.stop();
			oldCurrentPosition = -1;
			audioRollbacked = false;
			pause = false;
			endless = false;
			finish = false;
			if (firstStart) {
				firstStart = false;
				restarted = false;
			}
			else {
				restarted = true;
			}
			if (audio.useAudio() && audio.isPlaying()) {
				audio.pause();
			}
		}

		/// <summary>
		/// 最初から再生の準備
		/// </summary>
		/// <param name="seek">オーディオファイルに対するシーク</param>
		/// <param name="wait">ゲーム時間が何秒経過してからオーディオを再生するか</param>
		/// <param name="finishTime">ゲーム終了時間</param>
		public void initialize(long seek, long wait, long finishTime) {
			initialize (seek, wait, finishTime, 0);
		}

		/// <summary>
		/// 途中から再生の準備
		/// </summary>
		/// <param name="seek">オーディオファイルに対するシーク</param>
		/// <param name="wait">ゲーム時間が何秒経過してからオーディオを再生するか</param>
		/// <param name="finishTime">ゲーム終了時間</param>
		/// <param name="initTime">ゲーム時間シーク</param>
		public void initialize(long seek, long wait, long finishTime, long initTime) {
			initCommon(seek, wait, finishTime);
			long totalseek = initTime - this.wait + this.seek;
			if (forceUseAfterTimer || totalseek >= audio.getDuration()) {
				state = TimerState.AFTER;
				afterTimerSeek = initTime;
			}
			else if (wait > initTime) {
				state = TimerState.BEGIN;
				beginTimerSeek = initTime;
				audio.seek(this.seek);
			}
			else {
				state = TimerState.PLAYING;
				if (totalseek > 0) {
					audio.seek(totalseek);
				}
				else {
					audio.seek(0);
				}
			}
		}

		public void initialize_ModeEndless() {
			initCommon (0, 0, 0);
			state = TimerState.AFTER;
			endless = true;
		}
		
		public void initialize_ModeEndless(long initTime) {
			initialize_ModeEndless();
			afterTimerSeek = initTime;
		}

		/// <summary>
		/// ゲーム時間を更新
		/// </summary>
		public void timeUpdate() {
			restarted = false;

			if (state == TimerState.BEGIN) {
				if (!pause && !beginTimer.isRunning()) {
					beginTimer.start();
				}
				if (pause && beginTimer.isRunning()) {
					beginTimer.pause();
				}
				gameTime = beginTimer.now() + beginTimerSeek;

				if (gameTime >= wait) {
					if (audio.useAudio()) {
						state = TimerState.PLAYING;
						if (!pause) audio.start();
					}
					else {
						state = TimerState.AFTER;
						if (!pause) afterTimer.start();
						afterTimerSeek = 0;
					}
					beginTimer.stop();
				}
			}
			else if (state == TimerState.PLAYING) {
				if (!audio.useAudio()) {
					state = TimerState.AFTER;
					if (!pause) afterTimer.start();
					gameTime = wait + afterTimer.now();
					return;
				}

				if (!pause && !audio.isPlaying() && !audioRollbacked) {
					audio.start();
				}
				if (pause && audio.isPlaying()) {
					audio.pause();
				}
				if (pause && playingTimer.isRunning()) {
					playingTimer.stop();
				}

				long cp;
				if (!audioRollbacked) {
					// 音楽の時間を取得.
					cp = audio.getTime();
				}
				else {
					// 音楽が巻き戻った経験があるなら、現在時間ではなく長さを取得.
					cp = audio.getDuration();
				}
				long pt = 0;
				// 音楽の時間が前と変わらない.
				if (cp == oldCurrentPosition) {
					if (!pause && !playingTimer.isRunning()) {
						// 補正タイマースタート.
						playingTimer.start();
					}
					pt = playingTimer.now();
				}
				// 音楽の時間が進んだ.
				else if (cp > oldCurrentPosition) {
					if (playingTimer.isRunning()) {
						// 補正タイマーストップ＆リセット.
						playingTimer.stop();
						playingTimer.reset();
					}
				}
				// 音楽の時間が戻った (音楽が止まってtimeが0になった可能性がある).
				else if (cp < oldCurrentPosition) {
					// カレントポジションは音楽の長さである.
					cp = audio.getDuration();
					// 戻ったフラグを立てる
					audioRollbacked = true;
					// 補正タイマーストップ＆リセット.
					if (playingTimer.isRunning()) {
						playingTimer.stop();
						playingTimer.reset();
					}
				}
				gameTime = cp - seek + wait + pt;
				oldCurrentPosition = cp;

				if (gameTime > audio.getDuration() || audioRollbacked) {
					state = TimerState.AFTER;
					if (!pause) afterTimer.start();
					audio.pause();
					afterTimerSeek = gameTime;
				}
			}
			else if (state == TimerState.AFTER) {
				if (!pause && !afterTimer.isRunning()) {
					afterTimer.start();
				}
				if (pause && afterTimer.isRunning()) {
					afterTimer.pause();
				}
				gameTime = afterTimer.now() + afterTimerSeek;
			}

			if (gameTime >= finishTime && !endless) {
				finish = true;
				if (beginTimer.isRunning()) {
					beginTimer.stop();
				}
				if (playingTimer.isRunning()) {
					playingTimer.stop();
				}
				if (afterTimer.isRunning()) {
					afterTimer.stop();
				}
				if (audio.useAudio()) {
					audio.pause();
				}
			}

		}

	}
}
