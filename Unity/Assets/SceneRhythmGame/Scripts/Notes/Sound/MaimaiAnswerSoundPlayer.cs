using UnityEngine;
using System.Collections;

public class MaimaiAnswerSoundPlayer : IMaimaiSchedule {

	protected long time { get; set; }
	protected string seName { get; set; }
	protected int sourceIndex { get; set; }

	public MaimaiAnswerSoundPlayer(long time, ConfigTypes.AnswerSound asType, XMaimaiNote.XInspector.InspectorType nType, int sourceIndex) {
		this.time = time;
		this.sourceIndex = sourceIndex;
		if (nType == XMaimaiNote.XInspector.InspectorType.TAP ||
			nType == XMaimaiNote.XInspector.InspectorType.HOLD_HEAD ||
		    nType == XMaimaiNote.XInspector.InspectorType.TRAP)
			this.seName = GetTapAnswerSoundName (asType);
		else if (nType == XMaimaiNote.XInspector.InspectorType.HOLD_FOOT)
			this.seName = GetHoldFootAnswerSoundName (asType);
		else if (nType == XMaimaiNote.XInspector.InspectorType.BREAK)
			this.seName = GetBreakAnswerSoundName (asType);
		else if (nType == XMaimaiNote.XInspector.InspectorType.SLIDE)
			this.seName = GetSlideMoveStartAnswerSoundName (asType);
		else
			this.seName = null;
	}

	public long GetScheduleTime() {
		return time;
	}
	
	public long GetScheduleEndTime() {
		return GetScheduleTime();
	}

	public void ScheduleToDo(long nowTime) {
		if (seName != null) {
			Utility.SoundEffectManager.Play (seName);
		}
	}

	public int GetSourceIndex() {
		return sourceIndex;
	}

	public static string GetTapAnswerSoundName (ConfigTypes.AnswerSound type) {
		if (type == ConfigTypes.AnswerSound.BASIS ||
		    type == ConfigTypes.AnswerSound.BASIS_PLUS) {
			return "se_answersound";
		}
		else if (type == ConfigTypes.AnswerSound.SPECIAL ||
		         type == ConfigTypes.AnswerSound.SPECIAL_PLUS) {
			return "se_simai_tap";
		}
		return null;
	}

	public static string GetHoldFootAnswerSoundName (ConfigTypes.AnswerSound type) {
		if (type == ConfigTypes.AnswerSound.BASIS ||
		    type == ConfigTypes.AnswerSound.BASIS_PLUS) {
			return "se_answersound";
		}
		else if (type == ConfigTypes.AnswerSound.SPECIAL ||
		         type == ConfigTypes.AnswerSound.SPECIAL_PLUS) {
			return "se_simai_holdfoot";
		}
		return null;
	}
	
	public static string GetSlideMoveStartAnswerSoundName (ConfigTypes.AnswerSound type) {
		if (type == ConfigTypes.AnswerSound.BASIS_PLUS ||
		    type == ConfigTypes.AnswerSound.SPECIAL_PLUS) {
			return "se_simai_slide";
		}
		return null;
	}
	
	public static string GetBreakAnswerSoundName (ConfigTypes.AnswerSound type) {
		if (type == ConfigTypes.AnswerSound.BASIS ||
		    type == ConfigTypes.AnswerSound.BASIS_PLUS) {
			return "se_answersound";
		}
		else if (type == ConfigTypes.AnswerSound.SPECIAL ||
		         type == ConfigTypes.AnswerSound.SPECIAL_PLUS) {
			return "se_simai_break";
		}
		return null;
	}
}
