﻿using UnityEngine;
using System.Collections;

public class RhythmGameBeatCheckPlayer : IMaimaiSchedule {
	
	protected long time { get; set; }
	
	public RhythmGameBeatCheckPlayer(long time) {
		this.time = time;
	}
	
	public long GetScheduleTime() {
		return time;
	}
	
	public long GetScheduleEndTime() {
		return GetScheduleTime();
	}
	
	public void ScheduleToDo(long nowTime) {
		Utility.SoundEffectManager.Play("se_startcount");
	}
	
	public int GetSourceIndex() {
		return 0;
	}

	public static RhythmGameBeatCheckPlayer[] CreateData (float bpm, int beatAmount, long beatCountStartTime) {
		if (bpm == 0) return new RhythmGameBeatCheckPlayer[0];

		var data = new RhythmGameBeatCheckPlayer[beatAmount];
		for (int i = 0; i < beatAmount; i++) {
			long time = (long)(((60.0M / (decimal)bpm) * (decimal)i) * 1000.0M) + beatCountStartTime;
			data[i] = new RhythmGameBeatCheckPlayer(time);
		}
		return data;
	}
}
