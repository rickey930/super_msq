﻿using UnityEngine;
using System.Collections.Generic;

namespace XMaimaiNote {
	namespace XDocument {
		
		public enum GuideColor {
			SINGLE, EACH,
		}
		
		public enum Type {
			TAP, HOLD, SLIDE, BREAK, MESSAGE, SCROLL_MESSAGE, SOUND_MESSAGE, TRAP, 
		}

		public enum Alignment {
			LEFT, CENTER, RIGHT
		}

		public abstract class Document {
			public abstract Type type { get; }

			public class Button {
				private int id { get; set; }
				public Button(int id) {
					this.id = id;
				}
				public int GetId() { return id; }
				public int GetIndex() { return id - 1; }
			}
			/// <summary>
			/// 非表示ノートである.
			/// </summary>
			public bool secret { get; set; }

			public string uniqueId { get; set; }

			public interface IEach {
				int? archId { get; set; }
			}

			public class Break : Document, IEach {
				public enum GuideRingShapeType {
					CIRCLE, STAR, WSTAR
				}
				public override Type type { get { return Type.BREAK; } }
				public long justTime { get; set; }
				public Button button { get; set; }
				public float guideSpinSpeed { get; set; }
				public GuideRingShapeType guideShapeType { get; set; }
				public long guideFadeSpeed { get; set; }
				public long guideMoveSpeed { get; set; }
				public float guideScale { get; set; }
				public int? archId { get; set; }

				public int actionId { get { return Constants.instance.MAIMAI_NOTE_ACTION_ID_BASE + button.GetIndex(); } }
			}

			public class Tap : Break {
				public override Type type { get { return Type.TAP; } }
				public GuideColor guideColor { get; set; }
			}

			public class Hold : Document, IEach {
				public override Type type { get { return Type.HOLD; } }
				public Button button { get; set; }
				public GuideColor guideColor { get; set; }
				public long headTime { get; set; }
				public long footTime { get; set; }
				public long guideFadeSpeed { get; set; }
				public long guideMoveSpeed { get; set; }
				public float guideScale { get; set; }
				public int? archId { get; set; }
				
				public int headActionId { get { return Constants.instance.MAIMAI_NOTE_ACTION_ID_BASE + button.GetIndex(); } }
				public int footActionId { get { return Constants.instance.MAIMAI_NOTE_HOLD_FOOT_ACTION_ID_BASE + button.GetIndex(); } }
			}

			public class Slide : Document {
				public override Type type { get { return Type.SLIDE; } }
				public Pattern pattern { get; set; }

				/*ドキュメントの場合マーカーの書き方より、マーカーの位置をそのまま持ったほうがよくない？*/

				public class Pattern {
					public Chain[] chains { get; set; }
					public GuideColor markerColor { get; set; }
					public long justTime { get; set; }
					public long visualizeTime { get; set; }
					public long moveStartTime { get; set; }
					public long moveEndTime { get; set; }
					public long fadeSpeed { get; set; }
					public float guideScale { get; set; }
					public float markerScale { get; set; }
					public bool secret { get; set; }
					public EvaluateTexturePosition[] evaluateTexturePositions { get; set; }
				}

				public class Chain {
					public Command[] commands { get; set; }
					public float totalDistance { get; set; }
					public float guideTotalDistance { get; set; }
					public Section[] sections { get; set; }
					public Marker[] markers { get; set; }
					/// <summary>
					/// <para>表面である.</para>
					/// <para>ガイドが優先して上に表示される.</para>
					/// </summary>
					public bool isSurface { get; set; }
				}

				public class WideMarker {
					public Chain chain { get; set; }
					public Marker marker { get; set; }
				}

				public class Section {
					public Constants.SensorId sensor { get; set; }
					public int actionId { get { return Constants.instance.GetSlideActionId(sensor); } }
				}
				
				public class Marker {
					public Vector2 position { get; set; }
					public float degree { get; set; }
					/// <summary>
					/// 始点からの距離
					/// </summary>
					public float distanceFromStart { get; set; }
				}

				public class EvaluateTexturePosition {
					public Vector2 position { get; set; }
					/// <summary>
					/// xは進行方向, yは画像の角度.
					/// </summary>
					public Vector2 rotation { get; set; }
					/// <summary>
					/// 進行方向がプラスであるか (テクスチャは表を使うのか).
					/// </summary>
					public bool isPositive { get; set; }
					/// <summary>
					/// ワイドスライド用テクスチャであるか.
					/// </summary>
					public bool isWideSlide { get; set; }
				}

				public abstract class Command {
					public enum Type {
						STRAIGHT, CURVE
					}
					public abstract Type type { get; }
					/// <summary>
					/// <para>このコマンドの距離.</para>
					/// </summary>
					public abstract float distance { get; set; }
					/// <summary>
					/// <para>このコマンドの始点までの距離.</para>
					/// <para>このコマンド自体の距離は含めてない</para>
					/// </summary>
					public abstract float totalDistanceToPreviousCommand { get; set; }
					/// <summary>
					/// <para>このコマンドの終点までの合計距離.</para>
					/// <para>このコマンド自体の距離を含める</para>
					/// </summary>
					public abstract float totalDistanceToThisCommand { get; }

					public class Straight : Command {
						public override Type type { get { return Type.STRAIGHT; } }
						public Vector2 start { get; set; }
						public Vector2 target { get; set; }
						/// <summary>
						/// <para>このコマンドの距離.</para>
						/// </summary>
						public override float distance { get; set; }
						/// <summary>
						/// <para>このコマンドの始点までの距離.</para>
						/// <para>このコマンド自体の距離は含めてない</para>
						/// </summary>
						public override float totalDistanceToPreviousCommand { get; set; }
						/// <summary>
						/// <para>このコマンドの終点までの合計距離.</para>
						/// <para>このコマンド自体の距離を含める</para>
						/// </summary>
						public override float totalDistanceToThisCommand { get { return distance + totalDistanceToPreviousCommand; } }
					}

					public class Curve : Command {
						public override Type type { get { return Type.CURVE; } }
						public Vector2 axis { get; set; }
						public float radius { get; set; }
						public float degOfStart { get; set; }
						public float degOfDistance { get; set; }
						/// <summary>
						/// <para>このコマンドの距離.</para>
						/// </summary>
						public override float distance { get; set; }
						/// <summary>
						/// <para>このコマンドの始点までの距離.</para>
						/// <para>このコマンド自体の距離は含めてない</para>
						/// </summary>
						public override float totalDistanceToPreviousCommand { get; set; }
						/// <summary>
						/// <para>このコマンドの終点までの合計距離.</para>
						/// <para>このコマンド自体の距離を含める</para>
						/// </summary>
						public override float totalDistanceToThisCommand { get { return distance + totalDistanceToPreviousCommand; } }
					}
				}
			}


			public class Message : Document {
				public override Type type { get { return Type.MESSAGE; } }
				public string message { get; set; }
				public Vector2 position { get; set; }
				public float scale { get; set; }
				public Color color { get; set; }
				public long viewStartTime { get; set; }
				public long viewEndTime { get; set; }
				public Alignment alignment { get; set; }
			}

			public class ScrollMessage : Document {
				public override Type type { get { return Type.SCROLL_MESSAGE; } }
				public string message { get; set; }
				public float y { get; set; }
				public float scale { get; set; }
				public Color color { get; set; }
				public long viewStartTime { get; set; }
				public long viewEndTime { get; set; }
				public Alignment alignment { get; set; }
			}

			public class SoundMessage : Document {
				public override Type type { get { return Type.SOUND_MESSAGE; } }
				public string soundId { get; set; }
				public long playStartTime { get; set; }
			}
			
			public class Trap : Document {
				public override Type type { get { return Type.TRAP; } }
				public Constants.SensorId sensor { get; set; }
				public long headTime { get; set; }
				public long footTime { get; set; }
				public long guideFadeSpeed { get; set; }
				public float guideScale { get; set; }

				public int actionId { get { return Constants.instance.GetSlideActionId(sensor); } }
			}
		}
	}
}
