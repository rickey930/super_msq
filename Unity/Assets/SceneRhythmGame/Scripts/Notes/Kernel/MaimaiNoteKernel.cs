﻿using UnityEngine;
using System.Collections.Generic;
using XMaimaiNote.XInspector;
using XMaimaiNote.XObjectDesigner;

namespace XMaimaiNote {
	namespace XKernel {
		public abstract class Kernel {
			public string uniqueId { get; set; }
			/// <summary>
			/// カーネル作成順.
			/// </summary>
			public int sourceIndex { get; set; }

			public enum KernelType {
				TAP, HOLD, SLIDE, BREAK, MESSAGE, SCROLL_MESSAGE, TRAP, 
			}
			public abstract KernelType GetNoteType ();

			public bool evaluated { get; set; }
			public Inspector noteInfo { get; set; }
			public ObjectDesigner designer { get; set; }

			public Kernel(string uniqueId, int sourceIndex) {
				this.uniqueId = uniqueId;
				this.sourceIndex = sourceIndex;
				evaluated = false;
			}

			public virtual void Reset () {
				evaluated = false;
				if (noteInfo != null) {
					noteInfo.Reset ();
				}
			}
			
			public virtual void Release () {
				if (noteInfo != null) {
					noteInfo.Release ();
					noteInfo = null;
					designer = null;
				}
			}

			public class Tap : Kernel {
				public override KernelType GetNoteType () { return KernelType.TAP; }
				public new Inspector.Tap noteInfo { get { return (Inspector.Tap)base.noteInfo; } set { base.noteInfo = value; } }
				public new ObjectDesigner.Tap designer { get { return (ObjectDesigner.Tap)base.designer; } set { base.designer = value; } }
				public XEvaluateDesigner.EvaluateTextureDesigner evaluateTextureDesigner { get; set; }
				public XEvaluateDesigner.EvaluateEffectDesigner evaluateEffectDesigner { get; set; }
				public EachArchDesigner eachArchDesigner { get; set; }
				
				public Tap(string uniqueId, int sourceIndex) : base(uniqueId, sourceIndex) {
				}

				public void Setup (Inspector.Tap noteInfo, ObjectDesigner.Tap designer, XEvaluateDesigner.EvaluateTextureDesigner evaluateTextureDesigner, XEvaluateDesigner.EvaluateEffectDesigner evaluateEffectDesigner) {
					this.noteInfo = noteInfo;
					this.designer = designer;
					this.evaluateTextureDesigner = evaluateTextureDesigner;
					this.evaluateEffectDesigner = evaluateEffectDesigner;
				}

				public override void Reset () {
					base.Reset();
					evaluateTextureDesigner.Reset();
					evaluateEffectDesigner.Reset();
					evaluateTextureDesigner.Hide();
					evaluateEffectDesigner.Hide();
				}
			}

			public class Break : Kernel {
				public override KernelType GetNoteType () { return KernelType.BREAK; }
				public new Inspector.Break noteInfo { get { return (Inspector.Break)base.noteInfo; } set { base.noteInfo = value; } }
				public new ObjectDesigner.Break designer { get { return (ObjectDesigner.Break)base.designer; } set { base.designer = value; } }
				public XEvaluateDesigner.EvaluateTextureDesigner evaluateTextureDesigner { get; set; }
				public XEvaluateDesigner.EvaluateEffectDesigner evaluateEffectDesigner { get; set; }
				public EachArchDesigner eachArchDesigner { get; set; }

				public Break(string uniqueId, int sourceIndex) : base(uniqueId, sourceIndex) {
				}
				
				public void Setup (Inspector.Break noteInfo, ObjectDesigner.Break designer, XEvaluateDesigner.EvaluateTextureDesigner evaluateTextureDesigner, XEvaluateDesigner.EvaluateEffectDesigner evaluateEffectDesigner) {
					this.noteInfo = noteInfo;
					this.designer = designer;
					this.evaluateTextureDesigner = evaluateTextureDesigner;
					this.evaluateEffectDesigner = evaluateEffectDesigner;
				}
				
				public override void Reset () {
					base.Reset();
					evaluateTextureDesigner.Reset();
					evaluateEffectDesigner.Reset();
					evaluateTextureDesigner.Hide();
					evaluateEffectDesigner.Hide();
				}
			}

			public class Hold : Kernel {
				public override KernelType GetNoteType () { return KernelType.HOLD; }
				public Inspector.HoldHead headNoteInfo { get { return (Inspector.HoldHead)base.noteInfo; } set { base.noteInfo = value; } }
				public Inspector.HoldFoot footNoteInfo { get; set; }
				public new ObjectDesigner.Hold designer { get { return (ObjectDesigner.Hold)base.designer; } set { base.designer = value; } }
				public XEvaluateDesigner.EvaluateTextureDesigner evaluateTextureDesigner { get; set; }
				public XEvaluateDesigner.EvaluateEffectDesigner evaluateEffectDesigner { get; set; }
				public XEvaluateDesigner.HoldKeepEffectDesigner holdKeepEffectDesigner { get; set; }
				public EachArchDesigner eachArchDesigner { get; set; }

				//評価用.
				//ヘッドでの評価を記憶する（点数に反映されるのはフットのとき）.
				public XMaimaiNote.XEvaluateDesigner.DrawableEvaluateTapType evaluate { get; private set; }

				public Hold(string uniqueId, int sourceIndex) : base(uniqueId, sourceIndex) {
					this.evaluate = XMaimaiNote.XEvaluateDesigner.DrawableEvaluateTapType.NONE;
				}

				public void Setup (Inspector.HoldHead headInfo, Inspector.HoldFoot footInfo, ObjectDesigner.Hold designer, XEvaluateDesigner.EvaluateTextureDesigner evaluateTextureDesigner, XEvaluateDesigner.EvaluateEffectDesigner evaluateEffectDesigner, XEvaluateDesigner.HoldKeepEffectDesigner holdKeepEffectDesigner) {
					this.headNoteInfo = headInfo;
					this.footNoteInfo = footInfo;
					this.designer = designer;
					this.evaluateTextureDesigner = evaluateTextureDesigner;
					this.evaluateEffectDesigner = evaluateEffectDesigner;
					this.holdKeepEffectDesigner = holdKeepEffectDesigner;
				}

				public override void Reset () {
					base.Reset ();
					if (footNoteInfo != null) {
						footNoteInfo.Reset ();
					}
					evaluateTextureDesigner.Reset();
					evaluateEffectDesigner.Reset();
					holdKeepEffectDesigner.Reset();
					evaluateTextureDesigner.Hide();
					evaluateEffectDesigner.Hide();
					holdKeepEffectDesigner.Hide();
					this.evaluate = XMaimaiNote.XEvaluateDesigner.DrawableEvaluateTapType.NONE;
				}
				
				public override void Release () {
					base.Release ();
					if (footNoteInfo != null) {
						footNoteInfo.Release ();
						footNoteInfo = null;
					}
				}

				public void KeepStart (XMaimaiNote.XEvaluateDesigner.DrawableEvaluateTapType evaluate) {
					this.evaluate = evaluate;
					this.holdKeepEffectDesigner.Show (evaluate);
				}
			}

			public class Slide : Kernel {
				public override KernelType GetNoteType () { return KernelType.SLIDE; }
				public new Inspector.Slide noteInfo { get { return (Inspector.Slide)base.noteInfo; } set { base.noteInfo = value; } }
				public new ObjectDesigner.Slide designer { get { return (ObjectDesigner.Slide)base.designer; } set { base.designer = value; } }
				public XMaimaiNote.XEvaluateDesigner.SlideEvaluateTextureDesigner evaluateTextureDesigner { get; set; }

				public Chain[] chains { get; private set; }
				public long visualizeTime { get { return designer.visualizeTime; } }
				public long moveStartTime { get { return designer.moveStartTime; } }
				public long moveEndTime { get { return designer.moveEndTime; } }
				public int chainsAmount { get { return chains.Length; } }
				public bool calledSlideSound { get; set; }

				public Inspector.Slide GetFirstNoteInfo(int chainIndex) { return chains [chainIndex].firstNoteInfo; }
				public Inspector.Slide GetLastNoteInfo(int chainIndex) { return chains [chainIndex].lastNoteInfo; }

				public Slide(string uniqueId, int sourceIndex) : base(uniqueId, sourceIndex) {
				}
				
				public void Setup (Kernel.Slide.Chain[] chains, ObjectDesigner.Slide designer, XMaimaiNote.XEvaluateDesigner.SlideEvaluateTextureDesigner evaluateTextureDesigner) {
					this.chains = chains;
					this.noteInfo = chains[0].noteInfo; //スライドのインスペクタは、[0][0]を入れておく. 適当な代表になる.
					this.designer = designer;
					this.evaluateTextureDesigner = evaluateTextureDesigner;
					this.calledSlideSound = false;
				}
				
				public override void Reset () {
					base.Reset();
					evaluateTextureDesigner.Reset();
					evaluateTextureDesigner.Hide();
				}

				public void SetJudged (bool judged) {
					foreach (var chain in chains) {
						chain.SetJudged (judged);
					}
				}

				/// <summary>
				/// スライド操作がひとつ前で終わったならTooLateではなく緑Late. 
				/// </summary>
				public bool IsLateGoodPassible () {
					for (int i = 0; i < chains.Length; i++) {
						if (!chains[i].isLeft && !chains[i].isCompleted) {
							return false;
						}
					}
					return true; //すべてのパターンが一つ残すか全部処理しきっているなら緑Lateでもいい. (全部処理しきっているならこのメソッドが呼ばれる前に判定処理がされているはず)
				}

				public class Chain : Kernel {
					public override KernelType GetNoteType () { return KernelType.SLIDE; }
					public Kernel.Slide kernel { get; set; }
					public new Inspector.Slide noteInfo { get { return (Inspector.Slide)base.noteInfo; } set { base.noteInfo = value; } }
					public new ObjectDesigner.Slide designer { get { return kernel.designer; } set { kernel.designer = value; } }
					public Inspector.Slide[] noteInfos { get; private set; }
					public int index { get; set; }
					
					public Inspector.Slide firstNoteInfo { get { return noteInfos [0]; } }
					public Inspector.Slide lastNoteInfo { get { return noteInfos [noteInfos.Length - 1]; } }
					public Inspector.Slide nowNoteInfo { get { return noteInfos [index]; } }
					public Inspector.Slide nextNoteInfo { get { return noteInfos [index + 1]; } }
					public int infoAmount { get { return noteInfos.Length; } }
					
					public Chain() : base("", 0) {
					}
					public void Setup(Kernel.Slide kernel, Inspector.Slide[] noteInfos) {
						this.kernel = kernel;
						this.noteInfos = noteInfos;
						this.noteInfo = noteInfos[0];
						this.index = -1;
					}
					
					public override void Reset () {
						base.Reset();
						kernel.Reset();
					}

					public float Progress() {
						return (float)index / (float)noteInfos.Length;
					}
					
					public bool isCompleted {
						get {
							return index >= noteInfos.Length - 1;
						}
					}

					/// <summary>
					/// スライド操作をひとつ以上操作した上で、まだあとひとつ残っている.
					/// </summary>
					public bool isLeft {
						get {
							if (noteInfos.Length > 1) {
								return index == noteInfos.Length - 1;
							}
							return false; //始点と終点の間のセンサーは無い場合はTooLateにする
						}
					}
					
					public void SetJudged (bool judged) {
						foreach (var n in noteInfos) {
							n.judged = judged;
						}
					}
				}
			}
			
			public class Message : Kernel {
				public override KernelType GetNoteType () { return KernelType.MESSAGE; }
				public new Inspector.Message noteInfo { get { return (Inspector.Message)base.noteInfo; } set { base.noteInfo = value; } }
				public new ObjectDesigner.Message designer { get { return (ObjectDesigner.Message)base.designer; } set { base.designer = value; } }
				
				public Message(string uniqueId, int sourceIndex) : base(uniqueId, sourceIndex) {
				}
				
				public void Setup (Inspector.Message noteInfo, ObjectDesigner.Message designer) {
					this.noteInfo = noteInfo;
					this.designer = designer;
				}
				
				public override void Reset () {
					base.Reset();
				}
			}
			
			public class ScrollMessage : Kernel {
				public override KernelType GetNoteType () { return KernelType.SCROLL_MESSAGE; }
				public new Inspector.ScrollMessage noteInfo { get { return (Inspector.ScrollMessage)base.noteInfo; } set { base.noteInfo = value; } }
				public new ObjectDesigner.ScrollMessage designer { get { return (ObjectDesigner.ScrollMessage)base.designer; } set { base.designer = value; } }
				
				public ScrollMessage(string uniqueId, int sourceIndex) : base(uniqueId, sourceIndex) {
				}
				
				public void Setup (Inspector.ScrollMessage noteInfo, ObjectDesigner.ScrollMessage designer) {
					this.noteInfo = noteInfo;
					this.designer = designer;
				}
				
				public override void Reset () {
					base.Reset();
				}
			}
			
			public class Trap : Kernel {
				public override KernelType GetNoteType () { return KernelType.TRAP; }
				public new Inspector.Trap noteInfo { get { return (Inspector.Trap)base.noteInfo; } set { base.noteInfo = value; } }
				public new ObjectDesigner.Trap designer { get { return (ObjectDesigner.Trap)base.designer; } set { base.designer = value; } }
				public XEvaluateDesigner.EvaluateTextureDesigner evaluateTextureDesigner { get; set; }
				public XEvaluateDesigner.EvaluateEffectDesigner evaluateEffectDesigner { get; set; }
				
				public Trap(string uniqueId, int sourceIndex) : base(uniqueId, sourceIndex) {
				}
				
				public void Setup (Inspector.Trap noteInfo, ObjectDesigner.Trap designer, XEvaluateDesigner.EvaluateTextureDesigner evaluateTextureDesigner, XEvaluateDesigner.EvaluateEffectDesigner evaluateEffectDesigner) {
					this.noteInfo = noteInfo;
					this.designer = designer;
					this.evaluateTextureDesigner = evaluateTextureDesigner;
					this.evaluateEffectDesigner = evaluateEffectDesigner;
				}
				
				public override void Reset () {
					base.Reset ();
					evaluateTextureDesigner.Reset();
					evaluateEffectDesigner.Reset();
					evaluateTextureDesigner.Hide();
					evaluateEffectDesigner.Hide();
				}
				
				public override void Release () {
					base.Release ();
				}
			}

		}
	}
}
