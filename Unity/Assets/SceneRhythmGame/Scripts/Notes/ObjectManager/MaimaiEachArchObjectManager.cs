﻿using UnityEngine;
using System.Collections;

public class MaimaiEachArchObjectManager : MaimaiNoteObjectManager {
	public XMaimaiNote.XKernel.Kernel[] kernels { get; set; }
	protected MaimaiEachArchObjectController archCtrl { get; set; }
	protected long fadeJustTime { get; set; }
	protected long fadeTimeBefore { get; set; }
	protected long fadeTimeRange { get; set; }
	protected long scaleJustTime { get; set; }
	protected long scaleTimeBefore { get; set; }
	protected long scaleTimeRange { get; set; }
	public float fadePercent { get; protected set; }
	public float scalePercent { get; protected set; }
	protected bool haveHold { get; set; }
	
	protected override long judgeTimeLag { get { return kernels[0].designer.commonInfo.judgeTimeLag; } }
	protected override long gameTime { get { return kernels[0].designer.commonInfo.timer.gameTime; } }
	
	public void Setup (XMaimaiNote.XKernel.Kernel[] kernels, MaimaiEachArchObjectController archCtrl) {
		this.kernels = kernels;
		this.archCtrl = archCtrl;
		this.haveHold = false;
		foreach (var k in kernels) {
			var designer = k.designer as XMaimaiNote.XObjectDesigner.ObjectDesigner.EachArch;
			if (designer != null) {
				this.fadeJustTime = this.scaleJustTime = designer.eachArchArrivalTime;
				this.fadeTimeBefore = designer.guideSpeed;
				this.fadeTimeRange = designer.guideFadeSpeed;
				this.scaleTimeBefore = this.scaleTimeRange = designer.guideMoveSpeed;
			}
			if (k.GetNoteType() == XMaimaiNote.XKernel.Kernel.KernelType.HOLD) {
				this.haveHold = true;
			}
		}
	}

	public void ChangeSprite (Sprite sprite, float angle) {
		if (archCtrl != null) {
			archCtrl.ChangeSprite(sprite);
			archCtrl.ChangeAngle(angle);
		}
	}
	
	protected override void TimePercentCalculation () {
		if (this.kernels == null) return;

		fadePercent = CalcTimePercent(fadeJustTime, fadeTimeBefore, fadeTimeRange);
		scalePercent = CalcTimePercent(scaleJustTime, scaleTimeBefore, scaleTimeRange);
		if (scalePercent < 0) scalePercent = 0;
		if (haveHold && scalePercent > 1) scalePercent = 1;
		if (fadePercent < 0) fadePercent = 0;
		if (fadePercent > 1) fadePercent = 1;
	}
	
	protected override void Move () {
		if (archCtrl != null) {
			archCtrl.Move ();
		}
	}
	
	public override void Show () {
		base.Show ();
		if (archCtrl != null) {
			archCtrl.Show ();
		}
	}
	
	public override void Hide () {
		base.Hide ();
		if (archCtrl != null) {
			archCtrl.Hide ();
		}
	}
	
	public override void Release () {
		if (archCtrl != null) {
			archCtrl.Release ();
			archCtrl = null;
		}
		this.kernels = null;
		base.Release();
	}
}
