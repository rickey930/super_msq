﻿using UnityEngine;
using System.Collections;

public class MaimaiMessageNoteObjectManager : MaimaiKernelObjectManager {
	public new XMaimaiNote.XKernel.Kernel.Message kernel { get { return (XMaimaiNote.XKernel.Kernel.Message)base.kernel; } }
	protected MaimaiMessageNoteObjectController noteCtrl { get; set; }
	protected long disappearTime { get; set; }
	protected long appearTime { get; set; }
	protected long timeRange { get; set; }
	public float appearPercent { get; protected set; }
	
	public void Setup (XMaimaiNote.XKernel.Kernel.Message kernel, MaimaiMessageNoteObjectController noteCtrl) {
		base.Setup (kernel);
		this.noteCtrl = noteCtrl;
		this.disappearTime = kernel.designer.disappearTime;
		this.appearTime = kernel.designer.appearTime;
		this.timeRange = disappearTime - appearTime;
	}
	
	protected override void TimePercentCalculation () {
		if (this.kernel == null) return;
		
		appearPercent = CalcTimePercent(disappearTime, appearTime, timeRange);
	}
	
	protected override void Move () {
		if (noteCtrl != null) {
			noteCtrl.Move ();
		}
	}
	
	public override void Show () {
		base.Show ();
		if (noteCtrl != null) {
			noteCtrl.Show ();
		}
	}
	
	public override void Hide () {
		base.Hide ();
		if (noteCtrl != null) {
			noteCtrl.Hide ();
		}
	}
	
	public override void Release () {
		if (noteCtrl != null) {
			noteCtrl.Release ();
			noteCtrl = null;
		}
		base.Release();
	}
}
