﻿using UnityEngine;
using System.Collections;

public abstract class MaimaiNoteObjectManager : MonoBehaviour, IMaimaiObjectShowSwitch {

	void Start () {
		TimePercentCalculation ();
		Move ();
	}

	void Update () {
		TimePercentCalculation ();
		Move ();
	}

	protected abstract void TimePercentCalculation ();
	protected abstract void Move ();
	public virtual void Release () {
		Destroy (this.gameObject);
	}
	
	public virtual void Show () {
		this.gameObject.SetActive (true);
		TimePercentCalculation ();
		Move ();
	}

	public virtual void Hide () {
		this.gameObject.SetActive (false);
	}

	public virtual void Reset () {

	}
	
	protected abstract long judgeTimeLag { get; }
	protected abstract long gameTime { get; }

	/// <summary>
	/// 「判定時間」「その何秒前から表示」「何秒かけて移動」を指定して移動量パーセントを求める.
	/// </summary>
	protected float CalcTimePercent(long justTime, long timeBefore, long timeRange) {
		if (timeRange == 0) return 0;
		long startTime = (justTime + judgeTimeLag) - timeBefore;
		return (float)(gameTime - startTime) / (float)timeRange;
	}
}
