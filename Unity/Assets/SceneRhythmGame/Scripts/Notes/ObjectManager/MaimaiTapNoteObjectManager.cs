﻿using UnityEngine;
using System.Collections;

public class MaimaiTapNoteObjectManager : MaimaiKernelObjectManager, IMaimaiArchObjectManager {
	public new XMaimaiNote.XKernel.Kernel.Tap kernel { get { return (XMaimaiNote.XKernel.Kernel.Tap)base.kernel; } }
	protected MaimaiTapNoteObjectController noteCtrl { get; set; }
	protected MaimaiArchObjectController archCtrl { get; set; }
	protected long fadeJustTime { get; set; }
	protected long fadeTimeBefore { get; set; }
	protected long fadeTimeRange { get; set; }
	protected long moveJustTime { get; set; }
	protected long moveTimeBefore { get; set; }
	protected long moveTimeRange { get; set; }
	public float fadePercent { get; protected set; }
	public float movePercent { get; protected set; }

	public float archScalePercent { get { return movePercent; } }

	public void Setup (XMaimaiNote.XKernel.Kernel.Tap kernel, MaimaiTapNoteObjectController noteCtrl, MaimaiArchObjectController archCtrl) {
		base.Setup (kernel);
		this.noteCtrl = noteCtrl;
		this.archCtrl = archCtrl;
		this.fadeJustTime = this.moveJustTime = kernel.designer.justTime;
		this.fadeTimeBefore = kernel.designer.guideSpeed;
		this.fadeTimeRange = kernel.designer.guideFadeSpeed;
		this.moveTimeBefore = this.moveTimeRange = kernel.designer.guideMoveSpeed;
	}

	protected override void TimePercentCalculation () {
		if (this.kernel == null) return;

		fadePercent = CalcTimePercent(fadeJustTime, fadeTimeBefore, fadeTimeRange);
		movePercent = CalcTimePercent(moveJustTime, moveTimeBefore, moveTimeRange);
		if (movePercent < 0) movePercent = 0;
		//if (movePercent > 1) movePercent = 1;
		if (fadePercent < 0) fadePercent = 0;
		if (fadePercent > 1) fadePercent = 1;
	}

	protected override void Move () {
		if (noteCtrl != null) {
			noteCtrl.Move ();
		}
		if (archCtrl != null) {
			archCtrl.Move ();
		}
	}

	public override void Show () {
		base.Show ();
		if (noteCtrl != null) {
			noteCtrl.Show ();
		}
		if (archCtrl != null) {
			archCtrl.Show ();
		}
	}
	
	public override void Hide () {
		base.Hide ();
		if (noteCtrl != null) {
			noteCtrl.Hide ();
		}
		if (archCtrl != null) {
			archCtrl.Hide ();
		}
	}

	public override void Release () {
		if (noteCtrl != null) {
			noteCtrl.Release ();
			noteCtrl = null;
		}
		if (archCtrl != null) {
			archCtrl.Release ();
			archCtrl = null;
		}
		base.Release();
	}
}
