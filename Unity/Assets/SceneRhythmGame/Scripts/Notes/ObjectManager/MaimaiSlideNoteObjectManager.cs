﻿using UnityEngine;
using System.Collections;

public class MaimaiSlideNoteObjectManager : MaimaiKernelObjectManager {
	public new XMaimaiNote.XKernel.Kernel.Slide kernel { get { return (XMaimaiNote.XKernel.Kernel.Slide)base.kernel; } }
	protected MaimaiSlideGuideObjectController[] guideCtrls { get; set; }
	protected IMaimaiSlideMarkerController[] markerCtrls { get; set; }
	protected long fadeJustTime { get; set; }
	protected long fadeTimeBefore { get; set; }
	protected long fadeTimeRange { get; set; }
	protected long moveJustTime { get; set; }
	protected long moveTimeBefore { get; set; }
	protected long moveTimeRange { get; set; }
	public float fadePercent { get; protected set; }
	public float movePercent { get; set; }
	public float guideMovePercent { get; set; }
	
	public void Setup (XMaimaiNote.XKernel.Kernel.Slide kernel, MaimaiSlideGuideObjectController[] guideCtrls, IMaimaiSlideMarkerController[] markerCtrls) {
		base.Setup (kernel);
		this.guideCtrls = guideCtrls;
		this.markerCtrls = markerCtrls;
		this.fadeJustTime = kernel.designer.visualizeTime;
		this.fadeTimeBefore = this.fadeTimeRange = kernel.designer.fadeSpeed;
		this.moveJustTime = kernel.designer.moveEndTime;
		this.moveTimeBefore = this.moveTimeRange = kernel.designer.moveEndTime - kernel.designer.moveStartTime;
	}
	
	protected override void TimePercentCalculation () {
		if (this.kernel == null) return;

		fadePercent = CalcTimePercent(fadeJustTime, fadeTimeBefore, fadeTimeRange);
		guideMovePercent = CalcTimePercent (moveJustTime, moveTimeBefore, moveTimeRange);
		if (kernel.designer.commonInfo.autoPlay) {
			movePercent = guideMovePercent;
		}
		if (movePercent < 0) movePercent = 0;
		if (movePercent > 1) movePercent = 1;
		if (fadePercent < 0) fadePercent = 0;
		if (fadePercent > 1) fadePercent = 1;
	}
	
	protected override void Move () {
		if (guideCtrls != null) {
			foreach (var guideCtrl in guideCtrls) {
				if (guideCtrl != null) {
					guideCtrl.Move ();
				}
			}
		}
		if (markerCtrls != null) {
			foreach (var markerCtrl in markerCtrls) {
				if (markerCtrl != null) {
					markerCtrl.Move ();
				}
			}
		}
	}
	
	public override void Show () {
		base.Show ();
		if (guideCtrls != null) {
			foreach (var guideCtrl in guideCtrls) {
				if (guideCtrl != null) {
					guideCtrl.Show ();
				}
			}
		}
		if (markerCtrls != null) {
			foreach (var markerCtrl in markerCtrls) {
				if (markerCtrl != null) {
					markerCtrl.Show ();
				}
			}
		}
	}
	
	public override void Hide () {
		base.Hide ();
		if (guideCtrls != null) {
			foreach (var guideCtrl in guideCtrls) {
				if (guideCtrl != null) {
					guideCtrl.Hide ();
				}
			}
		}
		if (markerCtrls != null) {
			foreach (var markerCtrl in markerCtrls) {
				if (markerCtrl != null) {
					markerCtrl.Hide ();
				}
			}
		}
	}
	
	public override void Reset () {
		base.Reset();
		if (guideCtrls != null) {
			foreach (var guideCtrl in guideCtrls) {
				if (guideCtrl != null) {
					guideCtrl.Reset ();
				}
			}
		}
	}
	
	public override void Release () {
		if (guideCtrls != null) {
			foreach (var guideCtrl in guideCtrls) {
				if (guideCtrl != null) {
					guideCtrl.Release ();
				}
			}
		}
		guideCtrls = null;
		if (markerCtrls != null) {
			foreach (var markerCtrl in markerCtrls) {
				if (markerCtrl != null) {
					markerCtrl.Release ();
				}
			}
		}
		markerCtrls = null;
		base.Release();
	}
}
