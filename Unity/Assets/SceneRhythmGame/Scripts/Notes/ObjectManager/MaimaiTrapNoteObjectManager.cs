﻿using UnityEngine;
using System.Collections;

public class MaimaiTrapNoteObjectManager : MaimaiKernelObjectManager {
	public new XMaimaiNote.XKernel.Kernel.Trap kernel { get { return (XMaimaiNote.XKernel.Kernel.Trap)base.kernel; } }
	protected MaimaiTrapNoteObjectController noteCtrl { get; set; }
	protected long fadeJustTime { get; set; }
	protected long fadeTimeBefore { get; set; }
	protected long fadeTimeRange { get; set; }
	protected long throughJustTime { get; set; }
	protected long throughTimeBefore { get; set; }
	protected long throughTimeRange { get; set; }
	public float fadePercent { get; protected set; }
	public float throughPercent { get; protected set; }
	public void Setup (XMaimaiNote.XKernel.Kernel.Trap kernel, MaimaiTrapNoteObjectController noteCtrl) {
		base.Setup (kernel);
		this.noteCtrl = noteCtrl;
		this.fadeJustTime = kernel.designer.headTime;
		this.fadeTimeBefore = kernel.designer.guideSpeed;
		this.fadeTimeRange = kernel.designer.guideFadeSpeed;
		this.throughJustTime = kernel.designer.footTime;
		this.throughTimeBefore = this.throughTimeRange = kernel.designer.footTime - kernel.designer.headTime;
	}
	
	protected override void TimePercentCalculation () {
		if (this.kernel == null) return;
		
		fadePercent = CalcTimePercent(fadeJustTime, fadeTimeBefore, fadeTimeRange);
		throughPercent = CalcTimePercent(throughJustTime, throughTimeBefore, throughTimeRange);
		if (fadePercent < 0) fadePercent = 0;
		if (fadePercent > 1) fadePercent = 1;
		if (throughPercent < 0) throughPercent = 0;
		if (throughPercent > 1) throughPercent = 1;
	}
	
	protected override void Move () {
		if (noteCtrl != null) {
			noteCtrl.Move ();
		}
	}
	
	public override void Show () {
		base.Show ();
		if (noteCtrl != null) {
			noteCtrl.Show ();
		}
	}
	
	public override void Hide () {
		base.Hide ();
		if (noteCtrl != null) {
			noteCtrl.Hide ();
		}
	}
	
	public override void Release () {
		if (noteCtrl != null) {
			noteCtrl.Release ();
			noteCtrl = null;
		}
		base.Release();
	}
}
