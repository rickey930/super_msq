﻿using UnityEngine;
using System.Collections;
using XMaimaiNote.XKernel;

public abstract class MaimaiKernelObjectManager : MaimaiNoteObjectManager {
	protected Kernel kernel { get; private set; }
	protected void Setup(Kernel kernel) {
		this.kernel = kernel;
	}
	protected override long judgeTimeLag { get { return kernel.designer.commonInfo.judgeTimeLag; } }
	protected override long gameTime { get { return kernel.designer.commonInfo.timer.gameTime; } }

	public override void Release() {
		this.kernel = null;
		base.Release ();
	}
}
