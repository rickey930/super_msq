﻿using UnityEngine;
using System.Collections;

public interface IMaimaiArchObjectManager {
	float fadePercent { get; }
	float archScalePercent { get; }
}
