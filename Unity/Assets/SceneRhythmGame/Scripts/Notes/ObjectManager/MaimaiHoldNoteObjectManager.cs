﻿using UnityEngine;
using System.Collections;

public class MaimaiHoldNoteObjectManager : MaimaiKernelObjectManager, IMaimaiArchObjectManager {
	public new XMaimaiNote.XKernel.Kernel.Hold kernel { get { return (XMaimaiNote.XKernel.Kernel.Hold)base.kernel; } }
	protected MaimaiHoldNoteObjectController noteCtrl { get; set; }
	protected MaimaiArchObjectController archCtrl { get; set; }
	protected long fadeJustTime { get; set; }
	protected long fadeTimeBefore { get; set; }
	protected long fadeTimeRange { get; set; }
	protected long headMoveJustTime { get; set; }
	protected long headMoveTimeBefore { get; set; }
	protected long headMoveTimeRange { get; set; }
	protected long footMoveJustTime { get; set; }
	protected long footMoveTimeBefore { get; set; }
	protected long footMoveTimeRange { get; set; }
	public float fadePercent { get; protected set; }
	public float headMovePercent { get; protected set; }
	public float footMovePercent { get; protected set; }

	public float archScalePercent { get { return headMovePercent; } }
	
	public void Setup (XMaimaiNote.XKernel.Kernel.Hold kernel, MaimaiHoldNoteObjectController noteCtrl, MaimaiArchObjectController archCtrl) {
		base.Setup (kernel);
		this.noteCtrl = noteCtrl;
		this.archCtrl = archCtrl;
		this.fadeJustTime = this.headMoveJustTime = kernel.designer.headTime;
		this.fadeTimeBefore = kernel.designer.guideSpeed;
		this.fadeTimeRange = kernel.designer.guideFadeSpeed;
		this.headMoveTimeBefore = this.headMoveTimeRange = this.footMoveTimeBefore = this.footMoveTimeRange = kernel.designer.guideMoveSpeed;
		this.footMoveJustTime = kernel.designer.footTime;
	}
	
	protected override void TimePercentCalculation () {
		if (this.kernel == null) return;

		fadePercent = CalcTimePercent(fadeJustTime, fadeTimeBefore, fadeTimeRange);
		if (fadePercent < 0) fadePercent = 0;
		if (fadePercent > 1) fadePercent = 1;
		footMovePercent = CalcTimePercent(footMoveJustTime, footMoveTimeBefore, footMoveTimeRange);
		if (footMovePercent < 0) footMovePercent = 0;
		//if (footMovePercent > 1) footMovePercent = 1;
		headMovePercent = CalcTimePercent(headMoveJustTime, headMoveTimeBefore, headMoveTimeRange);
		if (headMovePercent < 0) headMovePercent = 0;
		if (headMovePercent > 1) {
			if (footMovePercent < 1)
				headMovePercent = 1;
			//フットが目標値まで動き切ったら、マーカーの元の形(円)になるようにする。
			else
				headMovePercent = footMovePercent;
		}
	}
	
	protected override void Move () {
		if (noteCtrl != null) {
			noteCtrl.Move ();
		}
		if (archCtrl != null) {
			archCtrl.Move ();
		}
	}
	
	public override void Show () {
		base.Show ();
		if (noteCtrl != null) {
			noteCtrl.Show ();
		}
		if (archCtrl != null) {
			archCtrl.Show ();
		}
	}
	
	public override void Hide () {
		base.Hide ();
		if (noteCtrl != null) {
			noteCtrl.Hide ();
		}
		if (archCtrl != null) {
			archCtrl.Hide ();
		}
	}
	
	public override void Release () {
		if (noteCtrl != null) {
			noteCtrl.Release ();
			noteCtrl = null;
		}
		if (archCtrl != null) {
			archCtrl.Release ();
			archCtrl = null;
		}
		base.Release();
	}
}
