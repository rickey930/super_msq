﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RhythmGameLibrary;
using XMaimaiNote.XKernel;

namespace XMaimaiNote {
	namespace XInspector {
		public enum InspectorType {
			TAP, HOLD_HEAD, HOLD_FOOT, BREAK, SLIDE, MESSAGE, SCROLL_MESSAGE, TRAP, 
		}

		public abstract class Inspector : RhythmGameNote {
			public Inspector(Kernel kernel, long justTime, long missTime, int actionId) : base (justTime, missTime) {
				this.kernel = kernel;
				this.actionId = actionId;
			}
			public Kernel kernel { get; set; }
			public int actionId { get; set; }
			
			public abstract InspectorType GetNoteType ();
			
			public virtual void Release() {
				kernel = null;
			}
			
			public virtual void Reset () {

			}
			
			/// <summary>
			/// シンク評価に対応するノートタイプ.
			/// </summary>
			public bool IsSyncableNoteType() {
				switch (GetNoteType ()) {
				case InspectorType.TAP:
				case InspectorType.SLIDE:
				case InspectorType.HOLD_FOOT:
				case InspectorType.BREAK:
					return true;
				}
				return false;
			}
			
			/// <summary>
			/// 評価に対応するノートタイプ.
			/// </summary>
			public bool IsEvaluatableNote() {
				switch (GetNoteType ()) {
				case InspectorType.TAP:
				case InspectorType.SLIDE:
				case InspectorType.HOLD_HEAD:
				case InspectorType.HOLD_FOOT:
				case InspectorType.BREAK:
				case InspectorType.TRAP:
					return true;
				}
				return false;
			}
			
			/// <summary>
			/// ガイドであるノートタイプ.
			/// </summary>
			public bool IsGuideNote() {
				switch (GetNoteType ()) {
				case InspectorType.MESSAGE:
				case InspectorType.SCROLL_MESSAGE:
					return true;
				}
				return false;
			}
			/*
			 * 譜面の情報を構成するクラス
			 * スケジュール(タイムライン)に乗るクラス
			 * オブジェクトを動かすクラス(:MonoBehaviour)
			 * 
			 * eachはアーチ用
			 * スケジュールに沿ってオブジェクトを生成する
			 * 判定も必要
			 * 
			 * note
			 * 	have each, evalute, schedule
			 * とある時間にActionIdを判定せよ (RhythmGameNote)
			 * 	tap, holdhead, holdfoot, slidepatterns, break
			 * とある時間に音を鳴らせ (MaimaiNoteAnswerSoundSchedule)
			 * 	tap, holdhead, holdfoot, slidemovestart, break
			 * とある時間にオブジェクトを生成せよ (eachの使用/MaimaiNoteVisualizeSchedule)
			 * 	tap, hold, slide, break
			 * 判定結果を持て (MaimaiNoteEvaluateResult)
			 * 	tap, hold, slide, break
			 * 以上を生成するためのデータ
			 * 	tap: buttonId, justTime, color, shape, eachButtonIds
			 * 	hold: buttonId, headJustTime, footJustTime, color, eachButtonIds
			 * 	slide: visualizeCompleteTime, moveStartTime, moveEndTime, justTime, color, slidePatterns
			 * 	break: buttonId, justTime, shape, eachButtonIds
			 * 	common: noteObjectController, evaluateResult, GetActionId(), GetAnswerSoundSchedule(), 
			 * 音を鳴らすのは独立できる
			 * オブジェクトの生成は非表示にしたいときに関連付けが必要
			 * ActionIdによる判定は結果の保持が必要なので関連付けも必要
			 */

			public class Tap : Inspector {
				public new Kernel.Tap kernel { get { return (Kernel.Tap)base.kernel; } set { base.kernel = value; } }
				public Tap (Kernel.Tap kernel, long justTime, long missTime, int actionId) : base (kernel, justTime, missTime, actionId) {
				}
				public override InspectorType GetNoteType () {
					return InspectorType.TAP;
				}
			}
			public class Break : Inspector {
				public new Kernel.Break kernel { get { return (Kernel.Break)base.kernel; } set { base.kernel = value; } }
				public Break (Kernel.Break kernel, long justTime, long missTime, int actionId) : base (kernel, justTime, missTime, actionId) {
				}
				public override InspectorType GetNoteType () {
					return InspectorType.BREAK;
				}
			}
			public class HoldHead : Inspector {
				public new Kernel.Hold kernel { get { return (Kernel.Hold)base.kernel; } set { base.kernel = value; } }
				public HoldHead (Kernel.Hold kernel, long justTime, long missTime, int actionId) : base (kernel, justTime, missTime, actionId) {
				}
				public override InspectorType GetNoteType() {
					return InspectorType.HOLD_HEAD;
				}
			}
			public class HoldFoot : Inspector {
				public new Kernel.Hold kernel { get { return (Kernel.Hold)base.kernel; } set { base.kernel = value; } }
				public HoldFoot(Kernel.Hold kernel, long justTime, long missTime, int actionId) : base (kernel, justTime, missTime, actionId) {
				}
				public override InspectorType GetNoteType() {
					return InspectorType.HOLD_FOOT;
				}
			}
			public class Slide : Inspector {
				public new Kernel.Slide.Chain kernel { get { return (Kernel.Slide.Chain)base.kernel; } set { base.kernel = value; } }
				public long? lastCensoredTime;
				public Slide (Kernel.Slide.Chain kernel, long justTime, long missTime, int actionId) : base (kernel, justTime, missTime, actionId) {
				}
				public override InspectorType GetNoteType () {
					return InspectorType.SLIDE;
				}
				public override void Reset () {
					base.Reset ();
					lastCensoredTime = null;
				}
				public override long overrideJustProcTime () {
					return kernel.kernel.moveEndTime;
				}
			}
			public class Message : Inspector {
				public new Kernel.Message kernel { get { return (Kernel.Message)base.kernel; } set { base.kernel = value; } }
				public Message (Kernel.Message kernel, long justTime, long missTime, int actionId) : base (kernel, justTime, missTime, actionId) {
				}
				public override InspectorType GetNoteType () {
					return InspectorType.MESSAGE;
				}
			}
			public class ScrollMessage : Inspector {
				public new Kernel.ScrollMessage kernel { get { return (Kernel.ScrollMessage)base.kernel; } set { base.kernel = value; } }
				public ScrollMessage (Kernel.ScrollMessage kernel, long justTime, long missTime, int actionId) : base (kernel, justTime, missTime, actionId) {
				}
				public override InspectorType GetNoteType () {
					return InspectorType.SCROLL_MESSAGE;
				}
			}
			public class Trap : Inspector {
				public new Kernel.Trap kernel { get { return (Kernel.Trap)base.kernel; } set { base.kernel = value; } }
				public Trap (Kernel.Trap kernel, long headTime, long footTime, int actionId) : base (kernel, headTime, footTime - headTime, actionId) {
				}
				public override InspectorType GetNoteType() {
					return InspectorType.TRAP;
				}
			}
		}
	}
}
