﻿using UnityEngine;
using System.Collections;

public class SlideMeshCreator {
	public static Mesh CreateWideSlideMesh (int chainCount) {
		if (chainCount == 0) return null;
		// メッシュに使う画像のサイズ.
		float width = 1.0f;
		float height = 16.0f;
		// 横方向の頂点数.
		int holizontalVertexAmount = chainCount;
		// メッシュに使う画像のpovit (0.5が中心)
		float centerXRate = 0.0f;
		float centerYRate = 0.5f;
		
		// 回転方向 (メッシュを作るときに(つまりここで)いじるべきではない)
		float vec = 1;
		
		const int verticalVertexAmount = 2;
		
		Mesh mesh = new Mesh();
		Vector3 [] newVertices = new Vector3[holizontalVertexAmount * verticalVertexAmount];
		Vector2 [] newUV       = new Vector2[holizontalVertexAmount * verticalVertexAmount];
		// 三角形の頂点数は3で、四角形を作るには三角形が2つ必要で、四角形が何個あるかで掛け算する.
		// 四角形用の頂点が2*2で三角形用の頂点が6個. 3*2で12個. 4*2で18個. 3*3で24個. 4*4で36個
		// 頂点数-1個の四角形が作れる.
		int[] newTriangles     = new int[3 * 2 * (holizontalVertexAmount - 1) * (verticalVertexAmount - 1)];
		
		// 鏡コの字を書くように頂点を配置する.
		
		// 頂点とUVを作成.
		for (int h = 0; h < holizontalVertexAmount; h++) {
			float progress = (float)h / (float)(holizontalVertexAmount - 1);
			int index = h;
			newVertices[index] = new Vector3(((width * progress) + (width * centerXRate)) * vec, -(height * centerYRate)).ChangePosHandSystem();
			newUV[index] = new Vector2((progress), 0).ChangeUVHandSystem();
		}
		for (int h = holizontalVertexAmount - 1; h >= 0; h--) {
			float progress = (float)h / (float)(holizontalVertexAmount - 1);
			int index = (holizontalVertexAmount - 1) + (verticalVertexAmount - 1) + (holizontalVertexAmount - 1 - h);
			newVertices[index] = new Vector3(((width * progress) + (width * centerXRate)) * vec, height - (height * centerYRate)).ChangePosHandSystem();
			newUV[index] = new Vector2((progress), 1).ChangeUVHandSystem();
		}
		// 三角形に頂点インデックス割り当て.
		
		// ※【ここはverticalVertexAmountが2固定である前提で作るよ】.
		// 0, 1, newVertices.Length-1
		// 0, newVertices.Length-1, newVertices.Length-2
		// 0～は次のループで+1, newVertices.Length系は次のループで-1
		// newVertices.Length-2 == holizontalVertexAmount で終了.
		int triangleIndex = 0;
		for (int i = 0; i < holizontalVertexAmount - 1; i++) {
			newTriangles[triangleIndex + 0] = i + 1;
			newTriangles[triangleIndex + 1] = i + 0;
			newTriangles[triangleIndex + 2] = newVertices.Length - 2 - i;
			newTriangles[triangleIndex + 3] = i + 0;
			newTriangles[triangleIndex + 4] = newVertices.Length - 1 - i;
			newTriangles[triangleIndex + 5] = newVertices.Length - 2 - i;
			triangleIndex += 6;
		}
		
		mesh.vertices  = newVertices;
		mesh.uv        = newUV;
		mesh.triangles = newTriangles;
		mesh.name = "Chain" + chainCount.ToString() + "SlideMesh";
		
		mesh.RecalculateNormals();
		mesh.RecalculateBounds();
		
		return mesh;
	}

	/// <summary>
	/// SlideEvaluateMeshEditorで呼び出したりする.
	/// </summary>
	public static Mesh CreateSlideEvaluateTextureMesh () {
		// メッシュの頂点数.
		int width = Constants.instance.SLIDE_EVALUATE_TEXTURE_WIDTH; //横
		int height = 2; //縦
		
		Mesh mesh = new Mesh();
		Vector3 [] newVertices = new Vector3[width * height];
		Vector2 [] newUV       = new Vector2[width * height];
		// 三角形の頂点数は3で、四角形を作るには三角形が2つ必要で、四角形が何個あるかで掛け算する.
		// 四角形用の頂点が2*2で三角形用の頂点が6個. 3*2で12個. 4*2で18個. 3*3で24個. 4*4で36個
		// 頂点数-1個の四角形が作れる.
		int[] newTriangles     = new int[3 * 2 * (width - 1) * (height - 1)];
		
		// コの字を書くように頂点を配置する.

		float w = 1, h = 1;
		
		// 頂点とUVを作成.
		// 頂点はあとから指定できるから、UVを優先する.
		// UVは0～1範囲で指定.
		for (int i = 0; i < width; i++) {
			float percent = (float)i / (float)width;
			newVertices[i] = new Vector3(i * w, 0, 0).ChangePosHandSystem();
			newUV[i] = new Vector2(percent, 0).ChangeUVHandSystem();
			newVertices[width * 2 - 1 - i] = new Vector3(i * w, h, 0).ChangePosHandSystem();
			newUV[width * 2 - 1 - i] = new Vector2(percent, 1).ChangeUVHandSystem();
		}

		// 三角形に頂点インデックス割り当て.
		
		// ※【ここはverticalVertexAmountが2固定である前提で作るよ】.
		// 頂点はコの字を書くように作ったけど、三角形は鏡コの字で考えないといけないらしい.
		//  １  - ０
		//  ｜    ｜
		// セミ - ラス
		// 2番目の頂点が1番目から反時計回りなら裏確定、
		// 2番目の頂点が1番目から時計回りなら表確定.
		// 2番目の頂点が1番目から見て斜めの位置なら、3番目の頂点を見て時計回りか反時計回りで表か裏かが決まる.
		int triangleIndex = 0;
		for (int i = 0; i < width - 1; i++) {
			newTriangles[triangleIndex + 0] = i + 0;
			newTriangles[triangleIndex + 1] = newVertices.Length - 2 - i;
			newTriangles[triangleIndex + 2] = i + 1;
			newTriangles[triangleIndex + 3] = i + 0;
			newTriangles[triangleIndex + 4] = newVertices.Length - 1 - i;
			newTriangles[triangleIndex + 5] = newVertices.Length - 2 - i;
			triangleIndex += 6;
		}
		
		mesh.vertices  = newVertices;
		mesh.uv        = newUV;
		mesh.triangles = newTriangles;
		mesh.name = "SlideEvaluateTextureMesh";
		
		mesh.RecalculateNormals();
		mesh.RecalculateBounds();
		
		return mesh;
	}

}
