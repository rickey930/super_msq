﻿using UnityEngine;
using System.Collections;

public interface IMaimaiObjectShowSwitch {
	void Show();
	void Hide();
	void Release();
	void Reset();
}
