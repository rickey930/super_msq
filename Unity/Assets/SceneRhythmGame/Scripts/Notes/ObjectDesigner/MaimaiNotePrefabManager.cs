﻿using UnityEngine;
using System.Collections;

public class MaimaiNotePrefabManager : MonoBehaviour {
	[SerializeField]
	public GameObject tapObjectPrefab;
	[SerializeField]
	public GameObject holdObjectPrefab;
	[SerializeField]
	public GameObject slideGuideClusterPrefab;
	[SerializeField]
	public GameObject slideGuidePrefab;
	[SerializeField]
	public GameObject slideMarkerPrefab;
	[SerializeField]
	public GameObject wideSlideMarkerPrefab;
	[SerializeField]
	public GameObject breakObjectPrefab;
	[SerializeField]
	public GameObject archObjectPrefab;
	[SerializeField]
	public GameObject eachArchObjectPrefab;
	[SerializeField]
	public GameObject messageNoteObjectPrefab;
	[SerializeField]
	public GameObject scrollMessageNoteObjectPrefab;
	[SerializeField]
	public GameObject trapObjectPrefab;
	
	[SerializeField]
	public GameObject tapManagerPrefab;
	[SerializeField]
	public GameObject holdManagerPrefab;
	[SerializeField]
	public GameObject slideManagerPrefab;
	[SerializeField]
	public GameObject breakManagerPrefab;
	[SerializeField]
	public GameObject eachArchManagerPrefab;
	[SerializeField]
	public GameObject messageNoteManagerPrefab;
	[SerializeField]
	public GameObject scrollMessageNoteManagerPrefab;
	[SerializeField]
	public GameObject trapManagerPrefab;

	[SerializeField]
	public GameObject evaluateTexturePrefab;
	[SerializeField]
	public GameObject evaluateEffectPrefab;
	[SerializeField]
	public GameObject evaluateSlidePrefab;
	[SerializeField]
	public GameObject holdKeepEffectPrefab;
}
