using UnityEngine;
using System.Collections.Generic;
using XMaimaiNote.XKernel;
using XMaimaiNote.XDocument;
using XMaimaiNote.XInspector;

namespace XMaimaiNote {
	namespace XObjectDesigner {
		public class CommonInformation {
			public MaimaiTimer timer { get; private set; }
			public long judgeTimeLag { get; private set; }
			public int restartId { get; set; }
			public bool pine { get; private set; }
			public bool angulatedHold { get; private set; }
			public bool autoPlay { get; set; }
			public bool realtimeCreateObject { get; private set; }
			public bool visualizeFastLate { get; private set; }
			public MaimaiNotePrefabManager prefabs { get; private set; }

			/// <summary>
			/// このインスタンスはGameManagerで作らせる.
			/// </summary>
			public CommonInformation (
				MaimaiTimer timer, 
				long judgeTimeLag,
				bool pine, 
				bool anglatedHold, 
				bool autoPlay, 
				bool realtimeCreateObject, 
				bool visualizeFastLate,
				MaimaiNotePrefabManager prefabs
				) {
				this.timer = timer;
				this.judgeTimeLag = judgeTimeLag;
				this.restartId = 0;
				this.pine = pine;
				this.angulatedHold = anglatedHold;
				this.autoPlay = autoPlay;
				this.realtimeCreateObject = realtimeCreateObject;
				this.visualizeFastLate = visualizeFastLate;
				this.prefabs = prefabs;
			}
		}
		public abstract class ObjectDesigner : IMaimaiSchedule, IMaimaiObjectDesigner {
			protected Kernel kernel { get; private set; }
			public CommonInformation commonInfo { get; private set; }
			protected List<IMaimaiObjectShowSwitch> createdObjects { get; private set; }
			
			protected RectTransform managerCluster;
			protected RectTransform canvas;

			public ObjectDesigner (RectTransform managerCluster, RectTransform canvas, Kernel kernel, CommonInformation objectCommonInfo) {
				this.kernel = kernel;
				this.commonInfo = objectCommonInfo;
				this.canvas = canvas;
				this.managerCluster = managerCluster;
				this.createdObjects = new List<IMaimaiObjectShowSwitch> ();
			}
			public abstract void Create ();
			public abstract long GetScheduleTime();
			/// <summary>
			/// オートプレーのとき時間を巻き戻すが、まだjustTimeが来てないものも表示したいので、それぞれのjustTimeを返す.
			/// </summary>
			public abstract long GetScheduleEndTime();
			public int GetSourceIndex () {
				return kernel.sourceIndex;
			}

			protected void AddGameObjectManager (IMaimaiObjectShowSwitch obj) {
				this.createdObjects.Add (obj);
				obj.Hide ();
			}

			protected virtual void ReleaseGameObjectManagers () {
				foreach (var obj in createdObjects) {
					obj.Release();
				}
				createdObjects.Clear ();
			}
			
			public virtual void ScheduleToDo(long nowTime) {
				if (commonInfo.realtimeCreateObject) {
					ReleaseGameObjectManagers ();
					Create ();
				}
				Show ();
			}
			
			public virtual void Show() {
				foreach (var obj in createdObjects) {
					if (!kernel.evaluated) {
						obj.Reset ();
						obj.Show ();
					}
				}
			}
			
			public virtual void Hide() {
				if (commonInfo.realtimeCreateObject) {
					ReleaseGameObjectManagers ();
				}
				else {
					foreach (var obj in createdObjects) {
						obj.Hide ();
					}
				}
			}

			public interface EachArch {
				long eachArchArrivalTime { get; }
				long guideFadeSpeed { get; set; }
				long guideMoveSpeed { get; set; }
				long guideSpeed { get; }
			}

			public class Break : ObjectDesigner, EachArch {
				protected new Kernel.Break kernel { get { return (Kernel.Break)base.kernel; } }
				protected RectTransform archCanvas { get; set; }
				/// <summary>
				/// 表示的なJust時間.タップだと判定時間だけど、スライドだと☆が動き終える時間
				/// </summary>
				public long justTime { get; set; }
				
				public long eachArchArrivalTime { get { return justTime; } }
				public long guideFadeSpeed { get; set; }
				public long guideMoveSpeed { get; set; }
				public long guideSpeed { get { return guideMoveSpeed + guideFadeSpeed; } }

				protected int buttonId { get; set; }

				/// <summary>
				/// タップノートオブジェクトの形
				/// </summary>
				protected Document.Break.GuideRingShapeType shape { get; set; }
				/// <summary>
				/// 初期角度deg
				/// </summary>
				protected float angle { get { return Constants.instance.GetPieceDegree (buttonId - 1); } }
				/// <summary>
				/// オブジェクトの大きさ
				/// </summary>
				protected float guideScale { get; set; }
				/// <summary>
				/// 表示開始時間
				/// </summary>
				public long appearanceTime { get; protected set; }
				/// <summary>
				/// 回転スピード.
				/// </summary>
				protected float spinSpeed { get; set; }
				/// <summary>
				/// 透明ノート.
				/// </summary>
				protected bool secret { get; set; }


				public Break(RectTransform managerCluster,
							 RectTransform canvas,
				             RectTransform archCanvas,
				             Kernel.Break kernel,
				           	 CommonInformation objectCommonInfo,
				             long justTime,
				             long guideFadeSpeed, 
				             long guideMoveSpeed,
				             float guideScale,
				             Document.Break.GuideRingShapeType shape,
				             float spinSpeed,
				             int buttonId,
				             bool secret)
				: base(managerCluster, canvas, kernel, objectCommonInfo) {
					this.archCanvas = archCanvas;
					this.shape = shape;
					this.justTime = justTime;
					this.guideFadeSpeed = guideFadeSpeed;
					this.guideMoveSpeed = guideMoveSpeed;
					this.guideScale = guideScale;
					this.appearanceTime = justTime - guideSpeed;
					this.spinSpeed = spinSpeed;
					this.buttonId = buttonId;
					this.secret = secret;
				}
				
				public Break(RectTransform managerCluster,
				             RectTransform canvas,
				             RectTransform archCanvas,
				             Kernel.Break kernel,
				             CommonInformation objectCommonInfo, 
				             Document.Break doc) : this(
					managerCluster, canvas, archCanvas, kernel, objectCommonInfo, doc.justTime, doc.guideFadeSpeed, doc.guideMoveSpeed, doc.guideScale,
					doc.guideShapeType, doc.guideSpinSpeed, doc.button.GetId(), doc.secret) {
					
				}
				public override void Create () {
					var mngobj = (GameObject)GameObject.Instantiate (commonInfo.prefabs.breakManagerPrefab);
					mngobj.transform.SetParentEx (managerCluster);
					var mng = mngobj.GetComponent<MaimaiBreakNoteObjectManager> ();

					var gobj = (GameObject)GameObject.Instantiate (commonInfo.prefabs.breakObjectPrefab);
					gobj.transform.SetParentEx(canvas);
					gobj.transform.SetAsFirstSibling ();
					var ctrl = gobj.GetComponent<MaimaiBreakNoteObjectController> ();
					Sprite main, center, arc;
					main = null;
					if (shape == Document.Break.GuideRingShapeType.CIRCLE) {
						if (!commonInfo.pine) {
							main = SpriteTank.Get ("note_break");
						}
						else {
							main = SpriteTank.Get ("pine_break");
						}
					}
					else if (shape == Document.Break.GuideRingShapeType.STAR ||
					         shape == Document.Break.GuideRingShapeType.WSTAR) {
						main = SpriteTank.Get ("note_star_break");
					}
					center = SpriteTank.Get ("center_red");
					ctrl.Setup (mng, angle, guideScale, spinSpeed, main, center, shape == Document.Break.GuideRingShapeType.WSTAR, secret);

					var agobj = (GameObject)GameObject.Instantiate (commonInfo.prefabs.archObjectPrefab);
					agobj.transform.SetParentEx(archCanvas);
					agobj.transform.SetAsFirstSibling ();
					var actrl = agobj.GetComponent<MaimaiArchObjectController> ();
					arc = MaipadDynamicCreatedSpriteTank.instance.breakArc;
					actrl.Setup (mng, angle, kernel.designer.justTime, kernel.designer.guideFadeSpeed, kernel.designer.guideMoveSpeed, arc, secret);

					mng.Setup (kernel, ctrl, actrl);

					AddGameObjectManager (mng);
				}

				public override long GetScheduleTime() {
					return appearanceTime;
				}

				public override long GetScheduleEndTime() {
					return justTime;
				}

			}

			public class Tap : ObjectDesigner, EachArch {
				protected new Kernel.Tap kernel { get { return (Kernel.Tap)base.kernel; } }
				protected RectTransform archCanvas { get; set; }
				
				/// <summary>
				/// タップノートオブジェクトの色
				/// </summary>
				protected GuideColor color { get; set; }

				/// <summary>
				/// 表示的なJust時間.タップだと判定時間だけど、スライドだと☆が動き終える時間
				/// </summary>
				public long justTime { get; set; }
				
				public long eachArchArrivalTime { get { return justTime; } }
				public long guideFadeSpeed { get; set; }
				public long guideMoveSpeed { get; set; }
				public long guideSpeed { get { return guideMoveSpeed + guideFadeSpeed; } }
				
				protected int buttonId { get; set; }
				
				/// <summary>
				/// タップノートオブジェクトの形
				/// </summary>
				protected Document.Tap.GuideRingShapeType shape { get; set; }
				/// <summary>
				/// 初期角度deg
				/// </summary>
				protected float angle { get { return Constants.instance.GetPieceDegree (buttonId - 1); } }
				/// <summary>
				/// オブジェクトの大きさ
				/// </summary>
				protected float guideScale { get; set; }
				/// <summary>
				/// 表示開始時間
				/// </summary>
				public long appearanceTime { get; protected set; }
				/// <summary>
				/// 回転スピード.
				/// </summary>
				protected float spinSpeed { get; set; }
				/// <summary>
				/// 透明ノート.
				/// </summary>
				protected bool secret { get; set; }

				public Tap(RectTransform managerCluster,
				           RectTransform canvas,
				           RectTransform archCanvas,
							Kernel.Tap kernel,
							CommonInformation objectCommonInfo,
							long justTime,
							long guideFadeSpeed, 
							long guideMoveSpeed,
							float guideScale,
							GuideColor color,
							Document.Tap.GuideRingShapeType shape,
							float spinSpeed,
							int buttonId,
				           bool secret)
				: base(managerCluster, canvas, kernel, objectCommonInfo) {
					this.archCanvas = archCanvas;
					this.color = color;
					this.shape = shape;
					this.justTime = justTime;
					this.guideFadeSpeed = guideFadeSpeed;
					this.guideMoveSpeed = guideMoveSpeed;
					this.guideScale = guideScale;
					this.appearanceTime = justTime - guideSpeed;
					this.spinSpeed = spinSpeed;
					this.buttonId = buttonId;
					this.secret = secret;
				}
				
				public Tap(RectTransform managerCluster,
				           RectTransform canvas,
				           RectTransform archCanvas,
				             Kernel.Tap kernel,
				             CommonInformation objectCommonInfo, 
				             Document.Tap doc) : this(
					managerCluster, canvas, archCanvas, kernel, objectCommonInfo, doc.justTime, doc.guideFadeSpeed, doc.guideMoveSpeed, doc.guideScale,
					doc.guideColor, doc.guideShapeType, doc.guideSpinSpeed, doc.button.GetId(), doc.secret) {
					
				}
				
				public override void Create () {
					var mngobj = (GameObject)GameObject.Instantiate (commonInfo.prefabs.tapManagerPrefab);
					mngobj.transform.SetParentEx (managerCluster);
					var mng = mngobj.GetComponent<MaimaiTapNoteObjectManager> ();

					var gobj = (GameObject)GameObject.Instantiate (commonInfo.prefabs.tapObjectPrefab);
					gobj.transform.SetParentEx(canvas);
					gobj.transform.SetAsFirstSibling ();
					var ctrl = gobj.GetComponent<MaimaiTapNoteObjectController> ();
					Sprite main, center, arc;
					main = center = arc = null;
					if (color == GuideColor.SINGLE) {
						if (shape == Document.Break.GuideRingShapeType.CIRCLE) {
							if (!commonInfo.pine) {
								main = SpriteTank.Get ("note_tap");
							}
							else {
								main = SpriteTank.Get ("pine_tap");
							}
							center = SpriteTank.Get ("center_pink");
							arc = MaipadDynamicCreatedSpriteTank.instance.tapArc;
						}
						else if (shape == Document.Break.GuideRingShapeType.STAR ||
						         shape == Document.Break.GuideRingShapeType.WSTAR) {
							main = SpriteTank.Get ("note_star");
							center = SpriteTank.Get ("center_blue");
							arc = MaipadDynamicCreatedSpriteTank.instance.starArc;
						}
					}
					else if (color == GuideColor.EACH) {
						if (shape == Document.Break.GuideRingShapeType.CIRCLE) {
							if (!commonInfo.pine) {
								main = SpriteTank.Get ("note_tap_each");
							}
							else {
								main = SpriteTank.Get ("pine_tap_each");
							}
						}
						else if (shape == Document.Break.GuideRingShapeType.STAR ||
						         shape == Document.Break.GuideRingShapeType.WSTAR) {
							main = SpriteTank.Get ("note_star_each");
						}
						center = SpriteTank.Get ("center_yellow");
						arc = MaipadDynamicCreatedSpriteTank.instance.eachArc0;
					}
					ctrl.Setup (mng, angle, guideScale, spinSpeed, main, center, shape == Document.Break.GuideRingShapeType.WSTAR, secret);
					
					var agobj = (GameObject)GameObject.Instantiate (commonInfo.prefabs.archObjectPrefab);
					agobj.transform.SetParentEx(archCanvas);
					agobj.transform.SetAsFirstSibling ();
					var actrl = agobj.GetComponent<MaimaiArchObjectController> ();
					actrl.Setup (mng, angle, kernel.designer.justTime, kernel.designer.guideFadeSpeed, kernel.designer.guideMoveSpeed, arc, secret);

					mng.Setup (kernel, ctrl, actrl);
					
					AddGameObjectManager (mng);
				}
				
				public override long GetScheduleTime() {
					return appearanceTime;
				}
				
				public override long GetScheduleEndTime() {
					return justTime;
				}

			}

			public class Hold : ObjectDesigner, EachArch {
				protected new Kernel.Hold kernel { get { return (Kernel.Hold)base.kernel; } }
				protected RectTransform archCanvas { get; set; }
				public long headTime { get; set; }
				public long footTime { get; set; }
				
				public long eachArchArrivalTime { get { return headTime; } }
				public long guideFadeSpeed { get; set; }
				public long guideMoveSpeed { get; set; }
				public long guideSpeed { get { return guideMoveSpeed + guideFadeSpeed; } }
				
				protected int buttonId { get; set; }
				
				/// <summary>
				/// タップノートオブジェクトの色
				/// </summary>
				protected GuideColor color { get; set; }
				/// <summary>
				/// 初期角度deg
				/// </summary>
				protected float angle { get { return Constants.instance.GetPieceDegree (buttonId - 1); } }
				/// <summary>
				/// オブジェクトの大きさ
				/// </summary>
				protected float guideScale { get; set; }
				/// <summary>
				/// 表示開始時間
				/// </summary>
				public long appearanceTime { get; protected set; }
				/// <summary>
				/// 透明ノート.
				/// </summary>
				protected bool secret { get; set; }
				
				
				public Hold(RectTransform managerCluster,
				            RectTransform canvas,
				            RectTransform archCanvas,
				            Kernel.Hold kernel,
				            CommonInformation objectCommonInfo, 
				            long headTime,
				            long footTime,
				            long guideFadeSpeed, 
				            long guideMoveSpeed,
				            float guideScale,
				            GuideColor color,
				            int buttonId,
				            bool secret)
				: base(managerCluster, canvas, kernel, objectCommonInfo) {
					this.archCanvas = archCanvas;
					this.color = color;
					this.headTime = headTime;
					this.footTime = footTime;
					this.guideFadeSpeed = guideFadeSpeed;
					this.guideMoveSpeed = guideMoveSpeed;
					this.guideScale = guideScale;
					this.appearanceTime = headTime - guideSpeed;
					this.buttonId = buttonId;
					this.secret = secret;
				}
				
				public Hold(RectTransform managerCluster,
				            RectTransform canvas,
				            RectTransform archCanvas,
				            Kernel.Hold kernel,
				            CommonInformation objectCommonInfo, 
				            Document.Hold doc) : this(
					managerCluster, canvas, archCanvas, kernel, objectCommonInfo, doc.headTime, doc.footTime, doc.guideFadeSpeed, doc.guideMoveSpeed, doc.guideScale,
					doc.guideColor, doc.button.GetId(), doc.secret) {
					
				}
				
				public override void Create () {
					var mngobj = (GameObject)GameObject.Instantiate (commonInfo.prefabs.holdManagerPrefab);
					mngobj.transform.SetParentEx (managerCluster);
					var mng = mngobj.GetComponent<MaimaiHoldNoteObjectManager> ();

					var gobj = (GameObject)GameObject.Instantiate (commonInfo.prefabs.holdObjectPrefab);
					gobj.transform.SetParentEx(canvas);
					gobj.transform.SetAsFirstSibling ();
					var ctrl = gobj.GetComponent<MaimaiHoldNoteObjectController> ();
					Sprite head, body, foot, center, arc;
					head = body = foot = center = arc = null;
					if (color == GuideColor.SINGLE) {
						if (commonInfo.angulatedHold) {
							head = SpriteTank.Get ("note_hold_corn_head");
							foot = SpriteTank.Get ("note_hold_corn_foot");
						}
						else {
							head = SpriteTank.Get ("note_hold_arc_head");
							foot = SpriteTank.Get ("note_hold_arc_foot");
						}
						body = SpriteTank.Get ("note_hold_body");
						center = SpriteTank.Get ("center_pink");
						arc = MaipadDynamicCreatedSpriteTank.instance.tapArc;
					}
					else if (color == GuideColor.EACH) {
						if (commonInfo.angulatedHold) {
							head = SpriteTank.Get ("note_hold_each_corn_head");
							foot = SpriteTank.Get ("note_hold_each_corn_foot");
						}
						else {
							head = SpriteTank.Get ("note_hold_each_arc_head");
							foot = SpriteTank.Get ("note_hold_each_arc_foot");
						}
						body = SpriteTank.Get ("note_hold_each_body");
						center = SpriteTank.Get ("center_yellow");
						arc = MaipadDynamicCreatedSpriteTank.instance.eachArc0;
					}
					ctrl.Setup (mng, angle, guideScale, head, foot, body, center, secret);
					
					var agobj = (GameObject)GameObject.Instantiate (commonInfo.prefabs.archObjectPrefab);
					agobj.transform.SetParentEx(archCanvas);
					agobj.transform.SetAsFirstSibling ();
					var actrl = agobj.GetComponent<MaimaiArchObjectController> ();
					actrl.Setup (mng, angle, kernel.designer.headTime, kernel.designer.guideFadeSpeed, kernel.designer.guideMoveSpeed, arc, secret);

					mng.Setup (kernel, ctrl, actrl);
					
					AddGameObjectManager (mng);
				}
				
				public override long GetScheduleTime() {
					return appearanceTime;
				}
				
				public override long GetScheduleEndTime() {
					return footTime;
				}

			}
			
			public class Slide : ObjectDesigner {
				protected new Kernel.Slide kernel { get { return (Kernel.Slide)base.kernel; } }
				protected RectTransform slideMarkerCanvas { get; set; }
				/// <summary>
				/// タップノートオブジェクトの色
				/// </summary>
				protected GuideColor color { get; set; }
				/// <summary>
				/// マーカーオブジェクトの大きさ
				/// </summary>
				protected float markerScale { get; set; }
				/// <summary>
				/// ガイドオブジェクトの大きさ
				/// </summary>
				protected float guideScale { get; set; }
				/// <summary>
				/// 表示開始時間
				/// </summary>
				public long appearanceTime { get; protected set; }
				/// <summary>
				/// 透明ガイド.
				/// </summary>
				protected bool secretGuide { get; set; }
				/// <summary>
				/// 透明マーカー.
				/// </summary>
				protected bool secretMarker { get; set; }
				
				public long fadeSpeed { get; set; }
				
				public long visualizeTime { get; set; }
				public long moveStartTime { get; set; }
				public long moveEndTime { get; set; }
				public Document.Slide.Chain[] chains { get; set; }
				public Document.Slide.WideMarker[][] wideMarkers { get; set; } //[縦の並び][横の並び]	

				private MaimaiSlideNoteObjectManager slideMng { get; set; }
				private GameObject slideHeadClusterObj;
				
				public Slide(RectTransform managerCluster,
				             RectTransform slideGuideCanvas,
				             RectTransform slideMarkerCanvas,
				             Kernel.Slide kernel,
				           	 CommonInformation objectCommonInfo,
				             long visualizeTime,
				             long moveStartTime, 
				             long moveEndTime,
				             long fadeSpeed,
				             float markerScale,
				             float guideScale,
				             GuideColor color,
				             bool secretGuide,
				             bool secretMarker,
				             Document.Slide.Chain[] chains)
				: base(managerCluster, slideGuideCanvas, kernel, objectCommonInfo) {
					this.slideMarkerCanvas = slideMarkerCanvas;
					this.color = color;
					this.visualizeTime = visualizeTime;
					this.moveStartTime = moveStartTime;
					this.moveEndTime = moveEndTime;
					this.fadeSpeed = fadeSpeed;
					this.markerScale = markerScale;
					this.guideScale = guideScale;
					this.chains = chains;
					this.appearanceTime = visualizeTime - fadeSpeed;
					this.secretGuide = secretGuide;
					this.secretMarker = secretMarker;

					if (chains != null && chains.Length > 1) {
						int chainsAmount = chains.Length;
						int markerAmount = Constants.instance.WIDE_SLIDE_DEVIDE_AMOUNT;
						this.wideMarkers = new Document.Slide.WideMarker[markerAmount][];

						for (int m = 0; m < markerAmount; m++) {
							this.wideMarkers[m] = new Document.Slide.WideMarker[chainsAmount];
							for (int i = 0; i < chainsAmount; i++) {
								var wideMarker = new Document.Slide.WideMarker();
								wideMarker.chain = chains[i];
								wideMarker.marker = chains[i].markers[m];
								this.wideMarkers[m][i] = wideMarker;
							}
						}
					}
				}
				
				public Slide(RectTransform managerCluster,
				             RectTransform slideGuideCanvas,
				             RectTransform slideMarkerCanvas,
				             Kernel.Slide kernel,
				           	 CommonInformation objectCommonInfo, 
				             Document.Slide doc) : this(
					managerCluster, slideGuideCanvas, slideMarkerCanvas, kernel, objectCommonInfo, doc.pattern.visualizeTime, doc.pattern.moveStartTime, doc.pattern.moveEndTime, doc.pattern.fadeSpeed, doc.pattern.markerScale, doc.pattern.guideScale,
					doc.pattern.markerColor, doc.secret, doc.pattern.secret, doc.pattern.chains) {
					
				}
				
				public override void Create () {
					var mngobj = (GameObject)GameObject.Instantiate (commonInfo.prefabs.slideManagerPrefab);
					mngobj.transform.SetParentEx (managerCluster);
					var mng = mngobj.GetComponent<MaimaiSlideNoteObjectManager> ();
					slideMng = mng;

					var cgobj = (GameObject)GameObject.Instantiate (commonInfo.prefabs.slideGuideClusterPrefab);
					slideHeadClusterObj = cgobj;
					cgobj.transform.SetParentEx (canvas);
					cgobj.transform.SetAsFirstSibling ();

					var guideList = new List<MaimaiSlideGuideObjectController> ();
					for (int c = 0; c < chains.Length; c++) {
						var chain = chains [c];
						var ggobj = (GameObject)GameObject.Instantiate (commonInfo.prefabs.slideGuidePrefab);
						ggobj.transform.SetParentEx (cgobj);
						if (!chain.isSurface)
							ggobj.transform.SetAsFirstSibling ();
						else
							ggobj.transform.SetAsLastSibling ();
						var gctrl = ggobj.GetComponent<MaimaiSlideGuideObjectController> ();
						Sprite main, center;
						main = center = null;
						if (color == GuideColor.SINGLE) {
							main = SpriteTank.Get ("note_star");
							center = SpriteTank.Get ("center_blue");
						} else if (color == GuideColor.EACH) {
							main = SpriteTank.Get ("note_star_each");
							center = SpriteTank.Get ("center_yellow");
						}
						gctrl.Setup (mng, chain, guideScale, main, center, secretGuide);
						guideList.Add (gctrl);
					}
					if (chains.Length == 1) {
						var chain = chains [0];
						Sprite main = null;
						if (color == GuideColor.SINGLE) {
							main = SpriteTank.Get ("note_slide");
						}
						else if (color == GuideColor.EACH) {
							main = SpriteTank.Get ("note_slide_each");
						}
						var markerList = new List<MaimaiSlideMarkerObjectController> ();
						for (int i = 0; i < chain.markers.Length; i++) {
							var mgobj = (GameObject)GameObject.Instantiate (commonInfo.prefabs.slideMarkerPrefab);
							mgobj.transform.SetParentEx (slideMarkerCanvas);
							mgobj.transform.SetAsFirstSibling ();
							var mctrl = mgobj.GetComponent<MaimaiSlideMarkerObjectController> ();
							mctrl.Setup (mng, chain, chain.markers [i], markerScale, main, secretMarker);
							markerList.Add (mctrl);
						}
						mng.Setup (kernel, guideList.ToArray(), markerList.ToArray ());
					}
					else {
						Sprite main = null;
						if (color == GuideColor.SINGLE) {
							main = SpriteTank.Get ("note_slide_wide");
						}
						else if (color == GuideColor.EACH) {
							main = SpriteTank.Get ("note_slide_wide_each");
						}
						float wideSlideMarkerSize = Constants.instance.WIDE_SLIDE_MARKER_TEXTURE_SIZE * markerScale; //チェインスライド画像の縦の大きさが16.
						int markerAmount = wideMarkers.Length;
						int chainsAmount = wideMarkers [0].Length;
						// 基本メッシュを作る.
						Mesh wideMesh = SlideMeshCreator.CreateWideSlideMesh (chainsAmount);
						var markerList = new List<MaimaiWideSlideMarkerObjectController> ();
						for (int m = 0; m < markerAmount; m++) {
							var mgobj = (GameObject)GameObject.Instantiate (commonInfo.prefabs.wideSlideMarkerPrefab);
							mgobj.transform.SetParentEx (slideMarkerCanvas);
							mgobj.transform.SetAsFirstSibling ();
							var mctrl = mgobj.GetComponent<MaimaiWideSlideMarkerObjectController> ();
							var filter = mgobj.GetComponentInChildren<MeshFilter> ();
							filter.sharedMesh = wideMesh;
							var vertices = filter.mesh.vertices;
							// マーカーの位置から頂点を求める.
							for (int i = 0; i < chainsAmount; i++) {
								var marker = wideMarkers [m] [i].marker;
								// 上.
								var up = CircleCalculator.PointOnCircle (marker.position, wideSlideMarkerSize * 0.5f, marker.degree).ChangePosHandSystem ();
								vertices [i].x = up.x;
								vertices [i].y = up.y;
								// 下.
								var down = CircleCalculator.PointOnCircle (marker.position, wideSlideMarkerSize * 0.5f, marker.degree + 180f).ChangePosHandSystem ();
								vertices [vertices.Length - 1 - i].x = down.x;
								vertices [vertices.Length - 1 - i].y = down.y;
							}
							filter.mesh.vertices = vertices;
							// スライドメッシュに情報を付与する.
							var renderer = mgobj.GetComponentInChildren<WideSlideMarkerMeshRenderer> ();
							renderer.slideMesh = filter.mesh;
							renderer.surfaceTexture = main.texture;
							mctrl.Setup (mng, wideMarkers [m], secretMarker);
							markerList.Add (mctrl);
						}
						mng.Setup (kernel, guideList.ToArray(), markerList.ToArray ());
					}

					AddGameObjectManager (mng);
				}
				
				public override long GetScheduleTime() {
					return appearanceTime;
				}
				
				public override long GetScheduleEndTime() {
					return moveEndTime;
				}
				
				protected override void ReleaseGameObjectManagers () {
					base.ReleaseGameObjectManagers ();
					if (slideMng != null) {
						slideMng.Release ();
						slideMng = null;
					}
					if (slideHeadClusterObj != null) {
						GameObject.Destroy(slideHeadClusterObj);
						slideHeadClusterObj = null;
					}
				}

				/// <summary>
				/// スライドの進捗(0.0～1.0)を入力すると対応するスライドマーカーが消える.
				/// </summary>
				public void SetSlideProgress (float progress) {
					if (slideMng != null) {
						slideMng.movePercent = progress;
					}
				}

			}
			
			public abstract class MessageBase<TKernel, TManager, TController> : ObjectDesigner where TKernel : Kernel where TManager : IMaimaiObjectShowSwitch {
				protected new TKernel kernel { get { return (TKernel)base.kernel; } }
				protected RectTransform archCanvas { get; set; }
				public long appearTime { get; set; }
				public long disappearTime { get; set; }
				public string message { get; set; }
				public float scale { get; set; }
				public Color color { get; set; }
				public TextAnchor alignment { get; set; }
				public bool secret { get; set; }
				
				protected long appearTimeRange { get { return disappearTime - appearTime; } }
				
				public MessageBase(
					RectTransform managerCluster,
					RectTransform canvas,
					TKernel kernel,
					CommonInformation objectCommonInfo,
					long appearTime,
					long disappearTime,
					string message,
					float scale,
					Color color,
					bool secret,
					Alignment alignment) : base (managerCluster, canvas, kernel, objectCommonInfo) {
					this.appearTime = appearTime;
					this.disappearTime = disappearTime;
					this.message = message;
					this.scale = scale;
					this.color = color;
					this.secret = secret;
					if (alignment == Alignment.LEFT)
						this.alignment = TextAnchor.MiddleLeft;
					else if (alignment == Alignment.CENTER)
						this.alignment = TextAnchor.MiddleCenter;
					else if (alignment == Alignment.RIGHT)
						this.alignment = TextAnchor.MiddleRight;
				}

				public override void Create () {
					var mngobj = (GameObject)GameObject.Instantiate (GetManagerPrefab());
					mngobj.transform.SetParentEx (managerCluster);
					
					var gobj = (GameObject)GameObject.Instantiate (GetControllerPrefab());
					gobj.transform.SetParentEx(canvas);
					gobj.transform.SetAsFirstSibling ();
					var mng = MngSetup(mngobj, gobj);
					
					AddGameObjectManager (mng);
				}
				protected abstract GameObject GetManagerPrefab();
				protected abstract GameObject GetControllerPrefab();
				protected abstract TManager MngSetup (GameObject mngObj, GameObject ctrlObj);
				
				public override long GetScheduleTime() {
					return appearTime;
				}
				
				public override long GetScheduleEndTime() {
					return disappearTime;
				}
				
			}

			public class Message : MessageBase<Kernel.Message, MaimaiMessageNoteObjectManager, MaimaiMessageNoteObjectController> {
				public Vector2 position { get; set; }
				protected override GameObject GetManagerPrefab() { return commonInfo.prefabs.messageNoteManagerPrefab; }
				protected override GameObject GetControllerPrefab() { return commonInfo.prefabs.messageNoteObjectPrefab; }
				protected override MaimaiMessageNoteObjectManager MngSetup (GameObject mngObj, GameObject ctrlObj) {
					var mng = mngObj.GetComponent<MaimaiMessageNoteObjectManager>();
					var ctrl = ctrlObj.GetComponent<MaimaiMessageNoteObjectController>();
					ctrl.Setup(mng, message, position, scale, color, secret, alignment);
					mng.Setup(kernel, ctrl);
					return mng;
				}
				public Message(
					RectTransform managerCluster,
					RectTransform canvas,
					Kernel.Message kernel,
					CommonInformation objectCommonInfo,
					long appearTime,
					long disappearTime,
					string message,
					Vector2 position,
					float scale,
					Color color,
					bool secret,
					Alignment alignment)
				: base (managerCluster, canvas, kernel, objectCommonInfo,
					    appearTime, disappearTime, message, scale, color, secret, alignment) {
					this.position = position;
				}
				
				public Message(
					RectTransform managerCluster,
					RectTransform canvas,
					Kernel.Message kernel,
					CommonInformation objectCommonInfo,
					Document.Message doc)
					: this(managerCluster, canvas, kernel, objectCommonInfo,
					      doc.viewStartTime, doc.viewEndTime, doc.message, doc.position, doc.scale, doc.color, doc.secret, doc.alignment) {
				}
			}
			
			public class ScrollMessage : MessageBase<Kernel.ScrollMessage, MaimaiScrollMessageNoteObjectManager, MaimaiScrollMessageNoteObjectController> {
				public float scrollPosY { get; set; }
				protected override GameObject GetManagerPrefab() { return commonInfo.prefabs.scrollMessageNoteManagerPrefab; }
				protected override GameObject GetControllerPrefab() { return commonInfo.prefabs.scrollMessageNoteObjectPrefab; }
				protected override MaimaiScrollMessageNoteObjectManager MngSetup (GameObject mngObj, GameObject ctrlObj) {
					var mng = mngObj.GetComponent<MaimaiScrollMessageNoteObjectManager>();
					var ctrl = ctrlObj.GetComponent<MaimaiScrollMessageNoteObjectController>();
					ctrl.Setup(mng, message, scrollPosY, scale, color, secret, alignment);
					mng.Setup(kernel, ctrl);
					return mng;
				}
				public ScrollMessage(
					RectTransform managerCluster,
					RectTransform canvas,
					Kernel.ScrollMessage kernel,
					CommonInformation objectCommonInfo,
					long appearTime,
					long disappearTime,
					string message,
					float scrollPosY,
					float scale,
					Color color,
					bool secret,
					Alignment alignment)
					: base (managerCluster, canvas, kernel, objectCommonInfo,
					        appearTime, disappearTime, message, scale, color, secret, alignment) {
					this.scrollPosY = scrollPosY;
				}
				
				public ScrollMessage(
					RectTransform managerCluster,
					RectTransform canvas,
					Kernel.ScrollMessage kernel,
					CommonInformation objectCommonInfo,
					Document.ScrollMessage doc)
					: this(managerCluster, canvas, kernel, objectCommonInfo,
					      doc.viewStartTime, doc.viewEndTime, doc.message, doc.y, doc.scale, doc.color, doc.secret, doc.alignment) {
				}
			}
			
			public class Trap : ObjectDesigner {
				protected new Kernel.Trap kernel { get { return (Kernel.Trap)base.kernel; } }
				/// <summary>
				/// 触っちゃいけない開始時間.
				/// </summary>
				public long headTime { get; set; }
				/// <summary>
				/// 触っちゃいけない終了時間.
				/// </summary>
				public long footTime { get; set; }
				public long guideFadeSpeed { get; set; }
				public long guideSpeed { get { return guideFadeSpeed; } }
				
				protected Constants.SensorId sensorId { get; set; }
				/// <summary>
				/// 初期pos
				/// </summary>
				protected float pos {
					get {
						var circum = Constants.instance.SensorCircumference(sensorId);
						if (circum == Constants.Circumference.OUTER)
							return Constants.instance.MAIMAI_OUTER_RADIUS;
						else if (circum == Constants.Circumference.INNER)
							return Constants.instance.MAIMAI_INNER_RADIUS;
						else
							return 0;
					}
				}
				/// <summary>
				/// 初期角度deg
				/// </summary>
				protected float angle {
					get {
						return Constants.instance.GetSensorDegFromId(sensorId);
					}
				}
				/// <summary>
				/// オブジェクトの大きさ
				/// </summary>
				protected float guideScale { get; set; }
				/// <summary>
				/// 表示開始時間
				/// </summary>
				public long appearanceTime { get; protected set; }
				/// <summary>
				/// 透明ノート.
				/// </summary>
				protected bool secret { get; set; }
				
				
				public Trap(RectTransform managerCluster,
				            RectTransform canvas,
				            Kernel.Trap kernel,
				            CommonInformation objectCommonInfo,
				            long headTime,
				            long footTime,
				            long guideFadeSpeed, 
				            float guideScale,
				            Constants.SensorId sensorId,
				            bool secret)
				: base(managerCluster, canvas, kernel, objectCommonInfo) {
					this.headTime = headTime;
					this.footTime = footTime;
					this.guideFadeSpeed = guideFadeSpeed;
					this.guideScale = guideScale;
					this.appearanceTime = headTime - guideSpeed;
					this.sensorId = sensorId;
					this.secret = secret;
				}
				
				public Trap(RectTransform managerCluster,
				            RectTransform canvas,
				            Kernel.Trap kernel,
				            CommonInformation objectCommonInfo, 
				            Document.Trap doc) : this(
					managerCluster, canvas, kernel, objectCommonInfo, doc.headTime, doc.footTime, doc.guideFadeSpeed, doc.guideScale,
					doc.sensor, doc.secret) {
					
				}
				public override void Create () {
					var mngobj = (GameObject)GameObject.Instantiate (commonInfo.prefabs.trapManagerPrefab);
					mngobj.transform.SetParentEx (managerCluster);
					var mng = mngobj.GetComponent<MaimaiTrapNoteObjectManager> ();
					
					var gobj = (GameObject)GameObject.Instantiate (commonInfo.prefabs.trapObjectPrefab);
					gobj.transform.SetParentEx(canvas);
					gobj.transform.SetAsFirstSibling ();
					var ctrl = gobj.GetComponent<MaimaiTrapNoteObjectController> ();
					ctrl.Setup (mng, pos, angle, guideScale, secret);

					mng.Setup (kernel, ctrl);
					
					AddGameObjectManager (mng);
				}
				
				public override long GetScheduleTime() {
					return appearanceTime;
				}
				
				public override long GetScheduleEndTime() {
					return footTime;
				}
				
			}

		}
	
		public class EachArchDesigner : IMaimaiSchedule, IMaimaiObjectDesigner {
			protected List<EachArchKernel> kernels { get; private set; }
			public CommonInformation commonInfo { get; private set; }
			protected RectTransform managerCluster;
			protected RectTransform canvas;
			public long justTime { get; set; }
			protected MaimaiEachArchObjectManager createdObject { get; private set; }
			
			public long guideFadeSpeed { get; set; }
			public long guideMoveSpeed { get; set; }
			public long guideSpeed { get { return guideMoveSpeed + guideFadeSpeed; } }
			/// <summary>
			/// 初期角度deg
			/// </summary>
			protected float angle {
				get {
					return Constants.instance.GetPieceDegree(angleButtonId - 1);
				}
			}
			/// <summary>
			/// 表示開始時間
			/// </summary>
			public long appearanceTime { get; protected set; }

			protected int angleButtonId { get; set; }

			protected Sprite sprite { get; set; }

			public EachArchDesigner (RectTransform managerCluster, RectTransform canvas, CommonInformation objectCommonInfo, long justTime, long guideFadeSpeed, long guideMoveSpeed) {
				this.kernels = new List<EachArchKernel>();
				this.managerCluster = managerCluster;
				this.canvas = canvas;
				this.commonInfo = objectCommonInfo;
				this.justTime = justTime;
				this.guideFadeSpeed = guideFadeSpeed;
				this.guideMoveSpeed = guideMoveSpeed;
				this.appearanceTime = justTime - guideSpeed;
				this.sprite = null;
				this.createdObject = null;
			}

			protected Kernel[] GetKernels() {
				int kernelAmount = kernels.Count;
				var ret = new Kernel[kernelAmount];
				for (int i = 0; i < kernelAmount; i++) {
					ret[i] = kernels[i].kernel;
				}
				return ret;
			}
			
			protected int[] GetButtonIds() {
				int kernelAmount = kernels.Count;
				var buttonIds = new int[kernelAmount];
				for (int i = 0; i < kernelAmount; i++) {
					buttonIds[i] = kernels[i].buttonId;
				}
				return buttonIds;
			}

			public void Setup (Kernel kernel, int buttonId) {
				kernels.Add (new EachArchKernel(kernel, buttonId));
			}

			public void FinalSetup() {
				sprite = GetEachOvalSprite (GetButtonIds());
			}

			public virtual void Create () {
				if (sprite == null)
					sprite = GetEachOvalSprite (GetButtonIds());
				if (sprite != null) {
					var mngobj = (GameObject)GameObject.Instantiate (commonInfo.prefabs.eachArchManagerPrefab);
					mngobj.transform.SetParentEx (managerCluster);
					var mng = mngobj.GetComponent<MaimaiEachArchObjectManager> ();

					var gobj = (GameObject)GameObject.Instantiate (commonInfo.prefabs.eachArchObjectPrefab);
					gobj.transform.SetParentEx (canvas);
					gobj.transform.SetAsFirstSibling ();
					var ctrl = gobj.GetComponent<MaimaiEachArchObjectController> ();

					ctrl.Setup (mng, angle, justTime, guideFadeSpeed, guideMoveSpeed, sprite);
					mng.Setup (GetKernels(), ctrl);

					AddGameObjectManager (mng);
				}
			}
			
			public long GetScheduleTime() {
				return appearanceTime;
			}
			
			public long GetScheduleEndTime() {
				// ホールドがあったら、一番最後に離すホールドフットの時間を返す.
				// なかったらタップかブレークなので、justTimeを返せばいい.
				long time = justTime;
				foreach (var kernel in kernels) {
					if (kernel.kernel.GetNoteType() == Kernel.KernelType.HOLD) {
						time = System.Math.Max ((kernel.kernel as Kernel.Hold).designer.footTime, time);
					}
				}
				return time;
			}
			
			protected void AddGameObjectManager (MaimaiEachArchObjectManager obj) {
				this.createdObject = obj;
				obj.Hide ();
			}
			
			protected void ReleaseGameObjectManagers () {
				if (createdObject != null) {
					createdObject.Release();
					createdObject = null;
				}
			}
			
			public virtual void ScheduleToDo(long nowTime) {
				if (commonInfo.realtimeCreateObject) {
					ReleaseGameObjectManagers ();
					Create ();
				}
				Show ();
			}
			
			public virtual void Show() {
				if (createdObject != null) {
					createdObject.Show ();
				}
			}
			
			public virtual void Hide() {
				if (createdObject != null) {
					if (commonInfo.realtimeCreateObject) {
						ReleaseGameObjectManagers ();
					}
					else {
						createdObject.Hide ();
					}
				}
			}

			/// <summary>
			/// ノートが独立状態かチェックして独立だったらHide、そうでなければスプライトを変更する、.
			/// </summary>
			public virtual void HideIfIndependentState() {
				//kernelsをチェックして独立を確認する.
				int remain = this.kernels.Count;
				var buttonIdList = new List<int> ();
				foreach (var kernel in kernels) {
					if (kernel.kernel.evaluated) {
						remain--;
					}
					else {
						buttonIdList.Add (kernel.buttonId);
					}
				}
				// 独立していたら.
				if (remain < 2) {
					// 隠す.
					this.Hide ();
				}
				else {
					// スプライト変更.
					this.sprite = GetEachOvalSprite(buttonIdList.ToArray());
					if (this.sprite != null && this.createdObject != null) {
						this.createdObject.ChangeSprite (this.sprite, angle);
					}
				}
			}
			
			public int GetSourceIndex () {
				return 0; //EachArchは作る順番とかどうでもいい.
			}

			protected class EachArchKernel {
				public Kernel kernel;
				public int buttonId;
				public EachArchKernel(Kernel kernel, int buttonId) {
					this.kernel = kernel;
					this.buttonId = buttonId;
				}
			}

			private class EachButtonMemo {
				public int button1 { get; private set; }
				public int button2 { get; private set; }
				public int clockwiseSub { get; private set; }
				public int sub { get; private set; }
				public bool isClockwise { get; private set; }
				public int result { get; private set; }
				public EachButtonMemo(int button1Id, int button2Id) {
					button1 = button1Id;
					button2 = button2Id;
					var b1 = button1Id - 1;
					var b2 = button2Id - 1;
					for (int b = 0; b < 8; b++) {
						if ((b1 + b) % 8 == b2) {
							clockwiseSub = b;
							break;
						}
					}
					if (clockwiseSub > 4) { 
						sub = 8 - clockwiseSub;
						isClockwise = false;
						result = button2;
					}
					else { 
						sub = clockwiseSub;
						isClockwise = true;
						result = button1;
					}
				}
				/// <summary>
				/// <para>途中で通過するボタンID群.</para>
				/// <para>8>3なら[8,1,2,3]を返す</para>
				/// </summary>
				public int[] PassingButtonIds () {
					var list = new List<int> ();
					int buttonAIndex = isClockwise ? button1 - 1 : button2 - 1;
					for (int i = 0; i < sub; i++) {
						int buttonBIndex = (buttonAIndex + i) % 8;
						list.Add (buttonBIndex + 1);
					}
					return list.ToArray ();
				}
			}

			protected Sprite GetEachOvalSprite(int[] buttonIds) {
				if (buttonIds.Length < 2)
					return null;
				bool isBreak = false; //ブレークが混ざると赤いイーチアーチになってた旧仕様.今は使ってない.
				int find = -1;
				var memos = new List<EachButtonMemo> ();
				// どれくらい離れているか求める.
				for (int i = 0; i < buttonIds.Length; i++) {
					for (int j = i + 1; j < buttonIds.Length; j++) {
						memos.Add (new EachButtonMemo(buttonIds[i], buttonIds[j]));
					}
				}
				// 一番離れているボタンペアを探す.
				int memolen = memos.Count;
				bool[] passingButtons = new bool[8];
				EachButtonMemo memo = null;
				for (int i = 0; i < memolen; i++) {
					if (memo == null || memos[i].sub > memo.sub) {
						memo = memos[i];
					}
					var passing = memos[i].PassingButtonIds();
					for (int p = 0; p < passing.Length; p++) {
						passingButtons[passing[p] - 1] = true;
					}
				}
				// 通過するボタンを確認する.
				bool allPassing = true;
				for (int p = 0; p < passingButtons.Length; p++) {
					if (!passingButtons[p]) {
						allPassing = false;
						break;
					}
				}
				if (allPassing) {
					// 全ボタン通過するなら全固定.
					find = 4;
				}
				else {
					// 0,1,2,3でどれだけ離れているか.
					find = memo.sub;
				}
				// 時計回りの先端のうち時計回りとして小さい方.
				angleButtonId = memo.result;
				Sprite ret = null;
				if (find == 1) {
					if (!isBreak) {
						ret = MaipadDynamicCreatedSpriteTank.instance.eachArc1;
					} else {
						ret = MaipadDynamicCreatedSpriteTank.instance.breakEachArc1;
					}
				} else if (find == 2) {
					if (!isBreak) {
						ret = MaipadDynamicCreatedSpriteTank.instance.eachArc2;
					} else {
						ret = MaipadDynamicCreatedSpriteTank.instance.breakEachArc2;
					}
				} else if (find == 3) {
					if (!isBreak) {
						ret = MaipadDynamicCreatedSpriteTank.instance.eachArc3;
					} else {
						ret = MaipadDynamicCreatedSpriteTank.instance.breakEachArc3;
					}
				} else if (find == 4) {
					if (!isBreak) {
						ret = MaipadDynamicCreatedSpriteTank.instance.eachArc4;
					} else {
						ret = MaipadDynamicCreatedSpriteTank.instance.breakEachArc4;
					}
				}
				return ret;
			}
		}
		
		public class NoteSoundMessageDesigner : IMaimaiSchedule {
			public CommonInformation commonInfo { get; private set; }
			protected int sourceIndex { get; set; }
			protected string soundId;
			protected long time;
			protected bool secret;
			
			public NoteSoundMessageDesigner(
				CommonInformation objectCommonInfo,
				int sourceIndex,
				string soundId,
				long time,
				bool secret) {
				this.commonInfo = objectCommonInfo;
				this.sourceIndex = sourceIndex;
				this.soundId = soundId;
				this.time = time;
				this.secret = secret;
			}
			
			public NoteSoundMessageDesigner(
				CommonInformation objectCommonInfo,
				int sourceIndex,
				Document.SoundMessage doc)
				: this(objectCommonInfo, sourceIndex,
				      doc.soundId, doc.playStartTime, doc.secret) {
			}

			public long GetScheduleTime() {
				return time;
			}
			
			public long GetScheduleEndTime() {
				return GetScheduleTime();
			}
			
			public void ScheduleToDo(long nowTime) {
				if (soundId != null) {
					Utility.SoundEffectManager.Play (soundId);
				}
			}
			
			public int GetSourceIndex() {
				return sourceIndex;
			}

		}



	}
}