﻿using UnityEngine;
using System.Collections;

public class MaimaiTrapNoteObjectController : MaimaiNoteObjectController {
	[SerializeField]
	private RectTransform positionElement;
	[SerializeField]
	private RectTransform angleElement;
	[SerializeField]
	private RectTransform InisializeScaleElement;
	[SerializeField]
	private UnityEngine.UI.Image headImageElement;
	[SerializeField]
	private UnityEngine.UI.Image footImageElement;
	
	protected MaimaiTrapNoteObjectManager noteObjMng;
	
	public void Setup(MaimaiTrapNoteObjectManager noteObjMng, float pos, float angle, float scale, bool secret) {
		base.Setup(secret);
		this.noteObjMng = noteObjMng;
		positionElement.anchoredPosition = new Vector2(0, pos);
		angleElement.localRotation = Quaternion.Euler (0, 0, Constants.instance.ToLeftHandedCoordinateSystemDegree(angle));
		InisializeScaleElement.localScale = new Vector3 (scale, scale, 1);
		headImageElement.rectTransform.localRotation = footImageElement.rectTransform.localRotation = Quaternion.Euler (0, 0, Constants.instance.ToLeftHandedCoordinateSystemDegree(-angle));
		footImageElement.rectTransform.localScale = Vector3.zero;
	}
	
	public override void Move () {
		if (this.noteObjMng == null) return;
		
		float fadePercent = noteObjMng.fadePercent;
		float throughPercent = noteObjMng.throughPercent;
		// ノートの不透明化.
		SetAlpha (fadePercent, headImageElement);
		// ノートの拡大
		footImageElement.rectTransform.localScale = new Vector3 (throughPercent, throughPercent, 1);
	}
	
	public override void Release () {
		this.noteObjMng = null;
		base.Release ();
	}
}
