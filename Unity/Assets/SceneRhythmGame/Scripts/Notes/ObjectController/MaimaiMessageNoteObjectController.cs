using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MaimaiMessageNoteObjectController : MaimaiNoteObjectController {
	
	[SerializeField]
	private RectTransform positionElement;
	[SerializeField]
	private RectTransform scaleElement;
	[SerializeField]
	private Text label;

	protected MaimaiMessageNoteObjectManager noteObjMng;

	public void Setup(MaimaiMessageNoteObjectManager noteObjMng, string message, Vector2 position, float scale, Color color, bool secret, TextAnchor alignment) {
		base.Setup(secret);
		this.noteObjMng = noteObjMng;
		label.text = message;
		label.alignment = alignment;
		positionElement.anchoredPosition = position;
		scaleElement.localScale = new Vector3(scale, scale, 1);
		float secretalpha = secret ? 0 : 1;
		label.color = new Color(color.r, color.g, color.b, color.a * secretalpha);
	}
	
	public override void Move () {

	}
	
	public override void Release () {
		this.noteObjMng = null;
		base.Release ();
	}
}
