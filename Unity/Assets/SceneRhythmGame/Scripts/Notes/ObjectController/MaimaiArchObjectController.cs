﻿using UnityEngine;
using System.Collections;
using XMaimaiNote.XKernel;

public class MaimaiArchObjectController : MaimaiNoteObjectController {
	[SerializeField]
	private RectTransform angleElement;
	[SerializeField]
	private RectTransform arcScaleElement;
	[SerializeField]
	private UnityEngine.UI.Image arcImageElement;
	
	protected IMaimaiArchObjectManager noteObjMng;
	
	public void Setup(IMaimaiArchObjectManager noteObjMng, float angle, long justTime, long guideFadeSpeed, long guideMoveSpeed, Sprite sprite, bool secret) {
		base.Setup(secret);
		this.noteObjMng = noteObjMng;
		angleElement.localRotation = Quaternion.Euler (0, 0, Constants.instance.ToLeftHandedCoordinateSystemDegree(angle));
		arcImageElement.sprite = sprite;
	}
	
	public override void Move () {
		if (this.noteObjMng == null) return;

		float fadePercent = noteObjMng.fadePercent;
		float archScalePercent = noteObjMng.archScalePercent;
		// ノートの不透明化.
		SetAlpha (fadePercent, arcImageElement);
		// 円弧の移動 (に見せかけた拡大)
		float arcInitScale = Constants.instance.NOTE_MOVE_START_MARGIN / Constants.instance.MAIMAI_OUTER_RADIUS;
		float arcRemainScale = 1.0f - arcInitScale;
		float arcPercent = archScalePercent * arcRemainScale + arcInitScale;
		arcScaleElement.localScale = new Vector3 (arcPercent, arcPercent, 1);
	}
	
	public override void Release () {
		this.noteObjMng = null;
		base.Release ();
	}

}
