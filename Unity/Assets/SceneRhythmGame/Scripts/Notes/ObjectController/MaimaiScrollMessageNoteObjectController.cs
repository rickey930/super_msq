﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MaimaiScrollMessageNoteObjectController : MaimaiNoteObjectController {

	[SerializeField]
	private GameObject entityObject;
	[SerializeField]
	private RectTransform moveElement;
	[SerializeField]
	private RectTransform scaleElement;
	[SerializeField]
	private Text label;
	
	protected MaimaiScrollMessageNoteObjectManager noteObjMng;

	[SerializeField]
	private Text verificationLabel;
	[SerializeField]
	private RectTransform verificationElement;

	private const float gameScreenRadius = 384.0f * 0.5f;
	private const float outOfScreen = 10000.0f;
	private float scrollPosY;
	private float scale;
	private float alpha;
	
	public void Setup(MaimaiScrollMessageNoteObjectManager noteObjMng, string message, float y, float scale, Color color, bool secret, TextAnchor alignment) {
		base.Setup(secret);
		this.noteObjMng = noteObjMng;
		label.text = message;
		verificationLabel.text = message;
		scrollPosY = y;
		label.alignment = alignment;
		scaleElement.localScale = new Vector3(scale, scale, 1);
		this.scale = scale;
		this.alpha = color.a;
		label.color = color;
	}
	
	public override void Move () {
		if (this.noteObjMng == null) return;

		float movePercent = noteObjMng.movePercent;
		// ノートの移動
		if (verificationElement.rect.width == 0) {
			SetAlpha(0, label);
		}
		else {
			SetAlpha(alpha, 1, label);
			float radius = gameScreenRadius + verificationElement.rect.width * 0.5f * scale;
			float x = radius - (radius * 2 * movePercent);
			moveElement.anchoredPosition = new Vector2 (x, scrollPosY);
		}
	}
	
	public override void Release () {
		this.noteObjMng = null;
		base.Release ();
	}
}
