﻿using UnityEngine;
using System.Collections.Generic;
using XMaimaiNote.XKernel;

public class MaimaiEachArchObjectController : MaimaiNoteObjectController {
	[SerializeField]
	private RectTransform angleElement;
	[SerializeField]
	private RectTransform arcScaleElement;
	[SerializeField]
	private UnityEngine.UI.Image arcImageElement;

	protected MaimaiEachArchObjectManager noteObjMng;

	public void Setup(MaimaiEachArchObjectManager noteObjMng, float angle, long justTime, long guideFadeSpeed, long guideMoveSpeed, Sprite sprite) {
		base.Setup(false);
		this.noteObjMng = noteObjMng;
		ChangeSprite (sprite);
		ChangeAngle (angle);
	}
	
	public void ChangeSprite (Sprite sprite) {
		arcImageElement.sprite = sprite;
	}
	
	public void ChangeAngle (float angle) {
		angleElement.localRotation = Quaternion.Euler (0, 0, Constants.instance.ToLeftHandedCoordinateSystemDegree(angle));
	}

	public override void Move () {
		if (this.noteObjMng == null) return;

		float fadePercent = noteObjMng.fadePercent;
		float archScalePercent = noteObjMng.scalePercent;
		// ノートの不透明化.
		SetAlpha (fadePercent, arcImageElement);
		// 円弧の移動 (に見せかけた拡大)
		float arcInitScale = Constants.instance.NOTE_MOVE_START_MARGIN / Constants.instance.MAIMAI_OUTER_RADIUS;
		float arcRemainScale = 1.0f - arcInitScale;
		float arcPercent = archScalePercent * arcRemainScale + arcInitScale;
		arcScaleElement.localScale = new Vector3 (arcPercent, arcPercent, 1);
	}
	
	public override void Release () {
		this.noteObjMng = null;
		base.Release ();
	}

}
