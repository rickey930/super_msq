﻿using UnityEngine;
using System.Collections;
using XMaimaiNote.XKernel;

public class MaimaiHoldNoteObjectController : MaimaiNoteObjectController {
	[SerializeField]
	private RectTransform angleElement;
	[SerializeField]
	private RectTransform headMoveElement;
	[SerializeField]
	private RectTransform headScaleElement;
	[SerializeField]
	private RectTransform InitializeHeadScaleElement;
	[SerializeField]
	private RectTransform footMoveElement;
	[SerializeField]
	private RectTransform footScaleElement;
	[SerializeField]
	private RectTransform InitializeFootScaleElement;
	[SerializeField]
	private RectTransform bodyMoveElement;
	[SerializeField]
	private RectTransform bodyScaleElement;
	[SerializeField]
	private RectTransform InitializeBodyScaleElement;
	[SerializeField]
	private UnityEngine.UI.Image headImageElement;
	[SerializeField]
	private UnityEngine.UI.Image bodyImageElement;
	[SerializeField]
	private UnityEngine.UI.Image footImageElement;
	[SerializeField]
	private UnityEngine.UI.Image headCenterImageElement;
	[SerializeField]
	private UnityEngine.UI.Image footCenterImageElement;
	
	protected MaimaiHoldNoteObjectManager noteObjMng;
	protected float guideScale { get; set; }
	
	public void Setup(MaimaiHoldNoteObjectManager noteObjMng, float angle, float scale, Sprite headImage, Sprite footImage, Sprite bodyImage, Sprite centerImage, bool secret) {
		base.Setup(secret);
		this.noteObjMng = noteObjMng;
		angleElement.localRotation = Quaternion.Euler (0, 0, Constants.instance.ToLeftHandedCoordinateSystemDegree(angle));
		InitializeHeadScaleElement.localScale = new Vector3 (scale, scale, 1);
		InitializeFootScaleElement.localScale = new Vector3 (scale, scale, 1);
		InitializeBodyScaleElement.localScale = new Vector3 (scale, scale, 1);
		headImageElement.sprite = headImage;
		bodyImageElement.sprite = bodyImage;
		footImageElement.sprite = footImage;
		headCenterImageElement.sprite = centerImage;
		footCenterImageElement.sprite = centerImage;
		guideScale = scale;
		footCenterImageElement.gameObject.SetActive (false);
	}

	public override void Move () {
		if (this.noteObjMng == null) return;

		float fadePercent = noteObjMng.fadePercent;
		// ノートの不透明化.
		SetAlpha (fadePercent, headImageElement, bodyImageElement, footImageElement, headCenterImageElement, footCenterImageElement);

		// 【下半身の移動】.
		float footMovePercent = noteObjMng.footMovePercent;
		// ノートの拡大.
		footScaleElement.localScale = new Vector3 (fadePercent, fadePercent, 1);
		// ノートの移動.
		float fy = (footMovePercent * (Constants.instance.MAIMAI_OUTER_RADIUS - Constants.instance.NOTE_MOVE_START_MARGIN));
		footMoveElement.anchoredPosition = new Vector2(0, fy);
		// センターのちっこいの表示.
		if (footMovePercent > 0 && !footCenterImageElement.gameObject.activeSelf) footCenterImageElement.gameObject.SetActive (true);
		
		// 【上半身の移動】.
		float headMovePercent = noteObjMng.headMovePercent;
		// ノートの拡大.
		headScaleElement.localScale = new Vector3 (fadePercent, fadePercent, 1);
		// ノートの移動.
		float hy = (headMovePercent * (Constants.instance.MAIMAI_OUTER_RADIUS - Constants.instance.NOTE_MOVE_START_MARGIN));
		headMoveElement.anchoredPosition = new Vector2(0, hy);

		// 【体の伸縮】.
		float optionScale = guideScale;
		float bodyScaleOffset = 1 / optionScale;
		float bodyHeight = hy - fy;
		bodyScaleElement.localScale = new Vector3 (1, bodyHeight * bodyScaleOffset, 1);
		bodyMoveElement.anchoredPosition = new Vector2 (0, bodyHeight / 2.0f + fy);
	}
	
	public override void Release () {
		this.noteObjMng = null;
		base.Release ();
	}

}
