﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public abstract class MaimaiNoteObjectController : MonoBehaviour {
	protected int secret { get; set; }
	protected void Setup (bool secret) {
		this.secret = secret ? 0 : 1;
	}

	public abstract void Move();

	public virtual void Show () {
		gameObject.SetActive (true);
	}

	public virtual void Hide () {
		gameObject.SetActive (false);
	}

	public virtual void Reset () {

	}

	public virtual void Release () {
		Destroy (this.gameObject);
	}
	
	protected void SetAlpha(float source, float fade, params UnityEngine.UI.Image[] images) {
		foreach (var image in images) {
			var color = image.color;
			color.a = source * fade * secret;
			image.color = color;
		}
	}
	
	protected void SetAlpha(float fade, params UnityEngine.UI.Image[] images) {
		SetAlpha(1, fade, images);
	}

	protected void SetAlpha(float source, float fade, params Graphic[] graphics) {
		foreach (var graphic in graphics) {
			if (graphic.material.HasProperty ("_SpColor")) {
				Color temp = graphic.material.GetColor ("_SpColor");
				temp.a = source * fade * secret;
				graphic.material.SetColor ("_SpColor", temp);
			}
			else {
				var color = graphic.color;
				color.a = source * fade * secret;
				graphic.color = color;
			}
		}
	}
	
	protected void SetAlpha(float fade, params Graphic[] graphics) {
		SetAlpha(1, fade, graphics);
	}
}
