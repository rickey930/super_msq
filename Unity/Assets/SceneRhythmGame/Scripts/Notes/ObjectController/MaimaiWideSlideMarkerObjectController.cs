using UnityEngine;
using System.Collections;
using XMaimaiNote.XKernel;

public class MaimaiWideSlideMarkerObjectController : MaimaiNoteObjectController, IMaimaiSlideMarkerController {
	[SerializeField]
	private WideSlideMarkerMeshRenderer wideSlideMarkerMesh;
	
	protected MaimaiSlideNoteObjectManager noteObjMng;
	protected XMaimaiNote.XDocument.Document.Slide.WideMarker[] wideMarkerInfo { get; set; }

	private Material copyMaterial;

	public void Setup(MaimaiSlideNoteObjectManager noteObjMng, XMaimaiNote.XDocument.Document.Slide.WideMarker[] wideMarkerInfo, bool secret) {
		base.Setup(secret);
		this.copyMaterial = new Material (this.wideSlideMarkerMesh.material);
		this.wideSlideMarkerMesh.material = this.copyMaterial; 
		this.noteObjMng = noteObjMng;
		this.wideMarkerInfo = wideMarkerInfo;
	}
	
	public override void Move () {
		if (this.noteObjMng == null || this.wideMarkerInfo == null) return;

		float fadePercent = noteObjMng.fadePercent;
		// ノートの不透明化.
		SetAlpha (fadePercent, wideSlideMarkerMesh);

		float movePercent = noteObjMng.movePercent;
		
		// すべての星が、それぞれのチェインのそれぞれのマーカーの距離を越していたら、消す.
		bool allChecked = true;
		foreach (var info in wideMarkerInfo) {
			// ガイドの移動
			// 現在の移動距離 = 全体の距離 * 移動割合.
			var nowMoveDistance = info.chain.totalDistance * movePercent; //movePercent<1.0fはstartとtargetの同位置対策
			if (info.marker.distanceFromStart > nowMoveDistance && movePercent < 1.0f) {
				allChecked = false;
				break;
			}
		}
		if (allChecked) {
			if (gameObject.activeSelf) {
				gameObject.SetActive(false);
			}
		}
		else {
			if (!gameObject.activeSelf) {
				gameObject.SetActive(true);
			}
		}
	}

	void OnDestroy () {
		if (copyMaterial != null) {
			GameObject.Destroy (copyMaterial);
		}
	}
	
	public override void Release () {
		this.noteObjMng = null;
		this.wideMarkerInfo = null;
		base.Release ();
	}

}
