﻿using UnityEngine;
using System.Collections;
using XMaimaiNote.XKernel;

public class MaimaiSlideMarkerObjectController : MaimaiNoteObjectController, IMaimaiSlideMarkerController {
	[SerializeField]
	private RectTransform positionElement;
	[SerializeField]
	private RectTransform InisializeScaleElement;
	[SerializeField]
	private UnityEngine.UI.Image mainImageElement;
	
	protected MaimaiSlideNoteObjectManager noteObjMng;
	protected XMaimaiNote.XDocument.Document.Slide.Chain chain { get; set; }
	protected XMaimaiNote.XDocument.Document.Slide.Marker marker { get; set; }

	public void Setup(MaimaiSlideNoteObjectManager noteObjMng, XMaimaiNote.XDocument.Document.Slide.Chain chain, XMaimaiNote.XDocument.Document.Slide.Marker marker, float scale, Sprite mainImage, bool secret) {
		base.Setup(secret);
		this.noteObjMng = noteObjMng;
		this.chain = chain;
		this.marker = marker;
		InisializeScaleElement.localScale = new Vector3 (1.0f, 1.0f, scale);
		positionElement.anchoredPosition = Constants.instance.ToLeftHandedCoordinateSystemPosition(marker.position);
		positionElement.localRotation = Quaternion.Euler (0, 0, Constants.instance.ToLeftHandedCoordinateSystemDegree(marker.degree));
		mainImageElement.sprite = mainImage;
	}
	
	public override void Move () {
		if (this.noteObjMng == null || this.chain == null || this.marker == null) return;

		float fadePercent = noteObjMng.fadePercent;
		// ノートの不透明化.
		SetAlpha (fadePercent, mainImageElement);

		float movePercent = noteObjMng.movePercent;
		// ガイドの移動
		// 現在の移動距離 = 全体の距離 * 移動割合.
		var nowMoveDistance = chain.totalDistance * movePercent; //movePercent>=1.0fはstartとtargetの同位置対策
		if (marker.distanceFromStart <= nowMoveDistance || movePercent >= 1.0f) {
			if (gameObject.activeSelf) {
				gameObject.SetActive(false);
			}
		}
		else {
			if (!gameObject.activeSelf) {
				gameObject.SetActive(true);
			}
		}
	}

	public override void Release () {
		this.noteObjMng = null;
		this.chain = null;
		this.marker = null;
		base.Release ();
	}

}
