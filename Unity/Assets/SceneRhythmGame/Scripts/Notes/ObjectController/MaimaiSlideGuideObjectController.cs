﻿using UnityEngine;
using System.Collections;
using XMaimaiNote.XKernel;

public class MaimaiSlideGuideObjectController : MaimaiNoteObjectController {
	[SerializeField]
	private RectTransform positionElement;
	[SerializeField]
	private RectTransform angleElement;
	[SerializeField]
	private RectTransform scaleElement;
	[SerializeField]
	private RectTransform InisializeScaleElement;
	[SerializeField]
	private UnityEngine.UI.Image mainImageElement;
	[SerializeField]
	private UnityEngine.UI.Image centerImageElement;
	
	protected MaimaiSlideNoteObjectManager noteObjMng;
	protected XMaimaiNote.XDocument.Document.Slide.Chain chain { get; set; }
	private int chainIndex;

	public void Setup(MaimaiSlideNoteObjectManager noteObjMng, XMaimaiNote.XDocument.Document.Slide.Chain chain, float scale, Sprite mainImage, Sprite centerImage, bool secret) {
		base.Setup(secret);
		this.noteObjMng = noteObjMng;
		this.chain = chain;
		InisializeScaleElement.localScale = new Vector3 (scale, scale, 1);
		mainImageElement.sprite = mainImage;
		centerImageElement.sprite = centerImage;
		chainIndex = 0;
	}
	
	public override void Move () {
		if (this.noteObjMng == null || this.chain == null) return;

		float fadePercent = noteObjMng.fadePercent;
		float movePercent = noteObjMng.guideMovePercent;
		// ガイドの不透明化.
		SetAlpha (fadePercent, mainImageElement, centerImageElement);
		// ガイドの拡大
		scaleElement.localScale = new Vector3 (fadePercent, fadePercent, 1);
		// ガイドの移動
		// 現在の移動距離 = 全体の距離 * 移動割合.
		var nowMoveDistance = chain.guideTotalDistance * movePercent;
		// chainIndexは検索効率化用であるが、万が一chainIndexに異常が起きた時に最初から再検索できるように。.
		bool loop = true;
		bool chainIndexResetted = false;
		bool find = false;
		while (loop) {
			for (int i = chainIndex; i < chain.commands.Length; i++) {
				// コマンドの終点までの距離 >= 現在の移動距離 になるコマンドを探して、.
				if (chain.commands[i].totalDistanceToThisCommand >= nowMoveDistance) {
					var command = chain.commands[i];
					// そのコマンドにおける現在の移動割合 = (現在の移動距離 - コマンドの始点までの距離) / コマンドの距離.
					var movePercentInCommand = (nowMoveDistance - command.totalDistanceToPreviousCommand) / command.distance;
					if (command.type == XMaimaiNote.XDocument.Document.Slide.Command.Type.STRAIGHT) {
						var straight = (XMaimaiNote.XDocument.Document.Slide.Command.Straight)command;
						positionElement.anchoredPosition = Constants.instance.ToLeftHandedCoordinateSystemPosition(CircleCalculator.PointToPointMovePoint(straight.start, straight.target, straight.distance * movePercentInCommand));
						angleElement.localRotation = Quaternion.Euler(0, 0, Constants.instance.ToLeftHandedCoordinateSystemDegree(CircleCalculator.PointToDegree(straight.start, straight.target)));
					}
					else if (command.type == XMaimaiNote.XDocument.Document.Slide.Command.Type.CURVE) {
						var curve = (XMaimaiNote.XDocument.Document.Slide.Command.Curve)command;
						int vec = curve.degOfDistance < 0 ? -1 : 1;
						var pDeg = CircleCalculator.ArcDegreeFromArcDistance(curve.radius, curve.distance * movePercentInCommand);
						positionElement.anchoredPosition = Constants.instance.ToLeftHandedCoordinateSystemPosition(CircleCalculator.PointOnCircle(curve.axis, curve.radius, curve.degOfStart + pDeg * vec));
						angleElement.localRotation = Quaternion.Euler(0, 0, Constants.instance.ToLeftHandedCoordinateSystemDegree(curve.degOfStart + 90.0f + pDeg * vec + (vec > 0 ? 0 : 180)));
					}
					chainIndex = i;
					find = true;
					loop = false;
					break;
				}
			}
			if (chainIndexResetted) {
				loop = false;
			}
			if (!find) {
				chainIndex = 0;
				chainIndexResetted = true;
			}
		}

	}

	public override void Reset () {
		base.Reset();
		chainIndex = 0;
	}
	
	public override void Release () {
		this.noteObjMng = null;
		this.chain = null;
		base.Release ();
	}

}
