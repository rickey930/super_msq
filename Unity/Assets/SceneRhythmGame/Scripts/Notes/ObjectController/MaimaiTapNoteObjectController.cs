﻿using UnityEngine;
using System.Collections;
using XMaimaiNote.XKernel;

public class MaimaiTapNoteObjectController : MaimaiNoteObjectController {
	[SerializeField]
	private RectTransform angleElement;
	[SerializeField]
	private RectTransform moveElement;
	[SerializeField]
	private RectTransform spinElement;
	[SerializeField]
	private RectTransform scaleElement;
	[SerializeField]
	private RectTransform InisializeScaleElement;
	[SerializeField]
	private UnityEngine.UI.Image mainImageElement;
	[SerializeField]
	private UnityEngine.UI.Image centerImageElement;
	[SerializeField]
	private UnityEngine.UI.Image wideStarImageElement;

	protected MaimaiTapNoteObjectManager noteObjMng;
	protected float spinSpeed { get; set; }

	public void Setup(MaimaiTapNoteObjectManager noteObjMng, float angle, float scale, float spinSpeed, Sprite mainImage, Sprite centerImage, bool wstar, bool secret) {
		base.Setup(secret);
		this.noteObjMng = noteObjMng;
		angleElement.localRotation = Quaternion.Euler (0, 0, Constants.instance.ToLeftHandedCoordinateSystemDegree(angle));
		InisializeScaleElement.localScale = new Vector3 (scale, scale, 1);
		mainImageElement.sprite = mainImage;
		wideStarImageElement.sprite = mainImage;
		centerImageElement.sprite = centerImage;
		this.spinSpeed = Constants.instance.ToLeftHandedCoordinateSystemDegree (spinSpeed);
		wideStarImageElement.gameObject.SetActive (wstar);
	}

	public override void Move () {
		if (this.noteObjMng == null) return;

		float fadePercent = noteObjMng.fadePercent;
		float movePercent = noteObjMng.movePercent;
		// ノートの不透明化.
		SetAlpha (fadePercent, mainImageElement, centerImageElement);
		// ノートの拡大
		scaleElement.localScale = new Vector3 (fadePercent, fadePercent, 1);
		// ノートの移動
		float y = movePercent * (Constants.instance.MAIMAI_OUTER_RADIUS - Constants.instance.NOTE_MOVE_START_MARGIN);
		moveElement.anchoredPosition = new Vector2 (0, y);
		// 回転.
		spinElement.Rotate (new Vector3(0, 0, spinSpeed * Time.deltaTime * 2));
	}

	public override void Release () {
		this.noteObjMng = null;
		base.Release ();
	}

}
