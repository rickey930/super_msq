﻿using UnityEngine;
using System.Collections;

public class RhythmGameSceneNetworkController : MonoBehaviour {
	public int initializedPlayers { get; set; }

	[RPC]
	public void Counter () {
		initializedPlayers++;
	}
}
