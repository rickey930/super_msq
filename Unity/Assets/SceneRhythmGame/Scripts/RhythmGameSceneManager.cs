#define DEBUG_AUTO_PLAY
#define DEBUG_AUTO_PLAY_AND_ENDLESS

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RhythmGameSceneManager : MonoBehaviour {
	/*
評価マネージャー作ったけど、まだ未実験.
特にスライドの評価機能はもうちょっと見直したい.
	 */

	
	[SerializeField]
	private RectTransform noteObjMngCluster;
	[SerializeField]
	private RectTransform notesCanvas;
	[SerializeField]
	private RectTransform archCanvas;
	[SerializeField]
	private RectTransform eachArchCanvas;
	[SerializeField]
	private RectTransform slideGuideCanvas;
	[SerializeField]
	private RectTransform slideMarkerCanvas;
	[SerializeField]
	private RectTransform evaluateEffectCanvas;
	[SerializeField]
	private RectTransform evaluateTextureCanvas;
	[SerializeField]
	private RectTransform holdKeepEffectCanvas;
	[SerializeField]
	private RectTransform slideEvaluateTextureCanvas;
	[SerializeField]
	private RectTransform messageNotesCanvas;
	[SerializeField]
	private RectTransform trapNotesCanvas;
	[SerializeField]
	private RectTransform sensorSampleCanvas;
	[SerializeField]
	private Transform sensorCanvas;
	[SerializeField]
	private GameObject sensorPrefab;
	[SerializeField]
	private SensorEffectMother sensorEffectMother;
	[SerializeField]
	private MaimaiNotePrefabManager prefabManager;
	[SerializeField]
	private RhythmGameOverController gameoverCtrl;
	[SerializeField]
	private GameObject syncEvaluaterPrefab;

	private MaimaiTimer timer;
	private MaimaiScheduler designerScheduler;
	private MaimaiScheduler answerSoundScheduler; //アンサーサウンド.
	private MaimaiScheduler rhythmGameBeatCheckScheduler; //最初の4拍カウント.
	private XMaimaiNote.XObjectDesigner.CommonInformation objectInfo;
	private XMaimaiNote.XInspector.Inspector[] noteInspectors;
	private MaimaiJudgeEvaluateManager evaluater;
	private MaimaiSyncEvaluateManager syncEvaluater;
	private RhythmGameSceneUIController uiCtrl;
	private BgInfoController bgInfoCtrl;
	private bool initialized; //準備完了したのでUpdate()していいよ.
	private bool oldPauseState;
	private int syncNoteCount;

	private GameVirtualHardwareButton[] sensors { get; set; }

	private const int maxMultiPlayers = 2;

	private long finishTime { get; set; }
	private long restartTime { get; set; }
	
	// ノートオブジェクト用コンフィグ.
	protected bool autoPlay { get; set; }
	protected float guideFadeSpeed { get; set; }
	protected float guideMoveSpeed { get; set; }
	protected long judgeTimeLag { get; set; }
	protected bool starRotation { get; set; }
	protected bool angulatedHold { get; set; }
	protected bool pine { get; set; }
	protected float ringScale { get; set; }
	protected float markerScale { get; set; }
	protected float trapScale { get; set; }
	// 楽譜用コンフィグ.
	protected ConfigTypes.ChangeBreak changeBreakType { get; set; }
	protected ConfigTypes.ChangeRing changeRingType { get; set; }
	protected int turn { get; set; }
	protected bool mirror { get; set; }
	protected bool tapToBreak { get; set; }
	protected bool breakToTap { get; set; }
	protected bool ringToStar { get; set; }
	protected bool starToRing { get; set; }
	// 評価機用コンフィグ.
	protected bool startBeatCountSoundEnabled { get; set; }
	protected ConfigTypes.SoundEffect soundEffectType { get; set; }
	protected ConfigTypes.AnswerSound answerSoundType { get; set; }
	protected ConfigTypes.Judgement judgementType { get; set; }
	protected bool visualizeFastLate { get; set; }
	protected bool allowSkipSlide { get; set; }
	protected ConfigTypes.LifeDifficulty challengeLife { get; set; }
	// 表示用コンフィグ.
	protected ConfigTypes.BackGroundInfomation bgInfoType { get; set; }
	protected ConfigTypes.UpScreenAchievementVision achievementVisionType { get; set; }
	protected ConfigTypes.RankVersion rankVisionVersion { get; set; }
	protected bool realtimeCreateObject { get; set; }
	// リピート用データ.
	protected float repeatStartTimeRate { get; set; }
	protected long repeatTime { get; set; }
	// ポーズ状態制御.
	private float pauseTimer { get; set; }
	private float pauseTime { get { return 1.0f; } }
	// ゲーム終了制御
	private bool gameEndProcCalled { get; set; }
	
	public void NotePrepare(MaimaiScoreConverter converter) {
		int uniqueId = 0;
		var inspectors = new List<XMaimaiNote.XInspector.Inspector> ();
		var designers = new List<XMaimaiNote.XObjectDesigner.ObjectDesigner> ();
		var archDesigners = new Dictionary<int, XMaimaiNote.XObjectDesigner.EachArchDesigner> ();
		var beforehandDesigners = new List<IMaimaiObjectDesigner> ();
		var answersoundPlayers = new List<MaimaiAnswerSoundPlayer> ();
		var soundMessageNoteDesigners = new List<XMaimaiNote.XObjectDesigner.NoteSoundMessageDesigner>();

		var docs = converter.ReadScore();
		foreach (var doc in docs) {
			if (doc.type == XMaimaiNote.XDocument.Type.TAP) {
				var tap = (XMaimaiNote.XDocument.Document.Tap)doc;
				var kernel = new XMaimaiNote.XKernel.Kernel.Tap(uniqueId.ToString(), uniqueId);
				var inspector = new XMaimaiNote.XInspector.Inspector.Tap(kernel, tap.justTime, MaimaiJudgeEvaluateManager.MissTimeBank(XMaimaiNote.XInspector.InspectorType.TAP), tap.actionId); 
				var designer = new XMaimaiNote.XObjectDesigner.ObjectDesigner.Tap(noteObjMngCluster, notesCanvas, archCanvas, kernel, objectInfo, tap);
				var evtxdsgn = new XMaimaiNote.XEvaluateDesigner.EvaluateTextureDesigner(evaluateTextureCanvas, objectInfo, tap.button.GetId()); 
				var evefdsgn = new XMaimaiNote.XEvaluateDesigner.EvaluateEffectDesigner(evaluateEffectCanvas, objectInfo, tap.button.GetId()); 
				var answersound = new MaimaiAnswerSoundPlayer(tap.justTime, answerSoundType, inspector.GetNoteType(), uniqueId);

				kernel.Setup (inspector, designer, evtxdsgn, evefdsgn);

				inspectors.Add(inspector);
				designers.Add(designer);
				beforehandDesigners.AddRange (new IMaimaiObjectDesigner[] { designer, evtxdsgn, evefdsgn });
				answersoundPlayers.Add (answersound);
			
				if (tap.archId.HasValue) {
					XMaimaiNote.XObjectDesigner.EachArchDesigner arch;
					if (!archDesigners.ContainsKey(tap.archId.Value)) {
						arch = new XMaimaiNote.XObjectDesigner.EachArchDesigner(noteObjMngCluster, eachArchCanvas, objectInfo, tap.justTime, tap.guideFadeSpeed, tap.guideMoveSpeed);
						archDesigners[tap.archId.Value] = arch;
						beforehandDesigners.Add (arch);
					}
					arch = archDesigners[tap.archId.Value];
					arch.Setup (kernel, tap.button.GetId());
					kernel.eachArchDesigner = arch;
				}

				uniqueId++;
			}
			else if (doc.type == XMaimaiNote.XDocument.Type.HOLD) {
				var hold = (XMaimaiNote.XDocument.Document.Hold)doc;
				var kernel = new XMaimaiNote.XKernel.Kernel.Hold(uniqueId.ToString(), uniqueId);
				var headInspector = new XMaimaiNote.XInspector.Inspector.HoldHead(kernel, hold.headTime, MaimaiJudgeEvaluateManager.MissTimeBank(XMaimaiNote.XInspector.InspectorType.HOLD_HEAD), hold.headActionId); 
				var footInspector = new XMaimaiNote.XInspector.Inspector.HoldFoot(kernel, hold.footTime, MaimaiJudgeEvaluateManager.MissTimeBank(XMaimaiNote.XInspector.InspectorType.HOLD_FOOT), hold.footActionId); 
				var designer = new XMaimaiNote.XObjectDesigner.ObjectDesigner.Hold(noteObjMngCluster, notesCanvas, archCanvas, kernel, objectInfo, hold);
				var evtxdsgn = new XMaimaiNote.XEvaluateDesigner.EvaluateTextureDesigner(evaluateTextureCanvas, objectInfo, hold.button.GetId()); 
				var evefdsgn = new XMaimaiNote.XEvaluateDesigner.EvaluateEffectDesigner(evaluateEffectCanvas, objectInfo, hold.button.GetId());
				var keepingdsgn = new XMaimaiNote.XEvaluateDesigner.HoldKeepEffectDesigner(holdKeepEffectCanvas, objectInfo, hold.button.GetId());
				var hAnswersound = new MaimaiAnswerSoundPlayer(hold.headTime, answerSoundType, headInspector.GetNoteType(), uniqueId);
				var fAnswersound = new MaimaiAnswerSoundPlayer(hold.footTime, answerSoundType, footInspector.GetNoteType(), uniqueId);

				kernel.Setup (headInspector, footInspector, designer, evtxdsgn, evefdsgn, keepingdsgn);

				inspectors.Add(headInspector);
				inspectors.Add(footInspector);
				designers.Add(designer);
				beforehandDesigners.AddRange (new IMaimaiObjectDesigner[] { designer, evtxdsgn, evefdsgn, keepingdsgn });
				answersoundPlayers.AddRange (new MaimaiAnswerSoundPlayer[] { hAnswersound, fAnswersound });

				if (hold.archId.HasValue) {
					XMaimaiNote.XObjectDesigner.EachArchDesigner arch;
					if (!archDesigners.ContainsKey(hold.archId.Value)) {
						arch = new XMaimaiNote.XObjectDesigner.EachArchDesigner(noteObjMngCluster, eachArchCanvas, objectInfo, hold.headTime, hold.guideFadeSpeed, hold.guideMoveSpeed);
						archDesigners[hold.archId.Value] = arch;
						beforehandDesigners.Add (arch);
					}
					arch = archDesigners[hold.archId.Value];
					arch.Setup (kernel, hold.button.GetId());
					kernel.eachArchDesigner = arch;
				}
				
				uniqueId++;
			}
			else if (doc.type == XMaimaiNote.XDocument.Type.SLIDE) {
				var slide = (XMaimaiNote.XDocument.Document.Slide)doc;
				var kernel = new XMaimaiNote.XKernel.Kernel.Slide(uniqueId.ToString(), uniqueId);
				var slideInspectorsList = new List<XMaimaiNote.XInspector.Inspector.Slide[]>();
				var chainKernels = new List <XMaimaiNote.XKernel.Kernel.Slide.Chain>();
				foreach (var chain in slide.pattern.chains) {
					var chainKernel = new XMaimaiNote.XKernel.Kernel.Slide.Chain();
					var slideInspectors = new List<XMaimaiNote.XInspector.Inspector.Slide>();
					foreach (var section in chain.sections) {
						var slideInspector = new XMaimaiNote.XInspector.Inspector.Slide(chainKernel, slide.pattern.justTime, MaimaiJudgeEvaluateManager.MissTimeBank(XMaimaiNote.XInspector.InspectorType.SLIDE), section.actionId);
						// ここで入れる…？.
						slideInspector.judgementableTime = slide.pattern.visualizeTime - MaimaiJudgeEvaluateManager.TapFastGoodTime;
						slideInspectors.Add (slideInspector);
					}
					var aSlideInspectors = slideInspectors.ToArray();
					slideInspectorsList.Add (aSlideInspectors);
					chainKernel.Setup (kernel, aSlideInspectors);
					chainKernels.Add (chainKernel);
				}
				var designer = new XMaimaiNote.XObjectDesigner.ObjectDesigner.Slide(noteObjMngCluster, slideGuideCanvas, slideMarkerCanvas, kernel, objectInfo, slide);
				var exstxdsgn = new XMaimaiNote.XEvaluateDesigner.SlideEvaluateTextureDesigner(slideEvaluateTextureCanvas, objectInfo, slide.pattern.evaluateTexturePositions);
				var answersound = new MaimaiAnswerSoundPlayer(slide.pattern.moveStartTime, answerSoundType, XMaimaiNote.XInspector.InspectorType.SLIDE, uniqueId);

				var aChainKernels = chainKernels.ToArray();
				kernel.Setup (aChainKernels, designer, exstxdsgn);
				chainKernels.Clear();

				foreach (var asl in slideInspectorsList) {
					inspectors.AddRange(asl);
				}
				slideInspectorsList.Clear ();
				designers.Add(designer);
				beforehandDesigners.AddRange (new IMaimaiObjectDesigner[] { designer, exstxdsgn });
				answersoundPlayers.Add (answersound);
				
				uniqueId++;
			}
			else if (doc.type == XMaimaiNote.XDocument.Type.BREAK) {
				var tap = (XMaimaiNote.XDocument.Document.Break)doc;
				var kernel = new XMaimaiNote.XKernel.Kernel.Break(uniqueId.ToString(), uniqueId);
				var inspector = new XMaimaiNote.XInspector.Inspector.Break(kernel, tap.justTime, MaimaiJudgeEvaluateManager.MissTimeBank(XMaimaiNote.XInspector.InspectorType.BREAK), tap.actionId); 
				var designer = new XMaimaiNote.XObjectDesigner.ObjectDesigner.Break(noteObjMngCluster, notesCanvas, archCanvas, kernel, objectInfo, tap);
				var evtxdsgn = new XMaimaiNote.XEvaluateDesigner.EvaluateTextureDesigner(evaluateTextureCanvas, objectInfo, tap.button.GetId()); 
				var evefdsgn = new XMaimaiNote.XEvaluateDesigner.EvaluateEffectDesigner(evaluateEffectCanvas, objectInfo, tap.button.GetId()); 
				var answersound = new MaimaiAnswerSoundPlayer(tap.justTime, answerSoundType, inspector.GetNoteType(), uniqueId);

				kernel.Setup (inspector, designer, evtxdsgn, evefdsgn);

				inspectors.Add(inspector);
				designers.Add(designer);
				beforehandDesigners.AddRange (new IMaimaiObjectDesigner[] { designer, evtxdsgn, evefdsgn });
				answersoundPlayers.Add (answersound);
				
				if (tap.archId.HasValue) {
					XMaimaiNote.XObjectDesigner.EachArchDesigner arch;
					if (!archDesigners.ContainsKey(tap.archId.Value)) {
						arch = new XMaimaiNote.XObjectDesigner.EachArchDesigner(noteObjMngCluster, eachArchCanvas, objectInfo, tap.justTime, tap.guideFadeSpeed, tap.guideMoveSpeed);
						archDesigners[tap.archId.Value] = arch;
						beforehandDesigners.Add (arch);
					}
					arch = archDesigners[tap.archId.Value];
					arch.Setup (kernel, tap.button.GetId());
					kernel.eachArchDesigner = arch;
				}
				
				uniqueId++;
			}
			else if (doc.type == XMaimaiNote.XDocument.Type.MESSAGE) {
				var mes = (XMaimaiNote.XDocument.Document.Message)doc;
				var kernel = new XMaimaiNote.XKernel.Kernel.Message(uniqueId.ToString(), uniqueId);
				var inspector = new XMaimaiNote.XInspector.Inspector.Message(kernel, mes.viewEndTime, 0, Constants.instance.MAIMAI_NOTE_OTHER_ACTION_ID);
				var designer = new XMaimaiNote.XObjectDesigner.ObjectDesigner.Message(noteObjMngCluster, messageNotesCanvas, kernel, objectInfo, mes);
				kernel.Setup(inspector, designer);

				inspectors.Add(inspector);
				designers.Add(designer);
				beforehandDesigners.Add(designer);
				uniqueId++;
			}
			else if (doc.type == XMaimaiNote.XDocument.Type.SCROLL_MESSAGE) {
				var mes = (XMaimaiNote.XDocument.Document.ScrollMessage)doc;
				var kernel = new XMaimaiNote.XKernel.Kernel.ScrollMessage(uniqueId.ToString(), uniqueId);
				var inspector = new XMaimaiNote.XInspector.Inspector.ScrollMessage(kernel, mes.viewEndTime, 0, Constants.instance.MAIMAI_NOTE_OTHER_ACTION_ID);
				var designer = new XMaimaiNote.XObjectDesigner.ObjectDesigner.ScrollMessage(noteObjMngCluster, messageNotesCanvas, kernel, objectInfo, mes);
				kernel.Setup(inspector, designer);

				inspectors.Add(inspector);
				designers.Add(designer);
				beforehandDesigners.Add(designer);
				uniqueId++;
			}
			else if (doc.type == XMaimaiNote.XDocument.Type.SOUND_MESSAGE) {
				var mes = (XMaimaiNote.XDocument.Document.SoundMessage)doc;
				var designer = new XMaimaiNote.XObjectDesigner.NoteSoundMessageDesigner(objectInfo, uniqueId, mes);
				soundMessageNoteDesigners.Add(designer);
				uniqueId++;
			}
			else if (doc.type == XMaimaiNote.XDocument.Type.TRAP) {
				var trap = (XMaimaiNote.XDocument.Document.Trap)doc;
				var kernel = new XMaimaiNote.XKernel.Kernel.Trap(uniqueId.ToString(), uniqueId);
				var inspector = new XMaimaiNote.XInspector.Inspector.Trap(kernel, trap.headTime, trap.footTime, trap.actionId); 
				var designer = new XMaimaiNote.XObjectDesigner.ObjectDesigner.Trap(noteObjMngCluster, trapNotesCanvas, kernel, objectInfo, trap);
				var evtxdsgn = new XMaimaiNote.XEvaluateDesigner.EvaluateTextureDesigner(evaluateTextureCanvas, objectInfo, trap.sensor); 
				var evefdsgn = new XMaimaiNote.XEvaluateDesigner.EvaluateEffectDesigner(evaluateEffectCanvas, objectInfo, trap.sensor);
				var answersound = new MaimaiAnswerSoundPlayer(trap.footTime, answerSoundType, inspector.GetNoteType(), uniqueId);

				kernel.Setup (inspector, designer, evtxdsgn, evefdsgn);
				
				inspectors.Add(inspector);
				designers.Add(designer);
				beforehandDesigners.AddRange (new IMaimaiObjectDesigner[] { designer, evtxdsgn, evefdsgn });
				answersoundPlayers.Add (answersound);
				
				uniqueId++;
			}
		}
		var archDesignersList = new XMaimaiNote.XObjectDesigner.EachArchDesigner[archDesigners.Count];
		archDesigners.Values.CopyTo (archDesignersList, 0);
		foreach (var a in archDesignersList) a.FinalSetup ();

		noteInspectors = inspectors.ToArray ();
		
		var schedule = new List<IMaimaiSchedule> ();
		schedule.AddRange (designers.ToArray ());
		schedule.AddRange (archDesignersList);
		schedule.AddRange (soundMessageNoteDesigners.ToArray());
		designerScheduler = new MaimaiScheduler (schedule.ToArray());

		answerSoundScheduler = new MaimaiScheduler (answersoundPlayers.ToArray ());
		rhythmGameBeatCheckScheduler = new MaimaiScheduler (RhythmGameBeatCheckPlayer.CreateData (converter.first_bpm, 4, Constants.instance.START_COUNT_START_TIME));

		// 予めオブジェクトを作るなら.
		if (!realtimeCreateObject) {
			foreach (var beforehandDesigner in beforehandDesigners) {
				beforehandDesigner.Create ();
			}
		}
	}

	IEnumerator Start () {
#if DEBUG_AUTO_PLAY
		bool debugAutoPlay = true;
#else
		bool debugAutoPlay = false;
#endif
#if DEBUG_AUTO_PLAY_AND_ENDLESS
		bool debugAutoPlayAndEndless = true;
#else
		bool debugAutoPlayAndEndless = false;
#endif

		initialized = false;
		uiCtrl = GetComponent<RhythmGameSceneUIController> ();
		uiCtrl.loadingScreen.SetActive (true);
		gameoverCtrl.Hide();

		while (UserDataController.instance == null) {
			yield return null;
		}

		// シンクモードでホストなら相手をリザルトゲームシーンにいざなう.
		if (MiscInformationController.instance.syncMode == 1) {
			SyncLobbyNetworkStateController.instance.networkView.RPC ("NotificateHostRhythmGameSceneMoved", RPCMode.OthersBuffered);
			while (!SyncLobbyNetworkStateController.instance.clientRhythmGameSceneMoved) {
				yield return null;
			}
		}
		else if (MiscInformationController.instance.syncMode == 2) {
			SyncLobbyNetworkStateController.instance.networkView.RPC ("NotificateClientRhythmGameSceneMoved", RPCMode.Server);
			while (!SyncLobbyNetworkStateController.instance.hostRhythmGameWait) {
				yield return null;
			}
		}

		// コンフィグを反映.
		SetGameConfig ();
		// センサーを生成.
		CreateSensors ();
		// タイマーのインスタンスを生成.
		timer = new MaimaiTimer(AudioManagerLite.GetManager());
		objectInfo = new XMaimaiNote.XObjectDesigner.CommonInformation(timer, judgeTimeLag, pine, angulatedHold, autoPlay, realtimeCreateObject, visualizeFastLate, prefabManager);
		// エディタからの起動ならエディタのインデックスからスタート位置とリピート位置を取得.
		int[] edtiorOption = null;
		if (MiscInformationController.instance.rhythmGameRequesterSceneName == "editor") {
			if (EditorInformationController.instance.multiSelectableMode) {
				edtiorOption = new int[] {
					EditorInformationController.instance.selectionStartIndex,
					EditorInformationController.instance.selectionEndIndex,
				};
			}
			else {
				edtiorOption = new int[] {
					EditorInformationController.instance.selectionStartIndex,
				};
			}
		}
		// 楽譜を読み込む.
		MaimaiScoreConverter converter = null;
		System.Action<RhythmGameLibrary.Score.Order[]> createConv = (score) => {
			converter = new MaimaiScoreConverter (score, mirror, turn, tapToBreak, breakToTap, ringToStar, starToRing, guideFadeSpeed, guideMoveSpeed, ringScale, markerScale, starRotation, trapScale, edtiorOption);
		};
		if (TrackInformationController.instance.calledSetup) {
			createConv(TrackInformationController.instance.score);
			NotePrepare(converter);
			if (TrackInformationController.instance.audioClip != null) {
				AudioManagerLite.Load(TrackInformationController.instance.audioClip);
				MiscInformationController.instance.lastLoadedBgmKey = "track_audio";
				AudioManagerLite.SetVolume ();
			}
			else {
				AudioManagerLite.Pause ();
				AudioManagerLite.Load (null);
				MiscInformationController.instance.lastLoadedBgmKey = "track_audio";
			}
			if (TrackInformationController.instance.jacket != null) {
				uiCtrl.backgroundImage.sprite = TrackInformationController.instance.jacket;
			}
			else {
				// ジャケットが無ければデフォ画像を表示.
				uiCtrl.backgroundImage.sprite = SpriteTank.Get ("bg_default");
			}
			while (MaimaiStyleDesigner.GetGuideCircleSprite(TrackInformationController.instance.difficulty) == null) {
				yield return null;
			}
			uiCtrl.guideCircle.sprite = MaimaiStyleDesigner.GetGuideCircleSprite(TrackInformationController.instance.difficulty);
		}
		else {
			uiCtrl.backgroundImage.sprite = SpriteTank.Get ("bg_default");
			yield return StartCoroutine (TestScoreCreator.LoadTestRawSimaiFile ((score)=>{
				createConv(score);
				NotePrepare(converter);
			}));
			while (MaipadDynamicCreatedSpriteTank.instance.guideCircleDef == null) {
				yield return null;
			}
			uiCtrl.guideCircle.sprite = MaipadDynamicCreatedSpriteTank.instance.guideCircleDef;
			if (debugAutoPlay || debugAutoPlayAndEndless) {
				autoPlay = true;
			}
			else {
				autoPlay = false;
			}
		}

		// エディタからの起動ならエディタのインデックスからスタート位置とリピート位置を設定など.
		if (MiscInformationController.instance.rhythmGameRequesterSceneName == "editor") {
			var results = converter.GetResultsOfGetTimeMSRequestScoreIndexes();
			if (results[0].HasValue) {
				if (EditorInformationController.instance.selectionStartIndex == 0) {
					// インデックスが0ならホントに初めから再生させる.
					restartTime = 0;
				}
				else {
					// ある程度前から再生させたいのでスタート位置からSTART_COUNT_START_TIMEだけ前にずらす.
					restartTime = results[0].Value - Constants.instance.START_COUNT_START_TIME;
					if (restartTime < 0) {
						restartTime = 0;
					}
				}
			}
			else {
				restartTime = 0;
			}
			if (edtiorOption.Length < 2 || !results[1].HasValue || !results[0].HasValue) {
				repeatTime = 0;
			}
			else {
				repeatTime = results[1].Value + Constants.instance.START_COUNT_START_TIME - results[0].Value; // end - startで区間を取得.
				if (repeatTime < 0) {
					repeatTime = 0;
				}
			}
			autoPlay = true;
			challengeLife = ConfigTypes.LifeDifficulty.INFINITY;
		}
		else if (MiscInformationController.instance.rhythmGameRequesterSceneName == "tutorial") {
			// チュートリアルのときオートプレーにしないなど.
			autoPlay = false;
			bgInfoType = ConfigTypes.BackGroundInfomation.OFF;
			challengeLife = ConfigTypes.LifeDifficulty.INFINITY;
		}
		else if (MiscInformationController.instance.syncMode > 0) {
			// シンクモードのときオートプレーにしないなど.
			autoPlay = false;
			challengeLife = ConfigTypes.LifeDifficulty.INFINITY;
		}
		// 評価機を生成.
		evaluater = new MaimaiJudgeEvaluateManager(timer, noteInspectors, judgeTimeLag, soundEffectType, answerSoundType, judgementType, challengeLife, visualizeFastLate, allowSkipSlide);
		// BgInfoの設定.
		bgInfoCtrl = GetComponent<BgInfoController>();
		bgInfoCtrl.Setup (evaluater, bgInfoType, challengeLife);
		
		bool isSyncMode = MiscInformationController.instance.syncMode > 0;
		// サーバーならシンク評価機を作る.
		if (MiscInformationController.instance.syncMode == 1) {
			var syncEvaluaterObj = Network.Instantiate (syncEvaluaterPrefab, Vector3.zero, Quaternion.Euler(Vector3.zero), 0) as GameObject;
			syncEvaluater = syncEvaluaterObj.GetComponent<MaimaiSyncEvaluateManager>();
			syncEvaluater.networkView.RPC ("Setup", RPCMode.AllBuffered, maxMultiPlayers, syncNoteCount);
			evaluater.syncEvaluater = syncEvaluater.networkView;
		}
		// クライアントならサーバーが作ったシンク評価機を見つけて参照する.
		else if (MiscInformationController.instance.syncMode == 2) {
			GameObject syncEvaluaterObj = null;
			while ((syncEvaluaterObj = GameObject.Find (syncEvaluaterPrefab.name + "(Clone)")) == null) {
				yield return null;
			}
			syncEvaluater = syncEvaluaterObj.GetComponent<MaimaiSyncEvaluateManager>();
			evaluater.syncEvaluater = syncEvaluater.networkView;
		}
		// 時間の設定.
		var _converterTime = converter.GetFinishTimeMS ();
		finishTime = _converterTime + 3000L;
		if (edtiorOption == null) {
			restartTime = (_converterTime * repeatStartTimeRate).ToLong ();
		}
		// simaiのfirstマクロがあるならseekとwaitにそれを反映させる.
		TrackInformationController.instance.SetSimaiFirstData (converter.first_bpm, Constants.instance.START_COUNT_START_TIME);
		// UIコントローラーへの情報伝達.
		uiCtrl.isAutoPlay = autoPlay;
		uiCtrl.achievement = GetAchievement ();
		uiCtrl.judgementType = judgementType;
		uiCtrl.rankVisionVersion = rankVisionVersion;
		uiCtrl.isSyncMode = isSyncMode;
		if (isSyncMode) {
			uiCtrl.syncPoint = syncEvaluater.syncPercent;
			syncEvaluater.onCountChanged = (count) => {
				uiCtrl.syncPoint = syncEvaluater.syncPercent;
			};
		}

		// 情報リセット.
		oldPauseState = false;
		gameEndProcCalled = false;

		// ポーズボタンの挙動設定 (オートプレーなら即時ポーズ、そうでないなら長押しでポーズ)
		uiCtrl.pauseButtonAnimation.fillAmount = 0;
		if (autoPlay) {
			uiCtrl.pauseButton.onClick.AddListener (OnPauseButtonClick);
		}
		else {
			var trigger = uiCtrl.pauseButton.GetComponent<UnityEngine.EventSystems.EventTrigger>();
			trigger.delegates = new List<UnityEngine.EventSystems.EventTrigger.Entry>();
			
			var entry = new UnityEngine.EventSystems.EventTrigger.Entry();
			entry.eventID = UnityEngine.EventSystems.EventTriggerType.PointerDown;
			entry.callback.AddListener( (x) => { uiCtrl.OnPauseButtonDown(); });
			trigger.delegates.Add(entry);
			
			entry = new UnityEngine.EventSystems.EventTrigger.Entry();
			entry.eventID = UnityEngine.EventSystems.EventTriggerType.PointerUp;
			entry.callback.AddListener( (x) => { uiCtrl.OnPauseButtonUp(); });
			trigger.delegates.Add(entry);
		}

		// シンクモードでホストなら相手を待つ.
		if (MiscInformationController.instance.syncMode == 1) {
			SyncLobbyNetworkStateController.instance.networkView.RPC ("NotificateHostRhythmGameWait", RPCMode.OthersBuffered);
			while (!SyncLobbyNetworkStateController.instance.clientRhythmGameWait) {
				yield return null;
			}
		}
		else if (MiscInformationController.instance.syncMode == 2) {
			SyncLobbyNetworkStateController.instance.networkView.RPC ("NotificateClientRhythmGameWait", RPCMode.Server);
			yield return null;
		}

		// タイマーを初期化する.
		if (TrackInformationController.instance.calledSetup) {
			if (!autoPlay) {
				timer.initialize (TrackInformationController.instance.seek, TrackInformationController.instance.wait, finishTime);
			}
			else {
				Restart ();
			}
		}
		else {
			if (!debugAutoPlay || debugAutoPlayAndEndless) {
				Restart();
				timer.initialize_ModeEndless (restartTime);
			}
			else {
				Restart ();
			}
		}
		
		objectInfo.autoPlay = autoPlay;

		while (!uiCtrl.initialize) {
			yield return null;
		}
		initialized = true;
		uiCtrl.loadingScreen.SetActive (false);
	}

	void Update () {
		if (!initialized) return;
		
		if ((!timer.finish || timer.restarted) && !gameEndProcCalled) {
			timer.timeUpdate ();

			if (!timer.pause) {
				if (!autoPlay) {
					// 操作系の処理.
					foreach (var sensor in sensors) {
						sensor.Check(oldPauseState, true);
					}
					var outerSensors = GetSensor(Constants.Circumference.OUTER);
					for (int i = 0; i < outerSensors.Length; i++) {
						var sensor = outerSensors[i];
						if (sensor.push) {
							sensorEffectMother.Birth(i);
							evaluater.JudgeNote(Constants.instance.MAIMAI_NOTE_ACTION_ID_BASE + i);
						}
						if (sensor.pull && !sensor.hold) {
							evaluater.JudgeNote(Constants.instance.MAIMAI_NOTE_HOLD_FOOT_ACTION_ID_BASE + i);
						}
						if (sensor.hold) {
							evaluater.JudgementableNotes(Constants.instance.MAIMAI_NOTE_SLIDE_PATTERN_ACTION_ID_OUTER_BASE + i);
						}
					}
					var innerSensors = GetSensor(Constants.Circumference.INNER);
					for (int i = 0; i < innerSensors.Length; i++) {
						var sensor = innerSensors[i];
						if (sensor.hold) {
							evaluater.JudgementableNotes(Constants.instance.MAIMAI_NOTE_SLIDE_PATTERN_ACTION_ID_INNER_BASE + i);
						}
					}
					var centerSensors = GetSensor(Constants.Circumference.CENTER);
					for (int i = 0; i < centerSensors.Length; i++) {
						var sensor = centerSensors[i];
						if (sensor.hold) {
							evaluater.JudgementableNotes(Constants.instance.MAIMAI_NOTE_SLIDE_PATTERN_ACTION_ID_CENTER);
						}
					}
				}
				else if (repeatTime > 0) {
					// リピート再生系の処理.
					if (timer.gameTime > restartTime + repeatTime) {
						Restart ();
					}
				}
				
				if (!timer.restarted) {
					answerSoundScheduler.Run (timer.gameTime);
					rhythmGameBeatCheckScheduler.Run (timer.gameTime);
					designerScheduler.Run (timer.gameTime);
					evaluater.AutoGuideProc();
					if (autoPlay) {
						evaluater.AutoJustProc();
					}
					evaluater.AutoMissProc();
					evaluater.NextUpdatePrepare();
				}
				
				uiCtrl.achievement = GetAchievement ();
				if (syncEvaluater != null) {
					uiCtrl.syncPoint = syncEvaluater.syncPercent;
				}

				if (evaluater.IsDie()) {
					Utility.SoundEffectManager.Play ("se_gameover");
					// ゲームオーバーならその画面を表示.
					gameoverCtrl.Show();
					sensorCanvas.gameObject.SetActive(false);
					timer.pause = true;
					gameEndProcCalled = true;
				}
				oldPauseState = false;
			}
			else {
				oldPauseState = true;
			}
		}
		else {
			if (autoPlay && repeatTime > 0) {
				// リピート再生系の処理.
				Restart ();
			}
			else {
				// リピートが無効ならゲーム終了.
				if (!gameEndProcCalled) {
					StartCoroutine (OnGameEnd());
					gameEndProcCalled = true;
				}
			}
		}
		
		// ポーズ状態管理.
		if (!autoPlay) {
			uiCtrl.pauseButtonAnimation.fillAmount = pauseTimer;
			if (!timer.pause) {
				if (uiCtrl.pauseButtonHolding) {
					if (pauseTimer < pauseTime) {
						pauseTimer += Time.deltaTime;
					}
					else {
						Utility.SoundEffectManager.Play ("se_decide");
						PauseGame ();
					}
				}
				else {
					if (pauseTimer > 0f) {
						pauseTimer -= Time.deltaTime;
					}
					else {
						pauseTimer = 0f;
					}
				}
			}
		}
	}
	
	private void Restart () {
		objectInfo.restartId++;
		timer.initialize (TrackInformationController.instance.seek, TrackInformationController.instance.wait, finishTime, restartTime);
		evaluater.ResetEvaluate(restartTime);
		rhythmGameBeatCheckScheduler.Cueing(restartTime);
		answerSoundScheduler.Cueing (restartTime);
		designerScheduler.Cueing (restartTime);
		bgInfoCtrl.SetBgInfoVisualizeTargetInit ();
		bgInfoCtrl.fullComboAnimationIsDone = false;
	}

	protected IEnumerator OnGameEnd () {
		if (MiscInformationController.instance.rhythmGameRequesterSceneName == "editor") {
			TrackInformationController.instance.SavingSeekWait ();
			Application.LoadLevel ("SceneScoreEditor");
		}
		else if (MiscInformationController.instance.rhythmGameRequesterSceneName == "tutorial") {
			Application.LoadLevel ("SceneMainMenu");
		}
		else {
			// シンクモードなら相手を待つ.
			if (MiscInformationController.instance.syncMode == 1) {
				SyncLobbyNetworkStateController.instance.networkView.RPC ("NotificateHostRhythmGameEndWait", RPCMode.OthersBuffered);
				while (!SyncLobbyNetworkStateController.instance.clientRhythmGameEndWait) {
					yield return null;
				}
			}
			else if (MiscInformationController.instance.syncMode == 2) {
				SyncLobbyNetworkStateController.instance.networkView.RPC ("NotificateClientRhythmGameEndWait", RPCMode.Server);
				while (!SyncLobbyNetworkStateController.instance.hostRhythmGameEndWait) {
					yield return null;
				}
			}
			
			int syncPoint = 0;
			if (syncEvaluater != null) {
				syncPoint = syncEvaluater.syncPercent.ToPercent2().ToInt();
			}
			ResultInformationController.instance.Setup(
				evaluater.GetScore(), evaluater.GetAchievement(),
				evaluater.IsAllPerfect(), evaluater.IsFullCombo(), evaluater.IsNoMiss(),
				syncPoint, evaluater.theoreticalScore,
				evaluater.GetTapScore(), evaluater.GetHoldScore(), evaluater.GetSlideScore(), evaluater.GetBreakScore(), evaluater.GetTrapScore(),
				evaluater.GetFullTapScore(), evaluater.GetFullHoldScore(), evaluater.GetFullSlideScore(), evaluater.GetBFullBreakScore(), evaluater.GetFullTrapScore(),
				evaluater.GetPerfectCount(), evaluater.GetGreatCount(), evaluater.GetGoodCount(), evaluater.GetMissCount(), evaluater.maxCombo,
				autoPlay, changeBreakType, syncEvaluater != null);
			if (!autoPlay) {
				SetUserExperience ();
				SetAvaterAbility ();
				UserDataController.instance.SaveUserInfo ();
			}
			TrackInformationController.instance.SavingSeekWait ();
			SavingRepeatData ();
			Application.LoadLevel ("SceneResult");
		}
	}
	
	protected virtual void SetGameConfig () {
		var c = UserDataController.instance.config;
		// ノートオブジェクト用コンフィグ.
		autoPlay = c.auto_play;
		guideMoveSpeed = c.move_guide_speed;
		guideFadeSpeed = c.fade_guide_speed;
		judgeTimeLag = (c.judge_offset * 1000.0f).ToLong();
		starRotation = c.star_rotation;
		angulatedHold = c.angulated_hold;
		pine = c.pine;
		ringScale = c.ring_size;
		markerScale = c.marker_size;
		trapScale = Constants.instance.SENSOR_RADIUS;
		// 楽譜用コンフィグ.
		turn = c.turn;
		mirror = c.mirror;
		changeBreakType = c.change_break_type.ToChangeBreakType();
		tapToBreak = changeBreakType == ConfigTypes.ChangeBreak.TAP_TO_BREAK || changeBreakType == ConfigTypes.ChangeBreak.TAP_EXCHANGE_BREAK;
		breakToTap = changeBreakType == ConfigTypes.ChangeBreak.BREAK_TO_TAP || changeBreakType == ConfigTypes.ChangeBreak.TAP_EXCHANGE_BREAK;
		changeRingType = c.change_ring_type.ToChangeRingType();
		ringToStar = changeRingType == ConfigTypes.ChangeRing.RING_TO_STAR || changeRingType == ConfigTypes.ChangeRing.RING_EXCHANGE_STAR;
		starToRing = changeRingType == ConfigTypes.ChangeRing.STAR_TO_RING || changeRingType == ConfigTypes.ChangeRing.RING_EXCHANGE_STAR;
		// 評価機用コンフィグ.
		startBeatCountSoundEnabled = true;
		soundEffectType = c.sound_effect_type.ToSoundEffectType ();
		answerSoundType = c.answer_sound_type.ToAnswerSoundType ();
		judgementType = c.judgement_type.ToJudgementType ();
		visualizeFastLate = c.visualize_fast_late;
		allowSkipSlide = c.skip_slide;
		challengeLife = c.challenge_life.ToLifeDifficultyType();
		// 表示用コンフィグ.
		bgInfoType = c.background_infomation_type.ToBackGroundInfomationType ();
		achievementVisionType = c.up_screen_achievement_vision_type.ToUpScreenAchievementVisionType ();
		rankVisionVersion = c.rank_version_type.ToRankVersionType ();
		realtimeCreateObject = true;//c.realtime_create_object;
		// リピート用データ.
		repeatStartTimeRate = UserDataController.instance.user.repeat_start_time_rate;
		repeatTime = (UserDataController.instance.user.repeat_time * 1000.0f).ToLong();
	}

	private void CreateSensors() {
		var sensorParent = sensorCanvas;
		
		sensors = new GameVirtualHardwareButton[17];
		for (int i = 0; i < 8; i++) {
			var sensorObj = Instantiate(sensorPrefab) as GameObject;
			sensorObj.name = "Sensor Outer " + (i + 1).ToString();
			sensorObj.transform.SetParentEx (sensorParent);
			var sensor = sensorObj.GetComponent<MaimaiSensor>();
			sensor.Setup(new Vector2(0, Constants.instance.MAIMAI_OUTER_RADIUS), Constants.instance.GetPieceDegree(i));
			sensor.SetupButtonNames ("MaiOuterSensor" + (i + 1).ToString ());
			sensors[i] = sensor;
		}
		for (int i = 0; i < 8; i++) {
			var sensorObj = Instantiate(sensorPrefab) as GameObject;
			sensorObj.name = "Sensor Inner " + (i + 1).ToString();
			sensorObj.transform.SetParentEx (sensorParent);
			var sensor = sensorObj.GetComponent<MaimaiSensor>();
			sensor.Setup(new Vector2(0, Constants.instance.MAIMAI_INNER_RADIUS), Constants.instance.GetPieceDegree(i));
			sensor.SetupButtonNames ("MaiInnerSensor" + (i + 1).ToString ());
			sensors[i + 8] = sensor;
		}
		{
			int i = 16;
			var sensorObj = Instantiate(sensorPrefab) as GameObject;
			sensorObj.name = "Sensor Center";
			sensorObj.transform.SetParentEx (sensorParent.transform);
			var sensor = sensorObj.GetComponent<MaimaiSensor>();
			sensor.Setup(Vector2.zero, 0);
			sensor.SetupButtonNames ("MaiCenterSensor");
			sensors[i] = sensor;
		}
	}

	private GameVirtualHardwareButton[] GetSensor(Constants.Circumference place) {
		if (place == Constants.Circumference.OUTER) {
			return new GameVirtualHardwareButton[] {
				sensors[0], sensors[1], sensors[2], sensors[3], sensors[4], sensors[5], sensors[6], sensors[7], 
			};
		}
		else if (place == Constants.Circumference.INNER) {
			return new GameVirtualHardwareButton[] {
				sensors[8], sensors[9], sensors[10], sensors[11], sensors[12], sensors[13], sensors[14], sensors[15], 
			};
		}
		else {
			return new GameVirtualHardwareButton[] {
				sensors[16],
			};
		}
	}
	
	protected float GetAchievement() {
		if (uiCtrl == null || evaluater == null)
			return 0;
		var type = achievementVisionType;
		switch (type) {
		case ConfigTypes.UpScreenAchievementVision.NORMAL:
			return evaluater.GetAchievement();
		case ConfigTypes.UpScreenAchievementVision.PACE:
			return evaluater.GetPaceAchievement();
		case ConfigTypes.UpScreenAchievementVision.HAZARD_BREAKPACE:
			return evaluater.GetHazardBreakPaceAchievement();
		case ConfigTypes.UpScreenAchievementVision.HAZARD:
			return evaluater.GetHazardAchievement();
		case ConfigTypes.UpScreenAchievementVision.T_NORMAL:
			return evaluater.GetTheoreticalAchievement();
		case ConfigTypes.UpScreenAchievementVision.T_PACE:
			return evaluater.GetTheoreticalPaceAchievement();
		case ConfigTypes.UpScreenAchievementVision.T_HAZARD:
			return evaluater.GetTheoreticalHazardAchievement();
		}
		return 0;
	}

	public void SetUserExperience () {
		float syncRate = syncEvaluater != null ? syncEvaluater.syncPercent : 0.0f;
		UserDataController.instance.SetUserExperience (evaluater.GetAchievement (), evaluater.GetNotesCountTotal (), evaluater.GetMissCount (), evaluater.IsAllPerfect (), evaluater.IsFullCombo(), syncRate);
		ProfileInformationController.instance.PlayerLevelReload ();
	}
	
	public void SetAvaterAbility () {
		UserDataController.instance.SetAvaterAbility (
			evaluater.GetTapCount(), evaluater.GetTapScore(), evaluater.GetFullTapScore(),
			evaluater.GetHoldCount(), evaluater.GetHoldScore(), evaluater.GetFullHoldScore(),
			evaluater.GetSlideCount(), evaluater.GetSlideScore(), evaluater.GetFullSlideScore(),
			evaluater.GetBreakCount(), evaluater.GetBreakScore(), evaluater.GetFullBreakScore());
		ProfileInformationController.instance.PlayerAvaterAbilityReload ();
	}
	
	public virtual void OnPauseButtonClick () {
		Utility.SoundEffectManager.Play ("se_decide");
		PauseGame ();
	}
	
	public virtual void OnResumeButtonClick () {
		Utility.SoundEffectManager.Play ("se_decide");
		ResumeGame ();
	}
	
	public virtual void OnRetryButtonClick() {
		Utility.SoundEffectManager.Play ("se_decide");
		RetryGame ();
	}
	
	public virtual void OnExitButtonClick() {
		Utility.SoundEffectManager.Play ("se_decide");
		ExitGame ();
	}
	
	public void OnSeekWaitSettingButtonClick () {
		Utility.SoundEffectManager.Play ("se_decide");
		uiCtrl.seekInputLabel.text = ((float)TrackInformationController.instance.seek / 1000.0f).ToString ();
		uiCtrl.waitInputLabel.text = ((float)TrackInformationController.instance.wait / 1000.0f).ToString ();
		uiCtrl.seekWaitSettingDialog.SetActive (true);
	}
	
	public void OnSeekWaitSettingOKButtonClick () {
		Utility.SoundEffectManager.Play ("se_decide");
		TrackInformationController.instance.SetSeekWait (uiCtrl.seekInputLabel.text, uiCtrl.waitInputLabel.text);
		uiCtrl.seekWaitSettingDialog.SetActive (false);
		RetryGame ();
	}
	
	public void OnSeekWaitSettingCancelButtonClick () {
		Utility.SoundEffectManager.Play ("se_decide");
		uiCtrl.seekWaitSettingDialog.SetActive (false);
	}
	
	public void OnRepeatSettingButtonClick () {
		Utility.SoundEffectManager.Play ("se_decide");
		uiCtrl.repeatStartSlider.value = repeatStartTimeRate;
		uiCtrl.repeatInputLabel.text = ((float)repeatTime / 1000.0f).ToString ();
		uiCtrl.repeatSettingDialog.SetActive (true);
	}
	
	public void OnRepeatSettingOKButtonClick () {
		Utility.SoundEffectManager.Play ("se_decide");
		repeatStartTimeRate = uiCtrl.repeatStartSlider.value;
		float time;
		if (string.IsNullOrEmpty (uiCtrl.repeatInputLabel.text)) {
			repeatTime = 0;
		}
		else if (float.TryParse (uiCtrl.repeatInputLabel.text, out time)) {
			if (time < 0) {
				repeatTime = 0;
			}
			else {
				repeatTime = (time * 1000.0f).ToLong();
			}
		}
		UserDataController.instance.user.repeat_start_time_rate = repeatStartTimeRate;
		UserDataController.instance.user.repeat_time = (float)repeatTime / 1000.0f;
		MiscInformationController.instance.repeatOptionChanged = true;
		uiCtrl.repeatSettingDialog.SetActive (false);
		if (autoPlay) {
			RetryGame ();
		}
	}
	
	public void OnRepeatSettingCancelButtonClick () {
		Utility.SoundEffectManager.Play ("se_decide");
		uiCtrl.repeatSettingDialog.SetActive (false);
	}
	
	private void SavingRepeatData () {
		if (MiscInformationController.instance.repeatOptionChanged) {
			UserDataController.instance.user.repeat_start_time_rate = repeatStartTimeRate;
			UserDataController.instance.user.repeat_time = (float)repeatTime / 1000.0f;
			UserDataController.instance.SaveUserInfo ();
			MiscInformationController.instance.repeatOptionChanged = false;
		}
	}
	
	
	// アプリケーションを中断したらタイマーをポーズする.
	protected virtual void OnApplicationPause(bool pause) {
		if (pause == true) {
			if (MiscInformationController.instance.syncMode > 0) {
				timer.pause = true;
				Network.Disconnect ();
			}
			else {
				uiCtrl.pauseButtonHolding = false;
				PauseGame ();
				TrackInformationController.instance.SavingSeekWait ();
				SavingRepeatData ();
			}
		}
	}
	
	// サーバーから切断されたらポーズする.
	protected virtual void OnDisconnectedFromServer () {
		timer.pause = true;
	}
	
	// クライアントが切断されたらポーズする.
	protected virtual void OnPlayerDisconnected () {
		timer.pause = true;
	}
	
	private void PauseGame () {
		timer.pause = true;
		if (!autoPlay) {
			pauseTimer = 1.0f;
		}
		uiCtrl.pauseDialog.SetActive (true);
	}
	
	private void ResumeGame () {
		timer.pause = false;
		if (!autoPlay) {
			pauseTimer = 0.0f;
		}
		uiCtrl.pauseDialog.SetActive (false);
	}
	
	private void RetryGame () {
		uiCtrl.pauseDialog.SetActive (false);
		if (!autoPlay && MiscInformationController.instance.rhythmGameRequesterSceneName != "editor" && MiscInformationController.instance.rhythmGameRequesterSceneName == "tutorial") {
			SetAvaterAbility ();
		}
		Application.LoadLevel ("SceneRhythmGame");
	}
	
	private void ExitGame () {
		uiCtrl.pauseDialog.SetActive (false);
		if (!autoPlay && MiscInformationController.instance.rhythmGameRequesterSceneName != "editor" && MiscInformationController.instance.rhythmGameRequesterSceneName != "tutorial") {
			SetAvaterAbility ();
			UserDataController.instance.SaveUserInfo ();
		}
		if (MiscInformationController.instance.rhythmGameRequesterSceneName != "tutorial") {
			TrackInformationController.instance.SavingSeekWait ();
		}
		if (MiscInformationController.instance.rhythmGameRequesterSceneName != "editor" && MiscInformationController.instance.rhythmGameRequesterSceneName != "tutorial") {
			SavingRepeatData ();
		}
		BackSceneLoad ();
	}
	
	protected virtual void BackSceneLoad() {
		if (MiscInformationController.instance.rhythmGameRequesterSceneName == "editor") {
			Application.LoadLevel ("SceneScoreEditor");
		}
		else if (MiscInformationController.instance.rhythmGameRequesterSceneName == "tutorial") {
			Application.LoadLevel ("SceneMainMenu");
		}
		else {
			MiscInformationController.instance.selectMusicVoiceCallRequest = true;
			Application.LoadLevel ("SceneSelectTrack");
		}
	}
}
