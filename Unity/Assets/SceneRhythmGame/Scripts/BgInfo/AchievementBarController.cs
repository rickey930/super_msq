﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AchievementBarController : MonoBehaviour {
	
	private Material copyMaterial;
	
	[SerializeField]
	private AchievementBarMeshRenderer abRenderer;
	[SerializeField]
	private MeshFilter filter;
	[SerializeField]
	private Image rankTextImage;
	[SerializeField]
	private Text syncPointText;
	[SerializeField]
	private Material achievementBarMaterial;
	[SerializeField]
	private Material syncBarMaterial;

	public bool initialized { get; private set; }
	public bool? isSyncMode { private get; set; }
	
	IEnumerator Start () {
		initialized = false;
		if (abRenderer == null) {
			abRenderer = GetComponent<AchievementBarMeshRenderer>();
		}
		if (filter == null) {
			filter = GetComponent<MeshFilter>();
		}
		while (!isSyncMode.HasValue) {
			yield return null;
		}

		if (!isSyncMode.Value) {
			this.copyMaterial = new Material(achievementBarMaterial);
			rankTextImage.gameObject.SetActive(true);
			syncPointText.gameObject.SetActive(false);
		}
		else {
			this.copyMaterial = new Material(syncBarMaterial);
			syncPointText.gameObject.SetActive(true);
			rankTextImage.gameObject.SetActive(false);
		}
		abRenderer.material = copyMaterial;
		abRenderer.barMesh = filter.mesh;

		// 色を更新しないと何故か表示されないっぽい.
		abRenderer.color = Color.black;
		abRenderer.color = Color.white;

		initialized = true;
	}

	public void SetAchievementValue(ConfigTypes.RankVersion rankVisionVersion, float achievementValue) {
		abRenderer.material.SetFloat("_Width", achievementValue);
		float pvalue = achievementValue.ToPercent2();
		rankTextImage.sprite = rankVisionVersion == ConfigTypes.RankVersion.PINK ? MaimaiStyleDesigner.GetRankSprite (pvalue) :
			rankVisionVersion == ConfigTypes.RankVersion.CLASSIC ? MaimaiStyleDesigner.GetRankSpriteClassic (pvalue) : null;
		rankTextImage.color = MaimaiStyleDesigner.GetAchievementColor(pvalue);
	}
	
	public void SetSyncPointValue(float syncPercentValue) {
		abRenderer.material.SetFloat("_Width", syncPercentValue);
		syncPointText.text = syncPercentValue.ToPercent2().ToInt().ToString() + "%";
	}
	
	void OnDestroy () {
		if (copyMaterial != null) {
			GameObject.Destroy (copyMaterial);
		}
	}
}
