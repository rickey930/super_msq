﻿using UnityEngine;
using System.Collections;

public class BgInfoController : MonoBehaviour {
	
	[SerializeField]
	private BgInfoNumberImageController numberCtrl;
	[SerializeField]
	private BgInfoAnimationController bgInfoNameSpriteCtrl;
	[SerializeField]
	private BgInfoAnimationController fullComboAnimationCtrl;
	[SerializeField]
	private BgInfoAnimationController allPerfectAnimationCtrl;
	[SerializeField]
	private BgInfoAnimationController hundredPercentSynchronizeAnimationCtrl;
	[SerializeField]
	private UnityEngine.UI.Image lifeBackImage;

	private float bgInfoVisualizeTarget; // コンボなら2コンボ目と、10コンボに1回のペースで表示するための記録用.
	private float bgInfoValue;
	private float bgInfoOldValue;
	private float bgInfoFirstTarget;
	private const float bgInfoStepValue = 10.0f;
	public bool fullComboAnimationIsDone { private get; set; }
	// 表示用コンフィグ.
	protected ConfigTypes.BackGroundInfomation bgInfoType { get; set; }
	protected ConfigTypes.LifeDifficulty challengeLife { get; set; }

	private bool initialize;
	private MaimaiJudgeEvaluateManager evaluater;

	public void Setup(MaimaiJudgeEvaluateManager evaluater, ConfigTypes.BackGroundInfomation bgInfoType, ConfigTypes.LifeDifficulty challengeLife) {
		this.evaluater = evaluater;
		this.bgInfoType = bgInfoType;
		this.challengeLife = challengeLife;
	}

	// Use this for initialization
	IEnumerator Start () {
		initialize = false;
		while (evaluater == null) {
			yield return null;
		}

		if (challengeLife != ConfigTypes.LifeDifficulty.INFINITY) {
			numberCtrl.Setup (0, false, false, MaimaiStyleDesigner.GetRemainLifeColor(challengeLife, evaluater.life.Value));
			bgInfoNameSpriteCtrl.gameObject.SetActive (false);
			numberCtrl.value = evaluater.life.Value;
			numberCtrl.SetActive (!fullComboAnimationIsDone);
			lifeBackImage.gameObject.SetActive(true);
			lifeBackImage.color = MaimaiStyleDesigner.GetLifeDifficultyColor(challengeLife);
		}
		else if (bgInfoType == ConfigTypes.BackGroundInfomation.COMBO) {
			numberCtrl.Setup (0, false, true, MaimaiStyleDesigner.GetComboColor());
			bgInfoNameSpriteCtrl.Setup (MaimaiStyleDesigner.GetComboColor(), "combo", 36f, 0.3f);
			numberCtrl.value = evaluater.combo;
			numberCtrl.SetActive (evaluater.combo > 1 && !fullComboAnimationIsDone);
			lifeBackImage.gameObject.SetActive(false);
		}
		else if (bgInfoType == ConfigTypes.BackGroundInfomation.PCOMBO) {
			numberCtrl.Setup (0, false, true, MaimaiStyleDesigner.GetPComboColor());
			bgInfoNameSpriteCtrl.Setup (MaimaiStyleDesigner.GetPComboColor(), "pcombo", 36f, 0.3f);
			numberCtrl.value = evaluater.pcombo;
			numberCtrl.SetActive (evaluater.pcombo > 1 && !fullComboAnimationIsDone);
			lifeBackImage.gameObject.SetActive(false);
		}
		else if (bgInfoType == ConfigTypes.BackGroundInfomation.BPCOMBO) {
			numberCtrl.Setup (0, false, true, MaimaiStyleDesigner.GetBPComboColor());
			bgInfoNameSpriteCtrl.Setup (MaimaiStyleDesigner.GetBPComboColor(), "bpcombo", 36f, 0.3f);
			numberCtrl.value = evaluater.bpcombo;
			numberCtrl.SetActive (evaluater.bpcombo > 1 && !fullComboAnimationIsDone);
			lifeBackImage.gameObject.SetActive(false);
		}
		else if (bgInfoType == ConfigTypes.BackGroundInfomation.ACHIEVEMENT) {
			float value = evaluater.GetAchievement().ToPercent2();
			numberCtrl.Setup (2, true, true, MaimaiStyleDesigner.GetAchievementColor(value));
			bgInfoNameSpriteCtrl.Setup (MaimaiStyleDesigner.GetAchievementColor(value), "achievement", 28f, 0.3f);
			numberCtrl.value = value;
			numberCtrl.SetActive (!fullComboAnimationIsDone);
			lifeBackImage.gameObject.SetActive(false);
		}
		else if (bgInfoType == ConfigTypes.BackGroundInfomation.SCORE) {
			numberCtrl.Setup (0, false, true, MaimaiStyleDesigner.GetScoreColor());
			bgInfoNameSpriteCtrl.Setup (MaimaiStyleDesigner.GetScoreColor(), "score", 36f, 0.3f);
			numberCtrl.value = evaluater.GetScore();
			numberCtrl.SetActive (!fullComboAnimationIsDone);
			lifeBackImage.gameObject.SetActive(false);
		}
		else {
			numberCtrl.SetActive (false);
			bgInfoNameSpriteCtrl.gameObject.SetActive (false);
			lifeBackImage.gameObject.SetActive(false);
		}
		SetBgInfoVisualizeTargetInit ();
		Color fullComboColor = MaimaiStyleDesigner.ByteToPercentRGBA(255,128,0,255);
		fullComboAnimationCtrl.Setup (fullComboColor, "full combo", 30f, 0.3f);
		allPerfectAnimationCtrl.Setup (fullComboColor, "all perfect", 30f, 0.3f);
		hundredPercentSynchronizeAnimationCtrl.Setup (fullComboColor, "100% sync", 32f, 0.3f);
		fullComboAnimationIsDone = false;

		initialize = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (!initialize)
			return;

		if (challengeLife != ConfigTypes.LifeDifficulty.INFINITY) {
			numberCtrl.value = evaluater.life.Value;
			numberCtrl.SetActive (!fullComboAnimationIsDone);
			numberCtrl.SetColor (MaimaiStyleDesigner.GetRemainLifeColor(challengeLife, evaluater.life.Value));
		}
		else if (bgInfoType == ConfigTypes.BackGroundInfomation.COMBO) {
			numberCtrl.value = evaluater.combo;
			numberCtrl.SetActive (evaluater.combo > 1 && !fullComboAnimationIsDone);
		}
		else if (bgInfoType == ConfigTypes.BackGroundInfomation.PCOMBO) {
			numberCtrl.value = evaluater.pcombo;
			numberCtrl.SetActive (evaluater.pcombo > 1 && !fullComboAnimationIsDone);
		}
		else if (bgInfoType == ConfigTypes.BackGroundInfomation.BPCOMBO) {
			numberCtrl.value = evaluater.bpcombo;
			numberCtrl.SetActive (evaluater.bpcombo > 1 && !fullComboAnimationIsDone);
		}
		else if (bgInfoType == ConfigTypes.BackGroundInfomation.ACHIEVEMENT) {
			float value = evaluater.GetAchievement().ToPercent2();
			if (numberCtrl.value != value) {
				numberCtrl.value = value;
				numberCtrl.spriteColor = MaimaiStyleDesigner.GetAchievementColor(value);
			}
			numberCtrl.SetActive (!fullComboAnimationIsDone);
		}
		else if (bgInfoType == ConfigTypes.BackGroundInfomation.SCORE) {
			float value = evaluater.GetScore();
			numberCtrl.value = value;
			numberCtrl.SetActive (!fullComboAnimationIsDone);
		}
		if (SetBgInfoVisualizeTarget ()) {
			bgInfoNameSpriteCtrl.ActivateSpriteRenderer();
		}

		if (!fullComboAnimationIsDone && MiscInformationController.instance.rhythmGameRequesterSceneName != "editor" && MiscInformationController.instance.rhythmGameRequesterSceneName != "tutorial") {
			const int NONE = 0, AP = 1, FC = 2, SYNC = 3;
			int type = NONE;
#if false
			// sync作ったらコメント解除.
			if (syncEvaluater != null) {
				if (syncEvaluater.isAllSyncEvaluated) {
					if (syncEvaluater.syncPercent == 1.0f) {
						type = SYNC;
					}
					else if (evaluater.IsAllPerfect ()) {
						type = AP;
					}
					else if (evaluater.IsNoMiss ()) {
						type = FC;
					}
				}
			}
			else 
#endif
			if (evaluater.IsAllPerfect ()) {
				type = AP;
			}
			else if (evaluater.IsNoMiss ()) {
				type = FC;
			}
			if (type > NONE) {
				bgInfoNameSpriteCtrl.gameObject.SetActive (false);
				numberCtrl.gameObject.SetActive (false);
				if (type == AP) {
					Utility.SoundEffectManager.Play ("maisq_voice_ap");
					allPerfectAnimationCtrl.ActivateSpriteRenderer();
				}
				else if (type == FC) {
					Utility.SoundEffectManager.Play ("maisq_voice_fc");
					fullComboAnimationCtrl.ActivateSpriteRenderer();
				}
				else if (type == SYNC) {
					Utility.SoundEffectManager.Play ("maisq_voice_100persync");
					hundredPercentSynchronizeAnimationCtrl.ActivateSpriteRenderer ();
				}
				fullComboAnimationIsDone = true;
			}
		}
	}

	public void SetBgInfoVisualizeTargetInit () {
		float initValue = 0;
		float stepValue = bgInfoStepValue;
		if (challengeLife != ConfigTypes.LifeDifficulty.INFINITY) {
			initValue = 0;
			bgInfoValue = evaluater.life.Value;
		}
		else if (bgInfoType == ConfigTypes.BackGroundInfomation.COMBO) {
			initValue = 2;
			bgInfoValue = evaluater.combo;
		}
		else if (bgInfoType == ConfigTypes.BackGroundInfomation.PCOMBO) {
			initValue = 2;
			bgInfoValue = evaluater.pcombo;
		}
		else if (bgInfoType == ConfigTypes.BackGroundInfomation.BPCOMBO) {
			initValue = 2;
			bgInfoValue = evaluater.bpcombo;
		}
		else if (bgInfoType == ConfigTypes.BackGroundInfomation.ACHIEVEMENT ||
		         bgInfoType == ConfigTypes.BackGroundInfomation.SCORE) {
			initValue = 0;
			bgInfoValue = evaluater.GetAchievement().ToPercent2();
		}
		bgInfoOldValue = bgInfoValue;
		if (bgInfoValue < initValue) {
			bgInfoVisualizeTarget = initValue;
			bgInfoNameSpriteCtrl.gameObject.SetActive (false);
		}
		else {
			float target = bgInfoValue.ToInt() / 10 * 10;
			if (target == bgInfoValue) {
				bgInfoVisualizeTarget = bgInfoValue;
			}
			else {
				bgInfoVisualizeTarget = target + stepValue;
			}
			bgInfoNameSpriteCtrl.gameObject.SetActive (!fullComboAnimationIsDone);
		}
		bgInfoFirstTarget = bgInfoVisualizeTarget;
	}
	
	public bool SetBgInfoVisualizeTarget () {
		bool ret = false;
		float initValue = 0;
		float stepValue = bgInfoStepValue;
		if (challengeLife != ConfigTypes.LifeDifficulty.INFINITY) {
			initValue = 0;
			bgInfoValue = evaluater.life.Value;
			return false;
		}
		else if (bgInfoType == ConfigTypes.BackGroundInfomation.COMBO) {
			initValue = 2;
			bgInfoValue = evaluater.combo;
		}
		else if (bgInfoType == ConfigTypes.BackGroundInfomation.PCOMBO) {
			initValue = 2;
			bgInfoValue = evaluater.pcombo;
		}
		else if (bgInfoType == ConfigTypes.BackGroundInfomation.BPCOMBO) {
			initValue = 2;
			bgInfoValue = evaluater.bpcombo;
		}
		else if (bgInfoType == ConfigTypes.BackGroundInfomation.ACHIEVEMENT ||
		         bgInfoType == ConfigTypes.BackGroundInfomation.SCORE) {
			initValue = 0;
			bgInfoValue = evaluater.GetAchievement().ToPercent2();
		}
		if (bgInfoOldValue > bgInfoValue) {
			bgInfoVisualizeTarget = initValue;
			ret = true;
		}
		else if (bgInfoVisualizeTarget <= bgInfoValue && bgInfoType != ConfigTypes.BackGroundInfomation.OFF) {
			bgInfoVisualizeTarget = bgInfoVisualizeTarget.ToInt() / 10 * 10 + stepValue;
			ret = true;
		}
		if (bgInfoValue < bgInfoFirstTarget) {
			if (bgInfoNameSpriteCtrl.gameObject.activeSelf) {
				bgInfoNameSpriteCtrl.gameObject.SetActive (false);
			}
		}
		else if (!bgInfoNameSpriteCtrl.gameObject.activeSelf) {
			bgInfoNameSpriteCtrl.gameObject.SetActive (!fullComboAnimationIsDone);
		}
		bgInfoOldValue = bgInfoValue;
		return ret;
	}
}
