﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class BgInfoAnimationController : MonoBehaviour {
	
	public GameObject imageObjPrefab;
	public float spriteWidth; // 文字幅.
	public float tokenWidthPer; // spriteWidthに対する、カンマなどの文字幅割合.
	public Sprite[] sprites;
	public Color spriteColor;
	
	private List<Image> renderers;
	
	public void LoadSprites (string text) {
		sprites = new Sprite[text.Length];
		for (int i = 0; i < text.Length; i++) {
			if (text[i] == ' ') {
				sprites[i] = SpriteTank.Get ("icon_dammy");
			}
			else if (text[i] == '.') {
				sprites[i] = SpriteTank.Get ("bginfo_dot");
			}
			else if (text[i] == ',') {
				sprites[i] = SpriteTank.Get ("bginfo_comma");
			}
			else if (text[i] == '%') {
				sprites[i] = SpriteTank.Get ("bginfo_percent");
			}
			else {
				sprites[i] = SpriteTank.Get ("bginfo_" + text[i]);
			}
		}
	}
	
	public void Setup (Color spriteColor, string text, float spriteWidth, float tokenWidthPer) {
		LoadSprites (text);
		
		this.spriteColor = spriteColor;
		this.spriteWidth = spriteWidth;
		this.tokenWidthPer = tokenWidthPer;
		
		renderers = new List<Image>();
		
		CreateSpriteRenderer (text);
	}
	
	private void CreateSpriteRenderer (string text) {
		MatchOfRendererObjAmount(text);
		int size = renderers.Count;
		for (int i = 0; i < size; i++) {
			var renderer = renderers[i];
			renderer.sprite = sprites[i];
			var rendererColor = new Color (spriteColor.r, spriteColor.g, spriteColor.b, 0);
			renderer.color = rendererColor;
			var anim = renderer.GetComponent<TimeAnimationAlpha>();
			anim.GetData[0].time = (0.5f / (float)size) * i;
			anim.loopEndEvent = (rendererObj) => {
				rendererObj.SetActive (false);
			};
		}
	}
	
	public void ActivateSpriteRenderer () {
		int size = renderers.Count;
		for (int i = 0; i < size; i++) {
			renderers[i].GetComponent<TimeAnimationAlpha>().Restart();
			renderers[i].gameObject.SetActive (true);
		}
	}
	
	// 文字数とゲームオブジェクトの数を合わせる.
	private void MatchOfRendererObjAmount(string vstr) {
		int amount = vstr.Length;
		bool move = false;
		for (int i = 0; i < renderers.Count; i++) {
			if (renderers[i] == null) {
				renderers.RemoveAt(i);
				i = -1;
			}
		}
		// 削除.
		if (renderers.Count > amount) {
			move = true;
			while (renderers.Count != amount) {
				var sr = renderers[renderers.Count - 1];
				renderers.Remove(sr);
				Destroy(sr.gameObject);
			}
		}
		// 生成.
		else if (renderers.Count < amount) {
			move = true;
			while (renderers.Count != amount) {
				var item = Instantiate(imageObjPrefab) as GameObject;
				var sr = item.GetComponent<Image>();
				sr.rectTransform.SetParent(this.transform);
				renderers.Add(sr);
				item.SetActive (false);
			}
		}
		// 中央揃えのため移動.
		if (move) {
			int size = renderers.Count;
			float[] xs = new float[size];
			float xTotal = 0;
			for (int i = 0; i < size; i++) {
				string sub = vstr.Substring(i, 1);
				float x;
				if (sub == "." || sub == ",") {
					x = spriteWidth * tokenWidthPer;
				}
				else {
					x = spriteWidth;
				}
				xTotal += x;
				renderers[i].rectTransform.sizeDelta = new Vector2(spriteWidth, spriteWidth);
				xs[i] = x;
			}
			float xTotalHalf = xTotal * 0.5f;
			float exTotal = 0;
			for (int i = 0; i < size; i++) {
				var t = renderers[i].rectTransform;
				t.anchoredPosition = new Vector2(exTotal - xTotalHalf + xs[i] * 0.5f, 0);
				exTotal += xs[i];
			}
		}
	}
}
