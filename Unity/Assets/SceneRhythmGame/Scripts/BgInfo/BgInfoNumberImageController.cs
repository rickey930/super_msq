﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class BgInfoNumberImageController : MonoBehaviour {
	
	public virtual float value { get; set; }
	private float? oldValue;
	
	public GameObject bgValuePrefab;
	public float spriteWidth; // 文字幅.
	public float tokenWidthPer; // spriteWidthに対する、カンマなどの文字幅割合.
	public Sprite[] NumberSprites;
	public Sprite commaSprite;
	public Sprite dotSprite;
	public Sprite percentSprite;
	public int digit; //小数点以下第何位切り捨てで表示するか.
	public bool isPercent; // パーセント記号を表示するか.
	public bool visibleComma; //桁区切りを表示するか
	public Color spriteColor;
	private Color? oldSpriteColor;
	private bool initialized;
	private System.IFormatProvider formatProvider;
	private bool? oldAcitveState;
	
	private List<Image> renderers;
	
	public void Setup (int digit, bool isPercent, bool visibleComma, Color spriteColor) {
		this.digit = digit;
		this.isPercent = isPercent;
		this.visibleComma = visibleComma;
		SetColor(spriteColor);
		
		oldSpriteColor = spriteColor;
		renderers = new List<Image>();
		//MatchOfRendererObjAmount(1);
		//renderers [0].sprite = NumberSprites [0];
		formatProvider = new System.Globalization.CultureInfo ("en-US");
		initialized = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (!initialized) return;
		
		if (!oldValue.HasValue || oldValue.Value != value) {
			string format = visibleComma ? "#,0" : "0";
			if (digit > 0) format += ".";
			for (int i = 0; i < digit; i++) format += "0";
			if (isPercent) format += "'%'";
			string vstr = value.ToString(format, formatProvider);
			if (!oldValue.HasValue || oldValue.Value.ToString(format, formatProvider).Length != vstr.Length) {
				MatchOfRendererObjAmount(vstr);
			}
			int size = renderers.Count;
			for (int i = 0; i < size; i++) {
				string sub = vstr.Substring(i, 1);
				if (sub == ".") {
					renderers[i].sprite = dotSprite;
				}
				else if (sub == ",") {
					renderers[i].sprite = commaSprite;
				}
				else if (sub == "%") {
					renderers[i].sprite = percentSprite;
				}
				else {
					renderers[i].sprite = NumberSprites[int.Parse(sub)];
				}
			}
			oldValue = value;
		}
		
		if (!oldSpriteColor.HasValue || oldSpriteColor.Value.r != spriteColor.r ||
		    oldSpriteColor.Value.g != spriteColor.g || oldSpriteColor.Value.b != spriteColor.b) {
			int size = renderers.Count;
			for (int i = 0; i < size; i++) {
				renderers[i].color = spriteColor;
			}
			oldSpriteColor = spriteColor;
		}
	}
	
	// 文字数とゲームオブジェクトの数を合わせる.
	private void MatchOfRendererObjAmount(string vstr) {
		int amount = vstr.Length;
		bool move = false;
		// 削除.
		if (renderers.Count > amount) {
			move = true;
			while (renderers.Count != amount) {
				var sr = renderers[renderers.Count - 1];
				renderers.Remove(sr);
				Destroy(sr.gameObject);
			}
		}
		// 生成.
		else if (renderers.Count < amount) {
			move = true;
			while (renderers.Count != amount) {
				var item = Instantiate(bgValuePrefab) as GameObject;
				var sr = item.GetComponent<Image>();
				sr.rectTransform.SetParent(this.transform);
				sr.color = spriteColor;
				renderers.Add(sr);
			}
		}
		// 中央揃えのため移動.
		if (move) {
			int size = renderers.Count;
			float[] xs = new float[size];
			float xTotal = 0;
			for (int i = 0; i < size; i++) {
				string sub = vstr.Substring(i, 1);
				float x;
				if (sub == "." || sub == ",") {
					x = spriteWidth * tokenWidthPer;
				}
				else {
					x = spriteWidth;
				}
				xTotal += x;
				renderers[i].rectTransform.sizeDelta = new Vector2(spriteWidth, spriteWidth);
				xs[i] = x;
			}
			float xTotalHalf = xTotal * 0.5f;
			float exTotal = 0;
			for (int i = 0; i < size; i++) {
				var t = renderers[i].rectTransform;
				t.anchoredPosition = new Vector2(exTotal - xTotalHalf + xs[i] * 0.5f, 0);
				exTotal += xs[i];
				if (!oldAcitveState.HasValue) oldAcitveState = true;
				t.gameObject.SetActive (oldAcitveState.Value);
			}
		}
	}
	
	public void SetActive (bool value) {
		if (renderers != null) {
			if (!oldAcitveState.HasValue || value != oldAcitveState.Value) {
				int size = renderers.Count;
				for (int i = 0; i < size; i++) {
					if (renderers [i] != null) {
						renderers [i].gameObject.SetActive (value);
					}
				}
				oldAcitveState = value;
			}
		}
	}

	public void SetColor(Color color) {
		this.spriteColor = color;
	}
	
}
