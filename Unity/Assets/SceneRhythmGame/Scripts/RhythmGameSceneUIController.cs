using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RhythmGameSceneUIController : MonoBehaviour {

	public Text titleLabel;
	public Text levelLabel;
	public Text difficultyLabel;
	public Text designerLabel;
	public Image difficultyIcon;
	public Text achievementLabel;
	public Text syncpointLabel;
	public GameObject autoPlayLabelObj; 
	public Text judgementTypeLabel;
	public Text achievementNameLabel;
	public Text synchronizeNameLabel;
	public Image jacket;
	public float? achievement { get; set; }
	public bool? isAutoPlay { get; set; }
	public ConfigTypes.Judgement? judgementType { get; set; }
	private float oldAchievement { get; set; }
	public float? syncPoint { get; set; }
	private float oldSyncPoint { get; set; }
	public bool isSyncMode { private get; set; }
	public GameObject pauseButtonObj;
	public Button pauseButton;
	public Image pauseButtonAnimation;
	public bool pauseButtonHolding { get; set; }
	public Button seekWaitSettingButton;
	public Button repeatSettingButton;
	public ConfigTypes.RankVersion? rankVisionVersion { get; set; }
	public AchievementBarController achievementBarCtrl;

	// Dialog.
	public GameObject pauseDialog;
	public GameObject seekWaitSettingDialog;
	public InputField seekInputLabel;
	public InputField waitInputLabel;
	public GameObject repeatSettingDialog;
	public Slider repeatStartSlider;
	public InputField repeatInputLabel;

	public GameObject loadingScreen;

	public Image backgroundImage;
	public Image guideCircle;

	public bool initialize { get; private set; }

	// Use this for initialization
	protected virtual IEnumerator Start () {
		initialize = false;
		while (!achievement.HasValue || !isAutoPlay.HasValue || !judgementType.HasValue || !rankVisionVersion.HasValue || UserDataController.instance == null) {
			yield return null;
		}

		var track = TrackInformationController.instance;
		titleLabel.text = track.title;
		levelLabel.text = track.level;
		difficultyLabel.text = track.difficulty;
		designerLabel.text = track.notes_design;
		titleLabel.color = MaimaiStyleDesigner.GetLevelColor (track.difficulty);
		//levelLabel.color = difficultyLabel.color = titleLabel.color;
		difficultyIcon.sprite = MaimaiStyleDesigner.GetDifficultyOctagonSprite (track.difficulty);
		if (track.jacket != null) {
			jacket.sprite = track.jacket;
		}
		else {
			// ジャケットが無いならデフォ画像を表示.
			jacket.sprite = SpriteTank.Get ("icon_track_failed");
		}
		if (jacket.sprite == null) {
			var color = jacket.color;
			color.a = 0;
			jacket.color = color;
		}
		else {
			var color = jacket.color;
			color.a = 1;
			jacket.color = color;
		}
		if (achievement.HasValue) {
			float achiv = achievement.Value.ToPercent2 ();
			achievementLabel.text = achiv.ToString ("F2") + "%";
			achievementLabel.color = achievementNameLabel.color = MaimaiStyleDesigner.GetAchievementColor (achiv * 100.0f);
		}
		if (syncPoint.HasValue) {
			float syncp = syncPoint.Value.ToPercent2 ();
			syncpointLabel.text = syncp.ToString ("F0") + "%";
			syncpointLabel.gameObject.SetActive (true);
			synchronizeNameLabel.gameObject.SetActive (true);
		}
		else {
			syncpointLabel.gameObject.SetActive (false);
			synchronizeNameLabel.gameObject.SetActive (false);
		}
		autoPlayLabelObj.SetActive (isAutoPlay.Value);
		judgementTypeLabel.text = judgementType.Value.ToConfigValueString ();
		judgementTypeLabel.color = judgementType.Value.ToConfigValueColor ();
		judgementTypeLabel.gameObject.SetActive (judgementType.Value != ConfigTypes.Judgement.NORMAL);
		seekWaitSettingButton.gameObject.SetActive (MiscInformationController.instance.rhythmGameRequesterSceneName != "tutorial");
		repeatSettingButton.gameObject.SetActive (MiscInformationController.instance.rhythmGameRequesterSceneName != "editor" && MiscInformationController.instance.rhythmGameRequesterSceneName != "tutorial");

		pauseButtonObj.SetActive (MiscInformationController.instance.syncMode == 0);

		backgroundImage.color = new Color(backgroundImage.color.r, backgroundImage.color.g, backgroundImage.color.b, UserDataController.instance.config.brightness);
		achievementBarCtrl.isSyncMode = isSyncMode;
		while (!achievementBarCtrl.initialized) {
			yield return null;
		}
		if (!isSyncMode) {
			achievementBarCtrl.SetAchievementValue(rankVisionVersion.Value, achievement.Value);
		}
		else {
			achievementBarCtrl.SetSyncPointValue(syncPoint.Value);
		}

		initialize = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (achievement.HasValue && oldAchievement != achievement.Value) {
			achievementLabel.text = achievement.Value.ToPercent2().ToString("F2") + "%";
			oldAchievement = achievement.Value;
			achievementLabel.color = achievementNameLabel.color = MaimaiStyleDesigner.GetAchievementColor (achievement.Value * 100.0f);
			if (!isSyncMode) {
				achievementBarCtrl.SetAchievementValue(rankVisionVersion.Value, achievement.Value);
			}
		}
		if (syncPoint.HasValue && oldSyncPoint != syncPoint.Value) {
			float syncp = syncPoint.Value.ToPercent2 ();
			syncpointLabel.text = syncp.ToInt().ToString() + "%";
			oldSyncPoint = syncPoint.Value;
			if (isSyncMode) {
				achievementBarCtrl.SetSyncPointValue(syncPoint.Value);
			}
		}
	}

	public void OnPauseButtonDown () {
		pauseButtonHolding = true;
	}

	public void OnPauseButtonUp () {
		pauseButtonHolding = false;
	}
}
