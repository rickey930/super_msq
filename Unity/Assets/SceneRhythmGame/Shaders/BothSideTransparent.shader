﻿Shader "Custom/BothSideTransparentShader" {
	Properties {
		_FrontTex ("Front (RGB)", 2D) = "white" {}
		_BackTex ("Back (RGB)", 2D) = "white" {}
		_SpColor ("Color", Color) = (1.0,1.0,1.0,1.0) // colors
	}
	SubShader {
		Pass {
			Lighting Off
			Cull Back
			Tags { "RenderType" = "Transparent" "Queue" = "Transparent" }
            Blend SrcAlpha OneMinusSrcAlpha
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			#include "UnityCG.cginc"

			uniform sampler2D _FrontTex;
			uniform float4 _SpColor;
			
			struct v2f {
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
			};
			
			float4 _FrontTex_ST;
			
			v2f vert (appdata_base v)
			{
				v2f o;
				o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
				o.uv = TRANSFORM_TEX (v.texcoord, _FrontTex);
				return o;
			}
			
			half4 frag (v2f i) : COLOR
			{
				half4 texcol = tex2D (_FrontTex, i.uv).rgba;
				texcol.r = texcol.r * _SpColor.r;
				texcol.g = texcol.g * _SpColor.g;
				texcol.b = texcol.b * _SpColor.b;
				texcol.a = texcol.a * _SpColor.a;
				return texcol;
			}

			ENDCG
		}
		
		Pass {
			Lighting Off
			Cull Front
			Tags { "RenderType" = "Transparent" "Queue" = "Transparent" }
            Blend SrcAlpha OneMinusSrcAlpha
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			#include "UnityCG.cginc"

			uniform sampler2D _BackTex;
			uniform float4 _SpColor;
			
			struct v2f {
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
			};
			
			float4 _BackTex_ST;
			
			v2f vert (appdata_base v)
			{
				v2f o;
				o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
				o.uv = TRANSFORM_TEX (v.texcoord, _BackTex);
				return o;
			}
			
			half4 frag (v2f i) : COLOR
			{
				half4 texcol = tex2D (_BackTex, i.uv).rgba;
				texcol.r = texcol.r * _SpColor.r;
				texcol.g = texcol.g * _SpColor.g;
				texcol.b = texcol.b * _SpColor.b;
				texcol.a = texcol.a * _SpColor.a;
				return texcol;
			}

			ENDCG
		}
		
		
	}
	FallBack "Diffuse"
}
