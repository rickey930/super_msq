﻿Shader "Custom/SurfaceTransparentShader" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_SpColor ("Color", Color) = (1.0,1.0,1.0,1.0) // colors
	}
	SubShader {
		Pass {
			Lighting Off
			Cull Off
			Tags { "RenderType" = "Transparent" "Queue" = "Transparent" }
            Blend SrcAlpha OneMinusSrcAlpha
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			#include "UnityCG.cginc"

			uniform sampler2D _MainTex;
			uniform float4 _SpColor;
			
			struct v2f {
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
			};
			
			float4 _MainTex_ST;
			
			v2f vert (appdata_base v)
			{
				v2f o;
				o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
				o.uv = TRANSFORM_TEX (v.texcoord, _MainTex);
				return o;
			}
			
			half4 frag (v2f i) : COLOR
			{
				half4 texcol = tex2D (_MainTex, i.uv).rgba;
				texcol.r = texcol.r * _SpColor.r;
				texcol.g = texcol.g * _SpColor.g;
				texcol.b = texcol.b * _SpColor.b;
				texcol.a = texcol.a * _SpColor.a;
				return texcol;
			}

			ENDCG
		}
	}
	FallBack "Diffuse"
}
