﻿Shader "Custom/AchievementBarShader" {
	Properties {
		_MainTex ("Base (ARGB)", 2D) = "white" { }
		_BarColor0 ("Bar Color (Normal)", Color) = (1.0,1.0,1.0,1.0) // colors
		_BarColor1 ("Bar Color (Clear Border)", Color) = (1.0,1.0,1.0,1.0) // colors
		_BarColor2 ("Bar Color (SS Border)", Color) = (1.0,1.0,1.0,1.0) // colors
		_BarColor3 ("Bar Color (Base)", Color) = (1.0,1.0,1.0,1.0) // colors
		_Width ("Achievement", Range (0.0, 2.0)) = 0.0 // sliders
	} 
	SubShader {
		Pass {
			Lighting Off
			Cull Off
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			#include "UnityCG.cginc"

			uniform sampler2D _MainTex;
			uniform float4 _Color;
			uniform float4 _BarColor0;
			uniform float4 _BarColor1;
			uniform float4 _BarColor2;
			uniform float4 _BarColor3;
			uniform float _Width;
			
			struct v2f {
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
			};
			
			float4 _MainTex_ST;
			
			v2f vert (appdata_base v)
			{
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = TRANSFORM_TEX (v.texcoord, _MainTex);
				return o;
			}
			
			half4 frag (v2f i) : COLOR
			{
				float play_result_percent = _Width;
				float clear_line_percent = 0.8;
				float ss_line_percent = 1.0;
				
				half4 texcol = _BarColor0;
				if (i.uv.x > play_result_percent) {
					texcol = _BarColor3;
				}
				else if (i.uv.x < play_result_percent - ss_line_percent && play_result_percent >= ss_line_percent) {
					texcol = _BarColor2;
				}
				else if (i.uv.x >= clear_line_percent && play_result_percent >= clear_line_percent) {
					texcol = _BarColor1;
				}
				fixed4 c = tex2D(_MainTex, i.uv);
    			texcol.a = c.a;

				return texcol;
			}

			ENDCG
		}
	}
	FallBack "Diffuse"
}

