﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(DialogButton))]
public class DialogButtonHelper : MonoBehaviour {

	public Graphic[] targetGraphics; 

	// Use this for initialization
	void Start () {
		GetComponent<DialogButton>().Setup(targetGraphics);
	}
}
