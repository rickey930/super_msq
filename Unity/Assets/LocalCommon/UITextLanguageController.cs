﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Text))]
public class UITextLanguageController : MonoBehaviour {
	private Text label;
	[SerializeField]
	public string key;
	IEnumerator Start() {
		label = GetComponent<Text>();
		while (UserDataController.instance == null || UserDataController.instance.language == null) {
			yield return null;
		}
		Rewrite(key);
	}

	public void Rewrite(string key) {
		this.key = key;
		if (UserDataController.instance == null || UserDataController.instance.language == null || label == null) {
			return;
		}
		if (string.IsNullOrEmpty(key)) {
			label.text = string.Empty;
		}
		else if (UserDataController.instance.language.ContainsKey(key)) {
			label.text = UserDataController.instance.language[key];
		}
		else {
			label.text = key;
			Debug.LogWarning("Language key not found.");
		}
	}
}
