﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ProfileIconController : Image {
	IEnumerator StartCustomCoroutine () {
		while (ProfileInformationController.instance == null || !ProfileInformationController.instance.loaded_player_icon) {
			yield return null;
		}
		this.sprite = ProfileInformationController.instance.player_icon;
		ProfileInformationController.instance.profileIconCtrl = this;
	}
	
	public void StartCustom () {
		StartCoroutine (StartCustomCoroutine ());
	}
	
	protected override void Start () {
		base.Start ();
		StartCustom ();
	}
}
