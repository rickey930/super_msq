﻿using UnityEngine;
using System.Collections;
using System.IO;

public class DataPath {
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
	public const string LOCAL_PATH_PREFIX = "file:///";
#else
	public const string LOCAL_PATH_PREFIX = "file://";
#endif
#if UNITY_EDITOR_WIN
	public const string TOP_PATH = "D:/maiPadAir/userdrive/";
#else
	public static string TOP_PATH = Application.persistentDataPath;
#endif
	//public const string TOP_PATH = "";
	
	public const string SYSTEM_DIRECTORY = "maisq";
	public const string AUDIOS_DIRECTORY = "audios";
	public const string JACKETS_DIRECTORY = "jackets";
	public const string PROFILES_DIRECTORY = "profiles";
	
	public const string TRACK_FILENAME = "track.dat";
	public const string RECORD_FILENAME = "record.dat";
	public const string USER_FILENAME = "user.dat";
	public const string CONFIG_FILENAME = "config.dat";
	public const string LIBRARY_FILENAME = "library.json";
	public const string LANGUAGE_FILENAME = "library.json";
	public const string SCOREEDITOR_AUTOSAVE_FILENAME = "editor_autosave.dat";
	public const string PLAYER_ICON_FILENAME = "icon";
	public const string PLAYER_PLATE_FILENAME = "plate";
	public const string PLAYER_FRAME_FILENAME = "frame";

	public const string OFFCIAL_SITE_SCORE_PACKAGE_URL = "http://www12.atpages.jp/prevo0120/maipadmini/data/maisq.json";
	
	private static string GetTopDirectoryPath() {
		string top_path_arrange = TOP_PATH;
		string top_path_lastone = top_path_arrange.Substring (top_path_arrange.Length - 1, 1);
		if (top_path_lastone != "/") {
			if (top_path_lastone == "\\") {
				top_path_arrange = top_path_arrange.Substring (0, top_path_arrange.Length - 1);
			}
			top_path_arrange += "/";
		}
		return top_path_arrange;
	}
	private static string GetSystemDirectoryPath() {
		string ret = GetTopDirectoryPath() + SYSTEM_DIRECTORY + "/";
		if (!Directory.Exists (ret)) {
			Directory.CreateDirectory (ret);
		}
		return ret;
	}
	private static string GetAudiosDirectoryPath() {
		string ret = GetSystemDirectoryPath() + AUDIOS_DIRECTORY + "/";
		if (!Directory.Exists (ret)) {
			Directory.CreateDirectory (ret);
		}
		return ret;
	}
	private static string GetJacketsDirectoryPath() {
		string ret = GetSystemDirectoryPath() + JACKETS_DIRECTORY + "/";
		if (!Directory.Exists (ret)) {
			Directory.CreateDirectory (ret);
		}
		return ret;
	}
	private static string GetProflesDirectoryPath() {
		string ret = GetSystemDirectoryPath() + PROFILES_DIRECTORY + "/";
		if (!Directory.Exists (ret)) {
			Directory.CreateDirectory (ret);
		}
		return ret;
	}
	private static string GetTempDirectoryPath () {
#if UNITY_EDITOR_WIN
		string ret = GetSystemDirectoryPath() + "temp" + "/";
#else
		string ret = Application.temporaryCachePath;
#endif
		if (!Directory.Exists (ret)) {
			Directory.CreateDirectory (ret);
		}
		return ret;
	}
	
	public static string GetTrackPath () {
		return GetSystemDirectoryPath() + TRACK_FILENAME;
	}
	public static string GetRecordPath () {
		return GetSystemDirectoryPath() + RECORD_FILENAME;
	}
	public static string GetUserPath () {
		return GetSystemDirectoryPath() + USER_FILENAME;
	}
	public static string GetConfigPath () {
		return GetSystemDirectoryPath() + CONFIG_FILENAME;
	}
	public static string GetLibraryPath () {
		return GetSystemDirectoryPath() + LIBRARY_FILENAME;
	}
	public static string GetLanguagePath () {
		return GetSystemDirectoryPath() + LANGUAGE_FILENAME;
	}
	public static string GetScoreEditorAutoSavePath () {
		return GetSystemDirectoryPath() + SCOREEDITOR_AUTOSAVE_FILENAME;
	}
	public static string GetAudioPath (string trackId, string extension) {
		return GetAudiosDirectoryPath() + GetResourceFileName(trackId) + extension;
	}
	public static string GetJacketPath (string trackId, string extension) {
		return GetJacketsDirectoryPath() + GetResourceFileName(trackId) + extension;
	}
	public static string GetPlayerIconPath (string extension) {
		return GetProflesDirectoryPath() + PLAYER_ICON_FILENAME + extension;
	}
	public static string GetPlayerPlatePath (string extension) {
		return GetProflesDirectoryPath() + PLAYER_PLATE_FILENAME + extension;
	}
	public static string GetPlayerFramePath (string extension) {
		return GetProflesDirectoryPath() + PLAYER_FRAME_FILENAME + extension;
	}
	
	public static string GetResourceFileName (string trackId) {
		byte[] trackIdUtf8Bytes = System.Text.Encoding.UTF8.GetBytes (trackId);
		string base64 = System.Convert.ToBase64String (trackIdUtf8Bytes);
		string base64url = base64.Replace ("+", "-").Replace ("/", "_");
		return base64url;
	}

	public static string GetOfficialSiteScorePackageURL () {
		return OFFCIAL_SITE_SCORE_PACKAGE_URL;
	}

	public static string GetSyncTempDirectoryPath () {
		string ret = GetTempDirectoryPath();
		if (!Directory.Exists (ret)) {
			Directory.CreateDirectory (ret);
		}
		return ret;
	}

}
