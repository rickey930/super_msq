using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Constants {
	private static Constants _instance;
	public static Constants instance {
		get {
			if (_instance == null) {
				_instance = new Constants();
			}
			return _instance;
		}
	}
	
	/// <summary>判定円の半径</summary>
	public float MAIMAI_OUTER_RADIUS { get { return 170.0f; } }
	/// <summary>ノート移動開始位置は、中心からどのくらい空けるか</summary>
	public float NOTE_MOVE_START_MARGIN { get { return 50.0f; } }
	/// <summary>評価表示位置の半径</summary>
	public float EVALUATE_SHOW_RADIUS { get { return MAIMAI_OUTER_RADIUS - 50.0f; } }
	/// <summary>スライドマーカーの描画間隔</summary>
	public float SLIDE_MARKER_TO_MARKER_INTERVAL { get { return 18.0f; } }
	/// <summary>スライドマーカーの置き始めと置き終わりのマージン</summary>
	public float SLIDE_MARKER_TO_SENSOR_MARGIN { get { return 15.0f; } }
	/// <summary>センサーの半径(デフォルト)</summary>
	public float SENSOR_RADIUS_DEFAULT { get { return 70.0f; } }
	/// <summary>センサーの半径</summary>
	public float SENSOR_RADIUS { get { return UserDataController.instance != null && UserDataController.instance.config != null ? UserDataController.instance.config.sensor_size : SENSOR_RADIUS_DEFAULT; } }
	/// <summary>一般ノートのActionIdの基準値</summary>
	public int MAIMAI_NOTE_ACTION_ID_BASE { get { return 0; } }
	/// <summary>ホールドノートのActionIdの基準値</summary>
	public int MAIMAI_NOTE_HOLD_FOOT_ACTION_ID_BASE { get { return 100; } }
	/// <summary>スライドパターンノートのActionIdの基準値</summary>
	public int MAIMAI_NOTE_SLIDE_PATTERN_ACTION_ID_BASE { get { return 200; } }
	public int MAIMAI_NOTE_SLIDE_PATTERN_ACTION_ID_CENTER { get { return MAIMAI_NOTE_SLIDE_PATTERN_ACTION_ID_BASE + 0; } }
	public int MAIMAI_NOTE_SLIDE_PATTERN_ACTION_ID_INNER_BASE { get { return MAIMAI_NOTE_SLIDE_PATTERN_ACTION_ID_BASE + 1; } }
	public int MAIMAI_NOTE_SLIDE_PATTERN_ACTION_ID_OUTER_BASE { get { return MAIMAI_NOTE_SLIDE_PATTERN_ACTION_ID_BASE + 9; } }
	/// <summary>スライドの準備完了用ノートのActionId</summary>
	public int MAIMAI_NOTE_SLIDE_HEAD_ACTION_ID { get { return 300; } }
	/// <summary>スライド移動開始音用ノートのActionId</summary>
	public int MAIMAI_NOTE_SLIDE_SOUND_ACTION_ID { get { return 400; } }
	/// <summary>その他判定を要しないノートのActionId</summary>
	public int MAIMAI_NOTE_OTHER_ACTION_ID { get { return 1000; } }
	/// <summary>最初の拍カウントは、ゲーム開始から何ミリ秒後に始めるか</summary>
	public long START_COUNT_START_TIME { get { return 500L; } }
	/// <summary>ワイドスライドのマーカー分割数</summary>
	public int WIDE_SLIDE_DEVIDE_AMOUNT { get { return 11; } }
	/// <summary>ワイドスライドマーカーのテクスチャサイズ</summary>
	public int WIDE_SLIDE_MARKER_TEXTURE_SIZE { get { return 12; } }
	/// <summary>スライド評価テクスチャの幅サイズ</summary>
	public int SLIDE_EVALUATE_TEXTURE_WIDTH { get { return 96; } }
	/// <summary>スライド評価テクスチャの高さサイズ</summary>
	public int SLIDE_EVALUATE_TEXTURE_HEIGHT { get { return 32; } }
	/// <summary>シンクモードのバージョン番号</summary>
	public int SYNC_MODE_VERSION { get { return 1; } }


	/// <summary>
	/// Unityは左手座標系であるのでCanvas等の右手座標系との互換.
	/// </summary>
	public float LEFT_HANDED_COORDINATE_SYSTEM { get { return -1; } }
	/// <summary>
	/// 右手座標系の位置を左手座標系の位置に変換する.
	/// </summary>
	public Vector3 ToLeftHandedCoordinateSystemPosition(Vector3 rightHandedCoordinateSystemPosition) {
		var ret = rightHandedCoordinateSystemPosition;
		ret.y *= LEFT_HANDED_COORDINATE_SYSTEM;
		return ret;
	}
	/// <summary>
	/// 右手座標系の回転値を左手座標系の回転値に変換する.
	/// </summary>
	public Vector3 ToLeftHandedCoordinateSystemRotation(Vector3 rightHandedCoordinateSystemRotation) {
		var ret = rightHandedCoordinateSystemRotation;
		ret.z *= LEFT_HANDED_COORDINATE_SYSTEM;
		return ret;
	}
	/// <summary>
	/// 右手座標系の角度を左手座標系の角度に変換する.
	/// </summary>
	public float ToLeftHandedCoordinateSystemDegree(float rightHandedCoordinateSystemDegree) {
		var ret = rightHandedCoordinateSystemDegree;
		ret *= LEFT_HANDED_COORDINATE_SYSTEM;
		return ret;
	}
		
	//内周センサーのノート基準値
	public float MAIMAI_INNER_RADIUS {
		get {
			var clossExLines = CircleCalculator.LinesIntersect(GetOuterPieceAxis(0), GetOuterPieceAxis(3), GetOuterPieceAxis(6), GetOuterPieceAxis(2)); //1-4と7-3の交点
			return CircleCalculator.PointToPointDistance(Vector2.zero, clossExLines); //中心から交点までの距離を内周円上点の半径とする
		}
	}
	
	//中心からみたセンサーピースの角度 (上が0度)
	public float GetPieceDegree(int buttonNumber) {
		float numberOfSensor = 8.0f;
		return ((360.0f / numberOfSensor) * buttonNumber + (360.0f / numberOfSensor / 2.0f));
	}

	//外周センサーピース位置
	public Vector2 GetOuterPieceAxis(int buttonNumber) {
		return GetOuterPieceAxis(Vector2.zero, buttonNumber);
	}
	public Vector2 GetOuterPieceAxis(Vector2 center, int buttonNumber) {
		return CircleCalculator.PointOnCircle(center, MAIMAI_OUTER_RADIUS, GetPieceDegree(buttonNumber));
	}
	
	//内周センサーピース位置
	public Vector2 GetInnerPieceAxis(int buttonNumber) {
		return GetInnerPieceAxis(Vector2.zero, buttonNumber);
	}
	public Vector2 GetInnerPieceAxis(Vector2 center, int buttonNumber) {
		return CircleCalculator.PointOnCircle(center, MAIMAI_INNER_RADIUS, GetPieceDegree(buttonNumber));
	}
	
	public enum SensorId {
		OUTER_1, OUTER_2, OUTER_3, OUTER_4, OUTER_5, OUTER_6, OUTER_7, OUTER_8,
		INNER_1, INNER_2, INNER_3, INNER_4, INNER_5, INNER_6, INNER_7, INNER_8,
		CENTER,
	}
	public static readonly SensorId[] OuterSensorIds = {
		SensorId.OUTER_1, SensorId.OUTER_2, SensorId.OUTER_3, SensorId.OUTER_4, SensorId.OUTER_5, SensorId.OUTER_6, SensorId.OUTER_7, SensorId.OUTER_8
	};
	public static readonly SensorId[] InnerSensorIds = {
		SensorId.INNER_1, SensorId.INNER_2, SensorId.INNER_3, SensorId.INNER_4, SensorId.INNER_5, SensorId.INNER_6, SensorId.INNER_7, SensorId.INNER_8
	};
	public Vector2 GetSensorAxisFromId(SensorId sensorId) {
		return GetSensorAxisFromId (Vector2.zero, sensorId);
	}
	public Vector2 GetSensorAxisFromId(Vector2 center, SensorId sensorId) {
		switch (sensorId) {
		case SensorId.OUTER_1:
			return GetOuterPieceAxis(center, 0);
		case SensorId.OUTER_2:
			return GetOuterPieceAxis(center, 1);
		case SensorId.OUTER_3:
			return GetOuterPieceAxis(center, 2);
		case SensorId.OUTER_4:
			return GetOuterPieceAxis(center, 3);
		case SensorId.OUTER_5:
			return GetOuterPieceAxis(center, 4);
		case SensorId.OUTER_6:
			return GetOuterPieceAxis(center, 5);
		case SensorId.OUTER_7:
			return GetOuterPieceAxis(center, 6);
		case SensorId.OUTER_8:
			return GetOuterPieceAxis(center, 7);
		case SensorId.INNER_1:
			return GetInnerPieceAxis(center, 0);
		case SensorId.INNER_2:
			return GetInnerPieceAxis(center, 1);
		case SensorId.INNER_3:
			return GetInnerPieceAxis(center, 2);
		case SensorId.INNER_4:
			return GetInnerPieceAxis(center, 3);
		case SensorId.INNER_5:
			return GetInnerPieceAxis(center, 4);
		case SensorId.INNER_6:
			return GetInnerPieceAxis(center, 5);
		case SensorId.INNER_7:
			return GetInnerPieceAxis(center, 6);
		case SensorId.INNER_8:
			return GetInnerPieceAxis(center, 7);
		case SensorId.CENTER:
			return Vector2.zero;
		}
		return Vector2.zero;
	}
	public int GetSlideActionId (SensorId sensorId) {
		//アクションIDは、ファクトリー初期値 + センサー番号。中円は0、内周は1～8、外周は9～16
		switch (sensorId) {
		case SensorId.OUTER_1:
			return MAIMAI_NOTE_SLIDE_PATTERN_ACTION_ID_OUTER_BASE + 0;
		case SensorId.OUTER_2:
			return MAIMAI_NOTE_SLIDE_PATTERN_ACTION_ID_OUTER_BASE + 1;
		case SensorId.OUTER_3:
			return MAIMAI_NOTE_SLIDE_PATTERN_ACTION_ID_OUTER_BASE + 2;
		case SensorId.OUTER_4:
			return MAIMAI_NOTE_SLIDE_PATTERN_ACTION_ID_OUTER_BASE + 3;
		case SensorId.OUTER_5:
			return MAIMAI_NOTE_SLIDE_PATTERN_ACTION_ID_OUTER_BASE + 4;
		case SensorId.OUTER_6:
			return MAIMAI_NOTE_SLIDE_PATTERN_ACTION_ID_OUTER_BASE + 5;
		case SensorId.OUTER_7:
			return MAIMAI_NOTE_SLIDE_PATTERN_ACTION_ID_OUTER_BASE + 6;
		case SensorId.OUTER_8:
			return MAIMAI_NOTE_SLIDE_PATTERN_ACTION_ID_OUTER_BASE + 7;
		case SensorId.INNER_1:
			return MAIMAI_NOTE_SLIDE_PATTERN_ACTION_ID_INNER_BASE + 0;
		case SensorId.INNER_2:
			return MAIMAI_NOTE_SLIDE_PATTERN_ACTION_ID_INNER_BASE + 1;
		case SensorId.INNER_3:
			return MAIMAI_NOTE_SLIDE_PATTERN_ACTION_ID_INNER_BASE + 2;
		case SensorId.INNER_4:
			return MAIMAI_NOTE_SLIDE_PATTERN_ACTION_ID_INNER_BASE + 3;
		case SensorId.INNER_5:
			return MAIMAI_NOTE_SLIDE_PATTERN_ACTION_ID_INNER_BASE + 4;
		case SensorId.INNER_6:
			return MAIMAI_NOTE_SLIDE_PATTERN_ACTION_ID_INNER_BASE + 5;
		case SensorId.INNER_7:
			return MAIMAI_NOTE_SLIDE_PATTERN_ACTION_ID_INNER_BASE + 6;
		case SensorId.INNER_8:
			return MAIMAI_NOTE_SLIDE_PATTERN_ACTION_ID_INNER_BASE + 7;
		case SensorId.CENTER:
			return MAIMAI_NOTE_SLIDE_PATTERN_ACTION_ID_CENTER;
		}
		return -1;
	}
	
	public enum Circumference {
		OUTER, INNER, CENTER,
	};

	public Circumference SensorCircumference(SensorId sensorId) {
		switch (sensorId) {
		case SensorId.OUTER_1:
		case SensorId.OUTER_2:
		case SensorId.OUTER_3:
		case SensorId.OUTER_4:
		case SensorId.OUTER_5:
		case SensorId.OUTER_6:
		case SensorId.OUTER_7:
		case SensorId.OUTER_8:
			return Circumference.OUTER;
		case SensorId.INNER_1:
		case SensorId.INNER_2:
		case SensorId.INNER_3:
		case SensorId.INNER_4:
		case SensorId.INNER_5:
		case SensorId.INNER_6:
		case SensorId.INNER_7:
		case SensorId.INNER_8:
			return Circumference.INNER;
		case SensorId.CENTER:
			return Circumference.CENTER;
		}
		throw new System.Exception("unknown type.");
	}

	public float GetSensorDegFromId(SensorId sensorId) {
		switch (sensorId) {
		case SensorId.OUTER_1:
		case SensorId.INNER_1:
			return GetPieceDegree(0);
		case SensorId.OUTER_2:
		case SensorId.INNER_2:
			return GetPieceDegree(1);
		case SensorId.OUTER_3:
		case SensorId.INNER_3:
			return GetPieceDegree(2);
		case SensorId.OUTER_4:
		case SensorId.INNER_4:
			return GetPieceDegree(3);
		case SensorId.OUTER_5:
		case SensorId.INNER_5:
			return GetPieceDegree(4);
		case SensorId.OUTER_6:
		case SensorId.INNER_6:
			return GetPieceDegree(5);
		case SensorId.OUTER_7:
		case SensorId.INNER_7:
			return GetPieceDegree(6);
		case SensorId.OUTER_8:
		case SensorId.INNER_8:
			return GetPieceDegree(7);
		case SensorId.CENTER:
			return 0;
		}
		return 0;
	}

	public enum Language {
		Extra, JapaneseDefault, JapaneseOnly, JapaneseHiragana, English, 
	}
	
}