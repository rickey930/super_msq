﻿using UnityEngine;
using System.Collections;

public class MaimaiSensor : GameVirtualHardwareButton {
	public CircleCollider2D GetSensorCollider() {
		return (CircleCollider2D)buttonCollider;
	}

	public void Setup(Vector2 pos, float deg) {
		var col = GetSensorCollider();
		col.radius = Constants.instance.SENSOR_RADIUS;
		col.center = CircleCalculator.PointOnCircle(Vector2.zero, -pos.y, Constants.instance.ToLeftHandedCoordinateSystemDegree(deg));
	}

	public void SetupButtonNames(params string[] names) {
		if (names != null) {
			copeGamePadButtonNames = names;
		}
	}
}
