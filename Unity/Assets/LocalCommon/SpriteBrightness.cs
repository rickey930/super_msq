﻿using UnityEngine;
using System.Collections;

public class SpriteBrightness : MonoBehaviour {
	// Use this for initialization
	void Start () {
		var renderer = GetComponent<SpriteRenderer> ();
		if (renderer != null) {
			var color = renderer.color;
			color.a = 1.0f - UserDataController.instance.config.brightness;
			renderer.color = color;
		}
	}
}
