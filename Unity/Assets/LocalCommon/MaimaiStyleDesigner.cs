using UnityEngine;
using System.Collections;

public static class MaimaiStyleDesigner {
	public enum NoteType {
		TAP_CIRCLE, TAP_STAR, HOLD_HEAD, HOLD_FOOT, BREAK_CIRCLE, BREAK_STAR, SLIDE, 
	}

	public static Color ByteToPercentRGBA(int r, int g, int b, int a) {
		float _r = (float)r / 255.0f;
		float _g = (float)g / 255.0f;
		float _b = (float)b / 255.0f;
		float _a = (float)a / 255.0f;
		return new Color (_r, _g, _b, _a);
	}
	public static Color ByteToPercentARGB(int a, int r, int g, int b) {
		return ByteToPercentRGBA (r, g, b, a);
	}

	/// <summary>
	/// レベル別カラー設定.
	/// </summary>
	public static Color GetLevelColor(int difficulty) {
		switch (difficulty) {
		case 0:
			return ByteToPercentRGBA(0,0,255,255); //Color.Blue;
		case 1:
			return ByteToPercentRGBA(0,140,0,255); //Color.FromArgb(0, 140, 0);
		case 2:
			return ByteToPercentRGBA(255,128,0,255); //Color.FromArgb(255, 128, 0);
		case 3:
			return ByteToPercentRGBA(255,0,0,255); //Color.Red;
		case 4:
		case 5:
			return ByteToPercentRGBA(117,21,234,255); //Color.FromArgb(117, 21, 234);
		case 6:
			return ByteToPercentRGBA(255,128,255,255); //Color.Pink;
		default:
			return ByteToPercentRGBA(180,180,180,255); //Color.Gray;
		}
	}

	/// <summary>
	/// レベル別カラー設定.
	/// </summary>
	public static Color GetLevelColor(string difficulty) {
		int diffnum = -1;
		string diffstr = difficulty.ToLower ();
		switch (diffstr) {
		case "easy":
			diffnum = 0;
			break;
		case "basic":
			diffnum = 1;
			break;
		case "advanced":
			diffnum = 2;
			break;
		case "expert":
			diffnum = 3;
			break;
		case "master":
			diffnum = 4;
			break;
		case "re_master":
		case "re:master":
		case "remaster":
		case "re master":
			diffnum = 5;
			break;
		case "utage":
			diffnum = 6;
			break;
		}
		return GetLevelColor (diffnum);
	}

	/// <summary>
	/// 達成率別カラー設定.
	/// </summary>
	public static Color GetAchievementColor(float percent) {
		if (percent >= 100.0)
			return ByteToPercentRGBA(254,237,176,255); //Color.FromArgb(0xFE, 0xED, 0xB0); //白金 FEEDB0
		else if (percent >= 94.0) //if (percent >= 85.0f)
			return ByteToPercentRGBA(230,180,34,255); //Color.FromArgb(0xE6, 0xB4, 0x22); //金 e6b422
		else if (percent >= 80.0) //if (percent >= 55.0f)
			return ByteToPercentRGBA(192,192,192,255); //Color.FromArgb(0xC0, 0xC0, 0xC0); //銀
		else
			return ByteToPercentRGBA(184,115,51,255); //Color.FromArgb(0xB8, 0x73, 0x33); //銅 b87333
	}

	/// <summary>
	/// シンク度別カラー設定.
	/// </summary>
	public static Color GetSyncRateColor(float syncRate) {
		if (syncRate >= 100)
			return ByteToPercentRGBA(254,237,176,255); //Color.FromArgb(0xFE, 0xED, 0xB0); //白金 FEEDB0
		else if (syncRate >= 90) //if (percent >= 85.0f)
			return ByteToPercentRGBA(230,180,34,255); //Color.FromArgb(0xE6, 0xB4, 0x22); //金 e6b422
		else if (syncRate >= 70) //if (percent >= 55.0f)
			return ByteToPercentRGBA(192,192,192,255); //Color.FromArgb(0xC0, 0xC0, 0xC0); //銀
		else
			return ByteToPercentRGBA(184,115,51,255); //Color.FromArgb(0xB8, 0x73, 0x33); //銅 b87333
	}

	/// <summary>
	/// コンボ文字のカラー.
	/// </summary>
	public static Color GetComboColor() {
		return ByteToPercentRGBA(255,0,0,255);
	}

	/// <summary>
	/// パーフェクトコンボ文字のカラー.
	/// </summary>
	public static Color GetPComboColor() {
		return ByteToPercentRGBA(255,200,0,255);
	}
	
	/// <summary>
	/// ブレークパーフェクトコンボ文字のカラー.
	/// </summary>
	public static Color GetBPComboColor() {
		return ByteToPercentRGBA(200,180,128,255);
	}
	
	/// <summary>
	/// スコア文字のカラー.
	/// </summary>
	public static Color GetScoreColor() {
		return ByteToPercentRGBA(15,200,10,255); //Color.FromArgb(15, 200, 10);
	}

	/// <summary>
	/// 理論値スコア文字のカラー.
	/// </summary>
	public static Color GetFullScoreColor() {
		return ByteToPercentRGBA(255,128,0,255);
	}
	
	/// <summary>
	/// シンクの基本カラー.
	/// </summary>
	public static Color GetSyncColor() {
		return ByteToPercentRGBA(230,15,255,255);
	}
	
	/// <summary>
	/// ノートの円弧のカラー.
	/// </summary>
	public static Color GetNoteArcColor(NoteType type, bool isEach) {
		if (isEach && type != NoteType.BREAK_CIRCLE && type != NoteType.BREAK_STAR)
			return ByteToPercentRGBA (255, 255, 0, 255);
		switch (type) {
		case NoteType.TAP_CIRCLE:
		case NoteType.HOLD_HEAD:
			return ByteToPercentRGBA(255,175,200,255);
		case NoteType.TAP_STAR:
			return ByteToPercentRGBA(0,255,255,255);
		case NoteType.BREAK_CIRCLE:
		case NoteType.BREAK_STAR:
			return ByteToPercentRGBA(255,0,0,255);
		default:
			return ByteToPercentRGBA(0,0,0,0);
		}
	}
	
	/// <summary>
	/// 判定カラー.
	/// </summary>
	public static Color GetJudgementColor(int type) {
		switch (type) {
		case 0:
			return ByteToPercentRGBA(255,255,0,255);
		case 1:
			return ByteToPercentRGBA(255,175,200,255);
		case 2:
			return ByteToPercentRGBA(0,128,0,255);
		default:
			return ByteToPercentRGBA(0,0,0,0);
		}
	}
	
	/// <summary>
	/// ランク文字列.
	/// </summary>
	public static string GetRankString(float achievement) {
		if (achievement < 10.0)
			return "F";
		else if (achievement < 20.0)
			return "E";
		else if (achievement < 40.0)
			return "D";
		else if (achievement < 60.0)
			return "C";
		else if (achievement < 80.0)
			return "B";
		else if (achievement < 90.0)
			return "A";
		else if (achievement < 94.0)
			return "AA";
		else if (achievement < 97.0)
			return "AAA";
		else if (achievement < 99.0)
			return "S";
		else if (achievement < 100.0)
			return "SS";
		else
			return "SSS";
	}
	
	/// <summary>
	/// ランク文字列.
	/// </summary>
	public static string GetRankStringClassic(float achievement) {
		if (achievement < 20.0)
			return "D";
		else if (achievement < 55.0)
			return "C";
		else if (achievement < 65.0)
			return "B-";
		else if (achievement < 75.0)
			return "B";
		else if (achievement < 80.0)
			return "B+";
		else if (achievement < 85.0)
			return "A-";
		else if (achievement < 90.0)
			return "A";
		else if (achievement < 94.0)
			return "A+";
		else if (achievement < 97.0)
			return "AA";
		else if (achievement < 100.0)
			return "S";
		else
			return "SS";
	}

	/// <summary>
	/// ランク文字列.
	/// </summary>
	public static Sprite GetRankSprite(float achievement) {
		if (achievement < 10.0)
			return SpriteTank.Get ("rank_pink_0");
		else if (achievement < 20.0)
			return SpriteTank.Get ("rank_pink_1");
		else if (achievement < 40.0)
			return SpriteTank.Get ("rank_pink_2");
		else if (achievement < 60.0)
			return SpriteTank.Get ("rank_pink_3");
		else if (achievement < 80.0)
			return SpriteTank.Get ("rank_pink_4");
		else if (achievement < 90.0)
			return SpriteTank.Get ("rank_pink_5");
		else if (achievement < 94.0)
			return SpriteTank.Get ("rank_pink_6");
		else if (achievement < 97.0)
			return SpriteTank.Get ("rank_pink_7");
		else if (achievement < 99.0)
			return SpriteTank.Get ("rank_pink_8");
		else if (achievement < 100.0)
			return SpriteTank.Get ("rank_pink_9");
		else
			return SpriteTank.Get ("rank_pink_10");
	}
	
	/// <summary>
	/// ランク文字列.
	/// </summary>
	public static Sprite GetRankSpriteClassic(float achievement) {
		if (achievement < 20.0)
			return SpriteTank.Get ("rank_classic_0");
		else if (achievement < 55.0)
			return SpriteTank.Get ("rank_classic_1");
		else if (achievement < 65.0)
			return SpriteTank.Get ("rank_classic_2");
		else if (achievement < 75.0)
			return SpriteTank.Get ("rank_classic_3");
		else if (achievement < 80.0)
			return SpriteTank.Get ("rank_classic_4");
		else if (achievement < 85.0)
			return SpriteTank.Get ("rank_classic_5");
		else if (achievement < 90.0)
			return SpriteTank.Get ("rank_classic_6");
		else if (achievement < 94.0)
			return SpriteTank.Get ("rank_classic_7");
		else if (achievement < 97.0)
			return SpriteTank.Get ("rank_classic_8");
		else if (achievement < 100.0)
			return SpriteTank.Get ("rank_classic_9");
		else
			return SpriteTank.Get ("rank_classic_10");
	}

	/// <summary>
	/// クリア評価文字列.
	/// </summary>
	public static string GetClearEvaluateResultString(float achievement) {
		if (achievement < 80.0)
			return "FAILED...";
		else if (achievement < 94.0)
			return "CLEAR!!";
		else if (achievement < 97.0)
			return "GREAT CLEAR!!";
		else if (achievement < 100.0)
			return "EXCELLENT CLEAR!!";
		else
			return "FANTASTIC CLEAR!!";
	}

	/// <summary>
	/// クリア評価文字列.
	/// </summary>
	public static string GetClearEvaluateString(float achievement) {
		if (achievement < 80.0)
			return "FAILED";
		else if (achievement < 94.0)
			return "CLEAR";
		else if (achievement < 97.0)
			return "GREAT";
		else if (achievement < 100.0)
			return "EXCELLENT";
		else
			return "FANTASTIC";
	}

	/// <summary>
	/// クリア評価色.
	/// </summary>
	public static Color GetClearEvaluateColor(float achievement) {
		if (achievement < 80.0)
			return ByteToPercentRGBA(192,192,192,255);
		else if (achievement < 94.0)
			return ByteToPercentRGBA(184,115,51,255);
		else if (achievement < 97.0)
			return ByteToPercentRGBA(230,180,34,255);
		else if (achievement < 100.0)
			return ByteToPercentRGBA(255,128,0,255);
		else
			return ByteToPercentRGBA(225,128,210,255);
	}
	
	/// <summary>
	/// シンクランク文字列.
	/// </summary>
	public static string GetSyncRankString(float syncRate) {
		if (syncRate < 30)
			return "BAD";
		else if (syncRate < 50)
			return "AVERAGE";
		else if (syncRate < 70)
			return "GOOD";
		else if (syncRate < 80)
			return "GREAT";
		else if (syncRate < 90)
			return "EXCELLENT";
		else if (syncRate < 95)
			return "AMAZING";
		else if (syncRate < 97)
			return "MIRACLE";
		else if (syncRate < 100)
			return "ULTIMATE";
		else
			return "PERFECT";
	}

	public static Sprite GetGuideCircleSprite(string difficulty) {
		var tank = MaipadDynamicCreatedSpriteTank.instance;
		if (tank == null || string.IsNullOrEmpty (difficulty))
			return null;
		switch (difficulty.ToLower()) {
		case "easy":
			return tank.guideCircle1;
		case "basic":
			return tank.guideCircle2;
		case "advanced":
			return tank.guideCircle3;
		case "expert":
			return tank.guideCircle4;
		case "master":
		case "re:master":
		case "re_master":
		case "remaster":
		case "re master":
			return tank.guideCircle5;
		default:
			return tank.guideCircle6;
		}
	}
	
	public static Sprite GetDifficultyOctagonSprite(string difficulty) {
		if (string.IsNullOrEmpty (difficulty)) {
			return SpriteTank.Get("icon_difficulty_default");
		}

		switch (difficulty.ToLower()) {
		case "easy":
		case "basic":
		case "advanced":
		case "expert":
		case "master":
			return SpriteTank.Get("icon_difficulty_" + difficulty.ToLower());
		case "re:master":
		case "re_master":
		case "remaster":
		case "re master":
			return SpriteTank.Get("icon_difficulty_remaster");
		default:
			return SpriteTank.Get("icon_difficulty_default");
		}
	}

	public static Color GetLifeDifficultyColor(ConfigTypes.LifeDifficulty lifeDif) {
		switch (lifeDif) {
		case ConfigTypes.LifeDifficulty.LIFE_1:
			return GetLevelColor(4);
		case ConfigTypes.LifeDifficulty.LIFE_5:
		case ConfigTypes.LifeDifficulty.LIFE_10:
			return GetLevelColor(3);
		case ConfigTypes.LifeDifficulty.LIFE_20:
		case ConfigTypes.LifeDifficulty.LIFE_50:
		case ConfigTypes.LifeDifficulty.LIFE_100:
			return GetLevelColor(2);
		case ConfigTypes.LifeDifficulty.LIFE_300:
			return GetLevelColor(1);
		default:
			return Color.clear;
		}
	}

	public static Color GetRemainLifeColor(ConfigTypes.LifeDifficulty lifeDif, int life) {
		float maxlife = 0;
		switch (lifeDif) {
		case ConfigTypes.LifeDifficulty.LIFE_1:
			maxlife = 1;
			break;
		case ConfigTypes.LifeDifficulty.LIFE_5:
			maxlife = 5;
			break;
		case ConfigTypes.LifeDifficulty.LIFE_10:
			maxlife = 10;
			break;
		case ConfigTypes.LifeDifficulty.LIFE_20:
			maxlife = 20;
			break;
		case ConfigTypes.LifeDifficulty.LIFE_50:
			maxlife = 50;
			break;
		case ConfigTypes.LifeDifficulty.LIFE_100:
			maxlife = 100;
			break;
		case ConfigTypes.LifeDifficulty.LIFE_300:
			maxlife = 300;
			break;
		default:
			return Color.clear;
		}
		float percent = life / maxlife;
		if (percent > 0.5f) {
			return Color.white;
		}
		else if (percent > 0.25f) {
			return Color.yellow;
		}
		else if (percent > 0f) {
			return Color.red;
		}
		return Color.gray;
	}

}
