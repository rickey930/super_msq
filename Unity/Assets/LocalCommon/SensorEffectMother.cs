using UnityEngine;
using System.Collections;

public class SensorEffectMother : MonoBehaviour {
	
	[SerializeField]
	private RectTransform canvas;
	[SerializeField]
	private GameObject buttonEffectPrefab;
	[SerializeField]
	public int bufferSize;

	private SensorEffectController[] ctrls { get; set; }
	private int index { get; set; }
	private int size { get; set; }

	void Start () {
		index = 0;
		size = bufferSize;
		ctrls = new SensorEffectController[size];
		for (int i = 0; i < size; i++) {
			var obj = Instantiate(buttonEffectPrefab) as GameObject;
			obj.SetParentEx(canvas);
			ctrls[i] = obj.GetComponent<SensorEffectController>();
			obj.SetActive(false);
		}
	}

	public void Birth(int buttonNum) {
		var ctrl = ctrls[index];
		ctrl.Restart();
		ctrl.SetPos(buttonNum);
		ctrl.Show();
		index = (index + 1) % size;
	}
	
	public void Birth() {
		var ctrl = ctrls[index];
		ctrl.Restart();
		ctrl.SetPos(Vector2.zero);
		ctrl.Show();
		index = (index + 1) % size;
	}
}
