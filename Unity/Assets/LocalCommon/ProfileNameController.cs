﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ProfileNameController : Text {
	IEnumerator StartCustomCoroutine () {
		while (ProfileInformationController.instance == null) {
			yield return null;
		}
		this.text = ProfileInformationController.instance.player_name;
		ProfileInformationController.instance.profileNameCtrl = this;
	}

	public void StartCustom () {
		StartCoroutine (StartCustomCoroutine ());
	}
	
	protected override void Start () {
		base.Start ();
		StartCustom ();
	}
}
