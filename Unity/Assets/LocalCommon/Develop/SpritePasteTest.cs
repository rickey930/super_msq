﻿using UnityEngine;
using System.Collections;

public class SpritePasteTest : MonoBehaviour {

	// Use this for initialization
	void Start () {
		int size = ((Constants.instance.MAIMAI_OUTER_RADIUS + 5) * 2).ToInt ();
		GetComponent<SpriteRenderer> ().sprite = SpriteCreator.Create (size, size, (Texture2D canvas) => { 
			//canvas.DrawRect(0, 0, 100, 100, Color.red, 1);
			//canvas.FillRect(0, 0, 100, 100, Color.red);
			//canvas.DrawCircle(192, 256, 100, Color.red, 5);
			//canvas.FillCircle(192, 256, 100, Color.red);
			//canvas.DrawLine(Vector2.zero, new Vector2(384, 512), Color.red, 5);
			//canvas.DrawArc(192, 256, 100, 90, 90, Color.red, 5);
			//canvas.FillArc(192, 256, 100, 0, 180, Color.red);
			Vector2 center = canvas.GetCenter();
			canvas.DrawCircle(center, Constants.instance.MAIMAI_OUTER_RADIUS, Color.red, 2);
			for (int i = 0; i < 8; i++) {
				Vector2 pos = Constants.instance.GetOuterPieceAxis(center, i);
				canvas.FillCircle(pos, 4, Color.red);
			}
			canvas.DrawCircle(center, Constants.instance.MAIMAI_INNER_RADIUS, Color.red, 2);
			for (int i = 0; i < 8; i++) {
				Vector2 pos = Constants.instance.GetInnerPieceAxis(center, i);
				canvas.FillCircle(pos, 4, Color.red);
			}
		}, "Maimai Circle");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
