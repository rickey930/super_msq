﻿using UnityEngine;
using System.Collections;

public class CreateMeshTest : MonoBehaviour {

	// Use this for initialization
	void Start () {
		CreateAchievementBarMesh ();
	}


	public static Mesh CreateAchievementBarMesh () {
		// メッシュに使う画像のサイズ.
		float width = 0.0f;
		float height = 0.0f;
		// 横方向の頂点数.
		int holizontalVertexAmount = 15;
		// メッシュに使う画像のpovit (0.5が中心)
		float centerXRate = 0.0f;
		float centerYRate = 0.5f;
		
		// 回転方向 (メッシュを作るときに(つまりここで)いじるべきではない)
		float vec = 1;
		
		const int verticalVertexAmount = 2;
		
		Mesh mesh = new Mesh();
		Vector3 [] newVertices = new Vector3[holizontalVertexAmount * verticalVertexAmount];
		Vector2 [] newUV       = new Vector2[holizontalVertexAmount * verticalVertexAmount];
		// 三角形の頂点数は3で、四角形を作るには三角形が2つ必要で、四角形が何個あるかで掛け算する.
		// 四角形用の頂点が2*2で三角形用の頂点が6個. 3*2で12個. 4*2で18個. 3*3で24個. 4*4で36個
		// 頂点数-1個の四角形が作れる.
		int[] newTriangles     = new int[3 * 2 * (holizontalVertexAmount - 1) * (verticalVertexAmount - 1)];
		
		// 鏡コの字を書くように頂点を配置する.
		
		// 頂点とUVを作成.
		for (int h = 0; h < holizontalVertexAmount; h++) {
			float progress = (float)h / (float)(holizontalVertexAmount - 1);
			int index = h;
			newVertices[index] = new Vector3(((width * progress) + (width * centerXRate)) * vec, -(height * centerYRate)).ChangePosHandSystem();
			// vecがマイナスのとき文字が鏡になるのを避けるためuvのy値を反転して回転しているように見せているけど別の画像用意すべきかも.
			newUV[index] = new Vector2((progress), 0).ChangeUVHandSystem();
		}
		for (int h = holizontalVertexAmount - 1; h >= 0; h--) {
			float progress = (float)h / (float)(holizontalVertexAmount - 1);
			int index = (holizontalVertexAmount - 1) + (verticalVertexAmount - 1) + (holizontalVertexAmount - 1 - h);
			newVertices[index] = new Vector3(((width * progress) + (width * centerXRate)) * vec, height - (height * centerYRate)).ChangePosHandSystem();
			// vecがマイナスのとき文字が鏡になるのを避けるためuvのy値を反転して回転しているように見せているけど別の画像用意すべきかも.
			newUV[index] = new Vector2((progress), 1).ChangeUVHandSystem();
		}
		// 三角形に頂点インデックス割り当て.
		
		// ※【ここはverticalVertexAmountが2固定である前提で作るよ】.
		// 0, 1, newVertices.Length-1
		// 0, newVertices.Length-1, newVertices.Length-2
		// 0～は次のループで+1, newVertices.Length系は次のループで-1
		// newVertices.Length-2 == holizontalVertexAmount で終了.
		int triangleIndex = 0;
		for (int i = 0; i < holizontalVertexAmount - 1; i++) {
			newTriangles[triangleIndex + 0] = i + 0;
			newTriangles[triangleIndex + 1] = i + 1;
			newTriangles[triangleIndex + 2] = newVertices.Length - 2 - i;
			newTriangles[triangleIndex + 3] = i + 0;
			newTriangles[triangleIndex + 4] = newVertices.Length - 1 - i;
			newTriangles[triangleIndex + 5] = newVertices.Length - 2 - i;
			triangleIndex += 6;
		}
		
		mesh.vertices  = newVertices;
		mesh.uv        = newUV;
		mesh.triangles = newTriangles;
		mesh.name = "AchievementBarMesh";
		
		mesh.RecalculateNormals();
		mesh.RecalculateBounds();
		
		CurveAchievementBarMesh(mesh);

		return mesh;
	}
	
	static void CurveAchievementBarMesh (Mesh mesh) {
		float height = 16.0f;
		float centerYRate = 0.5f;

		float vec = -1;
		float distanceDeg = 32.0f;
		float axisDeg = -distanceDeg / 2;
		float radius = 180;

		Vector3[] vertices = mesh.vertices;
		for (int i = 0; i < vertices.Length / 2; i++) {
			float progress = (float)i / (float)(vertices.Length / 2 - 1);
			// 上.
			var up = CircleCalculator.PointOnCircle(Vector2.zero, radius +height-(height * centerYRate), axisDeg.ChangeDegHandSystem() + distanceDeg * vec * progress).ChangePosHandSystem();
			vertices[i].x = -up.x;
			vertices[i].y = up.y;
			// 下.
			var down = CircleCalculator.PointOnCircle(Vector2.zero, radius +0-(height * centerYRate), axisDeg.ChangeDegHandSystem() + distanceDeg * vec * progress).ChangePosHandSystem();
			vertices[vertices.Length - 1 - i].x = -down.x;
			vertices[vertices.Length - 1 - i].y = down.y;
		}
		mesh.vertices = vertices;
	}




















	public static Mesh CreateResultAchievementCircleMesh () {
		// メッシュに使う画像のサイズ.
		float width = 0.0f;
		float height = 0.0f;
		// 横方向の頂点数.
		int holizontalVertexAmount = 90;
		// メッシュに使う画像のpovit (0.5が中心)
		float centerXRate = 0.0f;
		float centerYRate = 0.5f;
		
		// 回転方向 (メッシュを作るときに(つまりここで)いじるべきではない)
		float vec = 1;
		
		const int verticalVertexAmount = 2;
		
		Mesh mesh = new Mesh();
		Vector3 [] newVertices = new Vector3[holizontalVertexAmount * verticalVertexAmount];
		Vector2 [] newUV       = new Vector2[holizontalVertexAmount * verticalVertexAmount];
		// 三角形の頂点数は3で、四角形を作るには三角形が2つ必要で、四角形が何個あるかで掛け算する.
		// 四角形用の頂点が2*2で三角形用の頂点が6個. 3*2で12個. 4*2で18個. 3*3で24個. 4*4で36個
		// 頂点数-1個の四角形が作れる.
		int[] newTriangles     = new int[3 * 2 * (holizontalVertexAmount - 1) * (verticalVertexAmount - 1)];
		
		// 鏡コの字を書くように頂点を配置する.
		
		// 頂点とUVを作成.
		for (int h = 0; h < holizontalVertexAmount; h++) {
			float progress = (float)h / (float)(holizontalVertexAmount - 1);
			int index = h;
			newVertices[index] = new Vector3(((width * progress) + (width * centerXRate)) * vec, -(height * centerYRate)).ChangePosHandSystem();
			newUV[index] = new Vector2((progress), 0).ChangeUVHandSystem();
		}
		for (int h = holizontalVertexAmount - 1; h >= 0; h--) {
			float progress = (float)h / (float)(holizontalVertexAmount - 1);
			int index = (holizontalVertexAmount - 1) + (verticalVertexAmount - 1) + (holizontalVertexAmount - 1 - h);
			newVertices[index] = new Vector3(((width * progress) + (width * centerXRate)) * vec, height - (height * centerYRate)).ChangePosHandSystem();
			// vecがマイナスのとき文字が鏡になるのを避けるためuvのy値を反転して回転しているように見せているけど別の画像用意すべきかも.
			newUV[index] = new Vector2((progress), 1).ChangeUVHandSystem();
		}
		// 三角形に頂点インデックス割り当て.
		
		// ※【ここはverticalVertexAmountが2固定である前提で作るよ】.
		// 0, 1, newVertices.Length-1
		// 0, newVertices.Length-1, newVertices.Length-2
		// 0～は次のループで+1, newVertices.Length系は次のループで-1
		// newVertices.Length-2 == holizontalVertexAmount で終了.
		int triangleIndex = 0;
		for (int i = 0; i < holizontalVertexAmount - 1; i++) {
			newTriangles[triangleIndex + 0] = i + 0;
			newTriangles[triangleIndex + 1] = i + 1;
			newTriangles[triangleIndex + 2] = newVertices.Length - 2 - i;
			newTriangles[triangleIndex + 3] = i + 0;
			newTriangles[triangleIndex + 4] = newVertices.Length - 1 - i;
			newTriangles[triangleIndex + 5] = newVertices.Length - 2 - i;
			triangleIndex += 6;
		}
		
		mesh.vertices  = newVertices;
		mesh.uv        = newUV;
		mesh.triangles = newTriangles;
		mesh.name = "ResultAchievementCircleMesh";
		
		mesh.RecalculateNormals();
		mesh.RecalculateBounds();
		
		CurveResultAchievementCircleMesh(mesh);

		return mesh;
	}

	static void CurveResultAchievementCircleMesh (Mesh mesh) {
		float height = 90.0f; //円の半径.
		float centerYRate = 0.5f;
		
		float axisDeg = 0;
		float vec = -1;
		float distanceDeg = 360.0f;
		float radius = 145; //穴の半径

		Vector3[] vertices = mesh.vertices;
		for (int i = 0; i < vertices.Length / 2; i++) {
			float progress = (float)i / (float)(vertices.Length / 2 - 1);
			// 上.
			var up = CircleCalculator.PointOnCircle(Vector2.zero, radius +height-(height * centerYRate), axisDeg.ChangeDegHandSystem() + distanceDeg * vec * progress).ChangePosHandSystem();
			vertices[i].x = -up.x;
			vertices[i].y = up.y;
			// 下.
			var down = CircleCalculator.PointOnCircle(Vector2.zero, radius +0-(height * centerYRate), axisDeg.ChangeDegHandSystem() + distanceDeg * vec * progress).ChangePosHandSystem();
			vertices[vertices.Length - 1 - i].x = -down.x;
			vertices[vertices.Length - 1 - i].y = down.y;
		}
		mesh.vertices = vertices;
	}









	public static Mesh CreateSlideEvaluateMesh () {
		// メッシュに使う画像のサイズ.
		float width = 96.0f;
		float height = 32.0f;
		// 横方向の頂点数.
		int holizontalVertexAmount = 10;
		// メッシュに使う画像のpovit (0.5が中心)
		float centerXRate = -0.2f;
		float centerYRate = 0.5f;
		
		// 回転方向 (メッシュを作るときに(つまりここで)いじるべきではない)
		float vec = 1;
		
		const int verticalVertexAmount = 2;
		
		Mesh mesh = new Mesh();
		Vector3 [] newVertices = new Vector3[holizontalVertexAmount * verticalVertexAmount];
		Vector2 [] newUV       = new Vector2[holizontalVertexAmount * verticalVertexAmount];
		// 三角形の頂点数は3で、四角形を作るには三角形が2つ必要で、四角形が何個あるかで掛け算する.
		// 四角形用の頂点が2*2で三角形用の頂点が6個. 3*2で12個. 4*2で18個. 3*3で24個. 4*4で36個
		// 頂点数-1個の四角形が作れる.
		int[] newTriangles     = new int[3 * 2 * (holizontalVertexAmount - 1) * (verticalVertexAmount - 1)];
		
		// 鏡コの字を書くように頂点を配置する.
		
		// 頂点とUVを作成.
		for (int h = 0; h < holizontalVertexAmount; h++) {
			float progress = (float)h / (float)(holizontalVertexAmount - 1);
			int index = h;
			newVertices[index] = new Vector3((1 - (width * progress) + (width * centerXRate)) * vec, -(height * centerYRate)).ChangePosHandSystem();
			// vecがマイナスのとき文字が鏡になるのを避けるためuvのy値を反転して回転しているように見せているけど別の画像用意すべきかも.
			newUV[index] = new Vector2((1 - progress), vec > 0 ? 0 : 1).ChangeUVHandSystem();
		}
		//		for (int v = 0; v < verticalVertexAmount - 1; v++) {
		//			float progress = (float)v / (float)(verticalVertexAmount - 1);
		//			int index = holizontalVertexAmount + v;
		//			newVertices[index] = new Vector3(width - (width * centerXRate), (height * progress) + (height * centerYRate)).ChangePosHandSystem();
		//			newUV[index] = new Vector2(1, progress).ChangeUVHandSystem();
		//		}
		for (int h = holizontalVertexAmount - 1; h >= 0; h--) {
			float progress = (float)h / (float)(holizontalVertexAmount - 1);
			int index = (holizontalVertexAmount - 1) + (verticalVertexAmount - 1) + (holizontalVertexAmount - 1 - h);
			newVertices[index] = new Vector3((1 - (width * progress) + (width * centerXRate)) * vec, height - (height * centerYRate)).ChangePosHandSystem();
			// vecがマイナスのとき文字が鏡になるのを避けるためuvのy値を反転して回転しているように見せているけど別の画像用意すべきかも.
			newUV[index] = new Vector2((1 - progress), vec > 0 ? 1 : 0).ChangeUVHandSystem();
		}
		//		for (int v = 0; v < verticalVertexAmount - 2; v++) {
		//			float progress = (float)v / (float)(verticalVertexAmount);
		//			int index = (holizontalVertexAmount * 2 - 1) + (verticalVertexAmount - 1) + (verticalVertexAmount - 2 - v);
		//			newVertices[index] = new Vector3(-(width * centerXRate), (height * progress) + (height * centerYRate)).ChangePosHandSystem();
		//			newUV[index] = new Vector2(0, progress).ChangeUVHandSystem();
		//		}
		// 三角形に頂点インデックス割り当て.
		
		// ※【ここはverticalVertexAmountが2固定である前提で作るよ】.
		// 0, 1, newVertices.Length-1
		// 0, newVertices.Length-1, newVertices.Length-2
		// 0～は次のループで+1, newVertices.Length系は次のループで-1
		// newVertices.Length-2 == holizontalVertexAmount で終了.
		int triangleIndex = 0;
		for (int i = 0; i < holizontalVertexAmount - 1; i++) {
			newTriangles[triangleIndex + 0] = i + 1;
			newTriangles[triangleIndex + 1] = i + 0;
			newTriangles[triangleIndex + 2] = newVertices.Length - 2 - i;
			newTriangles[triangleIndex + 3] = i + 0;
			newTriangles[triangleIndex + 4] = newVertices.Length - 1 - i;
			newTriangles[triangleIndex + 5] = newVertices.Length - 2 - i;
			triangleIndex += 6;
		}
		
		mesh.vertices  = newVertices;
		mesh.uv        = newUV;
		mesh.triangles = newTriangles;
		mesh.name = "SlideEvaluaterMesh";
		
		mesh.RecalculateNormals();
		mesh.RecalculateBounds();
		
		//CurveSlideEvaluateMesh(mesh);

		return mesh;
	}

	static void CurveSlideEvaluateMesh (Mesh mesh) {
		float height = 32.0f;
		float centerYRate = 0.5f;

		float axisDeg = 0;//45.0f * 0 + 22.5f; //曲線スライドの中心から終点を見た角度を渡す.
		float vec = 1; //曲線スライドの方向(時計なら1/反時計なら(-1))を渡す.
		float distanceDeg = 45.0f; // 判定画像をどれだけ曲げるか.
		float radius = Constants.instance.MAIMAI_OUTER_RADIUS; //曲線スライドの半径.
		// このメッシュを使うゲームオブジェクトは曲線スライドの中心位置に移動させておく.
		// axisDegを0にする代わりにゲームオブジェクト自体を(曲線スライドのstartDeg + (曲線スライドのdistanceDeg * 曲線スライドのvec))で回していてもいいかも.


		// 半径レートを求めて判定画像のサイズを調整するのもアリではある.
		//float radiusRate = radius / Constants.instance.MAIMAI_OUTER_RADIUS;
		//height *= radiusRate;

		//Mesh mesh = GetComponent<MeshFilter>().mesh;
		Vector3[] vertices = mesh.vertices;
		for (int i = 0; i < vertices.Length / 2; i++) {
			float progress = (float)i / (float)(vertices.Length / 2 - 1);
			// 上.
			var up = CircleCalculator.PointOnCircle(Vector2.zero, radius +height-(height * centerYRate), axisDeg.ChangeDegHandSystem() + distanceDeg * vec * progress).ChangePosHandSystem();
			vertices[i].x = -up.x;
			vertices[i].y = up.y;
			// 下.
			var down = CircleCalculator.PointOnCircle(Vector2.zero, radius +0-(height * centerYRate), axisDeg.ChangeDegHandSystem() + distanceDeg * vec * progress).ChangePosHandSystem();
			vertices[vertices.Length - 1 - i].x = -down.x;
			vertices[vertices.Length - 1 - i].y = down.y;
		}
		mesh.vertices = vertices;
	}




	public static Mesh CreateCounterClockwiseSlideEvaluateMesh () {
		// メッシュに使う画像のサイズ.
		float width = 96.0f;
		float height = 32.0f;
		// 横方向の頂点数.
		int holizontalVertexAmount = 10;
		// メッシュに使う画像のpovit (0.5が中心)
		float centerXRate = -0.2f;
		float centerYRate = 0.5f;
		
		// 回転方向 (メッシュを作るときに(つまりここで)いじるべきではない)
		float vec = -1;
		
		const int verticalVertexAmount = 2;
		
		Mesh mesh = new Mesh();
		Vector3 [] newVertices = new Vector3[holizontalVertexAmount * verticalVertexAmount];
		Vector2 [] newUV       = new Vector2[holizontalVertexAmount * verticalVertexAmount];
		// 三角形の頂点数は3で、四角形を作るには三角形が2つ必要で、四角形が何個あるかで掛け算する.
		// 四角形用の頂点が2*2で三角形用の頂点が6個. 3*2で12個. 4*2で18個. 3*3で24個. 4*4で36個
		// 頂点数-1個の四角形が作れる.
		int[] newTriangles     = new int[3 * 2 * (holizontalVertexAmount - 1) * (verticalVertexAmount - 1)];
		
		// 鏡コの字を書くように頂点を配置する.
		
		// 頂点とUVを作成.
		for (int h = 0; h < holizontalVertexAmount; h++) {
			float progress = (float)h / (float)(holizontalVertexAmount - 1);
			int index = h;
			newVertices[index] = new Vector3((1 - (width * progress) + (width * centerXRate)) * vec, -(height * centerYRate)).ChangePosHandSystem();
			// vecがマイナスのとき文字が鏡になるのを避けるためuvのy値を反転して回転しているように見せているけど別の画像用意すべきかも.
			newUV[index] = new Vector2((1 - progress), vec > 0 ? 0 : 1).ChangeUVHandSystem();
		}
		//		for (int v = 0; v < verticalVertexAmount - 1; v++) {
		//			float progress = (float)v / (float)(verticalVertexAmount - 1);
		//			int index = holizontalVertexAmount + v;
		//			newVertices[index] = new Vector3(width - (width * centerXRate), (height * progress) + (height * centerYRate)).ChangePosHandSystem();
		//			newUV[index] = new Vector2(1, progress).ChangeUVHandSystem();
		//		}
		for (int h = holizontalVertexAmount - 1; h >= 0; h--) {
			float progress = (float)h / (float)(holizontalVertexAmount - 1);
			int index = (holizontalVertexAmount - 1) + (verticalVertexAmount - 1) + (holizontalVertexAmount - 1 - h);
			newVertices[index] = new Vector3((1 - (width * progress) + (width * centerXRate)) * vec, height - (height * centerYRate)).ChangePosHandSystem();
			// vecがマイナスのとき文字が鏡になるのを避けるためuvのy値を反転して回転しているように見せているけど別の画像用意すべきかも.
			newUV[index] = new Vector2((1 - progress), vec > 0 ? 1 : 0).ChangeUVHandSystem();
		}
		//		for (int v = 0; v < verticalVertexAmount - 2; v++) {
		//			float progress = (float)v / (float)(verticalVertexAmount);
		//			int index = (holizontalVertexAmount * 2 - 1) + (verticalVertexAmount - 1) + (verticalVertexAmount - 2 - v);
		//			newVertices[index] = new Vector3(-(width * centerXRate), (height * progress) + (height * centerYRate)).ChangePosHandSystem();
		//			newUV[index] = new Vector2(0, progress).ChangeUVHandSystem();
		//		}
		// 三角形に頂点インデックス割り当て.
		
		// ※【ここはverticalVertexAmountが2固定である前提で作るよ】.
		// 0, 1, newVertices.Length-1
		// 0, newVertices.Length-1, newVertices.Length-2
		// 0～は次のループで+1, newVertices.Length系は次のループで-1
		// newVertices.Length-2 == holizontalVertexAmount で終了.
		int triangleIndex = 0;
		for (int i = 0; i < holizontalVertexAmount - 1; i++) {
			newTriangles[triangleIndex + 0] = i + 1;
			newTriangles[triangleIndex + 1] = i + 0;
			newTriangles[triangleIndex + 2] = newVertices.Length - 2 - i;
			newTriangles[triangleIndex + 3] = i + 0;
			newTriangles[triangleIndex + 4] = newVertices.Length - 1 - i;
			newTriangles[triangleIndex + 5] = newVertices.Length - 2 - i;
			triangleIndex += 6;
		}
		
		mesh.vertices  = newVertices;
		mesh.uv        = newUV;
		mesh.triangles = newTriangles;
		mesh.name = "CounterClockwiseSlideEvaluaterMesh";
		
		mesh.RecalculateNormals();
		mesh.RecalculateBounds();
		
		//CurveSlideEvaluateMesh(mesh);
		
		return mesh;
	}







	// 横に複数、縦に2つの頂点。左上が0で、コの字を書いて左下まで頂点データを並べる.
	void Example2 () {
		float width = 96.0f;
		float height = 32.0f;
		int holizontalVertexAmount = 10;
		float centerXRate = 0.0f;
		float centerYRate = 0.5f;
		
		
		const int verticalVertexAmount = 2;
		
		Mesh mesh = new Mesh();
		Vector3 [] newVertices = new Vector3[holizontalVertexAmount * verticalVertexAmount];
		Vector2 [] newUV       = new Vector2[holizontalVertexAmount * verticalVertexAmount];
		// 三角形の頂点数は3で、四角形を作るには三角形が2つ必要で、四角形が何個あるかで掛け算する.
		// 四角形用の頂点が2*2で三角形用の頂点が6個. 3*2で12個. 4*2で18個. 3*3で24個. 4*4で36個
		// 頂点数-1個の四角形が作れる.
		int[] newTriangles     = new int[3 * 2 * (holizontalVertexAmount - 1) * (verticalVertexAmount - 1)];
		
		// 頂点とUVを作成.
		for (int h = 0; h < holizontalVertexAmount; h++) {
			float progress = (float)h / (float)(holizontalVertexAmount - 1);
			int index = h;
			newVertices[index] = new Vector3((width * progress) - (width * centerXRate), -(height * centerYRate)).ChangePosHandSystem();
			newUV[index] = new Vector2(progress, 0).ChangeUVHandSystem();
		}
		for (int h = holizontalVertexAmount - 1; h >= 0; h--) {
			float progress = (float)h / (float)(holizontalVertexAmount - 1);
			int index = (holizontalVertexAmount - 1) + (verticalVertexAmount - 1) + (holizontalVertexAmount - 1 - h);
			newVertices[index] = new Vector3((width * progress) - (width * centerXRate), height - (height * centerYRate)).ChangePosHandSystem();
			newUV[index] = new Vector2(progress, 1).ChangeUVHandSystem();
		}
		// 三角形に頂点インデックス割り当て.
		
		// ※【ここはverticalVertexAmountが2固定である前提で作るよ】.
		// 0, 1, newVertices.Length-1
		// 0, newVertices.Length-1, newVertices.Length-2
		// 0～は次のループで+1, newVertices.Length系は次のループで-1
		// newVertices.Length-2 == holizontalVertexAmount で終了.
		int triangleIndex = 0;
		for (int i = 0; i < holizontalVertexAmount - 1; i++) {
			newTriangles[triangleIndex + 0] = i + 0;
			newTriangles[triangleIndex + 1] = i + 1;
			newTriangles[triangleIndex + 2] = newVertices.Length - 2 - i;
			newTriangles[triangleIndex + 3] = i + 0;
			newTriangles[triangleIndex + 4] = newVertices.Length - 1 - i;
			newTriangles[triangleIndex + 5] = newVertices.Length - 2 - i;
			triangleIndex += 6;
		}
		
		mesh.vertices  = newVertices;
		mesh.uv        = newUV;
		mesh.triangles = newTriangles;
		mesh.name = "SlideEvaluaterMesh";
		
		mesh.RecalculateNormals();
		mesh.RecalculateBounds();
		
		GetComponent<MeshFilter>().sharedMesh = mesh;
	}




	void Example () {
		Mesh mesh = new Mesh();      
		Vector3 [] newVertices = new Vector3[4];
		Vector2 [] newUV       = new Vector2[4];
		int[] newTriangles     = new int[2 * 3];
		
		// 頂点座標の指定.
		newVertices[0] = new Vector3(0.0f, 0.0f, 0.0f);
		newVertices[1] = new Vector3(0.0f, -32.0f, 0.0f);
		newVertices[2] = new Vector3(92.0f, -32.0f, 0.0f);
		newVertices[3] = new Vector3(92.0f, 0.0f, 0.0f);
		
		// UVの指定 (頂点数と同じ数を指定すること).
		newUV[0] = new Vector2(0.0f, 1.0f);
		newUV[1] = new Vector2(0.0f, 0.0f);
		newUV[2] = new Vector2(1.0f, 0.0f);
		newUV[3] = new Vector2(1.0f, 1.0f);
		
		// 三角形ごとの頂点インデックスを指定.
		newTriangles[0] = 2;
		newTriangles[1] = 1;
		newTriangles[2] = 0;
		newTriangles[3] = 0;
		newTriangles[4] = 3;
		newTriangles[5] = 2;
		
		mesh.vertices  = newVertices;
		mesh.uv        = newUV;
		mesh.triangles = newTriangles;
		
		mesh.RecalculateNormals();
		mesh.RecalculateBounds();
		
		GetComponent<MeshFilter>().sharedMesh = mesh;
		GetComponent<MeshFilter>().sharedMesh.name = "myMesh";
	}
}
