﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DialogButton : Button {

	private Graphic[] targetGraphics;
	private Color[] targetGraphicsColors;
	private int buffSize;

	public void Setup(Graphic[] targetGraphics) {
		this.targetGraphics = targetGraphics;
		buffSize = targetGraphics.Length;
		targetGraphicsColors = new Color[buffSize];
		for (int i = 0; i < buffSize; i++) {
			targetGraphicsColors[i] = targetGraphics[i].color;
		}
	}

	protected override void DoStateTransition (SelectionState state, bool instant) {
		base.DoStateTransition (state, instant);
		switch(state) {
		case SelectionState.Normal:
			SetColor(colors.normalColor);
			break;
		case SelectionState.Highlighted:
			SetColor(colors.highlightedColor);
			break;
		case SelectionState.Pressed:
			SetColor(colors.pressedColor);
			break;
		case SelectionState.Disabled:
			SetColor(colors.disabledColor);
			break;
		}
	}

	private void SetColor(Color color) {
		if (targetGraphics != null && targetGraphicsColors != null) {
			for (int i = 0; i < buffSize; i++) {
				var g = targetGraphics[i];
				if (g != targetGraphic) {
					var source = targetGraphicsColors[i];
					StartCoroutine(SetColor(g, source, color));
				}
			}
		}
	}

	private IEnumerator SetColor(Graphic target, Color source, Color color) {
		yield return new WaitForSeconds(0.03f);
		target.color = new Color(source.r * color.r, source.g * color.g, source.b * color.b, source.a * color.a); 
	}
}
