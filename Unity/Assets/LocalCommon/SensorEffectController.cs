﻿using UnityEngine;
using System.Collections;

public class SensorEffectController : MonoBehaviour {
	[SerializeField]
	public RectTransform rectTransform;
	[SerializeField]
	public TimeAnimationScale timeAnimationScale;
	[SerializeField]
	public TimeAnimationAlpha timeAnimationAlpha;

	public void Restart() {
		timeAnimationScale.Restart();
		timeAnimationAlpha.Restart();
	}

	public void Show() {
		this.gameObject.SetActive(true);
	}

	public void Hide() {
		this.gameObject.SetActive(false);
	}

	public void SetPos(int buttonNum) {
		rectTransform.anchoredPosition = Constants.instance.GetOuterPieceAxis(buttonNum).ChangePosHandSystem();
	}

	public void SetPos(Vector2 pos) {
		rectTransform.anchoredPosition = pos;
	}
}
