using System.Collections.Generic;

[System.Serializable]
public class TrackInformation {
	public int savedata_version;
	public string title;
	public string title_ruby;
	public string artist;
	public string artist_ruby;
	public string whole_bpm;
	public float seek;
	public float wait;
	public float? simai_first;
	public string jacket;
	public string audio;
	public Datetime imported_at;
	public Dictionary<string, ScoreInformation> scores;

	public static TrackInformation NewInitialize() {
		var ret = new TrackInformation ();
		ret.UpdateNewVersion ();
		return ret;
	}
	
	public void UpdateNewVersion () {
		if (savedata_version < 1) {
			title = string.Empty;
			title_ruby = string.Empty;
			artist = string.Empty;
			artist_ruby = string.Empty;
			whole_bpm = string.Empty;
			imported_at = Datetime.Now ();
			scores = new Dictionary<string, ScoreInformation> ();
			jacket = string.Empty;
			audio = string.Empty;
			simai_first = null;
		}

		
		savedata_version = 1;
		foreach (var score in scores.Values) {
			score.UpdateNewVersion ();
		}
	}
}

[System.Serializable]
public class ScoreInformation {
	public int savedata_version;
	public string level;
	public string script_type;
	public string notes_design;
	[UnityEngine.Multiline]
	public string score; // 楽譜.
	public int full_score; // 得点.
	
	public static ScoreInformation NewInitialize() {
		var ret = new ScoreInformation ();
		ret.UpdateNewVersion ();
		return ret;
	}
	
	public void UpdateNewVersion () {
		if (savedata_version < 1) {
			level = string.Empty;
			script_type = string.Empty;
			notes_design = string.Empty;
			score = string.Empty;
			full_score = 0;
		}
		
		savedata_version = 1;
	}
}

[System.Serializable]
public class RecordInfomation {
	public int savedata_version;
	public Dictionary<string, ResultInformation> results;

	public static RecordInfomation NewInitialize() {
		var ret = new RecordInfomation ();
		ret.UpdateNewVersion ();
		return ret;
	}
	
	public void UpdateNewVersion () {
		if (savedata_version < 1) {
			results = new Dictionary<string, ResultInformation> ();
		}

		
		savedata_version = 1;
		foreach (var result in results.Values) {
			result.UpdateNewVersion ();
		}
	}
}

[System.Serializable]
public class ResultInformation {
	public int savedata_version;
	public int score;
	public int achievement;
	public bool full_ap;
	public bool ap;
	public bool fc;
	public bool no_miss;
	public int sync;
	public int play_count;
	public Datetime last_played_at;
	public int full_score; //楽譜改ざんチェック用.

	public static ResultInformation NewInitialize() {
		var ret = new ResultInformation ();
		ret.UpdateNewVersion ();
		return ret;
	}
	
	public void UpdateNewVersion () {
		if (savedata_version < 1) {
			last_played_at = Datetime.Now ();
		}
		
		savedata_version = 1;
	}
}

[System.Serializable]
public class Datetime {
	public int year;
	public int month;
	public int day;
	public int hour;
	public int minute;
	public int second;
	
	public System.DateTime ToSystem() {
		return new System.DateTime(year, month, day, hour, minute, second);
	}
	
	public Datetime() {}
	public Datetime(System.DateTime system) {
		year = system.Year;
		month = system.Month;
		day = system.Day;
		hour = system.Hour;
		minute = system.Minute;
		second = system.Second;
	}
	public static Datetime Now() {
		return new Datetime(System.DateTime.Now);
	}
}

[System.Serializable]
public class DownloadOfficialSiteScoresInformation {
	public DownloadOfficialSiteScoreDataInformation[] list;
}

[System.Serializable]
public class DownloadOfficialSiteScoreDataInformation {
	public string track_id;
	public string maidata;
}

[System.Serializable]
public class ScoreEditorAutoSaveInformation {
	public int savedata_version;
	public string track_id;
	public string score_id;
	public int selection_start_index;
	public int selection_end_index;
	public float timeline_scrolled_amount;
	public int score_end_mark_index;
	public string script_type;
	public string score;
	
	public static ScoreEditorAutoSaveInformation NewInitialize() {
		var ret = new ScoreEditorAutoSaveInformation ();
		ret.UpdateNewVersion ();
		return ret;
	}
	
	public void UpdateNewVersion () {
		if (savedata_version < 1) {
			track_id = string.Empty;
			score_id = string.Empty;
			selection_start_index = 0;
			selection_end_index = 0;
			timeline_scrolled_amount = 0;
			score_end_mark_index = -1;
			script_type = string.Empty;
			score = string.Empty;
		}
		
		savedata_version = 1;
	}
}

[System.Serializable]
public class UserInformation {
	public int savedata_version;
	public string user_name;
	public string user_title;
	public float user_title_color_r;
	public float user_title_color_g;
	public float user_title_color_b;
	public LevelInformation user_level_info;
	public Avater avater_ability;
	public float last_played_track_list_scroll;
	public float last_configed_list_scroll;
	public float last_profile_list_scroll;
	public Dictionary<string, string> last_played_difficulties;
	public string last_track_imported_path;
	public string last_profile_imported_path;
	public string user_icon_extension;
	public string user_plate_extension;
	public string user_frame_extension;
	public float repeat_start_time_rate; // 曲全体の何割の部分からスタートするか.
	public float repeat_time; // 何秒間リピートするか.
	public int sort_type;
	public string refine_level;
	public string refine_difficulty;
	public string refine_designer;
	public int refine_no_miss;
	public int refine_fc;
	public int refine_ap;
	public bool score_editor_processing; // スコアエディタを起動中だった.
	public int score_editor_auto_save_interval_minutes; // スコアエディタの自動保存間隔.

	public const string DEFAULT_USER_NAME = "NO NAME";
	public const string DEFAULT_USER_TITLE = "debut";
	
	public static UserInformation NewInitialize() {
		var ret = new UserInformation ();
		ret.UpdateNewVersion ();
		return ret;
	}

	public void UpdateNewVersion () {
		if (savedata_version < 1) {
			user_name = DEFAULT_USER_NAME;
			user_title = DEFAULT_USER_TITLE;
			user_title_color_r = 1;
			user_title_color_g = 1;
			user_title_color_b = 1;
			user_level_info = LevelInformation.NewInitialize ();
			avater_ability = Avater.NewInitialize ();
			user_icon_extension = ".png";
			user_plate_extension = ".png";
			user_frame_extension = ".png";
			last_played_difficulties = new Dictionary<string, string> ();
			score_editor_processing = false;
			score_editor_auto_save_interval_minutes = 3;
		}
		user_level_info.UpdateNewVersion ();
		avater_ability.UpdateNewVersion ();
		// バージョンアップして項目が増えたらifでバージョンチェックして新たに追加する.

		savedata_version = 1;
	}

	public void Copy (UserInformation src) {
		savedata_version = src.savedata_version;
		user_name = src.user_name;
		user_title = src.user_title;
		user_title_color_r = src.user_title_color_r;
		user_title_color_g = src.user_title_color_g;
		user_title_color_b = src.user_title_color_b;
		user_level_info = src.user_level_info;
		avater_ability = src.avater_ability;
		last_played_track_list_scroll = src.last_played_track_list_scroll;
		last_configed_list_scroll = src.last_configed_list_scroll;
		last_profile_list_scroll = src.last_profile_list_scroll;
		last_played_difficulties = src.last_played_difficulties;
		last_track_imported_path = src.last_track_imported_path;
		last_profile_imported_path = src.last_profile_imported_path;
		user_icon_extension = src.user_icon_extension;
		user_plate_extension = src.user_plate_extension;
		user_frame_extension = src.user_frame_extension;
		repeat_start_time_rate = src.repeat_start_time_rate;
		repeat_time = src.repeat_time;
		sort_type = src.sort_type;
		refine_level = src.refine_level;
		refine_difficulty = src.refine_difficulty;
		refine_designer = src.refine_designer;
		refine_no_miss = src.refine_no_miss;
		refine_fc = src.refine_fc;
		refine_ap = src.refine_ap;
		score_editor_processing = src.score_editor_processing;
	}
	
	public static UserInformation Clone (UserInformation src) {
		var ret = NewInitialize ();
		ret.Copy (src);
		return ret;
	}

	[System.Serializable]
	public class LevelInformation {
		public int savedata_version;
		public int level;
		public int experience;
		public int GetNextLevelUpExperience () {
			// レベル*10expでレベルアップ.
			// レベル100を超えたら1000exp固定.
			// 基本的には1度に最大10expもらえる.
			const int add = 10;
			int needbase = level > 100 ? 100 : level;
			return needbase * add;
		}
		public float GetExperienceRate () {
			float next = GetNextLevelUpExperience ();
			if (next > 0) {
				return (float)experience / next;
			}
			return 0;
		}
		public int GetMaxLevel () { return 100000000; } // 最大レベル1億.

		public static LevelInformation NewInitialize () {
			var ret = new LevelInformation ();
			ret.UpdateNewVersion ();
			return ret;
		}
		
		public void UpdateNewVersion () {
			if (savedata_version < 1) {
				level = 1;
				experience = 0;
			}
			savedata_version = 1;
		}

		public void InjectionExperience (int getExp) {
			if (level >= GetMaxLevel ()) {
				level = GetMaxLevel ();
				experience = 0;
				return;
			}

			while (getExp > 0) {
				// 次のレベルアップに必要な経験値.
				int next = GetNextLevelUpExperience ();
				// 次のレベルアップまでの残りの経験値.
				int nextRemain = next - experience;
				// 取得した経験値が次のレベルアップまでの残りの経験値を超えていたらレベルアップ.
				if (nextRemain <= getExp) {
					level++;
					experience = 0;
					// レベルアップに使った経験値を引く.
					getExp -= nextRemain;

					if (level >= GetMaxLevel ()) {
						break;
					}
				}
				else {
					// 経験値を加算して使い切る.
					experience += getExp;
					getExp = 0;
				}
			}

			if (level >= GetMaxLevel ()) {
				level = GetMaxLevel ();
				experience = 0;
			}
		}
	}
	
	[System.Serializable]
	public class Avater {

		[System.Serializable]
		public class Element {
			public int savedata_version;
			public float tap_ability;
			public float hold_ability;
			public float slide_ability;
			public float break_ability;

			public Element () {}
			public Element (float tapAbility, float holdAbility, float slideAbility, float breakAbility) {
				UpdateNewVersion ();
				tap_ability = tapAbility;
				hold_ability = holdAbility;
				slide_ability = slideAbility;
				break_ability = breakAbility;
			}

			public static Element NewInitialize () {
				var ret = new Element ();
				ret.UpdateNewVersion ();
				return ret;
			}

			public void UpdateNewVersion () {
				if (savedata_version < 1) {
					tap_ability = 0;
					hold_ability = 0;
					slide_ability = 0;
					break_ability = 0;
				}
				savedata_version = 1;
			}
		}

		public int savedata_version;
		public List<Element> elements;
		public Element average;
		
		public static Avater NewInitialize () {
			var ret = new Avater ();
			ret.UpdateNewVersion ();
			return ret;
		}
		
		public void UpdateNewVersion () {
			if (savedata_version < 2) {
				elements = new List<Element>();
				average = Element.NewInitialize ();
			}
			savedata_version = 2;

			foreach (var element in elements) {
				element.UpdateNewVersion ();
			}
		}

		public int GetElementsMax () { return 300; }

		public void AddElement (float tapAbility, float holdAbility, float slideAbility, float breakAbility) {
			// 全要素が0なら何もしない.
			if (tapAbility == 0 && holdAbility == 0 && slideAbility == 0 && breakAbility == 0)
				return;

			if (elements.Count > GetElementsMax ()) {
				elements.RemoveAt (0);
			}
			elements.Add (new Element (tapAbility, holdAbility, slideAbility, breakAbility));

			decimal tapTotal, holdTotal, slideTotal, breakTotal;
			tapTotal = holdTotal = slideTotal = breakTotal = 0;
			foreach (var element in elements) {
				tapTotal += (decimal)element.tap_ability;
				holdTotal += (decimal)element.hold_ability;
				slideTotal += (decimal)element.slide_ability;
				breakTotal += (decimal)element.break_ability;
			}
			decimal size = (decimal)elements.Count;
			average = new Element (
				(float)(tapTotal / size),
				(float)(holdTotal / size),
				(float)(slideTotal / size),
				(float)(breakTotal / size)
			);
		}
	}
}

[System.Serializable]
public class ConfigInformation {
	public int savedata_version;
	public int sound_effect_type;
	public int answer_sound_type;
	public int turn;
	public bool mirror;
	public float fade_guide_speed;
	public float move_guide_speed;
	public bool star_rotation;
	public float ring_size;
	public float marker_size;
	public bool angulated_hold;
	public bool pine;
	public int background_infomation_type;
	public int up_screen_achievement_vision_type;
	public int rank_version_type;
	public bool visualize_fast_late;
	public bool auto_play;
	public float brightness;
	public int change_break_type; // tapをbreak, またはbreakをtap. 得点は保存されない.
	public int change_ring_type; // ringをstar, またはstarをring.
	public float judge_offset;
	public int judgement_type; //Off,マジ,ガチ,ゴリ.
	public int challenge_life; //無限、1,5,10,20,50,100,300
	public int user_recources_cache_type;
	public bool is_load_jacket_when_select_track;
	public bool is_load_audio_when_select_track;
	public bool realtime_create_object; // InstantiateとDestroyをリアルタイムに行う.
	public bool skip_slide; // スライド1つ飛ばし機能
	public float sensor_size;

	public static ConfigInformation NewInitialize() {
		var ret = new ConfigInformation ();
		ret.UpdateNewVersion ();
		return ret;
	}
	
	public void UpdateNewVersion () {
		if (savedata_version < 1) {
			sound_effect_type = ConfigTypes.SoundEffect.OFF.ToInt();
			answer_sound_type = ConfigTypes.AnswerSound.SPECIAL_PLUS.ToInt();
			turn = 0;
			mirror = false;
			fade_guide_speed = 0.3f;
			move_guide_speed = 0.7f;
			star_rotation = true;
			ring_size = 0.75f;
			marker_size = 1.0f;
			angulated_hold = true;
			pine = false;
			background_infomation_type = ConfigTypes.BackGroundInfomation.COMBO.ToInt();
			up_screen_achievement_vision_type = ConfigTypes.UpScreenAchievementVision.NORMAL.ToInt();
			rank_version_type = ConfigTypes.RankVersion.PINK.ToInt();
			visualize_fast_late = false;
			auto_play = false;
			brightness = 0.4f;
			change_break_type = ConfigTypes.ChangeBreak.NORMAL.ToInt();
			judge_offset = 0.0f;
			judgement_type = ConfigTypes.Judgement.NORMAL.ToInt();
			sensor_size = 70.0f;
		}
		if (savedata_version < 2) {
			user_recources_cache_type = ConfigTypes.ResourcesCacheType.UNUSED.ToInt();
			is_load_jacket_when_select_track = true;
			is_load_audio_when_select_track = false;
		}
		if (savedata_version < 3) {
			// Addition.
			realtime_create_object = false;
			skip_slide = true;
			change_ring_type = ConfigTypes.ChangeRing.NORMAL.ToInt();
			challenge_life = ConfigTypes.LifeDifficulty.INFINITY.ToInt();
		}

		// バージョンアップして項目が増えたらifでバージョンチェックして新たに追加する.
		
		savedata_version = 3;
	}

	public static ConfigInformation NewDebugCustomInitialize() {
		var ret = NewInitialize ();
		ret.auto_play = true;
		return ret;
	}

	public void Copy (ConfigInformation src) {
		savedata_version = src.savedata_version;
		sound_effect_type = src.sound_effect_type;
		answer_sound_type = src.answer_sound_type;
		turn = src.turn;
		mirror = src.mirror;
		fade_guide_speed = src.fade_guide_speed;
		move_guide_speed = src.move_guide_speed;
		star_rotation = src.star_rotation;
		ring_size = src.ring_size;
		marker_size = src.marker_size;
		angulated_hold = src.angulated_hold;
		pine = src.pine;
		background_infomation_type = src.background_infomation_type;
		up_screen_achievement_vision_type = src.up_screen_achievement_vision_type;
		rank_version_type = src.rank_version_type;
		visualize_fast_late = src.visualize_fast_late;
		auto_play = src.auto_play;
		brightness = src.brightness;
		change_break_type = src.change_break_type;
		change_ring_type = src.change_ring_type;
		judge_offset = src.judge_offset;
		judgement_type = src.judgement_type;
		user_recources_cache_type = src.user_recources_cache_type;
		is_load_jacket_when_select_track = src.is_load_jacket_when_select_track;
		is_load_audio_when_select_track = src.is_load_audio_when_select_track;
		realtime_create_object = src.realtime_create_object;
		skip_slide = src.skip_slide;
		sensor_size = src.sensor_size;
	}

	public static ConfigInformation Clone (ConfigInformation src) {
		var ret = NewInitialize ();
		ret.Copy (src);
		return ret;
	}

}

public static class ConfigTypes {
	public enum SoundEffect {
		OFF, ON, ONLY_BREAK2600, 
	}
	public static SoundEffect ToSoundEffectType(this int value) {
		return (SoundEffect)value;
	}
	public static int ToInt(this SoundEffect value) {
		return (int)value;
	}

	public enum AnswerSound {
		OFF, BASIS, BASIS_PLUS, SPECIAL, SPECIAL_PLUS, 
	}
	public static AnswerSound ToAnswerSoundType(this int value) {
		return (AnswerSound)value;
	}
	public static int ToInt(this AnswerSound value) {
		return (int)value;
	}

	public enum BackGroundInfomation {
		OFF, COMBO, PCOMBO, BPCOMBO, ACHIEVEMENT, SCORE, 
	}
	public static BackGroundInfomation ToBackGroundInfomationType(this int value) {
		return (BackGroundInfomation)value;
	}
	public static int ToInt(this BackGroundInfomation value) {
		return (int)value;
	}

	public enum UpScreenAchievementVision {
		NORMAL, PACE, HAZARD_BREAKPACE, HAZARD, T_NORMAL, T_PACE, T_HAZARD, 
	}
	public static UpScreenAchievementVision ToUpScreenAchievementVisionType(this int value) {
		return (UpScreenAchievementVision)value;
	}
	public static int ToInt(this UpScreenAchievementVision value) {
		return (int)value;
	}

	public enum ChangeBreak {
		NORMAL, TAP_TO_BREAK, BREAK_TO_TAP, TAP_EXCHANGE_BREAK, 
	}
	public static ChangeBreak ToChangeBreakType(this int value) {
		return (ChangeBreak)value;
	}
	public static int ToInt(this ChangeBreak value) {
		return (int)value;
	}
	
	public enum ChangeRing {
		NORMAL, RING_TO_STAR, STAR_TO_RING, RING_EXCHANGE_STAR, 
	}
	public static ChangeRing ToChangeRingType(this int value) {
		return (ChangeRing)value;
	}
	public static int ToInt(this ChangeRing value) {
		return (int)value;
	}

	public enum Judgement {
		NORMAL, MAJI, GACHI, GORI, 
	}
	public static Judgement ToJudgementType(this int value) {
		return (Judgement)value;
	}
	public static int ToInt(this Judgement value) {
		return (int)value;
	}
	
	public enum RankVersion {
		PINK, CLASSIC, 
	}
	public static RankVersion ToRankVersionType(this int value) {
		return (RankVersion)value;
	}
	public static int ToInt(this RankVersion value) {
		return (int)value;
	}
	
	public enum LifeDifficulty {
		INFINITY, LIFE_1, LIFE_5, LIFE_10, LIFE_20, LIFE_50, LIFE_100, LIFE_300,
	}
	public static LifeDifficulty ToLifeDifficultyType(this int value) {
		return (LifeDifficulty)value;
	}
	public static int ToInt(this LifeDifficulty value) {
		return (int)value;
	}
	
	public enum SortLevels {
		IMPORTED, TITLE, ARTIST, SCORE, ACHIEVEMENT, LEVEL, PLAYCOUNT,
	}
	public static SortLevels ToSortLevels(this int value) {
		return (SortLevels)value;
	}
	public static int ToInt(this SortLevels value) {
		return (int)value;
	}

	public enum ResourcesCacheType {
		/// <summary>キャッシュを使わない</summary>
		UNUSED, 
		/// <summary>読み込んだらキャッシュする</summary>
		USED, 
		/// <summary>一括読み込みしてキャッシュする</summary>
		BULK, 
	}
	public static ResourcesCacheType ToResourcesCacheType(this int value) {
		return (ResourcesCacheType)value;
	}
	public static int ToInt(this ResourcesCacheType value) {
		return (int)value;
	}

}
