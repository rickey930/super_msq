﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor( typeof(IgnoreTouch) )]
public class IgnoreTouchEditor : Editor {
	string[] popupSelectNames = new string[] {
		"All", "Out", "In"
	};
	public override void OnInspectorGUI() {
		IgnoreTouch obj = target as IgnoreTouch;
		int oldIndex = obj.editorPopupSelectedIndex;
		int newIndex = EditorGUILayout.Popup("Ignore Range", obj.editorPopupSelectedIndex, popupSelectNames );
		if (oldIndex != newIndex) {
			obj.editorPopupSelectedIndex = newIndex;
			if (newIndex == 0) {
				obj.ignoreOutCircle = false;
				obj.ignoreInCircle = false;
			}
			else if (newIndex == 1) {
				obj.ignoreOutCircle = true;
				obj.ignoreInCircle = false;
			}
			else if (newIndex == 2) {
				obj.ignoreOutCircle = false;
				obj.ignoreInCircle = true;
			}
		}
		if (newIndex == 1 || newIndex == 2) {
			float oldRadius = obj.radius;
			float newRadius = EditorGUILayout.FloatField("Circle Radius", obj.radius);
			if (oldRadius != newRadius) {
				obj.radius = newRadius;
			}
		}
		EditorUtility.SetDirty( target );
	}
}
