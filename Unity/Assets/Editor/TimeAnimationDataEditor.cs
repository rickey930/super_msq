﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(TimeAnimation))]
public class TimeAnimationDataEditor : Editor {
	
	private int index;
	
	public override void OnInspectorGUI () {
		serializedObject.Update ();
		EditorGUI.BeginChangeCheck ();
		
		var obj = this.target as TimeAnimation;
		
		base.OnInspectorGUI ();
		
		if (GUILayout.Button ("Continuity")) {
			var data = obj.GetData;
			if (data != null && data.Length > 0) {
				data[0].translate.start = obj.transform.localPosition;
				data[0].rotate.start = obj.transform.localRotation.eulerAngles;
				data[0].scale.start = obj.transform.localScale;
				var spRenderer = obj.gameObject.GetComponent<SpriteRenderer>();
				if (spRenderer != null) data[0].alpha.start = spRenderer.color.a;
				for (int i = 1; i < data.Length; i++) {
					data[i].translate.start = data[i - 1].translate.target;
					data[i].rotate.start = data[i - 1].rotate.target;
					data[i].scale.start = data[i - 1].scale.target;
					data[i].alpha.start = data[i - 1].alpha.target;
				}
			}
		}
		
		if (EditorGUI.EndChangeCheck ()) {
			EditorUtility.SetDirty (target);
		}
	}
}

[CustomEditor(typeof(TimeAnimationRotate))]
public class TimeAnimationRotateDataEditor : Editor {
	
	private int index;
	
	public override void OnInspectorGUI () {
		serializedObject.Update ();
		EditorGUI.BeginChangeCheck ();
		
		var obj = this.target as TimeAnimationRotate;
		
		base.OnInspectorGUI ();
		
		if (GUILayout.Button ("Continuity")) {
			var data = obj.GetData;
			if (data != null && data.Length > 0) {
				data[0].start = obj.transform.localRotation.eulerAngles;
				for (int i = 1; i < data.Length; i++) {
					data[i].start = data[i - 1].target;
				}
			}
		}
		
		if (EditorGUI.EndChangeCheck ()) {
			EditorUtility.SetDirty (target);
		}
	}
}

[CustomEditor(typeof(TimeAnimationAlpha))]
public class TimeAnimationAlphaDataEditor : Editor {
	
	private int index;
	
	public override void OnInspectorGUI () {
		serializedObject.Update ();
		EditorGUI.BeginChangeCheck ();
		
		var obj = this.target as TimeAnimationAlpha;
		
		base.OnInspectorGUI ();
		
		if (GUILayout.Button ("Continuity")) {
			var data = obj.GetData;
			if (data != null && data.Length > 0) {
				var spRenderer = obj.gameObject.GetComponent<SpriteRenderer>();
				if (spRenderer != null) data[0].start = spRenderer.color.a;
				for (int i = 1; i < data.Length; i++) {
					data[i].start = data[i - 1].target;
				}
			}
		}
		
		if (EditorGUI.EndChangeCheck ()) {
			EditorUtility.SetDirty (target);
		}
	}
}

[CustomEditor(typeof(TimeAnimationTranslate))]
public class TimeAnimationTranslateDataEditor : Editor {
	
	private int index;
	
	public override void OnInspectorGUI () {
		serializedObject.Update ();
		EditorGUI.BeginChangeCheck ();
		
		var obj = this.target as TimeAnimationTranslate;
		
		base.OnInspectorGUI ();
		
		if (GUILayout.Button ("Continuity")) {
			var data = obj.GetData;
			if (data != null && data.Length > 0) {
				data[0].start = obj.transform.localPosition;
				for (int i = 1; i < data.Length; i++) {
					data[i].start = data[i - 1].target;
				}
			}
		}
		
		if (EditorGUI.EndChangeCheck ()) {
			EditorUtility.SetDirty (target);
		}
	}
}

[CustomEditor(typeof(TimeAnimationScale))]
public class TimeAnimationScaleDataEditor : Editor {
	
	private int index;
	
	public override void OnInspectorGUI () {
		serializedObject.Update ();
		EditorGUI.BeginChangeCheck ();
		
		var obj = this.target as TimeAnimationScale;
		
		base.OnInspectorGUI ();
		
		if (GUILayout.Button ("Continuity")) {
			var data = obj.GetData;
			if (data != null && data.Length > 0) {
				data[0].start = obj.transform.localScale;
				for (int i = 1; i < data.Length; i++) {
					data[i].start = data[i - 1].target;
				}
			}
		}
		
		if (EditorGUI.EndChangeCheck ()) {
			EditorUtility.SetDirty (target);
		}
	}
}
