﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class SlideEvaluateMeshEditor : MonoBehaviour
{
	// Unityのメニューバーに作られるこの項目を選ぶと、Createメソッドが実行されてメッシュがアセットに保存される.
	// 保存するとそれまで参照していたところがMissingになるっぽいので再設定が必要？.
	[MenuItem ("GameObject/rickey930 Extend/SlideEvaluaterMeshSave")]
	static void Create ()
	{
#if false
		Mesh mesh;
		string filename;
		mesh = CreateMeshTest.CreateResultAchievementCircleMesh ();
		// 出力先ファイル名
		filename = "Assets/Resources/Mesh/" + mesh.name + ".asset";
		// Assetへ保存
		AssetDatabase.CreateAsset (mesh, filename);
#endif

#if false
		Mesh mesh;
		string filename;
		mesh = CreateMeshTest.CreateAchievementBarMesh ();
		// 出力先ファイル名
		filename = "Assets/Resources/Mesh/" + mesh.name + ".asset";
		// Assetへ保存
		AssetDatabase.CreateAsset (mesh, filename);
#endif

#if false
		Mesh mesh;
		string filename;
		mesh = CreateMeshTest.CreateCounterClockwiseSlideEvaluateMesh ();
		// 出力先ファイル名
		filename = "Assets/Resources/Mesh/" + mesh.name + ".asset";
		// Assetへ保存
		AssetDatabase.CreateAsset (mesh, filename);
#endif

#if true
		Mesh mesh;
		string filename;
		mesh = SlideMeshCreator.CreateSlideEvaluateTextureMesh ();
		// 出力先ファイル名
		filename = "Assets/Resources/Mesh/" + mesh.name + ".asset";
		// Assetへ保存
		AssetDatabase.CreateAsset (mesh, filename);
#endif
		
#if true
		AssetDatabase.SaveAssets ();
#endif

	}
}