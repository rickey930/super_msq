﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;
using Newtonsoft.Json;

public class Language : MonoBehaviour {
	public enum Keys {
		// rhythmgame
		OK, CANCEL, YES, NO, 
		RESUME, RETRY, EXIT, SEEKWAIT_SETTING, REPEAT_SETTING, 
		SEEK, WAIT, START, REPEAT, 
		MAJI, GACHI, GORI, AUTOPLAY, ACHIEVEMENT, SYNCHRONIZE, 
		// result
		CLOSE, NEXT, DISCONNECT, 
		// mainmenu
		SELECT_TRACK, IMPORT_TRACK, ADJUST_DELAY, TUTORIAL, SYNC_HOST, SYNC_CLIENT,
		// importtrack
		IMPORT_FROM_OFFICIAL_WEB_SITE, IMPORT_FROM_FILELIST_TXT, IMPORT_FROM_MAIDATA_TXT, TRACK_MANAGER,
		TRACK_ID, TITLE, TITLE_RUBY, ARTIST, ARTIST_RUBY, BPM, USE_AUDIO, USE_JACKET, TRACK_MANAGER_SCORE_LIST,
		SCORE_ID, LEVEL, NOTES_DESIGNER, TRACK_MANAGER_SCORE,
		SELECTED, NOT_SELECTED, 
		// adjustdelay
		MENU, CHANGE_BPM, SAVE, ADJUST_DERAY_DESCRIPTION_1, ADJUST_DERAY_DESCRIPTION_2, ADJUST_DERAY_DESCRIPTION_3, 
		// config key
		LANGUAGE, SOUND_EFFECT_TYPE, ANSWER_SOUND_TYPE, TRUN, MIRROR, FADE_GUIDE_SPEED, MOVE_GUIDE_SPEED, 
		STAR_ROTATION, RING_SIZE, SLIDE_MARKER_SIZE, ANGULATED_HOLD, PINE, BG_INFO_TYPE,
		UP_SCREEN_ACHIEVEMENT_VISION_TYPE, RANK_VERSION, VISUALIZE_FASTLATE, BRIGHTNESS,
		CHANGE_BREAK_TYPE, CHANGE_RING_TYPE, JUDGE_OFFSET, JUDGEMENT_TYPE, CHALLENGE_LIFE, USER_RESOURCES_CACHE_TYPE,
		IS_LOAD_JACKET_WHEN_SELECT_TRACK, IS_LOAD_AUDIO_WHEN_SELECT_TRACK, REALTIME_CREATE_OBJECT, SKIP_SLIDE, SENSOR_SIZE, 
		// config value
		JAPANESE_DEFAULT, JAPANESE_STANDARD, JAPANESE_KANA, ENGLISH, 
		OFF, ON, ONLY_BREAK2600, BASIS, BASIS_PLUS, SPECIAL, SPECIAL_PLUS, 
		COMBO, PCOMBO, BPCOMBO, SCORE, 
		NORMAL, PACE, HAZARD_BREAKPACE, HAZARD, T_NORMAL, T_PACE, T_HAZARD, 
		TAP_TO_BREAK, BREAK_TO_TAP, TAP_EXCHANGE_BREAK, 
		RING_TO_STAR, STAR_TO_RING, RING_EXCHANGE_STAR, 
		PINK, CLASSIC, 
		INFINITY, LIFE_1, LIFE_5, LIFE_10, LIFE_20, LIFE_50, LIFE_100, LIFE_300,
		CHACHE_UNUSED, CHACHE_USED, CHACHE_BULK, SEC, 
		// config key description
		SOUND_EFFECT_TYPE_DESCRIPTION, ANSWER_SOUND_TYPE_DESCRIPTION, TRUN_DESCRIPTION, MIRROR_DESCRIPTION, FADE_GUIDE_SPEED_DESCRIPTION, MOVE_GUIDE_SPEED_DESCRIPTION, 
		STAR_ROTATION_DESCRIPTION, RING_SIZE_DESCRIPTION, SLIDE_MARKER_SIZE_DESCRIPTION, ANGULATED_HOLD_DESCRIPTION, PINE_DESCRIPTION, BG_INFO_TYPE_DESCRIPTION,
		UP_SCREEN_ACHIEVEMENT_VISION_TYPE_DESCRIPTION, RANK_VERSION_DESCRIPTION, VISUALIZE_FASTLATE_DESCRIPTION, AUTOPLAY_DESCRIPTION, BRIGHTNESS_DESCRIPTION,
		CHANGE_BREAK_TYPE_DESCRIPTION, CHANGE_RING_TYPE_DESCRIPTION, JUDGE_OFFSET_DESCRIPTION, JUDGEMENT_TYPE_DESCRIPTION, CHALLENGE_LIFE_DESCRIPTION, USER_RESOURCES_CACHE_TYPE_DESCRIPTION,
		IS_LOAD_JACKET_WHEN_SELECT_TRACK_DESCRIPTION, IS_LOAD_AUDIO_WHEN_SELECT_TRACK_DESCRIPTION, REALTIME_CREATE_OBJECT_DESCRIPTION, SKIP_SLIDE_DESCRIPTION, SENSOR_SIZE_DESCRIPTION,
		// config value description
		SOUND_EFFECT_TYPE_ON_DESCRIPTION, SOUND_EFFECT_TYPE_OFF_DESCRIPTION, SOUND_EFFECT_TYPE_ONLYBREAK2600_DESCRIPTION, 
		ANSWER_SOUND_TYPE_BASIS_DESCRIPTION, ANSWER_SOUND_TYPE_BASIS_PLUS_DESCRIPTION, ANSWER_SOUND_TYPE_SPECIAL_DESCRIPTION, ANSWER_SOUND_TYPE_SPECIAL_PLUS_DESCRIPTION, ANSWER_SOUND_TYPE_OFF_DESCRIPTION, 
		TRUN_VALUE_DESCRIPTION, TRUN_OFF_DESCRIPTION, 
		MIRROR_ON_DESCRIPTION, MIRROR_OFF_DESCRIPTION, 
		FADE_GUIDE_SPEED_VALUE_DESCRIPTION, 
		MOVE_GUIDE_SPEED_VALUE_DESCRIPTION, 
		STAR_ROTATION_ON_DESCRIPTION, STAR_ROTATION_OFF_DESCRIPTION, 
		RING_SIZE_VALUE_DESCRIPTION, 
		SLIDE_MARKER_SIZE_VALUE_DESCRIPTION, 
		ANGULATED_HOLD_ON_DESCRIPTION, ANGULATED_HOLD_OFF_DESCRIPTION,
		PINE_ON_DESCRIPTION, PINE_OFF_DESCRIPTION, 
		BG_INFO_TYPE_COMBO_DESCRIPTION, BG_INFO_TYPE_PCOMBO_DESCRIPTION, BG_INFO_TYPE_BPCOMBO_DESCRIPTION, BG_INFO_TYPE_ACHIEVEMENT_DESCRIPTION, BG_INFO_TYPE_SCORE_DESCRIPTION, BG_INFO_TYPE_OFF_DESCRIPTION, 
		UP_SCREEN_ACHIEVEMENT_VISION_TYPE_NORMAL_DESCRIPTION, UP_SCREEN_ACHIEVEMENT_VISION_TYPE_PACE_DESCRIPTION, UP_SCREEN_ACHIEVEMENT_VISION_TYPE_HAZARD_BREAKPACE_DESCRIPTION, UP_SCREEN_ACHIEVEMENT_VISION_TYPE_HAZARD_DESCRIPTION, UP_SCREEN_ACHIEVEMENT_VISION_TYPE_T_NORMAL_DESCRIPTION, UP_SCREEN_ACHIEVEMENT_VISION_TYPE_T_PACE_DESCRIPTION, UP_SCREEN_ACHIEVEMENT_VISION_TYPE_T_HAZARD_DESCRIPTION, 
		RANK_VERSION_PINK_DESCRIPTION, RANK_VERSION_CLASSIC_DESCRIPTION,
		VISUALIZE_FASTLATE_ON_DESCRIPTION, VISUALIZE_FASTLATE_OFF_DESCRIPTION,
		AUTOPLAY_ON_DESCRIPTION, AUTOPLAY_OFF_DESCRIPTION, 
		BRIGHTNESS_VALUE_DESCRIPTION,
		CHANGE_BREAK_TYPE_TAP_TO_BREAK_DESCRIPTION, CHANGE_BREAK_TYPE_BREAK_TO_TAP_DESCRIPTION, CHANGE_BREAK_TYPE_TAP_EXCHANGE_BREAK_DESCRIPTION, CHANGE_BREAK_TYPE_OFF_DESCRIPTION, 
		CHANGE_RING_TYPE_RING_TO_STAR_DESCRIPTION, CHANGE_RING_TYPE_STAR_TO_RING_DESCRIPTION, CHANGE_RING_TYPE_RING_EXCHANGE_STAR_DESCRIPTION, CHANGE_RING_TYPE_OFF_DESCRIPTION, 
		JUDGE_OFFSET_VALUE_DESCRIPTION, 
		JUDGEMENT_TYPE_NORMAL_DESCRIPTION, JUDGEMENT_TYPE_MAJI_DESCRIPTION, JUDGEMENT_TYPE_GACHI_DESCRIPTION, JUDGEMENT_TYPE_GORI_DESCRIPTION, 
		CHALLENGE_LIFE_INFINITY_DESCRIPTION, CHALLENGE_LIFE_1_DESCRIPTION, CHALLENGE_LIFE_5_DESCRIPTION, CHALLENGE_LIFE_10_DESCRIPTION, CHALLENGE_LIFE_20_DESCRIPTION, CHALLENGE_LIFE_50_DESCRIPTION, CHALLENGE_LIFE_100_DESCRIPTION, CHALLENGE_LIFE_300_DESCRIPTION, 
		USER_RESOURCES_CACHE_TYPE_UNUSED_DESCRIPTION, USER_RESOURCES_CACHE_TYPE_USED_DESCRIPTION, USER_RESOURCES_CACHE_TYPE_BULK_DESCRIPTION,
		IS_LOAD_JACKET_WHEN_SELECT_TRACK_ON_DESCRIPTION, IS_LOAD_JACKET_WHEN_SELECT_TRACK_OFF_DESCRIPTION, 
		IS_LOAD_AUDIO_WHEN_SELECT_TRACK_ON_DESCRIPTION, IS_LOAD_AUDIO_WHEN_SELECT_TRACK_OFF_DESCRIPTION, 
		REALTIME_CREATE_OBJECT_ON_DESCRIPTION, REALTIME_CREATE_OBJECT_OFF_DESCRIPTION,
		SKIP_SLIDE_ON_DESCRIPTION, SKIP_SLIDE_OFF_DESCRIPTION,
		SENSOR_SIZE_VALUE_DESCRIPTION,
		// profile key
		PLAYER_NAME, PLAYER_TITLE, PLAYER_ICON, PLAYER_PLATE, PLAYER_FRAME, SORT_TYPE,
		REFINE_LEVEL, REFINE_DIFFICULTY, REFINE_DESIGNER, REFINE_NOMISS, REFINE_FC, REFINE_AP,
		// profile value
		SORT_IMPORTED, SORT_TITLE, SORT_ARTIST, SORT_ACHIEVEMENT, SORT_SCORE, SORT_LEVEL, SORT_PLAYCOUNT, ACHIEVED, NOT_ACHIEVED,
		// profile key description
		PLAYER_NAME_DESCRIPTION, PLAYER_TITLE_DESCRIPTION, PLAYER_ICON_DESCRIPTION, PLAYER_PLATE_DESCRIPTION, PLAYER_FRAME_DESCRIPTION, SORT_TYPE_DESCRIPTION,
		REFINE_LEVEL_DESCRIPTION, REFINE_DIFFICULTY_DESCRIPTION, REFINE_DESIGNER_DESCRIPTION, REFINE_NOMISS_DESCRIPTION, REFINE_FC_DESCRIPTION, REFINE_AP_DESCRIPTION,
		// profile value
		SORT_IMPORTED_DESCRIPTION, SORT_TITLE_DESCRIPTION, SORT_ARTIST_DESCRIPTION, SORT_ACHIEVEMENT_DESCRIPTION, SORT_SCORE_DESCRIPTION, SORT_LEVEL_DESCRIPTION, SORT_PLAYCOUNT_DESCRIPTION, REFINE_ACHIEVED_DESCRIPTION, REFINE_NOT_ACHIEVED_DESCRIPTION, REFINE_OFF_DESCRIPTION, 
		// loading
		NOWLOADING,
		// sync mode
		DISCONNECT_HOST, DISCONNECT_CLIENT, 
		YOUR_IP_ADDRESS, YOUR_PORT_NUMBER, HOST_IP_ADDRESS, HOST_PORT_NUMBER, CONNECTION, CONNECTION_ESTABLISHED, 
		HOST_HAVE_SELECTION_THE_SCORE, PLEASE_WAIT, DOWNLOADING, WAITING_FOR_CLIENT, WAITING_FOR_HOST, 
	}

	private delegate void CreateResourceHelper(Keys key, string jpdefault, string jponly, string jpkana, string english);
	
	[MenuItem ("GameObject/rickey930 Extend/Output Language Files")]
	private static void CreateResource() {
		var jpdefault = new Dictionary<string, string>();
		var jponly___ = new Dictionary<string, string>();
		var jpkana___ = new Dictionary<string, string>();
		var english__ = new Dictionary<string, string>();
		CreateResourceHelper on = (key, value_jpdef, value_jponly, value_jpkana, value_english) => {
			string keystr = key.ToString();
			jpdefault[keystr] = value_jpdef;
			jponly___[keystr] = value_jponly;
			jpkana___[keystr] = value_jpkana;
			english__[keystr] = value_english;
		};
		// rhythmgame
		on(Keys.OK, "OK", "決定", "けってい", "OK");
		on(Keys.CANCEL, "CANCEL", "キャンセル", "やめる", "CANCEL");
		on(Keys.YES, "YES", "はい", "はい", "YES");
		on(Keys.NO, "NO", "いいえ", "いいえ", "NO");
		on(Keys.RESUME, "RESUME", "再開", "つづける", "RESUME");
		on(Keys.RETRY, "RETRY", "リトライ", "さいしょから", "RETRY");
		on(Keys.EXIT, "EXIT", "終了", "ゲームをやめる", "EXIT");
		on(Keys.SEEKWAIT_SETTING, "SEEK / WAIT", "音ズレ調整", "おとズレちょうせい", "SEEK / WAIT");
		on(Keys.REPEAT_SETTING, "REPEAT (AUTO PLAY)", "リピート設定", "リピートせってい", "REPEAT (AUTO PLAY)");
		on(Keys.SEEK, "SEEK", "シーク", "シーク", "SEEK");
		on(Keys.WAIT, "WAIT", "ウェイト", "ウェイト", "WAIT");
		on(Keys.START, "START", "開始時間", "スタート", "START");
		on(Keys.REPEAT, "REPEAT", "再生範囲", "リピート", "REPEAT");
		on(Keys.MAJI, "マジ", "マジ", "リピート", "MAJI");
		on(Keys.GACHI, "ガチ", "ガチ", "ガチ", "GACHI");
		on(Keys.GORI, "ゴリ", "ゴリ", "ゴリ", "GORI");
		on(Keys.AUTOPLAY, "AUTO PLAY", "オートプレー", "オートプレー", "AUTO PLAY");
		on(Keys.ACHIEVEMENT, "ACHIEVEMENT", "達成率", "たっせいりつ", "ACHIEVEMENT");
		on(Keys.SYNCHRONIZE, "SYNCHRONIZE", "シンクロ率", "シンクロりつ", "SYNCHRONIZE");
		// result
		on(Keys.CLOSE, "CLOSE", "閉じる", "とじる", "CLOSE");
		on(Keys.NEXT, "NEXT", "次へ", "つぎへ", "NEXT");
		on(Keys.DISCONNECT, "DISCONNECT", "通信切断", "せつだん", "DISCONNECT");
		// mainmenu
		on(Keys.SELECT_TRACK, "楽曲選択", "楽曲選択", "あそぶ きょくを えらぶ", "Select Track");
		on(Keys.IMPORT_TRACK, "楽曲管理", "楽曲管理", "がっきょくの かんり", "Import Track");
		on(Keys.ADJUST_DELAY, "遅延調整", "遅延調整", "ちえん ちょうせい", "Adjust Delay");
		on(Keys.TUTORIAL, "チュートリアル", "チュートリアル", "チュートリアル", "Tutorial");
		on(Keys.SYNC_HOST, "シンクモード 親機になる", "シンクモード 親機になる", "シンクモード 1Pになる", "Sync Host");
		on(Keys.SYNC_CLIENT, "シンクモード 子機になる", "シンクモード 子機になる", "シンクモード 2Pになる", "Sync Client");
		// importtrack
		on(Keys.IMPORT_FROM_OFFICIAL_WEB_SITE, "舞平方公式サイトからダウンロードしてインポート", "舞平方公式サイトからダウンロードしてインポート", "まいへいほうの こうしきサイトから ダウンロードして インポート", "Import From Official Web Site");
		on(Keys.IMPORT_FROM_FILELIST_TXT, "filelist.txtからインポート", "filelist.txtからインポート", "filelist.txtからインポート", "Import From filelist.txt");
		on(Keys.IMPORT_FROM_MAIDATA_TXT, "maidata.txtからインポート", "maidata.txtからインポート", "maidata.txtからインポート", "Import From maidata.txt");
		on(Keys.TRACK_MANAGER, "トラックマネージャー", "トラックマネージャー", "トラックマネージャー", "Track Manager");
		on(Keys.TRACK_ID, "ID", "ID", "ID", "ID");
		on(Keys.TITLE, "タイトル", "タイトル", "タイトル", "Title");
		on(Keys.TITLE_RUBY, "タイトル(ふりがな)", "タイトル(ふりがな)", "タイトル(ふりがな)", "Title ruby");
		on(Keys.ARTIST, "アーティスト名", "アーティスト名", "アーティストめい", "Artist");
		on(Keys.ARTIST_RUBY, "アーティスト名(ふりがな)", "アーティスト名(ふりがな)", "アーティストめい(ふりがな)", "Artist ruby");
		on(Keys.BPM, "BPM", "BPM", "BPM", "BPM");
		on(Keys.USE_AUDIO, "音楽ファイル", "音楽ファイル", "おんがくファイル", "Audio");
		on(Keys.USE_JACKET, "ジャケット", "ジャケット", "ジャケット", "Jacket");
		on(Keys.SELECTED, "設定済", "設定済", "せっていずみ", "Selected");
		on(Keys.NOT_SELECTED, "未設定", "未設定", "みせってい", "Not Selected");
		on(Keys.TRACK_MANAGER_SCORE_LIST, "譜面一覧", "譜面一覧", "ふめん いちらん", "Scores");
		on(Keys.SCORE_ID, "ID", "ID", "ID", "ID");
		on(Keys.LEVEL, "レベル", "レベル", "レベル", "Level");
		on(Keys.NOTES_DESIGNER, "譜面製作者", "譜面製作者", "ふめん せいさくしゃ", "Notes Designer");
		on(Keys.TRACK_MANAGER_SCORE, "譜面を表示", "譜面を表示", "ふめんを ひょうじ", "Score");
		// adjustdelay
		on(Keys.MENU, "MENU", "メニュー", "メニュー", "MENU");
		on(Keys.CHANGE_BPM, "CHANGE BPM", "BPM変更", "BPMへんこう", "CHANGE BPM");
		on(Keys.SAVE, "SAVE", "セーブ", "セーブ", "SAVE");
		on(Keys.ADJUST_DERAY_DESCRIPTION_1, "リズムに合わせてリングをタップしてください", "リズムに合わせてリングをタップしてください", "リズムにあわせてリングをタップしてください", "Please tap the ring to match the rhythm.");
		on(Keys.ADJUST_DERAY_DESCRIPTION_2, "発音の遅延時間", "発音の遅延時間", "はつおんの ちえんじかん", "Delay Time");
		on(Keys.ADJUST_DERAY_DESCRIPTION_3, "調整を終えるにはメニューを開いてください↓", "調整を終えるにはメニューを開いてください↓", "ちょうせいできたらメニューからおわれます↓", "Please open the menu to finishthe adjustment.");
		// config key
		on(Keys.SOUND_EFFECT_TYPE, "サウンドエフェクト", "サウンドエフェクト", "サウンドエフェクト", "Sound Effect");
		on(Keys.ANSWER_SOUND_TYPE, "アンサーサウンド", "アンサーサウンド", "アンサーサウンド", "Answer Sound");
		on(Keys.TRUN, "譜面アレンジ:回転", "譜面アレンジ:回転", "ふめんアレンジ:かいてん", "Turn");
		on(Keys.MIRROR, "譜面アレンジ:ミラー", "譜面アレンジ:ミラー", "ふめんアレンジ:ミラー", "Mirror");
		on(Keys.FADE_GUIDE_SPEED, "ガイドスピード (フェードイン)", "ガイドスピード (フェードイン)", "ガイドスピード (フェードイン)", "Guide Speed (Fade)");
		on(Keys.MOVE_GUIDE_SPEED, "ガイドスピード (移動)", "ガイドスピード (移動)", "ガイドスピード (いどう)", "Guide Speed (Move)");
		on(Keys.STAR_ROTATION, "スターローテーション", "スターローテーション", "スターローテーション", "Star Rotation");
		on(Keys.RING_SIZE, "リングサイズ", "リングサイズ", "リングサイズ", "Ring Size");
		on(Keys.SLIDE_MARKER_SIZE, "スライドマーカーサイズ", "スライドマーカーサイズ", "スライドマーカーサイズ", "Slide Marker Size");
		on(Keys.ANGULATED_HOLD, "角ホールド", "角ホールド", "とんがりホールド", "Angulated Hold");
		on(Keys.PINE, "パインマーカー", "パインマーカー", "パインマーカー", "Pine");
		on(Keys.BG_INFO_TYPE, "背景情報表示タイプ", "背景情報表示タイプ", "はいけい ひょうじタイプ", "BG Info");
		on(Keys.UP_SCREEN_ACHIEVEMENT_VISION_TYPE, "上画面達成率表示タイプ", "上画面達成率表示タイプ", "うえがめん たっせいりつ ひょうじタイプ", "Up Screen Achievement Vision");
		on(Keys.RANK_VERSION, "ランク表示バージョン", "ランク表示バージョン", "ランクひょうじバージョン", "Rank Version");
		on(Keys.VISUALIZE_FASTLATE, "FAST/LATEを表示", "FAST/LATEを表示", "FAST/LATEをひょうじ", "Visualize Fast/Late");
		on(Keys.BRIGHTNESS, "背景の明るさ", "背景の明るさ", "はいけいの あかるさ", "Brightness");
		on(Keys.CHANGE_BREAK_TYPE, "譜面アレンジ:BREAK", "譜面アレンジ:BREAK", "ふめんアレンジ:ブレーク", "Change Break Type");
		on(Keys.CHANGE_RING_TYPE, "譜面アレンジ:星", "譜面アレンジ:星", "ふめんアレンジ:スター", "Change Ring Type");
		on(Keys.JUDGE_OFFSET, "遅延調整", "遅延調整", "ちえん ちょうせい", "Judge Offset");
		on(Keys.JUDGEMENT_TYPE, "判定難易度", "判定難易度", "はんてい なんいど", "Judgement Type");
		on(Keys.CHALLENGE_LIFE, "ライフ設定", "ライフ設定", "ライフせってい", "Challenge Life");
		on(Keys.USER_RESOURCES_CACHE_TYPE, "リソースのキャッシュ設定", "リソースのキャッシュ設定", "リソースのキャッシュせってい", "User Resources Cache Type");
		on(Keys.IS_LOAD_JACKET_WHEN_SELECT_TRACK, "楽曲選択でジャケット読込", "楽曲選択でジャケット読込", "がっきょくせんたくでジャケットよみこみ", "Load Jacket When Select Track Scene");
		on(Keys.IS_LOAD_AUDIO_WHEN_SELECT_TRACK, "楽曲選択でオーディオ読込", "楽曲選択でオーディオ読込", "がっきょくせんたくでオーディオよみこみ", "Load Audio When Select Track Scene");
		on(Keys.REALTIME_CREATE_OBJECT, "オブジェクトのリアルタイム生成", "オブジェクトのリアルタイム生成", "オブジェクトをリアルタイムでつくる", "Realtime Create Object");
		on(Keys.SKIP_SLIDE, "アバウトなスライドを許可", "アバウトなスライドを許可", "アバウトなスライドをゆるす", "Allow Skip Slide");
		on(Keys.SENSOR_SIZE, "センサーサイズ", "センサーサイズ", "センサーサイズ", "Sensor Size");
		// config value
		on(Keys.OFF, "OFF", "オフ", "オフ", "OFF");
		on(Keys.ON, "ON", "オン", "オン", "ON");
		on(Keys.ONLY_BREAK2600, "BREAKが2600点の時のみ", "BREAKが2600点の時のみ", "ブレークが2600てんのとき だけ", "Only BREAK2600");
		on(Keys.BASIS, "BASIS", "鳴らす(BASIS)", "ならす(BASIS)", "BASIS");
		on(Keys.BASIS_PLUS, "BASIS+", "鳴らす(BASIS+)", "ならす(BASIS+)", "BASIS+");
		on(Keys.SPECIAL, "SPECIAL", "鳴らす(SPECIAL)", "ならす(SPECIAL)", "SPECIAL");
		on(Keys.SPECIAL_PLUS, "SPECIAL+", "鳴らす(SPECIAL+)", "ならす(SPECIAL+)", "SPECIAL+");
		on(Keys.COMBO, "COMBO", "コンボ", "コンボ", "COMBO");
		on(Keys.PCOMBO, "PCOMBO", "パーフェクトコンボ", "パーフェクトコンボ", "COMBO (PERFECT)");
		on(Keys.BPCOMBO, "BPCOMBO", "パーフェクトコンボ (ボーナス含む)", "パーフェクトコンボ (ボーナスふくむ)", "COMBO (PERFECT+BONUS)");
		on(Keys.SCORE, "SCORE", "スコア", "スコア", "SCORE");
		on(Keys.NORMAL, "NORMAL", "ノーマル", "ノーマル", "NORMAL");
		on(Keys.PACE, "PACE", "ペース", "ペース", "PACE");
		on(Keys.HAZARD_BREAKPACE, "HAZARD (BREAK PACE)", "ハザード (BREAKペース)", "ハザード (ブレークペース)", "HAZARD (BREAK PACE)");
		on(Keys.HAZARD_BREAKPACE, "HAZARD", "ハザード", "ハザード", "HAZARD");
		on(Keys.T_NORMAL, "NORMAL (LIMIT SCORE)", "ノーマル (リミットスコア)", "ノーマル (リミットスコア)", "NORMAL (LIMIT SCORE)");
		on(Keys.T_PACE, "PACE (LIMIT SCORE)", "ペース (リミットスコア)", "ペース (リミットスコア)", "PACE (LIMIT SCORE)");
		on(Keys.T_HAZARD, "HAZARD (LIMIT SCORE)", "ハザード (リミットスコア)", "ハザード (リミットスコア)", "HAZARD (LIMIT SCORE)");
		on(Keys.TAP_TO_BREAK, "TAP -> BREAK", "TAP -> BREAK", "タップをブレークに", "TAP -> BREAK");
		on(Keys.BREAK_TO_TAP, "BREAK -> TAP", "BREAK -> TAP", "ブレークをタップに", "BREAK -> TAP");
		on(Keys.TAP_EXCHANGE_BREAK, "TAP <=> BREAK", "TAP <=> BREAK", "そういれかえ", "TAP <=> BREAK");
		on(Keys.RING_TO_STAR, "RING -> STAR", "〇 -> ☆", "〇を☆に", "RING -> STAR");
		on(Keys.STAR_TO_RING, "STAR -> RING", "☆ -> 〇", "☆を〇に", "STAR -> RING");
		on(Keys.RING_EXCHANGE_STAR, "RING <=> STAR", "〇 <=> ☆", "そういれかえ", "RING <=> STAR");
		on(Keys.PINK, "PiNK", "PiNK", "PiNK", "PiNK");
		on(Keys.CLASSIC, "CLASSIC", "クラシック", "クラシック", "CLASSIC");
		on(Keys.INFINITY, "∞", "∞", "∞", "INFINITY");
		on(Keys.LIFE_1, "1", "1", "1", "1");
		on(Keys.LIFE_5, "5", "5", "5", "5");
		on(Keys.LIFE_10, "10", "10", "10", "10");
		on(Keys.LIFE_20, "20", "20", "20", "20");
		on(Keys.LIFE_50, "50", "50", "50", "50");
		on(Keys.LIFE_100, "100", "100", "100", "100");
		on(Keys.LIFE_300, "300", "300", "300", "300");
		on(Keys.CHACHE_UNUSED, "使わない", "使わない", "つかわない", "UNUSED");
		on(Keys.CHACHE_USED, "使う", "使う", "つかう", "USED");
		on(Keys.CHACHE_BULK, "一括", "一括", "いっかつ", "BULK");
		on(Keys.SEC, "秒", "秒", "びょう", "sec");
		// config key description
		on(Keys.SOUND_EFFECT_TYPE_DESCRIPTION, "リングやスライドを処理したときに鳴る音を設定します", "リングやスライドを処理したときに鳴る音を設定します", "リングや スライドを しょり したときに なる おとを せってい します", "");
		on(Keys.ANSWER_SOUND_TYPE_DESCRIPTION, "リングを処理すべきタイミングで鳴る音を設定します", "リングを処理すべきタイミングで鳴る音を設定します", "リングを しょり すべきタイミングで なる おとを せってい します", "");
		on(Keys.TRUN_DESCRIPTION, "譜面を時計回りに回転させます", "譜面を時計回りに回転させます", "ふめんを とけいまわりに かいてんさせます", "");
		on(Keys.MIRROR_DESCRIPTION, "譜面を左右反転させます", "譜面を左右反転させます", "ふめんを さゆうはんてん させます", "");
		on(Keys.FADE_GUIDE_SPEED_DESCRIPTION, "リングが表示されてから移動を開始するまでの時間を設定します", "リングが表示されてから移動を開始するまでの時間を設定します", "リングが ひょうじされてから いどうを はじめるまでの じかんを せってい します", "");
		on(Keys.MOVE_GUIDE_SPEED_DESCRIPTION, "リングの移動スピードを設定します", "リングの移動スピードを設定します", "リングの いどうスピードを せってい します", "");
		on(Keys.STAR_ROTATION_DESCRIPTION, "スライドの速さに合わせて☆が回転するようになります", "スライドの速さに合わせて☆が回転するようになります", "スライドの はやさに あわせて ☆が かいてん するように なります", "");
		on(Keys.RING_SIZE_DESCRIPTION, "リングの大きさ倍率を設定します", "リングの大きさ倍率を設定します", "リングの おおきさを ばいりつで せってい します", "");
		on(Keys.SLIDE_MARKER_SIZE_DESCRIPTION, "スライドマーカーの大きさ倍率を設定します", "スライドマーカーの大きさ倍率を設定します", "スライドマーカーの おおきさを ばいりつで せってい します", "");
		on(Keys.ANGULATED_HOLD_DESCRIPTION, "ホールドを角ばらせます", "ホールドを角ばらせます", "ホールドをとんがらせます", "");
		on(Keys.PINE_DESCRIPTION, "リングをパインアメにします", "リングをパインアメにします", "リングをパインアメにします", "");
		on(Keys.BG_INFO_TYPE_DESCRIPTION, "プレー中の背景にどんな情報を表示させるかを設定します", "プレー中の背景にどんな情報を表示させるかを設定します", "プレーちゅうの はいけいに どんな じょうほうを ひょうじ させるかを せってい します", "");
		on(Keys.UP_SCREEN_ACHIEVEMENT_VISION_TYPE_DESCRIPTION, "プレー中の上画面に表示する達成率の算出方法を設定します", "プレー中の上画面に表示する達成率の算出方法を設定します", "プレーちゅうの うえがめんに ひょうじする たっせいりつの けいさんほうほうを せってい します", "");
		on(Keys.RANK_VERSION_DESCRIPTION, "ランクの表示タイプを設定します", "ランクの表示タイプを設定します", "ランクの ひょうじタイプを せってい します", "");
		on(Keys.VISUALIZE_FASTLATE_DESCRIPTION, "評価がGreat以下であった場合に、早かったか遅かったかをエフェクトで表示します", "評価がGreat以下であった場合に、早かったか遅かったかをエフェクトで表示します", "ひょうかが グレートいかであったばあいに、はやかったか おそかったかを エフェクトで ひょうじ します", "");
		on(Keys.AUTOPLAY_DESCRIPTION, "自動でプレーさせるかを設定します", "自動でプレーさせるかを設定します", "じどうで プレーさせるかを せってい します", "");
		on(Keys.BRIGHTNESS_DESCRIPTION, "背景の明るさを調節します", "背景の明るさを調節します", "はいけいの あかるさを ちょうせつします", "");
		on(Keys.CHANGE_BREAK_TYPE_DESCRIPTION, "TAPノートとBREAKノートがアレンジされるようになります", "TAPノートとBREAKノートがアレンジされるようになります", "タップノートと ブレークノートが アレンジされるように なります", "");
		on(Keys.CHANGE_RING_TYPE_DESCRIPTION, "TAPノートやBREAKノートのリングとスターがアレンジされるようになります", "TAPノートやBREAKノートのリングとスターがアレンジされるようになります", "タップノートや ブレークノートの リングと スターが アレンジされるように なります", "");
		on(Keys.JUDGE_OFFSET_DESCRIPTION, "判定の時間をずらします", "判定の時間をずらします", "はんていの じかんを ずらします", "");
		on(Keys.JUDGEMENT_TYPE_DESCRIPTION, "判定の厳しさを設定します", "判定の厳しさを設定します", "はんていの きびしさを せってい します", "");
		on(Keys.CHALLENGE_LIFE_DESCRIPTION, "ゲーム中のライフを設定します", "ゲーム中のライフを設定します", "ゲームちゅうの ライフを せってい します", "");
		on(Keys.USER_RESOURCES_CACHE_TYPE_DESCRIPTION, "楽曲の画像ファイルや音楽ファイルのキャッシュを利用するか設定します", "楽曲の画像ファイルや音楽ファイルのキャッシュを利用するか設定します", "がっきょくの がぞうファイルや おんがくファイルの キャッシュを つかうかを せってい します", "");
		on(Keys.IS_LOAD_JACKET_WHEN_SELECT_TRACK_DESCRIPTION, "楽曲選択シーンにおいて、画像ファイルを読み込ませるかを設定します", "楽曲選択シーンにおいて、画像ファイルを読み込ませるかを設定します", "きょくをえらぶシーンで、がぞうファイルを よみこむかを せってい します", "");
		on(Keys.IS_LOAD_AUDIO_WHEN_SELECT_TRACK_DESCRIPTION, "楽曲選択シーンにおいて、音楽ファイルを読み込ませるかを設定します", "楽曲選択シーンにおいて、音楽ファイルを読み込ませるかを設定します", "きょくをえらぶシーンで、おんがくファイルを よみこむかを せってい します", "");
		on(Keys.REALTIME_CREATE_OBJECT_DESCRIPTION, "ノートなどのゲームオブジェクトをリアルタイムに生成するかを設定します", "ノートなどのゲームオブジェクトをリアルタイムに生成するかを設定します", "ノートなどの ゲームオブジェクトを リアルタイムに つくるかを せってい します", "");
		on(Keys.SKIP_SLIDE_DESCRIPTION, "スライドを1段飛ばしで判定してもいいかを設定します", "スライドを1段飛ばしで判定してもいいかを設定します", "スライドを 1こ ぬかして はんていしても いいかを せってい します", "");
		on(Keys.SENSOR_SIZE_DESCRIPTION, "センサーのサイズを設定します", "センサーのサイズを設定します", "センサーの サイズを せってい します", "");
		// config value description
		on(Keys.SOUND_EFFECT_TYPE_ON_DESCRIPTION, "[ON]サウンドエフェクトを鳴らします", "[オン]サウンドエフェクトを鳴らします", "[オン]サウンドエフェクトを ならします", "");
		on(Keys.SOUND_EFFECT_TYPE_OFF_DESCRIPTION, "[OFF]サウンドエフェクトを鳴らしません", "[オフ]サウンドエフェクトを鳴らしません", "[オフ]サウンドエフェクトを ならしません", "");
		on(Keys.SOUND_EFFECT_TYPE_ONLYBREAK2600_DESCRIPTION, "[BREAKが2600点の時のみ]BREAKノートで2600点を取った時のみ、サウンドエフェクトを鳴らします", "[BREAKが2600点の時のみ]BREAKノートで2600点を取った時のみ、サウンドエフェクトを鳴らします", "[ブレークが2600てんのとき だけ]ブレークノートで 2600てんを とったときだけ、サウンドエフェクトを ならします", "");
		on(Keys.ANSWER_SOUND_TYPE_BASIS_DESCRIPTION, "[BASIS]アンサーサウンドを、リングの種類に依らず、鳴らします", "[BASIS]アンサーサウンドを、リングの種類に依らず、鳴らします", "[BASIS]アンサーサウンドを、リングの しゅるいに かかわらず、ならします", "");
		on(Keys.ANSWER_SOUND_TYPE_BASIS_PLUS_DESCRIPTION, "[BASIS+]アンサーサウンドを、BASISに加えてスライドの動き始めにも、音を鳴らします、鳴らします", "[BASIS+]アンサーサウンドを、BASISに加えてスライドの動き始めにも、鳴らします", "[BASIS+]アンサーサウンドを、BASISに くわえて スライドの うごきはじめにも、ならします", "");
		on(Keys.ANSWER_SOUND_TYPE_SPECIAL_DESCRIPTION, "[SPECIAL]リングの種類ごとに別の音を鳴らします", "[SPECIAL]リングの種類ごとに別の音を鳴らします", "[SPECIAL]リングの しゅるいごとに べつの おとを ならします", "");
		on(Keys.ANSWER_SOUND_TYPE_SPECIAL_PLUS_DESCRIPTION, "[SPECIAL+]アンサーサウンドを、SPECIALに加えてスライドの動き始めにも、音を鳴らします、鳴らします", "[SPECIAL+]アンサーサウンドを、SPECIALに加えてスライドの動き始めにも、鳴らします", "[SPECIAL+]アンサーサウンドを、SPECIALに くわえて スライドの うごきはじめにも、ならします", "");
		on(Keys.ANSWER_SOUND_TYPE_OFF_DESCRIPTION, "[OFF]アンサーサウンドを鳴らしません", "[オフ]アンサーサウンドを鳴らしません", "[オフ]アンサーサウンドを ならしません", "");
		on(Keys.TRUN_VALUE_DESCRIPTION, "+1 ～ +7の範囲で指定できます", "+1 ～ +7の範囲で指定できます", "+1 ～ +7の はんいで してい できます", "");
		on(Keys.TRUN_OFF_DESCRIPTION, "+0は回転しない設定です", "+0は回転しない設定です", "+0は かいてん しない せってい です", "");
		on(Keys.MIRROR_ON_DESCRIPTION, "[ON]左右反転させます", "[オン]左右反転させます", "[オン]さゆうはんてん させます", "");
		on(Keys.MIRROR_OFF_DESCRIPTION, "[OFF]左右反転しません", "[オフ]左右反転しません", "[オフ]さゆうはんてん しません", "");
		on(Keys.FADE_GUIDE_SPEED_VALUE_DESCRIPTION, "0.00秒 ～ 2.00秒の範囲で指定できます\n値が小さいほど早くなります", "0.00秒 ～ 2.00秒の範囲で指定できます\n値が小さいほど早くなります", "0.00びょう ～ 2.00びょうの はんいで してい できます\nあたいが ちいさいほど はやくなります", "");
		on(Keys.MOVE_GUIDE_SPEED_VALUE_DESCRIPTION, "0.00秒 ～ 2.00秒の範囲で指定できます\n値が小さいほど早くなります", "0.00秒 ～ 2.00秒の範囲で指定できます\n値が小さいほど早くなります", "0.00びょう ～ 2.00びょうの はんいで してい できます\nあたいが ちいさいほど はやくなります", "");
		on(Keys.STAR_ROTATION_ON_DESCRIPTION, "[ON]回転させます\nスライドの移動スピードが速いほど回転も速くなります", "[オン]回転させます\nスライドの移動スピードが速いほど回転も速くなります", "[オン]かいてん させます\nスライドの いどうスピードが はやいほど かいてんも はやくなります", "");
		on(Keys.STAR_ROTATION_OFF_DESCRIPTION, "[OFF]回転させません", "[オフ]回転させません", "[オフ]回転させません", "");
		on(Keys.RING_SIZE_VALUE_DESCRIPTION, "0.00倍 ～ 2.00倍の範囲で指定できます\n値が大きいほどサイズが大きくなります", "0.00倍 ～ 2.00倍の範囲で指定できます\n値が大きいほどサイズが大きくなります", "0.00ばい ～ 2.00ばいの はんいで してい できます\nあたいが おおきいほど サイズが おおきくなります", "");
		on(Keys.SLIDE_MARKER_SIZE_VALUE_DESCRIPTION, "0.00倍 ～ 2.00倍の範囲で指定できます\n値が大きいほどサイズが大きくなります", "0.00倍 ～ 2.00倍の範囲で指定できます\n値が大きいほどサイズが大きくなります", "0.00ばい ～ 2.00ばいの はんいで してい できます\nあたいが おおきいほど サイズが おおきくなります", "");
		on(Keys.ANGULATED_HOLD_ON_DESCRIPTION, "[ON]ホールドの角が角ばります\nmaimai GreeN PLUS以降のデザインです", "[オン]ホールドの角が角ばります\nmaimai GreeN PLUS以降のデザインです", "[オン]ホールドの かどが かくばります\nmaimai GreeN PLUSいこうの デザインです", "");
		on(Keys.ANGULATED_HOLD_OFF_DESCRIPTION, "[OFF]ホールドの角が丸くなります\nmaimai GreeN以前のデザインです", "[オフ]ホールドの角が丸くなります\nmaimai GreeN以前のデザインです", "[オフ]ホールドの かどが まるくなります\nmaimai GreeNいぜんの デザインです", "");
		on(Keys.PINE_ON_DESCRIPTION, "[ON]リングのデザインがパインアメになります\nmaimai LIVE 2014 洗濯祭における特別なデザインです", "[オン]リングのデザインがパインアメになります\nmaimai LIVE 2014 洗濯祭における特別なデザインです", "[オン]リングの デザインが パインアメになります\nmaimai LIVE 2014 せんたくさいの とくべつな デザインです", "");
		on(Keys.PINE_OFF_DESCRIPTION, "[OFF]リングのデザインが通常のものになります", "[オフ]リングのデザインが通常のものになります", "[オフ]リングの デザインが つうじょうの ものに なります", "");
		on(Keys.BG_INFO_TYPE_COMBO_DESCRIPTION, "[COMBO]現在のコンボ数を表示します\nコンボはミスをしない限り増加していきます", "[コンボ]現在のコンボ数を表示します\nコンボはミスをしない限り増加していきます", "[コンボ]げんざいの コンボすうを ひょうじ します\nコンボは ミスをしないかぎり ふえていきます", "");
		on(Keys.BG_INFO_TYPE_COMBO_DESCRIPTION, "[PCOMBO]現在のパーフェクトコンボ数を表示します\nパーフェクトコンボはパーフェクトを取っている間、増加していきます", "[パーフェクトコンボ]現在のパーフェクトコンボ数を表示します\nパーフェクトコンボはパーフェクトを取っている間、増加していきます", "[パーフェクトコンボ]げんざいの パーフェクトコンボすうを ひょうじ します\nパーフェクトコンボは パーフェクトを とっているあいだ、ふえていきます", "");
		on(Keys.BG_INFO_TYPE_COMBO_DESCRIPTION, "[BPCOMBO]現在のボーナスパーフェクトコンボ数を表示します\nボーナスパーフェクトコンボはパーフェクトかつBREAKノートは2600点を取っている間、増加していきます", "[パーフェクトコンボ (ボーナス含む)]現在のボーナスパーフェクトコンボ数を表示します\nボーナスパーフェクトコンボはパーフェクトかつBREAKノートは2600点を取っている間、増加していきます", "[パーフェクトコンボ (ボーナスふくむ)]げんざいの ボーナスパーフェクトコンボすうを ひょうじ します\nボーナスパーフェクトコンボはパーフェクトかつブレークノートは2600てんを とっているあいだ、ふえていきます", "");
		on(Keys.BG_INFO_TYPE_ACHIEVEMENT_DESCRIPTION, "[ACHIEVEMENT]達成率を表示します", "[達成率]達成率を表示します", "[たっせいりつ]たっせいりつを ひょうじ します", "");
		on(Keys.BG_INFO_TYPE_SCORE_DESCRIPTION, "[SCORE]スコアを表示します", "[スコア]スコアを表示します", "[スコア]スコアを ひょうじ します", "");
		on(Keys.BG_INFO_TYPE_OFF_DESCRIPTION, "[OFF]何も表示しません", "[オフ]何も表示しません", "[オフ]なにも ひょうじ しません", "");
		on(Keys.UP_SCREEN_ACHIEVEMENT_VISION_TYPE_NORMAL_DESCRIPTION, "[NORMAL]0％から始まる、BREAKは2500点を満点とした算出方法です", "[ノーマル]0％から始まる、BREAKは2500点を満点とした算出方法です", "[ノーマル]0％から はじまる、ブレークは2500てんを まんてん とした けいさんほうほう です", "");
		on(Keys.UP_SCREEN_ACHIEVEMENT_VISION_TYPE_PACE_DESCRIPTION, "[PACE]現在までのノートの正確さを使う、BREAKは2500点を満点とした算出方法です", "[ペース]現在までのノートの正確さを使う、BREAKは2500点を満点とした算出方法です", "[ペース]げんざいまでの はんていの せいかくさを つかう、ブレークは2500てんを まんてん とした けいさんほうほう です", "");
		on(Keys.UP_SCREEN_ACHIEVEMENT_VISION_TYPE_HAZARD_BREAKPACE_DESCRIPTION, "[HAZARD (BREAK PACE)]100％から始まり、BREAKによるボーナス点は取った時に加算される、BREAKは2500点を満点とした算出方法です", "[ハザード (BREAKペース)]100％から始まり、BREAKによるボーナス点は取った時に加算される、BREAKは2500点を満点とした算出方法です", "[ハザード (ブレークペース)]100％からはじまり、ブレークによる　ボーナスてんは　とったときに　かさんされる、ブレークは2500てんを まんてん とした けいさんほうほう です", "");
		on(Keys.UP_SCREEN_ACHIEVEMENT_VISION_TYPE_HAZARD_DESCRIPTION, "[HAZARD]理論値％から始まる、BREAKは2500点を満点とした算出方法です", "[ハザード]理論値％から始まる、BREAKは2500点を満点とした算出方法です", "[ハザード]りろんち%からはじまる、ブレークは2500てんを まんてん とした けいさんほうほう です", "");
		on(Keys.UP_SCREEN_ACHIEVEMENT_VISION_TYPE_T_NORMAL_DESCRIPTION, "[NORMAL (LIMIT SCORE)]0％から始まる、BREAKは2600点を満点とした算出方法です\n理論値を取ると100％になります", "[ノーマル (リミットスコア)]0％から始まる、BREAKは2600点を満点とした算出方法です\n理論値を取ると100％になります", "[ノーマル (リミットスコア)]0％から はじまる、ブレークは2600てんを まんてん とした けいさんほうほう です\nりろんちを とると 100％になります", "");
		on(Keys.UP_SCREEN_ACHIEVEMENT_VISION_TYPE_T_PACE_DESCRIPTION, "[PACE (LIMIT SCORE)]現在までのノートの正確さを使う、BREAKは2600点を満点とした算出方法です\n最後のノートを処理した時点で100％なら、理論値です", "[ペース (リミットスコア)]現在までのノートの正確さを使う、BREAKは2600点を満点とした算出方法です\n最後のノートを処理した時点で100％なら、理論値です", "[ペース (リミットスコア)]げんざいまでの はんていの せいかくさを つかう、ブレークは2600てんを まんてん とした けいさんほうほう です\nさいごの ノートを しょりしたじてんで 100％なら りろんち です", "");
		on(Keys.UP_SCREEN_ACHIEVEMENT_VISION_TYPE_T_HAZARD_DESCRIPTION, "[HAZARD (LIMIT SCORE)]100％から始まる、BREAKは2600点を満点とした算出方法です\n最後のノートを処理した時点で100％なら、理論値です", "[ハザード (リミットスコア)]100％から始まる、BREAKは2600点を満点とした算出方法です\n最後のノートを処理した時点で100％なら、理論値です", "[ハザード (リミットスコア)]100％からはじまる、ブレークは2600てんを まんてん とした けいさんほうほう です\nさいごの ノートを しょりしたじてんで 100％なら りろんち です", "");
		on(Keys.RANK_VERSION_PINK_DESCRIPTION, "[PiNK]100％はSSSランクです\nmaimai PiNK以降の表示ルールです", "[PiNK]100％はSSSランクです\nmaimai PiNK以降の表示ルールです", "[PiNK]100％はSSSランクです\nmaimai PiNKいこうの ひょうじルールです", "");
		on(Keys.RANK_VERSION_CLASSIC_DESCRIPTION, "[CLASSIC]100％はSSランクです\nmaimai Orange PLUS以前の表示ルールです", "[クラシック]100％はSSランクです\nmaimai Orange PLUS以前の表示ルールです", "[クラシック]100％はSSランクです\nmaimai Orange PLUSいぜんの ひょうじルールです", "");
		on(Keys.VISUALIZE_FASTLATE_ON_DESCRIPTION, "[ON]リングを早めに処理してしまったら青、遅めだったら赤のエフェクトが表示されます", "[オン]リングを早めに処理してしまったら青、遅めだったら赤のエフェクトが表示されます", "[オン]リングを はやめに しょり してしまったら あお、おそめだったら あかの エフェクトが ひょうじ されます", "");
		on(Keys.VISUALIZE_FASTLATE_ON_DESCRIPTION, "[OFF]早いか遅いかに関わらず、エフェクトの色は評価ごとに変化します", "[オフ]早いか遅いかに関わらず、エフェクトの色は評価ごとに変化します", "[オフ]はやいか おそいかに かかわらず、エフェクトの いろは ひょうかごとに へんか します", "");
		on(Keys.AUTOPLAY_ON_DESCRIPTION, "[ON]自動でプレーします\nすべてのノートがパーフェクト評価になります\n指定した時間から再生する機能、及びリピート再生機能が使えるようになります\n結果は保存されません", "[オン]自動でプレーします\nすべてのノートがパーフェクト評価になります\n指定した時間から再生する機能、及びリピート再生機能が使えるようになります\n結果は保存されません", "[オン]じどうで プレーします\nすべての ノートが パーフェクトになります\nどこからでもさいせいするきのう、および リピートさいせいきのうが つかえるようになります\nけっかは セーブされません", "");
		on(Keys.AUTOPLAY_OFF_DESCRIPTION, "[OFF]自動ではプレーしません", "[オフ]自動ではプレーしません", "[オフ]じどうでは プレーしません", "");
		on(Keys.BRIGHTNESS_VALUE_DESCRIPTION, "0.0倍 ～ 1.0倍の範囲で指定できます\n値が小さいほど暗くなります", "0.0倍 ～ 1.0倍の範囲で指定できます\n値が小さいほど暗くなります", "0.0ばい ～ 1.0ばいの はんいで してい できます\nあたいが ちいさいほど くらくなります", "");
		on(Keys.CHANGE_BREAK_TYPE_TAP_TO_BREAK_DESCRIPTION, "[TAP -> BREAK]すべてのTAPノートがBREAKノートになります\n結果は保存されません", "[TAP -> BREAK]すべてのTAPノートがBREAKノートになります\n結果は保存されません", "[タップをブレークに]すべての タップノートが ブレークノートに なります\nけっかは セーブされません", "");
		on(Keys.CHANGE_BREAK_TYPE_BREAK_TO_TAP_DESCRIPTION, "[BREAK -> TAP]すべてのBREAKノートがTAPノートになります\n結果は保存されません", "[BREAK -> TAP]すべてのBREAKノートがTAPノートになります\n結果は保存されません", "[ブレークをタップに]すべての ブレークノートが タップノートに なります\nけっかは セーブされません", "");
		on(Keys.CHANGE_BREAK_TYPE_TAP_EXCHANGE_BREAK_DESCRIPTION, "[TAP <=> BREAK]すべてのTAPノートとBREAKノートが入れ替わります\n結果は保存されません", "[TAP -> BREAK]すべてのTAPノートとBREAKノートが入れ替わります\n結果は保存されません", "[そういれかえ]すべての タップノートと ブレークノートが いれかわります\nけっかは セーブされません", "");
		on(Keys.CHANGE_BREAK_TYPE_OFF_DESCRIPTION, "[OFF]アレンジしません", "[オフ]アレンジしません", "[オフ]アレンジしません", "");
		on(Keys.CHANGE_RING_TYPE_RING_TO_STAR_DESCRIPTION, "[RING -> STAR]すべてのリングがスターになります", "[〇 -> ☆]すべてのリングがスターになります", "[〇を☆に]すべての リングが スターに なります", "");
		on(Keys.CHANGE_RING_TYPE_STAR_TO_RING_DESCRIPTION, "[STAR -> RING]すべてのスターがリングになります", "[☆ -> 〇]すべてのスターがリングになります", "[☆を〇に]すべての スターが リングに なります", "");
		on(Keys.CHANGE_RING_TYPE_RING_EXCHANGE_STAR_DESCRIPTION, "[RING <=> STAR]すべてのリングとスターが入れ替わります", "[〇 -> ☆]すべてのリングとスターが入れ替わります", "[そういれかえ]すべての リングと スターが いれかわります", "");
		on(Keys.CHANGE_RING_TYPE_OFF_DESCRIPTION, "[OFF]アレンジしません", "[オフ]アレンジしません", "[オフ]アレンジしません", "");
		on(Keys.JUDGE_OFFSET_VALUE_DESCRIPTION, "-5.000秒 ～ +5.000秒の範囲で指定できます", "-5.000秒 ～ +5.000秒の範囲で指定できます", "-5.000びょう ～ +5.000びょうの はんいで してい できます", "");
		on(Keys.JUDGEMENT_TYPE_NORMAL_DESCRIPTION, "[NORMAL]通常の評価範囲です", "[ノーマル]通常の評価範囲です", "[ノーマル]つうじょうの ひょうかはんい です", "");
		on(Keys.JUDGEMENT_TYPE_MAJI_DESCRIPTION, "[マジ]通常よりちょっと狭まった評価範囲です", "[マジ]通常よりちょっと狭まった評価範囲です", "[マジ]つうじょうより ちょっと せまくなった ひょうかはんい です", "");
		on(Keys.JUDGEMENT_TYPE_GACHI_DESCRIPTION, "[ガチ]通常よりかなり狭まった評価範囲です", "[ガチ]通常よりかなり狭まった評価範囲です", "[ガチ]つうじょうより かなり せまくなった ひょうかはんい です", "");
		on(Keys.JUDGEMENT_TYPE_GACHI_DESCRIPTION, "[ゴリ]通常より非常に狭まり、パーフェクト評価以外はすべてミスになる究極の評価範囲です", "[ゴリ]通常より非常に狭まり、パーフェクト評価以外はすべてミスになる究極の評価範囲です", "[ゴリ]つうじょうより ひじょうに せまくなり、パーフェクトいがいは すべてミスになる きゅうきょくの ひょうかはんい です", "");
		on(Keys.CHALLENGE_LIFE_INFINITY_DESCRIPTION, "[∞]ライフを設定しません\nゲームオーバーになりません", "[∞]ライフを設定しません\nゲームオーバーになりません", "[∞]ライフを せってい しません\nゲームオーバーに なりません", "");
		on(Keys.CHALLENGE_LIFE_1_DESCRIPTION, "[1]ライフが1になります\nライフが0になるとゲームオーバーです", "[1]ライフが1になります\nライフが0になるとゲームオーバーです", "[1]ライフが 1になります\nライフが 0になると ゲームオーバーです", "");
		on(Keys.CHALLENGE_LIFE_5_DESCRIPTION, "[5]ライフが5になります\nライフが0になるとゲームオーバーです", "[5]ライフが5になります\nライフが0になるとゲームオーバーです", "[5]ライフが 5になります\nライフが 0になると ゲームオーバーです", "");
		on(Keys.CHALLENGE_LIFE_10_DESCRIPTION, "[10]ライフが10になります\nライフが0になるとゲームオーバーです", "[10]ライフが10になります\nライフが0になるとゲームオーバーです", "[10]ライフが 10になります\nライフが 0になると ゲームオーバーです", "");
		on(Keys.CHALLENGE_LIFE_20_DESCRIPTION, "[20]ライフが20になります\nライフが0になるとゲームオーバーです", "[20]ライフが20になります\nライフが0になるとゲームオーバーです", "[20]ライフが 20になります\nライフが 0になると ゲームオーバーです", "");
		on(Keys.CHALLENGE_LIFE_50_DESCRIPTION, "[50]ライフが50になります\nライフが0になるとゲームオーバーです", "[50]ライフが50になります\nライフが0になるとゲームオーバーです", "[50]ライフが 50になります\nライフが 0になると ゲームオーバーです", "");
		on(Keys.CHALLENGE_LIFE_100_DESCRIPTION, "[100]ライフが100になります\nライフが0になるとゲームオーバーです", "[100]ライフが100になります\nライフが0になるとゲームオーバーです", "[100]ライフが 100になります\nライフが 0になると ゲームオーバーです", "");
		on(Keys.CHALLENGE_LIFE_300_DESCRIPTION, "[300]ライフが300になります\nライフが0になるとゲームオーバーです", "[300]ライフが300になります\nライフが0になるとゲームオーバーです", "[300]ライフが 300になります\nライフが 0になると ゲームオーバーです", "");
		on(Keys.USER_RESOURCES_CACHE_TYPE_UNUSED_DESCRIPTION, "[使わない]キャッシュを利用せず、毎回外部ファイルを読み込みに行きます", "[使わない]キャッシュを利用せず、毎回外部ファイルを読み込みに行きます", "[つかわない]キャッシュをつかわず、ひつようなときに がいふファイルを よみこみます", "");
		on(Keys.USER_RESOURCES_CACHE_TYPE_USED_DESCRIPTION, "[使う]一度読み込んだ外部ファイルをキャッシュし、2回目以降のアクセスを軽くします", "[使う]一度読み込んだ外部ファイルをキャッシュし、2回目以降のアクセスを軽くします", "[つかう]いちど よみこんだ がいぶファイルを キャッシュして、2かいめ いこうの アクセスを かるくします", "");
		on(Keys.USER_RESOURCES_CACHE_TYPE_BULK_DESCRIPTION, "[一括]楽曲選択シーンのローディングですべての外部ファイルを一括で読み込み、キャッシュします\n処理が完了するまでは重たいですが、処理が終わった後は軽いです\nメモリを大幅に食うので、特に音楽ファイルが大量に存在する場合はアプリが落ちてしまうため注意してください", "[一括]楽曲選択シーンのローディングですべての外部ファイルを一括で読み込み、キャッシュします\n処理が完了するまでは重たいですが、処理が終わった後は軽いです\nメモリを大幅に食うので、特に音楽ファイルが大量に存在する場合はアプリが落ちてしまうため注意してください", "[いっかつ]がっきょくせんたくシーンの ローディングで すべての がいぶファイルを いちどに よみこみ、キャッシュします\nしょりが かんりょうするまでは おもたいですが、しょりが おわったあとは かるいです\nメモリを たくさん つかうので、とくに おんがくファイルが おおいと アプリが おちるので ちゅういしてください", "");
		on(Keys.IS_LOAD_JACKET_WHEN_SELECT_TRACK_ON_DESCRIPTION, "[ON]楽曲選択シーンで読み込みます", "[オン]楽曲選択シーンで読み込みます", "[オン]がっきょくせんたくシーンでよみこみます", "");
		on(Keys.IS_LOAD_JACKET_WHEN_SELECT_TRACK_OFF_DESCRIPTION, "[ON]楽曲選択シーンでは読み込みません", "[オン]楽曲選択シーンでは読み込みません", "[オン]がっきょくせんたくシーンではよみこみません", "");
		on(Keys.IS_LOAD_AUDIO_WHEN_SELECT_TRACK_ON_DESCRIPTION, "[ON]楽曲選択シーンで読み込みます", "[オン]楽曲選択シーンで読み込みます", "[オン]がっきょくせんたくシーンでよみこみます", "");
		on(Keys.IS_LOAD_AUDIO_WHEN_SELECT_TRACK_OFF_DESCRIPTION, "[ON]楽曲選択シーンでは読み込みません", "[オン]楽曲選択シーンでは読み込みません", "[オン]がっきょくせんたくシーンではよみこみません", "");
		on(Keys.REALTIME_CREATE_OBJECT_ON_DESCRIPTION, "[ON]オブジェクトは必要になったら生成されます\nその都度、負荷が掛かります", "[オン]オブジェクトは必要になったら生成されます\nその都度、負荷が掛かります", "[オン]オフジェクトは ひつようになったら つくられます\nそのたびに ふかが かかります", "");
		on(Keys.REALTIME_CREATE_OBJECT_OFF_DESCRIPTION, "[OFF]オブジェクトを最初にすべて生成します\n開始に時間が掛かりますが、ゲーム中は滑らかです", "[オフ]オブジェクトを最初にすべて生成します\n開始に時間が掛かりますが、ゲーム中は滑らかです", "[オフ]オフジェクトを さいしょに すべて つくります\nはじめるのに じかんが かかりますが、ゲームちゅうは なめらかです", "");
		on(Keys.SKIP_SLIDE_ON_DESCRIPTION, "[ON]1つ飛ばしでも判定されるようになりますが、譜面によっては誤反応が起こる場合があります", "[オン]1つ飛ばしでも判定されるようになりますが、譜面によっては誤反応が起こる場合があります", "[オン]1つとばしでも はんてい されるようになりますが、ふめんによっては まきこむばあいがあります", "");
		on(Keys.SKIP_SLIDE_OFF_DESCRIPTION, "[OFF]誤反応が起こりづらいですが、関連するすべてのセンサーを正確に触る必要があります", "[オフ]誤反応が起こりづらいですが、関連するすべてのセンサーを正確に触る必要があります", "[オフ]まきこみが おこりづらいですが、かんれんする すべての センサーを せいかくに さわる ひつようが あります", "");
		on(Keys.SENSOR_SIZE_VALUE_DESCRIPTION, "センサーの半径を30ピクセル ～ 100ピクセルの範囲で指定できます", "センサーの半径を30ピクセル ～ 100ピクセルの範囲で指定できます", "センサーの はんけいを 30ピクセル ～ 100ピクセルの はんいで してい できます", "");
		// profile key
		on(Keys.PLAYER_NAME, "プレイヤー名", "プレイヤー名", "おなまえ", "Player Name");
		on(Keys.PLAYER_TITLE, "称号", "称号", "ふたつな", "Player Title");
		on(Keys.PLAYER_ICON, "アイコン", "アイコン", "アイコン", "Player Icon");
		on(Keys.PLAYER_PLATE, "プレート", "プレート", "プレート", "Player Plate");
		on(Keys.PLAYER_FRAME, "フレーム", "フレーム", "フレーム", "Player Frame");
		on(Keys.SORT_TYPE, "楽曲の並べ替えタイプ", "楽曲の並べ替えタイプ", "がっきょくの ならべかえタイプ", "Sort Type");
		on(Keys.REFINE_LEVEL, "楽曲をレベルで絞り込み", "楽曲をレベルで絞り込み", "がっきょくを レベルで しぼりこみ", "Refine Level");
		on(Keys.REFINE_DIFFICULTY, "楽曲を難易度で絞り込み", "楽曲を難易度で絞り込み", "がっきょくを なんいどで しぼりこみ", "Refine Difficulty");
		on(Keys.REFINE_DESIGNER, "楽曲を制作者で絞り込み", "楽曲を制作者で絞り込み", "がっきょくを せいさくしゃで しぼりこみ", "Refine Notes Designer");
		on(Keys.REFINE_NOMISS, "楽曲をフルコンボで絞り込み", "楽曲をフルコンボで絞り込み", "がっきょくを フルコンボで しぼりこみ", "Refine Full Combo");
		on(Keys.REFINE_FC, "楽曲をフルコンボ(金)で絞り込み", "楽曲をフルコンボ(金)で絞り込み", "がっきょくを フルコンボ(きん)で しぼりこみ", "Refine Full Combo (Gold)");
		on(Keys.REFINE_AP, "楽曲をオールパーフェクトで絞り込み", "楽曲をオールパーフェクトで絞り込み", "がっきょくを オールパーフェクトで しぼりこみ", "Refine All Perfect");
		// profile value
		on(Keys.SORT_IMPORTED, "インポートした順", "インポートした順", "インポートした じゅんばん", "IMPORTED");
		on(Keys.SORT_TITLE, "タイトル順", "タイトル順", "タイトルじゅん", "TITLE");
		on(Keys.SORT_ARTIST, "アーティスト順", "アーティスト順", "アーティストじゅん", "ARTIST");
		on(Keys.SORT_ACHIEVEMENT, "達成率順", "達成率順", "たっせいりつじゅん", "ACHIEVEMENT");
		on(Keys.SORT_SCORE, "スコア順", "スコア順", "スコアじゅん", "SCORE");
		on(Keys.SORT_LEVEL, "レベル順", "レベル順", "レベルじゅん", "LEVEL");
		on(Keys.SORT_PLAYCOUNT, "プレー回数順", "プレー回数順", "プレーかいすうじゅん", "PLAY COUNT");
		on(Keys.ACHIEVED, "達成", "達成", "たっせい", "ACHIEVED");
		on(Keys.NOT_ACHIEVED, "未達成", "未達成", "みたっせい", "NOT ACHIEVED");
		// profile key description
		on(Keys.PLAYER_NAME_DESCRIPTION, "プレイヤーの名前を設定します", "プレイヤーの名前を設定します", "あなたの おなまえを せってい します", "");
		on(Keys.PLAYER_TITLE_DESCRIPTION, "プレイヤーの称号と色を設定します", "プレイヤーの称号と色を設定します", "あなたの ふたつな と いろ を せってい します", "");
		on(Keys.PLAYER_ICON_DESCRIPTION, "プレイヤーのアイコンを設定します", "プレイヤーのアイコンを設定します", "あなたの アイコンを せってい します", "");
		on(Keys.PLAYER_PLATE_DESCRIPTION, "画面下のプレイヤー情報領域の背景を設定します", "画面下のプレイヤー情報領域の背景を設定します", "がめんしたの プレイヤーじょうほうの はいけいを せってい します", "");
		on(Keys.PLAYER_FRAME_DESCRIPTION, "楽曲を選んでいない状態の画面中央の背景を設定します", "楽曲を選んでいない状態の画面中央の背景を設定します", "がっきょくを えらんでいないじょうたいの がめんちゅうおうの はいけいを せってい します", "");
		on(Keys.SORT_TYPE_DESCRIPTION, "楽曲の並び順を設定します", "楽曲の並び順を設定します", "がっきょくの ならびじゅんを せってい します", "");
		on(Keys.REFINE_LEVEL_DESCRIPTION, "楽曲をレベルで絞り込みます", "楽曲をレベルで絞り込みます", "がっきょくを レベルで しぼりこみます", "");
		on(Keys.REFINE_DIFFICULTY_DESCRIPTION, "楽曲を難易度で絞り込みます", "楽曲を難易度で絞り込みます", "がっきょくを なんいどで しぼりこみます", "");
		on(Keys.REFINE_DESIGNER_DESCRIPTION, "楽曲を制作者で絞り込みます", "楽曲を制作者で絞り込みます", "がっきょくを せいさくしゃで しぼりこみます", "");
		on(Keys.REFINE_NOMISS_DESCRIPTION, "フルコンボを達成しているか否かで楽曲を絞り込みます", "フルコンボを達成しているか否かで楽曲を絞り込みます", "フルコンボを たっせいしているかどうかで がっきょくを しぼりこみます", "");
		on(Keys.REFINE_FC_DESCRIPTION, "Goodがないフルコンボを達成しているか否かで楽曲を絞り込みます", "Goodがないフルコンボを達成しているか否かで楽曲を絞り込みます", "グッドはんていがないフルコンボを たっせいしているかどうかで がっきょくを しぼりこみます", "");
		on(Keys.REFINE_AP_DESCRIPTION, "オールパーフェクトを達成しているか否かで楽曲を絞り込みます", "オールパーフェクトを達成しているか否かで楽曲を絞り込みます", "オールパーフェクトを たっせいしているかどうかで がっきょくを しぼりこみます", "");
		// profile value description
		on(Keys.SORT_IMPORTED_DESCRIPTION, "[インポートした順]インポートした順に並び替えます", "[インポートした順]インポートした順に並び替えます", "[インポートした じゅんばん]インポートした じゅんばんに ならびかえます", "");
		on(Keys.SORT_TITLE_DESCRIPTION, "[タイトル順]タイトル順に並び替えます", "[タイトル順]タイトル順に並び替えます", "[タイトルじゅん]タイトルじゅんに ならびかえます", "");
		on(Keys.SORT_ARTIST_DESCRIPTION, "[アーティスト順]アーティスト順に並び替えます", "[アーティスト順]アーティスト順に並び替えます", "[アーティストじゅん]アーティストじゅんに ならびかえます", "");
		on(Keys.SORT_ACHIEVEMENT_DESCRIPTION, "[達成率順]達成率が高い順に並び替えます", "[達成率順]達成率が高い順に並び替えます", "[たっせいりつじゅん]たっせいりつが たかいじゅんに ならびかえます", "");
		on(Keys.SORT_SCORE_DESCRIPTION, "[スコア順]スコアが高い順に並び替えます", "[スコア順]スコアが高い順に並び替えます", "[スコアじゅん]スコアが たかいじゅんに ならびかえます", "");
		on(Keys.SORT_LEVEL_DESCRIPTION, "[レベル順]レベル順に並び替えます", "[レベル順]レベル順に並び替えます", "[レベルじゅん]レベルじゅんに ならびかえます", "");
		on(Keys.SORT_PLAYCOUNT_DESCRIPTION, "[プレー回数順]プレー回数が多い順に並び替えます", "[プレー回数順]プレー回数が多い順に並び替えます", "[プレーかいすうじゅん]プレーかいすうが おおいじゅんに ならびかえます", "");
		on(Keys.REFINE_ACHIEVED_DESCRIPTION, "[達成]条件を達成している楽曲に絞り込みます", "[達成]条件を達成している楽曲に絞り込みます", "[たっせい]じょうけんを たっせいしている がっきょくに しぼりこみます", "");
		on(Keys.REFINE_NOT_ACHIEVED_DESCRIPTION, "[未達成]条件を達成していない楽曲に絞り込みます", "[未達成]条件を達成していない楽曲に絞り込みます", "[みたっせい]じょうけんを たっせいしていない がっきょくに しぼりこみます", "");
		on(Keys.REFINE_OFF_DESCRIPTION, "[OFF]この条件では絞り込みしません", "[オフ]この条件では絞り込みしません", "[オフ]このじょうけんでは しぼりこみ しません", "");
		// loading
		on(Keys.NOWLOADING, "NOW LOADING...", "読込中...", "ロードちゅう...", "NOW LOADING...");
		// syncmode
		on(Keys.DISCONNECT_HOST, "親機との接続が切れました。\nタイトルに戻ります。", "親機との接続が切れました。\nタイトルに戻ります。", "1Pとのせつぞくがきれました。\nタイトルにもどります。", "Disconnected from host.\nReturn to title.");
		on(Keys.DISCONNECT_CLIENT, "子機との接続が切れました。\nタイトルに戻ります。", "子機との接続が切れました。\nタイトルに戻ります。", "2Pとのせつぞくがきれました。\nタイトルにもどります。", "Disconnected from client.\nReturn to title.");
		on(Keys.YOUR_IP_ADDRESS, "IPアドレス", "IPアドレス", "IPアドレス", "Your IP Address");
		on(Keys.YOUR_PORT_NUMBER, "ポート番号", "ポート番号", "ポートばんごう", "Your Port Number");
		on(Keys.HOST_IP_ADDRESS, "親機のIPアドレスを入力", "親機のIPアドレスを入力", "1PのIPアドレスを入力", "Host IP Address");
		on(Keys.HOST_PORT_NUMBER, "親機のポート番号を入力", "親機のポート番号を入力", "1Pのポートばんごうをにゅうりょく", "Host Port Number");
		on(Keys.CONNECTION, "CONNECTION", "接続開始", "せつぞく", "CONNECTION");
		on(Keys.CONNECTION_ESTABLISHED, "接続に成功しました", "接続に成功しました", "せつぞくかんりょう！", "Connection established !");
		on(Keys.HOST_HAVE_SELECTION_THE_SCORE, "親機が遊ぶ譜面を選んでいます…", "親機が遊ぶ譜面を選んでいます…", "1Pせんたくちゅう…", "Host have selecting the score...");
		on(Keys.PLEASE_WAIT, "データ送信中…", "データ送信中…", "データそうしんちゅう…", "Please Wait...");
		on(Keys.DOWNLOADING, "データ受信中…", "データ受信中…", "データじゅしんちゅう…", "Downloading...");
		on(Keys.WAITING_FOR_CLIENT, "子機を待っています…", "子機を待っています…", "つうしんたいきちゅう…", "Waiting for client...");
		on(Keys.WAITING_FOR_HOST, "親機を待っています…", "親機を待っています…", "つうしんたいきちゅう…", "Waiting for host...");





		
		// msqエラーとかも翻訳する.英語はerrorコードも込みで.















		string pathPrefix = Path.Combine(Application.dataPath, "Resources/Language/");
		string json;
		json = JsonConvert.SerializeObject (jpdefault);
		File.WriteAllText (pathPrefix + "japanese_default.json", json, System.Text.Encoding.UTF8);
		json = JsonConvert.SerializeObject (jponly___);
		File.WriteAllText (pathPrefix + "japanese_only.json", json, System.Text.Encoding.UTF8);
		json = JsonConvert.SerializeObject (jpkana___);
		File.WriteAllText (pathPrefix + "japanese_kana.json", json, System.Text.Encoding.UTF8);
		json = JsonConvert.SerializeObject (english__);
		File.WriteAllText (pathPrefix + "english.json", json, System.Text.Encoding.UTF8);
	}
}
