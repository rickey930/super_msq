﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ConfigSceneSensorSamples : MonoBehaviour {

	public GameObject sensorSamplePrefab;
	public RectTransform[] sensorSamples { get; set; }

	public void Create () {
		float size = Constants.instance.SENSOR_RADIUS * 2;
		sensorSamples = new RectTransform[17];
		for (int i = 0; i < 8; i++) {
			var sampleObj = Instantiate(sensorSamplePrefab) as GameObject;
			sampleObj.name = "Sensor Sample Outer " + (i + 1).ToString();
			sampleObj.transform.SetParentEx (this.transform);
			sampleObj.transform.localRotation = Quaternion.Euler (new Vector3 (0, 0, Constants.instance.ToLeftHandedCoordinateSystemDegree(Constants.instance.GetPieceDegree(i))));
			var image = sampleObj.GetComponentInChildren<Image> ();
			var rectTransfom = image.transform as RectTransform;
			rectTransfom.anchoredPosition = new Vector2(0, Constants.instance.MAIMAI_OUTER_RADIUS);
			rectTransfom.sizeDelta = new Vector2(size, size);
			image.sprite = MaipadDynamicCreatedSpriteTank.instance.sensorSizeSample;
			sensorSamples[i] = rectTransfom;
		}
		for (int i = 0; i < 8; i++) {
			var sampleObj = Instantiate(sensorSamplePrefab) as GameObject;
			sampleObj.name = "Sensor Sample Inner " + (i + 1).ToString();
			sampleObj.transform.SetParentEx (this.transform);
			sampleObj.transform.localRotation = Quaternion.Euler (new Vector3 (0, 0, Constants.instance.ToLeftHandedCoordinateSystemDegree(Constants.instance.GetPieceDegree(i))));
			var image = sampleObj.GetComponentInChildren<Image> ();
			var rectTransfom = image.transform as RectTransform;
			rectTransfom.anchoredPosition = new Vector2(0, Constants.instance.MAIMAI_INNER_RADIUS);
			rectTransfom.sizeDelta = new Vector2(size, size);
			image.sprite = MaipadDynamicCreatedSpriteTank.instance.sensorSizeSample;
			sensorSamples[i + 8] = rectTransfom;
		}
		{
			int i = 16;
			var sampleObj = Instantiate(sensorSamplePrefab) as GameObject;
			sampleObj.name = "Sensor Sample Center";
			sampleObj.transform.SetParentEx (this.transform);
			sampleObj.transform.localRotation = Quaternion.Euler (Vector3.zero);
			var image = sampleObj.GetComponentInChildren<Image> ();
			var rectTransfom = image.transform as RectTransform;
			rectTransfom.anchoredPosition = Vector2.zero;
			rectTransfom.sizeDelta = new Vector2(size, size);
			image.sprite = MaipadDynamicCreatedSpriteTank.instance.sensorSizeSample;
			sensorSamples[i] = rectTransfom;
		}
	}

	public void SetSize (float radius) {
		float size = radius * 2;
		foreach (var sample in sensorSamples) {
			sample.sizeDelta = new Vector2(size, size);
		}
	}

	// Use this for initialization
	IEnumerator Start () {
		while (MaipadDynamicCreatedSpriteTank.instance.sensorSizeSample == null) {
			yield return null;
		}
		if (sensorSamples == null) {
			Create ();
		}
	}
}
