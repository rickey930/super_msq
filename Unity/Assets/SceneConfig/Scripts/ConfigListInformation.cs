using UnityEngine;

public class ConfigListInformation {
	public ConfigCategory.CategoryId id { get; set; }
	public string key { get; set; }
	public string value { get; set; }
	public Color valueColor { get; set; }

	public ConfigListInformation(ConfigCategory.CategoryId id, string key, string value, Color valueColor) {
		this.id = id;
		this.key = key;
		this.value = value;
		this.valueColor = valueColor;
	}

	public void SetValue (string value, Color valueColor) {
		this.value = value;
		this.valueColor = valueColor;
	}
}

public static class ConfigCategory {
	public enum CategoryId {
		NONE,
		SOUND_EFFECT_TYPE,
		ANSWER_SOUND_TYPE,
		TURN,
		MIRROR,
		GUIDE_SPEED_FADE,
		GUIDE_SPEED_MOVE,
		STAR_ROTATION,
		RING_SIZE,
		MARKER_SIZE,
		ANGULATED_HOLD,
		PINE,
		BACKGROUND_INFOMATION_TYPE,
		ACHIEVEMENT_VISION_TYPE,
		RANK_VERSION_TYPE,
		VISUALIZE_FAST_LATE,
		AUTO_PLAY,
		BRIGHTNESS,
		CHANGE_BREAK_TYPE,
		JUDGE_OFFSET,
		JUDGEMENT_TYPE,
		USER_RESOURCES_CACHE_TYPE,
		IS_LOAD_JACKET_WHEN_SELECT_TRACK,
		IS_LOAD_AUDIO_WHEN_SELECT_TRACK,
		SENSOR_SIZE,
	}
	public static CategoryId ToConfigCategory(this int value) {
		return (CategoryId)value;
	}
	public static int ToInt(this CategoryId value) {
		return (int)value;
	}

	public static Color ToConfigValueColor (this ConfigTypes.SoundEffect value) {
		if (value == ConfigTypes.SoundEffect.OFF)
			return Color.cyan;
		return Color.red;
	}
	public static Color ToConfigValueColor (this ConfigTypes.AnswerSound value) {
		if (value == ConfigTypes.AnswerSound.OFF)
			return Color.cyan;
		return Color.red;
	}
	public static Color ToConfigValueColor (this ConfigTypes.BackGroundInfomation value) {
		if (value == ConfigTypes.BackGroundInfomation.OFF)
			return Color.cyan;
		return Color.red;
	}
	public static Color ToConfigValueColor (this ConfigTypes.UpScreenAchievementVision value) {
		if (value == ConfigTypes.UpScreenAchievementVision.NORMAL)
			return Color.cyan;
		return Color.red;
	}
	public static Color ToConfigValueColor (this ConfigTypes.ChangeBreak value) {
		if (value == ConfigTypes.ChangeBreak.NORMAL)
			return Color.cyan;
		return Color.red;
	}
	public static Color ToConfigValueColor (this ConfigTypes.Judgement value) {
		switch (value) {
		case ConfigTypes.Judgement.NORMAL:
			return Color.cyan;
		case ConfigTypes.Judgement.MAJI:
			return Color.yellow;
		case ConfigTypes.Judgement.GACHI:
			return Color.red;
		case ConfigTypes.Judgement.GORI:
			return Color.magenta;
		}
		return Color.red;
	}
	public static Color ToConfigValueColor (this ConfigTypes.RankVersion value) {
		if (value == ConfigTypes.RankVersion.CLASSIC)
			return Color.cyan;
		return Color.red;
	}
	public static Color ToConfigValueColor (this ConfigTypes.ResourcesCacheType value) {
		switch (value) {
		case ConfigTypes.ResourcesCacheType.UNUSED:
			return Color.cyan;
		case ConfigTypes.ResourcesCacheType.USED:
			return Color.red;
		case ConfigTypes.ResourcesCacheType.BULK:
			return Color.yellow;
		}
		return Color.white;
	}
	public static Color ToConfigValueColor (this bool value) {
		if (!value)
			return Color.cyan;
		return Color.red;
	}
	public static Color ToConfigValueColor (this int value) {
		if (value == 0)
			return Color.cyan;
		return Color.red;
	}

	public static string ToConfigValueString (this ConfigTypes.SoundEffect value) {
		switch (value) {
		case ConfigTypes.SoundEffect.ONLY_BREAK2600:
			return "BREAK2600 ONLY";
		}
		return value.ToString ();
	}
	public static string ToConfigValueString (this ConfigTypes.AnswerSound value) {
		switch (value) {
		case ConfigTypes.AnswerSound.BASIS_PLUS:
			return "BASIS PLUS";
		case ConfigTypes.AnswerSound.SPECIAL_PLUS:
			return "SPECIAL PLUS";
		}
		return value.ToString ();
	}
	public static string ToConfigValueString (this ConfigTypes.UpScreenAchievementVision value) {
		switch (value) {
		case ConfigTypes.UpScreenAchievementVision.HAZARD_BREAKPACE:
			return "HAZARD(BREAK PACE)";
		case ConfigTypes.UpScreenAchievementVision.T_NORMAL:
			return "NORMAL(LIMIT SCORE)";
		case ConfigTypes.UpScreenAchievementVision.T_PACE:
			return "PACE(LIMIT SCORE)";
		case ConfigTypes.UpScreenAchievementVision.T_HAZARD:
			return "HAZARD(LIMIT SCORE)";
		}
		return value.ToString ();
	}
	public static string ToConfigValueString (this ConfigTypes.ChangeBreak value) {
		switch (value) {
		case ConfigTypes.ChangeBreak.TAP_TO_BREAK:
			return "TAP -> BREAK";
		case ConfigTypes.ChangeBreak.BREAK_TO_TAP:
			return "BREAK -> TAP";
		case ConfigTypes.ChangeBreak.TAP_EXCHANGE_BREAK:
			return "TAP <=> BREAK";
		}
		return value.ToString ();
	}
	public static string ToConfigValueString (this ConfigTypes.Judgement value) {
		switch (value) {
		case ConfigTypes.Judgement.MAJI:
			return "マジ";
		case ConfigTypes.Judgement.GACHI:
			return "ガチ";
		case ConfigTypes.Judgement.GORI:
			return "ゴリ";
		}
		return value.ToString ();
	}
	public static string ToConfigValueString (this ConfigTypes.RankVersion value) {
		switch (value) {
		case ConfigTypes.RankVersion.PINK:
			return "PiNK";
		}
		return value.ToString ();
	}
	public static string ToConfigValueString (this ConfigTypes.ResourcesCacheType value) {
		return value.ToString ();
	}
	public static string ToConfigValueString (this bool value) {
		if (value)
			return "ON";
		return "OFF";
	}
	public static string ToConfigValueYesNoString (this bool value) {
		if (value)
			return "YES";
		return "NO";
	}
	public static string ToConfigValuePlusMinusString (this int value) {
		if (value < 0)
			return value.ToString();
		return "+" + value.ToString();
	}

}