using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ConfigSceneManager : MonoBehaviour {

	[SerializeField]
	private ConfigListController listCtrl;
	[SerializeField]
	private UnityEngine.UI.Image guideCircle;
	[SerializeField]
	private GameObject sensorUIPrefab;
	[SerializeField]
	private RectTransform sensorUIParent;
	[SerializeField]
	private ConfigSceneDetailUIController detailUICtrl;
	[SerializeField]
	private ConfigSceneSensorSamples sensorSampleCtrl;
	[SerializeField]
	private FadeController fade;

	private readonly Color defaultColor = Color.red;
	private Sprite[] sensorIcons;
	
	private Dictionary<ConfigCategory.CategoryId, ConfigListInformation> configContents { get; set; }
	public ConfigListInformation[] contents { get; set; }
	public bool forceUpdateContents { get; set; }

	private ConfigInformation configWork { get; set; }
	private SensorUIImageController[] sensorImages { get; set; }
	public ConfigCategory.CategoryId selectedId { get; set; }
	private float sensorTurboSpeed0 { get; set; }
	private float sensorTurboSpeed3 { get; set; }
	private float sensorTurboFirstSpeed { get { return 65.0f; } }
	private float sensorTurboAddSpeed { get { return 180.0f * Time.deltaTime; } }
	private float sensorTurboMaxSpeed { get { return sensorTurboFirstSpeed * 1.5f; } }

	// Use this for initialization
	IEnumerator Start () {
		while (UserDataController.instance == null || UserDataController.instance.config == null) {
			yield return null;
		}
		
		sensorIcons = new Sprite[] {
			SpriteTank.Get ("icon_allow_up"),
			SpriteTank.Get ("icon_level_plus"),
			SpriteTank.Get ("icon_level_minus"),
			SpriteTank.Get ("icon_allow_down"),
			SpriteTank.Get ("icon_step_decide"),
			SpriteTank.Get ("icon_step_cancel"),
			SpriteTank.Get ("icon_profile"),
			SpriteTank.Get ("icon_dammy"),
		};

		selectedId = ConfigCategory.CategoryId.NONE;
		configWork = ConfigInformation.Clone (UserDataController.instance.config);

		configContents = new Dictionary<ConfigCategory.CategoryId, ConfigListInformation> () {
			{ ConfigCategory.CategoryId.SOUND_EFFECT_TYPE, new ConfigListInformation(ConfigCategory.CategoryId.SOUND_EFFECT_TYPE, "Sound Effect Type", configWork.sound_effect_type.ToSoundEffectType().ToConfigValueString(), configWork.sound_effect_type.ToSoundEffectType().ToConfigValueColor()) },
			{ ConfigCategory.CategoryId.ANSWER_SOUND_TYPE, new ConfigListInformation(ConfigCategory.CategoryId.ANSWER_SOUND_TYPE, "Answer Sound Type", configWork.answer_sound_type.ToAnswerSoundType().ToConfigValueString(), configWork.answer_sound_type.ToAnswerSoundType().ToConfigValueColor()) },
			{ ConfigCategory.CategoryId.TURN, new ConfigListInformation(ConfigCategory.CategoryId.TURN, "Turn", configWork.turn.ToConfigValuePlusMinusString(), configWork.turn.ToConfigValueColor()) },
			{ ConfigCategory.CategoryId.MIRROR, new ConfigListInformation(ConfigCategory.CategoryId.MIRROR, "Mirror", configWork.mirror.ToConfigValueString(), configWork.mirror.ToConfigValueColor()) },
			{ ConfigCategory.CategoryId.GUIDE_SPEED_FADE, new ConfigListInformation(ConfigCategory.CategoryId.GUIDE_SPEED_FADE, "Guide Speed (Fade)", configWork.fade_guide_speed.ToString ("F2") + " sec", defaultColor) },
			{ ConfigCategory.CategoryId.GUIDE_SPEED_MOVE, new ConfigListInformation(ConfigCategory.CategoryId.GUIDE_SPEED_MOVE, "Guide Speed (Move)", configWork.move_guide_speed.ToString ("F2") + " sec", defaultColor) },
			{ ConfigCategory.CategoryId.STAR_ROTATION, new ConfigListInformation(ConfigCategory.CategoryId.STAR_ROTATION, "Star Rotation", configWork.star_rotation.ToConfigValueString(), configWork.star_rotation.ToConfigValueColor()) },
			{ ConfigCategory.CategoryId.RING_SIZE, new ConfigListInformation(ConfigCategory.CategoryId.RING_SIZE, "Ring Size", "x" + configWork.ring_size.ToString ("F2"), defaultColor) },
			{ ConfigCategory.CategoryId.MARKER_SIZE, new ConfigListInformation(ConfigCategory.CategoryId.MARKER_SIZE, "Slide Marker Size", "x" + configWork.marker_size.ToString ("F2"), defaultColor) },
			{ ConfigCategory.CategoryId.ANGULATED_HOLD, new ConfigListInformation(ConfigCategory.CategoryId.ANGULATED_HOLD, "Angulated Hold", configWork.angulated_hold.ToConfigValueString(), configWork.angulated_hold.ToConfigValueColor()) },
			{ ConfigCategory.CategoryId.PINE, new ConfigListInformation(ConfigCategory.CategoryId.PINE, "Pine", configWork.pine.ToConfigValueString(), configWork.pine.ToConfigValueColor()) },
			{ ConfigCategory.CategoryId.BACKGROUND_INFOMATION_TYPE, new ConfigListInformation(ConfigCategory.CategoryId.BACKGROUND_INFOMATION_TYPE, "Bg Info Type", configWork.background_infomation_type.ToBackGroundInfomationType().ToString(), configWork.background_infomation_type.ToBackGroundInfomationType().ToConfigValueColor()) },
			{ ConfigCategory.CategoryId.ACHIEVEMENT_VISION_TYPE, new ConfigListInformation(ConfigCategory.CategoryId.ACHIEVEMENT_VISION_TYPE, "Up Screen Achievement Vision Type", configWork.up_screen_achievement_vision_type.ToUpScreenAchievementVisionType().ToConfigValueString(), configWork.up_screen_achievement_vision_type.ToUpScreenAchievementVisionType().ToConfigValueColor()) },
			{ ConfigCategory.CategoryId.RANK_VERSION_TYPE, new ConfigListInformation(ConfigCategory.CategoryId.RANK_VERSION_TYPE, "Rank Version", configWork.rank_version_type.ToRankVersionType().ToConfigValueString(), configWork.rank_version_type.ToRankVersionType().ToConfigValueColor()) },
			{ ConfigCategory.CategoryId.VISUALIZE_FAST_LATE, new ConfigListInformation(ConfigCategory.CategoryId.VISUALIZE_FAST_LATE, "Visualize Fast/Late", configWork.visualize_fast_late.ToConfigValueString(), configWork.visualize_fast_late.ToConfigValueColor()) },
			{ ConfigCategory.CategoryId.AUTO_PLAY, new ConfigListInformation(ConfigCategory.CategoryId.AUTO_PLAY, "Auto Play", configWork.auto_play.ToConfigValueString(), configWork.auto_play.ToConfigValueColor()) },
			{ ConfigCategory.CategoryId.BRIGHTNESS, new ConfigListInformation(ConfigCategory.CategoryId.BRIGHTNESS, "Brightness", "x" + configWork.brightness.ToString ("F1"), defaultColor) },
			{ ConfigCategory.CategoryId.CHANGE_BREAK_TYPE, new ConfigListInformation(ConfigCategory.CategoryId.CHANGE_BREAK_TYPE, "Change Break Type", configWork.change_break_type.ToChangeBreakType().ToConfigValueString(), configWork.change_break_type.ToChangeBreakType().ToConfigValueColor()) },
			{ ConfigCategory.CategoryId.JUDGE_OFFSET, new ConfigListInformation(ConfigCategory.CategoryId.JUDGE_OFFSET, "Judge Offset", (configWork.judge_offset < 0 ? "" : "+") + ((configWork.judge_offset * 1000.0f).ToInt() / 1000.0f).ToString("F3") + " sec", defaultColor) },
			{ ConfigCategory.CategoryId.JUDGEMENT_TYPE, new ConfigListInformation(ConfigCategory.CategoryId.JUDGEMENT_TYPE, "Judgement Type", configWork.judgement_type.ToJudgementType().ToConfigValueString(), configWork.judgement_type.ToJudgementType().ToConfigValueColor()) },
			{ ConfigCategory.CategoryId.USER_RESOURCES_CACHE_TYPE, new ConfigListInformation(ConfigCategory.CategoryId.USER_RESOURCES_CACHE_TYPE, "User Resources Cache Type", configWork.user_recources_cache_type.ToResourcesCacheType().ToConfigValueString(), configWork.user_recources_cache_type.ToResourcesCacheType().ToConfigValueColor()) },
			{ ConfigCategory.CategoryId.IS_LOAD_JACKET_WHEN_SELECT_TRACK, new ConfigListInformation(ConfigCategory.CategoryId.IS_LOAD_JACKET_WHEN_SELECT_TRACK, "Load Jacket When Select Track Scene", configWork.is_load_jacket_when_select_track.ToConfigValueYesNoString(), configWork.is_load_jacket_when_select_track.ToConfigValueColor()) },
			{ ConfigCategory.CategoryId.IS_LOAD_AUDIO_WHEN_SELECT_TRACK, new ConfigListInformation(ConfigCategory.CategoryId.IS_LOAD_AUDIO_WHEN_SELECT_TRACK, "Load Audio When Select Track Scene", configWork.is_load_audio_when_select_track.ToConfigValueYesNoString(), configWork.is_load_audio_when_select_track.ToConfigValueColor()) },
			{ ConfigCategory.CategoryId.SENSOR_SIZE, new ConfigListInformation(ConfigCategory.CategoryId.SENSOR_SIZE, "Sensor Size", configWork.sensor_size.ToInt().ToString(), defaultColor) },
		};
		contents = new ConfigListInformation[configContents.Values.Count];
		configContents.Values.CopyTo (contents, 0);

		float radius = (Constants.instance.MAIMAI_OUTER_RADIUS + 5) * 2;
		guideCircle.transform.RectCast ().sizeDelta = new Vector2 (radius, radius);
		while (MaipadDynamicCreatedSpriteTank.instance.guideCircleMenu == null) {
			yield return null;
		}
		guideCircle.sprite = MaipadDynamicCreatedSpriteTank.instance.guideCircleMenu;
		CreateSensors ();

		if (MiscInformationController.instance.lastLoadedBgmKey != "maisq_bgm_main") {
			AudioManagerLite.Pause ();
			AudioManagerLite.Load (Utility.SoundEffectManager.GetClip ("maisq_bgm_main"));
			MiscInformationController.instance.lastLoadedBgmKey = "maisq_bgm_main";
			AudioManagerLite.Seek (0);
			AudioManagerLite.Play (true);
		}

		while (!listCtrl.setupCompleted) {
			yield return null;
		}
		(listCtrl.transform as RectTransform).anchoredPosition = new Vector2 (0, UserDataController.instance.user.last_configed_list_scroll);
	}

	private void CreateSensors() {
		sensorImages = new SensorUIImageController[8];
		for (int i = 0; i < 8; i++) {
			var sensorImageObj = Instantiate(sensorUIPrefab) as GameObject;
			sensorImageObj.name = "Sensor for UI " + (i + 1).ToString();
			sensorImageObj.SetParentEx (sensorUIParent);
			var sensorImage = sensorImageObj.GetComponent<SensorUIImageController>();
			sensorImage.SetPosition (Constants.instance.GetPieceDegree(i), Constants.instance.MAIMAI_OUTER_RADIUS);
			sensorImage.Setup(sensorIcons[i]);
			sensorImages[i] = sensorImage;
		}
		
		// Scroll Up.
		sensorImages [0].onSensorHoldDownRepeat = () => {
			Utility.SoundEffectManager.Play ("se_select");
			float speed;
			if (sensorTurboSpeed0 == 0) {
				speed = sensorTurboFirstSpeed;
			}
			else {
				speed = sensorTurboSpeed0;
			}
			sensorTurboSpeed0 += sensorTurboAddSpeed;
			if (speed > sensorTurboMaxSpeed) speed = sensorTurboMaxSpeed;
			var rect = listCtrl.transform.RectCast ();
			rect.anchoredPosition = new Vector2 (0, rect.anchoredPosition.y - speed);
		};
		sensorImages [0].onSensorHoldUp = () => {
			sensorTurboSpeed0 = 0;
		};
		// Level Up.
		sensorImages [1].onSensorHoldDownRepeat = ()=>OnPlusMinusButtonClick(true);
		// Level Down.
		sensorImages [2].onSensorHoldDownRepeat = ()=>OnPlusMinusButtonClick(false);
		// Scroll Down.
		sensorImages [3].onSensorHoldDownRepeat = () => {
			Utility.SoundEffectManager.Play ("se_select");
			float speed;
			if (sensorTurboSpeed3 == 0) {
				speed = sensorTurboFirstSpeed;
			}
			else {
				speed = sensorTurboSpeed3;
			}
			sensorTurboSpeed3 += sensorTurboAddSpeed;
			if (speed > sensorTurboMaxSpeed) speed = sensorTurboMaxSpeed;
			var rect = listCtrl.transform.RectCast ();
			rect.anchoredPosition = new Vector2 (0, rect.anchoredPosition.y + speed);
		};
		sensorImages [3].onSensorHoldUp = () => {
			sensorTurboSpeed3 = 0;
		};
		// OK.
		System.Action reference = () => {
			UserDataController.instance.config.Copy (configWork);
			UserDataController.instance.SaveConfig ();
			UserDataController.instance.user.last_configed_list_scroll = (listCtrl.transform as RectTransform).anchoredPosition.y;
			UserDataController.instance.SaveUserInfo ();
		};
		sensorImages [4].onSensorClick = () => {
			Utility.SoundEffectManager.Play ("se_decide");
			reference ();
			fade.Activate (()=>{
				string sceneName;
				if (MiscInformationController.instance != null && !string.IsNullOrEmpty(MiscInformationController.instance.optionSceneCallbackSceneName)) {
					sceneName = MiscInformationController.instance.optionSceneCallbackSceneName;
				}
				else {
					sceneName = "SceneSelectTrack";
				}
				Application.LoadLevel (sceneName);
			});
		};
		// Cancel.
		sensorImages [5].onSensorClick = () => {
			Utility.SoundEffectManager.Play ("se_decide");
			UserDataController.instance.user.last_configed_list_scroll = (listCtrl.transform as RectTransform).anchoredPosition.y;
			fade.Activate (()=>{
				string sceneName;
				if (MiscInformationController.instance != null && !string.IsNullOrEmpty(MiscInformationController.instance.optionSceneCallbackSceneName)) {
					sceneName = MiscInformationController.instance.optionSceneCallbackSceneName;
				}
				else {
					sceneName = "SceneSelectTrack";
				}
				Application.LoadLevel (sceneName);
			});
		};
		// Config.
		sensorImages [6].onSensorClick = () => {
			Utility.SoundEffectManager.Play ("se_decide");
			reference ();
			fade.Activate (()=>{
				Application.LoadLevel ("SceneProfile");
			});
		};
		// Empty.
		//sensorImages [7].onSensorClick = OnGoOptionSceneButtonClick;
	}

	public void OnTappedCell (ConfigListInformation info) {
		Utility.SoundEffectManager.Play ("se_decide");
		if (info.id == ConfigCategory.CategoryId.SENSOR_SIZE) {
			if (!sensorSampleCtrl.gameObject.activeSelf) {
				sensorSampleCtrl.gameObject.SetActive (true);
			}
		}
		else {
			if (sensorSampleCtrl.gameObject.activeSelf) {
				sensorSampleCtrl.gameObject.SetActive (false);
			}
		}
	}

	public void OnDoubleTappedCell (ConfigListInformation info) {
		Utility.SoundEffectManager.Play ("se_decide");
		var content = configContents [selectedId];
		System.Action contentUpdate = () => {
			string cellValue;
			Color cellValueColor;
			GatherContentData (info.id, out cellValue, out cellValueColor);
			content.SetValue (cellValue, cellValueColor);
			forceUpdateContents = true;
		};

		switch (info.id) {
		case ConfigCategory.CategoryId.SOUND_EFFECT_TYPE:
			detailUICtrl.Show (info.key, 
						configWork.sound_effect_type, 
						(result)=>{ configWork.sound_effect_type = result; contentUpdate(); }, 
						System.Enum.GetNames(typeof(ConfigTypes.SoundEffect)).Length,
						(value)=>value.ToSoundEffectType().ToConfigValueString());
			break;
		case ConfigCategory.CategoryId.ANSWER_SOUND_TYPE:
			detailUICtrl.Show (info.key, 
						configWork.answer_sound_type, 
						(result)=>{ configWork.answer_sound_type = result; contentUpdate(); }, 
						System.Enum.GetNames(typeof(ConfigTypes.AnswerSound)).Length,
						(value)=>value.ToAnswerSoundType().ToConfigValueString());
			break;
		case ConfigCategory.CategoryId.TURN:
			detailUICtrl.Show (info.key, 
						configWork.turn, 
						(result)=>{ configWork.turn = result; contentUpdate(); },  
						8,
						(value)=>value.ToConfigValuePlusMinusString());
			break;
		case ConfigCategory.CategoryId.MIRROR:
			detailUICtrl.Show (info.key,
						configWork.mirror,
						(result)=>{ configWork.mirror = result; contentUpdate(); });
			break;
		case ConfigCategory.CategoryId.GUIDE_SPEED_FADE:
			detailUICtrl.Show (info.key,
						configWork.fade_guide_speed,
						(result)=>{ configWork.fade_guide_speed = result; contentUpdate(); },
						0f, 2f, 
						(value)=>value.ToString("F2") + " sec",
						false);
			break;
		case ConfigCategory.CategoryId.GUIDE_SPEED_MOVE:
			detailUICtrl.Show (info.key,
						configWork.move_guide_speed,
						(result)=>{ configWork.move_guide_speed = result; contentUpdate(); },
						0f, 2f, 
						(value)=>value.ToString("F2") + " sec",
						false);
			break;
		case ConfigCategory.CategoryId.STAR_ROTATION:
			detailUICtrl.Show (info.key,
						 configWork.star_rotation,
						(result)=>{ configWork.star_rotation = result; contentUpdate(); });
			break;
		case ConfigCategory.CategoryId.RING_SIZE:
			detailUICtrl.Show (info.key,
						configWork.ring_size,
						(result)=>{ configWork.ring_size = result; contentUpdate(); },
						0.1f, 2f, 
						(value)=>"x" + value.ToString("F2"),
						false);
			break;
		case ConfigCategory.CategoryId.MARKER_SIZE:
			detailUICtrl.Show (info.key,
						configWork.marker_size,
						(result)=>{ configWork.marker_size = result; contentUpdate(); },
						0.1f, 2f, 
						(value)=>"x" + value.ToString("F2"),
						false);
			break;
		case ConfigCategory.CategoryId.ANGULATED_HOLD:
			detailUICtrl.Show (info.key,
						configWork.angulated_hold,
						(result)=>{ configWork.angulated_hold = result; contentUpdate(); });
			break;
		case ConfigCategory.CategoryId.PINE:
			detailUICtrl.Show (info.key,
						configWork.pine,
						(result)=>{ configWork.pine = result; contentUpdate(); });
			break;
		case ConfigCategory.CategoryId.BACKGROUND_INFOMATION_TYPE:
			detailUICtrl.Show (info.key, 
						configWork.background_infomation_type, 
						(result)=>{ configWork.background_infomation_type = result; contentUpdate(); }, 
						System.Enum.GetNames(typeof(ConfigTypes.BackGroundInfomation)).Length,
						(value)=>value.ToBackGroundInfomationType().ToString());
			break;
		case ConfigCategory.CategoryId.ACHIEVEMENT_VISION_TYPE:
			detailUICtrl.Show (info.key, 
						configWork.up_screen_achievement_vision_type, 
						(result)=>{ configWork.up_screen_achievement_vision_type = result; contentUpdate(); }, 
						System.Enum.GetNames(typeof(ConfigTypes.UpScreenAchievementVision)).Length,
						(value)=>value.ToUpScreenAchievementVisionType().ToConfigValueString());
			break;
		case ConfigCategory.CategoryId.RANK_VERSION_TYPE:
			detailUICtrl.Show (info.key, 
						configWork.rank_version_type, 
						(result)=>{ configWork.rank_version_type = result; contentUpdate(); }, 
						System.Enum.GetNames(typeof(ConfigTypes.RankVersion)).Length,
						(value)=>value.ToRankVersionType().ToConfigValueString());
			break;
		case ConfigCategory.CategoryId.VISUALIZE_FAST_LATE:
			detailUICtrl.Show (info.key,
						configWork.visualize_fast_late,
						(result)=>{ configWork.visualize_fast_late = result; contentUpdate(); });
			break;
		case ConfigCategory.CategoryId.AUTO_PLAY:
			detailUICtrl.Show (info.key,
						configWork.auto_play,
						(result)=>{ configWork.auto_play = result; contentUpdate(); });
			break;
		case ConfigCategory.CategoryId.BRIGHTNESS:
			detailUICtrl.Show (info.key,
						configWork.brightness,
						(result)=>{ configWork.brightness = result; contentUpdate(); },
						0f, 1f, 
						(value)=>"x" + value.ToString("F1"),
						false);
			break;
		case ConfigCategory.CategoryId.CHANGE_BREAK_TYPE:
			detailUICtrl.Show (info.key, 
						configWork.change_break_type, 
						(result)=>{ configWork.change_break_type = result; contentUpdate(); }, 
						System.Enum.GetNames(typeof(ConfigTypes.ChangeBreak)).Length,
						(value)=>value.ToChangeBreakType().ToConfigValueString());
			break;
		case ConfigCategory.CategoryId.JUDGE_OFFSET:
			detailUICtrl.Show (info.key,
						configWork.judge_offset,
						(result)=>{ configWork.judge_offset = result; contentUpdate(); },
						-5f, 5f,
						(value)=>(value < 0 ? "" : "+") + value.ToString("F3"),
						false);
			break;
		case ConfigCategory.CategoryId.JUDGEMENT_TYPE:
			detailUICtrl.Show (info.key, 
						configWork.judgement_type, 
						(result)=>{ configWork.judgement_type = result; contentUpdate(); }, 
						System.Enum.GetNames(typeof(ConfigTypes.Judgement)).Length,
						(value)=>value.ToJudgementType().ToConfigValueString());
			break;
		case ConfigCategory.CategoryId.USER_RESOURCES_CACHE_TYPE:
			detailUICtrl.Show (info.key, 
			                   configWork.user_recources_cache_type, 
			                   (result)=>{ configWork.user_recources_cache_type = result; contentUpdate(); }, 
			System.Enum.GetNames(typeof(ConfigTypes.ResourcesCacheType)).Length,
			(value)=>value.ToResourcesCacheType().ToConfigValueString());
			break;
		case ConfigCategory.CategoryId.IS_LOAD_JACKET_WHEN_SELECT_TRACK:
			detailUICtrl.Show (info.key,
			                   configWork.is_load_jacket_when_select_track,
			                   (result)=>{ configWork.is_load_jacket_when_select_track = result; contentUpdate(); }, true);
			break;
		case ConfigCategory.CategoryId.IS_LOAD_AUDIO_WHEN_SELECT_TRACK:
			detailUICtrl.Show (info.key,
			                   configWork.is_load_audio_when_select_track,
			                   (result)=>{ configWork.is_load_audio_when_select_track = result; contentUpdate(); }, true);
			break;
		case ConfigCategory.CategoryId.SENSOR_SIZE:
			detailUICtrl.Show (info.key,
						configWork.sensor_size,
			            (result)=>{ configWork.sensor_size = result; contentUpdate(); sensorSampleCtrl.SetSize (result); },
						30f, 100f, 
						(value)=>value.ToInt().ToString("F0"),
						true);
			break;
		}
	}

	public void OnPlusMinusButtonClick (bool isPlus) {
		if (selectedId == ConfigCategory.CategoryId.NONE)
			return;
		
		Utility.SoundEffectManager.Play ("se_select");
		var content = configContents [selectedId];
		string cellValue = null;
		Color cellValueColor = Color.white;
		switch (selectedId) {
		case ConfigCategory.CategoryId.SOUND_EFFECT_TYPE: {
			ChangeTypeAssignHelper (isPlus, configWork.sound_effect_type,
									System.Enum.GetNames(typeof(ConfigTypes.SoundEffect)).Length, (result)=>{
				configWork.sound_effect_type = result;
			});
			break;
		}
		case ConfigCategory.CategoryId.ANSWER_SOUND_TYPE: {
			ChangeTypeAssignHelper (isPlus, configWork.answer_sound_type, 
									System.Enum.GetNames(typeof(ConfigTypes.AnswerSound)).Length, (result)=>{
				configWork.answer_sound_type = result;
			});
			break;
		}
		case ConfigCategory.CategoryId.TURN: {
			ChangeTypeAssignHelper (isPlus, configWork.turn, 
									8, (result)=>{
				configWork.turn = result;
			});
			break;
		}
		case ConfigCategory.CategoryId.MIRROR: {
			ChangeSwitchAssignHelper (configWork.mirror, (result)=>{
				configWork.mirror = result;
			});
			break;
		}
		case ConfigCategory.CategoryId.GUIDE_SPEED_FADE: {
			ChangeValueAssignHelper (isPlus, configWork.fade_guide_speed, 10, 2000, 0, (result)=>{
				configWork.fade_guide_speed = result;
			});
			break;
		}
		case ConfigCategory.CategoryId.GUIDE_SPEED_MOVE: {
			ChangeValueAssignHelper (isPlus, configWork.move_guide_speed, 10, 2000, 0, (result)=>{
				configWork.move_guide_speed = result;
			});
			break;
		}
		case ConfigCategory.CategoryId.STAR_ROTATION: {
			ChangeSwitchAssignHelper (configWork.star_rotation, (result)=>{
				configWork.star_rotation = result;
			});
			break;
		}
		case ConfigCategory.CategoryId.RING_SIZE: {
			ChangeValueAssignHelper (isPlus, configWork.ring_size, 50, 2000, 100, (result)=>{
				configWork.ring_size = result;
			});
			break;
		}
		case ConfigCategory.CategoryId.MARKER_SIZE: {
			ChangeValueAssignHelper (isPlus, configWork.marker_size, 50, 2000, 100, (result)=>{
				configWork.marker_size = result;
			});
			break;
		}
		case ConfigCategory.CategoryId.ANGULATED_HOLD: {
			ChangeSwitchAssignHelper (configWork.angulated_hold, (result)=>{
				configWork.angulated_hold = result;
			});
			break;
		}
		case ConfigCategory.CategoryId.PINE: {
			ChangeSwitchAssignHelper (configWork.pine, (result)=>{
				configWork.pine = result;
			});
			break;
		}
		case ConfigCategory.CategoryId.BACKGROUND_INFOMATION_TYPE: {
			ChangeTypeAssignHelper (isPlus, configWork.background_infomation_type, 
									System.Enum.GetNames(typeof(ConfigTypes.BackGroundInfomation)).Length, (result)=>{
				configWork.background_infomation_type = result;
			});
			break;
		}
		case ConfigCategory.CategoryId.ACHIEVEMENT_VISION_TYPE: {
			ChangeTypeAssignHelper (isPlus, configWork.up_screen_achievement_vision_type, 
									System.Enum.GetNames(typeof(ConfigTypes.UpScreenAchievementVision)).Length, (result)=>{
				configWork.up_screen_achievement_vision_type = result;
			});
			break;
		}
		case ConfigCategory.CategoryId.RANK_VERSION_TYPE: {
			ChangeTypeAssignHelper (isPlus, configWork.rank_version_type, 
									System.Enum.GetNames(typeof(ConfigTypes.RankVersion)).Length, (result)=>{
				configWork.rank_version_type = result;
			});
			break;
		}
		case ConfigCategory.CategoryId.VISUALIZE_FAST_LATE: {
			ChangeSwitchAssignHelper (configWork.visualize_fast_late, (result)=>{
				configWork.visualize_fast_late = result;
			});
			break;
		}
		case ConfigCategory.CategoryId.AUTO_PLAY: {
			ChangeSwitchAssignHelper (configWork.auto_play, (result)=>{
				configWork.auto_play = result;
			});
			break;
		}
		case ConfigCategory.CategoryId.BRIGHTNESS: {
			ChangeValueAssignHelper (isPlus, configWork.brightness, 100, 1000, 0, (result)=>{
				configWork.brightness = result;
			});
			break;
		}
		case ConfigCategory.CategoryId.CHANGE_BREAK_TYPE: {
			ChangeTypeAssignHelper (isPlus, configWork.change_break_type, 
									System.Enum.GetNames(typeof(ConfigTypes.ChangeBreak)).Length, (result)=>{
				configWork.change_break_type = result;
			});
			break;
		}
		case ConfigCategory.CategoryId.JUDGE_OFFSET: {
			ChangeValueAssignHelper (isPlus, configWork.judge_offset, 1, 5000, -5000, (result)=>{
				configWork.judge_offset = result;
			});
			break;
		}
		case ConfigCategory.CategoryId.JUDGEMENT_TYPE: {
			ChangeTypeAssignHelper (isPlus, configWork.judgement_type, 
									System.Enum.GetNames(typeof(ConfigTypes.Judgement)).Length, (result)=>{
				configWork.judgement_type = result;
			});
			break;
		}
		case ConfigCategory.CategoryId.USER_RESOURCES_CACHE_TYPE: {
			ChangeTypeAssignHelper (isPlus, configWork.user_recources_cache_type, 
			                        System.Enum.GetNames(typeof(ConfigTypes.ResourcesCacheType)).Length, (result)=>{
				configWork.user_recources_cache_type = result;
			});
			break;
		}
		case ConfigCategory.CategoryId.IS_LOAD_JACKET_WHEN_SELECT_TRACK: {
			ChangeSwitchAssignHelper (configWork.is_load_jacket_when_select_track, (result)=>{
				configWork.is_load_jacket_when_select_track = result;
			});
			break;
		}
		case ConfigCategory.CategoryId.IS_LOAD_AUDIO_WHEN_SELECT_TRACK: {
			ChangeSwitchAssignHelper (configWork.is_load_audio_when_select_track, (result)=>{
				configWork.is_load_audio_when_select_track = result;
			});
			break;
		}
		case ConfigCategory.CategoryId.SENSOR_SIZE: {
			ChangeValueAssignHelper (isPlus, configWork.sensor_size.ToInt(), 1, 100, 30, (result)=>{
				configWork.sensor_size = result;
				sensorSampleCtrl.SetSize (result);
			});
			break;
		}

		}
		
		GatherContentData (selectedId, out cellValue, out cellValueColor);
		if (!string.IsNullOrEmpty (cellValue)) {
			content.SetValue (cellValue, cellValueColor);
			forceUpdateContents = true;
		}
	}

	private void ChangeTypeAssignHelper (bool isPlus, int typeId, int elements, System.Action<int> assign) {
		if (isPlus) {
			typeId++;
		}
		else {
			typeId += elements - 1;
		}
		int result = typeId % elements;
		assign (result);
	}

	private void ChangeSwitchAssignHelper (bool srcValue, System.Action<bool> assign) {
		assign (!srcValue);
	}
	
	private void ChangeValueAssignHelper (bool isPlus, float srcValue, int volume, int max, int min, System.Action<float> assign) {
		int work = (srcValue * 1000.0f).ToInt();
		if (isPlus) {
			work += volume;
			work = work / volume * volume;
			if (work > max) {
				work = max;
			}
		}
		else {
			work -= volume;
			work = work / volume * volume;
			if (work < min) {
				work = min;
			}
		}
		float result = (float)work / 1000.0f;
		assign (result);
	}
	
	private void ChangeValueAssignHelper (bool isPlus, int srcValue, int volume, int max, int min, System.Action<float> assign) {
		int work = srcValue;
		if (isPlus) {
			work += volume;
			work = work / volume * volume;
			if (work > max) {
				work = max;
			}
		}
		else {
			work -= volume;
			work = work / volume * volume;
			if (work < min) {
				work = min;
			}
		}
		float result = (float)work;
		assign (result);
	}

	private void GatherContentData (ConfigCategory.CategoryId id, out string cellValue, out Color cellValueColor) {
		cellValue = string.Empty;
		cellValueColor = Color.white;
		switch (id) {
		case ConfigCategory.CategoryId.SOUND_EFFECT_TYPE:
			{
				var type = configWork.sound_effect_type.ToSoundEffectType ();
				cellValue = type.ToConfigValueString ();
				cellValueColor = type.ToConfigValueColor ();
				break;
			}
		case ConfigCategory.CategoryId.ANSWER_SOUND_TYPE:
			{
				var type = configWork.answer_sound_type.ToAnswerSoundType ();
				cellValue = type.ToConfigValueString ();
				cellValueColor = type.ToConfigValueColor ();
				break;
			}
		case ConfigCategory.CategoryId.TURN:
			{
				var type = configWork.turn;
				cellValue = type.ToConfigValuePlusMinusString ();
				cellValueColor = type.ToConfigValueColor ();
				break;
			}
		case ConfigCategory.CategoryId.MIRROR:
			{
				var type = configWork.mirror;
				cellValue = type.ToConfigValueString ();
				cellValueColor = type.ToConfigValueColor ();
				break;
			}
		case ConfigCategory.CategoryId.GUIDE_SPEED_FADE:
			{
				var type = configWork.fade_guide_speed;
				cellValue = type.ToString ("F2") + " sec";
				cellValueColor = defaultColor;
				break;
			}
		case ConfigCategory.CategoryId.GUIDE_SPEED_MOVE:
			{
				var type = configWork.move_guide_speed;
				cellValue = type.ToString ("F2") + " sec";
				cellValueColor = defaultColor;
				break;
			}
		case ConfigCategory.CategoryId.STAR_ROTATION:
			{
				var type = configWork.star_rotation;
				cellValue = type.ToConfigValueString ();
				cellValueColor = type.ToConfigValueColor ();
				break;
			}
		case ConfigCategory.CategoryId.RING_SIZE:
			{
				var type = configWork.ring_size;
				cellValue = "x" + type.ToString ("F2");
				cellValueColor = defaultColor;
				break;
			}
		case ConfigCategory.CategoryId.MARKER_SIZE:
			{
				var type = configWork.marker_size;
				cellValue = "x" + type.ToString ("F2");
				cellValueColor = defaultColor;
				break;
			}
		case ConfigCategory.CategoryId.ANGULATED_HOLD:
			{
				var type = configWork.angulated_hold;
				cellValue = type.ToConfigValueString ();
				cellValueColor = type.ToConfigValueColor ();
				break;
			}
		case ConfigCategory.CategoryId.PINE:
			{
				var type = configWork.pine;
				cellValue = type.ToConfigValueString ();
				cellValueColor = type.ToConfigValueColor ();
				break;
			}
		case ConfigCategory.CategoryId.BACKGROUND_INFOMATION_TYPE:
			{
				var type = configWork.background_infomation_type.ToBackGroundInfomationType ();
				cellValue = type.ToString ();
				cellValueColor = type.ToConfigValueColor ();
				break;
			}
		case ConfigCategory.CategoryId.ACHIEVEMENT_VISION_TYPE:
			{
				var type = configWork.up_screen_achievement_vision_type.ToUpScreenAchievementVisionType ();
				cellValue = type.ToConfigValueString ();
				cellValueColor = type.ToConfigValueColor ();
				break;
			}
		case ConfigCategory.CategoryId.RANK_VERSION_TYPE:
			{
				var type = configWork.rank_version_type.ToRankVersionType ();
				cellValue = type.ToConfigValueString ();
				cellValueColor = type.ToConfigValueColor ();
				break;
			}
		case ConfigCategory.CategoryId.VISUALIZE_FAST_LATE:
			{
				var type = configWork.visualize_fast_late;
				cellValue = type.ToConfigValueString ();
				cellValueColor = type.ToConfigValueColor ();
				break;
			}
		case ConfigCategory.CategoryId.AUTO_PLAY:
			{
				var type = configWork.auto_play;
				cellValue = type.ToConfigValueString ();
				cellValueColor = type.ToConfigValueColor ();
				break;
			}
		case ConfigCategory.CategoryId.BRIGHTNESS:
			{
				var type = configWork.brightness;
				cellValue = "x" + type.ToString ("F1");
				cellValueColor = defaultColor;
				break;
			}
		case ConfigCategory.CategoryId.CHANGE_BREAK_TYPE:
			{
				var type = configWork.change_break_type.ToChangeBreakType ();
				cellValue = type.ToConfigValueString ();
				cellValueColor = type.ToConfigValueColor ();
				break;
			}
		case ConfigCategory.CategoryId.JUDGE_OFFSET:
			{
				var type = configWork.judge_offset;
				cellValue = (type < 0 ? "" : "+") + ((type * 1000.0f).ToInt () / 1000.0f).ToString ("F3") + " sec";
				cellValueColor = defaultColor;
				break;
			}
		case ConfigCategory.CategoryId.JUDGEMENT_TYPE:
			{
				var type = configWork.judgement_type.ToJudgementType ();
				cellValue = type.ToConfigValueString ();
				cellValueColor = type.ToConfigValueColor ();
				break;
			}
		case ConfigCategory.CategoryId.USER_RESOURCES_CACHE_TYPE:
			{
				var type = configWork.user_recources_cache_type.ToResourcesCacheType ();
				cellValue = type.ToConfigValueString ();
				cellValueColor = type.ToConfigValueColor ();
				break;
			}
		case ConfigCategory.CategoryId.IS_LOAD_JACKET_WHEN_SELECT_TRACK:
			{
				var type = configWork.is_load_jacket_when_select_track;
				cellValue = type.ToConfigValueYesNoString ();
				cellValueColor = type.ToConfigValueColor ();
				break;
			}
		case ConfigCategory.CategoryId.IS_LOAD_AUDIO_WHEN_SELECT_TRACK:
			{
				var type = configWork.is_load_audio_when_select_track;
				cellValue = type.ToConfigValueYesNoString ();
				cellValueColor = type.ToConfigValueColor ();
				break;
			}
		case ConfigCategory.CategoryId.SENSOR_SIZE:
			{
				var type = configWork.sensor_size;
				cellValue = type.ToInt ().ToString ();
				cellValueColor = defaultColor;
				break;
			}
		}
	}
}
