﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SerializeDictionary;

[System.Serializable]
public class StringSpriteDictionary : TableBase<string, Sprite, StringSpritePair> { }
[System.Serializable]
public class StringSpritePair : KeyAndValue<string, Sprite> { 
	public StringSpritePair (string key, Sprite value) : base (key, value) { }
}

public class SpriteTank : SingletonMonoBehaviour<SpriteTank> {
	public StringSpriteDictionary images;
	public static Sprite Get(string key) {
		if (instance != null && key != null) {
			if (instance.images.GetTable().ContainsKey(key)) {
				return instance.images.GetTable()[key];
			}
			else {
				Debug.LogError(key + " is not found in Sprite tank.");
			}
		}
		return null;
	}
}
