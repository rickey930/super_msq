using UnityEngine;
using System.Collections;

public class EternalAnimation : MonoBehaviour {
	///<summary>1秒間にどれだけ移動するか</summary>
	[SerializeField]
	private Vector3 moveSpeed;
	///<summary>1秒間にどれだけ回転するか</summary>
	[SerializeField]
	private Vector3 rotateSpeed;
	///<summary>1秒間にどれだけ拡大するか</summary>
	[SerializeField]
	private Vector3 scaleSpeed;
	
	private void Update() {
		transform.Translate(moveSpeed * Time.deltaTime);
		transform.Rotate(Constants.instance.ToLeftHandedCoordinateSystemRotation(rotateSpeed) * Time.deltaTime);
		transform.localScale += (scaleSpeed * Time.deltaTime);
	}
	
}