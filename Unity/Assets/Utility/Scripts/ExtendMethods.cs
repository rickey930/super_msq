﻿using UnityEngine;
using System.Collections;

public static class ExtendMethods {
	public static int ToInt (this float value) {
		return (int)(decimal)value;
	}
	public static long ToLong (this float value) {
		return (long)(decimal)value;
	}
	public static int ToInt (this double value) {
		return (int)(decimal)value;
	}
	public static long ToLong (this double value) {
		return (long)(decimal)value;
	}
	public static float ToPercent2 (this float value) {
		int value1 = Mathf.Floor(value * 10000.0f).ToInt();
		return value1 / 100.0f;
	}
	public static string ToPercent2String (this float value) {
		float value1 = value.ToPercent2 ();
		return value1.ToString ("F2");
	}
	public static string ToPercent2SignedString (this float value) {
		return value.ToPercent2String () + "%";
	}
	public static Sprite DownloadSprite (this WWW www) {
		try {
			if (string.IsNullOrEmpty(www.error)) {
				Texture2D tex = www.texture;
				return Sprite.Create(tex, new Rect (0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f), 1.0f);
			}
			Debug.LogError(www.error);
		}
		catch (System.Exception e) {
			Debug.LogError(e);
		}
		return null;
	}
	public static AudioClip DownloadAudio (this WWW www) {
		try {
			if (string.IsNullOrEmpty(www.error)) {
				return www.GetAudioClip(false, false);
			}
			Debug.LogError(www.error);
		}
		catch (System.Exception e) {
			Debug.LogError(e);
		}
		return null;
	}
	public static void Initialize (this Transform t) {
		t.localPosition = Vector3.zero;
		t.localRotation = Quaternion.Euler (Vector3.zero);
		t.localScale = Vector3.one;
	}
	public static void SetParentEx (this Transform t, Transform parent) {
		t.SetParent (parent);
		t.Initialize ();
	}
	public static void SetParentEx (this Transform t, GameObject parent) {
		t.SetParent (parent.transform);
		t.Initialize ();
	}
	public static void SetParentEx (this GameObject obj, GameObject parent) {
		obj.transform.SetParent (parent.transform);
		obj.transform.Initialize ();
	}
	public static void SetParentEx (this GameObject obj, Transform parent) {
		obj.transform.SetParent (parent);
		obj.transform.Initialize ();
	}
	public static RectTransform RectCast (this Transform t) {
		return t as RectTransform;
	}
	public static Vector3 ChangePosHandSystem (this Vector3 pos) {
		pos.y *= -1;
		return pos;
	}
	public static Vector3 ChangeRotHandSystem (this Vector3 rotate) {
		rotate.z *= -1;
		return rotate;
	}
	public static Vector2 ChangePosHandSystem (this Vector2 pos) {
		pos.y *= -1;
		return pos;
	}
	public static Vector2 ChangeUVHandSystem (this Vector2 uv) {
		uv.y = 1.0f - uv.y;
		return uv;
	}
	public static float ChangeDegHandSystem (this float deg) {
		deg *= -1;
		return deg;
	}
	public static Material CreateMaterial (this Sprite sprite, Shader shader) {
		var mat = new Material(shader);
		mat.name = sprite.name + " Created Material";
		mat.mainTexture = sprite.texture;
		return mat;
	}
}