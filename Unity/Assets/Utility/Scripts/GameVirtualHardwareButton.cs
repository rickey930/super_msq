using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameVirtualHardwareButton : MonoBehaviour {
	/*
	ボタンが指IDを覚える.
	さっきまでない指が触れていたらpush.
	さっきまで触れていた指がなければpull.
	指が1つでもあればhold.
	指が1つもなければfree.

	アップデートに頼るのは危ない.
	外部からCheck関数を呼ぶ.
	
	【ボタンをポーズ状態に関わらず常にチェックする場合 (CheckingAlways)】.
		・ボタンを離してからポーズ解除すると適切にpull判定が送られなくなる.
	<問題点>.
		・ホールドノートを離してもホールド状態になる.
	<解決案>.
		・ポーズ解除した瞬間で何も押されてなければpull判定を送る.
		・inPullかoutPullの判定は、前回pullしたときに記憶したpull判定を送る。デフォはinPull.
	
	【ボタンをポーズ状態でチェックしない場合 (CheckingExceptPause)】.
		・ポーズ前まで押されてなかったボタンを押しながらポーズを解除するとpush判定が送られる.
	<問題点>.
		・タイミングを合わせてポーズすると、ポーズ解除時にズルができる.
	<解決案>.
		・ポーズ解除した瞬間ならpush判定を送らない.
	*/

	[SerializeField]
	protected Collider2D buttonCollider; // タッチに対するボタンのコライダー.
	[SerializeField]
	protected string[] copeGamePadButtonNames; // 対応するゲームパッドボタン名.
	
	private HashSet<string> _fingerIDs;
	private HashSet<string> fingerIDs { get { if (_fingerIDs == null) { _fingerIDs = new HashSet<string> (); } return _fingerIDs; } }
	public bool push { get; protected set; }
	public bool inPull { get; protected set; }
	public bool outPull { get; protected set; }
	public bool pull { get { return inPull || outPull; } }
	public bool hold { get; protected set; }
	public bool free { get { return !hold; } }
	public float holdTime { get; protected set; }
	public float freeTime { get; protected set; }
	protected bool lastInPull { get; set; }
	protected bool lastOutPull { get; set; }
	
	public void Check () { Check (false, false); }
	public void Check (bool checkingExceptPause) { Check (false, checkingExceptPause); }
	
	// pauseFitr: ポーズ明け.
	// checkingExceptPause: ポーズ状態でボタンをチェックしない使い方をしている.
	public void Check (bool pauseFitr, bool checkingExceptPause) {
		// 様々な入力情報を統括.
		List<InputInfo> inputInfos = new List<InputInfo>();
		if (Input.touchSupported) {
			foreach (Touch touch in Input.touches) {
				inputInfos.Add(InputInfo.FromTouch(touch, buttonCollider));
			}
		}
		else {
			for (int i = 0; i < 2; i++) {
				inputInfos.Add(InputInfo.FromMouse(i, buttonCollider));
			}
			foreach (var name in copeGamePadButtonNames) {
				inputInfos.Add(InputInfo.FromGamePad(name));
			}
		}

		// 入力情報を元にボタンのステートを判定.
		bool _push = false;
		bool _inPull = false;
		bool _outPull = false;
		HashSet<string> deltaTouch = new HashSet<string> ();
		foreach (var inputInfo in inputInfos) {
			bool isTouch = inputInfo.isTouch;
			bool isInCollider = inputInfo.IsTouchInCollider();
			if (isTouch && isInCollider) {
				if (fingerIDs.Add (inputInfo.id)) {
					if (!pauseFitr || !checkingExceptPause) { // checkingExceptPauseのときポーズ解除した瞬間ならpush判定を送らない.
						_push = true;
					}
				}
				deltaTouch.Add (inputInfo.id);
			}
			else if (isTouch && !isInCollider && fingerIDs.Contains(inputInfo.id)) {
				_outPull = true;
			}
		}
		var deleteFingerIds = new HashSet<string>();
		foreach (string id in fingerIDs) {
			if (!deltaTouch.Contains (id)) {
				deleteFingerIds.Add (id);
				_inPull = true;
			}
		}
		foreach (string id in deleteFingerIds) {
			fingerIDs.Remove(id);
		}
		
		push = _push;
		hold = fingerIDs.Count > 0;
		
		if (_inPull || _outPull) {
			lastInPull = _inPull;
			lastOutPull = _outPull;
		}
		if (pauseFitr && !checkingExceptPause && !hold) { // checkingAlwaysのときポーズ解除した瞬間で何も押されてなければpull判定を送る.
			_inPull = lastInPull;
			_outPull = lastOutPull;
		}
		
		inPull = _inPull;
		outPull = _outPull;
		
		if (hold) {
			holdTime += Time.deltaTime;
			freeTime = 0.0f;
		}
		else {
			freeTime += Time.deltaTime;
			holdTime = 0.0f;
		}
	}

	private class InputInfo
	{
		// タッチされたのか.
		public bool isTouch { get; private set; }
		// ポジション キーならいらない.
		public Vector3 touchPos { get; private set; }
		// ユニークID.
		public string id { get; private set; }
		// コライダーチェッカー キーならいらない.
		public System.Func<bool> IsTouchInCollider { get; private set; }

		public InputInfo() {

		}

		public static InputInfo FromTouch(Touch touch, Collider2D buttonCollider) {
			var ret = new InputInfo();
			ret.isTouch = touch.phase != TouchPhase.Ended && touch.phase != TouchPhase.Canceled;
			ret.touchPos = touch.position;
			ret.id = "TOUCH_" + touch.fingerId.ToString();
			ret.IsTouchInCollider = ()=>{ return ret.TouchInCollider(buttonCollider); };
			return ret;
		}

		public static InputInfo FromMouse(int mouseButtonId, Collider2D buttonCollider) {
			var ret = new InputInfo();
			ret.isTouch = Input.GetMouseButton(mouseButtonId);
			ret.touchPos = Input.mousePosition;
			ret.id = "MOUSE_" + mouseButtonId.ToString();
			ret.IsTouchInCollider = ()=>{ return ret.TouchInCollider(buttonCollider); };
			return ret;
		}
		
		public static InputInfo FromGamePad(string buttonName) {
			var ret = new InputInfo();
			ret.isTouch = Input.GetButton(buttonName);
			ret.touchPos = new Vector3();
			ret.id = "GAMEPAD_" + buttonName;
			ret.IsTouchInCollider = ()=>{ return true; };
			return ret;
		}
		
		private bool TouchInCollider(Collider2D buttonCollider) {
			return TouchInCollider (touchPos, buttonCollider);
		}
		
		public static bool TouchInCollider(Vector3 touchPos, Collider2D buttonCollider) {
			Vector2 tapPoint = Camera.main.ScreenToWorldPoint (touchPos);
			//Collider2D[] colliders = Physics2D.OverlapPointAll (tapPoint);
			//if (colliders != null) {
			//	foreach (var collider in colliders) {
			//		if (collider == buttonCollider) {
			//			return true;
			//		}
			//	}
			//}
			{
				var c = buttonCollider as CircleCollider2D;
				if (c != null) {
					return CircleCalculator.CircleIntersect(tapPoint, 1, c.center, c.radius);
				}
			}
			{
				var c = buttonCollider as BoxCollider2D;
				if (c != null) {
					return c.bounds.Intersects(new Bounds(tapPoint, Vector2.one));
				}
			}
			return false;
		}
	}
}

