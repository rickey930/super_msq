﻿using UnityEngine;
using System.Collections.Generic;

public static class CircleCalculator {
	/// <summary>
	/// ラジアン角に変換.
	/// </summary>
	public static float ToRadian(float degree) {
		if (degree == 360.0f) {
			return ((0.0f-90.0f+360.0f)%360.0f+360.0f) * Mathf.Deg2Rad; //360を指定したら上0度から1周ってことにしたいので特別処理
		}
		degree -= 90.0f;
		while (degree > 360.0f) degree -= 360.0f;
		while (degree < 0.0f) degree += 360.0f;
		return degree * Mathf.Deg2Rad;
	}
	/// <summary>
	/// ディグリー角に変換 (上が0度).
	/// </summary>
	public static float ToDegree(float radian) {
		var ret = (radian * Mathf.Rad2Deg) - 90.0f;
		while (ret > 360.0f) ret -= 360.0f;
		while (ret < 0.0f) ret += 360.0f;
		return ret;
	}
	/// <summary>
	/// 原点から見て目標点の角度はいくつか.
	/// </summary>
	public static float PointToDegree(float x, float y) {
		var rad = Mathf.Atan2(y, x);
		var deg = ToDegree(rad);
		return deg;
	}
	/// <summary>
	/// 原点から見て目標点の角度はいくつか.
	/// </summary>
	public static float PointToDegree(Vector2 pos) {
		return PointToDegree(pos.x, pos.y);
	}
	/// <summary>
	/// スタート地点から見て目標点の角度はいくつか.
	/// </summary>
	public static float PointToDegree(Vector2 start, Vector2 target) {
		return PointToDegree(start.x - target.x, start.y - target.y);
	}
	/// <summary>
	/// 中心から指定した角度で指定した半径分移動したとき、どこに行くか.
	/// </summary>
	/// <param name="centerX">中心X座標.</param>
	/// <param name="centerY">中心Y座標.</param>
	/// <param name="radius">半径.</param>
	/// <param name="degree">ディグリー角.</param>
	public static Vector2 PointOnCircle(float centerX, float centerY, float radius, float degree) {
		var x = radius * Mathf.Cos(ToRadian(degree)) + centerX;  // X座標.
		var y = radius * Mathf.Sin(ToRadian(degree)) + centerY;  // Y座標.
		return new Vector2(x, y);
	}
	/// <summary>
	/// 中心から指定した角度で指定した半径分移動したとき、どこに行くか.
	/// </summary>
	/// <param name="pos">中心座標.</param>
	/// <param name="radius">半径.</param>
	/// <param name="degree">ディグリー角.</param>
	public static Vector2 PointOnCircle(Vector2 center, float radius, float degree) {
		return PointOnCircle(center.x, center.y, radius, degree);
	}
	/// <summary>
	/// 始点から終点まで、distanceだけ移動したときの位置.
	/// </summary>
	public static Vector2 PointToPointMovePoint(Vector2 start, Vector2 target, float distance) {
		return PointOnCircle(start, distance, PointToDegree(start, target));
	}
	/// <summary>
	/// 始点から終点までの距離.
	/// </summary>
	public static float PointToPointDistance(float sx, float sy, float tx, float ty) {
		var x = sx - tx;
		var y = sy - ty;
		return Mathf.Sqrt(Mathf.Pow(x, 2) + Mathf.Pow(y, 2));
	}
	/// <summary>
	/// 始点から終点までの距離.
	/// </summary>
	public static float PointToPointDistance(Vector2 start, Vector2 target) {
		return PointToPointDistance(start.x, start.y, target.x, target.y);
	}
	/// <summary>
	/// 円弧とタッチ(円)の衝突判定.
	/// </summary>
	/// <param name="cx">円の中心X座標.</param>
	/// <param name="cy">円の中心Y座標.</param>
	/// <param name="cr">円の半径.</param>
	/// <param name="startAngle">円の開始ディグリー角度.</param>
	/// <param name="sweepAngle">円のディグリー角度の大きさ.</param>
	/// <param name="px">タッチ(円)X座標.</param>
	/// <param name="py">タッチ(円)Y座標.</param>
	/// <param name="pr">タッチ(円)の半径.</param>
	/// <param name="hole">判定しない円の穴半径.</param>
	public static bool ArcIntersect(float cx, float cy, float cr, float startAngle, float sweepAngle, float px, float py, float pr, float hole) {
		float dx, dy, fAlpha, fBeta, fDelta, ar, fDistSqr, vx1, vy1, vx2, vy2;
		
		dx = px - cx;
		dy = py - cy;
		fDistSqr = dx * dx + dy * dy;
		
		var dsqrt = Mathf.Sqrt(fDistSqr); //点pと点cの長さ.
		
		//ドーナツ穴内のタッチは無効.
		//つまり、タッチ円が完全に穴の内部にあったら、タッチされていない.
		if (dsqrt < hole - pr) {
			return false;
		}
		else if (dsqrt < pr) {
			return true;
		}
		else {
			vx1 = Mathf.Cos(ToRadian(startAngle));
			vy1 = Mathf.Sin(ToRadian(startAngle));
			vx2 = Mathf.Cos(ToRadian(startAngle + sweepAngle));
			vy2 = Mathf.Sin(ToRadian(startAngle + sweepAngle));
			fDelta = vx1 * vy2 - vx2 * vy1;
			fAlpha = (dx * vy2 - dy * vx2) / fDelta;
			fBeta = (-dx * vy1 + dy * vx1) / fDelta;
			if ((fAlpha >= 0.0) && (fBeta >= 0.0)) {
				ar = cr + pr;
				if (fDistSqr <= ar * ar) {
					return true;
				}
			}
			else {
				//円と線の当たり判定 (扇形なので2本).
				var ret = LineAndCircleIntersect(px, py, pr, hole * vx1 + cx, hole * vy1 + cy, cr * vx1 + cx, cr * vy1 + cy);
				if (ret) return true;
				return LineAndCircleIntersect(px, py, pr, hole * vx2 + cx, hole * vy2 + cy, cr * vx2 + cx, cr * vy2 + cy);
			}
		}
		return false;
	}
	/// <summary>
	/// 円弧とタッチ(円)の衝突判定.
	/// </summary>
	/// <param name="center">円の中心座標.</param>
	/// <param name="cRadius">円の半径.</param>
	/// <param name="startAngle">円の開始ディグリー角度.</param>
	/// <param name="sweepAngle">円のディグリー角度の大きさ.</param>
	/// <param name="touchPoint">タッチ(円)座標.</param>
	/// <param name="tpRadius">タッチ(円)の半径.</param>
	/// <param name="holeRadius">判定しない円の穴半径.</param>
	public static bool ArcIntersect(Vector2 center, float cRadius, float startAngle, float sweepAngle, Vector2 touchPoint, float tpRadius, float holeRadius) {
		return ArcIntersect(center.x, center.y, cRadius, startAngle, sweepAngle, touchPoint.x, touchPoint.y, tpRadius, holeRadius);
	}
	/// <summary>
	/// 円と線の衝突判定.
	/// </summary>
	/// <param name="cx">円の中心X座標.</param>
	/// <param name="cy">円の中心Y座標.</param>
	/// <param name="cr">円の半径.</param>
	/// <param name="startX">線の始点のX座標.</param>
	/// <param name="startY">線の始点のY座標.</param>
	/// <param name="targetX">線の終点のX座標.</param>
	/// <param name="targetY">線の終点のY座標.</param>
	public static bool LineAndCircleIntersect(float cx, float cy, float cr, float startX, float startY, float targetX, float targetY) {
		var dx = targetX - startX;
		var dy = targetY - startY;
		if ((dx * (cx - startX) + dy * (cy - startY)) <= 0) {
			//始点を通る、線分に垂直な線を置いたとき、円の中心が線分の範囲外にあったとき.
			//「線分の始点から円の中心までの距離の2乗」と「円の半径の二乗」との比較.
			return Mathf.Pow(cr, 2) >= Mathf.Pow(cx - startX, 2) + Mathf.Pow(cy - startY, 2);
		}
		else if (((-dx) * (cx - (startX + dx)) + (-dy) * (cy - (startY + dy))) <= 0) {
			//終点を通る、線分に垂直な線を置いたとき、円の中心が線分の範囲外にあったとき.
			//「線分の終点から円の中心までの距離の2乗」と「円の半径の二乗」との比較.
			return Mathf.Pow(cr, 2) >= Mathf.Pow(cx - (startX + dx), 2) + Mathf.Pow(cy - (startY + dy), 2);
		}
		else {
			//線分の始点終点に垂線を引っ張ったとき、円の中心がその範囲内にあったとき.
			var e = Mathf.Sqrt(Mathf.Pow(dx, 2) + Mathf.Pow(dy, 2)); //これでx,y成分を割れば単ベクトルになる.
			var c2 = Mathf.Pow(cx - startX, 2) + Mathf.Pow(cy - startY, 2);
			var b = (cx - startX) * (dx / e) + (cy - startY) * (dy / e); //内責で算出した、隣辺の長さ.
			return Mathf.Pow(cr, 2) >= c2 - Mathf.Pow(b, 2);
		}
	}
	/// <summary>
	/// 円と線の衝突判定.
	/// </summary>
	/// <param name="center">円の中心座標.</param>
	/// <param name="radius">円の半径.</param>
	/// <param name="start">線の始点座標.</param>
	/// <param name="target">線の終点座標.</param>
	public static bool LineAndCircleIntersect(Vector2 center, float radius, Vector2 start, Vector2 target) {
		return LineAndCircleIntersect(center.x, center.y, radius, start.x, start.y, target.x, target.y);
	}
	/// <summary>
	/// 線と円の交点.
	/// </summary>
	/// <param name="cx">Cx.</param>
	/// <param name="cy">Cy.</param>
	/// <param name="cr">Cr.</param>
	/// <param name="sx">Sx.</param>
	/// <param name="sy">Sy.</param>
	/// <param name="tx">Tx.</param>
	/// <param name="ty">Ty.</param>
	public static Vector2[] LineAndCircleIntersectPoints(float cx, float cy, float cr, float sx, float sy, float tx, float ty) {
		// ax + by + c = 0
		float a, b, c;
		a = ty - sy;
		b = sx - tx;
		c = -(a * sx + b * sy);

		float l = a * a + b * b;
		float k = a * cx + b * cy + c;
		float d = l * cr * cr - k * k;
		if (d > 0){
			float ds = Mathf.Sqrt(d);
			float apl = a / l;
			float bpl = b / l;
			float xc = cx - apl * k;
			float yc = cy - bpl * k;
			float xd = bpl * ds;
			float yd = apl * ds;
			return new Vector2[] {
				new Vector2(xc - xd, yc + yd),
				new Vector2(xc + xd, yc - yd)
			};
		}
		else if (d == 0){
			return new Vector2[] {
				new Vector2(cx - a * k / l, cy - b * k / l)
			};
		}
		else{
			return new Vector2[0];
		}
	}
	/// <summary>
	/// 線と円の交点.
	/// </summary>
	/// <param name="center">円の中心座標.</param>
	/// <param name="radius">円の半径.</param>
	/// <param name="start">線の始点座標.</param>
	/// <param name="target">線の終点座標.</param>
	public static Vector2[] LineAndCircleIntersectPoints(Vector2 center, float radius, Vector2 start, Vector2 target) {
		return LineAndCircleIntersectPoints (center.x, center.y, radius, start.x, start.y, target.x, target.y);
	}
	/// <summary>
	/// 円と円の交点.
	/// </summary>
	/// <returns>The and circle intersect points.</returns>
	/// <param name="cx1">Cx1.</param>
	/// <param name="cy1">Cy1.</param>
	/// <param name="cr1">Cr1.</param>
	/// <param name="cx2">Cx2.</param>
	/// <param name="cy2">Cy2.</param>
	/// <param name="cr2">Cr2.</param>
	public static Vector2[] CircleAndCircleIntersectPoints(float cx1, float cy1, float cr1, float cx2, float cy2, float cr2) {
		// ax + by + c = 0
		float a, b, c;
		a = cx1 - cx2;
		b = cy1 - cy2;
		c = 0.5f * ((cr1 - cr2) * (cr1 + cr2) - a * (cx1 + cx2) - b * (cy1 + cy2));
		
		float l = a * a + b * b;
		float k = a * cx1 + b * cy1 + c;
		float d = l * cr1 * cr1 - k * k;
		if (d > 0){
			float ds = Mathf.Sqrt(d);
			float apl = a / l;
			float bpl = b / l;
			float xc = cx1 - apl * k;
			float yc = cy1 - bpl * k;
			float xd = bpl * ds;
			float yd = apl * ds;
			return new Vector2[] {
				new Vector2(xc - xd, yc + yd),
				new Vector2(xc + xd, yc - yd)
			};
		}
		else if (d == 0){
			return new Vector2[] {
				new Vector2(cx1 - a * k / l, cy1 - b * k / l)
			};
		}
		else{
			return new Vector2[0];
		}
	}
	/// <summary>
	/// 円と円の交点.
	/// </summary>
	/// <returns>The and circle intersect points.</returns>
	/// <param name="center1">円1の中心座標.</param>
	/// <param name="radius1">円1の半径.</param>
	/// <param name="center2">円2の中心座標.</param>
	/// <param name="radius2">円2の半径.</param>
	public static Vector2[] CircleAndCircleIntersectPoints(Vector2 center1, float radius1, Vector2 center2, float radius2) {
		return CircleAndCircleIntersectPoints (center1.x, center1.y, radius1, center2.x, center2.y, radius2);
	}
	/// <summary>
	/// 円と円弧の交点.
	/// </summary>
	/// <returns>The and arc intersect points.</returns>
	/// <param name="cx">Cx.</param>
	/// <param name="cy">Cy.</param>
	/// <param name="cr">Cr.</param>
	/// <param name="ax">Ax.</param>
	/// <param name="ay">Ay.</param>
	/// <param name="ar">Ar.</param>
	/// <param name="asd">円弧の開始角.</param>
	/// <param name="add">円弧の角度距離.</param>
	public static Vector2[] CircleAndArcIntersectPoints(float cx, float cy, float cr, float ax, float ay, float ar, float asd, float add) {
		var temp = CircleAndCircleIntersectPoints (cx, cy, cr, ax, ay, ar);
		var ret = new List<Vector2> ();
		foreach (var t in temp) {
			var deg = PointToDegree(new Vector2(ax, ay), t);
			if ((add >= 0 && asd <= deg && deg <= asd + add) ||
			    (add < 0 && asd + add <= deg && deg <= asd)) {
				ret.Add (t);
			}
		}
		return ret.ToArray ();
	}
	/// <summary>
	/// 円と円弧の交点.
	/// </summary>
	/// <returns>The and arc intersect points.</returns>
	/// <param name="cCenter">円の中心点.</param>
	/// <param name="cRadius">円の半径.</param>
	/// <param name="arcCenter">円弧の中心点.</param>
	/// <param name="arcRadius">円弧の半径.</param>
	/// <param name="arcDegOfStart">円弧の開始角.</param>
	/// <param name="arcDegOfDistance">円弧の角度距離.</param>
	public static Vector2[] CircleAndArcIntersectPoints(Vector2 cCenter, float cRadius, Vector2 arcCenter, float arcRadius, float arcDegOfStart, float arcDegOfDistance) {
		return CircleAndArcIntersectPoints (cCenter.x, cCenter.y, cRadius, arcCenter.x, arcCenter.y, arcRadius, arcDegOfStart, arcDegOfDistance);
	}
	/// <summary>
	/// s点とt点を通過する直線に対して、p点を通る垂直な線を引いたときの交点.
	/// </summary>
	/// <param name="px">Px.</param>
	/// <param name="py">Py.</param>
	/// <param name="sx">Sx.</param>
	/// <param name="sy">Sy.</param>
	/// <param name="tx">Tx.</param>
	/// <param name="ty">Ty.</param>
	public static Vector2 GetVerticalPoint (float px, float py, float sx, float sy, float tx, float ty) {
		// ax + by + c = 0
		float a, b, c;
		a = ty - sy;
		b = sx - tx;
		c = -(a * sx + b * sy);
		// 直線の始点、終点間の距離.
		float l = Mathf.Sqrt (Mathf.Pow (tx - sx, 2) + Mathf.Pow (ty - sy, 2));
		// 直線方向の単位ベクトル.
		float ex = (tx - sx) / l;
		float ey = (ty - sy) / l;
		// この直線に直角な単位ベクトル.
		float vx = -ey;
		float vy = ex;
		// 基準点から直線までの距離.
		float k = - (a * px + b * py + c) / (a * vx + b * vy);
		// 基準点から直線に対して垂直に交差する点.
		float qx = px + k * vx;
		float qy = py + k * vy;
		return new Vector2 (qx, qy);
	}
	/// <summary>
	/// s点とt点を通過する直線に対して、p点を通る垂直な線を引いたときの交点.
	/// </summary>
	/// <param name="verticalLineStartPoint">p点</param>
	/// <param name="linePassingPoint1">s点</param>
	/// <param name="linePassingPoint2">t点</param>
	public static Vector2 GetVerticalPoint (Vector2 verticalLineStartPoint, Vector2 linePassingPoint1, Vector2 linePassingPoint2) {
		return GetVerticalPoint (verticalLineStartPoint.x, verticalLineStartPoint.y, linePassingPoint1.x, linePassingPoint1.y, linePassingPoint2.x, linePassingPoint2.y);
	}
	/// <summary>
	/// 円Aと円Bの衝突判定.
	/// </summary>
	/// <param name="ax">円AのX座標.</param>
	/// <param name="ay">円AのY座標.</param>
	/// <param name="ar">円Aの半径.</param>
	/// <param name="bx">円BのX座標.</param>
	/// <param name="by">円BのY座標.</param>
	/// <param name="br">円Bの半径.</param>
	public static bool CircleIntersect(float ax, float ay, float ar, float bx, float by, float br) {
		return Mathf.Pow (bx - ax, 2) + Mathf.Pow (by - ay, 2) < Mathf.Pow (ar + br, 2);
	}
	/// <summary>
	/// 円Aと円Bの衝突判定.
	/// </summary>
	/// <param name="posA">円Aの座標.</param>
	/// <param name="radiusA">円Aの半径.</param>
	/// <param name="posB">円Bの座標.</param>
	/// <param name="radiusB">円Bの半径.</param>
	public static bool CircleIntersect(Vector2 posA, float radiusA, Vector2 posB, float radiusB) {
		return CircleIntersect(posA.x, posA.y, radiusA, posB.x, posB.y, radiusB);
	}
	/// <summary>
	/// 線1と線2の交点.
	/// </summary>
	/// <param name="sx1">線1の始点のX座標.</param>
	/// <param name="sy1">線1の始点のY座標.</param>
	/// <param name="tx1">線1の終点のX座標.</param>
	/// <param name="ty1">線1の終点のY座標.</param>
	/// <param name="sx2">線2の始点のX座標.</param>
	/// <param name="sy2">線2の始点のY座標.</param>
	/// <param name="tx2">線2の終点のX座標.</param>
	/// <param name="ty2">線2の終点のY座標.</param>
	public static Vector2 LinesIntersect(float sx1, float sy1, float tx1, float ty1, float sx2, float sy2, float tx2, float ty2) {
		var s1 = ((tx2 - sx2) * (sy1 - sy2) - (ty2 - sy2) * (sx1 - sx2)) / 2.0f;
		var s2 = ((tx2 - sx2) * (sy2 - ty1) - (ty2 - sy2) * (sx2 - tx1)) / 2.0f;
		var x = sx1 + (tx1 - sx1) * s1 / (s1 + s2);
		var y = sy1 + (ty1 - sy1) * s1 / (s1 + s2);
		return new Vector2(x, y);
	}
	/// <summary>
	/// 線1と線2の交点.
	/// </summary>
	/// <param name="start1">線1の始点の座標.</param>
	/// <param name="target1">線1の終点の座標.</param>
	/// <param name="start2">線2の始点の座標.</param>
	/// <param name="target2">線2の終点の座標.</param>
	public static Vector2 LinesIntersect(Vector2 start1, Vector2 target1, Vector2 start2, Vector2 target2) {
		return LinesIntersect(start1.x, start1.y, target1.x, target1.y, start2.x, start2.y, target2.x, target2.y);
	}
	/// <summary>
	/// 円弧の距離.
	/// </summary>
	/// <param name="radius">円弧の半径.</param>
	/// <param name="degree">円弧のディグリー角度.</param>
	public static float ArcDistance(float radius, float degree) {
		return 2.0f * Mathf.PI * radius * (degree / 360.0f);
	}
	/// <summary>
	/// 円弧上の距離から角度を求める.
	/// </summary>
	/// <param name="radius">円弧の半径.</param>
	/// <param name="distance">円弧上の距離.</param>
	public static float ArcDegreeFromArcDistance(float radius, float distance) {
		//距離 = 2*PI*radius * deg/360;
		//距離*360 = 2*PI*radius * deg;
		//距離*360/2/PI/radius = deg;
		return distance * 360.0f / 2.0f / Mathf.PI / radius;
	}
	/// <summary>
	/// 円から正方形を求める.
	/// </summary>
	/// <param name="cx">円の中心のX座標.</param>
	/// <param name="cy">円の中心のY座標.</param>
	/// <param name="radius">円の半径.</param>
	public static Rect CircleToRect(float cx, float cy, float radius) {
		float x = cx - radius;
		float y = cy - radius;
		float width = radius * 2.0f;
		float height = radius * 2.0f;
		return new Rect (x, y, width, height);
	}
	/// <summary>
	/// 円から正方形を求める.
	/// </summary>
	/// <param name="center">円の中心の座標.</param>
	/// <param name="radius">円の半径.</param>
	public static Rect CircleToRect(Vector2 center, float radius) {
		return CircleToRect(center.x, center.y, radius);
	}
	/// <summary>
	/// 中心から指定した角度で指定した半径分移動したとき、どこに行くか (楕円).
	/// </summary>
	/// <param name="centerX">中心X座標.</param>
	/// <param name="centerY">中心Y座標.</param>
	/// <param name="horizonRadius">横半径.</param>
	/// <param name="verticalRadius">縦半径.</param>
	/// <param name="degree">ディグリー角.</param>
	public static Vector2 PointOnEllipse(float centerX, float centerY, float horizonRadius, float verticalRadius, float degree) {
		float longRadius = Mathf.Max (horizonRadius, verticalRadius);
		float shortRadius = Mathf.Min (horizonRadius, verticalRadius);
		var x = longRadius * Mathf.Cos(ToRadian(degree)) + centerX;  // X座標.
		var y = shortRadius * Mathf.Sin(ToRadian(degree)) + centerY;  // Y座標.
		return new Vector2(x, y);
	}
	/// <summary>
	/// 中心から指定した角度で指定した半径分移動したとき、どこに行くか (楕円).
	/// </summary>
	/// <param name="pos">中心座標.</param>
	/// <param name="horizonRadius">横半径.</param>
	/// <param name="verticalRadius">縦半径.</param>
	/// <param name="degree">ディグリー角.</param>
	public static Vector2 PointOnEllipse(Vector2 center, float horizonRadius, float verticalRadius, float degree) {
		return PointOnEllipse(center.x, center.y, horizonRadius, verticalRadius, degree);
	}
	/// <summary>
	/// 中心から指定した角度で指定した半径分移動したとき、どこに行くか (楕円).
	/// </summary>
	/// <param name="pos">中心座標.</param>
	/// <param name="radius">半径.</param>
	/// <param name="degree">ディグリー角.</param>
	public static Vector2 PointOnEllipse(Vector2 center, Vector2 radius, float degree) {
		return PointOnEllipse(center.x, center.y, radius.x, radius.y, degree);
	}
	/// <summary>
	/// 楕円弧の距離.
	/// </summary>
	/// <param name="radius">円弧の半径.</param>
	/// <param name="degree">円弧のディグリー角度.</param>
	public static float EllipseArcDistance(float horizonRadius, float verticalRadius, float degree) {
		float longRadius = Mathf.Max (horizonRadius, verticalRadius);
		float shortRadius = Mathf.Min (horizonRadius, verticalRadius);
		float t = Mathf.Atan ((verticalRadius / horizonRadius) * Mathf.Tan (degree / 2));
		float e = Mathf.Sqrt (Mathf.Pow (longRadius, 2) - Mathf.Pow (shortRadius, 2)) / longRadius;

		float E;
		float L = 2 * longRadius;
		if (horizonRadius > verticalRadius) {
			float ret;
			bool ok = E2 (t, e, out ret);
			if (!ok) Debug.LogError("E2 error.");
			E = ret;
		}
		else {
			float ret1;
			float ret2;
			bool ok1 = E2 (Mathf.PI / 2, e, out ret1);
			bool ok2 = E2 (Mathf.PI / 2 - t, e, out ret2);
			if (!ok1 || !ok2) Debug.LogError("E2 error.");
			E = ret1 - ret2;
		}
		L *= E;
		return L;
	}

	/// <summary>
	/// 第二種楕円積分の近似法.
	/// </summary>
	public static bool E2(float fa, float k, out float result) {
		result = 0;
		float ph = 1.5707963269f;
		if (fa < 0 || fa > ph) return false; //"faが不適切".
		if (k < 0 || k > 1) return false; // "kが不適切".
		if (k > 0.9999999999) {
			result = Mathf.Sin(fa);
			return true;
		}
		float kr = k;
		float a2 = 1;
		float a1 = 0;
		float e = 0;
		float s = Mathf.Sin(fa);
		float k2;
		float kt;
		float b;
		float s2;
		float ac;
		float c;
		float tn;
		
		while (true) {
			k2 = Mathf.Pow(k, 2);
			if (k2 == 0) {
				kt = 0;
			}
			else {
				kt = Mathf.Pow((1 - Mathf.Sqrt(1 - k2)), 2) / k2;
			}
			if (kt < 0.0000000001) break;
			k = kt;
			b = 1 + k;
			if (s != 0) {
				s = (b - Mathf.Sqrt(Mathf.Pow(b, 2) - Mathf.Pow(4 * k * s, 2))) / 2 / k / s;
				s2 = Mathf.Pow(s, 2);
				a1 = a1 * b + a2 * (k - 1);
				ac = a2 * 2 * k / b;
				c = 1 - s2;
				if (c < 0) {
					c = 0;
				}
				else {
					c = Mathf.Sqrt(c);
				}
				e = e + ac * s * c * Mathf.Sqrt(1 - Mathf.Pow(k, 2) * s2) / (1 + k * s2);
				a2 = a2 * 2 / b;
			}
		}
		if (s < 1) {
			tn = s / Mathf.Sqrt(1 - Mathf.Pow(s, 2));
		}
		else {
			bool casted = float.TryParse("1E+20", System.Globalization.NumberStyles.AllowExponent, new System.Globalization.NumberFormatInfo(), out tn);
			if (!casted) Debug.LogError ("1E+20 is NaN.");
		}
		result = Mathf.Atan(tn) * (a2 + a1) + e;
		k = kr;
		return true;
	}

	/// <summary>
	/// 始点が同じである2本の線分の、近いほうの角度(0～180).
	/// </summary>
	public static float TwoVectorsNearestDegree (Vector2 target1, Vector2 target2) {
		return Mathf.Acos(Vector2.Dot (target1.normalized, target2.normalized)) * Mathf.Rad2Deg;
	}

	/// <summary>
	/// 始点が同じである2本の線分の、遠いほうの角度(180～360).
	/// </summary>
	public static float TwoVectorsFarthestDegree (Vector2 target1, Vector2 target2) {
		return 360.0f - TwoVectorsNearestDegree(target1, target2);
	}
	
	/// <summary>
	/// 始点が同じである2本の線分の、"線1に垂直な線"に対する線2の角度.
	/// </summary>
	public static float TwoVectorsVerticalLineDegree (Vector2 target1, Vector2 target2) {
		return Mathf.Asin(Vector3.Cross (target1.normalized, target2.normalized).z) * Mathf.Rad2Deg;
	}
	
	/// <summary>
	/// 始点が同じである2本の線分の、近道な回転方向の角度距離.
	/// </summary>
	public static float TwoVectorsNearestDegDistance (Vector2 target1, Vector2 target2) {
		float sin = TwoVectorsVerticalLineDegree (target1, target2);
		int vec = sin == 0 ? 0 : sin > 0 ? 1 : -1;
		float angle = TwoVectorsNearestDegree (target1, target2);
		return angle * vec;
	}

	/// <summary>
	/// 始点が同じである2本の線分の、遠回りな回転方向の角度距離.
	/// </summary>
	public static float TwoVectorsFarthestDegDistance (Vector2 target1, Vector2 target2) {
		float sin = TwoVectorsVerticalLineDegree (target1, target2);
		int vec = sin == 0 ? 0 : sin > 0 ? 1 : -1;
		float angle = TwoVectorsFarthestDegree (target1, target2);
		if (vec != 0) return angle * -vec;
		return 360.0f * vec;
	}

	/// <summary>
	/// 開始角から終了角まで、時計回りで回ったときの角度距離(単位:degree).
	/// </summary>
	public static float DegToDegClockwiseDistanceDeg (float degOfStart, float degOfTarget) {
		while (degOfStart < 0) degOfStart += 360.0f;
		while (degOfTarget < 0) degOfTarget += 360.0f;
		while (degOfStart > degOfTarget) degOfTarget += 360.0f;
		return degOfTarget - degOfStart;
	}

	/// <summary>
	/// 開始角から終了角まで、反時計回りで回ったときの角度距離(単位:-degree).
	/// </summary>
	public static float DegToDegCounterClockwiseDistanceDeg (float degOfStart, float degOfTarget) {
		while (degOfStart < 0) degOfStart += 360.0f;
		while (degOfTarget < 0) degOfTarget += 360.0f;
		while (degOfStart < degOfTarget) degOfStart += 360.0f;
		return -(degOfStart - degOfTarget);
	}



}
