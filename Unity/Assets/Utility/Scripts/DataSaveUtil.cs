﻿using System;
using System.Text;
using System.Security.Cryptography;

public class EncryptUtil
{
    // 暗号化キーなど.
    private const string AesIVStr = @"0123012301230123";
    private const string AesKeyStr = @"0123456789abcdef";
    private const int AesBlockSize = 128;
    private const int AesKeySize = 128;
    private static byte[] AesIV = Encoding.UTF8.GetBytes(AesIVStr);
    private static byte[] AesKey = Encoding.UTF8.GetBytes(AesKeyStr);

    // キー文字列.
    public static string saveDataKey = "SaveData";
    public static string backUpSaveDataKey = "SaveDataBack";

    /// <summary>
    /// 文字列を暗号化する.
    /// 暗号化された文字列はBase64でエンコードされる.
    /// </summary>
    /// <returns>
    /// 暗号化されたBase64文字列.
    /// </returns>
    /// <param name='text'>
    /// 暗号化する文字列.
    /// </param>
    /// <param name='iv'>
    /// 暗号化に使用する初期ベクター.
    /// </param>
    /// <param name='key'>
    /// 暗号に使用するキー文字列.
    /// </param>
    public string EncryptString(string text, byte[] iv = null, byte[] key = null)
    {
        if (iv == null) iv = AesIV;
        if (key == null) key = AesKey;
        AesManaged aes = new AesManaged();
        aes.BlockSize = AesBlockSize;
        aes.KeySize = AesKeySize;
        aes.IV = iv;
        aes.Key = key;
        aes.Mode = CipherMode.CBC;
        aes.Padding = PaddingMode.PKCS7;

        // 暗号化.
        byte[] textBytes = Encoding.UTF8.GetBytes(text);
        ICryptoTransform encryptor = aes.CreateEncryptor();
        byte[] cryptedTextBytes = encryptor.TransformFinalBlock(textBytes, 0, textBytes.Length);
        encryptor.Dispose();

        // MD5追加.
        MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
        byte[] md5hash = md5.ComputeHash(cryptedTextBytes);
        md5.Clear();
        byte[] resultBytes = new byte[md5hash.Length + cryptedTextBytes.Length];
        md5hash.CopyTo(resultBytes, 0);
        cryptedTextBytes.CopyTo(resultBytes, md5hash.Length);


        return Convert.ToBase64String(resultBytes);
    }

    /// <summary>
    /// EncryoptString関数で暗号化されたデータを複合化する.
    /// </summary>
    /// <returns>
    /// 成功ならtrue / データが破損していたらfalse.
    /// </returns>
    /// <param name='text'>
    /// 暗号化されたデータ.
    /// </param>
    /// <param name='dest'>
    /// 複合化された文字列の出力先.
    /// </param>
    /// <param name='iv'>
    /// 暗号化に使用する初期ベクター.
    /// </param>
    /// <param name='key'>
    /// 暗号に使用するキー文字列.
    /// </param>
    public bool DecryptString(string text, out string dest, byte[] iv = null, byte[] key = null)
    {
        if (iv == null) iv = AesIV;
        if (key == null) key = AesKey;
        dest = "Error";
        AesManaged aes = new AesManaged();
        aes.BlockSize = AesBlockSize;
        aes.KeySize = AesKeySize;
        aes.IV = iv;
        aes.Key = key;
        aes.Mode = CipherMode.CBC;
        aes.Padding = PaddingMode.PKCS7;

        byte[] bytes = Convert.FromBase64String(text);
        if (bytes.Length < 16)
            return false;
        byte[] md5hash = new byte[16];
        Array.Copy(bytes, md5hash, md5hash.Length);
        byte[] cryptedTextBytes = new byte[bytes.Length - md5hash.Length];
        Array.Copy(bytes, md5hash.Length, cryptedTextBytes, 0, bytes.Length - md5hash.Length);

        // MD5チェック.
        MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
        byte[] testMd5hash = md5.ComputeHash(cryptedTextBytes);
        md5.Clear();
        for (int i = 0; i < md5hash.Length; i++)
        {
            if (md5hash[i] != testMd5hash[i])
                return false;
        }

        // 複合化.
        try
        {
            ICryptoTransform decryptor = aes.CreateDecryptor();
            byte[] textBytes = decryptor.TransformFinalBlock(cryptedTextBytes, 0, cryptedTextBytes.Length);
            decryptor.Dispose();
            dest = Encoding.UTF8.GetString(textBytes);
            return true;
        }
        catch
        {
            return false;
        }

    }

    /// <summary>
    /// 文字列を暗号化する.
    /// バイナリデータとして返す.
    /// </summary>
    /// <returns>
    /// 暗号化されたバイナリ配列.
    /// </returns>
    /// <param name='text'>
    /// 暗号化する文字列.
    /// </param>
    /// <param name='iv'>
    /// 暗号化に使用する初期ベクター.
    /// </param>
    /// <param name='key'>
    /// 暗号に使用するキー文字列.
    /// </param>
    public static byte[] StringToEncryptByte(string text, byte[] iv = null, byte[] key = null)
    {
        if (iv == null) iv = AesIV;
        if (key == null) key = AesKey;
        AesManaged aes = new AesManaged();
        aes.BlockSize = AesBlockSize;
        aes.KeySize = AesKeySize;
        aes.IV = iv;
        aes.Key = key;
        aes.Mode = CipherMode.CBC;
        aes.Padding = PaddingMode.PKCS7;

        // 暗号化.
        byte[] textBytes = Encoding.UTF8.GetBytes(text);
        ICryptoTransform encryptor = aes.CreateEncryptor();
        byte[] cryptedTextBytes = encryptor.TransformFinalBlock(textBytes, 0, textBytes.Length);
        encryptor.Dispose();

        // MD5追加.
        MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
        byte[] md5hash = md5.ComputeHash(cryptedTextBytes);
        md5.Clear();
        byte[] resultBytes = new byte[md5hash.Length + cryptedTextBytes.Length];
        md5hash.CopyTo(resultBytes, 0);
        cryptedTextBytes.CopyTo(resultBytes, md5hash.Length);


        return resultBytes;
    }

    /// <summary>
    /// StringToEncryptByte関数で暗号化されたデータを複合化する.
    /// </summary>
    /// <returns>
    /// 成功ならtrue / データが破損していたらfalse.
    /// </returns>
    /// <param name='encryptSrc'>
    /// 暗号化されたデータ.
    /// </param>
    /// <param name='dest'>
    /// 複合化された文字列の出力先.
    /// </param>
    /// <param name='iv'>
    /// 暗号化に使用する初期ベクター.
    /// </param>
    /// <param name='key'>
    /// 暗号に使用するキー文字列.
    /// </param>
    public static bool EncryptByteToDecryptString(byte[] encryptSrc, out string dest, byte[] iv = null, byte[] key = null)
    {
        if (iv == null) iv = AesIV;
        if (key == null) key = AesKey;
        dest = "Error";
        AesManaged aes = new AesManaged();
        aes.BlockSize = AesBlockSize;
        aes.KeySize = AesKeySize;
        aes.IV = iv;
        aes.Key = key;
        aes.Mode = CipherMode.CBC;
        aes.Padding = PaddingMode.PKCS7;

        byte[] bytes = encryptSrc;
        if (bytes.Length < 16)
            return false;
        byte[] md5hash = new byte[16];
        Array.Copy(bytes, md5hash, md5hash.Length);
        byte[] cryptedTextBytes = new byte[bytes.Length - md5hash.Length];
        Array.Copy(bytes, md5hash.Length, cryptedTextBytes, 0, bytes.Length - md5hash.Length);

        // MD5チェック.
        MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
        byte[] testMd5hash = md5.ComputeHash(cryptedTextBytes);
        md5.Clear();
        for (int i = 0; i < md5hash.Length; i++)
        {
            if (md5hash[i] != testMd5hash[i])
                return false;
        }

        // 複合化.
        try
        {
            ICryptoTransform decryptor = aes.CreateDecryptor();
            byte[] textBytes = decryptor.TransformFinalBlock(cryptedTextBytes, 0, cryptedTextBytes.Length);
            decryptor.Dispose();
            dest = Encoding.UTF8.GetString(textBytes);
            return true;
        }
        catch
        {
            return false;
        }
    }

}
