﻿using UnityEngine;
using System.Collections;

public class FPS : MonoBehaviour {
	public bool fpsEnabled;
	public Vector2 infoDrawPos; // 表示位置。0～1で指定.
	private Vector2 oldInfoDrawPos;
	private float deltaTimeSum = 0.0f;
	private int fpsMeasurementCounter = 0;
	private float fps;
	private Vector2 fpsPos = Vector2.zero;
	private Rect fpsRect;

	void Start() {
		oldInfoDrawPos = infoDrawPos;
	}
	
	void Update () {
		// FPS.
		deltaTimeSum += Time.deltaTime;
		fpsMeasurementCounter++;
		if (fpsMeasurementCounter >= 30) {
			fps = (float)fpsMeasurementCounter / deltaTimeSum ;
			fpsMeasurementCounter = 0;
			deltaTimeSum = 0.0f;
		}
	}
	
	void OnGUI() {
		if (fpsEnabled) {
			if (fpsPos == Vector2.zero || infoDrawPos.x != oldInfoDrawPos.x || infoDrawPos.y != oldInfoDrawPos.y) {
				fpsPos = new Vector2 (infoDrawPos.x * Screen.width, infoDrawPos.y * Screen.height);
				fpsRect = new Rect (fpsPos.x, fpsPos.y, 480, 44);
			}
			GUI.Label (fpsRect, string.Format ("FPS: {0:0.000}", fps));
		}
	}
}