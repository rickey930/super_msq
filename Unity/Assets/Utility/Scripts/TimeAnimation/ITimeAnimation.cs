﻿using UnityEngine;
using System.Collections;

public interface ITimeAnimation {
	void SetClearanceType(ClearanceType value);
	void Restart();
}
