﻿using UnityEngine;
using System.Collections;

public abstract class TimeAnimationBase<T, D> : MonoBehaviour, ITimeAnimation where D : TimeAnimationData<T> {
	protected abstract void ReferenceValue (T value);

	/// <summary>ループ1回目を実行するまでの初期値</summary>
	[SerializeField]
	private T initValue;
	///<summary>ループ1回目を実行するまでの待ち時間</summary>
	[SerializeField]
	private float delayTime;
	///<summary>あと何回ループするか. 負の数で無限にループ</summary>
	[SerializeField]
	private int loop = -1;
	//	///<summary>初期値は前回の目標値を受け継ぎ、連続アニメーションにする</summary>
	//	[SerializeField]
	//	private bool continuity;
	///<summary>ループ終了時に毎回実行されるイベント</summary>
	public System.Action<GameObject> loopEndEvent { protected get; set; }
	///<summary>アニメーションが終わると実行されるイベント</summary>
	public System.Action<GameObject> animationEndEvent  { protected get; set; }
	[SerializeField]
	public ClearanceType clearanceType;
	[SerializeField]
	public Transform clearanceRoot;
	[SerializeField]
	private bool invalidate;
	
	[SerializeField]
	private D[] data;
	public D[] GetData { get { return data; } }
	private float timer;
	private int index;
	private float delayTimer;
	
	protected virtual void Start () {
		ReferenceValue(initValue);
		Update ();
	}

	public void Restart () {
		index = 0;
		timer = 0;
		delayTimer = 0;
		ReferenceValue(initValue);
		Update ();
	}
	
	private void Update () {
		if (delayTimer < delayTime) {
			delayTimer += Time.deltaTime;
			return;
		}
		if (index >= data.Length) {
			bool end = false;
			if (loop > 0 || loop < 0) {
				if (loopEndEvent != null) loopEndEvent(gameObject);
				if (loop > 0) loop--;
				if (loop == 0) {
					end = true;
				}
				else {
					index = 0;
				}
			}
			else {
				end = true;
			}
			if (end) {
				var maxvalue = data[data.Length - 1].GetValue (1.0f);
				if (!invalidate) {
					ReferenceValue(maxvalue);
				}
				if (animationEndEvent != null) animationEndEvent(gameObject);
				Clearance();
				return;
			}
		}
		
		var info = data[index];

		float percent;
		if (info.time != 0) {
			percent = timer / info.time;
		}
		else {
			percent = 1;
		}
		var value = info.GetValue (percent);
		if (!invalidate) {
			ReferenceValue(value);
		}
		timer += Time.deltaTime;
		if (timer >= info.time) {
			timer = 0;
			index++;
		}
	}
	
	private void Clearance() {
		switch (clearanceType) {
		case ClearanceType.DESTROY:
			if (clearanceRoot == null) {
				clearanceRoot = transform;
			}
			Destroy(clearanceRoot.gameObject);
			break;
		case ClearanceType.DEACTIVE:
			if (clearanceRoot == null) {
				clearanceRoot = transform;
			}
			clearanceRoot.gameObject.SetActive(false);
			break;
		}
	}
	
	public void SetClearanceType(ClearanceType value) {
		clearanceType = value;
	}
}

public enum ClearanceType {
	NOTHING, DESTROY, DEACTIVE, 
}

[System.Serializable]
public abstract class TimeAnimationData<T> {
	protected abstract T TargetMinusStart (T start, T target);
	protected abstract T StartPlusRangeMultiplyPercent (T start, T range, float percent);
	
	public float time;
	public T start;
	public T target;
	public PercentUnlimitedFlag unlimited;
	
	public T GetValue(float percent) {
		float rate = percent;
		if (!unlimited.under && rate < 0) rate = 0;
		if (!unlimited.over && rate > 1) rate = 1;
		T range = TargetMinusStart(start, target);
		T value = StartPlusRangeMultiplyPercent(start, range, percent);
		return value;
	}
}

[System.Serializable]
public class PercentUnlimitedFlag {
	public bool under;
	public bool over;
}