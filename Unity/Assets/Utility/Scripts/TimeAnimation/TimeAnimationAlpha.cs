﻿using UnityEngine;
using System.Collections;

public class TimeAnimationAlpha : TimeAnimationBase<float, FloatAnimationData> {
	private SpriteRenderer spRenderer;
	private MeshRenderer meshRenderer;
	private UnityEngine.UI.Image uiRenderer;
	protected override void Start () {
		base.Start ();
		spRenderer = GetComponent<SpriteRenderer> ();
		meshRenderer = GetComponent<MeshRenderer> ();
		uiRenderer = GetComponent<UnityEngine.UI.Image> ();
	}

	protected override void ReferenceValue (float value) {
		if (spRenderer != null) {
			if (spRenderer.sharedMaterial.HasProperty ("_Color")) {
				Color temp = spRenderer.sharedMaterial.GetColor ("_Color");
				temp.a = value;
				spRenderer.material.SetColor ("_Color", temp);
			}
			else {
				Color temp = spRenderer.color;
				temp.a = value;
				spRenderer.color = temp;
			}
		}
		if (meshRenderer != null) {
			Color temp = meshRenderer.sharedMaterial.GetColor ("_Color");
			temp.a = value;
			meshRenderer.material.SetColor ("_Color", temp);
		}
		if (uiRenderer != null) {
			Color temp = uiRenderer.color;
			temp.a = value;
			uiRenderer.color = temp;
		}
	}
}

[System.Serializable]
public class FloatAnimationData : TimeAnimationData<float> {
	protected override float TargetMinusStart (float start, float target) {
		return target - start;
	}
	protected override float StartPlusRangeMultiplyPercent (float start, float range, float percent) {
		return start + range * percent;
	}
}