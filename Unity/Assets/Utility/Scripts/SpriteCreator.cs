﻿using UnityEngine;
using System.Collections;

public class SpriteCreator {
	public delegate void DrawMethod(Texture2D texture);
	// 1度ロードで作ったらキャッシュして使いまわす.重たいのでリアルタイム向きではない.

	public static Sprite Create(int width, int height, DrawMethod method, string spriteName = "Dynamic Created Sprite", string textureName = "Dynamic Created Texture") {
		Texture2D texture = new Texture2D (width, height);
		texture.name = textureName;
		// テクスチャ全体を透過.
		texture.SetPixels (0, 0, width, height, new Color(0, 0, 0, 0));
		// お絵かき.
		if (method != null) method (texture);
		// 反映.
		texture.Apply();
		Sprite ret = Sprite.Create (texture, new Rect (0, 0, width, height), new Vector2(0.5f, 0.5f), 1.0f, 1, SpriteMeshType.FullRect);
		ret.name = spriteName;
		return ret;
	}
}

static partial class UnityEngineExtension {
	public static Vector2 GetCenter(this Texture2D texture) {
		return new Vector2 ((float)texture.width / 2.0f, (float)texture.height / 2.0f);
	}
	
	public static void SetPixels(this Texture2D texture, Vector2 pos, int width, int height, Color color) {
		texture.SetPixels(pos.x.ToInt(), pos.y.ToInt(), width, height, color);
	}
	public static void SetPixels(this Texture2D texture, int x, int y, int width, int height, Color color)
	{
		Color[] colors = new Color[width * height];
		for (int i = 0; i < colors.Length; i++) colors[i] = color;
		texture.SetPixels (x, y, width, height, colors);
	}
	
	public static void FillRect(this Texture2D canvas, Vector2 pos, int width, int height, Color color) {
		canvas.FillRect(pos.x.ToInt(), pos.y.ToInt(), width, height, color);
	}
	public static void FillRect(this Texture2D canvas, int x, int y, int width, int height, Color color) {
		for (int dy = y; dy < y + height; dy++) {
			if (dy >= 0 && dy < canvas.height) {
				for (int dx = x; dx < x + width; dx++) {
					if (dx >= 0 && dx < canvas.width) {
						canvas.SetPixel(dx, canvas.height - 1 - dy, color);
					}
				}
			}
		}
	}
	
	public static void DrawRect(this Texture2D canvas, Vector2 pos, int width, int height, Color color, int bold) {
		canvas.DrawRect(pos.x.ToInt(), pos.y.ToInt(), width, height, color, bold);
	}
	public static void DrawRect(this Texture2D canvas, int x, int y, int width, int height, Color color, int bold) {
		if (bold < 1) bold = 1;
		for (int dy = y; dy < y + height; dy++) {
			if (dy >= 0 && dy < canvas.height) {
				for (int dx = x; dx < x + width; dx++) {
					if (dx >= 0 && dx < canvas.width) {
						if (dy <= y + bold || dy > height - 2 - bold ||
						    dx <= x + bold || dx > width - 2 - bold) {
							canvas.SetPixel(dx, canvas.height - 1 - dy, color);
						}
					}
				}
			}
		}
	}

	public static void DrawLine(this Texture2D canvas, Vector2 start, Vector2 target, Color color, int bold) {
		float distance = CircleCalculator.PointToPointDistance(start, target);
		for (int i = 0; i < distance; i++) {
			Vector2 pos = CircleCalculator.PointToPointMovePoint (start, target, i);
			int dx = pos.x.ToInt();
			int dy = pos.y.ToInt();
			FillRect(canvas, dx, dy, bold, bold, color);
		}
	}

	public static void DrawArc(this Texture2D canvas, Vector2 pos, float radius, float startDeg, float sweepDeg, Color color, int bold) {
		canvas.DrawArc(pos.x.ToInt(), pos.y.ToInt(), radius, startDeg, sweepDeg, color, bold);
	}
	public static void DrawArc(this Texture2D canvas, int x, int y, float radius, float startDeg, float sweepDeg, Color color, int bold) {
		for (float deg = startDeg; deg < startDeg + sweepDeg + 1; deg++) {
			Vector2 d1 = CircleCalculator.PointOnCircle(x, y, radius, deg);
			Vector2 d2 = CircleCalculator.PointOnCircle(x, y, radius, deg + 1);
			DrawLine(canvas, d1, d2, color, bold);
		}
	}

	public static void DrawCircle(this Texture2D canvas, Vector2 pos, float radius, Color color, int bold) {
		canvas.DrawCircle(pos.x.ToInt(), pos.y.ToInt(), radius, color, bold);
	}
	public static void DrawCircle(this Texture2D canvas, int x, int y, float radius, Color color, int bold) {
		DrawArc(canvas, x, y, radius, 0, 359, color, bold);
	}
	
	public static void FillArc(this Texture2D canvas, Vector2 pos, float radius, float startDeg, float sweepDeg, Color color) {
		canvas.FillArc(pos.x.ToInt(), pos.y.ToInt(), radius, startDeg, sweepDeg, color);
	}
	public static void FillArc(this Texture2D canvas, int x, int y, float radius, float startDeg, float sweepDeg, Color color) {
		for (float deg = startDeg; deg < startDeg + sweepDeg + 1; deg++) {
			Vector2 d1 = new Vector2(x, y);
			Vector2 d2 = CircleCalculator.PointOnCircle(x, y, radius, deg);
			DrawLine(canvas, d1, d2, color, 2);
		}
	}
	
	public static void FillCircle(this Texture2D canvas, Vector2 pos, float radius, Color color) {
		canvas.FillCircle(pos.x.ToInt(), pos.y.ToInt(), radius, color);
	}
	public static void FillCircle(this Texture2D canvas, int x, int y, float radius, Color color) {
		for (int deg = 0; deg < 180; deg++) {
			Vector2 d1 = CircleCalculator.PointOnCircle(x, y, radius, deg);
			Vector2 d2 = CircleCalculator.PointOnCircle(x, y, radius, -deg);
			DrawLine(canvas, d1, d2, color, 2);
		}
	}
}
