﻿using UnityEngine;
using System.Collections;

public class TwoDimensionCamera : MonoBehaviour {

	public enum RealScreenVectorType : int
	{
		NONE,
		PORTRAIT,
		LANDSCAPE,
	}
	/// <summary>
	/// 実際の画面が縦長であるか否か.
	/// </summary>
	public static RealScreenVectorType screenType { get; set; }
	
	public int oldWidth { get; protected set; }
	public int oldHeight { get; protected set; }

	#region(inspector settings)
	public float virtualScreenSizeWidth;
	public float virtualScreenSizeHeight;
	public bool portrait = true;
	#endregion

	private void Awake () {
		screenType = RealScreenVectorType.NONE;
		
		oldWidth = Screen.width;
		oldHeight = Screen.height;
		
		AspectSetting ();
		// MEMO:NGUIのmanualHeight設定は不要、
		// UI Root下のカメラのアスペクト比固定すればよい
		// UI RootのAutomaticはOFF, Manual Heightは想定heightを設定する
	}
	
	private void AspectSetting () {
		float fw = portrait ? this.virtualScreenSizeWidth : this.virtualScreenSizeHeight;
		float fh = portrait ? this.virtualScreenSizeHeight : this.virtualScreenSizeWidth;
		
		// camera
		if( this.GetComponent<Camera>() != null ){
			Rect set_rect = this.calc_aspect(fw, fh, out resolutionScale);
			GetComponent<Camera>().rect = set_rect;
			GetComponent<Camera>().orthographicSize = virtualScreenSizeHeight / 2.0f;
		}
	}
	
	private void Update () {
		if (oldWidth != Screen.width || oldHeight != Screen.height) {
			oldWidth = Screen.width;
			oldHeight = Screen.height;
			AspectSetting ();
		}
	}
	
	public static float resolutionScale = -1.0f;
	// アスペクト比 固定するようにcameraのrect取得
	Rect calc_aspect(float width, float height, out float res_scale) {
		float target_aspect = width / height;
		float window_aspect = (float)Screen.width / (float)Screen.height;
		float scale = window_aspect / target_aspect;
		
		Rect rect = new Rect(0.0f, 0.0f, 1.0f, 1.0f);
		if(1.0f > scale){
			rect.x = 0;
			rect.width = 1.0f;
			rect.y = (1.0f - scale) / 2.0f;
			rect.height = scale;
			res_scale = (float)Screen.width / width;
			screenType = RealScreenVectorType.PORTRAIT;
		} else {
			scale = 1.0f / scale;
			rect.x = (1.0f - scale) / 2.0f;
			rect.width = scale;
			rect.y = 0.0f;
			rect.height = 1.0f;
			res_scale = (float)Screen.height / height;
			screenType = RealScreenVectorType.LANDSCAPE;
		}
		
		return rect;
	}
}
