﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SerializeDictionary {
	/// <summary>
	/// テーブルの管理クラス
	/// </summary>
	[System.Serializable]
	public class TableBase<TKey, TValue, Type> where Type : KeyAndValue<TKey, TValue>{
		[SerializeField]
		private List<Type> list;
		private Dictionary<TKey, TValue> table;
		

		/// <summary>
		/// プログラム上からDictionaryを見る.
		/// </summary>
		/// <returns>The table.</returns>
		public Dictionary<TKey, TValue> GetTable () {
			if (table == null) {
				table = ConvertListToDictionary(list);
			}
			return table;
		}
		
		/// <summary>
		/// Editor Only
		/// </summary>
		public List<Type> GetList () {
			return list;
		}
		
		static Dictionary<TKey, TValue> ConvertListToDictionary (List<Type> list) {
			Dictionary<TKey, TValue> dic = new Dictionary<TKey, TValue> ();
			foreach(KeyAndValue<TKey, TValue> pair in list){
				dic.Add(pair.Key, pair.Value);
			}
			return dic;
		}

		public void reCreateTable() {
			table = ConvertListToDictionary(list);
		}
	}
	
	/// <summary>
	/// シリアル化できる、KeyValuePair
	/// </summary>
	[System.Serializable]
	public class KeyAndValue<TKey, TValue>
	{
		public TKey Key;
		public TValue Value;
		
		public KeyAndValue(TKey key, TValue value)
		{
			Key = key;
			Value = value;
		}
		public KeyAndValue(KeyValuePair<TKey, TValue> pair)
		{
			Key = pair.Key;
			Value = pair.Value;
		}
		
		
	}
}