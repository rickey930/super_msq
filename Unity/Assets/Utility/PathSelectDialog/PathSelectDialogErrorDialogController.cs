﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PathSelectDialogErrorDialogController : MonoBehaviour {
	[SerializeField]
	private Text errorMessageLabel;
	
	public void SetMessage (string message) {
		errorMessageLabel.text = message;
	}
	
	public void Show () {
		gameObject.SetActive (true);
	}
	
	public void Hide () {
		gameObject.SetActive (false);
	}
	
}

