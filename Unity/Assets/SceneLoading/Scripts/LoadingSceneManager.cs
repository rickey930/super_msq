﻿using UnityEngine;
using System.Collections;

public class LoadingSceneManager : MonoBehaviour {

	[SerializeField]
	private UnityEngine.UI.Image gameLogo;

	private readonly string[] spriteNames = new string[] {
		"loading_logo_blue",
		"loading_logo_green",
		"loading_logo_orange",
		"loading_logo_pink",
	};

	// Use this for initialization
	IEnumerator Start () {
		gameLogo.sprite = SpriteTank.Get (spriteNames[Random.Range (0, spriteNames.Length)]);
		while (UserDataController.instance == null || MaipadDynamicCreatedSpriteTank.instance.GetLoadEndRate() < 1.0f) {
			yield return null;
		}
		if (!UserDataController.instance.user.score_editor_processing) {
			Application.LoadLevel ("SceneTitle");
		}
		else {
			yield return StartCoroutine (EditorInformationController.instance.Load ());
			Application.LoadLevel ("SceneScoreEditor");
		}
	}
}
