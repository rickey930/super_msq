﻿using UnityEngine;
using System.Collections;

public interface IInfiniteScrollSetup
{
	IEnumerator OnPostSetupItems();
	void OnUpdateItem (int itemCount, GameObject obj);
	bool setupCompleted { get; }
	bool forceUpdateContents { get; set; }

}
